import axios from 'axios';

axios.defaults.xsrfCookieName = "CSRF-TOKEN";
axios.defaults.xsrfHeaderName = "X-CSRF-Token";
axios.defaults.withCredentials = true;

axios.interceptors.response.use(
  response => response,
  error => {
      if (error.response.status === 401) {
        window.location = './users/sign_in'
      }
      else{
        return Promise.reject(error);
      }
  }
);

export default axios;
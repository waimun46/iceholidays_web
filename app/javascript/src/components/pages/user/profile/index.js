import React, { Component } from 'react';
import { Card, Button, Form, Input } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link, withRouter } from 'react-router-dom';
import './index.css';

class Profile extends Component {
  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { xl: { span: 24 }, xl: { span: 24 }, },
      wrapperCol: { xl: { span: 24 }, xl: { span: 24 }, },
    };
    return (
      <div id="profile">
        <div className="user">
          <div className="icon_warp">
            <div className="icon_user">
              <FontAwesomeIcon icon='user' className="icon" />
            </div>
            <span className="title">Users</span>
          </div>
          <div className="btn_user">
            <Button type="primary">View All Users</Button>
          </div>
          <div className="clear"></div>
        </div>

        <div className="content_user">
          <div className="content_warp">
            <h2>User Detail</h2>
            <Card >
              <Form {...formItemLayout} onSubmit={this.handleSubmit}>

                <div className="input_warp">
                  <div className="formItemWarp50">
                    <Form.Item label="Code">
                      {getFieldDecorator('Code', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your E-mail!',
                          },
                        ],
                      })(<Input placeholder="Code" />)}
                    </Form.Item>
                  </div>
                  <div className="formItemWarp50">
                    <Form.Item label="Username">
                      {getFieldDecorator('Username', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Username!',
                          },
                        ],
                      })(<Input placeholder="Username" />)}
                    </Form.Item>
                  </div>
                  <div className="clear"></div>
                </div>

                <div className="input_warp">
                  <div className="formItemWarp50">
                    <Form.Item label="Full name">
                      {getFieldDecorator('Full_name', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Full name!',
                          },
                        ],
                      })(<Input placeholder="Full name" />)}
                    </Form.Item>
                  </div>
                  <div className="formItemWarp50">
                    <Form.Item label="Other name">
                      {getFieldDecorator('Other_name', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Other name!',
                          },
                        ],
                      })(<Input placeholder="Other name" />)}
                    </Form.Item>
                  </div>
                  <div className="clear"></div>
                </div>
                <div className="input_warp">
                  <div className="formItemWarp50">
                    <Form.Item label="Alias name">
                      {getFieldDecorator('Alias_name', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Alias name!',
                          },
                        ],
                      })(<Input placeholder="Alias name" />)}
                    </Form.Item>
                  </div>
                  <div className="formItemWarp50">
                    <Form.Item label="Address">
                      {getFieldDecorator('Address', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Address!',
                          },
                        ],
                      })(<Input placeholder="Address" />)}
                    </Form.Item>
                  </div>
                  <div className="clear"></div>
                </div>
                <div className="input_warp">
                  <div className="formItemWarp50">
                    <Form.Item label="City">
                      {getFieldDecorator('City', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your City!',
                          },
                        ],
                      })(<Input placeholder="City" />)}
                    </Form.Item>
                  </div>
                  <div className="formItemWarp50">
                    <Form.Item label="State">
                      {getFieldDecorator('State', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your State!',
                          },
                        ],
                      })(<Input placeholder="State" />)}
                    </Form.Item>
                  </div>
                  <div className="clear"></div>
                </div>
                <div className="input_warp">
                  <div className="formItemWarp50">
                    <Form.Item label="Postal">
                      {getFieldDecorator('Postal', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Postal!',
                          },
                        ],
                      })(<Input placeholder="Postal" />)}
                    </Form.Item>
                  </div>
                  <div className="formItemWarp50">
                    <Form.Item label="Country">
                      {getFieldDecorator('Country', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Country!',
                          },
                        ],
                      })(<Input placeholder="Country" />)}
                    </Form.Item>
                  </div>
                  <div className="clear"></div>
                </div>
                <div className="input_warp">
                  <div className="formItemWarp50">
                    <Form.Item label="Phones">
                      {getFieldDecorator('Phones', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Phones!',
                          },
                        ],
                      })(<Input placeholder="Phones" />)}
                    </Form.Item>
                  </div>
                  <div className="formItemWarp50">
                    <Form.Item label="Faxes">
                      {getFieldDecorator('Faxes', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Faxes!',
                          },
                        ],
                      })(<Input placeholder="Faxes" />)}
                    </Form.Item>
                  </div>
                  <div className="clear"></div>
                </div>
                <div className="input_warp">
                  <div className="formItemWarp50">
                    <Form.Item label="Emails">
                      {getFieldDecorator('Emails', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Emails!',
                          },
                        ],
                      })(<Input placeholder="Emails" />)}
                    </Form.Item>
                  </div>
                  <div className="formItemWarp50">
                    <Form.Item label="Website">
                      {getFieldDecorator('Website', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Website!',
                          },
                        ],
                      })(<Input placeholder="Website" />)}
                    </Form.Item>
                  </div>
                  <div className="clear"></div>
                </div>
                <div className="input_warp">
                  <div className="formItemWarp50">
                    <Form.Item label="Max sign in">
                      {getFieldDecorator('Max_sign_in', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Max sign in!',
                          },
                        ],
                      })(<Input placeholder="Max sign in" />)}
                    </Form.Item>
                  </div>
                  <div className="formItemWarp50">
                    <Form.Item label="Role">
                      {getFieldDecorator('Role', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Role!',
                          },
                        ],
                      })(<Input placeholder="Role" />)}
                    </Form.Item>
                  </div>
                  <div className="clear"></div>
                </div>
                <div className="input_warp">
                  <div className="formItemWarp50">
                    <Form.Item label="Is active">
                      {getFieldDecorator('Is active', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Is active!',
                          },
                        ],
                      })(<Input placeholder="Is active" />)}
                    </Form.Item>
                  </div>
                  <div className="formItemWarp50">
                    <Form.Item label="Domain access">
                      {getFieldDecorator('Domain_access', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Domain access!',
                          },
                        ],
                      })(<Input placeholder="Domain access" />)}
                    </Form.Item>
                  </div>
                  <div className="clear"></div>
                </div>
                <div className="input_warp">
                  <div className="formItemWarp50">
                    <Form.Item label="Credits">
                      {getFieldDecorator('Credits', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Credits!',
                          },
                        ],
                      })(<Input placeholder="Credits" />)}
                    </Form.Item>
                  </div>
                  <div className="formItemWarp50">
                    <Form.Item label="Last edited user">
                      {getFieldDecorator('Last_user', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Last edited user!',
                          },
                        ],
                      })(<Input placeholder="Last edited user" />)}
                    </Form.Item>
                  </div>
                  <div className="clear"></div>
                </div>
                <div className="input_warp">

                  <div className="formItemWarp50">
                    <Form.Item label="Last edited at">
                      {getFieldDecorator('Last_edited', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Last edited at',
                          },
                        ],
                      })(<Input placeholder="Last edited at" />)}
                    </Form.Item>
                  </div>
                  <div className="formItemWarp50">
                    <Form.Item label="Last reset password edited user">
                      {getFieldDecorator('Last_reset_at_user', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Last reset password edited user!',
                          },
                        ],
                      })(<Input placeholder="Last reset password edited user" />)}
                    </Form.Item>
                  </div>
                  <div className="clear"></div>
                </div>
                <div className="input_warp">
                  <div className="formItemWarp50">
                    <Form.Item label="Last reset password edited at">
                      {getFieldDecorator('Last_reset_password', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your reset password!',
                          },
                        ],
                      })(<Input placeholder="Last reset password edited at" />)}
                    </Form.Item>
                  </div>
                  <div className="formItemWarp50">
                    <Form.Item label="Remark">
                      {getFieldDecorator('Remark', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Remark!',
                          },
                        ],
                      })(<Input placeholder="Remark" />)}
                    </Form.Item>
                  </div>
                  <div className="clear"></div>
                </div>


                <div className="input_warp btn_select">
                  <div className="btn_warp">
                    <Button type="primary">Edit</Button>
                  </div>
                  <div className="btn_warp">
                    <Button className="reset">Reset Password</Button>
                  </div>
                  <div className="btn_warp">
                    <Button className="topup">Topup Credits</Button>
                  </div>
                  <div className="btn_warp">
                    <Button type="dashed" className="cancel">Cancel</Button>
                  </div>
                </div>

                <div className="clear"></div>

              </Form>
            </Card>
          </div>

        </div>



        <div className="clear"></div>
      </div>
    );
  }
}

const ProfilePage = Form.create({ name: 'Profile' })(Profile);
export default withRouter(ProfilePage);

import React, { Component } from 'react'

export default class Buttons extends Component {
    render(props) {
        let { className, title, task, disabled } = this.props
        return (
            <button className={className} onClick={task} disabled={disabled}>{title}</button>
        )
    }
}
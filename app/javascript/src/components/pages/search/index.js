import React, { Component } from 'react';
import { Menu, Dropdown, Icon, Tabs } from 'antd';
import Series from './series';
import Flight from './flight';
import Hotel from './hotel';
import Sightseeing from './sightseeing';
import Attraction from './attraction';
import Wifi from './wifi';
import LandTour from './land_tour';
import pdpa from '../../../../../assets/images/pdpa.pdf'
import manual from '../../../../../assets/images/manual.pdf'
import logo from '../../../../images/hk.png';
import ice from '../../../../images/logo.png';
import API from '../../../api'
import { withRouter, Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './index.css'

const { TabPane } = Tabs;

function callback(key) {
  localStorage.setItem('SearchTabKey', key);
  //console.log(key);
}

class Search extends Component {

  constructor(props) {
    super(props);
    const prevTab = localStorage.getItem('SearchTabKey');
    this.state = {
      prevTab: prevTab === undefined || prevTab === null ? 'wifi' : prevTab,
      currentTab: 'wifi',
      user: { username: '', credits: 0, access_flight: false, access_wifi: true, access_series: false, access_activity: false, access_hotel: false, access_busferry: false, busferry_link: '', access_land_tour: false },
      isSearching: false
    }
  }

  componentDidMount() {
    const that = this;

    API.get('./api/v1/users/details')
      .then(function (response) {
        console.log(response, '------------res');
        that.setState({
          user: { username: response.data.username, credits: response.data.credits, access_flight: response.data.access_flight, access_wifi: response.data.access_wifi, access_series: response.data.access_series, access_activity: response.data.access_activity, access_hotel: response.data.access_hotel, access_busferry: response.data.access_busferry, busferry_link: response.data.busferry_link, access_land_tour: response.data.access_land_tour },
          currentTab: that.state.prevTab,
          isLoading: false,
        })
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }

  handleChange = (key) => {
    if(key === 'bus_ferry'){
      ;
    } else {
      localStorage.setItem('SearchTabKey', key);
      //console.log(key);
      this.setState({
        currentTab: key,
      });
    }
  }

  render() {

    const menu = (
      this.state.user.username !== "ktic" ?
        (<Menu>
          <Menu.Item disabled={true}>Credit: {this.state.user.credits}</Menu.Item>
          <Menu.Divider></Menu.Divider>
      
          {
            this.state.user.access_wifi ? 
              (<Menu.Item><Link to="/bookings/wifis">My Bookings</Link></Menu.Item>) : 
            this.state.user.access_series ? 
              (<Menu.Item><Link to="/bookings/series">My Bookings</Link></Menu.Item>) :
            this.state.user.access_flight ? 
              (<Menu.Item><Link to="/bookings/flight">My Bookings</Link></Menu.Item>) :
            this.state.user.access_activity ? 
              (<Menu.Item><Link to="/bookings/activity">My Bookings</Link></Menu.Item>) : ""
          }
          <Menu.Item><a href={homepage_new.pdpa} target="_blank">PDPA</a></Menu.Item>
          <Menu.Item><a href="./user/edit">Change Password</a></Menu.Item>
          <Menu.Item><a href="./users/sign_out" >Logout</a></Menu.Item>
        </Menu>)
      : 
        <Menu>
          <Menu.Item><a href={homepage_new.pdpa} target="_blank">PDPA</a></Menu.Item>
        </Menu>
    );

    const pathUrl = this.props.location.pathname;
    console.log(pathUrl, '-----pathUrl')

    return (
      <div className="Search">
        {/************************ Top Banner ************************/}
        <div className="img_warp">
          {/* <img src={this.props.cover[0].img} alt="cover" /> */}
          <img src={this.props.cover} alt="cover" />
        </div>

        {/************************ Top Title ************************/}
        <div className="top_pannel">
          {/* <Button type="primary" icon="login" size={30}>
              LOGIN
          </Button> */}
          {/* <div>
              <Avatar style={{ backgroundColor: '#f9ab25' }} icon="user" />
              <span className="name_user">Jason Lim</span>
          </div> */}
          <span className="logo_warp">

            {
              pathUrl === "/" ? (
                <div><img src={homepage_new.app_logo} alt="travelb2b.my" /><span>TRAVELB2B</span></div>
              ) : (
                  <Link to="/" className="a_link">
                    <img src={homepage_new.app_logo} alt="travelb2b.my" /><span>TRAVELB2B</span>
                  </Link>

                )
            }


          </span>

          <Dropdown overlay={menu} trigger={['click']}>
            <a className="ant-dropdown-link drop_btn" href="#">
              {this.state.user.username} <Icon type="down" />
            </a>
          </Dropdown>
        </div>
        {/************************ Top Title ************************/}
        <div className="title">
          <p>{this.props.tagline}</p>
        </div>

        <div className="tab_warp">

          <Tabs onChange={this.handleChange} type="card" activeKey={this.state.currentTab} >

            {/************************ Series Tab ************************/}
            {
              this.state.user.access_series && <TabPane tab={<span><FontAwesomeIcon icon="clipboard" className="tab_icon" />SERIES TOUR</span>} key="series">
                <Series activeTab={this.state.currentTab} />
              </TabPane>
            }
          {/************************ LandTour Tab ************************/}
            {
              this.state.user.access_land_tour && <TabPane tab={<span><FontAwesomeIcon icon='map-marker-alt' className="tab_icon" />GROUND TOUR</span>} key="land_tour">
                <LandTour activeTab={this.state.currentTab} />
              </TabPane>
            }
          {/************************ Sightseeing Tab ************************/}
            {
              this.state.user.access_activity && <TabPane tab={<span><FontAwesomeIcon icon='camera-retro' className="tab_icon" />SIGHTSEEING</span>} key="sightseeing">
                <Sightseeing />
              </TabPane>
            }
          {/************************ Attraction Tab ************************/}
            {
              this.state.user.access_activity && <TabPane tab={<span><FontAwesomeIcon icon='ticket-alt' className="tab_icon" />ATTRACTION TICKET</span>} key="attraction">
                <Attraction />
              </TabPane>
            }
            {/************************ Wifi Tab ************************/}
            {
              this.state.user.access_wifi && <TabPane tab={<span><FontAwesomeIcon icon='wifi' className="tab_icon" />WIFI</span>} key="wifi">
                <Wifi />
              </TabPane>
            }
            {/************************ Flight Tab ************************/}
            {
              this.state.user.access_flight && <TabPane tab={<span><FontAwesomeIcon icon='plane-departure' className="tab_icon" />FLIGHT</span>} key="flight">
                <Flight activeTab={this.state.currentTab} />
              </TabPane>
            }

            {/************************ Hotel Tab ************************/}
            {
              this.state.user.access_hotel && <TabPane tab={<span><FontAwesomeIcon icon='hotel' className="tab_icon" />HOTEL</span>} key="hotel">
                <Hotel activeTab={this.state.currentTab} />
              </TabPane>
            }
            {/************************ Bus & Ferry Tab ************************/}
            {
              this.state.user.access_busferry && <TabPane tab={<a href={this.state.user.busferry_link} ><span><FontAwesomeIcon icon='bus' className="tab_icon" /> <FontAwesomeIcon icon='ship' className="tab_icon" />BUS & Ferry</span></a>} key="bus_ferry">
              </TabPane>
            }
            {
              <TabPane tab={<span><FontAwesomeIcon icon='plane-departure' className="tab_icon" /> <FontAwesomeIcon icon='hotel' className="tab_icon" /> <FontAwesomeIcon icon='bus' className="tab_icon" /> <FontAwesomeIcon icon='ship' className="tab_icon" />COMING SOON</span>} disabled key="comingsoon">
              </TabPane>
            }

          </Tabs>
        </div>

      </div>
    );
  }
}

export default withRouter(Search);

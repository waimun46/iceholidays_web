import React, { Component } from 'react';
import ShowMenu from './menu';
import ShowFlight from './flight';
import ShowHotel from './hotel';
import './index.css'

class ShowPage extends Component {
    render() {
        return (
            <div id="ShowPage">
              <ShowFlight/>
              {/* <ShowHotel/> */}
            </div>
        );
    }
}

export default ShowPage;

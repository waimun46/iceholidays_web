import React, { Component } from 'react';
import {
  Form, Tooltip, Modal, Row, Col, PageHeader, Icon, Tag, Rate, Card, List, Avatar, Collapse, Input, Select, Button, Divider, Checkbox,
  Empty, DatePicker, AutoComplete, InputNumber, Skeleton, notification, Typography, Tabs
} from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './index.css';
import moment from 'moment';
import ReactMarkdown from 'react-markdown/with-html';
import master from '../../../../../images/payment/master.png';
import instant from '../../../../../images/payment/instant.png'
import fpx from '../../../../../images/payment/fpx.png';
import warning from '../../../../../images/warning.png';
import { Link, withRouter } from 'react-router-dom';
import countryList from 'react-select-country-list';
import qs from 'qs';
import API from '../../../../api';
import flightImg from '../../../../../images/flight.png';
import PaymentMethod from '../payment';
import PriceDetails from '../priceDetails';
import Humanize from 'humanize-string';

const { TabPane } = Tabs;
const { Option } = Select;
const { TextArea } = Input;
const dateFormat = 'DD MMM YYYY';
// const designations = ['Miss', 'Mr', 'Mrs', 'Ms', 'Mstr', 'Mdm'];
// const fareTypes = ['adult', 'child_with_bed', 'child_no_bed', 'child_twin' /* 'infant_price' */]

function onChange(value) {
  console.log(`selected ${value}`);
}

function onBlur() {
  console.log('blur');
}

function onFocus() {
  console.log('focus');
}

function onSearch(val) {
  console.log('search:', val);
}

function thingsToKnowShort(string) {
   if (string.length > 25)
      return string.substring(0,25) + '...';
   else
      return string;
};

class LeftPanel extends Component {

  constructor(props) {
    super(props);
    this.state = {
      // Modal state
      moreInfoModal: false,
      moreInfoModalcontent: '',
      bookingConfirmModal: false,
      confirmLoading: false,
      confirmCheck: [],
      bookingErrorModal: false,
      bookingErrorMessage: '',
      confirm_button_disabled: true,
      // Payment state
      method_visible: false,
      payment_type: '',
      payment_method: null,
      check_out_disabled: false,
      banklist: [],
      pbb_banklist: [],
      offline_banklist: '',
      banklist_select: [],
      // credits: 0,
      // Data State
      designations: '',
      quantity: 1,
      seats: '',
      sub_sale_rep: '',
      tour_id: '',
      detail: [],
      passengers: [],
      validationErrors: [],
      total_adult_twin: 0,
      total_child_with_bed: 0,
      total_child_no_bed: 0,
      agree: '',
      fare_type: ''
      // infant: [{ name: '', date_brith: '', gender: '' }],
      // showInfants: false,
    }
  };

  priceFormat(value) {
    // return value === undefined ? 0 : value.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') // this is for RM X,XXX.XX format
    return value === undefined ? 0 : value.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') // this is for RM X,XXX format
  }

  handleCallback = (key) => {
    this.props.handleCallback(key)
    // Reset the payment method value when user clicked the Tour
    this.setState({
      payment_type: "",
      banklist_select: [],
      method_visible: false,
      payment_method: null
    })
  };

  handleQuantity = (value) => {
    this.props.handleQuantity(value)
  };

  handlePaymentType = (value) => {
    this.props.handlePaymentType(value)
  };

  /****************************** onChangeRemark *******************************/
  onChangeData = (index, type, value, e = '') => {
    if (type == "sub_sale_rep") {
      this.setState({
        sub_sale_rep: value
      });
    } else if (type == "quantity") {
      let max_booking_seats = this.props.dataList && this.props.dataList.tours[this.props.tourKey].max_booking_seats;
      let price_type_index = 0
      let total_seats = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].seats
      let prices = []

      for (var i = 0; i < max_booking_seats; i++) {
        if (i < value) {
          this.state.passengers[i]['visible'] = true
          // Reset category to "adult" if the selected option is not "adult" or "child_twin"
          if (this.state.passengers[i]['category'] === "adult" || this.state.passengers[i]['category'] === "child_twin" || this.state.passengers[i]['category'] === "child_with_bed" || this.state.passengers[i]['category'] === "child_no_bed") {
            this.state.passengers[i]['category']
          } else {
            this.state.passengers[i]['category'] = "adult"
          }
          if (this.state.passengers[i] && this.state.passengers[i].visible === true) {
              let found_price = prices.find(x => x.name === this.state.passengers[i].category && x.type === this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).type)
              if (found_price === undefined) {
                prices.push({
                  name: this.state.passengers[i].category,
                  type: this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).type,
                  quantity: 1,
                  price: this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).price,
                  dta: this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).dta
                })
              } else {
                found_price.quantity += 1;
              }
            }
        } else {
          this.state.passengers[i]['visible'] = false
          // Reset category to "adult" if the selected option is not "adult" or "child_twin"
          if (this.state.passengers[i]['category'] === "adult" || this.state.passengers[i]['category'] === "child_twin") {
            this.state.passengers[i]['category']
          } else {
            this.state.passengers[i]['category'] = "adult"
          }
        }
      }

      let total_adult_twin = 0;
      let total_child_with_bed = 0;
      let total_child_no_bed = 0;
      prices.filter(x => x.name.includes('adult') || x.name.includes('child_twin')).map((price) => { total_adult_twin += price.quantity })
      prices.filter(x => x.name.includes('child_with_bed')).map((price) => { total_child_with_bed += price.quantity })
      prices.filter(x => x.name.includes('child_no_bed')).map((price) => { total_child_no_bed += price.quantity })

        this.setState({
          // detail: this.props.dataList,
          passengers: this.state.passengers,
          quantity: value,
          total_adult_twin: total_adult_twin,
          total_child_with_bed: total_child_with_bed,
          total_child_no_bed: total_child_no_bed
        // });
        }, () => { this.handleQuantity(prices) });
      // if (type == "quantity"){
      //   this.setState({
      //     // detail: this.props.dataList,
      //     passengers: this.state.passengers,
      //     prices: prices,
      //     quantity: value
      //   }, () => { this.handleQuantity(prices) });
      // } else {
      //   console.log(this.state.passengers[index], "this.state.passengers[i]")
      //   this.state.passengers[index][type] = value;
      //   this.setState({ 
      //     passengers: this.state.passengers,
      //     prices: prices
      //   }, () => { this.handleQuantity(prices) });
      // }
    }
    else {
      let prices = []

      this.state.passengers[index][type] = value;

      for(var i = 0; i < this.state.passengers.length; i ++){
        if (this.state.passengers[i]['visible']) {
          if (this.state.passengers[i] && this.state.passengers[i].visible === true) {
            let found_price = prices.find(x => x.name === this.state.passengers[i].category && x.type === this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).type)
            if (found_price === undefined) {
              prices.push({
                name: this.state.passengers[i].category,
                type: this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).type,
                quantity: 1,
                price: this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).price,
                dta: this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).dta
              })
            } else {
              found_price.quantity += 1;
            }
          }
        }
      }

      let total_adult_twin = 0;
      let total_child_with_bed = 0;
      let total_child_no_bed = 0;
      prices.filter(x => x.name.includes('adult') || x.name.includes('child_twin')).map((price) => { total_adult_twin += price.quantity })
      prices.filter(x => x.name.includes('child_with_bed')).map((price) => { total_child_with_bed += price.quantity })
      prices.filter(x => x.name.includes('child_no_bed')).map((price) => { total_child_no_bed += price.quantity })

      this.setState({ 
        passengers: this.state.passengers,
        total_adult_twin: total_adult_twin,
        total_child_with_bed: total_child_with_bed,
        total_child_no_bed: total_child_no_bed
      }, () => { this.handleQuantity(prices) });
    }
  };

  // onChangeDataBooking = (i, type, value, e = '') => {
  //   if (type == "sub_sale_rep") {
  //     this.setState({ 
  //       sub_sale_rep: value
  //     });
  //   } else if (type == "quantity") {
  //     // let addPassenger = this.state.passengers.concat({ designation: '', name: '', date_of_birth: '', passport_number: '', category: this.state.passengers[0]['category'], remark: '' }).filter((s, sidx) => value > sidx)

  //     // if (this.props.dataList != this.state.detail) {
  //       // let addPassengers = [];
  //       let max_booking_seats = this.props.dataList && this.props.dataList.tours[this.props.tourKey].max_booking_seats;
  //       let price_type_index = 0
  //       let total_seats = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].seats

  //         for (var i = 0; i < max_booking_seats; i++) {
  //           if (i < value) {
  //             this.state.passengers[i]['visible'] = true
  //           } else {
  //             this.state.passengers[i]['visible'] = false
  //           }

  //           if (i >= total_seats && price_type_index < total_seats) {
  //             price_type_index += 1
  //             total_seats += this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].seats
  //           }
  //           this.state.passengers[i].price_types.find(x => x.name === 'adult').price = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].adult
  //           this.state.passengers[i].price_types.find(x => x.name === 'child_with_bed').price = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].child_with_bed
  //           this.state.passengers[i].price_types.find(x => x.name === 'child_no_bed').price = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].child_no_bed
  //           this.state.passengers[i].price_types.find(x => x.name === 'child_twin').price = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].child_twin
          
  //           if (this.state.passengers[i].visible === true) {
  //             this.state.passengers[i].price_types.map((price_type,price_type_index) => {
  //               this.state.prices[price_type_index].name = price_type.name
  //               this.state.prices[price_type_index].quantity = this.state.passengers.filter(x => x.category === price_type.name && x.visible === true).length
  //             })
  //           }

  //         }

  //         this.setState({
  //           detail: this.props.dataList,
  //           passengers: this.state.passengers,
  //           prices: this.state.prices,
  //           quantity: value
  //         })
  //     // }
  //     this.handleQuantity(this.state.passengers)
  //   } else {
  //     //
  //   }
  // }

  onOpenModal = (modal, key, content) => {
    this.setState({
      moreInfoModal: true,
      moreInfoModalcontent: content
    });
  };

  onCloseModal = (modal) => {
    this.setState({
      bookingErrorModal: false,
      bookingConfirmModal: false,
      moreInfoModal: false
    });
  };

  // onClickMore = () => {
  //   this.setState({
  //     moreInfoModal: true,
  //   });
  // };

  onConfirmBooking = event => {
    event.preventDefault();

    let prices = []
      for(var i = 0; i < this.state.passengers.length; i ++){
        if (this.state.passengers[i]['visible']) {
          if (this.state.passengers[i] && this.state.passengers[i].visible === true) {
            let found_price = prices.find(x => x.type === this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).type)
            if (found_price === undefined) {
              prices.push({
                name: this.state.passengers[i].category,
                type: this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).type,
                quantity: 1,
                price: this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).price,
                dta: this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).dta
              })
            } else {
              found_price.quantity += 1;
            }
          }
        }
      }
      this.setState({ 
        passengers: this.state.passengers,
        confirm_button_disabled: false
      }, () => { this.handleQuantity(prices) });

    // setTimeout(() => {
      const { seats, quantity, sub_sale_rep, tour_id, passengers, payment_type, payment_method, validationErrors, agree, fare_type} = this.state;
      
      let outputJson = {
        booking_seats: prices.map((x) => { return x.quantity + " " + x.type + "(" + x.price + ")" }).join(", "),
        sub_sale_rep: sub_sale_rep,
        tour_id: this.props.dataList.tours && this.props.dataList.tours[this.props.tourKey].id,
        bookings: passengers.filter(x => x.visible === true),
        payment: { gateway: payment_type, method: payment_method },
        fare_type: fare_type,
        agree: agree
      };
      console.log("This is the outputJson", outputJson);
      
      let passengerValidate = passengers.filter(x => x.visible === true)
      console.log("passengerValidate", passengerValidate);
      console.log("validationErrors", validationErrors);

      this.props.form.validateFieldsAndScroll((err, values) => {
        if(payment_type === "" || (payment_type === 'credit_transaction' || payment_type === 'pay_later' ? "" : payment_method === null) || agree === "" || !err === false) {
            if (!err) {
              console.log('Received values of form: ', values);
            }
            if (payment_type === "") {
                this.openNotificationWithIcon('warning','payment_method_error');
            }
            if (payment_method === null) {
                validationErrors.payment_method = "*Please select a payment method";
                this.setState({ validationErrors: validationErrors })
            } else {
                validationErrors.payment_method = null;
                this.setState({ validationErrors: validationErrors })
            }
            if (agree === "") {
                validationErrors.agree = "*Please agreed the Terms of Use and Privacy Statement";
                this.setState({ validationErrors: validationErrors })
            } else {
                validationErrors.agree = "";
                this.setState({ validationErrors: validationErrors })
            }
        } else {
          this.setState({
            bookingConfirmModal: true,
          });
        }
      });
      console.log("Check validationerros",this.state.validationErrors)
    // },100);
  };

  // handleCancel = () => {
  //   console.log('Clicked cancel button');
  //   this.setState({
  //     bookingConfirmModal: false,
  //     moreInfoModal: false
  //   });
  // };

  /****************************** onChangeConfirmationCheckbox *******************************/
  onChangeConfirmationCheckbox(type, e = '') {
    this.state.confirmCheck[type] = e.target.checked
    this.setState({
      confirmCheck: this.state.confirmCheck
    })
    console.log("Confirm Check ---> ", this.state.confirmCheck)
  };

  /****************************** onChangePaymentCheckBox *******************************/
  onChangePaymentCheckBox(e) {
    let isChecked = e.target.checked === true ? "agree" : "";
    console.log(`checked = ${isChecked}`);
    this.setState({
      agree: isChecked
    })
  };

  /****************************** handlePaymentChange *******************************/
  handlePaymentChange(event) {
    console.log(event.target.value);
    if (event.target.value === 'public_bank') {
      this.setState({
        method_visible: true,
        banklist_select: this.state.pbb_banklist,
        payment_method: null
      })
    }
    else if (event.target.value === 'fpx_b2c') {
      this.banklistFetch();
      this.setState({
        method_visible: true,
        banklist_select: this.state.banklist,
        payment_method: null
      })
    }
    else /** if (event.target.value === 'credit_transaction') **/ {
      this.setState({
        banklist_select: [],
        method_visible: false,
        payment_method: null
      })
    }
    this.handlePaymentType(event.target.value)

    this.setState({
      payment_type: event.target.value
    })
  };

  handlePaymentMethodChange(value) {
    this.setState({
      payment_method: value
    })
    console.log("card method",value)
  };

  openNotificationWithIcon = (type, error_name) => {
    if (type === "warning") {
      if (error_name === "payment_method_error") {
        notification[type]({
          message: 'Warning!',
          description:
            'Please select payment method before you continue.',
        });
      }
    }
    if (type === "error") {
      if (error_name === "credits_error") {
        notification[type]({
          message: 'Error!',
          description:
            'Insufficient credits! please purchase more credits.',
        });
      }
    }
  };

  getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 
    {
      age--;
    }
    return age;
  };

  // /****************************** addInfants *******************************/
  // addInfants = () => {
  //   this.setState({
  //     infant: this.state.infant.concat([{ name: '', date_brith: '', gender: '' }]),
  //     showInfants: true,
  //   });
  // }; 
  // /****************************** removeInfants *******************************/
  // removeInfants = idx => () => {
  //   this.setState({
  //     infant: this.state.infant.filter((s, sidx) => idx !== sidx)
  //   });
  // };

  designationFetch() {
      let url = './api/v1/series/designation_list';
      var that = this;
      // console.log(params);

      API.get(url)
        .then(function (response) {
            // console.log(response, '------------res country');
            that.setState({
                designations: response.data.designations
            })
        })
        .catch(function (error) {
            console.log(error);
        })
        .then(function () {
            // always executed
        });
  };

  banklistFetch() {
    let url = './api/v1/payments/payment_method';
    var that = this;
    // console.log(params);

    API.get(url)
      .then(function (response) {
        // console.log(response, '------------res banklist');
        that.setState({
          banklist: response.data.banklist,
          pbb_banklist: response.data.pbb_banklist,
          offline_banklist: response.data.offline_banklist,
          // credits: response.data.credits
        })
        that.props.getCredits(response.data.credits)
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  };

  componentDidMount() {
    this.designationFetch();
    this.banklistFetch();
  };

  componentDidUpdate(prevProps) {
    if(this.props.dataList.hasOwnProperty("tours")){
      
      let passengers = [];
      let prices = [];

      if(this.state.passengers.length == 0){
        for (var i = 0; i < this.props.dataList.max_booking_seats; i++) {
          passengers.push({
            designation: 'Mr',
            name: '',
            date_of_birth: '',
            mobile: '',
            passport_number: '',
            category: 'adult', 
            remark: '', 
            price_types: [
                { name: 'adult', visiable: true },
                { name: 'child_twin', visiable: false },
                { name: 'child_with_bed', visiable: false },
                { name: 'child_no_bed', visiable: false }
            ],
            visible: false
          })
        }
      } else{
        passengers = this.state.passengers
      }

      let max_booking_seats = this.props.dataList && this.props.dataList.tours[this.props.tourKey].max_booking_seats;
      let price_type_index = 0;
      let total_seats = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].seats;
      let quantity = this.state.quantity > max_booking_seats ? max_booking_seats : this.state.quantity

      for (var i = 0; i < passengers.length; i++) {

        if (i < quantity) {
          passengers[i]['visible'] = true
        } else {
          passengers[i]['visible'] = false
        }

        if (i >= total_seats && price_type_index < this.props.dataList.tours[this.props.tourKey].prices.length - 1) {
          price_type_index += 1
          total_seats += this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].seats
        }

        // Validation of category dropdown select
        if (passengers[i] && passengers[i].visible === true) {
          for (var k = 0; k < passengers[i].price_types.length; k++) {
            if(passengers[i].category == "adult" || passengers[i].category == "child_twin") {
              total_adult_twin = this.state.total_adult_twin - 1
            } else {
              total_adult_twin = this.state.total_adult_twin
            }

            let left_child_no_bed = Math.floor(total_adult_twin / 2)
            let left_total_child_with_bed = 0

            if(total_adult_twin % 2 == 1){
              left_total_child_with_bed = Math.floor((total_adult_twin - 3)/2)
            } else {
              left_total_child_with_bed = Math.floor(total_adult_twin/2)
            }
            
            // console.log(total_adult_twin, "total_adult_twin")
            // console.log(left_total_child_with_bed, "left_total_child_with_bed")
            // console.log(left_child_no_bed, "left_child_no_bed")

            if(passengers[i].price_types[k].name == "child_no_bed"){
              passengers[i].price_types[k].visible = (passengers[i].category == "child_no_bed") || ((left_total_child_with_bed - this.state.total_child_with_bed) >= 0 &&  (left_child_no_bed - this.state.total_child_no_bed) > 0)
            } else if(passengers[i].price_types[k].name == "child_with_bed"){
              passengers[i].price_types[k].visible = (passengers[i].category == "child_with_bed") || ((left_total_child_with_bed - this.state.total_child_with_bed) > 0 &&  (left_child_no_bed - this.state.total_child_no_bed) >= 0)
            } else {
              passengers[i].price_types[k].visible =  (passengers[i].category != "child_no_bed") || this.state.total_child_with_bed == 0 || (left_total_child_with_bed - this.state.total_child_with_bed) > 0
            }
            // console.log(passengers[i].price_types[k].visible, passengers[i].price_types[k].name)
          }

          let child_options = passengers[i].price_types.filter(x => x.name.includes("child"))
          for (var j = 0; j < child_options.length; j++) {
            // if date_of_birth is more than 12 years old, then disable the child options
            // console.log(passengers[i].date_of_birth, "passengers[i].date_of_birth")
            child_options[j].visible &= (this.getAge(passengers[i].date_of_birth) <= 12)
          }
        } 
        passengers[i].price_types.find(x => x.name === 'adult').price = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].adult
        passengers[i].price_types.find(x => x.name === 'child_with_bed').price = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].child_with_bed
        passengers[i].price_types.find(x => x.name === 'child_no_bed').price = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].child_no_bed
        passengers[i].price_types.find(x => x.name === 'child_twin').price = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].child_twin

        passengers[i].price_types.find(x => x.name === 'adult').type = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].type
        passengers[i].price_types.find(x => x.name === 'child_with_bed').type = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].type
        passengers[i].price_types.find(x => x.name === 'child_no_bed').type = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].type
        passengers[i].price_types.find(x => x.name === 'child_twin').type = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].type

        passengers[i].price_types.find(x => x.name === 'adult').dta = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].dta_adult
        passengers[i].price_types.find(x => x.name === 'child_with_bed').dta = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].dta_child_with_bed
        passengers[i].price_types.find(x => x.name === 'child_no_bed').dta = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].dta_child_no_bed
        passengers[i].price_types.find(x => x.name === 'child_twin').dta = this.props.dataList && this.props.dataList.tours[this.props.tourKey].prices[price_type_index].dta_child_twin
      }
    
      for (var i = 0; i < quantity; i++) {
        if (passengers[i] && passengers[i].visible === true) {
          let found_price = prices.find(x => x.name === passengers[i].category && x.type === passengers[i].price_types.find(x => x.name === passengers[i].category).type)
          if (found_price === undefined) {
            prices.push({
              name: passengers[i].category,
              type: passengers[i].price_types.find(x => x.name === passengers[i].category).type,
              quantity: 1,
              price: passengers[i].price_types.find(x => x.name === passengers[i].category).price,
              dta: passengers[i].price_types.find(x => x.name === passengers[i].category).dta
            })
          } else {
            found_price.quantity += 1;
          }
        }
      }

      let total_adult_twin = 0;
      let total_child_with_bed = 0;
      let total_child_no_bed = 0;
      // let payment_method
      // let fare_type = this.props.dataList.tours && this.props.dataList.tours[this.props.tourKey].guaranteed_departure === false ? "deposit" : "full fare"
      let fare_type = this.props.dataList.tours && moment(this.props.dataList.tours[this.props.tourKey].cut_off_date,'DD-MM-YYYY').isAfter() ? "deposit" : "full fare"

      prices.filter(x => x.name.includes('adult') || x.name.includes('child_twin')).map((price) => { total_adult_twin += price.quantity })
      prices.filter(x => x.name.includes('child_with_bed')).map((price) => { total_child_with_bed += price.quantity })
      prices.filter(x => x.name.includes('child_no_bed')).map((price) => { total_child_no_bed += price.quantity })

      if (prevProps.tourKey !== this.props.tourKey || prevProps.dataList !== this.state.detail) {
        this.setState({
          detail: this.props.dataList,
          passengers: passengers,
          prices: prices,
          quantity: quantity,
          total_adult_twin: total_adult_twin,
          total_child_with_bed: total_child_with_bed,
          total_child_no_bed: total_child_no_bed,
          fare_type: fare_type
        })
        this.handleQuantity(prices)
      }
    }
  };

  submitForm = event => {
    event.preventDefault();
    this.banklistFetch();

    this.setState({
      confirmLoading: true,
      confirm_button_disabled: true
    });

    let prices = []
      for(var i = 0; i < this.state.passengers.length; i ++){
        if (this.state.passengers[i]['visible']) {
          if (this.state.passengers[i] && this.state.passengers[i].visible === true) {
            let found_price = prices.find(x => x.type === this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).type)
            if (found_price === undefined) {
              prices.push({
                name: this.state.passengers[i].category,
                type: this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).type,
                quantity: 1,
                price: this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).price,
                dta: this.state.passengers[i].price_types.find(x => x.name === this.state.passengers[i].category).dta
              })
            } else {
              found_price.quantity += 1;
            }
          }
        }
      }
      this.setState({ 
        passengers: this.state.passengers
      }, () => { this.handleQuantity(prices) });

    setTimeout(() => {
      const { seats, quantity, sub_sale_rep, tour_id, passengers, payment_type, payment_method, validationErrors, agree, fare_type} = this.state;
      
      let outputJson = {
        booking_seats: prices.map((x) => { return x.quantity + " " + x.type + "(" + x.price + ")" }).join(", "),
        sub_sale_rep: sub_sale_rep,
        tour_id: this.props.dataList.tours && this.props.dataList.tours[this.props.tourKey].id,
        bookings: passengers.filter(x => x.visible === true),
        payment: { gateway: payment_type, method: payment_method },
        agree: agree,
        fare_type: fare_type
      };
      console.log("This is the outputJson", outputJson);
      
      // let passengerValidate = passengers.filter(x => x.visible === true)
      // console.log("passengerValidate", passengerValidate);
      // console.log("validationErrors", validationErrors);
      this.setState({
        check_out_disabled: true,
      })

      let url = './api/v1/series/' + this.props.dataList.id + '/create_booking/';
      let that = this;

      API.post(url, outputJson)
        .then(function (response) {
            console.log(response,'------------res');
            console.log(response.data.redirect_to,'------------redirect');
            if(response.data.redirect_to){
              window.location = response.data.redirect_to
            } else if (response.data.credits_error) {
              that.openNotificationWithIcon('error','credits_error');
              that.setState({ payment_type: '', bookingConfirmModal: false, confirm_button_disabled: false })
            }
            else{
              that.setState({
                check_out_disabled: false,
                bookingErrorModal: true,
                bookingErrorMessage: response.data.error,
                confirm_button_disabled: false
              })
            }
            that.setState({
              loading: false,
              confirmLoading: false
            })
      })
      .catch(function (error) {
        console.log(error);
        that.setState({
          visible: true,
        })
      })
      .then(function () {
        // always executed
      });

    },100);
  };

  render() {
    const { bookingErrorModal, bookingErrorMessage, bookingConfirmModal, confirmLoading, confirmCheck, designations, quantity, method_visible, passengers, banklist_select, offline_banklist, payment_type, payment_method, /*credits,*/ validationErrors /* infant, showInfants */ } = this.state;
    const { getFieldDecorator } = this.props.form;
    const dataList = this.props.dataList

    /********************************* formItemLayout ******************************/
    const formItemLayout = {
      labelCol: { xl: { span: 24 }, xl: { span: 24 }, },
      wrapperCol: { xl: { span: 24 }, xl: { span: 24 }, },
    };

    // console.log("CHECK DATALIST ---> ", dataList)
    console.log("check passengers ---> ", passengers)
    console.log("check total adult twin ----> ", this.state.total_adult_twin)
    console.log("check total child with bed ----> ", this.state.total_child_with_bed)
    console.log("check total child no bed ----> ", this.state.total_child_no_bed)
    // const infantData = infant.slice(0,showInfants === true ? 2 : infant.length );

    return (
      <div className="LeftPanel">
        <div className="hotel_details">
          {/**************************** header title ****************************/}
          <div className="header_title">
            <h1>{dataList && dataList.tours[this.props.tourKey].caption}</h1>
            <p>
              <Tag color="orange">Itinerary : <strong>{dataList.code}</strong></Tag> 
              <Tag color="orange">Category : <strong>{dataList.category}</strong></Tag>
            </p>
          </div>

          {/************************************************ Form ************************************************/}
          <Form {...formItemLayout} onSubmit={this.onConfirmBooking} className="form_sty_select" >

          {/**************************** tab select ****************************/}
          <Tabs activeKey={`${this.props.tourKey}`} onChange={this.handleCallback}>
            {
              dataList && dataList.tours.map((tour, i) => {

                const price_columns = []
                const price_datas = []
                tour.prices.map((price, price_index) => {
                  let price_type_text = price.type
                  let dta_price_type_text = `dta_${price_type_text}`

                  price_columns.push({ key: price_index, dataIndex: price_type_text, title: <span>{price.display_price}<br /><span>{price_type_text.includes('normal') ? '' : Humanize(price_type_text)}</span></span>, rowSpan: 1, width: 200, className: 'priceTypeColumn' })

                  var found_adult = price_datas.find(x => x.name === 'Adult')
                  var found_child_twin = price_datas.find(x => x.name === 'Child Twin')
                  var found_child_no_bed = price_datas.find(x => x.name === 'Child No Bed')
                  var found_child_with_bed = price_datas.find(x => x.name === 'Child With Bed')

                  if (found_adult == undefined) {
                    price_datas.push({ key: price_index, name: "Adult", [price_type_text]: 'RM ' + this.priceFormat(price.adult), [dta_price_type_text]: 'RM ' + this.priceFormat(price.dta_adult) })
                    price_index++
                  } else {
                    found_adult[price_type_text] = 'RM ' + this.priceFormat(price.adult)
                    found_adult[dta_price_type_text] = 'RM ' + this.priceFormat(price.dta_adult)
                  }
                  if (found_child_twin == undefined) {
                    price_datas.push({ key: price_index, name: "Child Twin", [price_type_text]: 'RM ' + this.priceFormat(price.child_twin), [dta_price_type_text]: 'RM ' + this.priceFormat(price.dta_child_twin) })
                    price_index++
                  } else {
                    found_child_twin[price_type_text] = 'RM ' + this.priceFormat(price.child_twin)
                    found_child_twin[dta_price_type_text] = 'RM ' + this.priceFormat(price.dta_child_twin)
                  }
                  if (found_child_no_bed == undefined) {
                    price_datas.push({ key: price_index, name: "Child No Bed", [price_type_text]: 'RM ' + this.priceFormat(price.child_no_bed), [dta_price_type_text]: 'RM ' + this.priceFormat(price.dta_child_no_bed) })
                    price_index++
                  } else {
                    found_child_no_bed[price_type_text] = 'RM ' + this.priceFormat(price.child_no_bed)
                    found_child_no_bed[dta_price_type_text] = 'RM ' + this.priceFormat(price.dta_child_no_bed)
                  }
                  if (found_child_with_bed == undefined) {
                    price_datas.push({ key: price_index, name: "Child With Bed", [price_type_text]: 'RM ' + this.priceFormat(price.child_with_bed), [dta_price_type_text]: 'RM ' + this.priceFormat(price.dta_child_with_bed) })
                    price_index++
                  } else {
                    found_child_with_bed[price_type_text] = 'RM ' + this.priceFormat(price.child_with_bed)
                    found_child_with_bed[dta_price_type_text] = 'RM ' + this.priceFormat(price.dta_child_with_bed)
                  }
                })

                console.log("price_datas =====> ", price_datas)
                console.log("price_columns =====> ", price_columns)
                console.log("check_original_prices =====> ", tour.prices)
                
                return (
                  <TabPane 
                    tab={[
                        <div className="tab_select_style">
                          <p className="date_tab">{moment(tour.departure_date,'DD-MM-YYYY').format(dateFormat)} {tour.guaranteed_departure === false ? null : <Tag className="guaranteed_tag" color="green">G</Tag>}</p>
                          <p className="from_sty">From</p>
                          <p className="tab_price">RM {this.priceFormat(tour.price)}</p>
                        </div>
                      ]} 
                    key={i}
                    >

                    {/**************************** Things to know ****************************/}

                    <Card className="things">
                      <div className="quick-info">
                      <h3 className="title">Things to know</h3>
                        <Row className="things_to_know" gutter={2}>
                        {
                          tour.insurance === "" ? (
                            null
                          ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="user-shield" /></span>
                                  <span className="content_text">{tour.insurance}</span>
                                </div>
                            </Col>
                            )
                        }
                        {
                          this.state.fare_type === 'deposit' ? (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="male" /></span>
                                  <span className="content_text">Deposit per pax <strong>RM {this.priceFormat(tour.deposit)}</strong></span>
                                </div>
                            </Col>
                          ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="hand-holding-usd" /></span>
                                  <span className="content_text">Full Payment Required</span>
                                </div>
                            </Col>
                            )
                        }
                        {
                          tour.cut_off_date === "" ? (
                            null
                          ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="clock" /></span>
                                  <span className="content_text">
                                    <span className="cutoff">Cut Off :</span>
                                    {moment(tour.cut_off_date,"DD-MM-YYYY").format(dateFormat)}
                                  </span>
                                </div>
                            </Col>
                            )
                        }
                        {
                          tour.guaranteed_departure === false ? (
                            null
                          ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><Tag className="guaranteed_tag" color="green">G</Tag></span>
                                  <span className="content_text"></span>
                                  Guaranteed Departure
                                </div>
                            </Col>
                            )
                        }
                        {
                          dataList.file_url === "" ? (
                            null
                          ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="clipboard-list" /></span>
                                  <span className="content_text"><a href={dataList.file_url} target="_blank" >Itinerary Download</a></span>
                                </div>
                            </Col>
                            )
                        }
                        {
                          dataList.includings['airport_taxes'] === false ? (
                              null
                            ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="receipt" /></span>
                                  <span className="content_text">Airport Taxes</span>
                                </div>
                            </Col>
                            )
                        }
                        {
                          dataList.includings['group_departure'] === false ? (
                              null
                            ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="users" /></span>
                                  <span className="content_text">Group Departure</span>
                                </div>
                            </Col>
                            )
                        }
                        {
                          dataList.includings['tour_leader'] === false ? (
                              null
                            ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="user-check" /></span>
                                  <span className="content_text">Tour Leader</span>
                                </div>
                            </Col>
                            )
                        }
                        {
                          dataList.includings['luggage'] === false ? (
                              null
                            ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="shopping-bag" /></span>
                                  <span className="content_text">Check-in baggage</span>
                                </div>
                            </Col>
                            )
                        }
                        {
                          dataList.includings['red_luggage_protection'] === false ? (
                              null
                            ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="suitcase" /></span>
                                  <span className="content_text">Red Luggage Protection</span>
                                </div>
                            </Col>
                            )
                        }
                        {
                          dataList.includings['wifi'] === false ? (
                              null
                            ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="wifi" /></span>
                                  <span className="content_text">Share Wifi</span>
                                </div>
                            </Col>
                            )
                        }
                        {
                          dataList.includings['meal_onboard'] === false ? (
                              null
                            ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="utensils" /></span>
                                  <span className="content_text">Meal Onboard</span>
                                </div>
                            </Col>
                            )
                        }
                        {
                          dataList.includings['hotel'] === false ? (
                              null
                            ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="bed" /></span>
                                  <span className="content_text">Hotel</span>
                                </div>
                            </Col>
                            )
                        }
                        {
                          tour.pay_later === false ? (
                              null
                            ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="coins" /></span>
                                  <span onClick={() => this.onOpenModal('moreInfoModal', 0, 'This booking is accepted "Book Now, Pay Later". You are able to hold your booking at the same fare and make payment later up to 72 hours, booking will be auto-released if no payment received within 72 hours.')} className="content_text"><a>Book Now, Pay Later</a></span>
                                </div>
                            </Col>
                            )
                        }
                        {
                          dataList.includings['gratuities'] === false ? (
                              null
                            ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="dollar-sign" /></span>
                                  <span className="content_text">Gratuities</span>
                                </div>
                            </Col>
                            )
                        }
                        {
                          dataList.includings['acf'] === false ? (
                              null
                            ) : (
                            <Col className="gutter-row" md={12} lg={8}>
                                <div className="things_warp">
                                  <span className="info_icon"><FontAwesomeIcon fixedWidth icon="money-bill-alt" /></span>
                                  <span className="content_text">Agency Collection Fee (ACF)</span>
                                </div>
                            </Col>
                            )
                        }
                        <Col className="gutter-row" md={12} lg={8}>
                            <div className="things_warp">
                              <span className="info_icon"><FontAwesomeIcon fixedWidth icon="question-circle" /></span>
                              <span onClick={() => this.onOpenModal('moreInfoModal', 0, dataList.description)} className="content_text"><a>More</a></span>
                            </div>
                        </Col>
                        </Row>
                      </div>
                    </Card>

                    {/**************************** Remark ****************************/}
                    {
                      dataList.remark === '' || dataList.remark === undefined ?
                      (
                        null
                      ) :
                      (
                        <Card className="things">
                          <div className="quick-info">
                            <h3 className="title">Remark</h3>
                            <Row className="things_to_know" gutter={2}>
                              <Col className="gutter-row" md={12} lg={8}>
                                <p>{dataList.remark}</p>
                              </Col>
                            </Row>
                          </div>
                        </Card>
                      )
                    }

                    {/**************************** flight infor ****************************/}
                    {
                      tour.flights && tour.flights.map((flight, i) => {
                        return (
                          <Card className="flight_infor">
                            <h3 className="title">
                              <img src={flightImg} alt="img" />
                              <span>{flight.from_airport}</span>
                              <FontAwesomeIcon icon="long-arrow-alt-right" className="left_arrow" />
                              <span>{flight.to_airport}</span>
                            </h3>
                            <p className="date">{moment(flight.departure_date,"DD-MM-YYYY").format("dddd, " + dateFormat)}</p>
                            <div className="details">
                              <div className="logo">
                                <img src={flight.airline_logo !== '' ? flight.airline_logo : this.props.defaultImage} alt="img" />
                              </div>
                              <div className="content">
                                <p className="airline">{flight.airline}<span>{flight.flight_no}</span></p>
                                <p className="type"></p>
                                <div className="time">
                                  <div>
                                    <p className="time_style">{flight.departure_time}</p>
                                    <p className="country">{flight.from_airport}</p>
                                  </div>
                                  <div><FontAwesomeIcon icon="long-arrow-alt-right" className="left_arrow" /></div>
                                  <div>
                                    <p className="time_style">{flight.arrival_time}</p>
                                    <p className="country">{flight.to_airport}</p>
                                  </div>
                                  <div className="clear"></div>
                                </div>
                              </div>
                              <div className="clear"></div>
                            </div>
                          </Card>

                        )
                      })
                    }

                      {/**************************** Booking ****************************/}
                      <div className="header_title">
                        <h2>Your Booking</h2>
                      </div>
                      <Card title="Booking Selection" className="booking">
                        <p className="title_sub">Price Details</p>
                        <PriceDetails columns={price_columns} data={price_datas} />
                        {/* <div className="price_table">
                           <ul className="warp_list_title">
                            <li>Adult</li>
                            <li>Child twin</li>
                            <li>Child with extra bed</li>
                            <li>Child with no bed</li>
                          </ul>
                          <div className="warp_list_table">
                            {
                                tour.prices.map((item, i) => {
                                return (
                                  <ul key={i} className="warp_list_content"
                                    style={{
                                      backgroundColor:
                                        item.type === 'normal' ? '#f89623c4' :
                                        item.type === 'early_bird' ? '#fda53ead' :
                                        item.type === 'specialoffer' ? '#ed8812d9' :
                                        item.type === 'specialdeal' ? '#ed8812d9' :
                                        item.type === 'superpromo' ? '#ed8812d9' :
                                        item.type === 'promo' ? '#e07900e6' : ''
                                    }}>
                                    <li className="header_type" >
                                      <p>
                                        { 
                                          item.type === 'normal' ? 'Normal' :
                                          item.type === 'early_bird' ? 'Early Bird' :
                                          item.type === 'specialoffer' ? 'Special Offer' :
                                          item.type === 'specialdeal' ? 'Special Deal' :
                                          item.type === 'superpromo' ? 'Super Promo' :
                                          item.type === 'promo' ? 'Promo' : ''
                                        }
                                      </p>
                                      <p className="left_title">{item.seats} Left</p>
                                    </li>
                                    { item.adult.length !== '' ? (<Tooltip title={item.dta_adult}><li>RM {this.priceFormat(item.adult)}</li></Tooltip>) : ''}
                                    { item.child_twin.length !== '' ? (<Tooltip title={item.dta_child_twin}><li>RM {this.priceFormat(item.child_twin)}</li></Tooltip>) : ''}
                                    { item.child_with_bed.length !== '' ? (<Tooltip title={item.dta_child_with_bed}><li>RM {this.priceFormat(item.child_with_bed)}</li></Tooltip>) : ''}
                                    { item.child_no_bed.length !== '' ? (<Tooltip title={item.dta_child_no_bed}><li>RM {this.priceFormat(item.child_no_bed)}</li></Tooltip>) : ''}
                                  </ul>
                                )
                              })
                          }
                            <div className="clear"></div>
                          </div>
                        </div> */}

                        <Divider />
                        {/*<div className="form_booking">
                          <label>Number of seats</label>
                          <div style={{ width: '100%' }} className="selection">
                            <Select showSearch //defaultValue="01 Adult(s)"
                              allowClear={true}
                              placeholder="Select a seat"
                              optionFilterProp="children"
                              onChange={onChange} onFocus={onFocus} onBlur={onBlur} onSearch={onSearch}
                              filterOption={(input, option) =>
                                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                              }
                            >
                              {
                                tour.number_of_seats.map((seat) => {
                                  console.log("Check seat ---> ",seat)
                                  return(
                                    <Option value={seat}>{seat}</Option>
                                  )
                                })
                              }
                            </Select>
                          </div>
                        </div>*/}
                        <div className="form_booking">
                          <Form.Item label="No of Pax" >
                            {getFieldDecorator('quantity', {
                              initialValue: passengers && passengers.filter(item => item.visible === true).length,
                              rules: [{ required: true, message: '*Please select no of pax', }],
                            })(
                              <Select placeholder="Select" onChange={(value) => this.onChangeData(0, 'quantity', value)} onFocus={onFocus} onBlur={onBlur} >
                                {
                                  passengers && passengers.map((item, i) => {
                                    let quantity = i+1;
                                    return (
                                      <Option key={quantity} value={quantity}>{quantity} pax</Option>
                                    )
                                  })
                                }
                              </Select>
                            )}
                          </Form.Item>
                        </div>
                        <div className="form_booking">
                          <Form.Item label="Sales person" >
                            {getFieldDecorator('sub_sale_rep', {
                              rules: [{ required: true, pattern: new RegExp("^[A-Za-z]{1}[A-Za-z0-9\\s\\-\\.]{2,}$"), message: '*Please fill in the sales person', }],
                            })(
                              <Input /* className={validationErrors.sub_sale_rep ? 'error_border' : ''} */ onChange={(event) => this.onChangeData(0, 'sub_sale_rep', event.target.value)} placeholder="Sales person" />
                            )}
                          </Form.Item>
                        </div>
                        <div className="clear"></div>
                      </Card>
                  </TabPane>
                )
              })
            }
          </Tabs>
          {/**************************** Passenger ****************************/}
            <div className="header_title">
              <h2>Passenger Info</h2>
            </div>

            <Card className="passenger">
            {
                passengers.map((passenger, passenger_index) => {
                  return (
                  passenger.visible === true ?
                    <div className="passenger_warp">
                      <div className="form_passenger ">
                        <PageHeader
                          tags={<Tag color="#1890ff">Passenger {passenger_index + 1}</Tag>}
                        >
                        <Row type="flex">
                        <div className="form_control pandding_right">
                          <Form.Item label="Designation" >
                            {getFieldDecorator('designation' + passenger_index, {
                              initialValue: 'Mr',
                              rules: [{ required: true, message: '*Please select the designation', }],
                            })(
                              <Select value={this.state.passengers[passenger_index]['designation'] !== '' ? this.state.passengers[passenger_index]['designation'] : ''} /* className={validationErrors.designation ? 'error_border' : ''} */ placeholder="Select designation" optionFilterProp="children"
                                onChange={(value) => this.onChangeData(passenger_index, 'designation', value)}
                                filterOption={(input, option) =>
                                  option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                              >
                                {
                                  designations && designations.map((items, i) => {
                                    return (
                                      <Option key={i} value={items}>{items}</Option>
                                    )
                                  })
                                }

                              </Select>
                            )}
                          </Form.Item>
                        </div>

                        <div className="form_control pandding_left">
                          <Form.Item label="Name on passport" >
                            {getFieldDecorator('name' + passenger_index, {
                              rules: [{ required: true, message: '*Please fill in the name', }],
                            })(
                              <Input value={this.state.passengers[passenger_index]['name'] !== '' ? this.state.passengers[passenger_index]['name'] : ''} /* className={validationErrors.name ? 'error_border' : ''} */ onChange={(event) => this.onChangeData(passenger_index, 'name', event.target.value)} placeholder=" Passport name" />
                            )}
                          </Form.Item>
                        </div>

                        <div className="form_control pandding_right">
                          <Form.Item label="Date of birth" >
                            {getFieldDecorator('date_of_birth' + passenger_index, {
                              initialValue: this.state.passengers[passenger_index]['date_of_birth'] !== '' ? moment(this.state.passengers[passenger_index]['date_of_birth']) : '',
                              rules: [{ required: true, message: '*Please select the date of birth', }],
                            })(
                              <DatePicker defaultPickerValue={moment().add(-2,'year')} disabledDate={ current => { return current > moment().add(-2,'year').startOf('day') || current < moment().add(-100,'year').startOf('day') || current > moment(dataList && dataList.tours[this.props.tourKey].departure_date, 'DD/MM/YYYY').toDate() }} /* className={validationErrors.date_of_birth ? 'error_border' : ''} */ onChange={(value) => this.onChangeData(passenger_index, 'date_of_birth', moment(value).format('YYYY/MM/DD'))} placeholder="Select date of birth" />
                            )}
                          </Form.Item>
                        </div>

                        <div className="form_control pandding_left">
                          <Form.Item label="Mobile number" >
                            {getFieldDecorator('mobile' + passenger_index, {
                              initialValue: this.state.passengers[passenger_index]['mobile'] !== '' ? moment(this.state.passengers[passenger_index]['mobile']) : '',
                              rules: [{ required: true, message: '*Please fill in the mobile number', }],
                            })(
                              <Input value={this.state.passengers[passenger_index]['mobile'] !== '' ? this.state.passengers[passenger_index]['mobile'] : ''} onChange={(event) => this.onChangeData(passenger_index, 'mobile', event.target.value)} placeholder="Mobile number" />
                            )}
                          </Form.Item>
                        </div>

                        <div className="form_control pandding_right">
                          <Form.Item label="Passport number" >
                            {getFieldDecorator('passport_number' + passenger_index, {
                              rules: [{ required: true, message: '*Please fill in the passport number', }],
                            })(
                              <Input value={this.state.passengers[passenger_index]['passport_number'] !== '' ? this.state.passengers[passenger_index]['passport_number'] : ''} /* className={validationErrors.passport_number ? 'error_border' : ''} */ onChange={(event) => this.onChangeData(passenger_index, 'passport_number', event.target.value)} placeholder="Passport number" />
                            )}
                          </Form.Item>
                        </div>

                        <div className="form_control pandding_left">
                          <Form.Item label="Category" >
                            {getFieldDecorator('category' + passenger_index, {
                              initialValue: this.state.passengers[passenger_index]['category'],
                              rules: [{ required: true, message: '*Please select category', }],
                            })(
                              <Select placeholder="Select" optionFilterProp="children"
                                onChange={(value) => this.onChangeData(passenger_index, 'category', value)} onFocus={onFocus} onBlur={onBlur}
                                filterOption={(input, option) =>
                                  option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                }
                              >
                                {
                                  passenger && passenger.price_types.map((item, i) => {
                                    return (
                                      <Option disabled={!item.visible} key={i} value={item.name}>{item.name === "child_with_bed" ? ("Child with extra bed") : (item.name.charAt(0).toUpperCase() + item.name.substr(1).toLowerCase().replace(/_/g, " "))} (RM {this.priceFormat(item.price)})</Option>
                                    )
                                  })
                                }
                              </Select>
                            )}
                          </Form.Item>
                        </div>

                        <div className="form_control pandding_right">
                          <Form.Item label="Remark">
                            {getFieldDecorator('remark' + passenger_index)(
                              <TextArea
                                // value={passenger.remark}
                                value={this.state.passengers[passenger_index]['remark'] !== '' ? this.state.passengers[passenger_index]['remark'] : ''}
                                onChange={(event) => this.onChangeData(passenger_index, 'remark', event.target.value)}
                                placeholder="Remark VGML - vegetarian"
                                autosize={{ minRows: 1, maxRows: 3 }}
                              />
                            )}
                          </Form.Item>
                        </div>

                        {/*
                          showInfants === true ?
                            infant.map((items, i) => {
                              return (
                                <div className="infants">
                                  <p className="title">Infants
                                      <span className="plus" onClick={this.removeInfants(i)}>
                                      <FontAwesomeIcon icon="times" className="left_arrow remove_icon" />
                                    </span>
                                  </p>
                                  <div className="form_control_2 pandding_right">
                                    <label>Name on passport</label>
                                    <Input placeholder="Infants name" />
                                  </div>

                                  <div className="form_control_2 pandding_right">
                                    <label>Date of brith</label>
                                    <DatePicker onChange={(date, dateString) => this.dateOnChange(dateString)} />
                                  </div>
                                  <div className="form_control_2 ">
                                    <label>Gender</label>
                                    <Select showSearch placeholder="Select" optionFilterProp="children"
                                      onChange={onChange} onFocus={onFocus} onBlur={onBlur} onSearch={onSearch}
                                      filterOption={(input, option) =>
                                        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                      }
                                    >
                                      {
                                        ['Female', 'Male',].map((items, i) => {
                                          return (
                                            <Option key={i} value={items}>{items}</Option>
                                          )
                                        })
                                      }
                                    </Select>
                                  </div>
                                  <div className="clear"></div>
                                </div>
                              )
                            }) : null
                        }

                        {
                          infant.length < 2 ?
                          <div className="infants">
                            <p className="title">Infants
                              <span className="plus" onClick={this.addInfants}>
                                <FontAwesomeIcon icon="plus" className="left_arrow add_icon" />
                              </span>
                            </p>
                          </div> : ''
                        */}
                        </Row>
                        </PageHeader>
                      </div>
                    </div> : ''
                    )
                })
            }
          </Card>

            {/**************************** Payment Method ****************************/}
            <div className="header_title">
              <h2>Your Payment Method</h2>
            </div>

            <PaymentMethod handlePaymentChange={this.handlePaymentChange.bind(this)} credit_disabled={this.props.credit_disabled} fpx_disabled={false} paylater_disabled={dataList.tours && !dataList.tours[this.props.tourKey].pay_later}
              paymentTypeAvailable={4} payment_type={payment_type} payment_method={payment_method} credits={this.props.credits} method_visible={method_visible} validationErrors={validationErrors} banklist_select={banklist_select}
              offline_banklist={offline_banklist} handlePaymentMethodChange={(value) => this.handlePaymentMethodChange(value)}
              onChangePaymentCheckBox={(e) => this.onChangePaymentCheckBox(e)} terms={homepage_new.series_terms} /> 
            <div className="after_select">
              <Button htmlType="submit" disabled={this.state.check_out_disabled} /* onClick={this.onConfirmBooking} */ type="primary">CHECK OUT</Button>
            </div>
          </Form>

          <Modal
            title="Important Notice"
            style={{ top: 20 }}
            width={720}
            visible={bookingConfirmModal}
            closable={false}
            footer={[
              <Button key="no" onClick={() => this.onCloseModal('bookingConfirmModal')}>
                No
              </Button>,
              <Button key="yes" type="primary" loading={confirmLoading} /* disabled={confirmCheck['checkbox1'] == true && confirmCheck['checkbox2'] == true ? false : true} */ onClick={this.submitForm} disabled={this.state.confirm_button_disabled} >
                Yes
              </Button>,
            ]}
          >
            <h3><strong>Attention</strong></h3>
            {
              dataList.tours && dataList.tours[this.props.tourKey].pay_later && this.state.payment_type == 'pay_later' ?
              <React.Fragment>
                <p>Please note that once you have opted for the Book Now, Pay Later option, your booking will be secured up to 72 hours from the time of booking. </p>
                <p>Deposit needed before {moment().add(3, 'days').format('DD MMM YYYY')}: <strong>RM {this.priceFormat(this.props.deposit)}</strong></p>
                <p>Balance payment required before <strong>{moment(dataList && dataList.tours[this.props.tourKey].cut_off_date, 'DD/MM/YYYY').format('DD/MM/YYYY')}</strong></p>
                <p>Booking will be auto release if balance payment not received.</p>
                {/* <p>A deposit of <strong>RM {dataList && dataList.tours[this.props.tourKey].deposit}/Pax</strong> will be collected. A deposit does not constitute confirmation of the tour.</p>
                <p>All are subjected to a minimum group size (as determined by the company), in order for the confirmation to be affected for the departure to be finalized. </p> */}
              </React.Fragment> 
              :
              this.state.fare_type == 'deposit' ? 
               <React.Fragment>
                <p>Deposit payment{this.state.payment_method} of <strong>RM {this.priceFormat(this.props.deposit)}</strong> is require for this booking.</p>
                <p>Balance payment required by <strong>{moment(dataList && dataList.tours[this.props.tourKey].cut_off_date, 'DD/MM/YYYY').format('DD/MM/YYYY')}</strong></p>
                <p>Booking will be auto release if balance payment not received.</p>
                {/* <p>A deposit of <strong>RM {dataList && dataList.tours[this.props.tourKey].deposit}/Pax</strong> will be collected. A deposit does not constitute confirmation of the tour.</p>
                <p>All are subjected to a minimum group size (as determined by the company), in order for the confirmation to be affected for the departure to be finalized. </p> */}
              </React.Fragment> 
              :
               <React.Fragment>
                <p>Full payment is required for this booking.</p>
                <p>Balance payment required by <strong>{moment(dataList && dataList.tours[this.props.tourKey].cut_off_date, 'DD/MM/YYYY').format('DD/MM/YYYY')}</strong></p>
                <p>Booking will be auto release if balance payment not received.</p>
                {/*<p>A full payment of <strong>RM {this.props.total}</strong> will be collected. This is a guaranteed departure tour.</p>*/}
              </React.Fragment>
            }

              <p>Agreed and confirm to submit this booking transaction.</p>
          </Modal>

          <Modal
            title={<React.Fragment><FontAwesomeIcon fixedWidth icon="question-circle" />&nbsp; More</React.Fragment>}
            closable={false}
            visible={this.state.moreInfoModal}
            footer={[
              <Button type="primary" key="close" onClick={() => this.onCloseModal('moreInfoModal')}>
                Close
              </Button>,
            ]}
          >
          <p>
            <ReactMarkdown source={this.state.moreInfoModalcontent} escapeHtml={false} />
          </p>
          </Modal>

          <Modal
            visible={bookingErrorModal}
            title={null}
            footer={null}
            closable={false}
            destroyOnClose={() => this.onCloseModal('bookingErrorModal')}
          >
            <Empty
              image={warning}
              imageStyle={{
                height: 100,
              }}
              description={
                <p className="popup_text">{bookingErrorMessage}</p>
              }
              className="popup_footer"
            >
              <Button type="primary" onClick={() => this.onCloseModal('bookingErrorModal')}>Close</Button>
            </Empty>
        </Modal>

        </div>
      </div>
    );
  }
}

const SeriesShowPage = Form.create({ name: 'series' })(LeftPanel);
export default withRouter(SeriesShowPage);

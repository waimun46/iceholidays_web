import React, { Component } from 'react';
import { Collapse, Icon, Menu, Dropdown, Card, Button, Skeleton } from 'antd';
import './index.css'
import moment from 'moment';

const { Panel } = Collapse;

class RightPanel extends Component {

  priceFormat(value) {
    return value === undefined ? 0 : value.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') // this is for RM X,XXX.XX format
    // return value === undefined ? 0 : value.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') // this is for RM X,XXX format
  }

  render() {

    const sums = this.props.sums
    const dta = this.props.dta
    const visa_fee = this.props.visa_fee
    const subtract_others = this.props.subtract_others
    const insurance_rebate = this.props.insurance_rebate
    const subtraction = this.props.subtraction
    const pre_commisions_total = this.props.pre_commisions_total
    const compulsory_additional_fee = this.props.compulsory_additional_fee
    const addon_others = this.props.addon_others
    const addon = this.props.addon
    const deposit = this.props.deposit
    const single_supplement = this.props.single_supplement
    const pax_count = this.props.pax_count
    const total = this.props.total

    const prices = this.props.prices
    const dataList = this.props.dataList
    const loading = this.props.loading
    const tourKey = this.props.tourKey
    const payment_type = this.props.payment_type

    return (
      <div className="RightPanel res_panel_warp">
        <Card title={[
          <div className="title">
            <h2>Summary</h2>
          </div>]}>
          <div className="dates">
            <p className="date_title">Tour</p>
            <p className="date_select">{dataList.tours && dataList.tours[tourKey].caption}</p>
          </div>
          <div className="dates">
            <p className="date_title">Tour Code</p>
            <p className="date_select">{dataList.tours && dataList.tours[tourKey].code}</p>
          </div>
          <div className="dates">
            <p className="date_title">Departure Date</p>
            <p className="date_select">{dataList.tours && dataList.tours[tourKey].departure_date} - {dataList.tours && dataList.tours[tourKey].arrival_date}</p>
          </div>
         {/* <div className="type_description">
            <div className="rent">
              <p className="rent_title">Description</p>
              <p className="rent_select">{dataList.caption}</p>
            </div>

            <div className="clear"></div>
          </div>*/}
          <div className="select_list">
            <p className="note_label">
            <strong>Note:</strong> { loading ? '' : dataList.tours && moment(dataList.tours[tourKey].cut_off_date,'DD-MM-YYYY').isAfter() ? 'Only deposit required for this booking' : 'Full payment will be collected upon confirmation' }
            </p>
            
            <p className="date_title">Fare Breakdown</p>
              { 
                prices && prices.map((price) => { 
                  return(
                    price['quantity'] > 0 ?
                      <div className="list_warp">
                        {
                          price['quantity'] > 1 ?
                          <React.Fragment>
                            <div className="subtraction_collapse">
                                <Collapse bordered={false} expandIconPosition="left" expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 90 : 0} />} >
                                  <Panel header={
                                    <div className="list_warp fare_info">
                                      <span>
                                        <span>{price['type'].includes('normal') ? 'Normal' : price['type'].charAt(0).toUpperCase() + price['type'].substr(1).toLowerCase().replace(/_/g, " ")}</span>
                                        <div className="break-line"></div>
                                        <span>{price['name'] === "child_with_bed" ? "Child with extra bed" : price['name'].charAt(0).toUpperCase() + price['name'].substr(1).toLowerCase().replace(/_/g, " ")} x <strong>{price['quantity']}</strong></span>
                                      </span> 
                                      <span>RM {this.priceFormat(price['price'] * price['quantity'])}</span>
                                    </div>
                                  }>
                                    <div>
                                      <span>Price per pax</span>
                                      <span>RM {this.priceFormat(price['price'])}</span>
                                    </div>
                                  </Panel>
                              </Collapse>
                            </div>
                          </React.Fragment>
                          :
                          <React.Fragment>
                            <span>
                              <span>{price['type'].includes('normal') ? 'Normal' : price['type'].charAt(0).toUpperCase() + price['type'].substr(1).toLowerCase().replace(/_/g, " ")}</span>
                              <div className="break-line"></div>
                              <span>{price['name'] === "child_with_bed" ? "Child with extra bed" : price['name'].charAt(0).toUpperCase() + price['name'].substr(1).toLowerCase().replace(/_/g, " ")} x <strong>{price['quantity']}</strong></span>
                            </span>                        
                            <span>RM {this.priceFormat(price['price'] * price['quantity'])}</span>
                          </React.Fragment>
                        }
                        {/*<span>{price['name'] === "child_with_bed" ? "Child with extra bed" : price['name'].charAt(0).toUpperCase() + price['name'].substr(1).toLowerCase().replace(/_/g, " ")} ({price['type'].charAt(0).toUpperCase() + price['type'].substr(1).toLowerCase().replace(/_/g, " ")}) x <strong>{price['quantity']}</strong></span>                        
                        <span>RM {this.priceFormat(price['price'] * price['quantity'])}</span>*/}
                      </div>
                      : ''
                    )
                })
              }
            {
              pax_count === 1 && single_supplement > 0 ?
              <div className="list_warp">
                <span>Single Supplement</span>
                <span>RM {this.priceFormat(single_supplement)}</span>
              </div>
              : ''
            }
            {
              dataList.tours && dataList.tours[tourKey].insurance !== '' ?
              <div className="list_warp">
                <span>Travel Insurance</span>
                <span>{dataList.tours[tourKey].insurance.slice(0,4)}</span>
              </div> : ''
            }
            {
              addon > 0 ?
              <div className="list_warp">
                <div className="subtraction_collapse">
                  <Collapse bordered={false} expandIconPosition="left" expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 90 : 0} />} >
                    <Panel header={<div className="list_warp fare_info"><span>Add on</span><span>+ RM {this.priceFormat(addon)}</span></div>}>
                        {
                          compulsory_additional_fee > 0  ?
                          <div>
                            <span>Compulsory add. fee x <strong>{pax_count}</strong></span>
                            <span>+ RM {this.priceFormat(compulsory_additional_fee)}</span>
                          </div> : ''
                        }
                        {
                          addon_others > 0  ?
                          <div>
                            <span>Addon others x <strong>{pax_count}</strong></span>
                            <span>+ RM {this.priceFormat(addon_others)}</span>
                          </div> : ''
                        }
                    </Panel>
                  </Collapse>
                </div>
              </div> : ''
            }

            {
              subtraction > 0 ?
              <div className="list_warp">
                <div className="subtraction_collapse">
                  <Collapse bordered={false} expandIconPosition="left" expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 90 : 0} />} >
                    <Panel header={<div className="list_warp fare_info"><span>Subtraction</span><span>- RM {this.priceFormat(subtraction)}</span></div>}>
                        {
                          dta > 0  ?
                          <div>
                            <span>DTA x <strong>{pax_count}</strong></span>
                            <span>- RM {this.priceFormat(dta)}</span>
                          </div> : ''
                        }
                        {
                          visa_fee > 0  ?
                          <div>
                            <span>Visa fee x <strong>{pax_count}</strong></span>
                            <span>- RM {this.priceFormat(visa_fee)}</span>
                          </div> : ''
                        }
                        {
                          insurance_rebate > 0  ?
                          <div>
                            <span>Special rebate(s) x <strong>{pax_count}</strong></span>
                            <span>- RM {this.priceFormat(insurance_rebate)}</span>
                          </div> : ''
                        }
                        {
                          subtract_others > 0  ?
                          <div>
                            <span>Subtract others x <strong>{pax_count}</strong></span>
                            <span>- RM {this.priceFormat(subtract_others)}</span>
                          </div> : ''
                        }
                        {
                          pre_commisions_total > 0 ? 
                            dataList.tours && dataList.tours[tourKey].pre_commisions.map((pre_commision) => {
                              return(
                                <div>
                                  <span>{pre_commision.title}</span>
                                  <span>- RM {this.priceFormat(pre_commision.category == 'fixed' ? pre_commision.amount : pre_commision.category == 'percentage' ? ((sums - dta) * pre_commision.amount / 100) : 0)}</span>
                                </div>
                              )
                            }) : ''
                        }
                    </Panel>
                  </Collapse>
                </div>
              </div> : ''
            }
          </div>

          <div className="select_list">
            <div className="list_warp">
              <div className="total">
                <span>Grand Total</span><span className="total_summary">RM {this.priceFormat(total)}</span>
              </div>
            </div>
          </div>
          
          <div className="total">
            {
              payment_type == 'pay_later' ?
              <React.Fragment>
                <p className="pay_later">Please note that once you have opted for the Book Now, Pay Later option, your booking will be secured up to 72 hours from the time of booking.</p>
                <p className="deposit_summary">Deposit needed before {moment().add(3, 'days').format('DD MMM YYYY')}: <strong>RM {this.priceFormat(deposit)}</strong></p>
                <p className="balance_note_label">Final payment of <strong>RM {this.priceFormat(total - deposit)}</strong> is due before <strong>{moment(dataList.tours && dataList.tours[tourKey].cut_off_date, 'DD/MM/YYYY').format('DD MMM YYYY')}</strong></p>
              </React.Fragment>
              : dataList.tours && moment(dataList.tours[tourKey].cut_off_date,'DD-MM-YYYY').isAfter() ?
              <React.Fragment>
                <p className="deposit_summary">Deposit due now: <strong>RM {this.priceFormat(deposit)}</strong></p>
                <p className="balance_note_label">Final payment of <strong>RM {this.priceFormat(total - deposit)}</strong> is due before <strong>{moment(dataList.tours && dataList.tours[tourKey].cut_off_date, 'DD/MM/YYYY').format('DD MMM YYYY')}</strong></p>
              </React.Fragment>
              : ''
            }
          </div>
          {/* <Button type="primary" id="check_out">CHECK OUT</Button>
            <Button type="primary" id="guest_detail">Guest Detail</Button> */}
        </Card>
      </div>
    );
  }
}

export default RightPanel;

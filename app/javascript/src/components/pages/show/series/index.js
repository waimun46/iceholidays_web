import React, { Component } from 'react';
import { Drawer, Button, Spin } from 'antd';
import LeftPanel from './leftPanel';
import RightPanel from './rightPanel'
import './index.css';
import ShowMenu from '../menu';
import API from '../../../../api';
import { withRouter, Link } from 'react-router-dom';
import moment from 'moment';
import Footer from '../../home/footer/index';

class ShowSeries extends Component {
    constructor(props) {
      super(props);
      this.state = {
        visible: false,
        loading: true,
        credits: 0,
        credit_disabled: false,
        params: '',
        dataList: '',
        tourKey: 0,
        prices: [],
        dta: 0,
        visa_fee: 0,
        subtract_others: 0,
        insurance_rebate: 0,
        subtraction: 0,
        pre_commisions_total: 0,
        compulsory_additional_fee: 0,
        addon_others: 0,
        addon: 0,
        deposit: 0,
        single_supplement: 0,
        pax_count: 0,
        total: 0,
        payment_type: null,
        user: []
      };
    }

    componentDidMount() {
      this.userFetch();
      // this.banklistFetch();
      window.scrollTo(0, 0);
      //console.log(getUrl, '-----------getUrl');
      const params = this.props.location.search;
      let url = './api/v1' + this.props.location.pathname;
      var that = this;
      // console.log(params);

      API.get(url + params)
          .then(function (response) {
              // console.log(response,'------------res');
              that.setState({
                  dataList: response.data,
                  loading: false,
                  params: params
              })
          })
          .catch(function (error) {
              console.log(error);
          })
          .then(function () {
              // always executed
          });
    }

    getCredits = (value) => {
        this.setState({
          credits: value
        }, () => { this.handleUserCredits(); })
    }

    // banklistFetch() {
    //     let url = './api/v1/payments/payment_method';
    //     var that = this;

    //     API.get(url)
    //         .then(function (response) {
    //             // console.log(response, '------------res banklist');
    //             that.setState({
    //               credits: response.data.credits,
    //             }, () => { that.handleUserCredits(); })
    //         })
    //         .catch(function (error) {
    //             console.log(error);
    //         })
    //         .then(function () {
    //             // always executed
    //         });
    // }

    showDrawer = () => {
        this.setState({
            visible: true,
        });
    };

    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    handleCallback = (key) => {
        this.setState({
            tourKey: key
        });
    };

    handlePaymentType = (value) => {
      this.setState({
        payment_type: value
      });
    };

    handleQuantity = (value) => {
        let sums = 0;
        let dta = 0;
        let visa_fee = 0;
        let subtract_others = 0;
        let insurance_rebate = 0;
        let subtraction = 0;
        let compulsory_additional_fee = 0;
        let addon_others = 0;
        let addon = 0;
        let deposit = 0;
        let single_supplement = 0;
        let pre_commisions_total = 0;
        let pax_count = 0;
        let total = 0;        

        value.map((price) => {
          // sums
          sums += price.price * price.quantity
          // pax
          pax_count += price.quantity
          // subtraction
          visa_fee = this.state.dataList.tours && this.state.dataList.tours[this.state.tourKey].visa_fee * pax_count
          subtract_others = this.state.dataList.tours && this.state.dataList.tours[this.state.tourKey].subtract_others * pax_count
          subtraction = visa_fee + subtract_others
          if (this.state.user.collect_dta) {
            // dta
            dta += price.dta * price.quantity
            // insurance_rebate
            insurance_rebate = this.state.dataList.tours && this.state.dataList.tours[this.state.tourKey].insurance_rebate * pax_count
            // pre_commisions
            this.state.dataList.tours && this.state.dataList.tours[this.state.tourKey].pre_commisions.map((pre_commision) => {
              if (pre_commision.category == 'fixed') {
                pre_commisions_total += pre_commision.amount
              } else if (pre_commision.category == 'percentage') {
                pre_commisions_total += (sums - dta) * pre_commision.amount / 100
              }
            })
            subtraction += dta + insurance_rebate + pre_commisions_total
          }
          /*
          // Add on (Disabled for temporarily)
          // compulsory_additional_fee = this.state.dataList.tours && this.state.dataList.tours[this.state.tourKey].compulsory_additional_fee * pax_count
          // addon_others = this.state.dataList.tours && this.state.dataList.tours[this.state.tourKey].addon_others * pax_count
          // addon = compulsory_additional_fee + addon_others
          */
          // non-category fee
          single_supplement = pax_count > 1 ? 0 : this.state.dataList.tours && this.state.dataList.tours[this.state.tourKey].single_supplement_price
          deposit = this.state.dataList.tours && this.state.dataList.tours[this.state.tourKey].deposit * pax_count
          // total
          total = sums - subtraction + single_supplement + addon
        })

        this.setState({
          prices: value,
          sums: sums,
          dta: dta,
          visa_fee: visa_fee,
          subtract_others: subtract_others,
          insurance_rebate: insurance_rebate,
          subtraction: subtraction,
          pre_commisions_total: pre_commisions_total,
          compulsory_additional_fee: compulsory_additional_fee,
          addon_others: addon_others,
          addon: addon,
          deposit: deposit,
          single_supplement: single_supplement,
          pax_count: pax_count,
          total: total
        }, () => { this.handleUserCredits(); });
    };

    handleUserCredits() {
      if (this.state.credits >= (this.state.dataList.tours && this.state.dataList.tours[this.state.tourKey].guaranteed_departure === false ? this.state.deposit : this.state.total)) {
        this.setState({
            credit_disabled: false
        })
      }
      else {
        this.setState({
            credit_disabled: true
        })
      }
    };

    userFetch() {
      const that = this;

      API.get('./api/v1/users/details')
        .then(function (response) {
          console.log(response, '------------res');
          that.setState({
            user: response.data,
          })
        })
        .catch(function (error) {
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    }

    render() {
        const { credit_disabled, credits, loading, tourKey, dataList, sums, dta, visa_fee, subtract_others, insurance_rebate, subtraction, pre_commisions_total, compulsory_additional_fee, addon_others, addon, total, deposit, single_supplement, pax_count, prices, payment_type } = this.state

        return (
            <div>
                <ShowMenu /> 
                <div id="ShowSeries">
                    <div className="banner">
                        <img className="bannerImage" src={ dataList.images == '' ? homepage_new.default_show_header : dataList.images} />
                    </div>
                    <div className="logo_top">
                        <Link to="/">
                            <div className="logo_warp">
                                <img src={homepage_new.app_logo} alt="ice" />
                                <span>TRAVELB2B</span>
                            </div>
                        </Link>
                    </div>
                    <Spin size="large" spinning={loading}>
                      <div className="warppage">
                          <LeftPanel handleCallback={this.handleCallback} handleQuantity={this.handleQuantity} handlePaymentType={this.handlePaymentType} getCredits={this.getCredits} credit_disabled={credit_disabled} defaultImage={homepage_new.default_image} tourKey={tourKey} dataList={dataList} total={total} deposit={deposit} credits={credits} />
                          <RightPanel loading={loading} tourKey={tourKey} sums={sums} dta={dta} visa_fee={visa_fee} subtract_others={subtract_others} insurance_rebate={insurance_rebate} subtraction={subtraction} pre_commisions_total={pre_commisions_total} compulsory_additional_fee={compulsory_additional_fee} addon_others={addon_others} addon={addon} total={total} deposit={deposit} single_supplement={single_supplement} pax_count={pax_count} prices={prices} dataList={dataList} payment_type={payment_type} />
                          <div className="clear"></div>
                      </div>
                      <div className="res_panel">
                          <Button type="primary" onClick={this.showDrawer} className="res_panel_btn">
                              Summary
                           </Button>
                          <Drawer
                              title=""
                              placement="right"
                              closable={true}
                              onClose={this.onClose}
                              visible={this.state.visible}
                          >
                              <RightPanel loading={loading} tourKey={tourKey} dta={dta} visa_fee={visa_fee} subtract_others={subtract_others} insurance_rebate={insurance_rebate} subtraction={subtraction} compulsory_additional_fee={compulsory_additional_fee} addon_others={addon_others} addon={addon} total={total} deposit={deposit} single_supplement={single_supplement} pax_count={pax_count} prices={prices} dataList={dataList} />
                          </Drawer>
                      </div>
                    </Spin>
                </div>
              <Footer />
            </div>
        );
    }
}

export default withRouter(ShowSeries);

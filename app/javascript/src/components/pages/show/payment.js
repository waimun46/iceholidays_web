import React, { Component } from 'react';
import { Select, Checkbox, Button, } from 'antd';
import master from '../../../../images/payment/master.png';
import instant from '../../../../images/payment/instant.png'
import fpx from '../../../../images/payment/fpx.png';
import payLater from '../../../../images/payment/paylater.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const { Option } = Select;

class PaymentMethod extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paymentTypeAvailable: this.props.paymentTypeAvailable
    }
  }

  render() {
    const { paymentTypeAvailable } = this.state;
    return (
      <div className="payment">
        <div className={paymentTypeAvailable === 2 ? 'paymentTypeAvailable' : paymentTypeAvailable === 4 ? 'paymentTypeAvailable' : null }>
          <div className={paymentTypeAvailable === 2 ? "payment_50" : paymentTypeAvailable === 4 ? "payment_50" : "payment_type"} >
            <input type="radio" id="control_01" name="select" value="public_bank" checked={this.props.payment_type === "public_bank"} onChange={this.props.handlePaymentChange} />
            <label htmlFor="control_01">
              <img src={master} />
            </label>
          </div>

          {
            this.props.no_credit_payment == true ? 
            '' : (
              <div className={paymentTypeAvailable === 2 ? "payment_50" : paymentTypeAvailable === 4 ? "payment_50" : "payment_type"}>
                <input type="radio" id="control_02" name="select" value="credit_transaction" checked={this.props.payment_type === "credit_transaction"} disabled={this.props.credit_disabled}
                  onChange={this.props.handlePaymentChange} />
                <label htmlFor="control_02">
                  <img src={instant} />
                  <p className="credit_style">Credit Available: {this.props.credits}</p>
                </label>
              </div>
            )
          }
        </div>
        {
          paymentTypeAvailable === 2 ? null :
            <div className={paymentTypeAvailable === 2 ? "payment_50" : paymentTypeAvailable === 4 ? "payment_50" : "payment_type"}>
              <input type="radio" id="control_03" name="select" value="fpx_b2c" checked={this.props.payment_type === "fpx_b2c"} onChange={this.props.handlePaymentChange} disabled={this.props.fpx_disabled} />
              <label htmlFor="control_03">
                <img src={fpx} />
              </label>
            </div>
        }

        {
          paymentTypeAvailable === 4 ?
            <div className={paymentTypeAvailable === 2 ? "payment_50" : paymentTypeAvailable === 4 ? "payment_50" : "payment_type"}>
              <input type="radio" id="control_04" name="select" value="pay_later" checked={this.props.payment_type === "pay_later"} onChange={this.props.handlePaymentChange} disabled={this.props.paylater_disabled} />
              <label htmlFor="control_04">
                <img src={payLater} />
              </label>
            </div> : null
        }

        <div className="clear"></div>

        <div className="after_select" >
          {
            this.props.method_visible == true ?
              (
                <div className="form_control">
                  <label>Card Method</label>

                  <Select showSearch placeholder="Select your card method"
                    onChange={this.props.handlePaymentMethodChange}
                    // onFocus={onFocus}
                    // onBlur={onBlur}
                    // onSearch={onSearch}
                    defaultOpen={true}
                    className="payment_select"
                    value={this.props.payment_method}
                  >
                    {
                      this.props.banklist_select.map((bank) => {
                        if (this.props.offline_banklist.includes(bank[1]) == true) {
                          return (
                            <Option key={bank[1]} value={bank[1]} disabled>{bank[0]}</Option>
                          )
                        }
                        else {
                          return (
                            <Option key={bank[1]} value={bank[1]}>{bank[0]}</Option>
                          )
                        }

                      })
                    }
                  </Select>
                  <div className="error_payment_method">
                    <p className="error">{this.props.validationErrors.payment_method}</p>
                  </div>

                  <div className="clear"></div>

                </div>
              )
              : (null)
          }

          <Checkbox onChange={this.props.onChangePaymentCheckBox} >
            <small>
              By proceeding, I acknowledge that I have read and agreed to
              this {this.props.terms ? (<a href={this.props.terms} target="_blank">Terms of Use and Privacy Statement</a>) : ('Terms of Use and Privacy Statement.')}
            </small>
            <p className="error">{this.props.validationErrors.agree}</p>
          </Checkbox>

          <div className="clear"></div>
        </div>

      </div>

    )
  }
}

export default PaymentMethod;
import React, { Component } from "react";
import LeftPanel from "./leftPanel";
import RightPanel from "./rightPanel";
import "./index.css";
import ShowMenu from "../menu";
import API from "../../../../api";
import { withRouter, Link } from "react-router-dom";
import { Drawer, Button, Spin } from "antd";
import Footer from '../../home/footer/index';

class ShowFlight extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataList: [],
      loading: true,
      visible: false,
      brbCount: 0,
      visibleNodata: false,
      dataLenght: []
    };
    this.handleBrbCount = this.handleBrbCount.bind(this);

  }

  showDrawer = () => {
    this.setState({
      visible: true
    });
  };

  onClose = () => {
    this.setState({
      visible: false
    });
  };

  componentDidMount() {
    window.scrollTo(0, 0)
    //console.log(getUrl, '-----------getUrl');
    const params = this.props.location.search;
    let url = "./api/v1" + this.props.location.pathname;
    var that = this;
    //console.log(params);

    API.get(url + params)
      .then(function (response) {
        //console.log(response,'------------res');

        const count = Object.keys(response.data).length;
        console.log(count, '------------res-----count');

        that.setState({
          dataList: response.data,
          loading: false,
          visibleNodata: count === 0 ? true : false
        });
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      });

  }



  handleBrbCount(value) {
    this.setState({
      brbCount: value,
    });
  }

  render() {
    const { dataList, loading, visibleNodata } = this.state;
    console.log(dataList, '------------dataList-----dataList');

    return (
      <div>
        <ShowMenu />

        <div id="ShowFlight">
          <div className="banner"></div>
          <div className="logo_top">
            <Link to="/">
              <div className="logo_warp">
                <img src={homepage_new.app_logo} alt="ice" />
                <span>TRAVELB2B</span>
              </div>
            </Link>
          </div>
          <Spin size="large" spinning={loading}>
            <div className="warppage">
              <LeftPanel dataList={dataList} handleBrbCount={this.handleBrbCount} brbCount={this.state.brbCount} jsonLength={visibleNodata} />
              <RightPanel dataList={dataList} loading={loading} brbCount={this.state.brbCount} />
              <div className="clear"></div>
            </div>
            <div className="res_panel">
              <Button
                type="primary"
                onClick={this.showDrawer}
                className="res_panel_btn"
              >
                Summary
            </Button>
              <Drawer
                title=""
                placement="right"
                closable={true}
                onClose={this.onClose}
                visible={this.state.visible}
              >
                <RightPanel dataList={dataList} loading={loading} brbCount={this.brbCount} />
              </Drawer>
            </div>
          </Spin>
        </div>
        <Footer />
      </div>
    );
  }
}

export default withRouter(ShowFlight);

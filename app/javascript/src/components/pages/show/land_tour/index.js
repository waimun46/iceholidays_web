import React, { Component } from 'react';
import { Drawer, Button, Spin } from 'antd';
import LeftPanel from './leftPanel';
import RightPanel from './rightPanel'
import './index.css';
import ShowMenu from '../menu';
import API from '../../../../api';
import { withRouter, Link } from 'react-router-dom';
import moment from 'moment';
import img from "../../../../../images/roamin/01.png";
import ice from '../../../../../images/logo.png';
import defaultImage from '../../../../../images/default-image.png';
import headerImage from '../../../../../images/show_header.png';
import Footer from '../../home/footer/index';
import qs from 'qs';

const dateFormat = 'DD/MM/YYYY';

class ShowLandTour extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      loading: false,
      params: '',
      credits: 0,
      credit_disabled: false,
      dataList: '',
      user: '',
      prices: [],
      foc_prices: [],
      compulsory_charges: 0,
      dta: 0,
      dta_adult: 0,
      dta_child: 0,
      pax_count: 0,
      total: 0,
      deposit: 0,
      single_supplement: 0,
      departure_date: '',
      cut_off_date: '',
      price_category: '',
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    const params = this.props.location.search;
    let url = './api/v1' + this.props.location.pathname;
    var that = this;

    API.get(url + params)
      .then(function (response) {
        that.setState({
          dataList: response.data,
          loading: false,
          params: params
        })
      })
      .catch(function (error) {
          console.log(error);
      })
      .then(function () {
          // always executed
      });

    this.userFetch();
  }

  userFetch() {
    const that = this;

    API.get('./api/v1/users/details')
      .then(function (response) {
        // console.log(response, '------------res user');
        that.setState({
          user: response.data,
        })
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }

  handleTotalQty = (value) => {
    let sums = 0;
    let compulsory_charges = 0;
    let dta = 0;
    let dta_adult = 0;
    let dta_child = 0;
    let total = 0;
    let deposit = 0;
    let single_supplement = 0;
    let pax_count = 0;
    let dta_prices = this.state.dataList.dta_prices;
    let free_of_charge = this.state.dataList.free_of_charge;
    // let free_of_charge = this.state.dataList.free_of_charge !== 0 ? +(this.state.dataList.free_of_charge.match(/(\d+)/)[0]) : 0;
    let pax_charges = 0;
    let group_charges = 0;
    this.state.dataList.compulsory_charges.map((charge, i) => {
      if (charge.type === "Pax") {
        pax_charges += +charge.price;
      }
      else {
        group_charges += +charge.price;
      }
    });

    // FOC calculation
    let total_qty = 0
    var foc_contact = ''

    value.map((p, j) => { total_qty += p.quantity })
    if ((total_qty >= free_of_charge) && free_of_charge > 0) {
      var adult = value.find(x => x.name.includes('adult'))
      value.find(x => x.name.includes('adult')).quantity = adult.quantity - 1
      foc_contact = adult
    }

    value.map((price) => {
      // Pax
      pax_count += price.quantity

      // Charges per pax
      compulsory_charges += pax_charges * price.quantity;

      // DTA
      if (this.state.user.collect_dta) {
        if (price.name === "adult") {
          dta_adult += price.dta * price.quantity;
        }
        else {
          dta_child += price.dta * price.quantity;
        }
        dta = dta_adult + dta_child
      }
      else {
        dta = 0
      } 

      // Non-category fee
      single_supplement = pax_count > 1 ? 0 : this.state.dataList && this.state.dataList.single_supplement_price

      // Total
      total = (sums += price.price * price.quantity) + compulsory_charges + single_supplement
    })

    // Charges per group
    compulsory_charges += group_charges
    total = total - dta + group_charges

    // Deposit
    if (this.state.dataList.deposit > 0) {
      if (this.state.dataList.deposit_type === "Fixed") {
        deposit = this.state.dataList.deposit * pax_count
      }
      else {
        deposit = (this.state.dataList.deposit / 100) * total
      }
    }
    
    this.setState({
      prices: value,
      foc_prices: foc_contact,
      pax_count: pax_count,
      compulsory_charges: compulsory_charges,
      dta: dta,
      dta_adult: dta_adult,
      dta_child: dta_child,
      total: total,
      deposit: deposit,
      single_supplement: single_supplement,
    }, () => { this.handleUserCredits(); })
  }

  handleUserCredits() {
    if (this.state.credits >= this.state.total) {
      this.setState({
          credit_disabled: false
      })
    }
    else {
      this.setState({
          credit_disabled: true
      })
    }
  }

  handleDepartDate = (value) => {
    this.setState({
      departure_date: value,
    })
  }

  handleCutOffDate = (value) => {
    this.setState({
      cut_off_date: value,
    })
  }

  handlePriceCategory = (value) => {
    this.setState({
      price_category: value
    })
  }

  getCredits = (value) => {
    this.setState({
      credits: value
    }, () => { this.handleUserCredits(); })
  }

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  handleCallback = (key) => {
    this.setState({
      tourKey: key
    });
  };

  render() {
    const { loading, dataList, credits, credit_disabled, departure_date, compulsory_charges, total, pax_count, prices, price_category, dta, dta_child, dta_adult, cut_off_date, deposit, single_supplement, foc_prices } = this.state
    return (
      <div>
        <ShowMenu /> 
        <div id="ShowLandTour">
          <div className="banner">
            <img className="bannerImage" src={ dataList.images == '' ? headerImage : dataList.images} />
          </div>
          <div className="logo_top">
            <Link to="/">
              <div className="logo_warp">
                <img src={ice} alt="ice" />
                <span>TRAVELB2B</span>
              </div>
            </Link>
          </div>
          <Spin size="large" spinning={loading}>
            <div className="warppage">
              <LeftPanel handleTotalQty={this.handleTotalQty} handlePriceCategory={this.handlePriceCategory} getCredits={this.getCredits} handleDepartDate={this.handleDepartDate} handleCutOffDate={this.handleCutOffDate} credit_disabled={credit_disabled} defaultImage={defaultImage} dataList={dataList} departure_date={departure_date} credits={credits} />
              <RightPanel loading={loading} dataList={dataList} departure_date={departure_date} compulsory_charges={compulsory_charges} pax_count={pax_count} prices={prices} total={total} price_category={price_category} dta={dta} dta_child={dta_child} dta_adult={dta_adult} cut_off_date={cut_off_date} deposit={deposit} single_supplement={single_supplement} foc_prices={foc_prices} />
              <div className="clear"></div>
            </div>
            <div className="res_panel">
              <Button type="primary" onClick={this.showDrawer} className="res_panel_btn">
                Summary
              </Button>
              <Drawer
                title=""
                placement="right"
                closable={true}
                onClose={this.onClose}
                visible={this.state.visible}
              >
                <RightPanel loading={loading} dataList={dataList} departure_date={departure_date} compulsory_charges={compulsory_charges} pax_count={pax_count} prices={prices} total={total} price_category={price_category} dta={dta} dta_child={dta_child} dta_adult={dta_adult} cut_off_date={cut_off_date} deposit={deposit} single_supplement={single_supplement} foc_prices={foc_prices} />
              </Drawer>
            </div>
          </Spin>
        </div>
        <Footer />
      </div>
    );
  }
}

export default withRouter(ShowLandTour);

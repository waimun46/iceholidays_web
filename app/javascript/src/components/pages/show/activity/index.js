import React, { Component } from 'react';
import LeftPanel from './leftPanel';
import RightPanel from './rightPanel'
import './index.css';
import ShowMenu from '../menu';
import API from '../../../../api';
import { withRouter, Link,  } from 'react-router-dom';
import moment from 'moment';
import { Drawer, Button , Spin} from 'antd';
import Footer from '../../home/footer/index';

class ShowActivity extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            params: '',
            loading: true,
            credits: 0,
            credit_disabled: false,
            dataList: [],
            ticketDetails: [],
            visitDate: '',
            totalPrice: 0,
            user: []
        }
        this.handleTicketQuantity = this.handleTicketQuantity.bind(this);
        this.handleVisitDate = this.handleVisitDate.bind(this);
    }

    showDrawer = () => {
        this.setState({
            visible: true,
        });
    };

    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    componentDidMount() {
        this.userFetch();
        // this.banklistFetch();
        window.scrollTo(0, 0)

        // Change Moment.js "Invalid Date" message
        moment.updateLocale(moment.locale(), { invalidDate: '' });

        //console.log(getUrl, '-----------getUrl');
        const params = this.props.location.search;
        let url = './api/v1' + this.props.location.pathname;
        var that = this;
        // console.log(params);

        API.get(url + params)
            .then(function (response) {
                console.log(response,'------------res');

                that.setState({
                    dataList: response.data,
                    loading: false,
                    params: params
                })
            })
            .catch(function (error) {
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    }

    // banklistFetch() {
    //     let url = './api/v1/payments/payment_method';
    //     var that = this;

    //     API.get(url)
    //         .then(function (response) {
    //             // console.log(response, '------------res banklist');
    //             that.setState({
    //                 credits: response.data.credits,
    //             })
    //         })
    //         .catch(function (error) {
    //             console.log(error);
    //         })
    //         .then(function () {
    //             // always executed
    //         });
    // }

    getDataTickets = (value) => {
        this.setState({
            ticketDetails: value
        })
    };

    getCredits = (value) => {
        this.setState({
          credits: value
        }, () => { this.handleUserCredits(); })
    }

    handleTotalPrice(value) {
        this.setState({
            totalPrice: value
        }, () => { this.handleUserCredits(); })
    };
 
    handleUserCredits() {
        if (this.state.credits >= Number((this.state.totalPrice).toString().replace(/[^0-9\.-]+/g, ""))) {
            this.setState({
                credit_disabled: false
            })
        }
        else {
            this.setState({
                credit_disabled: true
            })
        }
    }

    handleTotalAmount() {
        // const ticketDetails = this.state.dataList.taggable_type == 'GT' ? this.state.ticketDetails : this.state.dataList.taggable_type == 'ACT' ? this.state.ticketDetails && this.state.ticketDetails.filter((t) => t.visible === true) : '';
        const ticketDetails = this.state.ticketDetails && this.state.ticketDetails.filter((t) => t.visible === true);
        let dta = 0;
        let total_price = 0;
        ticketDetails && ticketDetails.map((ticket, index) => {
          dta += ticket.dta * ticket.quantity;
          total_price += ticket.quantity * ticket.price;
        })
        return total_price - dta;
    }

    handleTicketQuantity(index, type, value, e = '') {
        let ticketDetails = this.state.ticketDetails
        ticketDetails[index][type] = value
        this.setState({
            ticketDetails: ticketDetails
        })
        this.handleTotalPrice(this.handleTotalAmount())
    }

    handleVisitDate(value) {
        this.setState({
            visitDate: moment(value).format("DD/MM/YYYY"),
        })
        // console.log(moment(value).format("DD/MM/YYYY"))
        this.handleTotalPrice(this.handleTotalAmount())
    }

    checkImages() {
        if (this.state.dataList.taggable_type === "GT") {
            return this.state.dataList.images
        } else {
            if (this.state.dataList.images && this.state.dataList.images.length > 0) {
                return this.state.dataList.images[0].url
            } else {
                return homepage_new.default_image
            }
        }
    }

    userFetch() {
      const that = this;

      API.get('./api/v1/users/details')
        .then(function (response) {
          console.log(response, '------------res');
          that.setState({
            user: response.data,
          })
        })
        .catch(function (error) {
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    }

    render() {
        const { credit_disabled, ticketDetails, totalPrice, credits, visitDate, params, dataList, loading } = this.state;
        // console.log("ticketDetails", ticketDetails)

        return (
            <div>
                <ShowMenu />
                <div id="ShowActivity">
                    <div className="banner">
                        <img src={this.checkImages()} className="bannerImage" />
                    </div>
                    <div className="logo_top">
                        <Link to="/">
                            <div className="logo_warp">
                                <img src={homepage_new.app_logo} alt="ice" />
                                <span>TRAVELB2B</span>
                            </div>
                        </Link>
                    </div>
                    <Spin size="large" spinning={loading}>
                        <div className="warppage">
                            <LeftPanel dataList={dataList} loading={loading} getDataTickets={this.getDataTickets} getCredits={this.getCredits} handleVisitDate={this.handleVisitDate} handleTicketQuantity={this.handleTicketQuantity} credit_disabled={credit_disabled} visitDate={visitDate} totalPrice={totalPrice} credits={credits} />
                            <RightPanel params={params} dataList={dataList} loading={loading} visitDate={visitDate} ticketDetails={ticketDetails} collectDta={this.state.user.collect_dta} />
                            <div className="clear"></div>
                        </div>
                        <div className="res_panel">
                            <Button type="primary" onClick={this.showDrawer} className="res_panel_btn">
                                Summary
                         </Button>
                            <Drawer
                                title=""
                                placement="right"
                                closable={true}
                                onClose={this.onClose}
                                visible={this.state.visible}
                            >
                                <RightPanel params={params} dataList={dataList} loading={loading} visitDate={visitDate} ticketDetails={ticketDetails} collectDta={this.state.user.collect_dta} />
                            </Drawer>
                        </div>
                    </Spin>

                </div>
                <Footer />
            </div>
        );
    }
}

export default withRouter(ShowActivity);

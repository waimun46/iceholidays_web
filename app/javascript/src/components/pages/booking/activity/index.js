import React, { Component } from 'react';
import { Table, Tag, Typography, Modal, Button, } from 'antd';
import MenuBooking from '../menu';
import API from '../../../../api'
import moment from 'moment';
import './index.css';

const { Paragraph } = Typography;

const expandedRowRender = (record) => {
  const columns1 = [
    { title: 'Package', dataIndex: 'package_name', key: 'package_name', className: 'hide', },
    { title: 'Quantity', dataIndex: 'total_quantity', key: 'total_quantity' },
    { title: 'Price', dataIndex: 'total_price', key: 'total_price' },
    { title: 'Category', dataIndex: 'category', key: 'category' },
    {
      title: 'Remark',
      dataIndex: 'remark',
      key: 'remark',
      width: 200,
      render: remark => (
        <Paragraph ellipsis={{ rows: 3, expandable: true }}>
          {remark}
        </Paragraph>
      ),
    },
  ]

  return <div>
    <Table columns={columns1} dataSource={record.activity_bookings} pagination={false} />
  </div>;

}

class ActivityBooking extends Component {
  state = {
    data: [],
    loading: false,
    visible: false,
    modalData: []
  };

  showModal(record) {
    console.log('record', record)
    this.setState({
      visible: true,
      modalData: this.state.data.filter((element) => {
        return element.order_no == record
      })
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };


  componentDidMount() {
    this.apiFetch();
  }

  apiFetch(params = '') {
    let url = './api/v1/activities/bookings';
    var that = this;
    console.log(params);

    API.get(url + params)
      .then(function (response) {
        console.log(response, '------------res');
        that.setState({
          data: response.data,
          isLoading: false
        })

      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      });

  }

  render() {
    const { data, modalData } = this.state;
    const activity = modalData[0];
    console.log('data-ac----', data)
    console.log('modalData-ac----', modalData)

    const columns = [
      {
        title: 'Date',
        width: 150,
        dataIndex: 'created_at',
        key: 'created_at',
      },
      {
        title: 'Order No.',
        width: 150,
        dataIndex: 'order_no',
        key: 'order_no',
      },
      {
        title: 'Name',
        width: 150,
        dataIndex: 'agent_name',
        key: 'agent_name',
      },
      {
        title: 'Package',
        dataIndex: 'package_name',
        key: 'package_name',
        width: 150,
      },
      // {
      //   title: 'Price',
      //   width: 150,
      //   dataIndex: 'total_price',
      //   key: 'price',
      // },  
      // {
      //   title: 'Quantity',
      //   width: 100,
      //   dataIndex: 'total_quantity',
      //   key: 'quantity',
      // },
      {
        title: 'Status',
        key: 'payment_status',
        dataIndex: 'payment_status',
        width: 100,
        render: (payment_status) => (
          <span>
            <Tag color={payment_status == 'Paid' ? 'green' : payment_status == 'Failed' ? 'volcano' : payment_status == 'Pending' ? 'geekblue' : ''} key={0}>
              {payment_status}
            </Tag>
          </span>
        ),
      },
      {
        title: 'Action',
        width: 100,
        key: 'action',
        dataIndex: 'order_no',
        render: (record) => (
          <Button type="link" onClick={() => this.showModal(record)} >View</Button>
        ),
      },
    ];


    return (
      <div id="ActivityContainer">
        <MenuBooking selected="activity" />
        <div className="content">
          <p className="title">Activity Bookings</p>
          <Table
            columns={columns}
            dataSource={this.state.data}
            pagination={false}
            // expandedRowRender={expandedRowRender}
            scroll={{ x: 'max-content' }}
            className="table_warp" />
        </div>
        <Modal
          // title="Order Information"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          className="bookingModal_sty"
          okText="DOWNLOAD INVOICE"
          cancelText="RESEND CONFIRMATION"
          footer={null}
        >
          {
            modalData.length === 0 ? null :
              <div className="table_order">
                <h2>Order Information</h2>
                <div className="responsive_sroll">
                  <table className="booking_modal_table">
                    <tr>
                      <th>Order No</th>
                      <td>{activity.order_no}</td>
                      <th>Package</th>
                      <td>{activity.package_name}</td>
                    </tr>
                    <tr>
                      <th>Country</th>
                      <td>{activity.country}</td>
                      <th>Quantity</th>
                      <td>{activity.total_quantity}</td>
                    </tr>
                    <tr>
                      <th>Visit Date</th>
                      <td>{activity.visit_date}</td>
                      <th>Price</th>
                      <td>{activity.total_price}</td>
                    </tr>
                    <tr>
                      <th>Remark</th>
                      <td>
                        <Paragraph ellipsis={{ rows: 3, expandable: true }}>
                          {activity.remark}
                        </Paragraph>
                      </td>
                      <th></th>
                      <td></td>
                    </tr>
                  </table>
                </div>
                <div className="btn_modal_booking">
                  <span>
                    <Button type="primary" href={activity.invoice_url}
                      disabled={activity.invoice_url === "" ? true : false}
                      target="_blank">DOWNLOAD INVOICE</Button>
                  </span>
                </div>
              </div>
          }
        </Modal>
      </div >
    );
  }
}

export default ActivityBooking;
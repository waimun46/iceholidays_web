import React, { Component } from 'react';
import { Menu, Dropdown, Icon, Affix } from 'antd';
import { withRouter, Link } from 'react-router-dom';
import API from '../../../../api'
import './index.css'

const { SubMenu } = Menu;

class MenuBooking extends Component {

  constructor(props) {
    super(props);
    this.state = {
      current: 'wifi',
      user: { username: '', credits: 0, access_flight: false, access_wifi: true, access_series: false, access_activity: false, access_hotel: false, access_land_tour: false },
      top: 0,
    }
  }

  componentDidMount() {
    const that = this;

    API.get('./api/v1/users/details')
      .then(function (response) {
        console.log(response, '------------res');
        that.setState({
          user: { username: response.data.username, credits: response.data.credits, access_flight: response.data.access_flight, access_wifi: response.data.access_wifi, access_series: response.data.access_series, access_activity: response.data.access_activity, access_hotel: response.data.access_hotel, access_land_tour: response.data.access_land_tour, access_credit_purchase: response.data.access_credit_purchase },
          currentTab: that.state.prevTab,
          isLoading: false,
        })
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }

  /****************************** handleClick *************************************/
  handleClick = e => {
    console.log('click ', e);
    this.setState({
      current: e.key,
    });
  };


  render() {
    /****************************** menu header dropdown *************************************/
    const menu = (
      <Menu>
        <Menu.Item disabled={true}>Credit: {this.state.user.credits}</Menu.Item>
        <Menu.Divider></Menu.Divider>
        <Menu.Item>My Bookings</Menu.Item>
        <Menu.Item><a href={homepage_new.pdpa} target="_blank">PDPA</a></Menu.Item>
        <Menu.Item><a href="./user/edit">Change Password</a></Menu.Item>
        <Menu.Item><a href="./users/sign_out" >Logout</a></Menu.Item>
      </Menu>
    );
    return (
      <Affix offsetTop={this.state.top}>
      <div className="MenuContainer">
 
        {/******************************************** menu header **************************************************/}
        <div className="menu_header">
          <div className="title">
            <Link to="/" className="a_link">
              <span>TRAVELB2B</span> 
            </Link>
          </div>
          <Dropdown overlay={menu} trigger={['click']} className="drop_menu">
            <a className="ant-dropdown-link" href="#">
              {this.state.user.username} <Icon type="down" />
            </a>
          </Dropdown>
        </div>

        {/******************************************** menu select **************************************************/}
        <Menu onClick={this.handleClick} selectedKeys={[this.props.selected]}
          mode="horizontal" className="menu_select">
          {
            this.state.user.access_series && <Menu.Item key="series"><Link to="/bookings/series">Series Bookings</Link></Menu.Item>
          }
          {
            this.state.user.access_activity && <Menu.Item key="activity"><Link to="/bookings/activities">Activity Bookings</Link></Menu.Item>
          }
          {
            this.state.user.access_wifi && <Menu.Item key="wifi"><Link to="/bookings/wifis">Wifi Bookings</Link></Menu.Item>
          }
          {
            this.state.user.access_flight && <Menu.Item key="flight"><Link to="/bookings/flights">Flight Bookings</Link></Menu.Item>
          }
          {
            this.state.user.access_hotel && <Menu.Item key="hotel"><Link to="/bookings/hotels">Hotel Bookings</Link></Menu.Item>
          }
          {
            this.state.user.access_land_tour && <Menu.Item key="land_tour"><Link to="/bookings/land_tours">Ground Tour Bookings</Link></Menu.Item>
          }
          <Menu.Item key="credit_usage"><Link to="/users/credit_usage">Credit Usage</Link></Menu.Item>
          {
            this.state.user.access_credit_purchase && <Menu.Item key="purchase_credit"><Link to="/users/purchase_credit">Purchase Credit</Link></Menu.Item>
          }
        </Menu>
      </div>
       </Affix>
    );
  }
}

export default MenuBooking;
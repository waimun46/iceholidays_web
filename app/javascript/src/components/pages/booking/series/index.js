import React, { Component } from 'react';
import {
  Table, Divider, Tag, Typography, Modal, Dropdown, Button, Menu, Form, Input, Radio, Select, DatePicker, Upload, Icon, Checkbox, message
} from 'antd';
import { Link, withRouter } from 'react-router-dom';
import MenuBooking from '../menu';
import API from '../../../../api'
import './index.css';
import moment from 'moment';
import jsonToFormData from 'json-form-data';

function onChange(value) {
  console.log(`selected ${value}`);
}

function onChangeDate(date, dateString) {
  console.log(date, dateString);
}
function beforeUpload(file) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
    return false;
  }
  return isJpgOrPng && isLt2M;
}

const { Paragraph } = Typography;
const { Option } = Select;

class TourBooking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: false,
      moadlVisible: false,
      confirmInsuranceModalVisible: false,
      viewOrderModalVisible: false,
      confirmDirty: false,
      autoCompleteResult: [],
      selectValueNational: 1,
      toggle: false,
      noteClick: true,
      selectedData: '',
      fileList: [],
      formDataState: null,
      isEditMode: false
    }
  }

  /************************************* componentDidMount ***************************************/
  componentDidMount() {
    this.apiFetch();
  }

  /************************************* apiFetch ***************************************/
  apiFetch = () => {
    let url = './api/v1/series/bookings';
    var that = this;
    API.get(url)
      .then(function (response) {
        console.log(response, '------------res');
        that.setState({
          data: response.data,
          isLoading: false
        })

      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  }

  /************************************* handleSubmit ***************************************/
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  /************************************* handleConfirmBlur ***************************************/
  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };
  handleBookingGroupDataChange = (type, value) => {
    this.state.selectedData[type] = value;
    console.log('this is selected data', this.state.selectedData);
    this.setState({
      selectedData: this.state.selectedData,
    })
  };
  handleBookingDataChange = (index, type, value) => {
    console.log(index, 'index')
    this.state.selectedData.bookings[index][type] = value
    console.log('this is selected data', this.state.selectedData)
    this.setState({
      selectedData: this.state.selectedData,
    })
  };
  handleBookingInsuranceDataChange = (index, nominee_index, type, value) => {
    console.log(index, nominee_index, 'index, nominee_index')
    this.state.selectedData.bookings[index]['insurance_nomination'][nominee_index][type] = value
    console.log('this is selected data', this.state.selectedData)
    this.setState({
      selectedData: this.state.selectedData,
    })
  };
  handleBookingUploadDataChange = (index, type, value) => {
    console.log(index, 'index');
    // console.log(value, 'handleBookingUploadDataChange -- value');
    this.state.selectedData.bookings[index][type] = value;
    console.log('this is selected data', this.state.selectedData)
    this.setState({
      selectedData: this.state.selectedData,
    })
  };

  /************************************* handleOk ***************************************/
  handleOk = (e, type) => {
    e.preventDefault();
    console.log("Submit modal --> ", type)

    // if (type == 'edit_modal') {
    //   const form = this.props.form;
    //   this.props.form.validateFieldsAndScroll((err, values) => {
    //     if (!err) {
    //     // console.log('Received values of form: ', values);
    //     let outputJson = {
    //       booking_group:{
    //         // insurance_receiver: form.getFieldValue('insurance_receiver'),
    //         insurance_receiver: this.state.selectedData.insurance_receiver,
    //         bookings_attributes: this.state.selectedData.bookings
    //       }
    //     };
    //     console.log("This is the outputJson", outputJson);
    //     var formData = jsonToFormData(outputJson);

    //     let url = './api/v1/series/' + this.state.selectedData.id + '/update_bookings';
    //     var that = this

    //     API.patch(url, formData)
    //       .then(function (response) {
    //         console.log(response, '------------res');
    //         that.setState({
    //           moadlVisible: false,
    //           isLoading: false,
    //           // data: response.data
    //         })
    //         if (response.data.message == 'success'){
    //           that.apiFetch();
    //           message.success('Updated');
    //         }else{
    //           message.error('Error while updating...');
    //         }
    //       })
    //       .catch(function (error) {
    //         console.log(error);
    //         message.error('Error while updating...');
    //       })
    //       .then(function () {
    //         // always executed
    //       });
    //     }
    //   });
    // } else 
    if (type == 'confirm_insurance_modal') {
      let url = './api/v1/series/' + this.state.selectedData.id + '/create_insurance_export';
      var that = this

      API.post(url)
        .then(function (response) {
          console.log(response, '------------res');
          that.setState({
            confirmInsuranceModalVisible: false,
            isLoading: false,
            // data: response.data
          })
          if (response.data.message == 'success'){
            message.success('Insurance Confirmed');
          }else{
            message.error('Error');
          }
        })
        .catch(function (error) {
          console.log(error);
          message.error('Error');
        })
        .then(function () {
          // always executed
        });
    }
  };

  /************************************* handleCancel ***************************************/
  handleCancel = (e, type) => {
    console.log("Close modal --> ", type);

    // this.setState({
    //   selectedData: ''
    // });
    // if (type == 'edit_modal') {
    //   this.setState({
    //     moadlVisible: false,
    //   });
    // } else 
    if (type == 'confirm_insurance_modal') {
      this.setState({
        confirmInsuranceModalVisible: false,
      });
    } else if (type == 'view_order_modal') {
      this.setState({
        viewOrderModalVisible: false,
        isEditMode: false,
      });
    }
  };

  /************************************* onChangeSelectNational ***************************************/
  onChangeSelectNational = e => {
    console.log('radio checked', e.target.value);
    this.setState({
      selectValueNational: e.target.value,
    });
  };

  /************************************* normFile ***************************************/
  // normFile = e => {
  //   console.log('Upload event:', e);
  //   if (Array.isArray(e)) {
  //     return e;
  //   }
  //   return e && e.fileList;
  // };

  normFile = info => {
    console.log('Upload event:', info.fileList);
    let fileList = [...info.fileList];
    fileList = fileList.slice(-1);
    return fileList;
  };

  /************************************* showModal ***************************************/
  showModal = (data, type) => {
    console.log('Opening modal --> ', type);
    this.setState({
      selectedData: data,
    });  
    // if (type == 'edit_modal') {
    //   this.setState({
    //     moadlVisible: true,
    //     viewOrderModalVisible: false,
    //   });
    // } else 
    if (type == 'confirm_insurance_modal') {
      this.setState({
        confirmInsuranceModalVisible: true,
      });
    } else if (type == 'view_order_modal') {
      this.setState({
        viewOrderModalVisible: true,
      });
    }
  };

  /************************************* isEditMode ***************************************/
  isEditMode = () => {
    this.setState(prevState => ({
      isEditMode: !prevState.isEditMode
    }))
  };

  updateBooking = e => {
    console.log("Clicked!")
    e.preventDefault();
    const form = this.props.form;
    this.props.form.validateFieldsAndScroll((err, values) => {
      console.log('check !err', !err)
      console.log('check error values', values)
      if (!err) {
      // console.log('Received values of form: ', values);
      let outputJson = {
        booking_group:{
          insurance_receiver: this.state.selectedData.insurance_receiver,
          bookings_attributes: this.state.selectedData.bookings
        }
      };
      console.log("This is the outputJson", outputJson);
      var formData = jsonToFormData(outputJson);

      let url = './api/v1/series/' + this.state.selectedData.id + '/update_bookings';
      var that = this

      API.patch(url, formData)
        .then(function (response) {
          console.log(response, '------------res');
          that.setState({
            isLoading: false,
          })
          if (response.data.message == 'success'){
            that.setState({
              isEditMode: false
            })
            that.apiFetch();
            message.success('Updated');
          }else{
            message.error('Error while updating...');
          }
        })
        .catch(function (error) {
          console.log(error);
          message.error('Error while updating...');
        })
        .then(function () {
          // always executed
        });
      }
    });
  };

  /************************************* onChangeClickBox ***************************************/
  onChangeClickBox(e) {
    let isChecked = e.target.checked === true
    console.log(`checked = ${isChecked}`);
    this.setState({
      noteClick: isChecked
    })
  }

  render() {
    console.log("check selected data booking ---> ", this.state.selectedData.bookings)
    const { selectValueNational, noteClick, fileList } = this.state;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { xl: { span: 24 }, xl: { span: 24 }, },
      wrapperCol: { xl: { span: 24 }, xl: { span: 24 }, },
    };

    const columns = [
    {
      title: 'Date',
      width: 150,
      dataIndex: 'created_at',
      key: 'created_at',
    },
    {
      title: 'Order No.',
      width: 150,
      dataIndex: 'order_no',
      key: 'order_no',
    },
    {
      title: 'Name',
      width: 150,
      dataIndex: 'agent_name',
      key: 'agent_name',
    },
    {
      title: 'Package',
      width: 150,
      dataIndex: 'code',
      key: 'code',
    },
    // {
    //   title: 'Price',
    //   width: 150,
    //   dataIndex: 'total_price',
    //   key: 'total_price',
    // },  
    // {
    //   title: 'Quantity',
    //   width: 150,
    //   dataIndex: 'total_quantity',
    //   key: 'total_quantity',
    // },
    // {
    //   title: 'Destination',
    //   width: 200,
    //   dataIndex: 'destination',
    //   key: 'destination',
    // },
    {
      title: 'Status',
      key: 'payment_status',
      dataIndex: 'payment_status',
      width: 100,
      render: (payment_status) => (
        <span>
          <Tag 
            color={
              payment_status == 'Paid' ? 'green' : 
              payment_status == 'Partial Paid' ? 'blue' : 
              payment_status == 'Awaiting' ? 'geekblue' : 
              payment_status == 'Pending' ? 'orange' :
              payment_status == 'Refunded' || payment_status == 'Cancelled' ? '' : 
              ''
            } 
            key={0}>
            {payment_status}
          </Tag>
        </span>
      ),
    },
    {
      title: 'Action',
      width: 100,
      key: 'action',
      fixed: 'right',
      // dataIndex: 'code',
      render: (record) => (
        <span>
          <Button type="link" onClick={() => { this.showModal(record, 'view_order_modal') }}>View</Button>
        </span>
       // <span className="actions">
       //    <ul>
       //    {
       //      record.editable ? 
       //      <li><a onClick={() => this.showModal(record, 'edit_modal')}>Edit</a></li>
       //      : ''
       //    }
       //    {
       //      record.payment_status == 'Pay Later' || record.payment_status.indexOf("Partial Paid") >= 0 ?
       //      <li><Link to={{pathname: "/bookings/series/"+record.id+"/payment"}}>Pay Now</Link></li> 
       //      : ''
       //    }
       //    {
       //      record.invoice_url !== '' ?
       //      <li><a href={record.invoice_url} target="_blank">Invoice</a></li>
       //      : ''
       //    }
       //    {
       //      record.insurance_confirmation ?
       //      <li><Link to={{pathname: ""}} onClick={() => this.showModal(record, 'confirm_insurance_modal')}>Confirm Insurance</Link></li>
       //      : ''
       //    }
       //    </ul>
       //  </span>
      )
    },
  ];

  const columns_order_information = [
    {
      title: 'Name',
      width: 150,
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Category',
      width: 150,
      dataIndex: 'category',
      key: 'category',
    },
    {
      title: 'Remark',
      width: 270,
      dataIndex: 'remark',
      key: 'remark',
      render: remark => (
        <Paragraph ellipsis={{ rows: 3, expandable: true }}>
          {remark}
        </Paragraph>
      ),
    }
  ];

  const expandedRowRender = (record) => {
    const columns_order_information1 = [
      { title: 'Mobile', dataIndex: 'mobile', key: 'mobile' },
      { title: 'Passport Number', dataIndex: 'passport_number', key: 'passport_number' },
      { title: 'Passport Expirty Date', dataIndex: 'passport_expiry_date', key: 'passport_expiry_date' },
      { title: 'Date of Birth', dataIndex: 'date_of_birth', key: 'date_of_birth' },
      { title: 'Price', dataIndex: 'total_price', key: 'total_price' },
    ];
    return <div>
      <Table scroll={{ x: 'max-content' }} columns={columns_order_information1} dataSource={[record]} pagination={false} />
    </div>;
  }

    const uploadButton = (
      <p className="ant-upload-drag-icon photo_p">
      <Icon type="inbox" />
      <small>Click or drag file to this area to upload</small>
      </p>
    );

    console.log("this.state.selectedData----",this.state.selectedData)

    return (
      <div id="TourContainer">
        <MenuBooking selected="series" />
        <div className="content">
          <p className="title">Series Bookings</p>
          <Table
            columns={columns}
            dataSource={this.state.data}
            pagination={false}
            // expandedRowRender={expandedRowRender}
            // scroll={{ x: 'max-content' }}
            className="table_warp" />
        </div>

      {/************************************* Modal View  ***************************************/}
        <Modal
          visible={this.state.viewOrderModalVisible}
          onOk={(e) => this.handleOk(e, 'view_order_modal')}
          onCancel={(e) => this.handleCancel(e, 'view_order_modal')}
          className="bookingModal_sty seriesModal"
          footer={
            <span>
            { 
              this.state.isEditMode ? 
                <Button type="primary" onClick={() => { this.isEditMode() }} >CANCEL</Button> : '' 
            }
            { 
              this.state.isEditMode ? 
                <Button type="primary" onClick={this.updateBooking} >SAVE</Button> : '' 
            }
            { 
              !this.state.isEditMode && this.state.selectedData.editable ? 
                <Button type="primary" onClick={() => { this.isEditMode() }} >EDIT</Button> : '' 
            }
            { 
              this.state.selectedData.booked_from == 'travelb2b' && !this.state.isEditMode && (this.state.selectedData.payment_status == 'Awaiting' || this.state.selectedData.payment_status == 'Partial Paid') ? 
                <Button type="primary"><Link to={{pathname: "/bookings/series/"+this.state.selectedData.id+"/payment"}}>PAY NOW</Link></Button> : '' 
            }
            { /*  
              !this.state.isEditMode && this.state.selectedData.invoice_url !== ''  ? 
                <Button type="primary"><a href={this.state.selectedData.invoice_url} target="_blank">DOWNLOAD INVOICE</a></Button> : '' 
            */ }
            {
              this.state.selectedData.booked_from == 'travelb2b' && !this.state.isEditMode && this.state.selectedData.invoices !== undefined && this.state.selectedData.invoices.length > 0 ? 
                this.state.selectedData.invoices.length > 1 ?
                  <Dropdown overlay={
                    <Menu>
                    {
                      this.state.selectedData.invoices.map((invoice, index) => {
                        return(
                          <Menu.Item key={index}>
                            <a href={invoice.url} target="_blank">Invoice {index + 1}</a>
                          </Menu.Item>
                        )
                      })
                    }
                    </Menu>
                  }>
                    <Button type="primary">
                      DOWNLOAD INVOICES <Icon type="down" />
                    </Button>
                  </Dropdown> : 
                  <Button type="primary">
                    <a href={this.state.selectedData.invoices[0].url} target="_blank">DOWNLOAD INVOICE</a>
                  </Button> 
              : ''
            }
            { 
              !this.state.isEditMode && this.state.selectedData.insurance_confirmation ? 
                <Button type="primary" onClick={() => this.showModal(this.state.selectedData, 'confirm_insurance_modal')}>CONFIRM INSURANCE</Button> : '' 
            }
            </span>
          }
        >
          <div className="table_order">
            <h2>Order Information</h2>
            <div className="responsive_sroll">
              <div className="booking_modal_table_warp">
                <h3>Order Details</h3>
                <table className="booking_modal_table">
                  <tr>
                    <th>Order No.</th>
                    <td>{this.state.selectedData.order_no}</td>
                    <th>Name</th>
                    <td>{this.state.selectedData.agent_name}</td>
                  </tr>
                  <tr>
                    <th>Package</th>
                    <td>{this.state.selectedData.code}</td>
                    <th>Destination</th>
                    <td>{this.state.selectedData.destination}</td>
                  </tr>
                  <tr>  
                    <th>Status</th>
                    <td>
                      <Tag 
                        color={
                          this.state.selectedData.payment_status == 'Paid' ? 'green' : 
                          this.state.selectedData.payment_status == 'Partial Paid' ? 'blue' : 
                          this.state.selectedData.payment_status == 'Awaiting' ? 'geekblue' : 
                          this.state.selectedData.payment_status == 'Pending' ? 'orange' :
                          this.state.selectedData.payment_status == 'Refunded' || this.state.selectedData.payment_status == 'Cancelled' ? '' : 
                          ''
                        } 
                        key={0}>
                        {this.state.selectedData.payment_status}
                      </Tag>
                    </td>
                    <th>Quantity</th>
                    <td>{this.state.selectedData.total_quantity}</td>
                  </tr>
                  <tr> 
                    <th>Price</th>
                    <td>{this.state.selectedData.total_price}</td>
                    <th>Paid Amount</th>
                    <td>{this.state.selectedData.paid_amount}</td>
                  </tr>
                </table>
              </div>
              <div className="booking_modal_table_warp">
                {/* <h3>Guest(s) Details</h3>
                <Table
                  columns={columns_order_information}
                  dataSource={this.state.selectedData.bookings}
                  pagination={false}
                  expandedRowRender={expandedRowRender}
                  // scroll={{ x: 'max-content' }}
                  className="table_warp" 
                /> */}
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                  {/************************************* Receiver's email ***************************************/}
                  { 
                    this.state.selectedData ? (
                      <React.Fragment>
                        <div className="formItemWarp_top">
                          <Form.Item label="Receiver's email address">
                            {getFieldDecorator(`insurance_receiver_${this.state.selectedData.id}`, {
                              rules: [
                                {
                                  type: 'email',
                                  message: 'The input is not valid E-mail!',
                                },
                                {
                                  required: true,
                                  message: 'Please input your E-mail!',
                                },
                              ],
                              initialValue: this.state.selectedData.insurance_receiver,
                            })(<Input placeholder="Insurance Receiver" onChange={(e) => this.handleBookingGroupDataChange('insurance_receiver', e.target.value)} disabled={!this.state.isEditMode} />)}
                          </Form.Item>
                        </div>
                        <div className="clear"></div>
                        { 
                          this.state.selectedData.bookings != undefined ? (
                            this.state.selectedData.bookings.map((booking, index) => {
                              {/************************************* Guest ***************************************/}
                              return(
                                <div className="guest">
                        <p className="title">Guest {index+1}, ref: {booking.booking_id}</p>
                        <p className="line"></p>
                        <div className="input_warp">
                          {/*************************** Price ***************************/}
                          <div className="formItemWarp50">
                            <Form.Item label="Price">
                              <Input disabled placeholder="Price" value={booking.total_price + index}/>
                            </Form.Item>
                          </div>

                          {/*************************** Price ***************************/}
                          <div className="formItemWarp50">
                            <Form.Item label="Price type">
                              {getFieldDecorator('price_type' + index, {
                                initialValue: booking.price_type,
                              })(
                                <Select
                                  showSearch
                                  placeholder="Select Price type"
                                  onChange={onChange}
                                  disabled
                                >
                                  {
                                    ['normal', 'early_bird', 'specialoffer', 'specialdeal', 'superpromo', 'promo'].map((Price_type) => {
                                      return (
                                        <Option key={Price_type}>{Price_type}</Option>
                                      )
                                    })
                                  }
                                </Select>
                              )}
                            </Form.Item>
                          </div>
                        </div>

                        <div className="input_warp">
                          {/*************************** Category ***************************/}
                          <div className="formItemWarp50">
                            <Form.Item label="Category">
                              {getFieldDecorator('Category' + index, {
                                initialValue: booking.category,
                              })(
                                <Select
                                  showSearch
                                  placeholder="Select Category"
                                  onChange={onChange}
                                  disabled
                                >
                                  {
                                    ['Adult', 'Child twin', 'Child with bed', 'Child no bed'].map((Category) => {
                                      return (
                                        <Option key={Category} value={Category}>{Category}</Option>
                                      )
                                    })
                                  }
                                </Select>
                              )}
                            </Form.Item>
                          </div>

                          {/*************************** Designation ***************************/}
                          <div className="formItemWarp50">
                            <Form.Item label="Designation">
                              {getFieldDecorator('designation' + index, {
                                initialValue: booking.designation,
                              })(
                                <Select showSearch placeholder="Select" disabled>
                                  {
                                    ['Mr', 'Mrs', 'Ms', 'Mstr', 'Mdm'].map((designation) => {
                                      return (
                                        <Option key={designation}>{designation}</Option>
                                      )
                                    })
                                  }
                                </Select>
                              )}
                            </Form.Item>
                          </div>
                        </div>

                        <div className="input_warp">
                          {/*************************** Full name ***************************/}
                          <div className="formItemWarp50">
                            <Form.Item label="Full name">
                              {getFieldDecorator('name' + index, {
                                initialValue: booking.name,
                              })(
                                <Input placeholder="Full name" disabled />
                              )}
                            </Form.Item>
                          </div>

                          {/*************************** Date of birth ***************************/}
                          <div className="formItemWarp50 date_sty">
                            <Form.Item label="Date of birth">
                              {getFieldDecorator('Date_birth' + index, {
                                initialValue: booking.date_of_birth !== '' ? moment(booking.date_of_birth) : '',
                              })(
                                <DatePicker onChange={onChangeDate} disabled />
                              )}
                            </Form.Item>
                          </div>
                        </div>

                        <div className="input_warp">
                          {/*************************** Mobile ***************************/}
                          <div className="formItemWarp50">
                            <Form.Item label="Mobile">
                              {getFieldDecorator('Mobile' + index, {
                                initialValue: booking.mobile,
                              })(
                                <Input placeholder="Mobile" disabled />
                              )}
                            </Form.Item>
                          </div>

                          {/*************************** Passport number ***************************/}
                          <div className="formItemWarp50">
                            <Form.Item label="Passport number">
                              {getFieldDecorator('passport_number' + index, {
                                initialValue: booking.passport_number,
                              })(
                                <Input placeholder="Passport number" disabled />
                              )}
                            </Form.Item>
                          </div>
                        </div>

                        <div className="input_warp">
                          {/*************************** Passport expiry date ***************************/}
                          <div className="formItemWarp50 date_sty">
                            <Form.Item label="Passport expiry date">
                              {getFieldDecorator('passport_expiry_date' + index, {
                                initialValue: booking.passport_expiry_date !== '' ? moment(booking.passport_expiry_date) : '',
                                rules: [
                                  {
                                    required: true,
                                    message: 'Please select your expiry date',
                                  },
                                ],
                              })(
                                <DatePicker onChange={(date, dateString) => this.handleBookingDataChange(index, 'passport_expiry_date', dateString)} disabled={!this.state.isEditMode} />
                              )}
                            </Form.Item>
                          </div>

                          {/*************************** Passport photocopy ***************************/}
                          <div className="formItemWarp50 photo_upload">
                            <Form.Item label="Passport photocopy">
                              {getFieldDecorator('passport_photocopy' + index, {
                                valuePropName: 'fileList',
                                getValueFromEvent: this.normFile,
                              })(
                                <Upload.Dragger 
                                // action="/upload.do"
                                beforeUpload={beforeUpload}
                                onChange={(value) => this.handleBookingUploadDataChange(index, 'passport_photocopy', value.fileList[0].originFileObj)}
                                disabled={!this.state.isEditMode}
                                >
                                {booking.passport_photocopy_image !== '' ? <img className="image-display-photocopy" src={booking.passport_photocopy_image} alt="avatar" /> : uploadButton}
                                </Upload.Dragger>,
                              )}
                            </Form.Item>
                          </div>
                        </div>

                        <div className="input_warp">
                          {/*************************** Room type ***************************/}
                          <div className="formItemWarp50">
                            <Form.Item label="Room type">
                              {getFieldDecorator('room_type' + index, {
                                initialValue: booking.room_type,
                                rules: [{ required: true, message: '*Please select the Room', }],
                              })(
                                <Select showSearch placeholder="Select Room type" onChange={(value) => this.handleBookingDataChange(index, 'room_type', value)} disabled={!this.state.isEditMode}>
                                  {
                                    ['Single', 'Double Bed', 'Triple Sharing', 'Twin Sharing', 'Child No Bed', 'Child With Bed', 'Child Twin Share'].map((Room_type) => {
                                      return (
                                        <Option key={Room_type} value={Room_type}>{Room_type}</Option>
                                      )
                                    })
                                  }
                                </Select>
                              )}
                            </Form.Item>
                          </div>

                          {/*************************** Agent remark ***************************/}
                          <div className="formItemWarp50">
                            <Form.Item label="Agent remark">
                              {getFieldDecorator('remark' + index, {
                                initialValue: booking.remark,
                              })(
                                <Input placeholder="Agent remark" onChange={(e) => this.handleBookingDataChange(index, 'remark', e.target.value)} disabled={!this.state.isEditMode} />
                              )}
                            </Form.Item>
                          </div>
                        </div>

                        <div className="insuran_detail">
                          {/************************************* Free Chubb Travel Insurance ***************************************/}
                          <div className="free_chubb">
                            <h3 className="title2">Free Chubb Travel Insurance</h3>
                            <span>
                              Our packages bundle up with Chubb Executive travel insurance that is underwritten by
                              Chubb Insurance Malaysia Berhad. Please choose your option below.
                              Click here to view the benefits Click here to view the policy wording
                            </span>
                            {
                              getFieldDecorator('insurance_confirmation' + index, {
                                initialValue: booking.insurance_confirmation,
                              })(
                              <Radio.Group onChange={(e) => this.handleBookingDataChange(index, 'insurance_confirmation', e.target.value)} disabled={!this.state.isEditMode}>
                                <Radio value={true}>Yes, please include with no additional charge</Radio>
                                <Radio value={false}>No, I don't want free insurance and understand the cost remain the same</Radio>
                              </Radio.Group>
                              )
                            }
                            {
                            booking.insurance_confirmation == true ?
                            <small className="notice_insuran">
                              "Certificate will only be issued to the customer after clicking the Confirm Insurance"
                            </small> : null
                            }
                          </div>

                          {/***************************************** Nationality *******************************************/}
                          <div className="free_chubb national">
                            <h3 className="title2">Nationality</h3>
                            {getFieldDecorator('nationality' + index, {
                              initialValue: booking.nationality,
                            })(
                            <Radio.Group onChange={(e) => this.handleBookingDataChange(index, 'nationality', e.target.value)} disabled={!this.state.isEditMode}>
                              <Radio value="malaysian" className="label_1">
                                I am a Malaysian, Malaysian Permanent Resident or holder of a valid working permit in Malaysia,
                                dependent pass, long term social visit pass or student pass, and will commence journey from Malaysia.
                              </Radio>
                              <Radio value="non-malaysian">No. Sorry, you are not eligible for this cover.</Radio>
                            </Radio.Group>
                            )}
                            {/*************************** Guardian ***************************/}
                            {/* <div className="formItemWarp50 ">
                              <Form.Item label="Guardian">
                                {getFieldDecorator('Guardian', {
                                  rules: [
                                    {
                                      required: true,
                                      message: 'Please select your Guardian!',
                                    },
                                  ],
                                })(
                                  <Select showSearch placeholder="Select" >
                                    {
                                      ['Mr', 'Mrs', 'Ms', 'Mstr', 'Mdm'].map((Guardian) => {
                                        return (
                                          <Option key={Guardian} value={Guardian}>{Guardian}</Option>
                                        )
                                      })
                                    }
                                  </Select>
                                )}
                              </Form.Item>
                            </div> */}
                          </div>
                          
                          <div className="note">
                            {getFieldDecorator('insurance_nomination_flag' + index, {
                            })(
                              <Checkbox onChange={(e) => this.handleBookingDataChange(index, 'insurance_nomination_flag', e.target.checked)} defaultChecked={booking.insurance_nomination_flag !== '' || 0 ? booking.insurance_nomination_flag : true} disabled={!this.state.isEditMode}>
                                Note: Death benefit will be paid to insured Person's estate. Please uncheck if the insured
                                person wishes to make a NOMINEE/NOMINEES.
                              </Checkbox>
                            )}
                          </div>

                          {/***************************************** Guardian *******************************************/}
                          {/* <div className="guardian">
                            <h4 className="title3">Guardian</h4>
                            <div className="input_warp"> */}
                          {/*************************** Designation ***************************/}
                          {/* <div className="formItemWarp50 ">
                                <Form.Item label="Designation">
                                  {getFieldDecorator('Designation_Guardian', {
                                    rules: [
                                      {
                                        required: true,
                                        message: 'Please select your Designation!',
                                      },
                                    ],
                                  })(
                                    <Select showSearch placeholder="Select" >
                                      {
                                        ['Mr', 'Mrs', 'Ms', 'Mstr', 'Mdm'].map((Guardian) => {
                                          return (
                                            <Option key={Guardian} value={Guardian}>{Guardian}</Option>
                                          )
                                        })
                                      }
                                    </Select>
                                  )}
                                </Form.Item>
                              </div> */}

                          {/*************************** Full Name ***************************/}
                          {/* <div className="formItemWarp50 ">
                                <Form.Item label="Full Name">
                                  {getFieldDecorator('Full_Name', {
                                    rules: [
                                      {
                                        required: true,
                                        message: 'Please input your Full Name!',
                                      },
                                    ],
                                  })(
                                    <Input placeholder="Full Name" />
                                  )}
                                </Form.Item>
                              </div>
                            </div> */}

                          {/* <div className="input_warp"> */}
                          {/*************************** Date of Birth ***************************/}
                          {/* <div className="formItemWarp50 date_sty">
                                <Form.Item label="Date of Birth">
                                  {getFieldDecorator('Date_Birth', {
                                    rules: [
                                      {
                                        required: true,
                                        message: 'Please select your Date of Birth!',
                                      },
                                    ],
                                  })(
                                    <DatePicker onChange={onChangeDate} />
                                  )}
                                </Form.Item>
                              </div> */}

                          {/*************************** Passport Number ***************************/}
                          {/* <div className="formItemWarp50 ">
                                <Form.Item label="Passport Number">
                                  {getFieldDecorator('Passport_Number', {
                                    rules: [
                                      {
                                        required: true,
                                        message: 'Please input your Passport Number!',
                                      },
                                    ],
                                  })(
                                    <Input placeholder="Passport Number" />
                                  )}
                                </Form.Item>
                              </div>
                            </div>
                          </div> */}


                          {/***************************************** Nominee Details  *******************************************/}
                          {
                            booking.insurance_nomination_flag == false ?
                              
                              /******************************************* nominee 1 **********************************************/
                              <div>
                                { 
                                  booking.insurance_nomination.map((insurance, nominee_index) => {
                                    return (
                                      <div className="nominee">
                                    <h4 className="title3">Nominee Details {nominee_index + 1}</h4>
                                    <div className="input_warp">
                                      {/*************************** Surname ***************************/}
                                      <div className="formItemWarp33 ">
                                        <Form.Item label="Surname">
                                          {getFieldDecorator('nominee_surname' + nominee_index, {
                                            // rules: [
                                            //   {
                                            //     required: true,
                                            //     message: 'Please input your Surname!',
                                            //   },
                                            // ],  
                                            initialValue: insurance.nominee_surname,
                                          })(
                                            <Input placeholder="Surname" onChange={(e) => this.handleBookingInsuranceDataChange(index, nominee_index, 'nominee_surname', e.target.value)} disabled={!this.state.isEditMode} />
                                          )}
                                        </Form.Item>
                                      </div>

                                      {/*************************** Given Name ***************************/}
                                      <div className="formItemWarp33 ">
                                        <Form.Item label="Given Name">
                                          {getFieldDecorator('nominee_given_name' + nominee_index, {
                                            // rules: [
                                            //   {
                                            //     required: true,
                                            //     message: 'Please input your Given Name!',
                                            //   },
                                            // ],
                                            initialValue: insurance.nominee_given_name,
                                          })(
                                            <Input placeholder="Given Name" onChange={(e) => this.handleBookingInsuranceDataChange(index, nominee_index, 'nominee_given_name', e.target.value)} disabled={!this.state.isEditMode} />
                                          )}
                                        </Form.Item>
                                      </div>

                                      {/*************************** Passport/NRIC Number ***************************/}
                                      <div className="formItemWarp33 ">
                                        <Form.Item label="Passport/NRIC Number">
                                          {getFieldDecorator('nominee_passport_number' + nominee_index, {
                                            // rules: [
                                            //   {
                                            //     required: true,
                                            //     message: 'Please input your Passport/NRIC Number!',
                                            //   },
                                            // ],
                                            initialValue: insurance.nominee_passport_number,
                                          })(
                                            <Input placeholder="Passport/NRIC Number" onChange={(e) => this.handleBookingInsuranceDataChange(index, nominee_index, 'nominee_passport_number', e.target.value)} disabled={!this.state.isEditMode} />
                                          )}
                                        </Form.Item>
                                      </div>
                                    </div>
                                    <div className="input_warp">
                                      {/*************************** Date of Birth ***************************/}
                                      <div className="formItemWarp33 date_sty">
                                        <Form.Item label="Date of Birth">
                                          {getFieldDecorator('nominee_date_of_birth' + nominee_index, {
                                            // rules: [
                                            //   {
                                            //     required: true,
                                            //     message: 'Please select your Date of Birth!',
                                            //   },
                                            // ],
                                            initialValue: insurance.nominee_date_of_birth !== '' ? moment(insurance.nominee_date_of_birth) : '',
                                          })(
                                            <DatePicker onChange={(date, dateString) => this.handleBookingInsuranceDataChange(index, nominee_index, 'nominee_date_of_birth', dateString)} disabled={!this.state.isEditMode} />
                                          )}
                                        </Form.Item>
                                      </div>

                                      {/*************************** Relationship ***************************/}
                                      <div className="formItemWarp33 ">
                                        <Form.Item label="Relationship">
                                          {getFieldDecorator('nominee_relationship' + nominee_index, {
                                            // rules: [
                                            //   {
                                            //     required: true,
                                            //     message: 'Please select your Relationship!',
                                            //   },
                                            // ],
                                            initialValue: insurance.nominee_relationship,
                                          })(
                                            <Select showSearch placeholder="Select" onChange={(value) => this.handleBookingInsuranceDataChange(index, nominee_index, 'nominee_relationship', value)} disabled={!this.state.isEditMode}>
                                              {
                                                ['Spouse', 'Father', 'Mother', 'Brother', 'Daughter', 'Sister', 'Son', 'Grandmother', 'Grandfather',
                                                  'Grandson', 'Granddaughter', 'Friend', 'Others'].map((Relationship) => {
                                                    return (
                                                      <Option key={Relationship} value={Relationship}>{Relationship}</Option>
                                                    )
                                                  })
                                              }
                                            </Select>
                                          )}
                                        </Form.Item>
                                      </div>

                                      {/*************************** Share ***************************/}
                                      <div className="formItemWarp33 ">
                                        <Form.Item label="Share">
                                          {getFieldDecorator('nominee_share' + nominee_index, {
                                            // rules: [
                                            //   {
                                            //     required: true,
                                            //     message: 'Please select your Share!',
                                            //   },
                                            // ],
                                            initialValue: insurance.nominee_share,
                                          })(
                                            <Select showSearch placeholder="Select" onChange={(value) => this.handleBookingInsuranceDataChange(index, nominee_index, 'nominee_share', value)} disabled={!this.state.isEditMode}>
                                              {
                                                ['0', '25', '50', '75', '100'].map((Share) => {
                                                  return (
                                                    <Option key={Share} value={Share}>{Share}</Option>
                                                  )
                                                })
                                              }
                                            </Select>
                                          )}
                                        </Form.Item>
                                      </div>
                                    </div>
                                      </div>
                                    )
                                  })
                                }

                                {/***************************************** Witness Details *******************************************/}
                                <div className="Witness_Details">
                                  <h4 className="title3">Witness Details</h4>
                                  <div className="input_warp">
                                    {/*************************** Surname ***************************/}
                                    <div className="formItemWarp33 ">
                                      <Form.Item label="Surname">
                                        {getFieldDecorator('witness_surname' + index, {
                                          rules: [
                                            {
                                              required: true,
                                              message: 'Please input your Surname!',
                                            },
                                          ],
                                          initialValue: booking.witness_surname,
                                        })(
                                          <Input placeholder="Surname" onChange={(e) => this.handleBookingDataChange(index, 'witness_surname', e.target.value)} disabled={!this.state.isEditMode} />
                                        )}
                                      </Form.Item>
                                    </div>
                                    {/*************************** Given Name ***************************/}
                                    <div className="formItemWarp33 ">
                                      <Form.Item label="Given Name">
                                        {getFieldDecorator('witness_given_name' + index, {
                                          rules: [
                                            {
                                              required: true,
                                              message: 'Please input your Given Name!',
                                            },
                                          ],
                                          initialValue: booking.witness_given_name,
                                        })(<Input placeholder="Given Name" onChange={(e) => this.handleBookingDataChange(index, 'witness_given_name', e.target.value)} disabled={!this.state.isEditMode} />)}
                                      </Form.Item>
                                    </div>

                                    {/*************************** Passport/NRIC Number ***************************/}
                                    <div className="formItemWarp33 ">
                                      <Form.Item label="Passport/NRIC Number">
                                        {getFieldDecorator('witness_passport_number' + index, {
                                          rules: [
                                            {
                                              required: true,
                                              message: 'Please input your Passport/NRIC Number!',
                                            },
                                          ],
                                          initialValue: booking.witness_passport_number,
                                        })(<Input placeholder="Passport/NRIC Number" onChange={(e) => this.handleBookingDataChange(index, 'witness_passport_number', e.target.value)} disabled={!this.state.isEditMode} />)}
                                      </Form.Item>
                                    </div>
                                  </div>
                                </div>

                                {/***************************************** notice_infor *******************************************/}
                                <div className="notice_infor">
                                  <small>By typing in my name and Passport/NRIC No., I confirm that:
                                    <ol>
                                      <li>I am of sound mind and have attained the age of 18 years old;</li>
                                      <li>I am not a nominee named by the Insured Person(s);</li>
                                      <li>I am the witness to the nomination(s) made, as the nomination(s) was(were) made to me by
                                       the respective Insured Person(s); and
                                      </li>
                                      <li>
                                        I have informed the Insured Person(s) that if the nomination does not create a trust policy,
                                        and he/she intends his/her nominee(s) to receive the policy benefits beneficially and not as an executor,
                                        he/she has to assign the policy benefits to his/her nominee(s).
                                      </li>
                                    </ol>
                                  </small>
                                </div>
                              </div>
                            : (null)
                          }
                        </div>
                        <div className="clear"></div>
                                </div>
                              )
                            }) 
                          ) : (null)
                        }
                      </React.Fragment>
                    ) : (null)
                  }
                </Form>
              </div>
            </div>
          </div>
        </Modal>

        {/************************************* Modal Confirm Insurance  ***************************************/}
        <Modal
          title="Chubb Travel Insurance Confirmation"
          visible={this.state.confirmInsuranceModalVisible}
          onOk={(e) => this.handleOk(e, 'confirm_insurance_modal')}
          onCancel={(e) => this.handleCancel(e, 'confirm_insurance_modal')}
          className="seriesModal"
          okText="Yes"
          cancelText="No"
          >
          {/************************************* Content ***************************************/}
          <div>
            <span>
              <h4>The following informations are <strong>required</strong> for Insurance Confirmation :</h4>
              <ol>
                <li>Designation</li>
                <li>Name</li>
                <li>D.O.B</li>
                <li>Mobile</li>
                <li>Passport Number</li>
                <li>Passport Expiry Date</li>
                <li>Upload Passport Copy</li>
              </ol>
              <p>The policy covers you twenty-four (24) hours prior to Your scheduled departure time in Malaysia provided You are in <strong class="insurance-disclaimer">direct transit</strong> between Your Home and the Overseas departure point in Malaysia or vice versa.</p>
              <h4>Insurance confirmation for:</h4>
              <ol>
              { 
                this.state.selectedData && this.state.selectedData.bookings.map((booking, index) => {
                  return (
                    <li>{booking.designation} {booking.name}</li>
                  )
                })  
              }
              </ol>
              <p>Confirm to proceed?</p>
              <p>Please make sure all required fields are filled out correctly before you click the “Yes” button.</p>
            </span>
          </div>
        </Modal>
      </div>
    );
  }
}

const SeriesBooking = Form.create({ name: 'Series' })(TourBooking);
export default withRouter(SeriesBooking);
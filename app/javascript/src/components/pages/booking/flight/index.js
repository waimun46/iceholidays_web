import React, { Component } from 'react';
import { Table, Tag, Typography, Modal, Button, } from 'antd';
import MenuBooking from '../menu';
import API from '../../../../api'
import './index.css';

const { Paragraph } = Typography;

const expandedRowRender = (record) => {
  const columns1 = [
    { title: 'Flight Carrier', dataIndex: 'carrier', key: 'carrier' },
    { title: 'Cabin Class', dataIndex: 'cabin_class', key: 'cabin_class' },
    { title: 'Origin/Terminal ', dataIndex: 'origin_terminal', key: 'origin_terminal' },
    { title: 'Destination/Terminal', dataIndex: 'destination_terminal', key: 'destination_terminal' },
    { title: 'Departure', dataIndex: 'departure_time', key: 'departure_time' },
    { title: 'Arrival', dataIndex: 'arrival_time', key: 'arrival_time' },
    { title: 'Travel Time', dataIndex: 'travel_time', key: 'travel_time' },
    {
      title: 'Remark', dataIndex: 'sell_message', key: 'sell_message', width: 300,
      render: remark => (
        <Paragraph ellipsis={{ rows: 1, expandable: true }}>
          {remark}
        </Paragraph>
      ),
    }
  ]
  const columns2 = [
    { title: 'Designation', dataIndex: 'designation', key: 'designation' },
    { title: 'First Name', dataIndex: 'first_name', key: 'first_name' },
    { title: 'Last Name', dataIndex: 'last_name', key: 'last_name' },
    { title: 'Country', dataIndex: 'country', key: 'country' },
    { title: 'Mobile', dataIndex: 'mobile', key: 'mobile' },
    { title: 'Passport Number', dataIndex: 'passport_number', key: 'passport_number' },
    { title: 'Passport Issue Date', dataIndex: 'passport_issue_date', key: 'passport_issue_date' },
    { title: 'Passport Expirty Date', dataIndex: 'passport_expiry_date', key: 'passport_expiry_date' },
    { title: 'Date of Birth', dataIndex: 'date_of_birth', key: 'date_of_birth' },
    { title: 'Category', dataIndex: 'category', key: 'category' },
    { title: 'Remark', dataIndex: 'remark', key: 'remark' },
    { title: 'BRB', dataIndex: 'brb', key: 'brb' }
  ]

  return <div>
    <Table columns={columns1} dataSource={record.flight_details} pagination={false} />
    <Table columns={columns2} dataSource={record.flight_bookings} pagination={false} />
  </div>;

}


class FlightBooking extends Component {
  state = {
    data: [],
    loading: false,
    visible: false,
    modalData: []
  };

  showModal(record) {
    console.log('record', record)
    this.setState({
      visible: true,
      // modalData: record,
      modalData: this.state.data.filter((element) => {
        return element.code == record
      })
    });
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  componentDidMount() {
    this.apiFetch();
  }

  apiFetch(params = '') {
    // const params = this.props.location.search;
    let url = './api/v1/flights/bookings';
    var that = this;
    console.log(params);

    API.get(url + params)
      .then(function (response) {
        console.log(response, '------------res');
        that.setState({
          data: response.data,
          isLoading: false
        })

      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      });

  }

  render() {
    const { data, modalData } = this.state;
    const infor = modalData[0];
    console.log('modalData-------', modalData)
    console.log('data-------', data)
    console.log('infor-----', infor)


    const columns = [
      {
        title: 'Date',
        dataIndex: 'created_at',
        key: 'created_at',
        width: 150,
      },
      {
        title: 'Order No.',
        dataIndex: 'code',
        key: 'code',
        width: 100,
      },
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        width: 150,
      },
      // {
      //   title: 'Price',
      //   dataIndex: 'price',
      //   key: 'price',
      //   width: 150,
      //   align: 'center'
      // },
      // {
      //   title: 'Pax',
      //   dataIndex: 'pax',
      //   key: 'pax',
      //   width: 100,
      //   align: 'center'
      // },
      // {
      //   title: 'Segment',
      //   dataIndex: 'segment',
      //   key: 'segment',
      //   width: 100,
      //   align: 'center'
      // },
      {
        title: 'Origin Destination',
        dataIndex: 'orgin_destination',
        key: 'orgin_destination',
        width: 250,
      },
      {
        title: 'Status',
        key: 'payment_status',
        dataIndex: 'payment_status',
        width: 100,
        render: (payment_status) => (
          <span>
            <Tag color={payment_status == 'Paid' ? 'green' : payment_status == 'Failed' ? 'volcano' : payment_status == 'Pending' ? 'geekblue' : ''} key={0}>
              {payment_status}
            </Tag>
          </span>
        ),
      },
      {
        title: 'Action',
        width: 100,
        key: 'action',
        dataIndex: 'code',
        render: (record) => (
          <Button type="link" onClick={() => this.showModal(record)} >View</Button>
        ),
      },
    ];
    return (
      <div id="FlightContainer">
        <MenuBooking selected="flight" />
        <div className="content">
          <p className="title">Flight Bookings</p>
          <Table
            columns={columns}
            dataSource={this.state.data}
            pagination={false}
            // onChange={this.handleTableChange}
            // expandedRowRender={expandedRowRender}
            scroll={{ x: 'max-content' }}
            className="table_warp" />
        </div>
        <Modal
          // title="Order Information"
          visible={this.state.visible}
          onOk={this.handleOk}
          okText="DOWNLOAD INVOICE"
          onCancel={this.handleCancel}
          cancelText="RESEND CONFIRMATION"
          className="bookingModal_sty"
          destroyOnClose={true}
          footer={
                    <span>
                    { 
                      infor && infor.invoice_url !== '' ? 
                        <Button type="primary"><a href={infor.invoice_url} target="_blank">DOWNLOAD INVOICE</a></Button> : '' 
                    }
                    </span>
                  }
        >
          {
            modalData.length === 0 ? null :
              <div className="table_order">
                <h2>Order Information</h2>
                <div className="responsive_sroll">
                  <div className="booking_modal_table_warp">
                    <h3>Personal Details</h3>
                    <table className="booking_modal_table">
                      <tr>
                        <th>Designation</th>
                        <td>{infor.flight_bookings[0].designation}</td>
                        <th>First Name</th>
                        <td>{infor.flight_bookings[0].first_name}</td>
                      </tr>
                      <tr>
                        <th>Last Name</th>
                        <td>{infor.flight_bookings[0].last_name}</td>
                        <th>Country</th>
                        <td>{infor.flight_bookings[0].country}</td>
                      </tr>
                      <tr>
                        <th>Mobile</th>
                        <td>{infor.flight_bookings[0].mobile}</td>
                        <th>Passport Number</th>
                        <td>{infor.flight_bookings[0].passport_number}</td>
                      </tr>
                      <tr>
                        <th>Passport Issue Date</th>
                        <td>{infor.flight_bookings[0].passport_issue_date}</td>
                        <th>Passport Expirty Date</th>
                        <td>{infor.flight_bookings[0].passport_expiry_date}</td>
                      </tr>
                      <tr>
                        <th>Date of Birth</th>
                        <td>{infor.flight_bookings[0].date_of_birth}</td>
                        <th>Category</th>
                        <td>{infor.flight_bookings[0].category}</td>
                      </tr>
                      <tr>
                        <th>Remark</th>
                        <td>{infor.flight_bookings[0].remark}</td>
                        <th>BRB</th>
                        <td>{infor.flight_bookings[0].brb}</td>
                      </tr>
                    </table>
                  </div>

                  <div className="booking_modal_table_warp">
                    <h3>Flight Details</h3>
                    {
                      infor.flight_details.map((item, index) => {
                        return (
                          <table className="booking_modal_table booking_modal_table_2">
                            <tr>
                              <th>Flight Carrier</th>
                              <td>{item.carrier}</td>
                              <th>Cabin Class</th>
                              <td>{item.cabin_class}</td>
                            </tr>
                            <tr>
                              <th>Origin/Terminal</th>
                              <td>{item.origin_terminal}</td>
                              <th>Destination/Terminal</th>
                              <td>{item.destination_terminal}</td>
                            </tr>
                            <tr>
                              <th>Departure</th>
                              <td>{item.departure_time}</td>
                              <th>Arrival</th>
                              <td>{item.arrival_time}</td>
                            </tr>
                            <tr>
                              <th>Travel Time</th>
                              <td>{item.travel_time}</td>
                              <th>Remark</th>
                              <td>
                                <Paragraph ellipsis={{ rows: 1, expandable: true }}>
                                  {item.sell_message}
                                </Paragraph>
                              </td>
                            </tr>
                          </table>
                        )
                      })
                    }


                  </div>
                </div>
              </div>
          }
        </Modal>
      </div>
    );
  }
}

export default FlightBooking;
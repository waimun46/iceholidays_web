import React, { Component } from 'react';
import { Table, Tag, Typography } from 'antd';
import MenuBooking from '../menu';
import API from '../../../../api'
import './index.css';

const { Paragraph } = Typography;

const columns = [
  {
    title: 'Date',
    width: 150,
    dataIndex: 'created_at',
    key: 'created_at',
  },
  {
    title: 'Amount',
    width: 100,
    dataIndex: 'amount',
    key: 'amount',
  },
  {
    title: 'References',
    width: 150,
    dataIndex: 'references',
    key: 'references',
  },
  {
    title: 'Remaining Balance',
    width: 150,
    dataIndex: 'remaining_balance',
    key: 'remaining_balance',
  }
];

class CreditUsage extends Component {
  state = {
    data: [],
    loading: false,
  };

  componentDidMount() {
    this.apiFetch();
  }

  apiFetch(params = '') {
    let url = './api/v1/users/credit_usage';
    var that = this;
    console.log(params);

    API.get(url + params)
      .then(function (response) {
        console.log(response, '------------res');
        that.setState({
          data: response.data,
          isLoading: false
        })

      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      });

  }

  render() {
    return (
      <div id="CreditUsageContainer">
        <MenuBooking selected="credit_usage" />
        <div className="content">
          <p className="title">Credit Usage</p>
          <Table 
            columns={columns}
            dataSource={this.state.data}
            pagination={false}
            scroll={{ x: 'max-content' }}
            className="table_warp" />
        </div>
      </div>
    );
  }
}

export default CreditUsage;
import React, { Component } from 'react';
import { Table, Divider, Tag, Typography, Modal, Dropdown, Button, Menu, Form, Input, Radio, Select, DatePicker, Upload, Icon, Checkbox, message} from 'antd';
import MenuBooking from '../menu';
import { Link } from 'react-router-dom';
import API from '../../../../api'
import './index.css';

const { Paragraph } = Typography;

const expandedRowRender = (record) => {
  const columns1 = [
    { title: 'Name', dataIndex: 'name', key: 'name' },
    { title: 'Mobile', dataIndex: 'mobile', key: 'mobile' },
    { title: 'Nationality', dataIndex: 'nationality', key: 'nationality' },
    // { title: 'Passport Expirty Date', dataIndex: 'passport_expiry_date', key: 'passport_expiry_date' },
    { title: 'Date of Birth', dataIndex: 'date_of_birth', key: 'date_of_birth' },
    { title: 'Category', dataIndex: 'category', key: 'category' },
    { title: 'Price', dataIndex: 'total_price', key: 'total_price' },
    {
      title: 'Remark',
      dataIndex: 'remark',
      key: 'remark',
      width: 200,
      render: remark =>(
        <Paragraph ellipsis={{ rows: 3, expandable: true }}>
          {remark}
        </Paragraph>
      ),
    },
  ]
  return <div>
    <Table columns={columns1} dataSource={record.bookings} pagination={false} />
  </div>;

}

class LandTourBooking extends Component {
  state = {
    data: [],
    loading: false,
    visible: false,
    modalData: []
  };

  showModal(record) {
    console.log('record', record)
    this.setState({
      visible: true,
      modalData: this.state.data.filter((element) => {
        return element.id == record
      })
    });
  };

  handleOk = e => {
    this.setState({
      visible: false,
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  componentDidMount() {
    this.apiFetch();
  }

  apiFetch(params = '') {
    let url = './api/v1/land_tours/bookings';
    var that = this;
    console.log(params);

    API.get(url + params)
      .then(function (response) {
        console.log(response, '------------res');
        that.setState({
          data: response.data,
          isLoading: false
        })

      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      });

  }

  render() {
    const { data, modalData } = this.state;
    const landData = modalData[0];

    const columns = [
      {
        title: 'Date',
        width: 150,
        dataIndex: 'created_at',
        key: 'created_at',
      },
      {
        title: 'Order No.',
        width: 150,
        dataIndex: 'order_no',
        key: 'order_no',
      },
      {
        title: 'Name',
        width: 150,
        dataIndex: 'agent_name',
        key: 'agent_name',
      },
      {
        title: 'Package',
        dataIndex: 'code',
        key: 'code',
        width: 150,
      },
      // {
      //   title: 'Price',
      //   width: 150,
      //   dataIndex: 'total_price',
      //   key: 'total_price',
      // },  
      // {
      //   title: 'Quantity',
      //   width: 150,
      //   dataIndex: 'total_quantity',
      //   key: 'total_quantity',
      // },
      // {
      //   title: 'Destination',
      //   width: 200,
      //   dataIndex: 'destination',
      //   key: 'destination',
      // },
      {
        title: 'Status',
        key: 'payment_status',
        dataIndex: 'payment_status',
        width: 100,
        render: (payment_status) => (
          <span>
            <Tag 
              color={
                payment_status == 'Paid' ? 'green' : 
                payment_status == 'Partial Paid' ? 'blue' : 
                payment_status == 'Awaiting' ? 'geekblue' : 
                payment_status == 'Pending' ? 'orange' :
                payment_status == 'Refunded' || payment_status == 'Cancelled' ? '' : 
                ''
              } 
              key={0}>
              {payment_status}
            </Tag>
          </span>
        ),
      },
      {
        title: 'Action',
        width: 90,
        key: 'action',
        dataIndex: 'id',
        render: (record) => (
          <Button type="link" onClick={() => this.showModal(record)}>View</Button>
        ),
      },
    ];

    return (
      <div id="LandTourContainer">
        <MenuBooking selected="land_tour" />
        <div className="content">
          <p className="title">Ground Tour Bookings</p>
          <Table 
            columns={columns}
            dataSource={this.state.data}
            pagination={false}
            // expandedRowRender={expandedRowRender}
            scroll={{ x: 'max-content' }}
            className="table_warp" />
        </div>

        <Modal
          // title="Order Information"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          className="bookingModal_sty"
          okText="DOWNLOAD INVOICE"
          cancelText="RESEND CONFIRMATION"
          // cancelButtonProps={{ style: { display: 'none' } }}
          footer={null}
        >
          {
            modalData.length === 0 ? null :
              <div className="table_order">
                <h2>Order Information</h2>
                <div className="responsive_sroll">
                  <table className="booking_modal_table">
                    <tr>
                      <th>Order No.</th>
                      <td>{landData.order_no}</td>
                      <th>Hotel Name</th>
                      <td>{landData.hotel_category}</td>
                    </tr>
                    <tr>
                      <th>Package</th>
                      <td>{landData.code}</td>
                      <th>Destination</th>
                      <td>{landData.destination}</td>
                    </tr>
                    <tr>
                      <th>Tour Guide</th>
                      <td>{landData.language}</td>
                      <th>Departure Date</th>
                      <td>{landData.departure_date}</td>
                    </tr>
                    <tr>  
                      <th>Status</th>
                      <td>
                        <Tag 
                          color={
                            landData.payment_status == 'Paid' ? 'green' : 
                            landData.payment_status == 'Partial Paid' ? 'blue' : 
                            landData.payment_status == 'Awaiting' ? 'geekblue' : 
                            landData.payment_status == 'Pending' ? 'orange' :
                            landData.payment_status == 'Refunded' || landData.payment_status == 'Cancelled' ? '' : 
                            ''
                          } 
                          key={0}>
                          {landData.payment_status}
                        </Tag>
                      </td>
                      <th>Rooms</th>
                      <td>{landData.rooms}</td>
                    </tr>
                    <tr> 
                      <th>Price</th>
                      <td>{landData.total_price}</td>
                      <th>Paid Amount</th>
                      <td>{landData.paid_amount}</td>
                    </tr>
                    <tr>
                      <th colspan="1">Remark</th>
                      <td colspan="3">{landData.special_request}</td>
                    </tr>
                  </table>
                { 
                  landData.bookings.map((booking, index) => 
                    <table className="booking_modal_table">
                      <tr>
                        <th colspan={4}>Guest #{index+1}</th>
                      </tr>
                      <tr>
                        <th>Name</th>
                        <td>{booking.name}</td>
                        <th>Mobile No.</th>
                        <td>{booking.mobile}</td>
                      </tr>
                      <tr>
                        <th>Date of Birth</th>
                        <td>{booking.date_of_birth}</td>
                        <th>Nationality</th>
                        <td>{booking.nationality}</td>
                      </tr>
                      <tr>
                        <th>Category</th>
                        <td>{booking.category}</td>
                        <th>Price</th>
                        <td>{booking.total_price}</td>
                      </tr>
                      <tr>
                        <th>Remark</th>
                        <td colspan="3">{booking.remark}</td>
                      </tr>
                    </table>
                  )
                }
                </div>
                <div className="btn_modal_booking">
                  <span>
                    { 
                      landData.voucher_url !== undefined ?
                        landData.voucher_url != "" ? 
                          <Button type="primary"><a href={landData.voucher_url} target="_blank">DOWNLOAD VOUCHER</a></Button> : ''
                      : ''
                    }
                  </span>

                  <span>
                    {
                      landData.invoices !== undefined && landData.invoices.length > 0 ? 
                        landData.invoices.length > 1 ?
                          <Dropdown overlay={
                            <Menu>
                            {
                              landData.invoices.map((invoice, index) => {
                                return(
                                  <Menu.Item key={index}>
                                    <a href={invoice.url} target="_blank">Invoice {index + 1}</a>
                                  </Menu.Item>
                                )
                              })
                            }
                            </Menu>
                          }>
                            <Button type="primary">
                              <Icon type="down"/>DOWNLOAD INVOICES
                            </Button>
                          </Dropdown> : 
                          <Button type="primary">
                            <a href={landData.invoices[0].url} target="_blank">DOWNLOAD INVOICE</a>
                          </Button> 
                      : ''
                    }
                  </span>

                  <span>
                    { 
                      (landData.payment_status == 'Awaiting' || landData.payment_status == 'Partial Paid') ? 
                        <Button type="primary"><Link to={{pathname: "/bookings/land_tours/"+landData.id+"/payment"}}>PAY NOW</Link></Button> : '' 
                    }
                  </span>
                </div>

              </div>
          }
        </Modal>
      </div>
    );
  }
}

export default LandTourBooking;
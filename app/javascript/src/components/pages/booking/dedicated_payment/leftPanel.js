import React, { Component } from 'react';
import { Form, Card, Descriptions, Radio, Skeleton, Button, notification } from 'antd';
import { Link, withRouter } from 'react-router-dom';
import API from '../../../../api';
import PaymentMethod from '../../show/payment';
import './index.css';



class LeftPanel extends Component {

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      method_visible: false,
      payment_method: null,
      payment_type: '',
      banklist: [],
      pbb_banklist: [],
      offline_banklist: '',
      banklist_select: [],
      agree: '',
      validationErrors: [],
      check_out_disabled: false,
    }
    this.handlePaymentMethodChange = this.handlePaymentMethodChange.bind(this);
  }

  /***************************************************** NotificationDialogBox function *******************************************************/
  openNotificationWithIcon = (type, error_name) => {
    if (type === "warning") {
      if (error_name === "payment_method_error") {
        notification[type]({
          message: 'Warning!',
          description:
            'Please select payment method before you continue.',
        });
      }
    }
    if (type === "error") {
      if (error_name === "credits_error") {
        notification[type]({
          message: 'Error!',
          description:
            'Insufficient credits! please purchase more credits.',
        });
      }
    }
  };

  /***************************************************** handlePaymentChange function *******************************************************/
  handlePaymentChange(event) {
    console.log(event.target.value);
    if (event.target.value === 'public_bank') {
      this.setState({
        method_visible: true,
        banklist_select: this.state.pbb_banklist,
        payment_method: null
      })
    }
    else if (event.target.value === 'fpx_b2c') {
      this.banklistFetch();
      this.setState({
        method_visible: true,
        banklist_select: this.state.banklist,
        payment_method: null
      })
    }
    else {
      this.setState({
        banklist_select: [],
        method_visible: false,
        payment_method: null
      })
    }
    this.setState({
      payment_type: event.target.value
    })
  }

  /***************************************************** handlePaymentMethodChange function *******************************************************/
  handlePaymentMethodChange(value) {
    this.setState({
      payment_method: value
    })
  };

  /***************************************************** onChangePaymentCheckBox function *******************************************************/
  onChangePaymentCheckBox(e) {
    let isChecked = e.target.checked === true ? "agree" : "";
    console.log(`checked = ${isChecked}`);
    this.setState({
      agree: isChecked
    })
  }

  /***************************************************** banklistFetch *******************************************************/
  banklistFetch() {
    let url = './api/v1/payments/payment_method';
    var that = this;
    // console.log(params);

    API.get(url)
      .then(function (response) {
        // console.log(response, '------------res banklist');
        that.setState({
          banklist: response.data.banklist,
          pbb_banklist: response.data.pbb_banklist,
          offline_banklist: response.data.offline_banklist,
          // credits: response.data.credits
        })
        that.props.getCredits(response.data.credits)
      })
      .catch(function (error) {
        console.log(error);
      })
      .then(function () {
        // always executed
      });
  };

  /***************************************************** componentDidMount *******************************************************/
  componentDidMount() {
    this.banklistFetch();
  };

  /***************************************************** submitForm *******************************************************/
  submitForm = event => {
    event.preventDefault();
    this.banklistFetch();

    const { payment_type, payment_method, agree, validationErrors } = this.state;

    let outputJson = {
      payment: { gateway: payment_type, method: payment_method },
      agree: agree,
      fare_type: this.props.data.fare_type
    };
    console.log("This is the outputJson", outputJson);

    this.props.form.validateFieldsAndScroll((err, values) => {
      if (payment_type === "" || (payment_type !== 'credit_transaction' ? payment_method === null : '') || agree === "" || !err === false) {

        if (!err) {
          console.log('Received values of form: ', values);
        }

        if (payment_type === "") {
          this.openNotificationWithIcon('warning','payment_method_error');
        }
        if (payment_method === null) {
            validationErrors.payment_method = "*Please select a payment method";
            this.setState({ validationErrors: validationErrors })
        } else {
            validationErrors.payment_method = null;
            this.setState({ validationErrors: validationErrors })
        }

        if (agree === "") {
          validationErrors.agree = "*Please agreed the Terms of Use and Privacy Statement";
          this.setState({ validationErrors: validationErrors })
        } else {
          validationErrors.agree = "";
          this.setState({ validationErrors: validationErrors })
        }

        console.log("validationErrors", validationErrors);
      }
      else {
        let url = './api/v1/'+this.props.product_type+'/' + this.props.data.id + '/create_payment/';
        let that = this;

        API.post(url, outputJson)
          .then(function (response) {
              console.log(response,'------------res');
              console.log(response.data.redirect_to,'------------redirect');
              if (response.data.redirect_to) {
                window.location = response.data.redirect_to
              } else if (response.data.credits_error) {
                that.openNotificationWithIcon('error','credits_error');
                that.setState({ payment_type: '' })
              }
              else {
                that.setState({
                  check_out_disabled: true,
                })
              }
        })
        .catch(function (error) {
          console.log(error);
          that.setState({
            visible: true,
          })
        })
        .then(function () {
          // always executed
        });
      }
    });
  }

  render() {
    const { method_visible, offline_banklist, banklist_select, credits, payment_method, payment_type, validationErrors, check_out_disabled } = this.state;
    const loading = this.props.loading;
    return (
      loading ? <Skeleton active /> :
        <div className="LeftPanel">
          <div className="header_title">
            <h2>Order: {this.props.data.order_no}</h2>
          </div>

          {/************************************************ Form ************************************************/}
          <Form onSubmit={this.submitForm} className="form_sty_select" >
            {/******************************* Payment table details  ********************************/}
            <Card >
              <Descriptions bordered size='default' layout="vertical">
                <Descriptions.Item label={<strong>Date</strong>}>{this.props.data.created_at}</Descriptions.Item>
                <Descriptions.Item label={<strong>Package</strong>} span={2}>{this.props.data.code}</Descriptions.Item>
                <Descriptions.Item label={<strong>Destination</strong>}>{this.props.data.destination}</Descriptions.Item>
                <Descriptions.Item label={<strong>Name</strong>} span={2}>{this.props.data.agent_name}</Descriptions.Item>
                <Descriptions.Item label={<strong>Price</strong>}>{this.props.data.total_price}</Descriptions.Item>
                <Descriptions.Item label={<strong>Deposit</strong>}>{this.props.data.deposit}</Descriptions.Item>
                <Descriptions.Item label={<strong>Balance</strong>}>{this.props.data.balance_amount}</Descriptions.Item>
                <Descriptions.Item label={<strong>Payment</strong>} span={3}>{this.props.data.fare_type === 'deposit' ? 'Collect Deposit: '+this.props.data.deposit : 'Collect Balance: '+this.props.data.balance_amount }</Descriptions.Item>
              </Descriptions>
            </Card>

            {/********************************************** Payment Method Component **********************************************/}
            <div className="payment_dedicated_warp ">
              <div className="header_title">
                <h2>Your Payment Method</h2>
              </div>
              <PaymentMethod handlePaymentChange={this.handlePaymentChange.bind(this)} credit_disabled={this.props.credit_disabled} fpx_disabled={false}
                payment_type={payment_type} payment_method={payment_method} credits={this.props.credits} 
                method_visible={method_visible} validationErrors={validationErrors} banklist_select={banklist_select}
                offline_banklist={offline_banklist} handlePaymentMethodChange={(value) => this.handlePaymentMethodChange(value)}
                onChangePaymentCheckBox={(e) => this.onChangePaymentCheckBox(e)} 
              />
            </div>
            <div className="after_select">
              <Button type="primary" htmlType="submit" disabled={check_out_disabled}>CHECK OUT</Button>
            </div>
          </Form>
        </div>

    );
  }
}

const DedicatedPaymentShowPage = Form.create({ name: 'dedicated_payment' })(LeftPanel);
export default withRouter(DedicatedPaymentShowPage);

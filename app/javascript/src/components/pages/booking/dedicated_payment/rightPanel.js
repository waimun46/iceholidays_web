import React, { Component } from 'react';
import { Collapse, Card, Button, Skeleton, Icon } from 'antd';
import './index.css'

const { Panel } = Collapse;

const listData = [
  { title: 'RM23.00/Day X 5day', description: 'RM200' },
  { title: 'Device x 1', description: 'Free' },
  { title: 'Shiping', description: '-' },
  { title: 'Tax', description: '-' }
]

class RightPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // loading: false
    }
  }



  render() {
    const loading = this.props.loading;

    return (
      <div className="RightPanel res_panel_warp">
        {
          loading ? (
            <Skeleton active />
          ) : (
              <Card title={[
                <div className="title">
                  <h2>Summary</h2>
                </div>]}>
                <div className="dates">
                  <p className="date_title">Package</p>
                  <p className="date_select">{this.props.data.code}</p>
                </div>

                <div className="select_list">
                  <div className="list_warp">
                    <div className="total">
                      <span>Total</span><span className="total_summary">{this.props.data.fare_type === 'deposit' ? this.props.data.deposit : this.props.data.balance_amount }</span>
                    </div>
                  </div>
                </div>

              </Card>
            )}
      </div>
    );
  }
}

export default RightPanel;

import React, { Component } from 'react';
import { Drawer, Button, Spin } from 'antd';
import MenuBooking from '../menu';
import LeftPanel from './leftPanel';
import RightPanel from './rightPanel';
import API from '../../../../api';
import './index.css';


class DedicatedPayment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      booking_id: null,
      visible: false,
      loading: true,
      credits: 0,
      credit_disabled: true,
      payment_amount: 0
    }
  }

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  getCredits = (value) => {
    this.setState({
      credits: value
    }, () => { this.handleUserCredits(); })
  };

  handleUserCredits = () => {
    if (this.state.credits && this.state.credits >= Number((this.state.payment_amount).toString().replace(/[^0-9\.-]+/g, ""))) {
      this.setState({
        credit_disabled: false
      })
    }
    else {
      this.setState({
        credit_disabled: true
      })
    }
  }

  componentDidMount() {
    // this.apiFetch();
    console.log(this.props.location.pathname)
    var split_url = this.props.location.pathname.split('/').filter(item => item)
    let booking_id = this.props.location.pathname.match(/\d+/);
    var product_type = split_url[1];
    var that = this;
    API.get('./api/v1/'+product_type+'/'+booking_id+'/new_payment')
    .then(function (response) {
        // console.log(response,'------------res');
        that.setState({
            data: response.data,
            loading: false,
            booking_id: booking_id,
            product_type: product_type,
            payment_amount: response.data.fare_type === 'deposit' ? response.data.deposit : response.data.balance_amount
        })
    })
    .catch(function (error) {
        console.log(error);
    })
    .then(function () {
        // always executed
    });
  }

  render() {
    const { loading, data, credit_disabled, credits, product_type } = this.state;

    var products = {
      "land_tours": "land_tour",
      "series": "series",
    }

    return (
      <div id="DedicatedPayment">
        <MenuBooking selected={products[product_type]} />
        <Spin size="large" spinning={loading}>
          <div className="warppage">
            <LeftPanel loading={loading} data={data} getCredits={this.getCredits} credits={credits} credit_disabled={credit_disabled} product_type={product_type} />
            <RightPanel loading={loading} data={data}/>
            <div className="clear"></div>
          </div>
          <div className="res_panel">
            <Button type="primary" onClick={this.showDrawer} className="res_panel_btn">
              Summary
           </Button>
            <Drawer
              title=""
              placement="right"
              closable={true}
              onClose={this.onClose}
              visible={this.state.visible}
            >
              <RightPanel loading={loading} data={data}/>
            </Drawer>
          </div>
        </Spin>
      </div>
    );
  }
}

export default DedicatedPayment;
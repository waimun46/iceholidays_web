import React, { Component } from 'react';
import Slider from "react-slick";
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './index.css';

class Explore extends Component {

  render() {

    const tabs = this.props.tabs;
    //console.log(this.props.cover, '---------cover-e')

    const settings = {
      className: "slider variable-width",
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 4,
      initialSlide: 0,
      responsive: [
        {
          breakpoint: 770,
          settings: {
            arrows: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 2,
            swipeToSlide: true,
          }
        },
        {
          breakpoint: 625,
          settings: {
            arrows: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 2,
            swipeToSlide: true,
          }
        },
       

      ]
    };

    return (
      <div id="Explore">
        <div className="content_title">
          <h1>{this.props.title}</h1>
          {/* <p>In publishing and graphic design, Lorem ipsum</p> */}
        </div>
        <div className="slider">
          <Slider {...settings}>
            {
              tabs.map((tab) => {
                return (
                  <div className="card_warp">
                    <div className="img_warp"> 
                    {
                      tab.cards.map((card) => {
                        return(
                          card.link ? 
                          (<a href={`${card.link}`} target="_blank"><img alt={card.cover} src={card.cover} /></a>) 
                          : 
                          (<img alt={card.cover} src={card.cover} />)
                        )
                      })
                    }
                    </div>
                    {/* <div className="content_warp">
                      <p>{tab.title}</p>
                      <p>{tab.description}</p>
                    </div> */}
                  </div>
                )
              })
            }
          </Slider>
        </div>
        <div className="clear"></div>

      </div>
    );
  }
}

export default Explore;

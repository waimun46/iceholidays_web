import React, { Component } from "react";
import { Icon } from 'antd';
import "./index.css";
import { withRouter, Link } from 'react-router-dom';

class Footer extends Component {

  render() {

    return (
      <div className="Footer">
        <div className="warp">
          <div className="company">
            <div className="logo">
              <img src={homepage_new.app_logo} alt="travelb2b.my" />
              <span>TRAVELB2B</span>
              <div className="clear"></div>
            </div>
            <div className="content">
              <p>
                Today, ICE Holidays Sdn. Bhd. is a leading Business-to-Business (B2B) Travel Wholesaler in Malaysia. Our 'B2B Travel Wholesale'
                 business model is unique as we offer our partners ready-made products and packages, with advertisements and promotions support.
            </p>
            </div>
          </div>

          <div className="list_btn">
            <ul>
              <li>
                <Link to="/" className="footer_hover">
                  <div className="icon_warp">
                    <Icon type="home" theme="filled" />
                  </div>
                  <p>Home</p>
                </Link>
              </li>
              <li>
                <a href={homepage_new.pdpa} target="_blank" className="footer_hover">
                  <div className="icon_warp">
                    <Icon type="file-ppt" theme="filled" />
                  </div>
                  <p>PDPA</p>
                </a>
              </li>
              {/* <li>
                <a href={pdpa} target="_blank" className="footer_hover">
                  <div className="icon_warp">
                    <Icon type="safety-certificate" theme="filled" />
                  </div>
                  <p>Terms & Conditions</p>
                </a>
              </li> */}
              <li>
                <a href={homepage_new.privacy} target="_blank" className="footer_hover">
                  <div className="icon_warp">
                    <Icon type="lock" theme="filled" />
                  </div>
                  <p>Privacy Policy</p>
                </a>
              </li>
              <div className="clear"></div>
            </ul>
          </div>
        </div>
        <div className="copyright">
          <p>Copyright © 2019 TRAVELB2B. Operated by Ice Holidays Sdn Bhd. All rights reserved.</p>
        </div>

      </div>
    );
  }
}

export default withRouter(Footer);

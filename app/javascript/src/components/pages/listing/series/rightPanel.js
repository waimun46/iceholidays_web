import React, { Component } from 'react';
import { Divider, Spin, Button, List, Icon, Modal, Typography, Tag, Skeleton, Empty } from 'antd';
import InfiniteScroll from 'react-infinite-scroller';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import ReactMarkdown from 'react-markdown/with-html';
import './index.css';
import { withRouter, Link } from 'react-router-dom';
import gd from '../../../../../images/gd.png';
import fs from '../../../../../images/fs.png';
import premium from '../../../../../images/premium.png';
import qs from 'qs';

const { Paragraph } = Typography;

class RightPanel extends Component {
   constructor(props) {
      super(props);
      this.state = {
         limit: 10,
         loadingMore: false,
         hasMore: true,
         isVisible: false,
         modalData: { title: '', other_caption: '', description: '', category: '', location: '', type: '', departdate: '' }
      }
   }

   showModal = (id) => {
      const modalData = this.props.detail.find(elm => {
         return elm.id === id
      })
      this.setState({
         isVisible: true,
         modalData: {
            title: modalData.caption, other_caption: modalData.other_caption, description: modalData.description, category: modalData.category, location: modalData.country,
            type: modalData.code, departdate: modalData.departure_date.map((departdate) => <Tag style={{ margin: '3px 6px 3px 0' }} color="orange">{departdate}</Tag>)
         }
      });
   };

   handleCancel = e => {
      console.log(e);
      this.setState({
         isVisible: false,
      });
   };

   loadMoreData = () => {
      this.setState({
         loadingMore: true,
      });

      if (this.state.limit >= this.props.dataLength) {
         this.setState({
            loadingMore: false,
            hasMore: false
         });
      } else {
         setTimeout(() => {
            this.setState({
               limit: this.state.limit + 5,
               loadingMore: false,
            });
         }, 1000);
      }
   }

   componentDidUpdate() {
      console.log("Check limit count ---> ", this.state.limit)
   }

   render() {
      const dataDetail = this.props.detail;
      const dataLength = this.props.dataLength;
      const { loadingMore, hasMore, limit, isVisible, modalData } = this.state;

      // const filtered = ["GD Premium"];

      // console.log("check filterd data ----> ", filtered)
      // console.log("Check data filter ----> ", dataDetail.filter(x => filtered.includes(x.category)))// dataDetail.filter(x => x.category == !filtered.includes(x.category))); //dataDetail.filter(x => x.category === ["GD Standard","GD Premium"]))

      return (
         <div id="RightPanel">
            {
               dataLength === 0 ? (
                  <div className="empty">
                     {
                        this.props.loading ? (
                           <Skeleton active avatar paragraph={{ rows: 4 }} />
                        ) : (
                              <Empty description="Data not Found" />
                           )
                     }
                  </div>
               ) : (
                     <InfiniteScroll
                        initialLoad={false}
                        loadMore={this.loadMoreData}
                        hasMore={!this.state.loadingMore && this.state.hasMore}
                     >
                        <List
                           itemLayout="vertical"
                           size="large"
                           dataSource={dataDetail.slice(0, limit)}
                           renderItem={item => (
                              <List.Item
                                 key={item.id}
                                 extra={[
                                    <div className="right_price">
                                       <p className="from"><small>From</small></p>
                                       <p className="price_tag">RM {item.price}</p>
                                       <p className="tax_tag"><small>All In</small></p>
                                       <div className="select_btn">
                                          <Link to={{
                                             pathname: "/series/" + item.id + "?month=" + encodeURIComponent(qs.parse(this.props.location.search, { ignoreQueryPrefix: true })['month'])
                                          }}
                                          >
                                             <Button type="primary" ghost>SELECT</Button>
                                          </Link>
                                       </div>
                                       <div className="clear"></div>
                                    </div>
                                 ]}
                              >
                                 <List.Item.Meta
                                    avatar={
                                       <img
                                          alt="logo"
                                          src={item.images.length > 0 ? item.images[0] : homepage_new.default_image}
                                          className="img_sty"
                                       />}
                                    title={
                                       <p className="title_panel">
                                          <Paragraph ellipsis={{ rows: 1, }}
                                             className="ellipsis_style ellipsis_style_view">{item.caption}</Paragraph>
                                          <span className="view_sty">
                                             <Icon type="question-circle" className="view_icon"
                                                onClick={() => this.showModal(item.id)}
                                             />
                                          </span>
                                          <div className="clear"></div>
                                       </p>
                                    }
                                    description={[
                                       <div className="content_rate">
                                          <div className="locate">
                                             <span className="locate_warp">
                                                <FontAwesomeIcon size="lg" icon='map-marker-alt' className="location_icon" />{item.country}
                                             </span>
                                             <span className="locate_warp">
                                                <a href={item.file_url} target="_blank" ><FontAwesomeIcon size="lg" icon='clipboard-list' className="file_icon" />{item.code}</a>
                                             </span>
                                             {item.category !== '' ?
                                                <span className="locate_warp">
                                                   <img
                                                      style={{ width: '6%', height: '100%' }}
                                                      alt="logo"
                                                      src={item.category == "GD Standard" ? gd : item.category == "Four Season" ? fs : item.category == "GD Premium" ? premium : ''}
                                                      className="img_sty"
                                                   />
                                                </span>
                                                : ''
                                             }
                                             <div className="date">
                                                <p>Departure Dates</p>
                                                <p>
                                                   <div className="depart_style">
                                                      {item.departure_date.map((departdate) => <Tag color="orange">{departdate}</Tag>)}
                                                      {/* item.departure_date.slice(0,4).map((departdate) => <Tag color="orange">{departdate}</Tag>) */}
                                                      {/* item.departure_date.length > 4 ? <Tag color="orange">more..</Tag> : '' */}
                                                   </div>
                                                </p>
                                             </div>
                                          </div>


                                       </div>

                                    ]}
                                 />
                              </List.Item>
                           )}
                        >
                           {this.state.loadingMore && this.state.hasMore && (
                              <Skeleton avatar title={false} paragraph={{ rows: 4 }} loading={this.state.loadingMore} active></Skeleton>
                           )}
                        </List>
                     </InfiniteScroll>
                  )
            }
            <Modal
               title={
                  <React.Fragment>
                     <span>{modalData.title}</span>
                     <br />
                     <small>{modalData.other_caption}</small>
                  </React.Fragment>
               }
               visible={isVisible}
               onCancel={this.handleCancel}
               footer={null}
               className="modal_series_listing"
            >
               <p>
                  <span className="modal_locate_warp">
                     <FontAwesomeIcon icon='map-marker-alt' size='lg' className="location_icon" />{modalData.location}
                  </span>
                  <span className="modal_locate_warp">
                     <FontAwesomeIcon icon='clipboard-list' size='lg' className="file_icon" />{modalData.type}
                  </span>
                  <span className="modal_locate_warp">
                     <img
                        style={{ width: '6%', height: '100%' }}
                        alt="logo"
                        src={modalData.category == "GD Standard" ? gd : modalData.category == "Four Season" ? fs : modalData.category == "GD Premium" ? premium : ''}
                        className="img_sty"
                     />
                  </span>
               </p>
               <p className="departure_style">Departure Dates</p>
               <p className="departure">{modalData.departdate}</p>
               {
                  modalData.description !== '' ?
                     <React.Fragment>
                        <p className="departure_style">Highlight</p>
                        <p className="departure"><ReactMarkdown source={modalData.description} escapeHtml={false} /></p>
                     </React.Fragment>
                     : ''
               }
            </Modal>
         </div>
      );
   }
}

export default withRouter(RightPanel);

class HotelBookingGroupsController < ApplicationController

  def cancel
    authorize Hotel, :index?

    if params[:hotel_booking_group].present?
      @hotel_booking_group = HotelBookingGroup.find(params[:hotel_booking_group])

      @cancelled_hotel_booking_group = TravelPrologue::Booking.cancel(@hotel_booking_group)

      if @cancelled_hotel_booking_group.cancel_successful? == true
        @hotel_booking_group.cancelled!
        redirect_to admin_hotel_booking_groups_path, notice: "Your booking was successfully cancelled"
      else
        redirect_to admin_hotel_booking_groups_path, alert: "#{@cancelled_hotel_booking_group.message}"
      end
    end
  end

end

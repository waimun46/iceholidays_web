module Api
  module TBOHolidays
    class CitiesController < ApiController
      def index
        # Return cities of active countries
        cities = TBOCity.all_cities
        render json: cities
      end
    end
  end
end

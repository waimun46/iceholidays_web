module Api
  class SeriesController < ApiController
    def index
      respond_to do |format|
        format.json do
          if params[:key].present? && params[:key] == 'GDApiYEK'
            keyword = params[:keyword].present? ? params[:keyword].strip : ''
            category = params[:category].present? ? params[:category] : ''

            if params[:page].present?
              page = params[:page]
            else
              page = 1
            end

            if params[:limit].present?
              limit = params[:limit]
            else
              limit = 100
            end

            @itineraries = Itinerary.with_available_tours(interval_params).search_keyword(keyword).order_by_departure.exclude_cruise.exclude_tour_code.exclude_grip.tour_category_filter(category).page(page).per(limit)
          else
            render json: { ret: "Failed" }, status: 401
          end
        end
      end
    end

    def show
      respond_to do |format|
        format.json do
          if params[:key].present? && params[:key] == 'GDApiYEK'
            @itinerary = Itinerary.with_available_tours(interval_params).exclude_cruise.exclude_tour_code.exclude_grip.order_by_departure.find(params[:id])
          else
            render json: { ret: "Failed" }, status: 401
          end
        end
      end
    end

    def country_list
      respond_to do |format|
        format.json do
          if params[:key].present? && params[:key] == 'GDApiYEK'
            @countries = Itinerary.with_available_tours(interval_params).exclude_cruise.pluck("contient_country").map{|v|v.strip}.uniq.sort
            render json: { countries: @countries }
          else
            render json: { ret: "Failed" }, status: 401
          end
        end
      end
    end

    def itinerary_list
      respond_to do |format|
        format.json do
          if params[:key].present? && params[:key] == 'GDApiYEK'
            @itineraries = Itinerary.with_available_tours(interval_params).exclude_cruise.pluck("id","code")
            render json: { codes: @itineraries }
          else
            render json: { ret: "Failed" }, status: 401
          end
        end
      end
    end

private
    def interval_params
      @today = Date.today
      if params[:month].present?
        interval_start = "#{params[:month]}".in_time_zone
        interval_end = "#{params[:month]}".in_time_zone.end_of_month
          if interval_start < (@today+1.day)
            interval_start = (@today+1.day).beginning_of_day
          end
      else
        interval_start = (@today+1.day).beginning_of_day
        interval_end = DateTime::Infinity.new
      end
      interval = interval_start..interval_end
    end
  end
end
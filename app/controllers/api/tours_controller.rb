module Api
  class ToursController < ApiController

    def index
      respond_to do |format|
        format.json do
          if params[:key].present? && params[:key] == 'GDApiYEK'
            destination = params[:destination].present? ? params[:destination] : ''
            caption = params[:caption].present? ? params[:caption] : ''
            code = params[:code].present? ? params[:code] : ''
            category = params[:category].present? ? params[:category] : ''
            keyword = params[:keyword].present? ? params[:keyword] : ''
            caption_uniq = params[:caption_uniq].present? ? true : false
            only_departure_date = params[:only_departure_date].present? ? true : false
            only_bookable = params[:only_bookable].present? ? true : false

            if params[:end].present? && "#{params[:end]}".in_time_zone
              date_range = Date.tomorrow.."#{params[:end]}".in_time_zone.end_of_day
            else
              date_range = Date.tomorrow...DateTime::Infinity.new
            end

            if params[:departure_date_start].present? && "#{params[:departure_date_start]}".in_time_zone
              departure_date_start = "#{params[:departure_date_start]}".in_time_zone.beginning_of_day
            else
              departure_date_start = Date.tomorrow
            end

            if params[:departure_date_end].present? && "#{params[:departure_date_end]}".in_time_zone
              departure_date_end = "#{params[:departure_date_end]}".in_time_zone.end_of_day
            else
              departure_date_end = DateTime::Infinity.new
            end

            if params[:page].present?
              page = params[:page]
            else
              page = 1
            end

            if params[:limit].present?
              limit = params[:limit]
            else
              limit = 1000
            end

            departure_date_interval = departure_date_start...departure_date_end

            
            if caption_uniq
              if only_bookable
                @tours = Tour.visible.caption_uniq.search(keyword).destination(destination).where("tours.code ILIKE ?", "%#{code}%").category_filter(category).caption(caption).bookable(date_range).in_between_departure(departure_date_interval).page(page).per(limit)
              else
                @tours = Tour.visible.caption_uniq.search(keyword).destination(destination).where("tours.code ILIKE ?", "%#{code}%").category_filter(category).caption(caption).in_between_departure(departure_date_interval).page(page).per(limit)
              end
            elsif only_departure_date
              if only_bookable
                departure_dates = Tour.visible.search(keyword).destination(destination).where("tours.code ILIKE ?", "%#{code}%").category_filter(category).caption(caption).bookable(date_range).in_between_departure(departure_date_interval).order(:departure_date).pluck(:departure_date).uniq
              else
                departure_dates = Tour.visible.search(keyword).destination(destination).where("tours.code ILIKE ?", "%#{code}%").category_filter(category).caption(caption).in_between_departure(departure_date_interval).order(:departure_date).pluck(:departure_date).uniq
              end
              render json: { departure_dates: departure_dates.map{|d| d.strftime("%F")} }
            else
              if only_bookable
                @tours = Tour.visible.search(keyword).destination(destination).where("tours.code ILIKE ?", "%#{code}%").category_filter(category).caption(caption).bookable(date_range).in_between_departure(departure_date_interval).order(:departure_date, :code).page(page).per(limit)
              else
                @tours = Tour.visible.search(keyword).destination(destination).where("tours.code ILIKE ?", "%#{code}%").category_filter(category).caption(caption).in_between_departure(departure_date_interval).order(:departure_date, :code).page(page).per(limit)
              end
            end
          else
            render json: { ret: "Failed" }, status: 401
          end
        end
      end
    end
    
  end
end
module Api
  module V1
    class LandToursController < ApiV1Controller
      if Rails.env.development?
        skip_before_action :authenticate_user!
      end

      def index
        respond_to do |format|
          format.json do
            keyword = params[:keyword].present? ? params[:keyword].strip : ''
            
            @land_tours = []
            @cities = City.search_keyword(keyword)
            @city_ids = @cities.map(&:id)
            @tours = LandTour.search_keyword(keyword).search_travel_date(interval_params[0], interval_params[1]).visible
            if @tours.present?
              @tours.each do |tour|
                if @city_ids.present?
                  tour.land_tour_cities.each do |city|
                    if @city_ids.include?(city.city_id)
                      @land_tours << tour
                    end
                  end
                else
                  @land_tours << tour
                end
              end
            else
              @land_tour_cities = @cities.map{|c|c.land_tour_cities}.flatten
              @land_tour_cities.each do |land_tour_city|
                if land_tour_city.land_tour_day.present? && land_tour_city.land_tour_day.land_tour.present? && land_tour_city.land_tour_day.land_tour.active?
                  @land_tours << land_tour_city.land_tour_day.land_tour
                end
              end
            end
          end
        end
      end

      def show
        respond_to do |format|
          format.json do
            @land_tour = LandTour.visible.find(params[:id])
            @interval = interval_params[0]..interval_params[1]
            @land_tour_days = @land_tour.land_tour_days.order(day_no: :asc)
          end
        end
      end

      def create_booking
        respond_to do |format|
          format.json do
            @land_tour = LandTour.visible.find(params[:tour_id])
            @category = LandTourCategory.find(params[:category_id])

            price_qty = process_qty_params
            
            if price_qty.present?
              @land_tour_booking_group = LandTourBooking.bulk_create(@land_tour, @category, params[:depart_date], params[:rooms], params[:language], price_qty, current_user, booking_params, flight_params)
              if @land_tour_booking_group.id.present?
                @booking_payment = Payment.new(payment_params)
                if @booking_payment.credit_transaction? && !current_user.sufficient_credits?(@booking_payment.amount)
                  render json: { credits_error: "Insufficient credits, please purchase more credits." }
                else
                  if @booking_payment.save
                    render json: { redirect_to: proceed_to_payment_path(:payment_id => @booking_payment.id) }
                  else
                    render json: { error: "Your payment could not processed. Please try again." }
                  end
                end
              else
                render json: { error: "The booking prices are no longer available, please book again. Sorry for inconvenient." }
              end
            else
              render json: { error: "The booking prices are no longer available, please book again. Sorry for inconvenient." }
            end
          end
        end
      end

      def bookings
        @land_tour_booking_groups = current_user.land_tour_booking_groups.includes(land_tour_bookings: [:land_tour, :agent_user]).order("land_tour_booking_groups.created_at desc")
      end

      def new_payment
        respond_to do |format|
          format.json do
            @land_tour_booking_group = current_user.land_tour_booking_groups.with_booking.find(params[:id])
          end
        end
      end

      def create_payment
        respond_to do |format|
          format.json do
            @land_tour_booking_group = current_user.land_tour_booking_groups.with_booking.find(params[:id])
            @booking_payment = Payment.new(balance_payment_params)
            if @booking_payment.save
              render json: { redirect_to: proceed_to_payment_path(:payment_id => @booking_payment.id) }
            else
              @booking_payment.failed_and_action
              render json: { error: "Your payment could not processed. Please try again." }
            end
          end
        end
      end

      def country_list
        @cities = []
        @countries = []
        @land_tour_cities = LandTour.visible.map{|lt|lt.land_tour_cities}.flatten
        @land_tour_cities.each do |land_tour_city|
          @cities << land_tour_city.city.name
          @countries << land_tour_city.city.country
        end
        render json: { countries: @countries.uniq.sort, cities: @cities.uniq.sort }
      end

      def land_tours_list
        @land_tours = LandTour.visible.pluck("id","title")
        render json: { tours: @land_tours }
      end

      def designation_list
        @designations = LandTourBooking::DESIGNATION
        render json: { designations: @designations }
      end

    private
      def process_qty_params
        prices = []
        @category.land_tour_prices.each do |price|
          all_date = price.dates.map {|e| e.inject([]){|k, date| k << date} }.flatten
          if all_date.any?{|d|d == params[:depart_date].to_date}
            params[:booking_prices].each do |bp|
              qty = bp["quantity"]
              prices << ["#{bp["name"]}", qty.to_i]
            end
          end
        end
        prices.reject(&:blank?).to_h
      end

      def payment_params
        if params[:payment][:gateway] == "pay_later"
          price = 0
        elsif params[:fare_type] == "deposit"
          price = @land_tour_booking_group.deposit.to_f
        else
          price = @land_tour_booking_group.total_price.to_f
        end

        params.require(:payment).permit(:gateway, :method).merge!(amount: price, taggable: @land_tour_booking_group, callback: 0, user_id: current_user.id) 
      end

      def balance_payment_params
        if params[:fare_type] == "deposit"
          price = @land_tour_booking_group.deposit.to_f
        else
          price = @land_tour_booking_group.balance_amount
        end

        params.require(:payment).permit(:gateway, :method).merge!(amount: price, taggable: @land_tour_booking_group, callback: 0, user_id: current_user.id) 
      end

      def booking_params
        params.require(:bookings).map{|b| b.permit(:designation, :name, :mobile, :category, :nationality, :date_of_birth, :remark)}
      end

      def flight_params
        params.require(:flights).map{|b| b.permit(:flight_departure_details, :flight_return_details)}
      end

      def interval_params
        @today = Date.today
        if params[:month].present?
          interval_start = "#{params[:month]}".in_time_zone
          interval_end = "#{params[:month]}".in_time_zone.end_of_month
            if interval_start < (@today+1.day)
              interval_start = (@today+1.day).beginning_of_day
            end
        else
          interval_start = (@today+1.day).beginning_of_day
          interval_end = (@today+1.year).beginning_of_day
          # interval_end = DateTime::Infinity.new
        end
        interval = [interval_start, interval_end]
      end
    end
  end
end
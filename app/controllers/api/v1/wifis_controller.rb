module Api
  module V1
    class WifisController < ApiV1Controller
      if Rails.env.development?
        skip_before_action :authenticate_user!
      end
      before_action :check_auth

      def index
        respond_to do |format|
          format.json do
            keywords = params[:keyword].present? ? params[:keyword] : ''
            @packages = RoamingmanPackage.visible.search(keywords)
          end
        end
      end

      def show
        respond_to do |format|
          format.json do
            @package = RoamingmanPackage.find(params[:id])
          end
        end
      end

      def country_list
        @countries = RoamingmanPackage.all.map(&:countries).flatten.collect(&:strip).uniq.sort
        render json: { countries: @countries }
      end

      def create_booking
        respond_to do |format|
          format.json do
            @roamingman_booking_group = RoamingmanBookingGroup.new(booking_params)
            if @roamingman_booking_group.save
              @roamingman_payment = Payment.new(payment_params)
              
              if @roamingman_payment.save
                render json: { redirect_to: proceed_to_payment_path(:payment_id => @roamingman_payment.id) }
              else
                @roamingman_payment.failed_and_action
                render json: { error: "Your payment could not processed. Please try again." }
              end
            else
              render json: { error: "Wifi Booking was failed to create. Please try again." }
            end
          end
        end
      end

      def bookings
        @roamingman_booking_groups = current_user.roamingman_booking_groups.includes(:roamingman_bookings).order("roamingman_booking_groups.created_at desc")
      end

private
      def booking_params
        new_params = params.require(:roamingman_booking_group).permit(:remark, roamingman_bookings_attributes: [:id, :name, :mobile, :email, :depart_date, :return_date, :quantity, :ship_way, :deliver_point_id, :address, :postcode, :is_send_sms, :is_send_email, :_destroy])
        if new_params[:roamingman_bookings_attributes].present?
          new_params[:roamingman_bookings_attributes].each {|v| v.merge!(roamingman_package_id: params[:id], agent_user_id: current_user.id)}
        end
        new_params
      end

      def payment_params
        price = @roamingman_booking_group.price.to_f
        params.require(:payment).permit(:gateway, :method).merge!(amount: price, taggable: @roamingman_booking_group, callback: 0, user_id: current_user.id)
      end

      def check_auth
        authorize RoamingmanBookingGroup, :index?
      end
    end
  end
end
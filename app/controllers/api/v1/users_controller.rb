module Api
  module V1
    class UsersController < ApiV1Controller
      def details
        @user = current_user
      end

      def credit_usage
        @credit_usage_transactions = current_user.credit_transactions.order("created_at desc")
    	end

      def credit_packages
        authorize CreditPurchase, :index?
        @credit_packages = CreditPurchase::CREDIT_PACKAGES
        render json: @credit_packages
      end

      def purchase_credits
        authorize CreditPurchase, :index?
        @user = current_user
        respond_to do |format|
          format.json do
             @purchase_credit = CreditPurchase.create(purchase_credits_params)
             if @purchase_credit.id.present?
              if payment_params[:gateway] != 'credit_transaction' 
                @purchase_payment = Payment.new(payment_params)
                if @purchase_payment.save
                  render json: { redirect_to:  proceed_to_payment_path(:payment_id => @purchase_payment.id) }
                else
                  render json: { error: "Your payment could not processed. Please try again." }
                end
               else
                render json: { error: "The credits was failed to purchase, please try again. Sorry for inconvenient." }
               end
             else
              render json: { error: "Your payment could not processed. Please try again." }
             end
          end
        end
      end
    end
  end
end

private
  def purchase_credits_params
    params.require(:purchase_info).permit(:amount, :credits).merge(user_id: current_user.id, remark: '', status: 0)
  end

  def payment_params
    params.require(:payment).permit(:gateway, :method).merge!(amount: @purchase_credit.amount, taggable: @purchase_credit, callback: 0, user_id: current_user.id)
  end
module Api
  module V1
    class SeriesController < ApiV1Controller
      if Rails.env.development?
        skip_before_action :authenticate_user!, raise: false
      end
      before_action :check_auth

      def index
        respond_to do |format|
          format.json do
            keyword = params[:keyword].present? ? params[:keyword].strip : ''
            @itineraries = Itinerary.with_available_tours(interval_params).search_keyword(keyword).order_by_departure.exclude_cruise.exclude_tour_code.exclude_grip #.page(params[:page]).per(Itinerary::ITINERARIES_PER_PAGE)
          end
        end
      end

      def show
        respond_to do |format|
          format.json do
            @current_time = Time.now
            @itinerary = Itinerary.with_available_tours(interval_params).exclude_cruise.exclude_tour_code.exclude_grip.order_by_departure.find(params[:id])
          end
        end
      end

      def create_booking
        respond_to do |format|
          format.json do
            @tour = Tour.visible.find(params[:tour_id])
            
            booking_seats = booking_seats_params

            if booking_seats.present? && current_user.present? && @tour.able_to_purcharse?(current_user, booking_seats)
              if !@tour.is_cruise? && !params[:bookings].any?{|b|b[:category]== 'adult'}
                render json: { error: "The booking must at least have one adult." }
              else
                @bookings = Booking.bulk_create(@tour, booking_seats, current_user, nil, params[:sub_sale_rep], booking_params, 'travelb2b')
                if @bookings.present?
                  @booking_payment = Payment.new(payment_params)
                  if @booking_payment.save
                    render json: { redirect_to: proceed_to_payment_path(:payment_id => @booking_payment.id) }
                  else
                    @booking_payment.failed_and_action
                    render json: { error: "Your payment could not processed. Please try again." }
                  end
                else
                  render json: { error: "The booking seats are no longer available, please book again. Sorry for inconvenient." }
                end
              end
            else
              render json: { error: "The booking seats are no longer available, please book again. Sorry for inconvenient." }
            end
          end
        end
      end

      def update_bookings
        respond_to do |format|
          format.json do
            @booking_group = current_user.booking_groups.with_booking.find(params[:id])
            # authorize @booking_group
            if @booking_group.update(update_booking_group_params)
              render json: { message: "success" }
            else
              render json: { error: "error" }
            end
          end
        end
      end

      def bookings
        @tour_booking_groups = current_user.booking_groups.includes(bookings: [:tour, :agent_user]).order("booking_groups.created_at desc")
      end

      def new_payment
        respond_to do |format|
          format.json do
            @booking_group = current_user.booking_groups.with_booking.find(params[:id])
            # authorize @booking_group
          end
        end
      end

      def create_payment
        respond_to do |format|
          format.json do
            @booking_group = current_user.booking_groups.with_booking.find(params[:id])
            # authorize @booking_group
            @booking_payment = Payment.new(balance_payment_params)
            if @booking_payment.save
              render json: { redirect_to: proceed_to_payment_path(:payment_id => @booking_payment.id) }
            else
              @booking_payment.failed_and_action
              render json: { error: "Your payment could not processed. Please try again." }
            end
          end
        end
      end

      def create_insurance_export
        respond_to do |format|
          format.json do
            @booking_group = current_user.booking_groups.with_booking.find(params[:id])
            # authorize @booking_group
            if @booking_group.export_insurance(current_user, "new")
              render json: { redirect_to: booking_group_path(@booking_group), notice: 'Insurance for BookingGroup was successfully exported.' }
            else
              render json: { error: "Failed to export insurance, please try again later or check on chubb system." }
            end
          end
        end
      end 

      def country_list
        @countries = Itinerary.with_available_tours(interval_params).exclude_cruise.pluck("contient_country").map{|v|v.strip}.uniq.sort
        render json: { countries: @countries }
      end

      def itinerary_list
        @itineraries = Itinerary.with_available_tours(interval_params).exclude_cruise.pluck("id","code")
        render json: { codes: @itineraries }
      end

      def designation_list
        @designations = Booking::DESIGNATION
        render json: { designations: @designations }
      end

    private
      def payment_params
        if params[:payment][:gateway] == "pay_later"
          price = 0
        elsif params[:fare_type] == "deposit"
          price = @bookings.first.booking_group.deposit.to_f
        else
          price = @bookings.first.booking_group.price.to_f
        end

        params.require(:payment).permit(:gateway, :method).merge!(amount: price, taggable: @bookings.first.booking_group, callback: 0, user_id: current_user.id) 
      end

      def balance_payment_params
        if params[:fare_type] == "deposit"
          price = @booking_group.deposit.to_f
        else
          price = @booking_group.balance_amount
        end

        params.require(:payment).permit(:gateway, :method).merge!(amount: price, taggable: @booking_group, callback: 0, user_id: current_user.id) 
      end

      def booking_seats_params
        if params[:booking_seats].class === 'String'
          booking_seats = process_seats_params(params[:booking_seats])
          {''=> booking_seats}
        elsif @tour.is_cruise? && params[:booking_seats].keys.length > 0
          booking_seats = {}
          params[:booking_seats].each do |type, value|
            data = process_seats_params(value)
            if data.present?
              booking_seats[type] = data
            end
          end
          booking_seats
        end
      end

      def process_seats_params(data_string)
        data_string.split(', ').map do |seat|
          # parsing "2 promo(188)"
          seat_match = seat.match /\A(\d+)\s(\w+)\(\d+\)/
          if seat_match.present? && seat_match.length == 3
            [seat_match[2], seat_match[1].to_i]
          else
            nil
          end
        end.reject(&:blank?).to_h
      end

      def booking_params
        params.require(:bookings).map{|b| b.permit(:category, :designation, :name, :date_of_birth, :mobile, :passport_number, :remark)} #, infant_bookings_attributes: [:id, :name, :date_of_birth, :gender])}
      end

      def interval_params
        @today = Date.today
        if params[:month].present?
          interval_start = "#{params[:month]}".in_time_zone
          interval_end = "#{params[:month]}".in_time_zone.end_of_month
            if interval_start < (@today+1.day)
              interval_start = (@today+1.day).beginning_of_day
            end
        else
          interval_start = (@today+1.day).beginning_of_day
          interval_end = DateTime::Infinity.new
        end
        interval = interval_start..interval_end
      end

      def update_booking_group_params
        new_params = params.require(:booking_group).permit(:insurance_receiver, bookings_attributes: [:id, :insurance_confirmation, :nationality, :passport_expiry_date, :passport_photocopy, :room_type, :remark, :guardian_id, :ph_designation, :ph_name, :ph_dob, :ph_id_number, :insurance_nomination_flag, :witness_surname, :witness_given_name, :witness_passport_number, insurance_nomination: [:nominee_surname, :nominee_given_name, :nominee_passport_number, :nominee_date_of_birth, :nominee_relationship, :nominee_share]])

        if new_params[:bookings_attributes].present?
          new_params[:bookings_attributes].each do |k, b|
            b.merge!(edited_user_id: current_user.id, insurance_nomination: b[:insurance_nomination].values)
          end
        end
        new_params
      end

      def check_auth
        authorize Tour, :index?
      end

    end
  end
end
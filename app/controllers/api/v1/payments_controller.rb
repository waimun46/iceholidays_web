module Api
  module V1
    class PaymentsController < ApiV1Controller
      if Rails.env.development?
        skip_before_action :authenticate_user!
      end

      def payment_method
        @payment = Payment.new
        @banklist = @payment.fpx_banklist
        @offline_banklist = @payment.offline_banklist(@banklist)
        render json: { pbb_banklist: Payment::PBB_METHOD, banklist: @banklist, offline_banklist: @offline_banklist, credits: current_user.credits }
      end

      def show
        respond_to do |format|
          format.json do
            @payment = current_user.payments.find_by_id(params[:id])
            if @payment.blank?
              render json: { error: "Payment not found."}
            end
          end
        end
      end
    end
  end
end
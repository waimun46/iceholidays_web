module Api
	module V1
		class ActivitiesController < ApiV1Controller
			if Rails.env.development?
        skip_before_action :authenticate_user!
      end
      before_action :check_auth

			def index
				respond_to do |format|
					format.json do
						
						# if params[:from_date].present? && params[:to_date].present?
					 #      from_date = params[:from_date]
					 #      to_date = params[:to_date]
					 #    elsif params[:from_date].present?
					 #      from_date = params[:from_date]
					 #      to_date = params[:from_date].to_datetime.end_of_year
					 #    elsif params[:to_date].present?
					 #      from_date = Time.now.midnight
					 #      to_date = params[:to_date]
					 #    else
					 #      from_date = Time.now.midnight
					 #      to_date = Time.now.end_of_year
					 #    end

						@activities = []
						@activities << GlobaltixAttraction.search_keyword(params[:keyword]).search_category(params[:category]).active_tickets #.active.group_by(&:attraction_id)
            @activities << Activity.search_keyword(params[:keyword]).search_category(params[:category]).available.visible.bookable
            @activities.flatten!
            @activities.sort_by!{|g| -g.created_at.to_i}
  				end
				end
			end

			def show
				respond_to do |format|
					format.json do
						if params[:taggable_type].present? && params[:taggable_type] == "ACT"
							@activity = Activity.available.visible.bookable.find(params[:id])
						elsif params[:taggable_type].present? && params[:taggable_type] == "GT"
              @globaltix_attraction = GlobaltixAttraction.find(params[:id])
              @globaltix_tickets = @globaltix_attraction.globaltix_tickets.active.order_by_priority
              @tickets, tickets_err = Globaltix::Ticket.where(attraction_id: @globaltix_attraction.attraction_id)
              @attraction, attraction_err = Globaltix::Attraction.find(@globaltix_attraction.attraction_id)
						else
							render json: {}
						end
					end
				end
			end

			def create_booking
				respond_to do |format|
					format.json do
						if params[:taggable_type] == "ACT"
							# authorize ActivityBooking
					    @activity = Activity.find(params[:id])
					    price_qty = process_qty_params(@activity.activity_prices)
            elsif params[:taggable_type] == "GT"
              @globaltix_tickets = GlobaltixTicket.where(id: params[:ticketDetails].map{|t| t[:id].to_i})
              price_qty = process_qty_params(@globaltix_tickets)
            end

				   	if price_qty.present?
              if params[:taggable_type] == "ACT"
  				      @booking_group = ActivityBooking.bulk_create(@activity, price_qty, current_user, params[:visitDate], activity_params)
              else
                @booking_group = GlobaltixTicketBookingGroup.create_booking(@globaltix_tickets, price_qty, globaltix_params[0], current_user)
              end

				      if @booking_group.id.present?						      	
				        @booking_payment = Payment.new(payment_params)
                
				        if @booking_payment.save
                  render json: { redirect_to:  proceed_to_payment_path(:payment_id => @booking_payment.id) }
				        else
                  @booking_payment.failed_and_action
              		render json: { error: "Your payment could not processed. Please try again." }
				        end
				      else
        				render json: { error: "The booking was failed to create, please book again. Sorry for inconvenient." }
				      end
				  	else
				  		render json: { error: "The booking tickets are no longer available. Sorry for inconvenient." }
			    	end
					end
				end
			end

			def bookings
				respond_to do |format|
					format.json do
						@activity_booking_groups = []
						@activity_booking_groups << current_user.globaltix_ticket_booking_groups.includes(globaltix_ticket_bookings: [:agent_user]).order("globaltix_ticket_booking_groups.created_at desc")
						@activity_booking_groups << current_user.activity_booking_groups.includes(:activity_bookings).order("activity_booking_groups.created_at desc")
						@activity_booking_groups.flatten!
						@activity_booking_groups.sort_by!{|g| -g.created_at.to_i}
					end
				end
			end

      def country_list
        @countries = (GlobaltixAttraction.search_category(params[:category]).active_tickets.countries + Activity.search_category(params[:category]).available.visible.bookable.countries).uniq.sort
        render json: { countries: @countries }
      end

private

		  # def price_qty_params
    #     if params[:taggable_type] == "ACT"
    #       qty_prices = process_qty_params(@activity.activity_prices)
    #     elsif params[:taggable_type] == "GT"
    #       qty_prices = process_qty_params(@globaltix_tickets)
    #     else
    #       qty_prices = 0
    #     end
		  #   {'' => qty_prices}
		  # end

		  def process_qty_params(activity_tickets)
        if params[:taggable_type] == "ACT"
          activity_tickets.map do |activity_price|
            ticket = params[:ticketDetails].detect{|j| j["id"] == activity_price.id && (activity_price.blockout_dates.blank? || activity_price.blockout_dates.map{|bd| bd.include?(params[:visitDate]) }.none?) }
            if ticket.present? && ticket["quantity"].present? && ticket["quantity"].to_i > 0
              ["#{activity_price.category}", ticket["quantity"]]
            else
              nil
            end
          end.reject(&:blank?).to_h
        elsif params[:taggable_type] == "GT"
          activity_tickets.map do |price|
            ticket = params[:ticketDetails].detect{|j| j["id"] == price.id}
            if ticket.present? && ticket["quantity"].present? && ticket["quantity"].to_i > 0
              ["#{price.id}", ticket["quantity"]]
            else
              nil
            end
          end.reject(&:blank?).to_h
        else
          #
        end
		  end

		  def activity_params
				params.permit(:country)
				params.require(:bookingDetails).map{|b| b.permit(:name, :mobile, :email, :remark)}
		  end

		  def globaltix_params
		  	# merge index, fromResellerId, alternateEmail, paymentMethod
		    new_params = params.require(:bookingDetails).map{|b| b.permit(:email, :customerName, :remark, ticketDetails: [:id, :quantity, :visitDate, questionList: [:id, :answer]]).merge(paymentMethod: 'CREDIT', visitDate: params[:visitDate])}
		    if new_params[0][:ticketDetails].present?
		      # remove the invalid ticket
          new_params[0][:ticketDetails].delete_if{|ticket| @globaltix_tickets.detect{|gt| gt.id == ticket[:id].to_i}.blank? || ticket[:quantity].to_i <= 0}

		      new_params[0][:ticketDetails].each_with_index do |ticket, index|
		        ticket[:id] = @globaltix_tickets.detect{|gt| gt.id == ticket[:id].to_i}.ticket_id
		        ticket[:quantity] = ticket[:quantity].to_i
            # ticket[:visitDate].delete{@globaltix_tickets.find_by_ticket_id(ticket[:id]).ticket.is_visit_date_compulsory? == false}
            if @globaltix_tickets.find_by_ticket_id(ticket[:id]).ticket.is_visit_date_compulsory? == false
             ticket.delete(:visitDate)
            end
            # Rails.logger.info "Check visitdateee ----> #{@globaltix_tickets.find_by_ticket_id(ticket[:id]).ticket.is_visit_date_compulsory?}"
            Rails.logger.info "Check visit date ----> #{ticket[:visitDate]}"
		        ticket.merge!(index: index, fromResellerId: nil)
		      end
          new_params[0][:ticketTypes] = new_params[0].delete(:ticketDetails)
		    end
		    new_params			   
      end

		  def payment_params
        params.require(:payment).permit(:gateway, :method).merge!(amount: @booking_group.price, taggable: @booking_group, callback: 0, user_id: current_user.id)
      end

      def check_auth
        authorize Activity, :index?
      end

		end 
	end
end

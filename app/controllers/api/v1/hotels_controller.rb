module Api
  module V1
    class HotelsController < ApiV1Controller
      if Rails.env.development?
        skip_before_action :authenticate_user!
      end
      before_action :check_auth

      def index
        respond_to do |format|
          format.json do
            if hotel_params[:search_criteria].present?
              check_in = Date.parse(hotel_params[:search_criteria][:check_in_date], '%d/m/y')
              check_out = Date.parse(hotel_params[:search_criteria][:check_out_date], '%d/m/y')
              no_of_rooms = hotel_params[:search_criteria][:rooms]
              guest_nationality = hotel_params[:search_criteria][:country]
              @guest_in_room = hotel_params[:guest_in_room]
              search_criteria = {        
                check_in: check_in,       
                check_out: check_out,
                city_id: hotel_params[:search_criteria][:city_id],
                no_of_rooms: no_of_rooms,
                guest_nationality: guest_nationality,
                room_guests: get_room_guests
              }             
              resp = HotelBooker[:tbo_holidays].search_hotels(search_criteria)
              if resp.success?    
                @booker_hotels = resp.hotel_result_list
                @booking_params = {
                  session_id: resp.session_id,
                  check_in: check_in,
                  check_out: check_out,
                  no_of_rooms: no_of_rooms,
                  guest_nationality: guest_nationality
                }

                @hotels = []
                @booker_hotels.each do |hotel|
                  tbo_hotel = TBOHotel.find_by_hotel_code(hotel.hotel_info.hotel_code)
                  if tbo_hotel.present?
                    if tbo_hotel.is_active
                      @hotels << hotel
                    end
                  else
                    @hotels << hotel
                  end
                end
        
                @general_markup = TBOHotel.general_markup
                @hotel_markups = TBOHotel.select(:hotel_code, :markup_amount,:markup_type).where.not(markup_amount:nil, hotel_code:nil).map{|x| [x.hotel_code, x.hotel_markup] }.to_h
              else
                render json: { error: "#{resp.message}" }
              end
            else
              render json: { error: "#{resp.message}" }
            end
          end
        end
      end

      def show
        respond_to do |format|
          format.json do
            resp = HotelBooker[:tbo_holidays].hotel_room_availability(params.slice(:session_id, :result_index, :hotel_code).permit!)   
            if resp.success?      
              @hotel_rooms     = resp.hotel_rooms
              @booking_options = resp.booking_options
              set_hotel
              @hotel_markup  = @hotel.markup_amount? ? @hotel.hotel_markup : TBOHotel.general_markup
              @room_guests = get_room_guests
              @guest_in_room = hotel_params[:guest_in_room]
              @check_in = hotel_params[:search_criteria][:check_in_date]
              @check_out = hotel_params[:search_criteria][:check_out_date]
              @session_id = params[:session_id]
              @result_index = params[:result_index]
              @hotel_code = params[:hotel_code]
              @city_id = hotel_params[:search_criteria][:city_id]
            else
              render json: { error: "#{resp.message}" }
            end
          end
        end
      end

      def room_policy
        resp_avail = HotelBooker[:tbo_holidays].availability_and_pricing(
          session_id: params[:session_id],
          result_index: params[:result_index],
          booking_options: booking_options
        )
        if resp_avail.success?
          if resp_avail.price_verification.available_on_new_price === "false" && resp_avail.price_verification.price_changed === "false"
            if resp_avail.available_for_book === true || resp_avail.available_for_confirm_book === true
              @available_for_confirm_book = resp_avail.available_for_confirm_book
              @cancellation_policies_available = resp_avail.cancellation_policies_available
              @cancellation_policies = resp_avail.hotel_cancellation_policies
              @selected_rooms = params[:selected_rooms]
            else
              render json: { error: "Room selected is unavailable for booking." }
            end
          else
            render json: { error: "New price is available for selected room." }
          end
        else
          render json: { error: "#{resp_avail.message}" }
        end 
      end

      def create_booking
        respond_to do |format|
          format.json do
            @booking_form = HotelBooker::BookingForm.new(booking_form_params.merge!(agent_user: current_user))
            @booking_form.client_reference_number = "#{Time.current.strftime("%d%m%y%H%M%S%3N")}#ICEH"
            resp, @booking = @booking_form.submit
            if resp
              @booking_payment = Payment.new(payment_params)
              if @booking_payment.save
                render json: { redirect_to: proceed_to_payment_path(:payment_id => @booking_payment.id) }
              else
                @booking_payment.failed_and_action
                render json: { error: "Your payment could not processed. Please try again." }
              end
            else
              render json: { error: "#{@booking_form.errors.full_messages.to_sentence}" }
              # redirect_to tbo_holidays_hotels_url(search_criteria: search_params, retry: true)
            end
          end
        end
      end

      def cities
        # Return cities of active countries
        cities = TBOCity.all_cached
        render json: { cities: cities }
      end

      def bookings
        respond_to do |format|
          format.json do
            @bookings = current_user.tbo_bookings.includes(:lead_guest).order(created_at: :desc)
          end
        end
      end

private
      def hotel_params
        search_criteria = params[:search_criteria]
        adults = []
        childs = []
        ages = []
        # beds = []
        guest_room = []
        room_data = params[:room_datas].values
        room_data.each_with_index do |all_rooms, i|
          rooms = all_rooms.select { |k,v| !v.blank? }
          rooms.each do |type, value|
            if type == "adult"
              adults << value
              (1..value.to_i).each do
                guest_room << i + 1
              end
            elsif type == "child"
              childs << value
              (1..value.to_i).each do
                guest_room << i + 1
              end
            else # child_ages
              all_ages = value.values.select{|i| i["age"].to_i > 0 && i["visible"] == "true"}
              if all_ages.count > 0
                all_ages.each do |data_age|
                  if data_age[:age].to_i > 0
                    ages << data_age[:age]
                  end
                end
              end
              # if type == "child_bed"
              #   beds << "true"
              # else
              #   beds << "false"
              # end
            end
          end

          # if only adult room present
          if !rooms.select{ |k,v| k.include?("child") }.present?
            childs << "0"
            ages << "1"
            # beds << "false"
          end
        end

        params = ActionController::Parameters.new({search_criteria: search_criteria, room_adults: adults, room_childs: childs, childs_ages: ages, guest_in_room: guest_room})
        # params = ActionController::Parameters.new({search_criteria: search_criteria, room_adults: adults, room_childs: childs, childs_ages: ages, childs_beds: beds, guest_in_room: guest_room})
        params
      end

      def get_room_guests
        child_ages  = hotel_params[:childs_ages].dup    
        room_guests = hotel_params[:room_adults].map.with_index do |x, idx|  
          child_age = child_ages.slice!(0...hotel_params[:room_childs][idx].to_i)
          HotelBooker::RoomGuest.new(adult_count:x, child_count:hotel_params[:room_childs][idx], child_age:child_age)
        end  
        room_guests
      end

      def booking_options
        HotelBooker::BookingOptions.new(
          fixed_format: true,
          room_combinations: HotelBooker::RoomCombination.new(room_index: params[:room_index].to_unsafe_h.map{|k,v| v})
        )
      end

      def booking_form_params
        params.require(:booking_form).permit(
          :result_index,
          :session_id,
          :hotel_code,
          :hotel_name,
          :hotel_address, 
          :hotel_description,
          :adult_count,
          :check_in,
          :check_out,
          :child_count,
          :city_id,
          :guest_nationality,
          :total_cost,
          :no_of_rooms,
          :cancellation_policies,            
          :remark,            
          hotel_rooms_attributes:[:room_index, :room_type_name, :room_type_code, :rate_plan_code,
            room_rate_attributes:[:room_fare, :currency, :room_tax, :total_fare, :agent_mark_up, :pref_currency, :pref_price],
            supplements_attributes:[:supp_id, :price, :supp_is_mandatory, :supp_charge_type, :currency_code]
          ],
          guests_attributes:[:guest_type, :lead_guest, :first_name, :last_name, :age, :title, :guest_in_room],
          address_info_attributes:[:address_line_1, :address_line_2, :country_code, :area_code, :phone_no, :email, :city, :state, :country, :zip_code]
        )
      end

      def payment_params
        params.require(:payment).permit(:gateway, :method).merge!(amount: @booking.total_fare.to_f, taggable: @booking, callback: 0, user_id: current_user.id)
      end

      def set_hotel
        hotel_code = params[:hotel_code] 
        @hotel = TBOHotel.find_by(hotel_code: hotel_code)
        if @hotel.present? && @hotel.is_active?
          @hotel_images = @hotel.image_urls.present? ? @hotel.image_urls : get_image_urls_from_api(hotel_code) # Get hotel images from API if not exists
        else                
          @hotel, @hotel_images = get_hotel_details_from_api(hotel_code, true) # Get hotel details from API and save it
        end
      end

      def get_hotel_details_from_api(hotel_code, save = false)
        resp = HotelBooker[:tbo_holidays].hotel_details(hotel_code: hotel_code)
        if resp.success?
          save ? [ TBOHotel.create(resp.hotel_details.to_h.except(:image_urls)), cache_image_urls!(hotel_code, resp.hotel_details.image_urls)] : resp.hotel_details
        else
          [nil, nil]
        end 
      end

      def get_image_urls_from_api(hotel_code)
        # Get images from redis cache and fetch from api and save if not exists
        Rails.cache.fetch("#{hotel_code}_cached_hotel_image_urls") do
          image_urls = get_hotel_details_from_api(hotel_code).image_urls          
          cache_image_urls!(hotel_code, image_urls)      
        end
      end
      def cache_image_urls!(hotel_code, image_urls)
        Rails.cache.write("#{hotel_code}_cached_hotel_image_urls", image_urls, expires_in: 2.days )
        image_urls
      end

      def check_auth
        authorize Hotel, :index?
      end
    end
  end
end
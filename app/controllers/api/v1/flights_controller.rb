module Api
  module V1
    class FlightsController < ApiV1Controller
      if Rails.env.development?
        skip_before_action :authenticate_user!
      end
      before_action :check_auth

      def index
        respond_to do |format|
          format.json do
            trips = params[:trips].values
            if params[:type].present? && params[:type] == 'round_trip'
              trips << {from: trips[0][:to], to: trips[0][:from], depart: trips[0][:return] }
            end
            params.merge!("trips": trips)
            @params_data = trips_params

            @solutions, @airlines = Travelport::AirSolution.where(trips_params)

          end
        end
      end

      def show
        respond_to do |format|
          format.json do
            trips = params[:trips].values
            if params[:type].present? && params[:type] == 'round_trip'
              trips << {from: trips[0][:to], to: trips[0][:from], depart: trips[0][:return] }
            end
            params.merge!("trips": trips)
            @solution = Travelport::AirSolution.find(trips_params)
          end
        end
      end

      def create_booking
        respond_to do |format|
          format.json do
            trips = params[:trips]
            if params[:type].present? && params[:type] == 'round_trip'
              trips << {from: trips[0][:to], to: trips[0][:from], depart: trips[0][:return] }
            end
            params.merge!("trips": trips)
            solution = Travelport::AirSolution.find(trips_params)

            if solution.present?
              # if (@new_domain && current_user.sufficient_credits?(solution.final_total_price + brb_prices)) || !@new_domain
                @flight_booking_group = Travelport::AirSolution.reservation({flight_booking_params: flight_booking_params, solution: solution, user: current_user, agent_rep: nil})
                if @flight_booking_group.present?
                  flight_payment = Payment.new(payment_params)
                  
                  if flight_payment.save
                    render json: { redirect_to: proceed_to_payment_path(:payment_id => flight_payment.id) }
                  else
                    @flight_payment.failed_and_action
                    render json: { error: "Your payment could not processed. Please try again." }
                  end
                else
                  render json: { error: 'The booking seats are no longer available. Sorry for inconvenient.' }
                end
              # else
              #   render json: { error: 'Insufficient credits, please purchase more credits.' }
              # end
            else
              render json: { error: 'The booking seats are no longer available. Sorry for inconvenient.' }
            end

          end
        end
      end

      def bookings
        @flight_booking_groups = current_user.flight_booking_groups.includes(:flight_booking_details, flight_bookings: [:agent_user]).order("flight_booking_groups.created_at desc ").page(params[:page]).per(BookingGroup::ADMIN_BOOKINGGROUPS_PER_PAGE)
      end

private

      def trips_params
        params.permit(:id, :adults, :children, :infants, :cabin_class, trips: [:from, :to, :depart])
      end

      def payment_params
        params.require(:payment).permit(:gateway, :method).merge!(amount: @flight_booking_group.price, taggable: @flight_booking_group, callback: 0, user_id: current_user.id)
      end

      def flight_booking_params
        params.require(:flight_bookings).map{|b| b.permit(:designation, :first_name, :last_name, :country, :mobile, :passport_number, :passport_issue_date, :passport_expiry_date, :date_of_birth, :category, :remark, :brb)}
      end

      def brb_count
        params["flight_bookings"].count{|booking| booking["brb"] == "Yes"}
      end

      def brb_prices
        brb_count * BrbBooking::PRICE
      end

      def check_auth
        authorize FlightBookingGroup, :index?
      end

    end
  end
end
class ActivitiesController < ApplicationController

  def index
    @countries = (GlobaltixTicket.countries + Activity.countries).uniq
    @globaltix_ticket_groups = GlobaltixTicket.search_country(params[:country]).search_title(params[:title]).active.group_by(&:attraction_id)
    @activities = Activity.available.visible.bookable.search_country(params[:country]).search_title(params[:title])
  end

  def show
    @globaltix_ticket = GlobaltixTicket.find(params[:id])
    @ticket = @globaltix_ticket.ticket
    # @ticket, ticket_err = Globaltix::Ticket.find(@globaltix_ticket.ticket_id)
    # @ticket.questions << Globaltix::Question.new(id: 123, question: 'This is Option', options: ['A', 'B'], type: {'name' => 'OPTION'})
    # @ticket.questions << Globaltix::Question.new(id: 124, question: 'This is Date', type: {'name' => 'DATE'})
    # @booking = Globaltix::Booking.new(tickets: [@ticket])
  end

  def new_booking
    @globaltix_tickets = GlobaltixTicket.where(attraction_id: params[:id])
    @globaltix_attraction = GlobaltixAttraction.find_by_attraction_id(@globaltix_tickets.first.attraction_id)
    @attraction, attraction_err = Globaltix::Attraction.find(@globaltix_attraction.attraction_id)
  end

  def create_booking
    @globaltix_tickets = GlobaltixTicket.where(id: params[:ticketTypes].map{|t| t[:id].to_i})
    
    price_qty = process_qty_params(@globaltix_tickets)
    # all_price = price_qty.map{ |k, v| (@globaltix_tickets.find(k).final_price * v) - (@globaltix_tickets.find(k).dta_amount * v) }.sum
    all_price = price_qty.map{ |k, v| (@globaltix_tickets.find(k).final_price(current_user.collect_dta?) * v) }.sum
    if price_qty.present?      
      if @globaltix_tickets.present? && current_user.sufficient_credits?(all_price)
        globaltix_ticket_booking_group = GlobaltixTicketBookingGroup.create_booking(@globaltix_tickets, price_qty, booking_ticket_params, current_user)
        if globaltix_ticket_booking_group.present?
          current_user.deduct_credits(globaltix_ticket_booking_group.price, globaltix_ticket_booking_group)
          redirect_to globaltix_ticket_booking_group_path(globaltix_ticket_booking_group.id), notice: 'Your booking was successfully created.'
        else
          redirect_to new_booking_activities_path(id: @globaltix_tickets.first.attraction_id), notice: 'The booking was failed to created, please book again. Sorry for inconvenient.'
        end
      else
        flash.discard
        flash[:error] = 'Insufficient credits, please purchase more credits.'
        render :show
      end
    else
    end
  end

private
  def price_qty_params
    qty_prices = process_qty_params(@globaltix_tickets)
    {'' => qty_prices}
  end

  def process_qty_params(globaltix_tickets)
    globaltix_tickets.map.with_index do |price, index|
        qty = params[:ticketTypes][index][:quantity]
        ["#{price.id}", qty.to_i]
    end.reject(&:blank?).to_h
  end

  def booking_ticket_params
    # merge index, fromResellerId, alternateEmail, paymentMethod
    new_params = params.permit(:email, :customerName, ticketTypes: [:id, :quantity, :visitDate, questionList: [:id, :answer]]).merge(paymentMethod: 'CREDIT')
    if new_params[:ticketTypes].present?

      # remove the invalid ticket
      new_params[:ticketTypes].delete_if{|ticket| @globaltix_tickets.detect{|gt| gt.id == ticket[:id].to_i}.blank?}

      new_params[:ticketTypes].each_with_index do |ticket, index|
        ticket[:id] = @globaltix_tickets.detect{|gt| gt.id == ticket[:id].to_i}.ticket_id
        ticket[:quantity] = ticket[:quantity].to_i
        ticket.merge!(index: index, fromResellerId: nil)
      end
    end
    new_params
  end
end
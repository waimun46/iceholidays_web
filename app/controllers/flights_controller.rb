class FlightsController < ApplicationController
  include TravelPortApi
  before_action :check_auth

  def index
    @starting_date = Date.today + 3.days

    if params[:trip_type].present? && params[:from].present? && params[:to].present? && (params[:depart].present? || params[:date_range].present?)
      @trips = []
      depart_return = []

      if params[:date_range].present? && params[:date_range].include?(' - ')
        depart_return = params[:date_range].split(' - ').map{|s| Date.parse(s)}
      end

      if params[:trip_type] == 'one_way' && params[:depart].present? && params[:depart].first.present? && Date.parse(params[:depart][0]) >= @starting_date
        @trips << {from: params[:from][0], to: params[:to][0], depart: Date.parse(params[:depart][0]).strftime("%F")}
      elsif params[:trip_type] == 'multi_city' && params[:depart].all?{|x| x.present? && Date.parse(x) >= @starting_date } 
        params[:from].count.times do |index|
          @trips << {from: params[:from][index], to: params[:to][index], depart: Date.parse(params[:depart][index]).strftime("%F")}
        end
      # elsif params[:trip_type] == 'round_trip' && depart_return.length == 2 && depart_return[0] >= @starting_date 
    elsif params[:trip_type] == 'round_trip' && params[:return][0].present? && Date.parse(params[:depart][0]) >= @starting_date && Date.parse(params[:return][0]) >= @starting_date
        from = params[:from][0]
        to = params[:to][0]

        @trips << {from: from, to: to, depart: Date.parse(params[:depart][0]).strftime("%F")}
        @trips << {from: to, to: from, depart: Date.parse(params[:return][0]).strftime("%F")}

        # @trips << {from: from, to: to, depart: depart_return[0].strftime('%F')}
        # @trips << {from: to, to: from, depart: depart_return[1].strftime('%F')}
      end

      
      # @solutions, @airlines = Travelport::AirSolution.where({trips: @trips, adults: params[:adults].to_i, children: params[:children].to_i, infants: params[:infants].to_i, cabin_class: params[:cabin_class]})
      # trips_params[:trips] = @trips
      # Rails.logger.info trips_params.merge!(trips: @trips)
      params.merge!("trips": @trips)

      @solutions, @airlines = Travelport::AirSolution.where(trips_params)
      if @solutions.present?

        if params[:directly_fly_only].present? && params[:directly_fly_only] == 'true'
          @solutions.delete_if {|s| s.journeys.map(&:stop).any?{ |stop| stop > 0}}
        end

        if params[:preferred_airlines].present?
          @solutions.delete_if do |s| 
            !params[:preferred_airlines].include?(s.airline.iata_code)
          end
        end

        if params[:sort_by] == 'Duration Shortest'
          @solutions.sort_by! do |s|
            s.journeys.map(&:travel_time).inject(&:+)
          end
        elsif params[:sort_by] == 'Departure Time Earliest'
          @solutions.sort_by!{|s| s.journeys.first.departure_segment.departure_time}
        elsif params[:sort_by] == 'Arrival Time Earliest'
          @solutions.sort_by!{|s| s.journeys.first.arrival_segment.arrival_time}
        else
          @solutions.sort_by! do |s|
            s.final_total_price
          end
        end
      end

      # @solutions_data = search_trips(@trips, params[:adults].to_i, params[:children].to_i, params[:infants].to_i, params[:cabin_class])
      # if @solutions_data.present?
        # if params[:directly_fly_only].present? && params[:directly_fly_only] == 'true'
        #   @solutions_data.solutions.delete_if {|s| s.Journeys.map{|j| j.AirSegments.count}.any?{|count| count > 1}}
        # end

        # if params[:preferred_airlines].present?

        #   @solutions_data.solutions.delete_if do |s| 
        #     !s.Journeys.map{|j| j.AirSegments.map(&:Carrier)}.flatten.uniq.all? {|e| params[:preferred_airlines].include?(e) }
        #   end
        # end

        # if params[:sort_by] == 'Duration Shortest'
        #   @solutions_data.solutions.sort_by! do |s|
        #     s.Journeys.map(&:TravelTime).inject(&:+)
        #   end
        # elsif params[:sort_by] == 'Departure Time Earliest'
        #   @solutions_data.solutions.sort_by!{|s| s.Journeys.first.AirSegments.first.DepartureTime}
        # elsif params[:sort_by] == 'Arrival Time Earliest'
        #   @solutions_data.solutions.sort_by!{|s| s.Journeys.first.AirSegments.last.ArrivalTime}
        # else
        #   @solutions_data.solutions.sort_by! do |s|
        #     s.markupTotalPrice
        #   end
        # end


      # end
    else
      @solutions = nil
    end
    if params[:trip_type].blank?
      params[:trip_type] = 'round_trip'
    end
    if @trips.blank?
      @trips = [{from: nil, to: nil, depart: nil, return: nil}]
    end
    
  end

  def show
    @date_today = Date.today
    # @solution = check_price_detail(trips_detail.as_json["trips"].map(&:symbolize_keys!), trips_detail[:id], trips_detail[:adults].to_i, trips_detail[:children].to_i, trips_detail[:infants].to_i, trips_detail[:cabin_class])

    # trips_detail = trips_params
    # @solution = Travelport::AirSolution.find({trips: trips_detail.as_json["trips"].map(&:symbolize_keys!), key: params[:id], adults: params[:adults].to_i, children: params[:children].to_i, infants: params[:infants].to_i, cabin_class: params[:cabin_class]})
    @solution = Travelport::AirSolution.find(trips_params)

    @payment = Payment.new
    # @banklist = @payment.fpx_banklist
    # @offline_banklist = @payment.offline_banklist(@banklist)

    if @solution.present?
      if @new_domain && !current_user.sufficient_credits?(@solution.final_total_price)
        flash[:warning] = "Reminder: Insufficient credits."
      end
      create_flight_bookings_array(@solution.air_pricing_infos.map(&:passenger_count).inject(:+))
    else
      flash[:error] = 'The flights are no longer available, please book again. Sorry for inconvenient.'
      flash.discard
    end
  end

  def create_booking
    trips_detail = trips_params
    @payment = Payment.new
    # @solution = check_price_detail(trips_detail.as_json["trips"].map(&:symbolize_keys!), trips_detail[:id], trips_detail[:adults].to_i, trips_detail[:children].to_i, trips_detail[:infants].to_i, trips_detail[:cabin_class])
    @solution = Travelport::AirSolution.find(trips_params)
    if @solution.present?

      # if (@new_domain && current_user.sufficient_credits?(@solution.markupTotalPrice)) || !@new_domain
      if (@new_domain && current_user.sufficient_credits?(@solution.final_total_price)) || !@new_domain
        # flight_booking_group = reservation(flight_booking_params, payment_params, trips_detail.as_json["trips"].map(&:symbolize_keys!), trips_detail[:id], current_user, nil, trips_detail[:adults].to_i, trips_detail[:children].to_i, trips_detail[:infants].to_i, trips_detail[:cabin_class])
        @flight_booking_group = Travelport::AirSolution.reservation({flight_booking_params: flight_booking_params, solution: @solution, user: current_user, agent_rep: nil})

        if @flight_booking_group.present?
          @flight_payment = Payment.new(payment_params)
          if @flight_payment.save
            if @new_domain
              redirect_to proceed_to_payment_path(:payment_id => @flight_payment.id)
            else
              @flight_booking_group.send_notify
              redirect_to flight_booking_group_path(@flight_booking_group.id), notice: 'Your booking was successfully created.'
              # redirect_to proceed_to_payment_path(:payment_id => @flight_payment.id)
            end
          else
            create_flight_bookings_array(@solution.AirPricings.map{|x| x.Count}.inject(&:+))
            @payment
            flash.discard
            flash[:error] = 'Failed to process payment. Please try again.'
            render :show
          end
        else
          redirect_to flights_path, notice: 'The booking seats are no longer available, please book again. Sorry for inconvenient.'
        end
      else
        create_flight_bookings_array(@solution.AirPricings.map{|x| x.Count}.inject(&:+))
        @payment
        flash.discard
        flash[:error] = 'Insufficient credits, please purchase more credits.'
        render :show
      end
    else
      redirect_to flights_path, notice: 'The booking seats are no longer available, please book again. Sorry for inconvenient.'
    end
  end

  def thankyou
    @flight_booking_group = current_user.flight_booking_groups.find(params[:flight_booking_group_id])
  end

private
  def trips_params
    params.permit(:id, :adults, :children, :infants, :cabin_class, trips: [:from, :to, :depart])
  end

  def flight_booking_params
    params.require(:flight_bookings).map{|b| b.permit(:designation, :first_name, :last_name, :country, :mobile, :passport_number, :passport_issue_date, :passport_expiry_date, :date_of_birth, :category, :remark)}
  end

  def payment_params
    params.require(:payment).permit(:gateway, :method).merge!(amount: @flight_booking_group.price, taggable: @flight_booking_group, callback: 0, user_id: current_user.id)
  end

  def create_flight_bookings_array(pax)
    @flight_bookings = []
    for i in 0..pax do
      if params[:flight_bookings].present? && flight_booking_params[i].present?
        @flight_bookings << FlightBooking.new(flight_booking_params[i])
      else
        @flight_bookings << FlightBooking.new
      end
    end
  end

  def check_auth
    authorize FlightBookingGroup, :index?
  end

end

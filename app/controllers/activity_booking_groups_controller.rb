class ActivityBookingGroupsController < ApplicationController
  def index
    authorize ActivityBookingGroup

    @today = DateTime.current
    if params[:begin].present? && params[:end].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      interval = DateTime.new.."#{params[:end]}".in_time_zone.end_of_day
    else
      interval = DateTime::Infinity.new...DateTime::Infinity.new
    end

    @activity_booking_groups = current_user.activity_booking_groups.includes(activity_bookings: [:agent_user]).in_between(interval).order("activity_booking_groups.created_at desc").page(params[:page]).per(30)
  end

  def show
    @activity_booking_group = current_user.activity_booking_groups.find(params[:id])
    @activity_booking = @activity_booking_group.activity_bookings.first
    authorize @activity_booking_group
  end

  def edit
    @activity_booking_group = current_user.activity_booking_groups.find(params[:id])
    @activity_booking = @activity_booking_group.activity_bookings.first
    authorize @activity_booking_group
  end

  def update
    @activity_booking_group = current_user.activity_booking_groups.find(params[:id])
    authorize @activity_booking_group
    @activity_booking_group.assign_attributes(update_booking_group_params)

    if @activity_booking_group.save
      redirect_to activity_booking_group_path(@activity_booking_group), notice: 'All bookings was successfully updated.'
    else
      render :edit
    end
  end

  def thank_you
    @activity_booking_group = current_user.activity_booking_groups.find(params[:id])
    @activity_booking = @activity_booking_group.activity_bookings.first
  end

  def invoice
    # @activity_booking_group = current_user.activity_booking_groups.find(params[:id])
    @activity_booking_group = ActivityBookingGroup.find(params[:id])
    authorize @activity_booking_group
    @activity_bookings = @activity_booking_group.activity_bookings.includes(:agent_user)
  end

private
  def update_booking_group_params
    new_params = params.require(:activity_booking_group).permit(:travel_date, activity_bookings_attributes: [:id, :name, :mobile, :email, :remark, :_destroy])
    if new_params[:activity_bookings_attributes].present?
      new_params[:activity_bookings_attributes].each {|k,v| v.merge!(last_edited_user_id: current_user.id)}
    end
    new_params.merge(last_edited_user_id: current_user.id)
  end
end
class PaymentsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:pbb_direct, :pbb_indirect,:fpx_direct, :fpx_indirect]
  skip_before_action :verify_authenticity_token, only: [:pbb_direct, :pbb_indirect,:fpx_direct, :fpx_indirect]
  
  def index
    @payments = current_user.payments.order("created_at desc").page(params[:page]).per(50)
  end

  # def new
  #   taggable = Payment.find_taggable(params[:item_name], params[:item_id])
  #   if taggable.present?
  #     @payment = Payment.new(taggable: taggable)
  #   else
  #     redirect_to activity_booking_groups_path
  #   end
  # end

  # def create
  #   @taggable = Payment.find_taggable(params[:payment][:item_name], params[:payment][:item_id])

  #   if @taggable.present?
  #     @payment = Payment.new(payment_params)

  #     if @payment.save
  #       redirect_to proceed_to_pbb_path(@payment), notice: 'Payment was successfully created.'
  #     else
  #       redirect_to new_payment_path(:item_name => @taggable.class.name, :item_id => @taggable.id)
  #     end
  #   else
  #     redirect_to new_payment_path(:item_name => @taggable.class.name, :item_id => @taggable.id)
  #   end
  # end

  def proceed_to_payment
    @payment = Payment.find(params[:payment_id])
    @taggable = @payment.taggable

    if @payment.gateway.include?("fpx")
      redirect_to proceed_to_fpx_path(:payment_id => @payment.id)
    elsif @payment.public_bank?
      redirect_to proceed_to_pbb_path(:payment_id => @payment.id)
    elsif @payment.credit_transaction?
      if current_user.sufficient_credits?(@payment.amount)
        current_user.deduct_credits(@payment.amount, @payment)
        @payment.paid_and_action!
        if @new_domain
          redirect_to app_path(anchor: "/payments/#{@payment.id}")
        else
          redirect_to @taggable.redirect_url, notice: 'Payment was successfully processed.'
        end
      else
        @payment.failed_and_action!('Insufficient credits, please purchase more credits.')
        flash.discard
        flash[:error] = 'Insufficient credits, please purchase more credits.'
        if @new_domain
          redirect_to app_path(anchor: "/payments/#{@payment.id}")
        else
          render :error_page
        end
      end
    elsif @payment.pay_later?
      if @payment.pay_later_available?
        @payment.paid_and_action!
        if @new_domain
          redirect_to app_path(anchor: "/payments/#{@payment.id}")
        else
          redirect_to @taggable.redirect_url, notice: 'The bookings is on hold.'
        end
      else
        @payment.failed_and_action!('The bookings are not allow to book now pay later.')
        flash.discard
        flash[:error] = 'The bookings are not allow to book now pay later.'
        if @new_domain
          redirect_to app_path(anchor: "/payments/#{@payment.id}")
        else
          render :error_page
        end
      end

    end
  end

  def proceed_to_pbb
    @payment = Payment.find(params[:payment_id])
    @merchant_id = @payment.pbb_merchant_id(@payment.method)
    @pbb_secret_code = Payment::SECRET_CODE
    @payment_url = Payment::PBB_URL
    @invoice_no = @payment.generate_invoice_no
    @amount = @payment.convert_pbb_amount
    @redirect_url = "#{request.protocol}#{request.host}#{@payment.redirect_url}"
    @security_key = @payment.encode_pbb_key(@invoice_no, @amount, @pbb_secret_code, @merchant_id)
  end

  def pbb_direct
    @response = params[:response]
    @invoice_no = params[:invoiceNo]
    @payment = Payment.find_by_invoice(@invoice_no)
    @taggable = @payment.taggable
    @pbb_secret_code = Payment::SECRET_CODE

    if @payment.pending? && Payment.pbb_verify(pbb_params, params[:securityKeyRes]) && @taggable.present?
      if @response == "00"
        @payment.paid_and_action!
        if @new_domain
          redirect_to app_path(anchor: "/payments/#{@payment.id}")
        else
          redirect_to @taggable.redirect_url
        end
      else
        @payment.failed_and_action!
        if @new_domain
          redirect_to app_path(anchor: "/payments/#{@payment.id}")
        else
          render :error_page
        end
      end
    else
      render :error_page
    end
  end

  def pbb_indirect
    @response = params[:response]
    @invoice_no = params[:invoiceNo]
    @payment = Payment.find_by_invoice(@invoice_no)
    @taggable = @payment.taggable
    @pbb_secret_code = Payment::SECRET_CODE

    if @payment.pending? && Payment.pbb_verify(pbb_params, params[:securityKeyRes]) && @taggable.present?
      if @response == "00"
        @payment.paid_and_action!
        if @new_domain
          redirect_to app_path(anchor: "/payments/#{@payment.id}")
        else
          redirect_to @taggable.redirect_url
        end
      else
        @payment.failed_and_action!
        if @new_domain
          redirect_to app_path(anchor: "/payments/#{@payment.id}")
        else
          render :error_page
        end
      end
    else
      if @new_domain
        redirect_to app_path(anchor: "/payments/#{@payment.id}")
      else
        render :error_page
      end
    end
  end

  def proceed_to_fpx
    @payment = Payment.find(params[:payment_id])

    @buyer_bank_id = @payment.method
    @buyer_email = @payment.taggable.buyer_email
    @seller_ex_id = Payment::FPX_SELLER_EX_ID
    @payment_url = Payment::FPX_URL
    @msg_token = Payment::FPX_MSG_TOKEN
    @msg_type = "AR"
    @version = Payment::FPX_VERSION
    @transaction_time = Time.now.in_time_zone.strftime("%Y%m%d%H%M%S")
    @invoice_no = @payment.generate_invoice_no
    @amount = "#{@payment.amount}.00"
    @amount = @payment.convert_fpx_amount
    @product_desc = @payment.taggable.product_description
    @seller_bankcode = "01"
    @seller_id = Payment::FPX_SELLER_ID
    @checksum = Payment.fpx_generate_checksum(fpx_params)   
  end

  def fpx_direct
    @checksum = params[:fpx_checkSum]
    payment = Payment.find_by_invoice(params[:fpx_sellerOrderNo])

    if payment.present? && Payment.fpx_verify_checksum(@checksum, fpx_direct_params) 
      if payment.pending?
        payment.fpx_update_status_and_action!(params[:fpx_debitAuthCode], params[:fpx_fpxTxnId])
      end
      render plain: "OK"
    else
      render plain: ""
    end
  end

  def fpx_indirect
    @payment = Payment.find_by_invoice(params[:fpx_sellerOrderNo])
    @taggable = @payment.taggable


    if @payment.paid?
      render :fpx_indirect
    elsif @payment.pending? && params[:fpx_debitAuthCode] == "00"
      # recheck the payment after 5 seconds
      sleep 5
      @payment = Payment.find_by_invoice(params[:fpx_sellerOrderNo])
      if @payment.paid?
        render :fpx_indirect
      else
        # if the payment is still pending, check the payment status from fpx
        if @payment.pending?
          @payment.fpx_status
          @payment = Payment.find_by_invoice(params[:fpx_sellerOrderNo])
          if @payment.paid?
            render :fpx_indirect
          else
            render :fpx_error
          end
        else
          render :fpx_error
        end
      end
    else
      render :fpx_error
    end

    # if @payment.pending? && @taggable.present? && @payment.fpx_update_status_and_action!(params[:fpx_debitAuthCode], params[:fpx_fpxTxnId])
    #   # fpx_debitAuthCode == 00 treat it as successful or fpx_debitAuthCode and fpx_creditAuthCode == 0 treat it as successful
    #   # if params[:fpx_debitAuthCode] == "00" && params[:fpx_creditAuthCode] == "00"
    #   if params[:fpx_debitAuthCode] == "00"
    #     # @taggable.send_notify
    #     render :fpx_indirect
    #     # redirect_to @taggable.redirect_url
    #   else
    #     render :fpx_error
    #   end
    # else
    #   render :fpx_error
    # end
  end

  def fpx_status
    @payment = Payment.find(params[:payment_id])
    @order_no = @payment.fpx_status
    @taggable = @payment.taggable
    redirect_to @taggable.show_url, notice: 'FPX payment status successfully updated.'
  end

private
  def payment_params
    params.require(:payment).permit(:method, :gateway, :callback).merge!(user_id: current_user.id, amount: @taggable.total_price, taggable: @taggable, redirect_url: pbb_direct_path)
  end

  def pbb_params
    {
      "authCode" => params[:authCode],
      "response" => @response,
      "invoiceNo" => @invoice_no,
      "secretCode" => @pbb_secret_code,
      "PAN" => params[:PAN],
      "expiryDate" => params[:expiryDate],
      "amount" => params[:amount]
    }
  end

  def fpx_params
    {
      "fpx_buyerAccNo" => "",
      "fpx_buyerBankBranch" => "",
      "fpx_buyerBankId" => @buyer_bank_id,
      "fpx_buyerEmail" => @buyer_email,
      "fpx_buyerIban" => "",
      "fpx_buyerId" => "",
      "fpx_buyerName" => "",
      "fpx_makerName" => "",
      "fpx_msgToken" => @msg_token,
      "fpx_msgType" => @msg_type,
      "fpx_productDesc" => @product_desc,
      "fpx_sellerBankCode" => @seller_bankcode,
      "fpx_sellerExId" => @seller_ex_id,
      "fpx_sellerExOrderNo" => @invoice_no,
      "fpx_sellerId" => @seller_id,
      "fpx_sellerOrderNo" => @invoice_no,
      "fpx_sellerTxnTime" => @transaction_time,
      "fpx_txnAmount" => @amount,
      "fpx_txnCurrency" => 'MYR',
      "fpx_version" => @version,
    }
  end

  def fpx_direct_params
    {
      "fpx_buyerBankBranch" => params[:fpx_buyerBankBranch], 
      "fpx_buyerBankId" => params[:fpx_buyerBankId], 
      "fpx_buyerIban" => params[:fpx_buyerIban], 
      "fpx_buyerId" => params[:fpx_buyerId], 
      "fpx_buyerName" => params[:fpx_buyerName], 
      "fpx_creditAuthCode" => params[:fpx_creditAuthCode], 
      "fpx_creditAuthNo" => params[:fpx_creditAuthNo], 
      "fpx_debitAuthCode" => params[:fpx_debitAuthCode],
      "fpx_debitAuthNo" => params[:fpx_debitAuthNo], 
      "fpx_fpxTxnId" => params[:fpx_fpxTxnId],
      "fpx_fpxTxnTime" => params[:fpx_fpxTxnTime],
      "fpx_makerName" => params[:fpx_makerName],
      "fpx_msgToken" => params[:fpx_msgToken],
      "fpx_msgType" => params[:fpx_msgType],
      "fpx_sellerExId" => params[:fpx_sellerExId],
      "fpx_sellerExOrderNo" => params[:fpx_sellerExOrderNo],
      "fpx_sellerId" => params[:fpx_sellerId],
      "fpx_sellerOrderNo" => params[:fpx_sellerOrderNo],
      "fpx_sellerTxnTime" => params[:fpx_sellerTxnTime],
      "fpx_txnAmount" => params[:fpx_txnAmount], 
      "fpx_txnCurrency" => params[:fpx_txnCurrency]
    }
  end
end
class UsersController < ApplicationController
  before_action :authenticate_user!, except: [:ktic_login]

  def edit
    @user = current_user
    authorize @user
  end

  def update_password
    @user = current_user
    authorize @user
    if @user.update_with_password(user_params)
      # Sign in the user by passing validation in case their password changed
      bypass_sign_in(@user)
      redirect_to root_path
    else
      render "edit"
    end
  end

  def ktic_login
    if user_signed_in? && current_user.username == 'ktic' && @new_domain
      redirect_to app_path
    elsif !user_signed_in? && params[:key].present? && @new_domain
      user = User.find_by_username('ktic')
      if user.key_authenticate?(params[:key])
        sign_in :user, user
        redirect_to app_path
      else
        redirect_to 'https://ktic.com.my/main.php?menu=home'
      end
    else
      redirect_to 'https://ktic.com.my/main.php?menu=home'
    end
  end

  private

  def user_params
    params.require(:user).permit(:current_password, :password)
  end
end
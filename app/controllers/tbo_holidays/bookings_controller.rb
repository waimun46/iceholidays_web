class TBOHolidays::BookingsController < TBOHolidays::BaseController  

  def index
    @bookings = current_user.tbo_bookings.includes(:lead_guest).order(created_at: :desc)
  end

  def show
    @booking = current_user.tbo_bookings.find(params[:id])
    update_details!(get_details) if @booking.booking_status.nil?
    update_cancellation_status!(get_cancellation_status) if check_booking_status?
    update_amendment_status! if check_amendment_status?
  end
  
  def new      
    resp = HotelBooker[:tbo_holidays].availability_and_pricing(
      session_id: booking_form_params[:session_id],
      result_index: booking_form_params[:result_index],
      booking_options: booking_options
    )
    if resp.success?

      # logger.debug resp.available_for_book
      # logger.debug resp.available_for_confirm_book
      # logger.debug resp.cancellation_policies_available
      # logger.debug resp.price_verification.hotel_rooms
      # logger.debug resp.account_info.inspect
      # logger.debug resp.hotel_details.inspect

      if resp.price_verification.available_on_new_price === "false" && resp.price_verification.price_changed === "false"
        if resp.available_for_book === true || resp.available_for_confirm_book === true
          @total_cost  = params[:total_cost]   

          guests_attributes, adult_count, child_count, = get_guests_attributes        
          @booking_form = HotelBooker::BookingForm.new(booking_form_params)
          @booking_form.cancellation_policies = helpers.render_room_cancellation_policies(resp.hotel_cancellation_policies)
       
          # hotel_rooms_attributes: params.permit!.to_h[:hotel_rooms_attributes],
          @booking_form.guests_attributes = guests_attributes
          @booking_form.adult_count = adult_count
          @booking_form.child_count = child_count
          @booking_form.address_info = HotelBooker::AddressInfo.new
        else
          flash[:error] = "Room selected is unavailable for booking."
          redirect_back fallback_location: tbo_holidays_hotels_url(search_criteria: search_params, retry: true)
        end
      else
        flash[:error] = "New price is available for selected room."
        redirect_back fallback_location: tbo_holidays_hotels_url(search_criteria: search_params, retry: true)
      end
    else
      flash[:error] = resp.message
      redirect_to tbo_holidays_hotels_url(search_criteria: search_params, retry: true)
    end    
  end

  def cancellation_policies
    resp = HotelBooker[:tbo_holidays].hotel_cancellation_policies(
      session_id: booking_form_params[:session_id],
      result_index: booking_form_params[:result_index],
      booking_options: booking_options
    )
    if resp.success?
    end
  end

  def create
    @booking_form = HotelBooker::BookingForm.new(booking_form_params)
    @booking_form.client_reference_number = "#{Time.current.strftime("%d%m%y%H%M%S%3N")}#ICEH"
    if resp = @booking_form.submit            
      redirect_to tbo_holidays_bookings_path, notice: "Your booking was successfully created"        
      # redirect_to admin_tbo_bookings_url, notice: "Your booking was successfully created"        
    else            
      flash[:error] = @booking_form.errors.full_messages.to_sentence
      render :new
      # redirect_to tbo_holidays_hotels_url(search_criteria: search_params, retry: true)
    end
  end

  def thankyou
    @booking = TBOBooking.find(params[:id])
  end

  def invoice
    @tbo_booking = TBOBooking.find(params[:id])
    @tbo_guests = @tbo_booking.guests
  end

private
  def booking_options
    HotelBooker::BookingOptions.new(
      fixed_format: params[:booking_options][:fixed_format],
      room_combinations: HotelBooker::RoomCombination.new(room_index: params[:booking_options][:room_combination])
    )
  end
    
  def get_guests_attributes
    guest_attributes = []
    total_child_count = 0
    total_adult_count = 0
    child_ages  = search_params[:childs_ages].dup    
    search_params[:room_adults].each.with_index do |x, idx| 
      adult_count = x.to_i   
      child_count = search_params[:room_childs][idx].to_i  
      total_adult_count += adult_count
      total_child_count += child_count            
      room_no =  idx + 1
      guest_attributes += (1..adult_count).map{|i| {guest_in_room:room_no, index: i, guest_type:'Adult', age: nil, lead_guest: room_no == 1 && i == 1  }}
      guest_attributes += (1..child_count).map{|i| {guest_in_room:room_no, index: i,guest_type:'Child', age: child_ages.shift, lead_guest: false}} unless child_count.zero?      
    end  
    [guest_attributes, total_adult_count, total_child_count]
  end

  def get_details
    HotelBooker[:tbo_holidays].booking_details(client_reference_number: @booking.reference_no) 
  end

  def get_cancellation_status    
    HotelBooker[:tbo_holidays].cancel?(confirmation_no: @booking.confirmation_no)
  end

  def update_details!(response)      
    if response.success?            
      detail = response.booking_detail
      @booking.booking_status = detail.booking_status
      @booking.voucher_status = detail.voucher_status      
      @booking.amendment_status = detail.amendment_status   
      @booking.save
    end
  end

  def update_cancellation_status!(response)       
    if response.success?    
      @booking.cancellation_status = response.request_status              
      if response.request_status == "Processed"
        exchange_rate = ExchangeRate.find_by(from_currency: 'USD', to_currency: 'MYR').try(:rate) || TBOHotel::USD_CONVERTED_TO_MYR
        refund_in_myr = (response.refund_amount.to_f * exchange_rate).ceil
        cancellation_charge_in_myr = (response.cancellation_charge.to_f * exchange_rate).ceil
        @booking.refund_amount = refund_in_myr
        @booking.cancellation_charge = cancellation_charge_in_myr
        @booking.booking_status = "Cancelled"
      end
    else
      @booking.cancellation_status = "UnProcessed"      
      flash[:error] = response.message
    end
    @booking.save
    # update_details!(get_details) 
  end

  def update_amendment_status!
    amendment_form = HotelBooker::AmendmentForm.new(
      booking: @booking,
      remarks: '?',
      type: HotelBooker::AmendmentForm::REQUEST_TYPES[:check_status]
    )       
     
    if response = amendment_form.submit   
      if @booking.amendment_request.persisted?
        @booking.amendment_request.update status: response.request_status
      end               
      @booking.update amendment_status: response.request_status
      # update_details!(get_details)
    end
    
  end
  
  def check_booking_status?
    @booking.CancellationInProgress?
  end

  def check_amendment_status?
    !@booking.Cancelled? && @booking.amendment_status.in?(%w(RequestSent Unprocessed InProgress AgentApprovalPending PendingWithSupplier))
  end
end
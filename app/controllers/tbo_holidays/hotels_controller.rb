class TBOHolidays::HotelsController < TBOHolidays::BaseController
  before_action :set_hotel, only: [:show]

  def index    
    if search_params_valid?
      check_in, check_out = check_in_out
      no_of_rooms = search_params[:rooms]
      guest_nationality = search_params[:country]
      filters = HotelBooker::HotelRequestFilters.new(order_by: search_params[:sort_by])
      search_criteria = {        
        check_in:   check_in,       
        check_out:  check_out,
        city_id:    search_params[:city_id],
        no_of_rooms: no_of_rooms,
        guest_nationality: guest_nationality,
        room_guests: get_room_guests,
        filters: filters
      }                  
      resp = HotelBooker[:tbo_holidays].search_hotels(search_criteria)
      if resp.success?    
        @booker_hotels = resp.hotel_result_list
        @booking_params = {
          session_id: resp.session_id,
          check_in: check_in,
          check_out: check_out,
          no_of_rooms: no_of_rooms,
          guest_nationality: guest_nationality
        }

        @hotels = []
        @booker_hotels.each do |hotel|
          tbo_hotel = TBOHotel.find_by_hotel_code(hotel.hotel_info.hotel_code)
          if tbo_hotel.present?
            if tbo_hotel.is_active
              @hotels << hotel
            end
          else
            @hotels << hotel
          end
        end

        @general_markup = TBOHotel.general_markup
        @hotel_markups = TBOHotel.select(:hotel_code, :markup_amount,:markup_type).where.not(markup_amount:nil, hotel_code:nil).map{|x| [x.hotel_code, x.hotel_markup] }.to_h
        flash.now[:success] = "#{resp.hotel_result_list.count} Result/s Found" unless params[:retry].present?
      else
        logger.info resp.message
        flash.now[:error] = "#{ resp.message } \n0 Results Found" 
      end
    end    
  end

  def show
    resp = HotelBooker[:tbo_holidays].hotel_room_availability(booking_form_params.slice(:session_id, :result_index, :hotel_code ))    
    if resp.success?      
      @hotel_rooms     = resp.hotel_rooms
      @booking_options = resp.booking_options

      @hotel_markup  = @hotel.markup_amount? ? @hotel.hotel_markup : TBOHotel.general_markup
      @booking_params = booking_form_params      
      # @hotel_details = TBOHotel.where(hotel_code: hotel_code ).first_or_initialize
    else
      flash[:error] = resp.message
      redirect_to tbo_holidays_hotels_url(search_criteria: search_params, retry: true)
    end    
  end

private

  def check_in_out
    return [Date.today + 1, Date.today + 2] unless search_params[:checkin_checkout].present?
    search_params[:checkin_checkout].split(' - ').map{|x| Date.parse(x, '%d/m/y')}
  end
  
  def get_room_guests
    child_ages  = search_params[:childs_ages].dup    
    room_guests = search_params[:room_adults].map.with_index do |x, idx|  
      child_age = child_ages.slice!(0...search_params[:room_childs][idx].to_i)
      HotelBooker::RoomGuest.new(adult_count:x, child_count:search_params[:room_childs][idx], child_age:child_age)
    end  
    room_guests
  end

  def set_hotel
    hotel_code = booking_form_params[:hotel_code] 
    @hotel = TBOHotel.find_by(hotel_code: hotel_code)
    if @hotel.present? && @hotel.is_active?
      @hotel_images = @hotel.image_urls.present? ? @hotel.image_urls : get_image_urls_from_api(hotel_code) # Get hotel images from API if not exists
    else                
      @hotel, @hotel_images = get_hotel_details_from_api(hotel_code, true) # Get hotel details from API and save it
    end
  end

  def get_hotel_details_from_api(hotel_code, save = false)
    resp = HotelBooker[:tbo_holidays].hotel_details(hotel_code: hotel_code)
    if resp.success?
      save ? [ TBOHotel.create(resp.hotel_details.to_h.except(:image_urls)), cache_image_urls!(hotel_code, resp.hotel_details.image_urls)] : resp.hotel_details
    else
      flash[:error] = resp.message
      redirect_to tbo_holidays_hotels_url(search_criteria: search_params, retry: true)
    end 
  end

  def get_image_urls_from_api(hotel_code)
    # Get images from redis cache and fetch from api and save if not exists
    Rails.cache.fetch("#{hotel_code}_cached_hotel_image_urls") do
      image_urls = get_hotel_details_from_api(hotel_code).image_urls    
      cache_image_urls!(hotel_code, image_urls)      
    end
  end

  def cache_image_urls!(hotel_code, image_urls) 
    Rails.cache.write("#{hotel_code}_cached_hotel_image_urls", image_urls, expires_in: 1.month )
    image_urls
  end
end

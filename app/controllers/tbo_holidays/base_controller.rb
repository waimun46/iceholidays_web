class TBOHolidays::BaseController < ApplicationController
  rescue_from Net::OpenTimeout, with: :redirect_to_search
  
  helper_method :search_params
  private
  def booking_form_params
    params.require(:booking_form).permit(
      :result_index,
      :session_id,
      :hotel_code,
      :hotel_name,
      :hotel_address, 
      :hotel_description,
      :adult_count,
      :check_in,
      :check_out,
      :child_count,
      :city_id,
      :guest_nationality,
      :total_cost,      
      :no_of_rooms,
      :cancellation_policies,            
      :last_cancellation_deadline,  
      hotel_rooms_attributes:[:room_index, :room_type_name, :room_type_code, :rate_plan_code,
        room_rate_attributes:[:room_fare, :currency, :room_tax, :total_fare, :agent_mark_up, :pref_currency, :pref_price],
        supplements_attributes:[:supp_id, :price, :supp_is_mandatory, :supp_charge_type, :currency_code]
      ],
      guests_attributes:[:guest_type, :lead_guest, :first_name, :last_name, :age, :title, :guest_in_room],
      address_info_attributes:[:address_line_1, :address_line_2, :country_code, :area_code, :phone_no, :email, :city, :state, :country, :zip_code]
    )
  end

  def search_params
    @search_params ||= params[:search_criteria].present? ? params[:search_criteria].permit!.to_h.deep_symbolize_keys : {}
  end

  def search_params_valid?
    search_params.present?
  end

  def redirect_to_search
    flash[:error] = 'Network Timeout'
    redirect_to tbo_holidays_hotels_url(search_criteria: search_params, retry: true)
  end
end
class RoamingmanPackagesController < ApplicationController
  def index
    keywords = params[:keywords].present? ? params[:keywords] : ''
    if params[:only_active_package].present? && params[:only_active_package] == 'false'
      only_active_package = false
    else
      only_active_package = true
    end
    @roamingman_packages = RoamingmanPackage.search(keywords).with_visible(only_active_package).page(params[:page]).per(50)
  end

  def show
    @roamingman_package = RoamingmanPackage.find(params[:id])
  end
end

class LandTourBookingsController < ApplicationController
  def new
    authorize LandTourBooking
    @land_tour = LandTour.find(params[:land_tour_id])
    @land_tour_booking_group = LandTourBookingGroup.new
    @land_tour_booking_group.land_tour_bookings << LandTourBooking.new
    @payment = Payment.new
    @banklist = @payment.fpx_banklist
    @offline_banklist = @payment.offline_banklist(@banklist)
  end

  def create
    authorize LandTourBooking
    @land_tour = LandTour.find(params[:land_tour_id])
    @payment = Payment.new
    @banklist = @payment.fpx_banklist
    @offline_banklist = @payment.offline_banklist(@banklist)

    price_qty = process_qty_params
    
    if price_qty.present?
      @land_tour_booking_group = LandTourBooking.bulk_create(@land_tour, price_qty, current_user, booking_params)
      if @land_tour_booking_group.id.present?
        @booking_payment = Payment.new(payment_params)
        if @booking_payment.save
          redirect_to proceed_to_payment_path(:payment_id => @booking_payment.id, :item_name => @land_tour_booking_group.class.name, :item_id => @land_tour_booking_group)
        else
          @payment
          render :new
        end
      else
        @payment
        render :new
      end
    end
  end

  def thankyou
    authorize LandTourBooking
    @land_tour = LandTour.visible.find(params[:land_tour_id])
    @land_tour_booking_group = current_user.land_tour_booking_groups.visible.find(params[:land_tour_booking_group_id])
  end

private
  def price_qty_params
    qty_prices = process_qty_params
    {'' => qty_prices}
  end

  def process_qty_params
    prices = []
    LandTour::PRICE_CATEGORY.each do |price|
      qty = params["qty_#{price}".to_sym]
      prices << ["#{price}", qty.to_i]
    end
    prices.reject(&:blank?).to_h
  end

  def booking_params
    params.require(:land_tour_booking_group).permit(:remark, land_tour_bookings_attributes: [:id, :name, :mobile, :email, :_destroy])
  end

  def payment_params
    if params[:payment][:gateway] == "public_bank"
      net_amount = @land_tour_booking_group.total_price - (@land_tour_booking_group.total_price * (Payment::PBB_CC_PERCENTAGE) / 100)
      redirect_url = pbb_direct_path
    elsif params[:payment][:gateway].include?("fpx")
      net_amount = @land_tour_booking_group.total_price
      redirect_url = fpx_direct_path
    else
      net_amount = @land_tour_booking_group.total_price
      redirect_url = nil
    end

    params.require(:payment).permit(:gateway, :method).merge!(amount: @land_tour_booking_group.total_price, taggable: @land_tour_booking_group, callback: 0, user_id: current_user, net_amount: net_amount, redirect_url: redirect_url)
  end
end
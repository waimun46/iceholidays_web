class BookingsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:cruise_options]

  # def index
  #   @bookings = Booking.includes(:tour).where(agent_user: current_user).visible.order("created_at desc").page(params[:page]).per(80)
  # end

  def new
    authorize Booking
    @tour = Tour.visible.find(params[:tour_id])
    create_bookings_array
  end

  def create
    authorize Booking
    @tour = Tour.visible.find(params[:tour_id])
    
    booking_seats = booking_seats_params

    if booking_seats.present? && current_user.present? && @tour.able_to_purcharse?(current_user, booking_seats)

      if !@tour.is_cruise? && !params[:bookings].any?{|b|b[:category]== 'adult'}
        create_bookings_array
        flash[:error] = 'The booking must at least have one adult.'
        flash.discard
        render :new
      else
        
        bookings = Booking.bulk_create(@tour, booking_seats, current_user, nil, params[:sub_sale_rep], booking_params)
        if bookings.present?
          redirect_to thankyou_tour_bookings_path(@tour, booking_group_id: bookings.first.booking_group_id)
          # redirect_to tour_path(@tour), notice: "#{bookings.length} #{'booking'.pluralize(bookings.length)} successfully created."
        else
          create_bookings_array
          flash[:error] = 'The booking seats are no longer available, please book again. Sorry for inconvenient.'
          flash.discard
          render :new
        end
      end
    else
      create_bookings_array
      flash[:error] = 'The booking seats are no longer available, please book again. Sorry for inconvenient.'
      flash.discard
      render :new
    end
  end

  def thankyou
    authorize Booking
    @tour = Tour.visible.find(params[:tour_id])
    @booking_group = current_user.booking_groups.visible.find(params[:booking_group_id])
  end

  def cruise_options
    authorize Booking
    respond_to do |format|
      format.json do
        @tour = Tour.visible.find(params[:tour_id])
        type = params[:type]
        if @tour.is_cruise? && (type.include?('single_') || type.include?('twin_')) && Tour::ROOM_TYPES.include?(type.sub('single_', '').sub('twin_', ''))
          avs = @tour.available_vacant_seats_sorted(current_user)
          if avs.has_key?(type)
            available_vacant_list = avs[type]
            process_seats_params(params[:booking_seats]).each do |price_type, seats|
              if available_vacant_list.has_key?(price_type)
                available_vacant_list[price_type][:seats] -= seats
              end
            end

            max_seats = if request.referer.include?('/admin/') && current_user.able_access_admin_dashboard?
              @tour.all_vacant_total_seats
            else
              @tour.max_booking_seats
            end

            render json: { html: helpers.render_booking_options(max_seats, type, available_vacant_list.select{|k,v| v[:seats] > 0}) }
          else
            render json: { html: '' }
          end
        else
          render json: { html: '' }
        end
      end
    end
  end

private
  def booking_seats_params
    if params[:booking_seats].class === 'String'
      booking_seats = process_seats_params(params[:booking_seats])
      {''=> booking_seats}
    elsif @tour.is_cruise? && params[:booking_seats].keys.length > 0
      booking_seats = {}
      params[:booking_seats].each do |type, value|
        data = process_seats_params(value)
        if data.present?
          booking_seats[type] = data
        end
      end
      booking_seats
    end
  end

  def process_seats_params(data_string)
    data_string.split(', ').map do |seat|
      # parsing "2 promo(188)"
      seat_match = seat.match /\A(\d+)\s(\w+)\(\d+\)/
      if seat_match.present? && seat_match.length == 3
        [seat_match[2], seat_match[1].to_i]
      else
        nil
      end
    end.reject(&:blank?).to_h
  end

  def booking_params
    params.require(:bookings).map{|b| b.permit(:category, :designation, :name, :date_of_birth, :passport_number, :visa_no, :remark, infant_bookings_attributes: [:id, :name, :date_of_birth, :gender])}
  end

  def create_bookings_array
    @bookings = []
    max_seats = if @tour.is_cruise?
      @tour.max_booking_seats * 4 - 1
    else
      @tour.max_booking_seats - 1
    end
    for i in 0..max_seats do
      if params[:bookings].present? && booking_params[i].present?
        @bookings << Booking.new(booking_params[i])
      else
        @bookings << Booking.new
      end
    end
  end
end
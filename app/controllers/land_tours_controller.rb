class LandToursController < AdminController
  before_action :check_auth

  def index
    keywords = params[:keywords].present? ? params[:keywords] : ''
    @land_tours = LandTour.visible.order("land_tours.created_at desc").page(params[:page]).per(50)
  end

  def show
    @land_tour = LandTour.includes(:covers).find(params[:id])
  end

private
  def check_auth
    authorize LandTour, :index?
  end
end

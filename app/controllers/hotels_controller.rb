class HotelsController < ApplicationController
  before_action :check_auth
  before_action :set_rate_rule, only: [:booking]

  def index
    if params[:search_fields].present? && params[:room_adults].present? && params[:room_childs].present? && params[:childs_ages].present? && params[:childs_beds].present?
      initial = params[:initial].present? ? params[:initial] : 'true'
      request_id = params[:request_id].present? ? params[:request_id] : ''

      @hotels, hotels_err, @search_criteria = TravelPrologue::Hotel.search(params[:search_fields], params[:room_adults].to_a, params[:room_childs].to_a, params[:childs_ages].to_a, params[:childs_beds].to_a, initial, request_id)

      if params[:search_fields][:sort_by].present? && params[:search_fields][:sort_by] == 'Lowest Price'
        @hotels.sort_by!{ |h| h.average_nightly_rate }
      end
    end
  end

  def details
    if params[:hotel].present?
      @room_booking_count = params[:hotel]["rooms"]
      @hotel_details = TravelPrologue::Hotel.getDetail(params[:hotel])

      if @hotel_details.present? && !@hotel_details.errors.present?
        @hotel = Hotel.find_by_tp_hotel_id(@hotel_details.tp_hotel_id)
        @hotel_images = @hotel_details.lst_hotel_images.map { |image| TravelPrologue::Image.new(image) } if @hotel_details.lst_hotel_images.present?

        if @hotel_details.lst_rooms.present?
          hotel_list_rooms = @hotel_details.lst_rooms[0]["LstRoomDetails"]

          if hotel_list_rooms.present?
            @room_list = hotel_list_rooms.map { |room| TravelPrologue::Room.new(room) }
            @rooms = @room_list.sort_by!{ |r| r.average_nightly_rate }
          end
        end
      else
        flash[:alert] = @hotel_details.errors
      end

    end
  end

  def booking
    if params[:hotel_bookings].present? && params[:request_code].present? && params[:avail_token].present?

      @hotel = Hotel.find_by_tp_hotel_id(@rate_rule.tp_hotel_id)
      @room_travellers = [params[:room_adults].map{|v| v.to_i}, params[:room_childs].map{|v| v.to_i}].transpose.map(&:sum)
      @room_adults = params[:room_adults].map{|v| v.to_i}
      @room_childs = params[:room_childs].map{|v| v.to_i}

      @payment = Payment.new
      # @banklist = @payment.fpx_banklist
      # @offline_banklist = @payment.offline_banklist(@banklist)

      if !@hotel.present?
        @hotel = Hotel.general_markup
      end

      all_travellers = params[:hotel_bookings][:adults].to_i + params[:hotel_bookings][:childs].to_i
      create_hotel_bookings_array(all_travellers)

    elsif request.post? && params[:hotel_booking_group].present? && params[:commit] == "Create Booking"

      @hotel_booking, @hotel_booking_group = HotelBookingGroup.create_booking(
        hotel_booking_group_params, 
        current_user, 
        @rate_rule.hotel_name, 
        @price_break_down.room_description, 
        @cancellation_policy.as_of_date, 
        @cancellation_policy.penalty_type_code == "FREE_CANCELLATION" ? true : false
      )

      if @hotel_booking.present? && @hotel_booking_group.present?
        if @hotel_booking.booking_status == "Success"
          @hotel_payment = Payment.new(payment_params)
          if @hotel_payment.save
            redirect_to proceed_to_payment_path(:payment_id => @hotel_payment.id)
          else
            redirect_to admin_hotel_booking_groups_path, alert: "Your booking was not created. #{@hotel_booking.error_msg}"
          end
        else
          redirect_to admin_hotel_booking_groups_path, alert: "Your booking was not created. #{@hotel_booking.error_msg}"
        end
      end

    end
  end

  private
    def hotel_bookings_params
      params.require(:hotel_bookings).map{|h| h.permit(:first_name, :last_name, :email, :country_code, :phone, :phone_number, :traveller_type, :salutation, :nationality, :in_room_no)}
    end

    def hotel_booking_group_params
      params.require(:hotel_booking_group).permit(:hotel_name, :hotel_address, :room, :avail_token, :unique_txn_id, :amount, :customer_name, :customer_email, :booking_status, :booking_id, :currency, :error_msg, :product_type, :booking_ref, :product_booking_id, :payment_id, :source_application, :cancellation_policy, :important_information, :additional_information, :check_in, :check_out, :cancel_upto, :free_cancellation,
        hotel_bookings: [:index, :salutation, :country_code, :first_name, :last_name, :email, :phone_number, :traveller_type, :in_room_no]
      )
    end

    def set_rate_rule
      @rate_rule, error = TravelPrologue::Hotel.getRateRules(params[:request_code])

      @price_break_down = TravelPrologue::PriceOption.new(@rate_rule.price_break_down[0])
      if @price_break_down.listpromotions.present?
        @promotions = @price_break_down.listpromotions.map { |promotion| TravelPrologue::Promotion.new(promotion) }
      end

      @cancellation_policy = TravelPrologue::CancellationPolicy.new(@price_break_down.cancellation_policy) if @price_break_down.cancellation_policy.present?
    end

    def create_hotel_bookings_array(travellers)
      @hotel_bookings = []

      for i in 0..travellers.to_i-1 do
        if params[:travellers].present? && hotel_bookings_params[i].present?
          @hotel_bookings << HotelBooking.new(hotel_bookings_params[i])
        else
          @hotel_bookings << HotelBooking.new
        end
      end
    end

    def payment_params
      @amount = @hotel_booking_group.amount.to_f
      params.require(:payment).permit(:gateway, :method).merge!(amount: @hotel_booking_group.amount.to_f, taggable: @hotel_booking_group, callback: 0, user_id: current_user.id)
    end

    def check_auth
      authorize Hotel, :index?
    end

end

class GiftTrackingGroupsController < ApplicationController
  def index
    authorize GiftTrackingGroup
    @gift_tracking_groups = current_user.gift_tracking_groups.order("created_at desc").page(params[:page]).per(50)
    @booking_groups = current_user.booking_groups
  end

  def show
    @gift_tracking_group = current_user.gift_tracking_groups.includes(:gift_trackings).find(params[:id])
    authorize @gift_tracking_group
    if params[:print].present?
      render :print
    else
      render :show
    end
  end

  def edit
    @gift_tracking_group = current_user.gift_tracking_groups.includes(:gift_trackings).find(params[:id])
    authorize @gift_tracking_group
  end

  def update
    @gift_tracking_group = current_user.gift_tracking_groups.includes(:gift_trackings).find(params[:id])
    authorize @gift_tracking_group

    if @gift_tracking_group.update(gift_tracking_params)
      redirect_to gift_tracking_group_path(@gift_tracking_group), notice: 'Gift tracking was successfully updated.'
    else
      render :edit
    end
  end

  def submit
    @gift_tracking_group = current_user.gift_tracking_groups.includes(:gift_trackings).find(params[:id])
    authorize @gift_tracking_group

    @gift_tracking_group.submitted!
    redirect_to gift_tracking_groups_path, notice: 'Gift redemption was successfully submitted.'
  end

  def acknowledge
    @gift_tracking_group = current_user.gift_tracking_groups.find(params[:id])
    authorize @gift_tracking_group
    
    if @gift_tracking_group.update(status_detail_params)
      @gift_tracking_group.received!
      redirect_to gift_tracking_groups_path, notice: 'Gift was successfully acknowledged as received.'
    else
      render :show
    end
  end

  def reject
    @gift_tracking_group = current_user.gift_tracking_groups.find(params[:id])
    authorize @gift_tracking_group
    
    if @gift_tracking_group.update(status_detail_params)
      @gift_tracking_group.rejected!
      redirect_to gift_tracking_groups_path, notice: 'Gift was rejected.'
    else
      render :show
    end
  end

private
  def gift_tracking_params
    params.require(:gift_tracking_group).permit(:agent_id, :cut_off_date, :category, :remark, gift_trackings_attributes: [:id, :point_topup, :booking_group_id, :_destroy, gift_tracking_details_attributes: [:id, :gift_id, :total_points, :quantity, :_destroy]])
  end

  def status_detail_params
    params.require(:gift_tracking_group).permit(:received_boxes, :reason)
  end

  def deliver_params
    params.require(:gift_tracking_group).permit(:total_boxes)
  end
end

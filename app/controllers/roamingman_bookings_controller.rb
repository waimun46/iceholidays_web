class RoamingmanBookingsController < ApplicationController
  def new
    @roamingman_package = RoamingmanPackage.find(params[:roamingman_package_id])
    @roamingman_booking_group = RoamingmanBookingGroup.new
    @roamingman_booking_group.roamingman_bookings << RoamingmanBooking.new
  end

  def create
    @roamingman_package = RoamingmanPackage.find(params[:roamingman_package_id])
    
    @roamingman_booking_group = RoamingmanBookingGroup.new(booking_params)
    if @roamingman_booking_group.save
      if @roamingman_booking_group.export_roamingman_create(current_user)
        redirect_to roamingman_booking_groups_path, notice: 'Roamingman Booking was successfully created and exported.'
      else
        redirect_to roamingman_booking_groups_path, notice: 'Roamingman Booking was failed to export.'
      end
    else
      render :new
    end
  end

private
  def booking_params
    new_params = params.require(:roamingman_booking_group).permit(:remark, roamingman_bookings_attributes: [:id, :name, :mobile, :email, :depart_date, :return_date, :quantity, :ship_way, :deliver_point_id, :address, :postcode, :is_send_sms, :is_send_email, :_destroy])
    if new_params[:roamingman_bookings_attributes].present?
      new_params[:roamingman_bookings_attributes].each {|k,v| v.merge!(roamingman_package_id: params[:roamingman_package_id], agent_user_id: current_user.id)}
    end
    new_params
  end
end

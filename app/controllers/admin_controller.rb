class AdminController < ApplicationController
  before_action :verify_dashboard_access

  private

  def verify_dashboard_access
    if current_user.blank?
      redirect_to new_user_session_url, alert: 'Not authorized to access. Please log in.'
    elsif !current_user.able_access_admin_dashboard?
      redirect_to root_url, alert: 'Not authorized to access.'
    end
  end

  def authorize(record, query = nil)
    super([:admin, record], query)
  end
end
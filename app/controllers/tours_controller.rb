class ToursController < ApplicationController
  before_action :check_auth

  def index
    @today = Date.today
    keywords = params[:keywords].present? ? params[:keywords] : ''

    if params[:begin].present?
      interval_start = "#{params[:begin]}".in_time_zone
      if interval_start < (@today+1.day)
        interval_start = (@today+1.day).beginning_of_day
      end
    else
      interval_start = (@today+1.day).beginning_of_day
    end

    if params[:end].present?
      interval_end = "#{params[:end]}".in_time_zone.end_of_day
    else
      interval_end = DateTime::Infinity.new
    end

    interval = interval_start..interval_end
    contient_country = params[:contient_country].present? ? params[:contient_country] : ''

    if params[:bookable].present? && params[:bookable] == 'false'
      @tours = Tour.visible.brand_filter(params[:brand]).search(keywords).destination(contient_country).in_between_departure(interval).page(params[:page]).per(Tour::TOURS_PER_PAGE).order(:departure_date, :code)
    else
      @tours = Tour.visible.brand_filter(params[:brand]).search(keywords).destination(contient_country).bookable(interval).page(params[:page]).per(Tour::TOURS_PER_PAGE).order(:departure_date, :code)
    end
  end

  def show
    if current_user.able_access_admin_dashboard?
      @tour = Tour.find(params[:id])
    else
      @tour = Tour.find(params[:id])
    end
    @bookings = @tour.bookings.visible.agent_user(current_user).order(:id)
    if params[:expired].present?
      flash[:error] = "The remaining booking time was expired, please book again. Sorry for inconvenient."
    end
  end

private
  def check_auth
    authorize Tour, :index?
  end

end
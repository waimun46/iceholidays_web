class Admin::GlobaltixTicketsController < AdminController
  before_action :check_auth
  def index
    @globaltix_tickets = GlobaltixTicket.search_status(params[:status])
  end

  def select
    @countries, countries_err = Globaltix::Country.all

    if params[:country_id].blank?
      params[:country_id] = 1
    end
    if params[:page].blank?
      params[:page] = 1
    end

    if params[:keyword].present?
      @attractions, attractions_err = Globaltix::Attraction.where(countryId: params[:country_id], page: params[:page], keyword: params[:keyword], field: :title)
    else
      @attractions, attractions_err = Globaltix::Attraction.where(countryId: params[:country_id], page: params[:page])
    end
  end

  def activities
    @tickets, tickets_err = Globaltix::Ticket.where(attraction_id: params[:attraction_id])
    render json: { html: helpers.render_admin_activity_tickets(@tickets) }
  end

  def new
    @ticket, ticket_err = Globaltix::Ticket.find(params[:ticket_id])
    @globaltix_attraction = GlobaltixAttraction.find_by_attraction_id(@ticket.attraction.id)
    @globaltix_ticket = GlobaltixTicket.new
  end

  def create
    @globaltix_ticket = GlobaltixTicket.new(globaltix_ticket_params)

    if @globaltix_ticket.save
      redirect_to admin_globaltix_tickets_path, notice: 'Globaltix ticket was successfully created.'
    else
      @ticket, ticket_err = Globaltix::Ticket.find(params[:globaltix_ticket][:ticket_id])
      render :new
    end
  end

  def show
    @globaltix_ticket = GlobaltixTicket.find(params[:id])
    @globaltix_attraction = GlobaltixAttraction.find_by_attraction_id(@globaltix_ticket.attraction_id)
  end

  def edit
    @globaltix_ticket = GlobaltixTicket.find(params[:id])
    @globaltix_attraction = GlobaltixAttraction.find_by_attraction_id(@globaltix_ticket.attraction_id)
  end

  def update
    @globaltix_ticket = GlobaltixTicket.find(params[:id])

    if @globaltix_ticket.update(globaltix_ticket_params)
      redirect_to admin_globaltix_tickets_path(@globaltix_ticket), notice: 'Globaltix ticket was successfully updated.'
    else
      render :edit
    end
  end



private
  def globaltix_ticket_params
    params.require(:globaltix_ticket).permit(:ticket_id, :attraction_id, :title, :name, :variation, :country, :currency, :price, :markup_category, :markup, :remark, :status, :globaltix_attraction_id, covers_attributes: [:id, :image, :_destroy])
  end

  def check_auth
    authorize GlobaltixTicket, :index?
  end
end
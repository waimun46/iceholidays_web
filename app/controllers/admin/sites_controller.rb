class Admin::SitesController < AdminController
  def dashboard
    @today = Date.today
    @current_active_users = Session.non_expired(Session.expired_time).count
  end

  def cover
    authorize Cover, :index?
    @covers = Cover.not_others.order(priority: :desc)
    if request.post?
      authorize Cover, :update?
      if Cover.create(cover_params)
        redirect_to admin_cover_path, notice: 'Cover was successfully updated.'
      else
        render :cover
      end
    elsif request.delete?
      authorize Cover, :update?
      cover = Cover.find(params[:id])
      cover.destroy
      redirect_to admin_cover_path, notice: 'Cover was successfully deleted.'
    end
  end

  def itinerary_tag
    respond_to do |format|
      format.json do
        if params[:term].blank?
          @itinerary_tags = Itinerary.all
        else
          @itinerary_tags = Itinerary.search(params[:term]).limit(30)
        end
      end
      format.html do
        redirect_to '/'
      end
    end
  end

  def fair_tag
    respond_to do |format|
      format.json do
        @fair_tags = Fair.search(params[:term]).limit(30)
      end
      format.html do
        redirect_to '/'
      end
    end
  end

  def tour_tag
    respond_to do |format|
      format.json do
        if params[:exclude_id].present? && params[:exclude_id].to_i != 0
          @tour_tags = Tour.search(params[:term]).where.not(id: params[:exclude_id].to_i).limit(20)
        else
          @tour_tags = Tour.search(params[:term]).limit(20)
        end
      end
      format.html do
        redirect_to '/'
      end
    end
  end

  def user_tag
    respond_to do |format|
      format.json do
        if params[:term].blank?
          @user_tags = User.all
        else
          @user_tags = User.search(params[:term]).limit(30)
        end
      end
      format.html do
        redirect_to '/'
      end
    end
  end

  def country_tag
    respond_to do |format|
      format.json do
        render json: ISO3166::Country.all.map(&:name)
      end
      format.html do
        redirect_to '/'
      end
    end
  end

private
  def cover_params
    params.require(:cover).permit(:image, :remote_image_url, :category, :priority)
  end

end
class Admin::InvoicesController < AdminController
  before_action :check_auth

  def index
    @invoices = Invoice.page(params[:page]).per(50)
  end

  def show
    @invoice = Invoice.find(params[:id])
    @bookings = Booking.where(id: @invoice.charged_booking_ids)
  end

private
  def check_auth
    authorize Invoice, :index?
  end

end
class Admin::SlidersController < AdminController
  before_action :check_auth

  def index
    @sliders = Slider.all.order_by_priority
  end

  def show
    @slider = Slider.find(params[:id])
    @slider_tabs = @slider.slider_tabs
  end

  def new
    @slider = Slider.new
  end

  def edit
    @slider = Slider.find(params[:id])
    @slider_tabs = @slider.slider_tabs
  end

  def create
    @slider = Slider.new(slider_params)

    if @slider.save
      redirect_to admin_sliders_path, notice: 'Slider was successfully created.'
    else
      render :new
    end
  end

  def update
    @slider = Slider.find(params[:id])

    if @slider.update(slider_params)
      redirect_to admin_slider_path(@slider), notice: 'Slider was successfully updated.'
    else
      render :edit
    end
  end

private
  def slider_params
    params.require(:slider).permit(:title, :category, :priority, :status, slider_tabs_attributes: [:id, :title, :priority, :_destroy, slider_cards_attributes: [:id, :priority, :link, :cover, :title1, :subtitle1, :highlight1, :logo1, :title2, :subtitle2, :highlight2, :logo2, :_destroy]])
  end

  def check_auth
    authorize Slider, :index?
  end
end
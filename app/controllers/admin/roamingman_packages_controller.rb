class Admin::RoamingmanPackagesController < AdminController
  before_action :check_auth

  def index
    keywords = params[:keywords].present? ? params[:keywords] : ''
    if params[:only_active_package].present? && params[:only_active_package] == 'false'
      only_active_package = false
    else
      only_active_package = true
    end
    @roamingman_packages = RoamingmanPackage.search(keywords).with_visible(only_active_package).page(params[:page]).per(50)
  end

  def show
    @roamingman_package = RoamingmanPackage.find(params[:id])
    @roamingman_bookings = @roamingman_package.roamingman_bookings.visible.order(created_at: :desc, id: :desc)
  end

  def new
    @roamingman_package = RoamingmanPackage.new
  end

  def create
    @roamingman_package = RoamingmanPackage.new(package_params)
    if @roamingman_package.save
      redirect_to admin_roamingman_packages_path, notice: 'Roamingman Package was successfully created.'
    else
      render :new
    end
  end

  def edit
    @roamingman_package = RoamingmanPackage.find(params[:id])
  end

  def update
    @roamingman_package = RoamingmanPackage.find(params[:id])

    if @roamingman_package.update(package_params)
      redirect_to admin_roamingman_package_path(@roamingman_package), notice: 'Roamingman Package was successfully updated.'
    else
      render :edit
    end
  end

  def new_booking
    @roamingman_package = RoamingmanPackage.find(params[:id])
    @roamingman_booking_group = RoamingmanBookingGroup.new
    @roamingman_booking_group.roamingman_bookings << RoamingmanBooking.new
  end

  def create_booking
    @roamingman_package = RoamingmanPackage.find(params[:id])
    
    @roamingman_booking_group = RoamingmanBookingGroup.new(booking_params)
    if @roamingman_booking_group.save
      if @roamingman_booking_group.export_roamingman_create(current_user)
        redirect_to admin_roamingman_booking_groups_path, notice: 'Roamingman Booking was successfully created and exported.'
      else
        redirect_to admin_roamingman_booking_groups_path, notice: 'Roamingman Booking was failed to export.'
      end
    else
      render :new_booking
    end
  end

private
  def check_auth
    authorize RoamingmanPackage, :index?
  end

  def booking_params
    new_params = params.require(:roamingman_booking_group).permit(:remark, roamingman_bookings_attributes: [:id, :name, :mobile, :email, :depart_date, :return_date, :quantity, :ship_way, :deliver_point_id, :address, :postcode, :is_send_sms, :is_send_email, :_destroy])
    if new_params[:roamingman_bookings_attributes].present?
      new_params[:roamingman_bookings_attributes].each {|k,v| v.merge!(roamingman_package_id: params[:id], agent_user_id: current_user.id)}
    end
    new_params
  end

  def package_params
    params[:roamingman_package][:countries] = params[:roamingman_package][:countries].split(',')
    params.require(:roamingman_package).permit(:code, :name, :category, :package_type, :data_rules, :channel_price, :retail_price, :min_rent_days, :highlight, :status, :terms_and_conditions, countries:[], covers_attributes: [:id, :image, :_destroy])
  end
end
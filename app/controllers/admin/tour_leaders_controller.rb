class Admin::TourLeadersController < AdminController
  before_action :check_auth
    
  def index
    @tour_leader = TourLeader.page(params[:page]).per(50)
  end

  def new
    @tour_leader = TourLeader.new
  end

  def show
    @tour_leader = TourLeader.find(params[:id])
  end

  def create
    @tour_leader = TourLeader.new(tour_leader_params)

    if @tour_leader.save
      redirect_to admin_tour_leaders_path, notice: 'Tour Leader was successfully created.'
    else 
      render :new
    end
  end

  def edit
    @tour_leader = TourLeader.find(params[:id])
  end

  def update
    @tour_leader = TourLeader.find(params[:id])

    if @tour_leader.update(tour_leader_params)
      redirect_to admin_tour_leader_path(@tour_leader), notice: 'Tour Leader was successfully updated.'
    else
      render :edit
    end
  end

private
  def tour_leader_params 
    languages = (params[:tour_leader][:languages].reject{|id| id.blank? })
    params.require(:tour_leader).permit(:first_name, :last_name, :date_of_birth, :mobile, :gender, :photo, :description, :room_type, :passport_copy, :passport_expiry_date, :passport_number, :passport_country, :insurance_confirmation, :insurance_expiry_date, :insurance_copy,  :emergency_number, :nationality, :address, :experience, :expertise, :booking_room_id, :designation, :visa_no, :visa_expiry_date).merge(languages: languages) 
  end

  def check_auth
    authorize TourLeader, :index?
  end
end
class Admin::FlightInventoriesController < AdminController
	before_action :check_auth

	def index
		@flight_inventory = FlightInventory.page(params[:page]).per(50)
	end

	def new
	  @flight_inventory = FlightInventory.new
	end

	def create
	  @flight_inventory = FlightInventory.new(flight_inventory_params)

	  if @flight_inventory.save
	    redirect_to admin_flight_inventories_path, notice: 'Flight Inventory was successfully created.'
	  else
	    render :new
	  end
	end

	def show
	  @flight_inventory = FlightInventory.find(params[:id])
	end

	def edit
	  @flight_inventory = FlightInventory.find(params[:id])
	end

	def update
	  @flight_inventory = FlightInventory.find(params[:id])

	  if @flight_inventory.update(flight_inventory_params)
	    redirect_to admin_flight_inventories_path, notice: 'Flight Inventory was successfully updated.'
	  else
	    render :edit
	  end
	end
private
	def flight_inventory_params
		params.require(:flight_inventory).permit(:title, :purchased_at, :purchased_by, :materialized_rate, :remark, reminders_attributes: [:id, :title, :description, :reminder_date, :email, :_destroy], flight_details_attributes: [:id, :airline_iata, :flight_no, :departure, :arrival, :origin, :destination, :_destroy], flight_seats_attributes: [:id, :total_seat, :pnr, :price, :tax, :fee, :bag_seat_fee, :remark, :_destroy, flight_receipts_attributes: [:id, :number, :amount, :remark, :paid_at, :_destroy]])
	end

  def check_auth
    authorize FlightInventory, :index?
  end
end

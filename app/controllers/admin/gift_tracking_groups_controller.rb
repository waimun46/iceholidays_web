class Admin::GiftTrackingGroupsController < AdminController
  before_action :check_auth

  def index
    @gift_tracking_groups = GiftTrackingGroup.includes(:gift_trackings).order("created_at desc").page(params[:page]).per(50)
  end

  def new
    @gift_tracking_group = GiftTrackingGroup.new
  end

  def create
    @gift_tracking_group = GiftTrackingGroup.new(gift_tracking_params)
    
    if @gift_tracking_group.save
      redirect_to admin_gift_tracking_groups_path, notice: 'Gift tracking was successfully created.'
    else
      render :new
    end
  end

  def show
    @gift_tracking_group = GiftTrackingGroup.includes(:gift_trackings).find(params[:id])
    if params[:print].present?
      render :print
    else
      render :show
    end
  end

  def edit
    @gift_tracking_group = GiftTrackingGroup.find(params[:id])
  end

  def update
    @gift_tracking_group = GiftTrackingGroup.find(params[:id])

    if @gift_tracking_group.update(gift_tracking_params)
      redirect_to admin_gift_tracking_groups_path, notice: 'Gift tracking was successfully updated.'
    else
      render :edit
    end
  end

  def deliver
    @gift_tracking_group = GiftTrackingGroup.find(params[:id])

    if @gift_tracking_group.update(deliver_params)
      @gift_tracking_group.delivering!
      redirect_to admin_gift_tracking_groups_path, notice: 'Gift status was successfully updated to delivering.'
    else
      render :show
    end
  end

  def tracking_summary
    @today = DateTime.current
    @agent_id = params[:agent_id].present? ? params[:agent_id] : ''

    if params[:begin].present? && params[:end].present?
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      @interval = DateTime.new.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:commit].blank?
      params[:begin] = (DateTime.now.beginning_of_month - 2.months).strftime("%F")
      params[:end] = DateTime.now.end_of_month.strftime("%F")
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    else
      @interval = DateTime::Infinity.new...DateTime::Infinity.new
    end
    
    @gift_tracking_groups = GiftTrackingGroup.in_between_tracking(@interval).search_agent(@agent_id).order("gift_tracking_groups.created_at desc").page(params[:page]).per(50)
  end

  def summary_detail
    @gift_tracking_group = GiftTrackingGroup.includes(:gift_trackings).find(params[:id])
  end

private
  def gift_tracking_params
    params.require(:gift_tracking_group).permit(:agent_id, :cut_off_date, :category, :remark, gift_trackings_attributes: [:id, :point_topup, :booking_group_id, :_destroy, gift_tracking_details_attributes: [:id, :gift_id, :total_points, :quantity, :_destroy]])
  end

  def deliver_params
    params.require(:gift_tracking_group).permit(:total_boxes)
  end

  def check_auth
    authorize GiftTrackingGroup
  end
end

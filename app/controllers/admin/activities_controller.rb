class Admin::ActivitiesController < AdminController
  before_action :check_auth

  def index
    @countries = (GlobaltixTicket.countries + Activity.countries).uniq
    @activities = Activity.search_country(params[:country]).search_title(params[:title]).page(params[:page]).per(50)
  end

  def new
    @activity = Activity.new
  end

  def create
    @activity = Activity.new(activity_params)

    if @activity.save
      redirect_to admin_activity_path(@activity), notice: 'Activity was successfully created.'
    else
      render :new
    end
  end

  def show
    @activity = Activity.includes(:covers).find(params[:id])
  end

  def edit
    @activity = Activity.includes(:covers).find(params[:id])
  end

  def update
    @activity = Activity.includes(:covers).find(params[:id])

    if @activity.update(activity_params)
      redirect_to admin_activity_path(@activity), notice: 'Activity was successfully updated.'
    else
      render :edit
    end
  end

private
  def activity_params
    if params[:activity][:booking_date_start].present? && params[:activity][:booking_date_end].present?
      booking_date = (params[:activity][:booking_date_start]).in_time_zone..(params[:activity][:booking_date_end]).in_time_zone
    else
      booking_date = nil
    end

    if params[:activity][:travelling_date_start].present? && params[:activity][:travelling_date_end].present?
      travelling_date = (params[:activity][:travelling_date_start]).in_time_zone..(params[:activity][:travelling_date_end]).in_time_zone
    else
      travelling_date = nil
    end

    # params[:activity][:operation_days] = params[:activity][:operation_days].split(',')
    operation_days = (params[:activity][:operation_days].reject{|id| id.blank? })
    categories = (params[:activity][:categories].reject{|id| id.blank? })
    languages = (params[:activity][:languages].reject{|id| id.blank? })

    params.require(:activity).permit(:title, :highlight, :description, :terms_and_conditions, :min_booking, :max_booking, :supplier, :country, :status, :duration, :cancellation, :e_ticketing, :travel_date_type, :ticket_type, :transfer, :meeting_point, :confirmation_time, :category, :advance_booking_days, activity_prices_attributes: [:id, :category, :code_type, :description, :terms_and_conditions, :cost_price, :price, :_destroy, :blockout_dates_text], covers_attributes: [:id, :image, :remote_image_url, :_destroy]).merge(booking_date: booking_date, travelling_date: travelling_date, operation_days: operation_days, categories: categories, languages: languages)
  end

  def check_auth
    authorize Activity, :index?
  end

end
class Admin::GlobaltixAttractionsController < AdminController
  before_action :check_auth
  def index
    @countries = GlobaltixAttraction.countries.uniq
    @globaltix_attractions = GlobaltixAttraction.search_country(params[:country]).search_title(params[:title]).page(params[:page]).per(50)
  end

  def select
    @countries, countries_err = Globaltix::Country.all

    if params[:country_id].blank?
      params[:country_id] = 1
    end
    if params[:page].blank?
      params[:page] = 1
    end

    if params[:keyword].present?
      @attractions, attractions_err = Globaltix::Attraction.where(countryId: params[:country_id], page: params[:page], keyword: params[:keyword], field: :title)
    else
      @attractions, attractions_err = Globaltix::Attraction.where(countryId: params[:country_id], page: params[:page])
    end
  end

  def new
    @attraction, attraction_err = Globaltix::Attraction.find(params[:attraction_id])
    @globaltix_attraction = GlobaltixAttraction.new
  end

  def create
    @globaltix_attraction = GlobaltixAttraction.new(globaltix_attraction_params)

    if @globaltix_attraction.save
      redirect_to admin_globaltix_attraction_path(@globaltix_attraction), notice: 'Globaltix attraction was successfully created.'
    else
      @attraction, attraction_err = Globaltix::Attraction.find(params[:attraction_id])
      render :new
    end
  end

  def add_ticket
    respond_to do |format|
      format.json do
        @globaltix_ticket = GlobaltixTicket.create_ticket(params[:id])
        if @globaltix_ticket.id.present? 
          render json: { notice: 'Globaltix ticket was successfully created.' }
        else
          render json: { error: 'Globaltix ticket already created.' }
        end
      end
    end
  end

  def show
    globaltix_tickets_array
    @attraction, attraction_err = Globaltix::Attraction.find(@globaltix_attraction.attraction_id)
  end

  def edit
    @globaltix_attraction = GlobaltixAttraction.find(params[:id])
    @globaltix_tickets = @globaltix_attraction.globaltix_tickets
    @tickets, tickets_err = Globaltix::Ticket.where(attraction_id: @globaltix_attraction.attraction_id)
    @attraction, attraction_err = Globaltix::Attraction.find(@globaltix_attraction.attraction_id)
  end

  def update
    @globaltix_attraction = GlobaltixAttraction.find(params[:id])

    if @globaltix_attraction.update(globaltix_attraction_params)
      redirect_to admin_globaltix_attraction_path(@globaltix_attraction), notice: 'Globaltix attraction was successfully updated.'
    else
      @attraction, attraction_err = Globaltix::Attraction.find(@globaltix_attraction.attraction_id)
      render :edit
    end
  end

private
  def globaltix_attraction_params
    languages = (params[:globaltix_attraction][:languages].reject{|id| id.blank? })
    attraction, attraction_err = Globaltix::Attraction.find(params[:globaltix_attraction][:attraction_id])

    params.require(:globaltix_attraction).permit(:attraction_id, :term_and_condition, :duration, :cancellation, :e_ticketing, :travel_date_type, :ticket_type, :transfer, :meeting_point, :confirmation_time, :category, :travelling_date, globaltix_tickets_attributes: [:id, :ticket_id, :attraction_id, :title, :name, :variation, :country, :markup_category, :markup, :blockout_dates_text,:remark, :status, :priority, :globaltix_attraction_id], covers_attributes: [:id, :image, :remote_image_url, :_destroy]).merge(languages: languages, country: attraction.country, title: attraction.title, description: attraction.description, hours_of_operation: attraction.hours_of_operation)
  end

  def globaltix_tickets_array 
    @globaltix_attraction = GlobaltixAttraction.includes(:covers).find(params[:id])
    @globaltix_tickets = @globaltix_attraction.globaltix_tickets
    @globaltix_tickets_array_id = []
    @globaltix_tickets.map { |v| @globaltix_tickets_array_id << v.ticket_id }
    @list_tickets, list_tickets_err = Globaltix::Ticket.where(attraction_id: @globaltix_attraction.attraction_id)
    @tickets = @list_tickets.reject { |x| @globaltix_tickets_array_id.include?(x.id) }
  end

  def check_auth
    authorize GlobaltixAttraction, :index?
  end
end

class Admin::PreCommisionsController < AdminController
  before_action :check_auth
  before_action :pre_set, only: [:edit, :update, :new, :create]
  def index
    @pre_commision = PreCommision.page(params[:page]).per(50)
  end

  def new
    @pre_commision = PreCommision.new
  end

  def create
    @pre_commision = PreCommision.new(pre_commision_params)

    if @pre_commision.save
      redirect_to admin_pre_commisions_path, notice: 'Pre Commision was successfully created.'
    else
      render :new
    end
  end

  def show
    @pre_commision = PreCommision.find(params[:id])
  end

  def edit
    @pre_commision = PreCommision.find(params[:id])
  end

  def update
    @pre_commision = PreCommision.find(params[:id]) 

    if @pre_commision.update(pre_commision_params)
      redirect_to admin_pre_commision_path(@pre_commision), notice: 'Pre Commision was successfully updated.'
    else
      render :edit
    end
  end

private
  def pre_commision_params
    if params[:pre_commision][:duration_start].present? && params[:pre_commision][:duration_end].present?
      duration = (params[:pre_commision][:duration_start]).in_time_zone..(params[:pre_commision][:duration_end]).in_time_zone
    else
      duration = nil
    end
    params[:pre_commision][:allow_user_ids] = params[:pre_commision][:allow_user_ids].map(&:to_i).reject{|id| id.zero? }
    params[:pre_commision][:allow_itinerary_ids] = params[:pre_commision][:allow_itinerary_ids].map(&:to_i).reject{|id| id.zero? }
    params.require(:pre_commision).permit(:code, :title, :remark, :active, :commision_category, :commisions, allow_user_ids:[], allow_itinerary_ids: []).merge(duration: duration)
  end

  def pre_set
    @users = User.all.order(:username)
    @states = @users.map(&:state).uniq
    @itineraries = Itinerary.visible.order(:id)
    # @departments = @itineraries.map(&:department).uniq
    @departments = Department.all + [nil]
  end

  def check_auth
    authorize Fair, :index?
  end
end

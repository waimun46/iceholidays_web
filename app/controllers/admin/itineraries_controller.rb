class Admin::ItinerariesController < AdminController
  before_action :check_auth, except: [:print_itinerary_detail]

  def index
    keywords = params[:keywords].present? ? params[:keywords] : ''
    department_id = params[:department_id].present? ? params[:department_id] : ''
    @itineraries = Itinerary.includes(:tours).visible.search(keywords).search_department_id(department_id).page(params[:page]).per(50)
  end

  def new
    @itinerary = Itinerary.new
  end

  def create
    @itinerary = Itinerary.new(itinerary_params)

    if @itinerary.save
      redirect_to admin_itineraries_path, notice: 'Itinerary was successfully created.'
    else
      render :new
    end
  end

  def duplicate
    itinerary = Itinerary.find(params[:id])
    if params[:code].present?
      if Itinerary.find_by_code(params[:code]).blank?
        new_itinerary = Itinerary.duplicate(itinerary, params[:code])
        redirect_to edit_admin_itinerary_path(new_itinerary)
      else
        redirect_to admin_itinerary_path(itinerary), notice: 'Tour code is duplicated.'
      end
    else
      redirect_to admin_itinerary_path(itinerary), notice: 'Please fill in the code to duplicate the itinerary.'
    end
  end

  def show
    @itinerary = Itinerary.find(params[:id])
    if params[:print].present? || params[:summary].present?
      @holidays = HolidayCalendar.where("date && tsrange(?, ?)", Time.now.midnight, Time.now.midnight + 365.day)
      render :summary
    else
      render :show
    end
  end

  def edit
    @itinerary = Itinerary.find(params[:id])

  end

  def update
    @itinerary = Itinerary.find(params[:id])

    if @itinerary.update(itinerary_params)
      redirect_to admin_itinerary_path(@itinerary), notice: 'Itinerary was successfully updated.'
    else
      render :edit
    end
  end

  def tour_status
    if params[:category].present?
      @holidays = HolidayCalendar.where("date && tsrange(?, ?)", Time.now.midnight, Time.now.midnight + 365.day)
      @itineraries = Itinerary.includes(:department, tours: [:tour_flights, :bookings]).with_tours(Date.today..DateTime::Infinity.new).order(:category, :code).order_by_code.order_by_departure.with_tour_status_category(params[:category])
      @is_four_season = params[:category].include?("Four Season")
      @department = Department.find_by_name(params[:category].sub("Four Season, ", "").sub("GD Standard, ", "").sub(", All", ""))
      render :summary_tour_status
    else
      render :tour_status
    end
  end

  def tour_summary
    if params[:begin].present? && params[:end].present?
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      @interval = Date.today.."#{params[:end]}".in_time_zone.end_of_day
    else
      @interval = Date.today...DateTime::Infinity.new
    end

    @itinerary = Itinerary.find(params[:id])
    @tours = @itinerary.tours.in_between_departure(@interval)
    authorize @itinerary
    render :print
  end

  def destroy
    itinerary = Itinerary.find(params[:id])
    if itinerary.present? && itinerary.tours.non_expired.count == 0
      itinerary.archived!
      redirect_to admin_itinerary_path, notice: 'Itinerary was successfully archived.'
    else
      redirect_to admin_itinerary_path, notice: 'Itinerary was failed to archived.'
    end
  end

private
  def itinerary_params
    if params[:itinerary][:temperature_start].present? && params[:itinerary][:temperature_start].present?
      temperature = (params[:itinerary][:temperature_start])..((params[:itinerary][:temperature_end]).to_i-1).to_s
    else
      temperature = nil
    end
    params[:itinerary][:countries] = params[:itinerary][:countries].split(',')
    params[:itinerary][:currency_english] =params[:itinerary][:currency_english].split(',')
    params[:itinerary][:currency_chinese] =params[:itinerary][:currency_chinese].split(',')
    # params[:itinerary][:countries] = params[:itinerary][:countries].reject{|v| v.blank?}

    params.require(:itinerary).permit(:zone,:code, :remark, :file, :remove_file, :remote_file_url, :cn_file, :remove_cn_file, :remote_cn_file_url, :editable_file, :remove_editable_file, :remote_editable_file_url, :other_editable_file, :remove_other_editable_file, :remote_other_editable_file_url, :en_editable_file, :remove_en_editable_file, :remote_en_editable_file_url, :cn_editable_file, :remove_cn_editable_file, :remote_cn_editable_file_url, :duration, :caption, :other_caption, :time_difference_english, :time_difference_chinese, :voltage, :tour_confirmation_remarks,  :optional_tour, :additional_tour, :description, :department_id, :category, :including_airport_taxes, :group_departure, :including_tour_leader, :including_luggage, :including_wifi, :including_meal_onboard, :including_hotel, :including_gratuities, :including_acf, covers_attributes: [:id, :image, :remote_image_url, :_destroy], countries:[], currency_english:[], currency_chinese:[]).merge(temperature: temperature)
  end

  def check_auth
    authorize Itinerary, :index?
  end
end
class Admin::SettingsController < AdminController

  def travelb2b
    # something
  end

  def edit_travelb2b
    # something
  end

  def update_travelb2b
    setting_params.keys.each do |key|
      Setting.send("#{key}=", setting_params[key].strip) unless setting_params[key].nil?
    end
    redirect_to travelb2b_admin_settings_path, notice: "Setting was successfully updated."
  end

  private
    def setting_params
      params.require(:setting).permit(:tagline, :activity_dta_markup, :hotel_dta_markup)
    end

end
class Admin::TBOBookingsController < AdminController
  before_action :set_booking, except:[:index]
  def index
    @bookings = TBOBooking.includes(:lead_guest).order(created_at: :desc)
  end

  def show
    update_details!(get_details) if @booking.booking_status.nil?
    update_cancellation_status!(get_cancellation_status) if check_booking_status?
    update_amendment_status! if check_amendment_status?
  end

  def cancel
    response = HotelBooker[:tbo_holidays].cancel!(confirmation_no: @booking.confirmation_no, remarks: 'Customer canceled booking.')
    @booking.cancellation_requested = true
    update_cancellation_status!(response)    
    redirect_to admin_tbo_booking_url(@booking)
  end

  private
  def set_booking
    @booking = TBOBooking.find(params[:id])
  end

  def get_details
    HotelBooker[:tbo_holidays].booking_details(client_reference_number: @booking.reference_no) 
  end

  def get_cancellation_status    
    HotelBooker[:tbo_holidays].cancel?(confirmation_no: @booking.confirmation_no)
  end

  def update_details!(response)      
    if response.success?            
      detail = response.booking_detail
      @booking.booking_status = detail.booking_status
      @booking.voucher_status = detail.voucher_status      
      @booking.amendment_status = detail.amendment_status   
      @booking.save
    end
  end


  def update_cancellation_status!(response)       
    if response.success?    
      @booking.cancellation_status = response.request_status              
      if response.request_status == "Processed"
        exchange_rate = ExchangeRate.find_by(from_currency: 'USD', to_currency: 'MYR').try(:rate) || TBOHotel::USD_CONVERTED_TO_MYR
        refund_in_myr = (response.refund_amount.to_f * exchange_rate).ceil
        cancellation_charge_in_myr = (response.cancellation_charge.to_f * exchange_rate).ceil
        @booking.refund_amount = refund_in_myr
        @booking.cancellation_charge = cancellation_charge_in_myr
        @booking.booking_status = "Cancelled"
      end
    else
      @booking.cancellation_status = "UnProcessed"      
      flash[:error] = response.message
    end
    @booking.save
    # update_details!(get_details) 
  end

  def update_amendment_status!
    amendment_form = HotelBooker::AmendmentForm.new(
      booking: @booking,
      remarks: '?',
      type: HotelBooker::AmendmentForm::REQUEST_TYPES[:check_status]
    )       
     
    if response = amendment_form.submit   
      if @booking.amendment_request.persisted?
        @booking.amendment_request.update status: response.request_status
      end               
      @booking.update amendment_status: response.request_status
      # update_details!(get_details)
    end
    
  end
  
  def check_booking_status?
    @booking.CancellationInProgress?
  end

  def check_amendment_status?
    !@booking.Cancelled? && @booking.amendment_status.in?(%w(RequestSent Unprocessed InProgress AgentApprovalPending PendingWithSupplier))
  end

end

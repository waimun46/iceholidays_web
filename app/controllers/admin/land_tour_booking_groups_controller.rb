class Admin::LandTourBookingGroupsController < AdminController
  before_action :check_auth

  def index
    @today = DateTime.current

    if params[:begin].present? && params[:end].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      interval = DateTime.new.."#{params[:end]}".in_time_zone.end_of_day
    else
      interval = DateTime::Infinity.new...DateTime::Infinity.new
    end

    if params[:begin_departure_date].present? && params[:end_departure_date].present?
      interval_departure_date = "#{params[:begin_departure_date]}".in_time_zone.beginning_of_day.."#{params[:end_departure_date]}".in_time_zone.end_of_day
    elsif params[:begin_departure_date].present?
      interval_departure_date = "#{params[:begin_departure_date]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end_departure_date].present?
      interval_departure_date = DateTime.new.."#{params[:end_departure_date]}".in_time_zone.end_of_day
    else
      interval_departure_date = DateTime::Infinity.new...DateTime::Infinity.new
    end

    booked_by = params[:booked_by].present? ? params[:booked_by] : ''


    @land_tour_booking_groups = LandTourBookingGroup.in_between_booked(interval).in_between_departure_date(interval_departure_date).search_agent(booked_by).order("land_tour_booking_groups.created_at desc").page(params[:page]).per(50)
  end

  def show
    @land_tour_booking_group = LandTourBookingGroup.includes(:land_tour_bookings).find(params[:id])
    @land_tour = @land_tour_booking_group.land_tour
  end

  def edit
    @land_tour_booking_group = LandTourBookingGroup.includes(:land_tour_bookings).find(params[:id])
    @land_tour = @land_tour_booking_group.land_tour
  end

  def update
    @land_tour_booking_group = LandTourBookingGroup.find(params[:id])
    @land_tour_booking_group.assign_attributes(update_booking_group_params)

    if @land_tour_booking_group.save
      redirect_to admin_land_tour_booking_group_path(@land_tour_booking_group), notice: 'All bookings was successfully updated.'
    else
      render :edit
    end
  end

  def resend_confirmation
    @land_tour_booking_group = LandTourBookingGroup.find(params[:id])
    @land_tour_booking_group.send_notify(true)
    redirect_to admin_land_tour_booking_group_path(@land_tour_booking_group), notice: 'Confirmation email successfully sent.'
  end

  def new_withdraw
    @land_tour_booking_group = LandTourBookingGroup.includes(:land_tour_bookings).find(params[:id])
    @land_tour = @land_tour_booking_group.land_tour
  end

  def create_withdraw
    @land_tour_booking_group = LandTourBookingGroup.find(params[:id])
    @land_tour = @land_tour_booking_group.land_tour
    alter_bookings = LandTourBooking.where(id: params[:booking_ids].split(','))

    if alter_bookings.blank?
      flash[:error] = "Please select the bookings for withdraw."
      render :new_withdraw
    else
      @withdraw = LandTourBookingGroup.bulk_withdraw(alter_bookings, current_user)
      redirect_to admin_land_tour_booking_group_path(@land_tour_booking_group), notice: 'All bookings was successfully withdrawn.'
    end
  end

  def voucher
    @land_tour_booking_group = LandTourBookingGroup.includes(:land_tour_bookings).find(params[:id])
    @land_tour = @land_tour_booking_group.land_tour
    @land_tour_booking = @land_tour_booking_group.land_tour_bookings.first
    @user = @land_tour_booking_group.agent_user
  end

private
  def update_booking_group_params
    params[:land_tour_booking_group][:special_request] = params[:land_tour_booking_group][:special_request].split(',')

    new_params = params.require(:land_tour_booking_group).permit(:rooms, :language, :hotel_name, :signboard_text, :flight_departure_details, :flight_return_details, :remark, special_request: [], land_tour_bookings_attributes: [:id, :price, :category, :designation, :name, :date_of_birth, :mobile, :nationality, :remark, :_destroy])
    if new_params[:land_tour_bookings_attributes].present?
      new_params[:land_tour_bookings_attributes].each {|k,v| v.merge!(last_edited_user_id: current_user.id)}
    end
    new_params.merge(last_edited_user_id: current_user.id)
  end

  def check_auth
    authorize LandTourBookingGroup
  end
end
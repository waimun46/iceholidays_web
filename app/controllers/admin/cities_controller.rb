class Admin::CitiesController < AdminController
  def index
    respond_to do |format|
      format.json do
        @cities = City.select(:id, :name, :country)
      end
    end
  end
end
class Admin::AirlinesController < AdminController
  before_action :check_auth

  def index
    keywords = params[:keywords].present? ? params[:keywords] : ''
    @airlines = Airline.search(keywords).page(params[:page]).per(50)
  end

  def new
    @airline = Airline.new
  end

  def create
    @airline = Airline.new(airline_params)

    if @airline.save
      redirect_to admin_airlines_path, notice: 'Airline was successfully created.'
    else
      render :new
    end
  end

  def show
    @airline = Airline.find(params[:id])
  end

  def edit
    @airline = Airline.find(params[:id])
  end

  def update
    @airline = Airline.find(params[:id])

    if @airline.update(airline_params)
      redirect_to admin_airlines_path, notice: 'Airline was successfully updated.'
    else
      render :edit
    end
  end

private
  def airline_params
    params.require(:airline).permit(:name, :iata_code, :country, :economy_markup_category, :business_markup_category, :first_markup_category, :economy_markup, :business_markup, :first_markup, :logo, :remote_logo_url, :block)
  end

  def check_auth
    authorize Airline, :index?
  end

end
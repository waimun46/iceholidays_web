class Admin::EtravelInvoicesController < AdminController

  def index
    keywords = params[:keywords].present? ? params[:keywords] : ''
    @etravel_invoices = EtravelInvoice.search(keywords).order("created_at desc").order("taggable_type desc").page(params[:page]).per(EtravelInvoice::ADMIN_PER_PAGE) 
  end
  
private
   def check_auth

  end
end

class Admin::FairsController < AdminController
  before_action :check_auth
  before_action :pre_set, only: [:edit, :update, :new, :create]

  def index
    keywords = params[:keywords].present? ? params[:keywords] : ''
    @fairs = Fair.search(keywords).order(duration: :desc).page(params[:page]).per(50)
  end

  def new
    @fair = Fair.new
  end

  def create
    @fair = Fair.new(fair_params)

    if @fair.save
      redirect_to admin_fairs_path, notice: 'Fair was successfully created.'
    else
      render :new
    end
  end

  def show
    @fair = Fair.find(params[:id])
  end

  def edit
    @fair = Fair.find(params[:id])
  end

  def update
    @fair = Fair.find(params[:id])

    if @fair.update(fair_params)
      redirect_to admin_fair_path(@fair), notice: 'Fair was successfully updated.'
    else
      render :edit
    end
  end

private
  def fair_params
    if params[:fair][:duration_start].present? && params[:fair][:duration_end].present?
      duration = (params[:fair][:duration_start]).in_time_zone..(params[:fair][:duration_end]).in_time_zone
    else
      duration = nil
    end
    params[:fair][:allow_user_ids] = params[:fair][:allow_user_ids].map(&:to_i).reject{|id| id.zero? }
    params[:fair][:allow_itinerary_ids] = params[:fair][:allow_itinerary_ids].map(&:to_i).reject{|id| id.zero? }
    params.require(:fair).permit(:code, :title, :other_title, :remark, :active, :messages, :internal_reference, :fair_type, allow_user_ids:[], allow_itinerary_ids:[]).merge(duration: duration)
  end

  def pre_set
    @users = User.all.order(:username)
    @states = @users.map(&:state).uniq
    @itineraries = Itinerary.includes(:department).visible.order(:code)
    # @departments = @itineraries.map(&:department).uniq
    @departments = Department.all + [nil]
  end

  def check_auth
    authorize Fair, :index?
  end

end
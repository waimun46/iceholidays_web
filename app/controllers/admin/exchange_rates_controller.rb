class Admin::ExchangeRatesController < AdminController
  before_action :check_auth

  def index
    @exchange_rates = ExchangeRate.all
  end

  def new
    @currencies = Money::Currency.map(&:iso_code)
    @exchange_rate = ExchangeRate.new
  end

  def create
    @currencies = Money::Currency.map(&:iso_code)
    @exchange_rate = ExchangeRate.new(exchange_rate_params)

    if @exchange_rate.save
      redirect_to admin_exchange_rates_path, notice: 'Exchange rate was successfully created.'
    else
      render :new
    end
  end

  def show
    @exchange_rate = ExchangeRate.find(params[:id])
  end

  def edit
    @currencies = Money::Currency.map(&:iso_code)
    @exchange_rate = ExchangeRate.find(params[:id])
  end

  def update
    @currencies = Money::Currency.map(&:iso_code)
    @exchange_rate = ExchangeRate.find(params[:id])

    if @exchange_rate.update(exchange_rate_params)
      redirect_to admin_exchange_rates_path, notice: 'Exchange rate was successfully updated.'
    else
      render :edit
    end
  end

private
  def exchange_rate_params
    params.require(:exchange_rate).permit(:from_currency, :to_currency, :rate)
  end

  def check_auth
    authorize ExchangeRate, :index?
  end

end
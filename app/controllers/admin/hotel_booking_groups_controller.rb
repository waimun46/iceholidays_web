class Admin::HotelBookingGroupsController < AdminController
  before_action :check_auth

  def index
    @hotel_booking_groups = HotelBookingGroup.all.order(created_at: :desc)
  end

  def show
    @hotel_booking_group = HotelBookingGroup.find(params[:id])
  end

  private
    def check_auth
      authorize HotelBookingGroup, :index?
    end

end

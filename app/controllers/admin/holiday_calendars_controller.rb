class Admin::HolidayCalendarsController < AdminController

before_action :check_auth

def index
  @holiday_calendar = HolidayCalendar.order(date: :asc).page(params[:page]).per(50)
end

def new
  @holiday_calendar = HolidayCalendar.new
end

def create
  @holiday_calendar = HolidayCalendar.new(holiday_calendar_params)

  if @holiday_calendar.save
    redirect_to admin_holiday_calendars_path, notice: 'Holiday Calendars was successfully created.'
  else
    render :new
  end
end

def show
  @holiday_calendar = HolidayCalendar.find(params[:id])
end

def edit
  @holiday_calendar = HolidayCalendar.find(params[:id])
end

def update
  @holiday_calendar = HolidayCalendar.find(params[:id])

  if @holiday_calendar.update(holiday_calendar_params)
    redirect_to admin_holiday_calendar_path(@holiday_calendar), notice: 'Holiday Calendars was successfully updated.'
  else  
    render :edit
  end
end

private
def holiday_calendar_params
if params[:holiday_calendar][:date_start].present? && params[:holiday_calendar][:date_end].present?
  date = (params[:holiday_calendar][:date_start]).in_time_zone..(params[:holiday_calendar][:date_end]).in_time_zone+1.days
else
  date = nil
end
  params.require(:holiday_calendar).permit(:name).merge(date: date)
end

def check_auth
   authorize HolidayCalendar, :index?
end
end


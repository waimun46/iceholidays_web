class Admin::FlightCommissionsController < AdminController
  before_action :check_auth

  def index
    @flight_commission = FlightCommission.page(params[:page]).per(50)
  end

  def new
    @flight_commission = FlightCommission.new
  end

  def show
    @flight_commission = FlightCommission.find(params[:id])
  end

  def create
    @flight_commission = FlightCommission.new(flight_commission_params)

    if @flight_commission.save
      redirect_to admin_flight_commissions_path, notice: 'Flight Commission was successfully created.'
    else
      render :new
    end
  end

  def edit
    @flight_commission = FlightCommission.find(params[:id])
  end

  def update
    @flight_commission = FlightCommission.find(params[:id])

    if @flight_commission.update(flight_commission_params)
      redirect_to admin_flight_commissions_path, notice: 'Flight Commission was successfully updated.'
    else
      render :edit
    end
  end

private
  def flight_commission_params 
    params.require(:flight_commission).permit( :name, :airline_iata, :origin_airport_iata, :destination_airport_iata, :commission, :commission_category) 
  end

  def check_auth
    authorize FlightCommission, :index?
  end
end


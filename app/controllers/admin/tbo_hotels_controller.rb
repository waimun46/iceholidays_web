class Admin::TBOHotelsController < AdminController  
  before_action :set_hotel, only: [:show, :edit, :update]
  before_action :set_hotel_details, only: [:edit]

  def index
    @general_mark_up = TBOHotel.general_markup_holder
    @hotels = TBOHotel.search_by_hotel(params[:hotel_keyword]).search_by_city(params[:city_name_keyword], params[:city_code_keyword]).search_by_rating(params[:hotel_rating]).page(params[:page]).per(50)
  end

  def show
  end

  def new    
  end

  def create
  end

  def edit
  end

  def update
    if @hotel.update(hotel_params)      
      params[:image_url].map{ |image_url| @hotel.create_image(image_url) } if params[:image_url].present?
      @hotel.delete_images(params[:delete_image_ids]) if params[:delete_image_ids].present?
      redirect_to admin_tbo_hotel_url(@hotel), notice: @hotel.hotel_code.present? ? 'Hotel was successfully updated.' : 'General Markup was successfully updated.'
    else
      render :edit
    end
  end

  def activate
    respond_to do |format|
      format.json do
        set_hotel
        if @hotel.activate! 
          render json: { notice: 'Hotel was successfully activated.' }
        else
          render json: { error: 'Hotel already activated.' }
        end
      end
    end
  end

  def deactivate
    respond_to do |format|
      format.json do
        set_hotel
        if @hotel.deactivate! 
          render json: { notice: 'Hotel was successfully deactivated.' }
        else
          render json: { error: 'Hotel already deactivated.' }
        end
      end
    end
  end

  private
    def hotel_params
      params.require(:tbo_hotel).permit(
        :hotel_name, :markup_type, :markup_amount,
        covers_attributes: [:id, :image, :_destroy]
      )
    end

    def set_hotel
      @hotel = TBOHotel.find(params[:id])
    end

    def set_hotel_details      
      resp = HotelBooker[:tbo_holidays].hotel_details(hotel_code: @hotel.hotel_code) 
      if resp.success?
        @hotel_details = resp.hotel_details       
      end
    end

end

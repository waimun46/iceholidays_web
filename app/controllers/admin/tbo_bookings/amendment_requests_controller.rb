class Admin::TBOBookings::AmendmentRequestsController < AdminController
  before_action :set_booking
  def new    
    redirect_to admin_tbo_booking_amendment_request_url(@booking) if @booking.amendment_request.persisted?
    @amendment_form =  HotelBooker::AmendmentForm.new(
      check_in: @booking.check_in,
      check_out: @booking.check_out,      
    )
    
    @amendment_form.rooms_attributes = rooms_attributes
  end

  def create
    @amendment_form = HotelBooker::AmendmentForm.new(amendment_form_params)      
    @amendment_form.type = HotelBooker::AmendmentForm::REQUEST_TYPES[:offline_amendment]
    @amendment_form.booking = @booking    
    if response = @amendment_form.submit            
      @booking.update amendment_status: response.request_status
      flash[:success] = response.message
      redirect_to admin_tbo_booking_amendment_request_path(@booking)
    else            
      flash.now[:error] = @amendment_form.errors.full_messages.to_sentence
      render :new     
    end
  end

  def widthraw
    amendment_form = HotelBooker::AmendmentForm.new(
      booking: @booking,
      remarks: '?',
      type: HotelBooker::AmendmentForm::REQUEST_TYPES[:withdraw_request]
    ) 
    if response = amendment_form.submit                
      @booking.update amendment_status: response.request_status
      flash[:success] = response.message
    else
      flash[:error] = @amendment_form.errors.full_messages.to_sentence
    end
    redirect_to admin_tbo_booking_amendment_request_path(@booking)
  end
 
  def show
    @amendment_request = @booking.amendment_request
  end

  private
  def set_booking
    @booking = TBOBooking.find(params[:tbo_booking_id])
  end

  def rooms_attributes
    rooms = []
    @booking.guests.group_by(&:guest_in_room).each do |idx, guests|
      guests_attributes = []
      guests.each do |guest|
        guests_attributes << {
          title: guest.title ,
          first_name: guest.first_name, 
          last_name: guest.last_name, 
          age: guest.age,
          guest_type: guest.guest_type, 
          existing_name: "#{guest.title} #{guest.first_name} #{guest.last_name}", 
        }
      end
      rooms << { amend: HotelBooker::RoomRequested[idx.to_i], guest_attributes: guests_attributes }
    end
    rooms
  end

  def amendment_form_params
    params.require(:amendment_form).permit(
      :remarks, :check_in, :check_out,
      rooms_attributes:[:amend, guest_attributes:[:first_name, :last_name, :age, :guest_type, :title, :action, :existing_name ]])
  end
end
class Admin::UsersController < AdminController
  before_action :check_auth, except: [:edit_password, :update_password]
  before_action :pre_set, only: [:edit, :update, :new, :create, :show]

  def index
    keywords = params[:keywords].present? ? params[:keywords] : ''
    state = params[:state].present? ? params[:state] : ''

    @users = User.active_type(params[:user_active_type]).search(keywords).search_state(state).page(params[:page]).order(:full_name)
  end

  def new
    if params[:agent].present? && params[:agent] == 'true'
      @user = User.new(role: :agent)
    else
      @user = User.new
    end
  end

  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to admin_users_path, notice: 'User was successfully created.'
    else
      render :new
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      redirect_to admin_user_path(@user), notice: 'User was successfully updated.'
    else
      render :edit
    end
  end

  def reset_password
    @user = User.find(params[:id])
    if params[:user].present?
      if @user.update(reset_password_params)
        redirect_to admin_user_path(@user), notice: 'User password was successfully updated.'
      else
        flash[:error] = 'User password was failed to updated. Please contact the IT regarding this issue.'
        render :reset_password
      end
    end
  end

  def edit_password
    @user = current_user
    authorize @user
  end

  def update_password
    @user = current_user
    authorize @user
    if @user.update_with_password(user_password_params)
      # Sign in the user by passing validation in case their password changed
      bypass_sign_in(@user)
      redirect_to root_path
    else
      render "edit_password"
    end
  end

  def topup_credits
    @user = User.find(params[:id])
    if params[:credits].present? &&  params[:credits].to_i > 0
      @user.topup_credits(params[:credits].to_i, current_user, params[:remark])
      redirect_to admin_user_path(@user), notice: 'User credits was successfully updated.'
    end
  end

  def access_log

    @today = Date.today

    if params[:begin].present? && params[:end].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      interval = DateTime::Infinity.new.."#{params[:end]}".in_time_zone.end_of_day
    else
      interval = DateTime::Infinity.new...DateTime::Infinity.new
    end

    name = params[:name].present? ? params[:name] : ''
    @users = User.search(name).in_between_current_sign_in_at(interval).page(params[:page]).order("current_sign_in_at DESC NULLS LAST")
  end

  def online_list
    @users = Session.non_expired(Session.expired_time).map(&:user).uniq
  end

private

  def user_params
    params[:user][:phones] =params[:user][:phones].split(',')
    params[:user][:faxes] =params[:user][:faxes].split(',')
    params[:user][:emails] =params[:user][:emails].split(',')
    params[:user][:domains] =params[:user][:domains] - [""]
    params.require(:user).permit(:code, :username, :full_name, :other_name, :alias_name, :address, :city, :state, :postal, :country, :website, :max_sign_in, :role, :is_active, :access_series, :access_activity, :access_flight, :access_wifi, :access_hotel, :access_land_tour, :access_busferry, :remark, phones:[], faxes:[], emails:[], domains:[]).merge(edited_user_id: current_user.id)
  end

  def user_password_params
    params.require(:user).permit(:current_password, :password)
  end

  def reset_password_params
    params.require(:user).permit(:password).merge(edited_user_password: current_user.id)
  end

  def pre_set
    @user_roles = User.roles.except(*[:agent, :subagent])
    @states = Autocomplete.find_by_name(:state).keywords
  end

  def check_auth
    authorize User
  end

end
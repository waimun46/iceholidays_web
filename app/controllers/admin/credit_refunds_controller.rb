class Admin::CreditRefundsController < AdminController
  before_action :check_auth

  def index
    @credit_refunds = CreditRefund.all.page(params[:page]).per(50)
  end

  def show
    @credit_refund = CreditRefund.find(params[:id])
  end

  def update_status
    @credit_refund = CreditRefund.find(params[:id])

    if @credit_refund.present?
      if params[:status] == 'refund'
        @credit_refund.refund!
      elsif params[:status] == 'archive'
        @credit_refund.archive!
      end  
      redirect_to admin_credit_refund_path(@credit_refund), notice: 'Credit refund was successfully updated.'
    else
      render :show
    end
  end

private
  def check_auth
    authorize CreditRefund, :index?
  end

end
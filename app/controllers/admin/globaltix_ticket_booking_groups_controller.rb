class Admin::GlobaltixTicketBookingGroupsController < AdminController
  before_action :check_auth
  def show
    @globaltix_ticket_booking_group = GlobaltixTicketBookingGroup.includes(globaltix_ticket_bookings: [:agent_user]).find(params[:id])
  end

  def index
    @today = DateTime.current
    if params[:begin].present? && params[:end].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      interval = DateTime.new.."#{params[:end]}".in_time_zone.end_of_day
    else
      interval = DateTime::Infinity.new...DateTime::Infinity.new
    end

    if params[:begin].present? || params[:end].present?
      @total_amount = GlobaltixTicketBookingGroup.in_between(interval).calculate_all(:sum_price)
      @total_pax = GlobaltixTicketBooking.in_between(interval).count
    end

    @globaltix_ticket_booking_groups = GlobaltixTicketBookingGroup.includes(globaltix_ticket_bookings: [:agent_user]).in_between(interval).order("globaltix_ticket_booking_groups.created_at desc ").page(params[:page]).per(BookingGroup::ADMIN_BOOKINGGROUPS_PER_PAGE)
  end

private
  def check_auth
    authorize GlobaltixTicketBookingGroup, :index?
  end
end
class Admin::RoamingmanBookingGroupsController < ApplicationController
  before_action :check_auth, except: [:create_export]

  def index
    @today = DateTime.current
    if params[:begin].present? && params[:end].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      interval = DateTime.new.."#{params[:end]}".in_time_zone.end_of_day
    else
      interval = DateTime::Infinity.new...DateTime::Infinity.new
    end

    if params[:depart_begin].present? && params[:depart_end].present?
      depart_interval = "#{params[:depart_begin]}".in_time_zone.beginning_of_day.."#{params[:depart_end]}".in_time_zone.end_of_day
    elsif params[:depart_begin].present?
      depart_interval = "#{params[:depart_begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:depart_end].present?
      depart_interval = DateTime.new.."#{params[:depart_end]}".in_time_zone.end_of_day
    else
      depart_interval = DateTime::Infinity.new...DateTime::Infinity.new
    end

    country = params[:country].present? ? params[:country] : ''
    package_code = params[:package_code].present? ? params[:package_code] : ''
    customer_name = params[:name].present? ? params[:name] : ''
    booked_by = params[:booked_by].present? ? params[:booked_by] : ''
    booking_id = params[:booking_id].present? ? params[:booking_id] : ''

    @roamingman_booking_groups = RoamingmanBookingGroup.includes(:roamingman_bookings).visible.in_between(interval).in_between_departure(depart_interval).search_country(country).search_package_code(package_code).search_name(customer_name).search_agent(booked_by).search_booking_id(booking_id).order("roamingman_booking_groups.created_at desc ").page(params[:page]).per(50)
  end

  def show
    @roamingman_booking_group = RoamingmanBookingGroup.includes(:roamingman_bookings).find(params[:id])
  end

  def edit
    @roamingman_booking_group = RoamingmanBookingGroup.find(params[:id])
  end

  def update
    @roamingman_booking_group = RoamingmanBookingGroup.find(params[:id])

    if @roamingman_booking_group.update(update_booking_group_params)
      redirect_to admin_roamingman_booking_group_path(@roamingman_booking_group), notice: 'All bookings was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @roamingman_booking_group = RoamingmanBookingGroup.find(params[:id])

    if @roamingman_booking_group.cancel_booking
      redirect_to admin_roamingman_booking_groups_path, notice: 'Roamingman Bookings was successfully cancelled.'
    else
      render :index
    end
  end

  # def create_export
  #   @roamingman_booking_group = RoamingmanBookingGroup.find(params[:id])

  #   if @roamingman_booking_group.export_roamingman_create(current_user)
  #     redirect_to admin_roamingman_booking_group_path(@roamingman_booking_group), notice: 'Roamingman for BookingGroup was successfully exported.'
  #   else
  #     flash[:error] = "Failed to export, please try again later or check on roamingman system."
  #     render :show
  #   end
  # end

private
  def check_auth
    authorize RoamingmanBookingGroup, :index?
  end

  def update_booking_group_params
    new_params = params.require(:roamingman_booking_group).permit(:remark, roamingman_bookings_attributes: [:id, :name, :mobile, :email, :depart_date, :return_date, :quantity, :ship_way, :deliver_point_id, :address, :postcode, :is_send_sms, :is_send_email, :remark])
    if new_params[:roamingman_bookings_attributes].present?
      new_params[:roamingman_bookings_attributes].each {|k,v| v.merge!(last_edited_user_id: current_user.id)}
    end
    new_params.merge(last_edited_user_id: current_user.id)
  end
end

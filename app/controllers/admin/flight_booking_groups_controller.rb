class Admin::FlightBookingGroupsController < AdminController
  before_action :check_auth

  def index
    @today = DateTime.current
    if params[:begin].present? && params[:end].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      interval = DateTime.new.."#{params[:end]}".in_time_zone.end_of_day
    else
      interval = DateTime::Infinity.new...DateTime::Infinity.new
    end

    if params[:begin].present? || params[:end].present?
      @total_amount = FlightBookingGroup.in_between(interval).calculate_all(:sum_price)
      @total_pax = FlightBooking.in_between(interval).count
    end

    code = params[:code].present? ? params[:code] : ''
    customer_name = params[:name].present? ? params[:name] : ''

    @flight_booking_groups = FlightBookingGroup.includes(:flight_booking_details, flight_bookings: [:agent_user]).in_between(interval).search_code(code).search_name(customer_name).order("flight_booking_groups.created_at desc ").page(params[:page]).per(BookingGroup::ADMIN_BOOKINGGROUPS_PER_PAGE)

    render :index
  end

  def show
    @flight_booking_group = FlightBookingGroup.includes(:flight_booking_details, flight_bookings: [:agent_user]).find(params[:id])
  end

private
  def check_auth
    authorize FlightBookingGroup, :index?
  end
end
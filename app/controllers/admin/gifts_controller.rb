class Admin::GiftsController < AdminController
  before_action :check_auth
  def index
    @gifts = Gift.visible.includes(:gift_stocks).order("title").page(params[:page]).per(50)
  end

  def new
    @gift = Gift.new
  end

  def create
    @gift = Gift.new(gift_params)

    if @gift.save
      redirect_to admin_gifts_path, notice: 'Gift was successfully created.'
    else
      render :new
    end
  end

  def show
    @gift = Gift.visible.includes(:gift_stocks).find(params[:id])
  end

  def edit
    @gift = Gift.find(params[:id])
  end

  def update
    @gift = Gift.find(params[:id])

    if @gift.update(update_gift_params)
      redirect_to admin_gifts_path, notice: 'Gift was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @gift = Gift.find(params[:id])

    if @gift.archived!
      redirect_to admin_gifts_path, notice: 'Gift was successfully deleted.'
    else
      render :index
    end
  end

  def gift_stock_report
    if params[:begin].present? && params[:end].present?
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day..DateTime.now.end_of_day
      params[:end] = DateTime.now.end_of_month.strftime("%F")
    elsif params[:end].present?
      params[:begin] = (DateTime.now.beginning_of_month - 2.months).strftime("%F")
      @interval = DateTime.new.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:commit].blank?
      params[:begin] = (DateTime.now.beginning_of_month - 2.months).strftime("%F")
      params[:end] = DateTime.now.end_of_month.strftime("%F")
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    else
      @interval = DateTime::Infinity.new...DateTime::Infinity.new
    end
    @gifts = Gift.in_between_created_at(@interval)
  end

private
  def gift_params
    params.require(:gift).permit(:title, :color, :image, :points, :price, :sku)
  end

  def update_gift_params
    params.require(:gift).permit(policy([:admin, @gift]).permitted_attributes)
  end

  def check_auth
    authorize Gift
  end
end

class Admin::HotelsController < AdminController
  before_action :check_auth
  before_action :set_hotel, only: [:show, :edit, :update]
  before_action :set_hotel_details, only: [:edit]

  def index
    @general_mark_up = Hotel.general_markup

    if params[:hotel_keyword].present?
      if params[:city_keyword].present?
        @hotels = Hotel.search_with_city(params[:hotel_keyword], params[:city_keyword]).page(params[:page]).per(50)
      else
        @hotels = Hotel.search_by_hotel(params[:hotel_keyword]).page(params[:page]).per(50)
      end
    elsif params[:city_keyword].present?
      @hotels = Hotel.search_by_city(params[:city_keyword]).page(params[:page]).per(50)
    else
      @hotels = Hotel.all.page(params[:page]).per(50)
    end
  end

  def show
  end

  def new
    @hotel = Hotel.new
  end

  def create
    @hotel = Hotel.new(hotel_params)

    if @hotel.save
      redirect_to admin_hotel_path(@hotel), notice: @hotel.tp_hotel_id.present? ? 'Hotel was successfully created.' : 'General Markup was successfully created.'
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @hotel.update(hotel_params)      
      params[:image_url].map{ |image_url| @hotel.create_image(image_url) } if params[:image_url].present?
      @hotel.delete_images(params[:delete_image_ids]) if params[:delete_image_ids].present?

      redirect_to admin_hotel_path(@hotel), notice: @hotel.tp_hotel_id.present? ? 'Hotel was successfully updated.' : 'General Markup was successfully updated.'
    else
      render :edit
    end
  end

  private
    def hotel_params
      params.require(:hotel).permit(
        :hotel_name, :tp_hotel_id, :hotel_source, :markup_category, :markup,
        covers_attributes: [:id, :image, :_destroy]
      )
    end

    def set_hotel
      @hotel = Hotel.find(params[:id])
    end

    def set_hotel_details
      @hotel_details = TravelPrologue::Hotel.getDetailByHotelID(@hotel)
      if @hotel_details.lst_hotel_images.present?
        @hotel_images = @hotel_details.lst_hotel_images.map { |image| TravelPrologue::Image.new(image) }
      end
    end

    def check_auth
      authorize Hotel, :index?
    end

end

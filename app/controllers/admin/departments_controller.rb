class Admin::DepartmentsController < AdminController
  before_action :check_auth

  def index
    keywords = params[:keywords].present? ? params[:keywords] : ''
    @department = Department.search(keywords).page(params[:page]).per(50)            
  end

  def new
    @department = Department.new
  end

  def create
    @department = Department.new(department_params)

    if @department.save
      redirect_to admin_departments_path, notice: 'Department was successfully created.'
    else
      render :new
    end
  end

  def show
    @department = Department.find(params[:id])
  end

  def edit
    @department = Department.find(params[:id])
  end

  def update
    @department = Department.find(params[:id])

    if @department.update(department_params)
      redirect_to admin_department_path(@department), notice: 'Department was successfully updated.'
    else
      render :edit
    end
  end

private
  def department_params
    params.require(:department).permit(:name, :email, :phone, :important_note, :description, :logo, :title, :remote_logo_url, :fs_important_note, :fs_logo , :fs_description)
  end

  def check_auth
    authorize Department, :index?
  end

end
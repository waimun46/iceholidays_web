class Admin::LandToursController < AdminController
  before_action :check_auth

  def index
    keywords = params[:keywords].present? ? params[:keywords] : ''

    if params[:only_active_tour].present? && params[:only_active_tour] == 'false'
      only_active_tour = false
    else
      only_active_tour = true
    end

    @land_tours = LandTour.search_keyword(keywords).with_visible(only_active_tour).order("land_tours.created_at desc").page(params[:page]).per(50)
  end

  def new
    @land_tour = LandTour.new
    activity_list
  end

  def create
    @land_tour = LandTour.new(land_tour_params)
    activity_list

    if @land_tour.save
      redirect_to admin_land_tours_path, notice: 'Land Tour was successfully created.'
    else
      render :new
    end
  end

  def show
    @land_tour = LandTour.includes(:covers).find(params[:id])
    @land_tour_days = @land_tour.land_tour_days.order(day_no: :asc)
  end

  def edit
    @land_tour = LandTour.includes(:covers).find(params[:id])
    @land_tour_days = @land_tour.land_tour_days.order(day_no: :asc)
    activity_list
  end

  def update
    @land_tour = LandTour.includes(:covers).find(params[:id])
    activity_list

    if @land_tour.update(land_tour_params)
      redirect_to admin_land_tour_path(@land_tour), notice: 'Land Tour was successfully updated.'
    else
      render :edit
    end
  end

private
  def land_tour_params
    # if params[:land_tour][:travel_date_start].present? && params[:land_tour][:travel_date_end].present?
    #   travel_date = (params[:land_tour][:travel_date_start]).in_time_zone..(params[:land_tour][:travel_date_end]).in_time_zone
    # else
    #   travel_date = nil
    # end
    
    params[:land_tour][:guide_languages] = params[:land_tour][:guide_languages].reject{|lang| lang.blank? }
    params[:land_tour][:depart_day] = (params[:land_tour][:depart_day].reject{|id| id.blank? })
    params[:land_tour][:supplier_email] = params[:land_tour][:supplier_email].split(',')
    all_contact = contact_params
    all_charge = charge_params
   
    new_params = params.require(:land_tour).permit(:title, :cn_title, :code, :min_booking, :max_booking, :category, :highlight, :description, :cn_description, :terms_and_conditions, :deposit_type, :deposit, :single_supplement_price, :cut_off_day, :blockout_dates_text, :blockout_dates_text, :child_with_bed, :child_without_bed, :child_twin, :infant, :dta_type, :dta_adult, :dta_child, :inc_hotel, :inc_flight, :inc_half_board_meals, :inc_full_board_meals, :inc_entrance_ticket, :inc_transportation, :inc_shopping_stop, :free_of_charge, :status, :supplier_tour_code, :supplier_name, :supplier_address, :supplier_pic_name, :supplier_pic_phone_no, supplier_email: [], guide_languages: [], depart_day: [], land_tour_days_attributes: [:id, :day_no, :title, :description, :inc_breakfast, :inc_lunch, :inc_dinner, :_destroy, land_tour_activities_attributes: [:id, :taggable_type, :taggable_id, :_destroy], land_tour_cities_attributes: [:id, :city_id, :_destroy]], land_tour_categories_attributes: [:id, :name, :_destroy, land_tour_prices_attributes: [:id, :dates_text, :_destroy, prices: []]], covers_attributes: [:id, :image, :remote_image_url, :_destroy]).merge(compulsory_charges: all_charge, emergency_contact: all_contact)

    if new_params[:land_tour_categories_attributes].present? && params[:land_tour_prices][:quantity].present?
      new_params[:land_tour_categories_attributes].each do |key, category|
        if category[:land_tour_prices_attributes].present?
          category[:land_tour_prices_attributes].each do |key, prices|
            if prices.present?
              all_price = {}
              prices["prices"].each_with_index do |price, i|
                qty_str = params[:land_tour_prices][:quantity][i].split('-')
                if params[:land_tour_prices][:quantity][i].include?('-')
                  qty_range = Range.new(qty_str[0].to_i, qty_str[1].to_i)
                else
                  qty_range = qty_str[0]
                end

                if !all_price.include?(qty_range)
                  all_price[qty_range] = price
                end
              end
              prices.merge!(prices: all_price)
            end
          end
        end
      end
    end
    new_params
  end

  def activity_list
    @all_activity = []
    @activities = []
    @activities << GlobaltixTicket.active
    @activities << Activity.visible.bookable

    @activities.each do |activity|
      activity.each do |act|
        activity_data = []
        activity_data << act.title
        activity_data << "#{act.id}, #{act.class.name}"
        @all_activity << activity_data
      end
    end
  end

  def contact_params
    contacts = params[:land_tour][:emergency_contact]
    all_contact = []
    j = 0
    contacts["contact_name"].each do |name|
      detail = {
        name: name,
        phone_no: contacts["contact_phone_no"][j],
      }
      j += 1
      all_contact << detail
    end
    all_contact
  end

  def charge_params
    charges = params[:land_tour][:compulsory_charges]
    all_charge = []
    j = 0
    charges["charge_title"].each do |title|
      detail = {
        title: title,
        type: charges["charge_type"][j],
        price: charges["charge_price"][j]
      }
      j += 1
      all_charge << detail
    end
    all_charge
  end

  def check_auth
    authorize LandTour, :index?
  end
end

class Admin::BookingAlterationsController < AdminController
  before_action :check_auth, except: [:restore]

  def index
    keywords = params[:keywords].present? ? params[:keywords] : ''
    created_user = params[:created_user].present? ? params[:created_user] : ''
    alter_type = params[:alter_type].present? ? params[:alter_type].downcase : ''

    if keywords.present?
      @booking_alterations = BookingAlteration.includes(:original_tour, :original_booking_group, :bookings).search(keywords).filter_type(alter_type).filter_user(created_user).visible.order("booking_alterations.created_at desc").page(params[:page]).per(50)
    else
      @booking_alterations = BookingAlteration.includes(:original_tour, :original_booking_group, :bookings).filter_type(alter_type).filter_user(created_user).order("booking_alterations.created_at desc").page(params[:page]).per(50)
    end
  end
  
  def show
    @booking_alteration = BookingAlteration.includes(:bookings).find(params[:id])
  end

  def restore
    @booking_alteration = BookingAlteration.includes(:bookings).find(params[:id])
    authorize @booking_alteration 
    if @booking_alteration.restore_withdrawn(current_user)
      redirect_to admin_booking_alterations_path, notice: 'Bookings was successfully restored.'
    else
      redirect_to admin_booking_alteration_path(@booking_alteration), notice: 'Unable to restore due to the tour is full.'
    end
  end

private
  def check_auth
    authorize BookingAlteration
  end
end
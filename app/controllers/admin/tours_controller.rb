class Admin::ToursController < AdminController
  before_action :check_auth

  def index
    @today = Date.today
    keywords = params[:keywords].present? ? params[:keywords] : ''
    itinerary_id = params[:itinerary_id].present? ? params[:itinerary_id] : ''

    if params[:begin].present? && params[:end].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      interval = DateTime::Infinity.new.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:commit].blank? && params[:page].blank?
      params[:begin] = DateTime.current.strftime("%F")
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    else
      interval = DateTime::Infinity.new...DateTime::Infinity.new
    end

    if params[:only_booking_tour].present?
      only_booking_tour = params[:only_booking_tour]
    else
      only_booking_tour = false
    end

    if params[:only_active_tour].present? && params[:only_active_tour] == 'false'
      only_active_tour = false
    else
      only_active_tour = true
    end

    if params[:tour_seats_summary].present?
      tour_seats_summary = params[:tour_seats_summary]
    else
      tour_seats_summary = false
    end

    if params[:sorting_type].present?
      sorting_type = params[:sorting_type]
    else
      sorting_type = ""
    end

    if tour_seats_summary && (params[:begin].present? || params[:end].present?)
      @bookings_count = Booking.visible.where(tour_id: Tour.search(keywords).with_bookings(only_booking_tour).with_visible(only_active_tour).in_between_departure(interval).ids).count
      @total_seats = Tour.search(keywords).with_bookings(only_booking_tour).with_visible(only_active_tour).in_between_departure(interval).map(&:all_total_seats).sum
    end

    @tours = Tour.includes(:bookings).search(keywords).with_bookings(only_booking_tour).with_sorting_type(sorting_type).with_visible(only_active_tour).with_itinerary(itinerary_id).in_between_departure(interval).page(params[:page]).per(Tour::ADMIN_TOURS_PER_PAGE).order("tours.departure_date")
  end

  def new
    if params[:cruise].present? && params[:cruise] == 'true'
      @tour = Tour.new(is_cruise: true)
    else
      @tour = Tour.new
    end
    @autocomplete = Autocomplete.find_by_name(:contient_country).keywords
  end

  def create
    @tour = Tour.new(tour_params.merge(created_user: current_user))
    @autocomplete = Autocomplete.find_by_name(:contient_country).keywords

    if @tour.save
      redirect_to admin_tours_path, notice: 'Tour was successfully created.'
    else
      render :new
    end
  end

  def show
    @tour = Tour.includes(bookings: [:agent_user]).find(params[:id])
    @bookings = @tour.bookings.visible.order(created_at: :desc, id: :desc)
    if params[:print].present?
      render :print
    else
      render :show
    end
  end

  def edit
    @tour = Tour.find(params[:id])
    @tour_leader= TourLeader.all
    @autocomplete = Autocomplete.find_by_name(:contient_country).keywords
  end

  def update
    @tour = Tour.find(params[:id])
    @tour_leader= TourLeader.all
    @autocomplete = Autocomplete.find_by_name(:contient_country).keywords

    if @tour.update(tour_params.merge(edited_user_id: current_user.id))
      redirect_to admin_tour_path(@tour), notice: 'Tour was successfully updated.'
    else
      render :edit
    end
  end

  def create_booking
    @tour = Tour.find(params[:id])
    booking_seats = booking_seats_params
    if params[:booking_seats].blank? || booking_seats.size == 0
      redirect_to admin_tour_path(@tour), notice: 'Require at least one booking to create.'
    elsif !@tour.admin_able_to_purcharse?(booking_seats)
      redirect_to admin_tour_path(@tour), notice: 'The booking seats are no longer available, please book again.'
    else
      bookings = Booking.bulk_create(@tour, booking_seats, current_user, params[:agent_rep].blank? ? current_user.username : params[:agent_rep])
      if bookings.present? && bookings.first.booking_group.present?
        redirect_to edit_admin_booking_group_path(bookings.first.booking_group)
      else
        redirect_to admin_tour_path(@tour), notice: 'The booking seats are no longer available, please book again.'
      end
    end
  end

  def duplicate
    tour = Tour.find(params[:id])
    if params[:code].present? && params[:departure_date].present?
      if Tour.find_by_code(params[:code]).blank?
        new_tour = Tour.duplicate(tour, params[:departure_date], params[:code])
        if new_tour.id.present?
          redirect_to edit_admin_tour_path(new_tour)
        else
          redirect_to admin_tour_path(tour), notice: 'The tour is failed to duplicated because the '
        end
      else
        redirect_to admin_tour_path(tour), notice: 'Failed to duplicate, please fill all original tour required fields.'
      end
    else
      redirect_to admin_tour_path(tour), notice: 'Please fill in the departure_date to duplicate the tour.'
    end
  end

  def uncertainty
    if params[:begin].present? && params[:end].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      interval = DateTime::Infinity.new.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:commit].blank?
      params[:begin] = (DateTime.current).strftime("%F")
      params[:end] = (DateTime.current + 3.months).strftime("%F")
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    else
      interval = DateTime::Infinity.new...DateTime::Infinity.new
    end
    @today = DateTime.current
    @tours = Tour.uncertainty.in_between_departure(interval).page(params[:page]).per(Tour::ADMIN_TOURS_PER_PAGE).order("tours.departure_date")
  end

  def insurance
    @tour = Tour.includes(bookings: [:agent_user]).find(params[:id])
    @exported_bookings = @tour.bookings.exported.order(created_at: :desc, id: :desc)
    @non_exported_bookings = @tour.bookings.non_exported.order(created_at: :desc, id: :desc)
    if params[:print].present?
      render :insurance_print
    else
      render :insurance
    end
  end

  def calendar
    if params[:begin].present? && params[:end].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      interval = DateTime::Infinity.new.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:commit].blank?
      params[:begin] = DateTime.current.strftime("%F")
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    else
      interval = DateTime::Infinity.new...DateTime::Infinity.new
    end
    @today = DateTime.current
    
    if params[:cut_off].present?
      @tours = Tour.all.in_between_cut_off(interval).order("tours.cut_off_date")
      render :cut_off
    elsif params[:insurance].present?
      @tours = Tour.all.in_between_departure(interval).order("tours.departure_date")
      render :insurance_calendar
    else
      redirect_to admin_tours_path
    end
  end

  def booking_rooms
    @tour = Tour.find(params[:id])
    if params[:print].present?
      render :booking_rooms_print
    else
      render :booking_rooms
    end
  end

  def edit_booking_rooms
    @tour = Tour.find(params[:id])
    @booking_adult_child_twin = @tour.bookings.visible.adult_child_twin
    @booking_child_with_and_without_bed = @tour.bookings.visible.where(category:[:child_with_bed, :child_no_bed])
    @tour_adult_child_twin = @tour.tour_leader
  end

  def update_booking_rooms
  @tour = Tour.find(params[:id])
    if booking_room_params.present?
      booking_ids = booking_room_params[:booking_rooms_attributes].values.map{|br|br[:bookings].values}.flatten
      if booking_ids.uniq.count == booking_ids.count
        create_booking_room = BookingRoom.create_booking_room(@tour, @tour.bookings.ids, booking_room_params.to_h)
        # Rails.logger.info "Check ---> #{create_booking_room}"
        if create_booking_room
          # Rails.logger.info "Result ---> Booking Success!"
          redirect_to booking_rooms_admin_tour_path(@tour), notice: 'Booking room was successfully updated.'
        else
          # Rails.logger.info "Error ---> Booking Failed!"
          redirect_to edit_booking_rooms_admin_tour_path(@tour), notice: 'Booking room was not saved. Try again.'
        end
      end
    end
  end

  def confirmation
      @tour = Tour.find(params[:id])
      @is_four_season = @tour.itinerary.category.include?("Four Season")
      if params[:print].present?
      render :confirmation
    else
      render :confirmation
    end
  end

private
  def tour_params
    params[:tour][:operator_remarks] =params[:tour][:operator_remarks].split(',')
    params.require(:tour).permit(:code, :caption, :other_caption, :contient_country, :pnrs, Tour::ALL_SEATS_TYPE, Tour::ALL_PRICE_TYPE, Tour::DTA_CATEGORY, Tour::CHILD_FARE_APPLY_PRICE_TYPES, :early_bird_date, :zone, :departure_date, :cut_off_date, :ground_price, :ground_price_flag,:guaranteed_departure_flag, :hand_carry_weight, :luggage_weight, :luggage_quantity, :confirmation_emergency_contact, :guaranteed_departure_seats, :guaranteed_departure, :single_supplement_price, :misc_fee, :visa_fee, :subtract_others, :meet_up_point, :assembly_time, :tour_guide_name, :tour_guide_phone, :tour_guide_chinese_name, :compulsory_additional_fee, :addon_others, :child_with_bed_percentage, :child_no_bed_percentage, :child_twin_percentage, :infant_price, :max_booking_seats, :deposit, :tour_leader_name, :tour_leader_id, :category, :tour_leader_remark, :remark, :internal_reference, :active, :itinerary_code, :letter_head_image, :land_staff, :land_company, :guide_languages, :tipping, :welcome_board, :is_cruise, :highlight, operator_remarks:[], child_fares:[], tour_accomodations_attributes: [:id, :hotel_detail, :check_in_date, :check_out_date], tour_flights_attributes: [:id, :departure, :arrival, :from_to, :flight_no, :internal_remark, :agent_remark, :_destroy], reminders_attributes: [:id, :title, :description, :reminder_date, :email, :_destroy])
  end

  def booking_room_params
    params.require(:tour).permit(booking_rooms_attributes: [:id, :room_type, :_destroy, bookings: [:guest_1, :guest_2, :guest_3, :guest_4, :_destroy]])
  end

  def booking_seats_params
    if params[:booking_seats].class === 'String'
      booking_seats = process_seats_params(params[:booking_seats])
      {''=> booking_seats}
    elsif @tour.is_cruise? && params[:booking_seats].keys.length > 0
      booking_seats = {}
      params[:booking_seats].each do |type, value|
        data = process_seats_params(value)
        if data.present?
          booking_seats[type] = data
        end
      end
      booking_seats
    end
  end

  def process_seats_params(data_string)
    data_string.split(', ').map do |seat|
      # parsing "2 promo(188)"
      seat_match = seat.match /\A(\d+)\s(\w+)\(\d+\)/
      if seat_match.present? && seat_match.length == 3
        [seat_match[2], seat_match[1].to_i]
      else
        nil
      end
    end.reject(&:blank?).to_h
  end

  def check_auth
    authorize Tour, :index?
  end
end
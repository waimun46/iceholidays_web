class Admin::InsuranceRebatesController < AdminController
  before_action :check_auth
  def index
    @insurance_rebate = InsuranceRebate.page(params[:page]).order(days_duration: :asc, zone: :asc).per(50)
  end

  def show
    @insurance_rebate = InsuranceRebate.find(params[:id])
  end

  def edit
    @insurance_rebate = InsuranceRebate.find(params[:id])
  end

  def update
    @insurance_rebate = InsuranceRebate.find(params[:id])

    if @insurance_rebate.update(insurance_rebate_params)
      redirect_to admin_insurance_rebate_path(@insurance_rebate), notice: 'Insurance Rebate was successfully updated.'
    else
      render :edit
    end
  end
private
  def insurance_rebate_params
    params.require(:insurance_rebate).permit(:price)
  end

  def check_auth
    authorize Fair, :index?
  end
end

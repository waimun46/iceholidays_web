class Admin::BookingGroupsController < AdminController
  before_action :check_auth, except: [:new_export, :create_export, :create_insurance_export, :proforma_invoice]

  def index
    @today = DateTime.current
    if params[:begin].present? && params[:end].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      interval = DateTime.new.."#{params[:end]}".in_time_zone.end_of_day
    else
      interval = DateTime::Infinity.new...DateTime::Infinity.new
    end
    if params[:begin_departure_date].present? && params[:end_departure_date].present?
      interval_departure_date = "#{params[:begin_departure_date]}".in_time_zone.beginning_of_day.."#{params[:end_departure_date]}".in_time_zone.end_of_day
    elsif params[:begin_departure_date].present?
      interval_departure_date = "#{params[:begin_departure_date]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end_departure_date].present?
      interval_departure_date = DateTime.new.."#{params[:end_departure_date]}".in_time_zone.end_of_day
    else
      interval_departure_date = DateTime::Infinity.new...DateTime::Infinity.new
    end

    if params[:tour_status].present?
      tour_status = params[:tour_status]
    else
      tour_status = nil
    end

    if params[:insurance_confirmation].present?
      insurance_confirmation = params[:insurance_confirmation]
    else
      insurance_confirmation = nil
    end
    if params[:insurance_exported].present?
      insurance_exported = params[:insurance_exported]
    else
      insurance_exported = nil
    end

    if params[:sorting_type].present?
      sorting_type = params[:sorting_type]
    else
      sorting_type = ""
    end


    if params[:receipt].present?
      if params[:receipt] == 'with_partial'
        @booking_groups = BookingGroup.includes(bookings: [:tour, :agent_user, :infant_bookings]).in_between(interval).with_receipt.order("booking_groups.created_at desc").page(params[:page]).per(BookingGroup::ADMIN_BOOKINGGROUPS_PER_PAGE)
      elsif params[:receipt] == 'with_non_overpaid'
        @booking_groups = BookingGroup.includes(bookings: [:tour, :agent_user, :infant_bookings]).in_between(interval).with_receipt.non_overpaid.order("booking_groups.created_at desc").page(params[:page]).per(BookingGroup::ADMIN_BOOKINGGROUPS_PER_PAGE)
        elsif params[:receipt] == 'with_overpaid'
        @booking_groups = BookingGroup.includes(bookings: [:tour, :agent_user, :infant_bookings]).in_between(interval).with_receipt.overpaid.order("booking_groups.created_at desc").page(params[:page]).per(BookingGroup::ADMIN_BOOKINGGROUPS_PER_PAGE)
      elsif params[:receipt] == 'with_outstanding'
        @booking_groups = BookingGroup.includes(bookings: [:tour, :agent_user, :infant_bookings]).in_between(interval).with_receipt.outstanding.order("booking_groups.created_at desc").page(params[:page]).per(BookingGroup::ADMIN_BOOKINGGROUPS_PER_PAGE)
      elsif params[:receipt] == 'without'
        @booking_groups = BookingGroup.includes(bookings: [:tour, :agent_user, :infant_bookings]).in_between(interval).without_receipt.order("booking_groups.created_at desc").page(params[:page]).per(BookingGroup::ADMIN_BOOKINGGROUPS_PER_PAGE)
      else
        @booking_groups = nil
      end
    else
      destination = params[:destination].present? ? params[:destination] : ''
      code = params[:code].present? ? params[:code] : ''
      tour_code = params[:tour_code].present? ? params[:tour_code] : ''
      customer_name = params[:name].present? ? params[:name] : ''
      booked_by = params[:booked_by].present? ? params[:booked_by] : ''
      sub_sales = params[:sub_sales].present? ? params[:sub_sales] : ''
      user_state = params[:user_state].present? ? params[:user_state] : ''
      price_type = params[:price_type].present? ? params[:price_type] : ''
      booking_id = params[:booking_id].present? ? params[:booking_id] : ''
      category = params[:category].present? ? params[:category] : ''
      

      @booking_groups = BookingGroup.includes(bookings: [:tour, :agent_user]).visible.in_between_booked(interval).in_between_departure_date(interval_departure_date).search_destination(destination).search_code(code).search_tour_code(tour_code).search_visible_tour(tour_status).search_name(customer_name).seach_booking_id(booking_id).search_agent(booked_by).search_sub_sales(sub_sales).search_user_origin_state(user_state).search_price_type(price_type).with_sorting_type(sorting_type).with_insurance_confirmation(insurance_confirmation).with_insurance_exported(insurance_exported).search_category(category).order("booking_groups.created_at desc ").page(params[:page]).per(BookingGroup::ADMIN_BOOKINGGROUPS_PER_PAGE)

      if params.permit(:begin, :end, :begin_departure_date, :end_departure_date, :tour_code, :name, :destination, :booked_by, :sub_sales, :booking_id, :category).values.any?(&:present?)
        @booking_datas = Booking.visible.in_between_booked(interval).in_between_departure_date(interval_departure_date).search_destination(destination).search_tour_code(tour_code).search_visible_tour(tour_status).search_name(customer_name).seach_booking_id(booking_id).search_agent(booked_by).search_sub_sales(sub_sales).search_user_origin_state(user_state).search_price_type(price_type).with_insurance_confirmation(insurance_confirmation).with_insurance_exported(insurance_exported).search_category(category).calculate_all(:count, :sum_price)
      end
      if params[:by_tour].present?
        render :index_by_tour
      else
        render :index
      end
    end
  end

  def show
    @booking_group = BookingGroup.includes(bookings: [:agent_user, :tour]).find(params[:id])
    @tour = @booking_group.tour
  end

  def edit
    @booking_group = BookingGroup.includes(bookings: [:agent_user, :tour]).find(params[:id])
    @tour = @booking_group.tour
  end

  def update
    @booking_group = BookingGroup.find(params[:id])
    @tour = @booking_group.tour

    if !params[:booking_group][:bookings_attributes].values.any?{|b| b[:category]=='adult'}
      @booking_group.assign_attributes(update_booking_group_params)
      @booking_group.valid?
      @booking_group.errors[:base] << "Bookings must at least have one adult."
      render :edit
    elsif @booking_group.update(update_booking_group_params)
      redirect_to admin_booking_group_path(@booking_group), notice: 'All bookings was successfully updated.'
    else
      render :edit
    end
  end

  def new_alter
    @booking_group = BookingGroup.find(params[:id])
    @tour = @booking_group.tour
    @alter = BookingAlteration.new
  end

  def create_alter
    @booking_group = BookingGroup.find(params[:id])
    @tour = @booking_group.tour
    alter_bookings = Booking.where(id: params[:booking_ids].split(','))

    if alter_bookings.blank?
      flash[:error] = "Please select the bookings for withdraw/transfer"
      render :new_alter
    elsif params[:booking_alteration][:alter_type] == 'withdrawn'
      @alter = BookingAlteration.bulk_withdraw(alter_bookings, create_booking_alteration_params, current_user)
      redirect_to admin_booking_group_path(@booking_group), notice: 'All bookings was successfully withdrawn.'
    elsif params[:booking_alteration][:alter_type] == 'transferred'
      new_tour = Tour.find(params[:tour_id])
      @alter = BookingAlteration.bulk_transfer(alter_bookings, create_booking_alteration_params, new_tour, current_user)
      redirect_to admin_booking_group_path(@booking_group), notice: 'All bookings was successfully trasferred.'
    else
      flash[:error] = "Please select the action type: withdraw/transfer"
      render :new_alter
    end
  end

  def new_export
    @booking_group = BookingGroup.find(params[:id])
    authorize @booking_group
    @tour = @booking_group.tour
  end

  def create_export
    @booking_group = BookingGroup.find(params[:id])
    authorize @booking_group
    @tour = @booking_group.tour
    if @booking_group.update(create_export_params)
      if @booking_group.export(current_user)
        redirect_to admin_booking_group_path(@booking_group), notice: 'BookingGroup was successfully exported.'
      else
        flash[:error] = "Failed to export, please try again later or check on ecard system."
        render :new_export
      end
    else
      render :new_export
    end
  end

  def received_deposit
    @business_days0to2_interval = 2.business_days.ago.change({hour: 9})..Time.now
    @business_days0to2_with_receipt_nop = BookingGroup.in_between(@business_days0to2_interval).with_receipt.non_overpaid.order("booking_groups.created_at desc")
    @business_days0to2_with_receipt_op = BookingGroup.in_between(@business_days0to2_interval).with_receipt.overpaid.order("booking_groups.created_at desc")
    @business_days0to2_with_receipt_pax = Booking.where(booking_group_id: @business_days0to2_with_receipt_nop.ids + @business_days0to2_with_receipt_op).visible.count
    @business_days0to2_with_receipt_nop_deposit = @business_days0to2_with_receipt_nop.map(&:deposit).inject(:+).to_i
    @business_days0to2_with_receipt_op_deposit = @business_days0to2_with_receipt_op.map(&:deposit).inject(:+).to_i
    @business_days0to2_without_receipt = BookingGroup.in_between(@business_days0to2_interval).without_receipt.order("booking_groups.created_at desc")
    @business_days0to2_without_receipt_deposit = @business_days0to2_without_receipt.map(&:deposit).inject(:+).to_i
    @business_days0to2_without_receipt_pax = Booking.where(booking_group_id: @business_days0to2_without_receipt.ids).visible.count

    @business_days3to7_interval = 7.business_days.ago.change({hour: 9})...2.business_days.ago.change({hour: 9})
    @business_days3to7_with_receipt_nop = BookingGroup.in_between(@business_days3to7_interval).with_receipt.non_overpaid.order("booking_groups.created_at desc")
    @business_days3to7_with_receipt_op = BookingGroup.in_between(@business_days3to7_interval).with_receipt.overpaid.order("booking_groups.created_at desc")
    @business_days3to7_with_receipt_pax = Booking.where(booking_group_id: @business_days3to7_with_receipt_nop.ids + @business_days3to7_with_receipt_op).visible.count
    @business_days3to7_with_receipt_nop_deposit = @business_days3to7_with_receipt_nop.map(&:deposit).inject(:+).to_i
    @business_days3to7_with_receipt_op_deposit = @business_days3to7_with_receipt_op.map(&:deposit).inject(:+).to_i
    @business_days3to7_without_receipt = BookingGroup.in_between(@business_days3to7_interval).without_receipt.order("booking_groups.created_at desc")
    @business_days3to7_without_receipt_deposit = @business_days3to7_without_receipt.map(&:deposit).inject(:+).to_i
    @business_days3to7_without_receipt_pax = Booking.where(booking_group_id: @business_days3to7_without_receipt.ids).visible.count

    @business_days8to15_interval = 15.business_days.ago.change({hour: 9})...7.business_days.ago.change({hour: 9})
    @business_days8to15_with_receipt_nop = BookingGroup.in_between(@business_days8to15_interval).with_receipt.non_overpaid.order("booking_groups.created_at desc")
    @business_days8to15_with_receipt_op = BookingGroup.in_between(@business_days8to15_interval).with_receipt.overpaid.order("booking_groups.created_at desc")
    @business_days8to15_with_receipt_pax = Booking.where(booking_group_id: @business_days8to15_with_receipt_nop.ids + @business_days8to15_with_receipt_op).visible.count
    @business_days8to15_with_receipt_nop_deposit = @business_days8to15_with_receipt_nop.map(&:deposit).inject(:+).to_i
    @business_days8to15_with_receipt_op_deposit = @business_days8to15_with_receipt_op.map(&:deposit).inject(:+).to_i
    @business_days8to15_without_receipt = BookingGroup.in_between(@business_days8to15_interval).without_receipt.order("booking_groups.created_at desc")
    @business_days8to15_without_receipt_deposit = @business_days8to15_without_receipt.map(&:deposit).inject(:+).to_i
    @business_days8to15_without_receipt_pax = Booking.where(booking_group_id: @business_days8to15_without_receipt.ids).visible.count


    @business_days16to30_interval = 30.business_days.ago.change({hour: 9})...15.business_days.ago.change({hour: 9})
    @business_days16to30_with_receipt_nop = BookingGroup.in_between(@business_days16to30_interval).with_receipt.non_overpaid.order("booking_groups.created_at desc")
    @business_days16to30_with_receipt_op = BookingGroup.in_between(@business_days16to30_interval).with_receipt.overpaid.order("booking_groups.created_at desc")
    @business_days16to30_with_receipt_pax = Booking.where(booking_group_id: @business_days16to30_with_receipt_nop.ids + @business_days16to30_with_receipt_op).visible.count
    @business_days16to30_with_receipt_nop_deposit = @business_days16to30_with_receipt_nop.map(&:deposit).inject(:+).to_i
    @business_days16to30_with_receipt_op_deposit = @business_days16to30_with_receipt_op.map(&:deposit).inject(:+).to_i
    @business_days16to30_without_receipt = BookingGroup.in_between(@business_days16to30_interval).without_receipt.order("booking_groups.created_at desc")
    @business_days16to30_without_receipt_deposit = @business_days16to30_without_receipt.map(&:deposit).inject(:+).to_i
    @business_days16to30_without_receipt_pax = Booking.where(booking_group_id: @business_days16to30_without_receipt.ids).visible.count


  end

  def tour_booking_summary
    @today = DateTime.current
    keywords = params[:keywords].present? ? params[:keywords] : ''
    @booked_by = params[:booked_by].present? ? params[:booked_by] : ''

    if params[:begin].present? && params[:end].present?
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      @interval = DateTime.new.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:commit].blank?
      params[:begin] = (DateTime.now.beginning_of_month - 2.months).strftime("%F")
      params[:end] = DateTime.now.end_of_month.strftime("%F")
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    else
      @interval = DateTime::Infinity.new...DateTime::Infinity.new
    end

    @booking_datas = Booking.visible.in_between_booked(@interval).search_agent(@booked_by).where(tour_id: Tour.includes(bookings: [:booking_group]).search(keywords).search_agent(@booked_by).with_bookings(true).in_between_bookings(@interval).ids).calculate_all(:count, :sum_price)
    @tours = Tour.includes(bookings: [:booking_group]).search(keywords).search_agent(@booked_by).with_bookings(true).in_between_bookings(@interval).page(params[:page]).per(Tour::ADMIN_TOURS_PER_PAGE).order("tours.code")
  end

  def booking_statistic
    if params[:group_by].blank? || !Booking::SUMMARY_GROUP.include?(params[:group_by])
      params[:group_by] = 'month'
    end
    group_by = params[:group_by]

    if params[:begin].present? && params[:end].present?
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day..DateTime.now.end_of_day
    elsif params[:end].present?
      @interval = Booking.first_book.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:commit].blank?
      params[:begin] = (DateTime.now.beginning_of_month - 2.months).strftime("%F")
      params[:end] = DateTime.now.end_of_month.strftime("%F")
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    else
      @interval = Booking.first_book..DateTime.now.end_of_day
    end
    @booking_datas = Booking.visible.group_by_period(group_by, :created_at, range: @interval).calculate_all(:count, :sum_price)
    @total_booking = @booking_datas.values.delete_if{|x| x == 0}.map{|x| x[:count]}.inject(:+)
    @total_amount = @booking_datas.values.delete_if{|x| x == 0}.map{|x| x[:sum_price]}.inject(:+)
  end

  def sales_statistic
    if params[:group_by].blank?
      params[:group_by] = 'Sales Force'
    end
    if params[:begin].present? && params[:end].present?
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      @interval = DateTime.new.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:commit].blank?
      params[:begin] = (DateTime.now.beginning_of_month - 2.months).strftime("%F")
      params[:end] = DateTime.now.end_of_month.strftime("%F")
      @interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    else
      @interval = DateTime::Infinity.new...DateTime::Infinity.new
    end

    if params[:begin_departure_date].present? && params[:end_departure_date].present?
      @interval_departure_date = "#{params[:begin_departure_date]}".in_time_zone.beginning_of_day.."#{params[:end_departure_date]}".in_time_zone.end_of_day
    elsif params[:begin_departure_date].present?
      @interval_departure_date = "#{params[:begin_departure_date]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end_departure_date].present?
      @interval_departure_date = DateTime.new.."#{params[:end_departure_date]}".in_time_zone.end_of_day
    elsif params[:commit].blank?
      params[:begin_departure_date] = (DateTime.now.beginning_of_month - 2.months).strftime("%F")
      params[:end_departure_date] = DateTime.now.end_of_month.strftime("%F")
      @interval_departure_date = "#{params[:begin_departure_date]}".in_time_zone.beginning_of_day.."#{params[:end_departure_date]}".in_time_zone.end_of_day
    else
      @interval_departure_date = DateTime::Infinity.new...DateTime::Infinity.new
    end

    if params[:group_by] == 'Sales Force'
      @sales_data = Booking.in_between_departure_date(@interval_departure_date).in_between_booked(@interval).group(:agent_user_id).calculate_all(:count, :sum_price)
      @sales_withdrawn_data = Booking.in_between_departure_date(@interval_departure_date).withdrawn.in_between_booked(@interval).group(:agent_user_id).calculate_all(:count, :sum_price)
      @merge_data = @sales_data.merge(@sales_withdrawn_data) {|k, v1, v2| [[:count, v1[:count] - v2[:count]],[ :sum_price, v1[:sum_price] - v2[:sum_price]]].to_h }.sort_by{|k, v| v[:sum_price]}.reverse.to_h
      @users_mapping = User.where(id: @merge_data.keys).select(:id, :username, :role).map{|x| [x.id, Hash[:username, x.username, :role, x.role]]}.to_h
      @total_amount = @merge_data.values.map{|x| x[:sum_price]}.inject(:+)
      @total_booking = @sales_data.values.map{|x| x[:count]}.inject(:+)
    elsif params[:group_by] == 'State'
      @sales_data = Booking.visible.joins(:agent_user).in_between_departure_date(@interval_departure_date).in_between_booked(@interval).group('users.state').calculate_all(:count, :sum_price).sort_by{|k, v| v[:sum_price]}.reverse.to_h
      @sales_withdrawn_data = Booking.visible.joins(:agent_user).in_between_departure_date(@interval_departure_date).withdrawn.in_between_booked(@interval).group('users.state').calculate_all(:count, :sum_price).sort_by{|k, v| v[:sum_price]}.reverse.to_h
      @total_amount = @sales_data.values.map{|x| x[:sum_price]}.inject(:+)
      @total_booking = @sales_data.values.map{|x| x[:count]}.inject(:+)
    elsif params[:group_by] == 'Destination'
      @sales_data = Booking.visible.joins(:tour).in_between_departure_date(@interval_departure_date).in_between_booked(@interval).group('tours.contient_country').calculate_all(:count, :sum_price).sort_by{|k, v| v[:sum_price]}.reverse.to_h
      @total_amount = @sales_data.values.map{|x| x[:sum_price]}.inject(:+)
      @total_booking = @sales_data.values.map{|x| x[:count]}.inject(:+)
    end
  end

  def insurance_changes
     @today = DateTime.current
    insurance_endorse_count =  params[:insurance_endorse_count].present? ? params[:insurance_endorse_count] : ""
    search_name = params[:search_name].present? ? params[:search_name]: ''

    if params[:begin].present? && params[:end].present?
      booked_interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      booked_interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      booked_interval = DateTime.new.."#{params[:end]}".in_time_zone.end_of_day
    else
      booked_interval = DateTime::Infinity.new...DateTime::Infinity.new
    end


    if params[:begin_insurance_exported_at].present? && params[:end_insurance_exported_at].present?
      exported_interval = "#{params[:begin_insurance_exported_at]}".in_time_zone.beginning_of_day.."#{params[:end_insurance_exported_at]}".in_time_zone.end_of_day
    elsif params[:begin_insurance_exported_at].present?
      exported_interval = "#{params[:begin_insurance_exported_at]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end_insurance_exported_at].present?
      exported_interval = DateTime.new.."#{params[:end_insurance_exported_at]}".in_time_zone.end_of_day
    else
      exported_interval = DateTime::Infinity.new...DateTime::Infinity.new
    end

    if params[:begin_cut_off_date].present? && params[:end_cut_off_date].present?
      exported_interval = "#{params[:begin_insurance_cut_off_date]}".in_time_zone.beginning_of_day.."#{params[:end_cut_off_date]}".in_time_zone.end_of_day
    elsif params[:begin_insurance_cut_off_date].present?
      exported_interval = "#{params[:begin_insurance_cut_off_date]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end_cut_off_date].present?
      exported_interval = DateTime.new.."#{params[:end_cut_off_date]}".in_time_zone.end_of_day
    else
      exported_interval = DateTime::Infinity.new...DateTime::Infinity.new
    end

    if params[:insurance_include].present? 
      cut_off_interval = params[:insurance_include] 
    else
      cut_off_interval = false
    end

    @bookings = Booking.where("insurance_exported_at < updated_at").visible.in_between_booked(booked_interval).visible.insurance_exported_at(exported_interval).search_insurance_endorse_count(insurance_endorse_count).search_name(search_name).with_insurance_include(cut_off_interval).page(params[:page]).per(Booking::ADMIN_BOOKINGS_PER_PAGE)
  end

  def create_insurance_endorse
    @booking_group = BookingGroup.find(params[:id])
    authorize @booking_group

    if @booking_group.export_insurance(current_user, "update")
      redirect_to admin_booking_group_path(@booking_group), notice: 'Insurance for BookingGroup was successfully endorsed.'
    else
      flash[:error] = "Failed to endorse insurance, please try again later or check on chubb system."
      redirect_to admin_booking_group_path(@booking_group)
    end
  end

  def create_insurance_export
    @booking_group = BookingGroup.find(params[:id])
    authorize @booking_group

    if @booking_group.export_insurance(current_user, "new")
      redirect_to admin_booking_group_path(@booking_group), notice: 'Insurance for BookingGroup was successfully exported.'
    else
      flash[:error] = "Failed to export insurance, please try again later or check on chubb system."
      redirect_to admin_booking_group_path(@booking_group)
    end
  end

  def proforma_invoice
    @booking_group = BookingGroup.includes(bookings: [:agent_user, :tour]).find(params[:id])
    @tour = @booking_group.tour
    authorize @booking_group
    if @booking_group.export_proforma_invoice(current_user, params[:attention], params[:consultant])
      render :print
    else
      render :print
    end
  end

  def assign_agent
    @booking_group = BookingGroup.find(params[:id])
    authorize @booking_group
    agent_user = User.find_by_username params[:agent_username]
    if agent_user.present?
      @booking_group.update_agent_user(agent_user)
      redirect_to admin_booking_group_path(@booking_group), notice: 'Agent is successfully assigned!'
    else
      flash[:error] = "The agent does not exist!"
      redirect_to admin_booking_group_path(@booking_group)
    end
  end

private
  def create_booking_group_params
  end

  def update_booking_group_params
    Rails.logger.info "params: #{params}"
    new_params = params.require(:booking_group).permit(:deposit, :insurance_receiver, receipts_attributes: [:id, :number, :amount, :_destroy], bookings_attributes: [:id, :category, :price, :price_type, :designation, :name, :mobile, :insurance_confirmation, :nationality, :visa_no, :visa_expiry_date, :operator_remark, :passport_number, :passport_expiry_date, :passport_photocopy, :remove_passport_photocopy, :date_of_birth, :room_type, :remark, :admin_remark, :created_at, :guardian_id, :ph_designation, :ph_name, :ph_dob, :ph_id_number, :insurance_nomination_flag, :witness_surname, :witness_given_name, :witness_passport_number, :mighty, infant_bookings_attributes: [:id, :name, :passport_number, :passport_expiry_date, :passport_photocopy, :remove_passport_photocopy, :date_of_birth, :gender, :remark, :insurance_confirmation, :nationality, :parent_id, :ph_designation, :ph_name, :ph_dob, :ph_id_number, :insurance_nomination_flag, :witness_surname, :witness_given_name, :witness_passport_number, :_destroy]])
    
    if new_params[:bookings_attributes].present? && params[:nominee_details].present?
      new_params[:bookings_attributes].each do |key, b|
        all_nominee = []
        for i in 0..2
          details = {
            nominee_surname: params[:nominee_details][b[:id]]["nominee_surname"][i],
            nominee_given_name: params[:nominee_details][b[:id]]["nominee_given_name"][i],
            nominee_passport_number: params[:nominee_details][b[:id]]["nominee_passport_number"][i],
            nominee_date_of_birth: params[:nominee_details][b[:id]]["nominee_date_of_birth"][i],
            nominee_relationship: params[:nominee_details][b[:id]]["nominee_relationship"][i],
            nominee_share: params[:nominee_details][b[:id]]["nominee_share"][i],
          }
          all_nominee << details
        end
        b.merge!(edited_user_id: current_user.id, insurance_nomination: all_nominee)
      end
      # new_params[:bookings_attributes].each {|k,v| v.merge!(edited_user_id: current_user.id)}
    end

    # dddddddd
    new_params.merge(edited_user_id: current_user.id)
  end

  def create_booking_alteration_params
    params.require(:booking_alteration).permit(:alter_type, :reason)
  end

  def create_export_params
    params.require(:booking_group).permit(bookings_attributes: [:id, :tour_fee, :DTA, :PRE, :visa_fee, :insurance_fee])
  end

  def check_auth
    authorize BookingGroup
  end

end
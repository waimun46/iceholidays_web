class Admin::HolidaysController < AdminController
  before_action :check_auth

  def index
    @holidays = Holiday.page(params[:page]).per(50)
  end

  def new
    @holiday = Holiday.new
  end

  def create
    @holiday = Holiday.new(holiday_params)

    if @holiday.save
      redirect_to admin_holidays_path, notice: 'Holiday was successfully created.'
    else
      render :new
    end
  end

  def show
    @holiday = Holiday.find(params[:id])
  end

  def edit
    @holiday = Holiday.find(params[:id])
  end

  def update
    @holiday = Holiday.find(params[:id])

    if @holiday.update(holiday_params)
      redirect_to admin_holidays_path, notice: 'Holiday was successfully updated.'
    else
      render :edit
    end
  end

private
  def holiday_params
    params.require(:holiday).permit(:year, :dates_preset).merge(by_preset: true)
  end

  def check_auth
    authorize Holiday, :index?
  end
end
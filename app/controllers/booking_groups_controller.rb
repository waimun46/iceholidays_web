class BookingGroupsController < ApplicationController

  def index
    authorize BookingGroup
    if params[:begin].present? && params[:end].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      interval = DateTime.new.."#{params[:end]}".in_time_zone.end_of_day
    else
      interval = DateTime::Infinity.new...DateTime::Infinity.new
    end
    if params[:begin_departure_date].present? && params[:end_departure_date].present?
      interval_departure_date = "#{params[:begin_departure_date]}".in_time_zone.beginning_of_day.."#{params[:end_departure_date]}".in_time_zone.end_of_day
    elsif params[:begin_departure_date].present?
      interval_departure_date = "#{params[:begin_departure_date]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end_departure_date].present?
      interval_departure_date = DateTime.new.."#{params[:end_departure_date]}".in_time_zone.end_of_day
    else
      interval_departure_date = DateTime::Infinity.new...DateTime::Infinity.new
    end


    customer_name = params[:name].present? ? params[:name] : ''
    contient_country = params[:contient_country].present? ? params[:contient_country] : ''
    tour_code = params[:tour_code].present? ? params[:tour_code] : ''

    @booking_groups = current_user.booking_groups.includes(bookings: [:tour, :agent_user]).in_between(interval).in_between_departure_date(interval_departure_date).search_name(customer_name).search_destination(contient_country).search_tour_code(tour_code).with_booking.order("booking_groups.created_at desc").page(params[:page]).per(50)
    @booking_datas = current_user.bookings.visible.in_between_booked(interval).in_between_departure_date(interval_departure_date).search_name(customer_name).search_destination(contient_country).search_tour_code(tour_code).calculate_all(:count, :sum_price)
    if params[:print].present?
      render :print
    else
      render :index
    end
  end

  def show
    @booking_group = current_user.booking_groups.with_booking.find(params[:id])
    authorize @booking_group
    @tour = @booking_group.tour
  end

  def edit
    @booking_group = current_user.booking_groups.with_booking.find(params[:id])
    authorize @booking_group
    @tour = @booking_group.tour
    if @tour.cut_off?
      redirect_to booking_group_path(@booking_group), notice: 'Cannot edit after the cut off date, please contact the operator for assistance'
    end
  end

  def update
    @booking_group = current_user.booking_groups.with_booking.find(params[:id])
    authorize @booking_group
    @tour = @booking_group.tour
    @booking_group.assign_attributes(update_booking_group_params)

    if @tour.cut_off?
      redirect_to booking_group_path(@booking_group), notice: 'Cannot update after the cut off date, please contact the operator for assistance'
    elsif !params[:booking_group][:bookings_attributes].values.any?{|b| b[:category]=='adult'}
      @booking_group.valid?
      @booking_group.errors[:base] << "Bookings must at least have one adult."
      render :edit
    elsif @booking_group.save
      redirect_to booking_group_path(@booking_group), notice: 'All bookings was successfully updated.'
    else
      render :edit
    end
  end

  def create_insurance_export
    @booking_group = BookingGroup.find(params[:id])
    authorize @booking_group

    if @booking_group.export_insurance(current_user, "new")
      redirect_to booking_group_path(@booking_group), notice: 'Insurance for BookingGroup was successfully exported.'
    else
      flash[:error] = "Failed to export insurance, please try again later or check on chubb system."
      redirect_to booking_group_path(@booking_group)
    end
  end

  def booking_rooms
    @booking_group = current_user.booking_groups.with_booking.find(params[:id])
    # authorize @booking_group
    @tour = @booking_group.tour
  end

  def edit_booking_rooms
    @booking_group = current_user.booking_groups.with_booking.find(params[:id])
    authorize @booking_group
    @tour = @booking_group.tour
    @booking_adult_child_twin = @booking_group.bookings.visible.adult_child_twin
    @booking_child_with_and_without_bed = @booking_group.bookings.visible.where(category:[:child_with_bed, :child_no_bed])
  end

  def update_booking_rooms
    @booking_group = current_user.booking_groups.with_booking.find(params[:id])
    authorize @booking_group
    @tour = @booking_group.tour
    if booking_room_params.present?
      booking_ids = booking_room_params[:booking_rooms_attributes].values.map{|br|br[:bookings].values}.flatten
      if booking_ids.uniq.count == booking_ids.count && booking_ids.count == @booking_group.bookings.visible.count
        create_booking_room = BookingRoom.create_booking_room(@tour, @booking_group.bookings.ids, booking_room_params.to_h)
        # Rails.logger.info "Check ---> #{create_booking_room}"
        if create_booking_room
          # Rails.logger.info "Result ---> Booking Success!"
          redirect_to booking_group_path(@booking_group), notice: 'Booking room was successfully updated.'
        else
          # Rails.logger.info "Error ---> Booking Failed!"
          redirect_to edit_booking_rooms_booking_group_path(@booking_group), notice: 'Booking room was successfully updated.'
        end
      else
        # Rails.logger.info "Error ---> Please assign all guest!"
        redirect_to edit_booking_rooms_booking_group_path(@booking_group), notice: 'Please assign all guest!'
      end
    end
  end

  def create_gift_tracking
    booking_group = current_user.booking_groups.with_booking.find(params[:id])
    authorize booking_group
    gift_tracking_group = GiftTrackingGroup.new(agent_id: current_user.id, category: GiftTrackingGroup.categories[:no_category], cut_off_date: (booking_group.created_at+2.weeks))
    gift_tracking_group.gift_trackings << GiftTracking.new(point_topup: booking_group.redemption_points, booking_group_id: booking_group.id)
    
    if gift_tracking_group.save
      #   Rails.logger.info "RESULT --> Gift tracking was successfully created."
      redirect_to gift_tracking_group_path(gift_tracking_group), notice: 'Gift tracking was successfully created.'
    else
      #   Rails.logger.info "RESULT --> Gift tracking group not created!"
      redirect_to booking_group_path(params[:id]), notice: 'Gift tracking group not created!'
    end
  end

  def invoice
    @booking_group = current_user.booking_groups.with_booking.find(params[:id])
    @bookings = @booking_group.bookings
    @tour = @booking_group.tour
    @etravel_invoice = @booking_group.etravel_invoices.find(params[:invoice_id])
    render :invoice
  end

private
  def update_booking_group_params
    new_params = params.require(:booking_group).permit(:insurance_receiver, bookings_attributes: [:id, :category, :designation, :name, :mobile, :insurance_confirmation, :nationality, :visa_no, :passport_number, :passport_expiry_date, :passport_photocopy, :date_of_birth, :room_type, :remark, :guardian_id, :ph_designation, :ph_name, :ph_dob, :ph_id_number, :insurance_nomination_flag, :witness_surname, :witness_given_name, :witness_passport_number, infant_bookings_attributes: [:id, :name, :passport_number, :passport_expiry_date, :passport_photocopy, :date_of_birth, :gender, :remark, :insurance_confirmation, :nationality, :parent_id, :ph_designation, :ph_name, :ph_dob, :ph_id_number, :insurance_nomination_flag, :witness_surname, :witness_given_name, :witness_passport_number, :_destroy]])

    if new_params[:bookings_attributes].present? && params[:nominee_details].present?
      new_params[:bookings_attributes].each do |key, b|
        all_nominee = []
        for i in 0..3
          details = {
            nominee_surname: params[:nominee_details][b[:id]]["nominee_surname"][i],
            nominee_given_name: params[:nominee_details][b[:id]]["nominee_given_name"][i],
            nominee_passport_number: params[:nominee_details][b[:id]]["nominee_passport_number"][i],
            nominee_date_of_birth: params[:nominee_details][b[:id]]["nominee_date_of_birth"][i],
            nominee_relationship: params[:nominee_details][b[:id]]["nominee_relationship"][i],
            nominee_share: params[:nominee_details][b[:id]]["nominee_share"][i],
          }
          all_nominee << details
        end
        b.merge!(edited_user_id: current_user.id, insurance_nomination: all_nominee)
      end
      # new_params[:bookings_attributes].each {|k,v| v.merge!(edited_user_id: current_user.id)}
    end
    new_params
  end

  def booking_room_params
    params.require(:tour).permit(booking_rooms_attributes: [:id, :room_type, :_destroy, bookings: [:guest_1, :guest_2, :guest_3, :guest_4, :_destroy]])
  end

end
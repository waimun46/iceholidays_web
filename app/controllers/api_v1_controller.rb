class ApiV1Controller < ApplicationController
  before_action :set_csrf_cookie

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

private

  def set_csrf_cookie
    cookies["CSRF-TOKEN"] = form_authenticity_token
  end

  def authorize(record, query = nil)
    super([:api, :v1, record], query)
  end

  def user_not_authorized
    render json: { error: "You are not authorized to perform this action." }
  end

end
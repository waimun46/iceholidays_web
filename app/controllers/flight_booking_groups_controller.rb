class FlightBookingGroupsController < ApplicationController
  before_action :check_auth
  def show
    @flight_booking_group = current_user.flight_booking_groups.includes(:flight_booking_details, flight_bookings: [:agent_user]).find(params[:id])
  end

  def index
    @today = DateTime.current
    if params[:begin].present? && params[:end].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      interval = DateTime.new.."#{params[:end]}".in_time_zone.end_of_day
    else
      interval = DateTime::Infinity.new...DateTime::Infinity.new
    end

    if params[:begin].present? || params[:end].present?
      @booking_datas = current_user.flight_booking_groups.in_between(interval).calculate_all(:count, :sum_price)
    end

    @flight_booking_groups = current_user.flight_booking_groups.includes(:flight_booking_details, flight_bookings: [:agent_user]).in_between(interval).order("flight_booking_groups.created_at desc ").page(params[:page]).per(BookingGroup::ADMIN_BOOKINGGROUPS_PER_PAGE)

  end

  def invoice
    @flight_booking_group = current_user.flight_booking_groups.find(params[:id])
    @flight_bookings = @flight_booking_group.flight_bookings
  end

private
  def check_auth
    authorize FlightBookingGroup, :index?
  end
end
class AttractionsController < ApplicationController

  def index
    @countries, countries_err = Globaltix::Country.all

    if params[:country_id].blank?
      params[:country_id] = 1
    end
    if params[:page].blank?
      params[:page] = 1
    end

    @attractions, attractions_err = Globaltix::Attraction.where(countryId: params[:country_id], page: params[:page])
  end

  def show
  end


end
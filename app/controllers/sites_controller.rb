class SitesController < ApplicationController
  layout 'application_react', except: [:dashboard]

  def dashboard
    # Only for iceb2b
    if @new_domain
      redirect_to app_path
    end
  end

  def app
    # Only for travelb2b
    if !@new_domain
      redirect_to dashboard_path
    end
  end

end
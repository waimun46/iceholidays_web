class LandTourBookingGroupsController < ApplicationController

  def index
    authorize LandTourBookingGroup

    @today = DateTime.current
    if params[:begin].present? && params[:end].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      interval = DateTime.new.."#{params[:end]}".in_time_zone.end_of_day
    else
      interval = DateTime::Infinity.new...DateTime::Infinity.new
    end
    if params[:begin_departure_date].present? && params[:end_departure_date].present?
      interval_departure_date = "#{params[:begin_departure_date]}".in_time_zone.beginning_of_day.."#{params[:end_departure_date]}".in_time_zone.end_of_day
    elsif params[:begin_departure_date].present?
      interval_departure_date = "#{params[:begin_departure_date]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end_departure_date].present?
      interval_departure_date = DateTime.new.."#{params[:end_departure_date]}".in_time_zone.end_of_day
    else
      interval_departure_date = DateTime::Infinity.new...DateTime::Infinity.new
    end

    customer_name = params[:name].present? ? params[:name] : ''

    @land_tour_booking_groups = current_user.land_tour_booking_groups.includes(:land_tour_bookings).visible.in_between_booked(interval).in_between_departure_date(interval_departure_date).search_name(customer_name).with_booking.order("land_tour_booking_groups.created_at desc ").page(params[:page]).per(50)
    @booking_datas = current_user.land_tour_bookings.visible.in_between_booked(interval).in_between_departure_date(interval_departure_date).search_name(customer_name).calculate_all(:count, :sum_price)
  end

  def show
    @land_tour_booking_group = current_user.land_tour_booking_groups.includes(:land_tour_bookings).find(params[:id])
    @land_tour = @land_tour_booking_group.land_tour
    authorize @land_tour_booking_group
  end

  def edit
    @land_tour_booking_group = current_user.land_tour_booking_groups.find(params[:id])
    @land_tour = @land_tour_booking_group.land_tour
    authorize @land_tour_booking_group
  end

  def update
    @land_tour_booking_group = current_user.land_tour_booking_groups.find(params[:id])
    authorize @land_tour_booking_group
    @land_tour_booking_group.assign_attributes(update_booking_group_params)

    if @land_tour_booking_group.save
      redirect_to land_tour_booking_group_path(@land_tour_booking_group), notice: 'All bookings was successfully updated.'
    else
      render :edit
    end
  end

  def invoice
    @land_tour_booking_group = LandTourBookingGroup.includes(:etravel_invoices).find(params[:id])
    authorize @land_tour_booking_group
    @land_tour_booking = @land_tour_booking_group.land_tour_bookings.includes(:agent_user).first
    @etravel_invoice = @land_tour_booking_group.etravel_invoices.find(params[:invoice_id])
  end

  def voucher
    @land_tour_booking_group = LandTourBookingGroup.includes(:land_tour_bookings).find(params[:id])
    @land_tour = @land_tour_booking_group.land_tour
    @land_tour_booking = @land_tour_booking_group.land_tour_bookings.first
    @user = @land_tour_booking_group.agent_user
  end

private
  def update_booking_group_params
    new_params = params.require(:land_tour_booking_group).permit(:flight_departure_details, :flight_return_details, land_tour_bookings_attributes: [:id, :price, :category, :designation, :name, :date_of_birth, :mobile, :nationality, :remark])
    if new_params[:land_tour_bookings_attributes].present?
      new_params[:land_tour_bookings_attributes].each {|k,v| v.merge!(last_edited_user_id: current_user.id)}
    end
    new_params
  end
end

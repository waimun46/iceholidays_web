class ActivityBookingsController < ApplicationController
  def new
    authorize ActivityBooking
    @activity = Activity.find(params[:activity_id])
    @activity_booking_group = ActivityBookingGroup.new
    @payment = Payment.new
    create_bookings_array
    @banklist = @payment.fpx_banklist
    @offline_banklist = @payment.offline_banklist(@banklist)
  end

  def create
    authorize ActivityBooking
    @activity = Activity.find(params[:activity_id])
    @payment = Payment.new
    @banklist = @payment.fpx_banklist
    @offline_banklist = @payment.offline_banklist(@banklist)
    
    price_qty = process_qty_params(@activity)
    
    if price_qty.present?
      @activity_booking_group = ActivityBooking.bulk_create(@activity, price_qty, current_user, params[:travel_date], booking_params)
      if @activity_booking_group.id.present?
        @booking_payment = Payment.new(payment_params)
        if @booking_payment.save
          redirect_to proceed_to_payment_path(:payment_id => @booking_payment.id)
        else
          create_bookings_array
          @payment
          render :new
        end
      else
        create_bookings_array
        @payment
        render :new
      end
    else
    end
  end

private
  def price_qty_params
    qty_prices = process_qty_params(@activity)
    {'' => qty_prices}
  end

  def process_qty_params(activity)
    activity.activity_prices.map do |price|
      qty = params["price_#{price.category}".to_sym]
      ["#{price.category}", qty.to_i]
    end.reject(&:blank?).to_h
  end

  def booking_params
    params.require(:activity_bookings).map{|b| b.permit(:name, :mobile, :email, :remark)}
  end

  def payment_params
    params.require(:payment).permit(:gateway, :method).merge!(amount: @activity_booking_group.price, taggable: @activity_booking_group, callback: 0, user_id: current_user.id)
  end

  def create_bookings_array
    @activity_bookings = []
    @activity_bookings << ActivityBooking.new
  end
end
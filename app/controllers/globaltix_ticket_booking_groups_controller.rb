class GlobaltixTicketBookingGroupsController < ApplicationController
  before_action :check_auth, except: [:invoice]
  def show
    @globaltix_ticket_booking_group = current_user.globaltix_ticket_booking_groups.includes(globaltix_ticket_bookings: [:agent_user]).find(params[:id])
  end

  def index
    @today = DateTime.current
    if params[:begin].present? && params[:end].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day.."#{params[:end]}".in_time_zone.end_of_day
    elsif params[:begin].present?
      interval = "#{params[:begin]}".in_time_zone.beginning_of_day...DateTime::Infinity.new
    elsif params[:end].present?
      interval = DateTime.new.."#{params[:end]}".in_time_zone.end_of_day
    else
      interval = DateTime::Infinity.new...DateTime::Infinity.new
    end

    if params[:begin].present? || params[:end].present?
      @booking_datas = current_user.globaltix_ticket_booking_groups.in_between_booked(interval).calculate_all(:count, :sum_price)
    end

    @globaltix_ticket_booking_groups = current_user.globaltix_ticket_booking_groups.includes(globaltix_ticket_bookings: [:agent_user]).in_between(interval).order("globaltix_ticket_booking_groups.created_at desc ").distinct.page(params[:page]).per(BookingGroup::ADMIN_BOOKINGGROUPS_PER_PAGE)

  end

  def thank_you
    @globaltix_ticket_booking_group = current_user.globaltix_ticket_booking_groups.find(params[:id])
    @globaltix_ticket_booking = @globaltix_ticket_booking_group.globaltix_ticket_bookings.first
  end

  def invoice
    # @globaltix_ticket_booking_group = current_user.globaltix_ticket_booking_groups.find(params[:id])
    @globaltix_ticket_booking_group = GlobaltixTicketBookingGroup.find(params[:id])
    authorize @globaltix_ticket_booking_group
    @globaltix_ticket_bookings = @globaltix_ticket_booking_group.globaltix_ticket_bookings
  end

private
  def check_auth
    authorize GlobaltixTicketBookingGroup, :index?
  end
end
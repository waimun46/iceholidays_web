class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :exception
  before_action :authenticate_user!, :set_variables

private
  def after_sign_in_path_for(resource)
    if @new_domain
      app_path
    elsif current_user.able_access_admin_dashboard?
      admin_dashboard_path
    else
      dashboard_path
    end
  end

  rescue_from ActionController::InvalidAuthenticityToken, with: :redirect_to_referer_or_path

  def redirect_to_referer_or_path
    flash[:notice] = "The session is timeout, please try again."
    redirect_back(fallback_location: root_path)
  end

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def user_not_authorized
    flash[:warning] = "You are not authorized to perform this action."
    if current_user.able_access_admin_dashboard?
      redirect_to admin_dashboard_path
    else
      redirect_to dashboard_path
    end
  end

  def pundit_user
    UserDomain.new(current_user, @new_domain)
  end

  def set_variables
    if request.domain.present?
      if Rails.env.iceb2b?
        @new_domain = request.domain.include?('travelb2b')
      elsif Rails.env.testb2b?
        @new_domain = request.domain(3).match?('test\d')
      else
        @new_domain = request.domain.include?('iceholidays2')
      end
    else
      @new_domain = false
    end
    if @new_domain
      @app_name = 'Travelb2b'
    else
      @app_name = Rails.application.secrets.app_name
    end
  end

end

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  attr_accessor :edited_user_id, :edited_user_password
  devise :database_authenticatable, :recoverable, :trackable, :lockable, :authentication_keys => [:username]

  belongs_to :last_edited_user, class_name: "User", optional: true
  belongs_to :last_reset_password_edited_user, class_name: "User", optional: true
  belongs_to :agency, optional: true

  enum role: [:guest, :subagent, :agent, :admin, :boss, :distributer, :warehouse]

  DOMAINS = [
    'iceb2b',
    'travelb2b'
  ]

  COMMISSION_TYPES = [
    'wifi_commission',
    'activity_commission',
    'globaltix_commission',
    'series_commission',
    'flight_commission',
    'hotel_commission',
    'land_tour_commission',
  ]


  typed_store :commission_details, coder: PostgresCoder do |s|
    COMMISSION_TYPES.each do |ct|
      s.integer ct, blank: false, default: 0
    end
  end

  typed_store :travelb2b_module_access, coder: PostgresCoder do |s|
    s.boolean :access_flight, default: false, null: false
    s.boolean :access_wifi, default: false, null: false
    s.boolean :access_series, default: false, null: false
    s.boolean :access_activity, default: false, null: false
    s.boolean :access_hotel, default: false, null: false
    s.boolean :access_busferry, default: false, null: false
    s.boolean :access_land_tour, default: false, null: false
  end

  validates :username, :code, :role, presence: true
  validates :username, format: { with: /\A[A-Za-z0-9_\-]+\z/, message: "only allows letters" }
  if !Rails.configuration.crawling
    validates :state, presence: true
  end
  validates :code, uniqueness: { case_sensitive: false }
  validates :username, uniqueness: { scope: :agency_id, case_sensitive: false }
  if !Rails.configuration.crawling
    validates :full_name, uniqueness: { case_sensitive: false }
  end
  validates :domains, array: { inclusion: { in: User::DOMAINS } }

  has_many :bookings, class_name: "Booking", foreign_key: "agent_user_id"
  has_many :booking_groups, -> { distinct }, through: :bookings
  has_many :flight_bookings, class_name: "FlightBooking", foreign_key: "agent_user_id"
  has_many :flight_booking_groups, -> { distinct }, through: :flight_bookings
  has_many :globaltix_ticket_bookings, class_name: "GlobaltixTicketBooking", foreign_key: "agent_user_id"
  has_many :globaltix_ticket_booking_groups, -> { distinct }, through: :globaltix_ticket_bookings
  has_many :sessions
  has_many :gift_tracking_groups, class_name: "GiftTrackingGroup", foreign_key: "agent_id"
  has_many :credit_transactions
  has_many :credit_refunds
  has_many :credit_purchases
  has_many :roamingman_bookings, class_name: "RoamingmanBooking", foreign_key: "agent_user_id"
  has_many :roamingman_booking_groups, -> { distinct }, through: :roamingman_bookings
  has_many :activity_bookings, class_name: "ActivityBooking", foreign_key: "agent_user_id"
  has_many :activity_booking_groups, -> { distinct }, through: :activity_bookings
  has_many :payments
  has_many :tbo_bookings, class_name: "TBOBooking", foreign_key: "agent_user_id"
  has_many :land_tour_bookings, class_name: "LandTourBooking", foreign_key: "agent_user_id"
  has_many :land_tour_booking_groups, -> { distinct }, through: :land_tour_bookings

  scope :in_between_current_sign_in_at, ->(interval) { where(current_sign_in_at:(interval))}

  paginates_per 50 #paginate
  scope :search, ->(keyword) { where("users.code ILIKE ? or users.full_name ILIKE ? or users.other_name ILIKE ? or users.alias_name ILIKE ? or users.username ILIKE ?", "%#{keyword}%", "%#{keyword}%", "%#{keyword}%", "%#{keyword}%", "%#{keyword}%") }
  scope :active, -> { where("users.is_active = true") }
  scope :inactive, -> { where("users.is_active = false") }
  scope :agent, -> { where("users.role = ? and users.is_active = true ", 2).order("full_name asc") }
  scope :search_state, ->(keyword) { where("users.state ILIKE ?", "%#{keyword}%") }
  scope :non_subagent, -> { where.not(role: User.roles[:subagent]) }

  before_save :update_last_edited_user, :update_last_reset_password
  after_create :create_agency, if: -> { agent? }

  def self.active_type(type)
    if type == 'Active'
      active
    elsif type == 'Inactive'
      inactive
    else
      self
    end
  end

  def self.find_by_alias_name(alias_name)
    User.where("lower(alias_name) = ?", alias_name.downcase).first
  end

  def self.find_for_authentication(warden_conditions)
    # Rails.logger.info "#{warden_conditions}"
    username, agency_name = warden_conditions[:username].split('@')
    if agency_name.present?
      user = User.joins(:agency).where(username: username).where(agencies: {name: agency_name}).first
    else
      user = User.where(username: warden_conditions[:username]).non_subagent.first
    end

    if Rails.env.iceb2b?
      request_domain = warden_conditions[:domain].split('.')[0]
    elsif Rails.env.testb2b?
      request_domain = warden_conditions[:subdomain].match?('test\d') ? User::DOMAINS[1] : User::DOMAINS[0]
    else
      request_domain = warden_conditions[:domain].split('.')[0].include?('2') ? User::DOMAINS[1] : User::DOMAINS[0]
    end


    if user.present?
      if user.boss?
        user
      else
        if user.domains.include?(request_domain) && ( !user.subagent? || (user.agency.present? && user.agency.domains.include?(request_domain) ))
          user
        else
          nil
        end
      end
    else
      nil
    end
  end

  def alias_name=(val)
    if val.blank?
      write_attribute(:alias_name, nil)
    else
      write_attribute(:alias_name, val.upcase)
    end
  end

  def create_agency
    if self.agency.blank? && self.agent?
      new_agency = Agency.create(
        code: self.code,
        name: self.username,
        full_name: self.full_name,
        other_name: self.other_name,
        alias_name: self.alias_name,
        address: self.address,
        city: self.city,
        state: self.state,
        postal: self.postal,
        country: self.country,
        phones: self.phones,
        faxes: self.faxes,
        emails: self.emails,
        website: self.website,
        remark: self.remark,
        domains: self.domains,
        max_sign_in: self.max_sign_in,
        is_active: self.is_active
      )
      self.update_columns(agency_id: new_agency.id)
    end
  end

  def full_name=(val)
    if val.blank?
      write_attribute(:full_name, nil)
    else
      write_attribute(:full_name, val.upcase)
    end
  end

  def able_access_admin_dashboard?
    warehouse? || distributer? || admin? || boss?
  end

  def email_changed?
    false
  end

  def phone
    if phones.present?
      phones[0]
    else
      ''
    end
  end

  ['phones', 'faxes', 'emails'].each do |f|
    define_method "join_#{f}" do
      if self.send(f).present?
        self.send(f).join(',')
      else
        ''
      end
    end
  end

  def active_for_authentication?
    super and self.is_active?
  end

  # def check_session_valid?(token)
  #   session = self.sessions.non_expired(Session.expired_time).find_by_token(token)
  #   if session.present?
  #     session.touch
  #   else
  #     false
  #   end
  # end

  def domain_accessible?(request)
    # Rails.logger.info "User request#{request}"
    # Rails.logger.info "User request domain #{request.domain}"
    # Rails.logger.info "User request domain(2) #{request.domain(2)}"
    # Rails.logger.info "User request domain(3) #{request.domain(3)}"

    if self.boss?
      true
    elsif Rails.env.iceb2b?
      self.domains.include?(request.domain.split('.')[0])
    elsif Rails.env.testb2b?
      if request.domain(3).split('.')[0].match?('test\d')
        self.domains.include?(User::DOMAINS[1])
      else
        self.domains.include?(User::DOMAINS[0])
      end      
    else
      if request.domain.split('.')[0].include?('2')
        self.domains.include?(User::DOMAINS[1])
      else
        self.domains.include?(User::DOMAINS[0])
      end
    end

  end

  def create_session(token, domain = '')
    expired_time = Session.expired_time
    valid_sessions = self.sessions.non_expired(expired_time)
    if !self.boss? && valid_sessions.count >= self.max_sign_in
      false
    else
      ret = self.sessions.expired(expired_time).order("random()").limit(1).update_all(token: token, updated_at: Time.now)

      if ret > 0
        # successful reuse a old session
      else
        Session.create(user: self, token: token)
      end
      true
    end
  end

  def find_session_by_token(token)
    self.sessions.find_by_token(token)
  end

  def remove_session(token)
    session = self.sessions.non_expired(Session.expired_time).find_by_token(token)
    if session.present?
      session.token = ''
      session.save
    end
  end

  def session_count
    self.sessions.non_expired(Session.expired_time).count
  end

  def topup_credits(amount, by_user, remark = "")
    CreditTransaction.create(user: self, remark: remark, amount: amount.round(2), taggable: by_user, prev: self.credit_transactions.last, user_balance: self.credits)
    User.update_counters(self.id, credits_decimal: (amount*100).to_i)
  end

  def deduct_credits(amount, by_resource, remark = "")
    CreditTransaction.create(user: self, remark: remark, amount: -amount.round(2), taggable: by_resource, prev: self.credit_transactions.last, user_balance: self.credits)
    User.update_counters(self.id, credits_decimal: (-amount*100).to_i)
  end

  def sufficient_credits?(amount)
    self.credits >= amount
  end

  def credits
    self.credits_decimal.to_f / 100
  end

  def email
    if emails.present?
      emails.first
    else
      ''
    end
  end

  def phone
    if phones.present?
      phones.first
    else
      ''
    end
  end

  def username_phone
    username_phone =  "#{alias_name.gsub("SDN BHD","")}<br>(#{ phones.first})"
    if phones.present? && full_name.present?
      username_phone
    else 
      ''
    end
  end

  def username_fullname
    " #{username}, #{full_name} "
  end

  def full_address
    "#{address}, #{city} #{postal} #{state}"
  end

  def fair_available?(type = '', itinerary_id = '')
    if self.id.present?
      @fairs_available ||= Fair.ongoing.active.allow_user(self.id).map{|f| [f.fair_type, f.allow_itinerary_ids] }.to_h
      if @fairs_available.present?
        if type.present?
          if itinerary_id.present?
            @fairs_available.has_key?(type) && @fairs_available[type].include?(itinerary_id)
          else
            @fairs_available.has_key?(type)
          end
        elsif itinerary_id.present?
          @fairs_available.values.flatten.include?(itinerary_id)
        else
          true
        end
      else
        false
      end
    else
      false
    end
  end

  def fair_countdown
    fair_pre_ongoing = Fair.pre_ongoing.active.order("lower(duration)").allow_user(self.id).limit(1).first
    if fair_pre_ongoing.present? && fair_pre_ongoing.duration.present?
      fair_pre_ongoing.duration.begin.to_i
    else
      nil
    end
  end

  def update_last_edited_user
    if self.edited_user_id.present? && self.changed?
      self.last_edited_user_id = edited_user_id
      self.last_edited_at = Time.now
    end
  end

  def update_last_reset_password
    if self.edited_user_password.present? && self.changed?
      self.last_reset_password_edited_user_id = edited_user_password
      self.last_reset_password_edited_at = Time.now
    end
  end

  def collect_dta?
    self.agent? || self.admin? || self.boss?
  end

  def commission_percentage(type)
    if self.subagent?
      self.send(type)
    else
      0
    end
  end

  def travelb2b_access_text
    self.travelb2b_module_access.map{|k,v| v == true ? k : '' }.reject(&:blank?).map{|i|i.sub("access_", "").sub("_", " ").titleize}.join(", ")
  end

  # def send_notify
  #   @payments = Payment.paid.where(created_at: DateTime.parse('6pm') - 24.hours..DateTime.parse('5:59pm'))
  #   ReportMailer.daily_sales_report(@payments).deliver_now
  # end

  def key_authenticate?(key)
    # only allow ktic to use key_authenticate
    if key.length >= 16 && self.username == 'ktic'
      cipher = OpenSSL::Cipher.new('DES-EDE3-CBC').decrypt
      # KEY = "KTIC-whiteLaBEL-wifi"
      # cipher.key = Digest::SHA1.hexdigest(KEY)[0..23]
      cipher.key = 'b5fd4d69af96e1dd6095f24e'
      s = [key].pack("H*").unpack("C*").pack("c*")

      decrypt_s = cipher.update(s) + cipher.final
      key_username, key_timestamp = decrypt_s.split('/')
      parsing_timestamp = DateTime.parse("#{key_timestamp} +8") rescue nil

      if Rails.env.iceb2b?
        key_username.downcase == self.username.downcase && parsing_timestamp.present? && parsing_timestamp >= (Time.now - 15.minutes)
      else
        key_username.downcase == self.username.downcase && parsing_timestamp.present? && parsing_timestamp >= (Time.now - 1.day)
      end
    else
      return false
    end
  end

  def is_ktic?
    self.username.present? && self.username == "ktic"
  end

  def easybook_access_link
    access_role = (self.boss? || self.admin?) ? 'admin' : 'agent'
    cipher = OpenSSL::Cipher.new('DES-EDE3-CBC').encrypt
    # KEY = "EasyB00k-Bus-fErry-WL"
    # cipher.key = Digest::SHA1.hexdigest(KEY)[0..23]
    cipher.key = '46279411ba28d859ca49d79a'
    s = cipher.update("#{self.id}/#{access_role}/#{Time.zone.now.strftime("%FT%T")}") + cipher.final

    key = s.unpack('H*')[0].upcase

    "https://book.travelb2b.my?key=#{key}"
  end

end

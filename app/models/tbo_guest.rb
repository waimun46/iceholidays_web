class TBOGuest < ApplicationRecord
  enum guest_type: %w( Adult Child )
  enum title: %w( Mr Ms Mrs )
  attr_accessor :index

  def full_name
    "#{self.first_name}, #{self.last_name}"
  end
end
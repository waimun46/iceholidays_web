class LandTourBooking < ApplicationRecord
  include LandTourValidator

  belongs_to :land_tour_booking_group, optional: true
  belongs_to :land_tour
  belongs_to :agent_user, class_name: "User", optional: true
  belongs_to :last_edited_user, class_name: "User", optional: true
  belongs_to :withdrawn_user, class_name: "User", optional: true

  enum status: [:active, :withdrawn]

  DESIGNATION = [
    :Miss, 
    :Mr, 
    :Mrs, 
    :Ms, 
    :Mstr, 
    :Mdm
    # :Dr, 
    # :Dato,
    # :Datin
  ]

  CATEGORY = [
    :adult,
    :child_with_bed,
    :child_without_bed,
    :child_twin,
    :infant
  ]

  typed_store :price_breakdown, coder: PostgresCoder do |s|
    s.decimal :collect_dta, default: 0
    s.decimal :commission_percentage, default: 0
    s.decimal :commission, default: 0
    s.boolean :free_of_charge, default: false
    s.decimal :pax_charges, default: 0
  end

  scope :visible, -> { where("land_tour_bookings.status != ?", LandTourBooking.statuses[:withdrawn]) }
  scope :in_between_booked, -> (interval) { where(land_tour_bookings: {created_at: interval})}
  scope :search_name, ->(keyword) { where("name ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  scope :in_between_departure_date, ->(interval) { joins(:land_tour_booking_group).where("land_tour_booking_groups.departure_date" => interval) }

  before_save :update_last_edited_user

  # validates_with BookingCountValidator

  def self.bulk_create(land_tour, land_tour_category, depart_date, rooms, language, price_qty, user, booking_params = [], flight_params = [])
    single_supplement_price = 0
    pax_charges = 0
    group_charges = 0
    if land_tour.compulsory_charges.present?
      land_tour.compulsory_charges.each do |charge|
        if charge["type"] === "Pax"
          pax_charges += charge["price"].to_f
        else
          group_charges += charge["price"].to_f
        end
      end
    end

    group = LandTourBookingGroup.new(flight_params[0].merge(:departure_date => depart_date, :rooms => rooms, :language => language, :group_charges => group_charges, :land_tour_category_id => land_tour_category.id))
    
    index = 0
    commission_percentage = user.collect_dta? ? 0 : user.commission_percentage("land_tour_commission")
    total_qty = price_qty.inject(0) { |k, v| k += v[1] }
    if land_tour.free_of_charge.present? && land_tour.free_of_charge > 0 && total_qty >= land_tour.free_of_charge
      booking_params.select{|b| b[:category] == "adult"}.first.merge!(free_of_charge: true)
    end

    price_qty.each do |category, qty|
      qty.times do |pax_index|
        price = land_tour_category.final_price(category, depart_date, price_qty["adult"], booking_params[index][:free_of_charge], user.collect_dta?)
        collect_dta = user.collect_dta? ? 0 : land_tour_category.dta(price, category, booking_params[index][:free_of_charge])
        commission = collect_dta * commission_percentage / 100

        if booking_params[index].present?
          group.land_tour_bookings << LandTourBooking.new(booking_params[index].merge(
            land_tour_id: land_tour.id, 
            quantity: 1, 
            category: category, 
            price: booking_params[index][:free_of_charge] ? price : price + pax_charges, 
            collect_dta: collect_dta,
            commission_percentage: commission_percentage,
            commission: commission,
            pax_charges: pax_charges,
            agent_user: user))
        else
          group.land_tour_bookings << LandTourBooking.new(
            land_tour_id: land_tour.id, 
            quantity: 1, 
            category: category, 
            price: price + pax_charges, 
            collect_dta: collect_dta,
            commission_percentage: commission_percentage,
            commission: commission,
            agent_user: user)
        end
        index += 1
      end
    end

    if total_qty == 1 && land_tour.single_supplement_price.present? && land_tour.single_supplement_price > 0
      single_supplement_price += land_tour.single_supplement_price
    end

    total_without_foc = group.land_tour_bookings.select{|b|!b.free_of_charge?}
    total_price = total_without_foc.map(&:price).inject(:+) + group_charges + single_supplement_price

    if land_tour.deposit.present? && land_tour.deposit > 0
      if land_tour.deposit_type === "fixed"
        deposit_amount = land_tour.deposit * total_without_foc.count
      else
        deposit_amount = total_price * (land_tour.deposit.to_f / 100.to_f)
      end
    else
      deposit_amount = 0
    end

    group.deposit = deposit_amount
    group.total_price = total_price
    group.save

    group
  end

  def update_last_edited_user
    if self.last_edited_user_id.present? && self.changed?
      self.last_edited_user_id = last_edited_user_id
      self.last_edited_at = Time.now
    end
  end

  def booked_by
    if agent_user.present?
      agent_user.username
    else
      ''
    end
  end
end
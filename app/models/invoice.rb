class Invoice < ApplicationRecord

  CHARGE_PER_BOOKING = 3
  CHARGE_STARTED = "1 November 2018".in_time_zone
  has_one :last_invoice, class_name: "Invoice", foreign_key: 'last_invoice_id'

  # def self.auto_generate(datetime = Time.zone.now.beginning_of_day)
  def self.auto_generate(datetime_range = Time.zone.now.beginning_of_month..Time.zone.now.end_of_month)

    datetime = Time.zone.now
    last_invoice = Invoice.last
    # chargable_bookings = Booking.chargable.where("bookings.created_at < ?", datetime)
    # refundable_bookings = Booking.refundable.where("created_at < ?", datetime)

    chargable_bookings = Booking.chargable(datetime_range)

    total_charge = chargable_bookings.count * Invoice::CHARGE_PER_BOOKING

    # total_refund = refundable_bookings.count * Invoice::CHARGE_PER_BOOKING
    total_refund = 0
    outstanding = 0
    if last_invoice.present? && last_invoice.balance < 0
      outstanding = - last_invoice.balance
    end

    chargable_bookings.map{ |b| b.charge!(datetime) }
    # refundable_bookings.map{ |b| b.refund!(datetime) }

    Invoice.create(
      description: "Invoice generate for tours departure date between #{datetime_range.first.strftime("%F %T")} to #{datetime_range.last.strftime("%F %T")}", 
      charged_booking_ids: chargable_bookings.ids,
      total_charge: total_charge,
      # refunded_booking_ids: refundable_bookings.ids,
      total_refund: total_refund,
      last_invoice_id: last_invoice.try(&:id),
      outstanding: outstanding,
      balance: total_charge - total_refund - outstanding
    )
  end

end
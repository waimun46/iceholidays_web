class ExchangeRate < ApplicationRecord
  validates :from_currency, uniqueness: { scope: :to_currency }
  validates :from_currency, :to_currency, :rate, presence: true
end

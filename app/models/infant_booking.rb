class InfantBooking < ApplicationRecord
  include BookingValidator
  include ExportChubb

  belongs_to :booking
  belongs_to :agent_user, class_name: "User", optional: true
  belongs_to :initiator_user, class_name: "User", optional: true
  belongs_to :last_edited_user, class_name: "User", optional: true

  enum status: [:active, :withdrawn]
  enum gender: [:male, :female]

  typed_store :policy_holder, coder: PostgresCoder do |j|
    j.string :ph_name
    j.datetime :ph_dob
    j.string :ph_designation
    j.string :ph_id_number
  end

  typed_store :insurance_export_remark, coder: PostgresCoder do |r|
    r.string :exported_name, default: ""
    r.datetime :exported_date_of_birth, default: ""
    r.string :exported_gender, default: ""
    r.string :exported_passport_number, default: ""
    r.datetime :exported_passport_expiry_date, default: ""
    r.string :exported_tour_code, default: ""
  end

  typed_store :witness_details, coder: PostgresCoder do |w|
    w.string :witness_surname, default: ""
    w.string :witness_given_name, default: ""
    w.string :witness_passport_number, default: ""
  end

  mount_uploader :passport_photocopy, PassportUploader

  validates :date_of_birth, presence: true
  validates :date_of_birth, infant_date_of_birth: true
  validates :ph_dob, infant_policy_holder_date_of_birth: true

  def policy_holder_available?
    self.over_1month? || (self.parent_id.present? && self.parent_id != 0) || (self.ph_name.present? && self.ph_dob.present? && self.ph_dob < self.booking.tour.departure_date - 18.years)
  end

  def over_1month?
    self.date_of_birth.present? && self.date_of_birth < self.booking.tour.departure_date - 1.month
  end

  def passport_valid?
    self.passport_expiry_date.present? && self.passport_expiry_date > self.booking.tour.departure_date + 195.days
  end

  def able_to_export?
    self.booking.insurance_available? && self.insurance_confirmation == true && self.nationality == "malaysian" && self.name.present? && self.passport_number.present? && self.passport_valid? && self.date_of_birth.present? && self.insurance_exported == false && self.policy_holder_available? && self.passport_photocopy.present?
  end

  def export_error_message
    error = []
    if !self.booking.insurance_available?
      error << "Insurance export is not available as for today's date."
    end
    if self.insurance_confirmation != true
      error << "Insurance export confirmation must be checked."
    end
    if self.insurance_exported != false
      error << "Insurance already exported."
    end
    if !self.name.present?
      error << "Guest's name must be present."
    end
    if self.nationality != "malaysian"
      error << "Guest's nationality must be Malaysian."
    end
    if !self.passport_number.present?
      error << "Guest's passport number must be present."
    end
    if !self.passport_valid?
      error << "Guest's passport expiry date is not valid. The date must be present and valid 6 month from departure date."
    end
    if !self.date_of_birth.present?
      error << "Guest's date of birth must be present."
    end
    if !self.policy_holder_available?
      error << "Guardian information is not valid."
    end
    if !self.passport_photocopy.present?
      error << "Passport photocopy must be uploaded."
    end
    error
  end

  def able_to_endorse?
    [:name, :date_of_birth, :gender, :passport_number, :passport_expiry_date].any? { |e| self.send(e) != self.send("exported_#{e}") } && self.exported_tour_code.present? && self.booking.tour.code != self.exported_tour_code && self.insurance_exported == true
  end

  def policy_holder_detail
    if self.parent_id.present? && self.parent_id != 0
      policy_holder = Booking.find(self.parent_id)
      { name: policy_holder.name, dob: policy_holder.date_of_birth, designation: policy_holder.designation, passport_number: policy_holder.passport_number }
    elsif self.ph_name.present? && self.ph_dob < self.booking.tour.departure_date - 18.years
      { name: self.ph_name, dob: self.ph_dob, designation: self.ph_designation, passport_number: ph_id_number }
    end
  end

  def check_user(user)
    user != "admin"
  end

  def over_18?
    false
  end

  def export_insurance(user, status)
    policy_details = export_to_chubb(self, status)
    if policy_details.present? && policy_details != false
      self.exported_name = self.name
      self.exported_date_of_birth = self.date_of_birth
      self.exported_gender = self.gender
      self.exported_passport_number = self.passport_number
      self.exported_passport_expiry_date = self.passport_expiry_date
      self.exported_tour_code = self.booking.tour.code

      if status == "new"
        new_policy_no = policy_details["policy_no"]
      else
        self.increment!(:insurance_endorse_count)

        if self.policy_number[-2] == "E"
          new_policy_no = self.policy_number.sub(self.policy_number.last(2), "E#{self.insurance_endorse_count}")
        else
          new_policy_no = "#{self.policy_number}E#{self.insurance_endorse_count}"
        end
      end

      self.update_columns(insurance_cert: policy_details["insurance_cert"], policy_number: new_policy_no, insurance_exporter_id: user.id, insurance_exported: true, insurance_exported_at: Time.now, insurance_export_remark: self.insurance_export_remark)
      true
    else
      false
    end
  end
end
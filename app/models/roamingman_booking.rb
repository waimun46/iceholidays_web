class RoamingmanBooking < ApplicationRecord
  include ExportRoamingman

  belongs_to :roamingman_booking_group, optional: true
  belongs_to :roamingman_package
  belongs_to :agent_user, class_name: "User", optional: true
  belongs_to :last_edited_user, class_name: "User", optional: true
  belongs_to :exporter, class_name: "User", optional: true

  enum status: [:active, :cancelled]
  enum export_status: [:not_exported, :exported, :order_cancelled]
  enum ship_way: [:self_pickup, :delivery]

  typed_store :cancelled_details, coder: PostgresCoder do |s|
    s.integer :audit_duration, default: 0
    s.integer :charge, default: 0
  end

  typed_store :price_breakdown, coder: PostgresCoder do |s|
    s.decimal :collect_dta, default: 0
    s.decimal :commission_percentage, default: 0
    s.decimal :commission, default: 0
  end

  scope :visible, -> { where("roamingman_bookings.status != ?", RoamingmanBooking.statuses[:cancelled]) }

  validates :deliver_point_id, presence: true, if: :self_pickup?
  validates :address, presence: true, if: :delivery?
  validates :postcode, presence: true, if: :delivery?

  before_save :update_last_edited_user
  before_create :calculate_price_and_breakdown

  DELIVER_POINT = {
    "SEA10492" => "Malaysia KLIA1",
    "SEA10582" => "Malaysia KLIA2",
    "WHE00001" => "Senai Airport",
  }

  def self_pickup?
    ship_way == "self_pickup"
  end

  def calculate_price_and_breakdown
    if self.delivery?
      shipping = RoamingmanBookingGroup::SHIPPING_FEE
    else
      shipping = 0.00
    end

    duration = (self.return_date - self.depart_date).to_i + 1

    if self.agent_user.present? && self.agent_user.collect_dta?
      self.price = ((duration * self.roamingman_package.channel_price_markup(self.agent_user)) * self.quantity) + shipping
      #
      if self.agent_user.username == "ktic"
        self.collect_dta = ((duration * self.roamingman_package.channel_markup(self.agent_user)) * self.quantity)
      else
        self.collect_dta = 0
      end
      self.commission_percentage = 0
      self.commission = 0
    else
      self.price = ((duration * self.roamingman_package.retail_price) * self.quantity) + shipping
      self.collect_dta = (duration * self.roamingman_package.dta(self.agent_user)) * self.quantity
      self.commission_percentage = self.agent_user.commission_percentage("wifi_commission")
      self.commission = collect_dta * commission_percentage / 100
    end

  end

  def delivery?
    ship_way == "delivery"
  end

  def ship_way_text
    if self.ship_way.present?
      if self.ship_way == "delivery"
        "Delivery (Shipping fee MYR 8)"
      else
        self.ship_way.humanize
      end
    else
      ""
    end
  end

  def deliver_point_name(id)
    if DELIVER_POINT.has_key?(id)
      DELIVER_POINT[id]
    else
      ''
    end
  end

  def update_last_edited_user
    if self.last_edited_user_id.present? && self.changed?
      self.last_edited_user_id = last_edited_user_id
      self.last_edited_at = Time.now
    end
  end

  def able_to_export?
    if self.export_status == "not_exported"
      true
    else
      false
    end
  end

  def non_editable?
    if self.export_status == "exported"
      true
    else
      false
    end
  end

  def export_roamingman_create(user)
    order_details = export_to_roamingman(self)
    if Rails.env.iceb2b? || Rails.env.testb2b? 
      if order_details.present? && order_details[:resultCode] == "0000" && order_details[:data][:vendorOrderId].present?
        self.update_columns(oms_order_id: order_details[:data][:vendorOrderId], export_status: RoamingmanBooking.export_statuses[:exported], exporter_id: user.present? ? user.id : nil, exported_at: Time.now)
        true
      else
        false
      end
    else
      true
    end
  end

  def self.deliver_method
    # Roaming Man close the develiry option
    # ship_ways.map.with_index{ |j, i| j[0] != "delivery" ? ["#{j[0].humanize}", j[0]] : ["Delivery (Shipping fee MYR 8)", j[0]] }
    [["Self pickup", "self_pickup"]]
  end

  def encode_booking_details(data_hash)
    Base64.strict_encode64(data_hash)
  end

  def calculate_sign(sign_string)
    Digest::MD5.hexdigest(sign_string).upcase
  end

  def encode_request_data(request_hash)
    Base64.strict_encode64(request_hash)
  end

  def decode_response(response)
    Base64.strict_decode64(response)
  end
end
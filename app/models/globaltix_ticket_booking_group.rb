class GlobaltixTicketBookingGroup < ApplicationRecord
  has_many :globaltix_ticket_bookings
  has_one :payment, as: :taggable
  has_many :etravel_invoices, as: :taggable

  scope :in_between, ->(inverval) { where(created_at:(inverval))}

  def self.create_booking(globaltix_tickets, price_qty, params, user, agent_rep = nil)
    if agent_rep == nil
      agent_user = user
    else
      agent_user = User.find_by_username(agent_rep)
    end

    globaltix_ticket_booking_group = GlobaltixTicketBookingGroup.create(export_params: params)

    commission_percentage = user.collect_dta? ? 0 : user.commission_percentage("globaltix_commission")

    params["ticketTypes"].each do |ticket_params|
      globaltix_ticket = globaltix_tickets.detect{|gt| gt.ticket_id == ticket_params['id']}
      collect_dta = user.collect_dta? ? 0 : globaltix_ticket.dta * ticket_params['quantity']
      commission = collect_dta * commission_percentage / 100

      GlobaltixTicketBooking.create(
        email: params[:email], 
        name: params[:customerName], 
        globaltix_ticket_booking_group: globaltix_ticket_booking_group, 
        globaltix_ticket: globaltix_tickets.detect{|gt| gt.ticket_id == ticket_params['id']},
        agent_user: agent_user,
        sale_rep: agent_rep,
        price: globaltix_ticket.final_price(user.collect_dta?) * ticket_params['quantity'],
        collect_dta: collect_dta,
        commission_percentage: commission_percentage,
        commission: commission,
        initiator_user: user)
    end

    globaltix_ticket_booking_group.update_columns(price: globaltix_ticket_booking_group.globaltix_ticket_bookings.map(&:price).inject(:+))

    globaltix_ticket_booking_group
  end

  def export_globaltix
    params = self.export_params
    booking = Globaltix::Booking.create(params)

    if booking.present?
      self.update_columns(orignal_price: "#{booking.currency}#{booking.amount}", reference_number: booking.reference_number, e_ticket_url: booking.e_ticket_url, detail: booking.tickets, exported_at: Time.now)
      booking.tickets.each do |ticket|
        gt = GlobaltixTicket.find_by_ticket_id ticket["product"]["id"]
        if gt.present?
          gtb = self.globaltix_ticket_bookings.find_by_globaltix_ticket_id(gt.id)
          gtb.update_columns(globaltix_ticket_booking_group_detail_id: ticket["id"])
        end
      end
    end
  end

  def reference_id
    "#{self.id.to_s.rjust(10, '0')}G"
  end

  def total_quantity
    if self.detail.present?
      self.detail.map{|d| d["quantity_total"]}.inject(:+)
      # self.detail["quantity_total"]}.inject(&:+)
    elsif self.export_params.present? && self.export_params["ticketTypes"].present? 
      self.export_params["ticketTypes"].map{|t| t["quantity"]}.inject(:+)
    else
      0
    end
  end

  def able_to_export?
    self.detail.blank? && self.export_params.present? && self.exported_at.blank?
  end

  def redirect_url
    Rails.application.routes.url_helpers.thank_you_globaltix_ticket_booking_groups_path(:id => self.id)
  end

  def show_url
    Rails.application.routes.url_helpers.globaltix_ticket_booking_group_path(:id => self.id)
  end

  def agent_user
    if globaltix_ticket_bookings.present?
      globaltix_ticket_bookings.first.agent_user
    else
      nil
    end
  end

  def product_name
    if globaltix_ticket_bookings.first.globaltix_ticket.present?
      globaltix_ticket_bookings.first.globaltix_ticket.title
    else
      ""
    end
  end

  def travel_date
    if self.export_params.present? && self.export_params['visitDate'].present?
      Time.parse(self.export_params['visitDate']).try(:strftime, "%d %b %Y")
    elsif self.detail.present? && self.detail.first['visitDate'].present?
      Time.parse(self.detail.first['visitDate']).try(:strftime, "%d %b %Y")
    else
      ""
    end
  end

  def remark
    if self.export_params.present? && self.export_params['remark'].present?
      self.export_params['remark'].humanize
    else
      ""
    end
  end

  def product_description
    "Ice Holidays Activity Booking"
  end

  def booking_quantity
    if globaltix_ticket_bookings.present?
      globaltix_ticket_bookings.count
    else
      0
    end
  end

  def buyer_email
    if globaltix_ticket_bookings.first.agent_user.present?
      globaltix_ticket_bookings.first.agent_user.email
    else
      ""
    end
  end

  def send_notify
    agent = self.agent_user
    if agent.present?
      if agent.emails.present?
        GlobaltixTicketMailer.notify(agent, self, agent.emails.join(', ')).deliver_now
        if self.has_payment?
          GlobaltixTicketMailer.receipt(agent, self, agent.emails.join(', ')).deliver_now
        end
      end
    end
  end

  def has_payment?
    self.payment.paid? && self.payment.amount >= 1
  end

  def ref_no
    "BGTX#{self.id.to_s.rjust(9, '0')}"
  end

end
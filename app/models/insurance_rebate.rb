class InsuranceRebate < ApplicationRecord
  scope :query_days, ->(days) { where("days_duration::int4range @> ?::int", days) }

  def duration_start
    if days_duration.present?
      days_duration.begin
    else
      nil
    end
  end

  def duration_end
    if days_duration.present?
      days_duration.end - 1
    else
      nil
    end
  end
end

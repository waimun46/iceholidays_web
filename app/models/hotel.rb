class Hotel < ApplicationRecord

  TRAVELLER_TYPES = ["Adult", "Child"]

  has_many :covers, as: :taggable
  accepts_nested_attributes_for :covers, allow_destroy: true

  enum markup_category: [:percentage, :fixed]
  enum hotel_source: [:travel_prologue]

  scope :search_by_hotel, ->(hotel_keyword) { where("hotels.hotel_name ILIKE ?", "%#{hotel_keyword}%") }
  scope :search_by_city, ->(city_keyword) { where("hotels.city_name ILIKE ?", "%#{city_keyword}%") }
  scope :search_with_city, ->(hotel_keyword, city_keyword) { where("hotels.hotel_name ILIKE ? and hotels.city_name ILIKE ?", "%#{hotel_keyword}%", "%#{city_keyword}%") }

  USD_CONVERTED_TO_MYR = 4

  def create_image(url)
    if not_duplicate_image?(url)
      new_image = Cover.new
      new_image.remote_image_url = url
      new_image.taggable = self
      new_image.save
    end
  end

  def delete_images(image_ids)
    covers = Cover.where(id: image_ids)
    covers.delete_all
  end

  def not_duplicate_image?(url)
    duplicate = true
    images = self.covers.map(&:image)

    new_image = Cover.new
    new_image.remote_image_url = url

    images.each do |image|
      if image.file.original_filename == new_image.image.file.original_filename
        duplicate = false
        break
      end
    end

    duplicate
  end

  def self.rate_with_markup(rate, markup, markup_category, currency)
    rate_in_myr = 0

    if currency == 'USD'
      exchange_rate = ExchangeRate.find_by(from_currency: 'USD', to_currency: 'MYR').try(:rate) || Hotel::USD_CONVERTED_TO_MYR
      rate_in_myr = (rate * exchange_rate).ceil
    else
      rate_in_myr = rate
    end

    if markup.present? 
      if markup_category == 'percentage'
        decimal_rate  = (markup / 100)
        markup = (rate_in_myr.to_f * decimal_rate)

        total_cost = rate_in_myr.to_f + markup
      else
        total_cost = rate_in_myr.to_f + markup
      end
    else
      general_markup = Hotel.general_markup

      if general_markup.markup_category == 'percentage'
        decimal_rate  = (general_markup.markup / 100)
        markup = (rate_in_myr.to_f * decimal_rate)

        total_cost = rate_in_myr.to_f + general_markup.markup
      else
        total_cost = rate_in_myr.to_f + general_markup.markup
      end
    end

    total_cost
  end

  def self.general_markup
    Hotel.where(tp_hotel_id: nil).first
  end

  def self.search_by_tp_hotel_id(tp_hotel_id)
    Hotel.find_by_tp_hotel_id(tp_hotel_id)
  end

end

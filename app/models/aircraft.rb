class Aircraft < ApplicationRecord

  def self.aircraft_name(code)
    if @aircrafts_list.blank?
      @aircrafts_list = Hash.new
    end

    unless @aircrafts_list.has_key?(code)
      aircraft = Aircraft.find_by_iata_code(code)
      if aircraft.present?
        @aircrafts_list[code] = "#{aircraft.manufacturer} #{aircraft.model}"
      else
        aircraft_group = Aircraft.find_by_iata_group(code)
        if aircraft_group.present?
          @aircrafts_list[code] = "#{aircraft_group.manufacturer} #{aircraft_group.iata_group}"
        else
          @aircrafts_list[code] = "#{code}"
        end
      end
    end
    @aircrafts_list[code]
  end

end

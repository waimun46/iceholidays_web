class TBOCity < ApplicationRecord
  self.primary_key = :code

  belongs_to :country, class_name:'TBOCountry', foreign_key: :country_code, primary_key: :code
  has_many :hotels, class_name:'TBOHotel', foreign_key: :city_name, primary_key: :name, dependent: :destroy

  scope :search_by_name, ->(city_keyword) { where("name ILIKE ?", "%#{city_keyword}%") }

  def self.all_cached
    Rails.cache.fetch("cached_cities") do
      joins(:hotels).where(tbo_hotels: {is_active:true}).distinct.map do |x| 
      # includes(:country).where(tbo_countries: {is_active:true}).map do |x| 
        {city_id: x.code, city_name: "#{x.name}", country_code: x.country.code, country_name: x.country.name }
      end
    end
  end

  def self.clear_cache
    Rails.cache.delete("cached_cities")
  end

  def self.all_cities
    Rails.cache.fetch("all_cities_cache") do
      TBOCity.all.map do |x| 
        {city_id: x.code, city_name: "#{x.name}", country_code: x.country.code, country_name: x.country.name }
      end
    end
  end
end
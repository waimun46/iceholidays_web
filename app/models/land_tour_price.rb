class LandTourPrice < ApplicationRecord
  include LandTourValidator
  
  belongs_to :land_tour_category, inverse_of: :land_tour_prices

  validates :dates, presence: true
  validates :prices, presence: true
  validates_with PriceValidator

  def land_tour
    self.land_tour_category.land_tour
  end

  def dates_text
    if dates.present?
      dates.map{|date| "#{date.begin.strftime("%d/%m/%Y")}-#{date.max.strftime("%d/%m/%Y")}"}.join(', ')
    else
      ""
    end
  end

  def dates_text=(val)
    dates_text = val.split(',')
    all_date = []
    dates_text.each do |date_text|
      date_text_start, date_text_end = date_text.split('-')

      date_start = Date.parse(date_text_start) rescue nil
      date_end = Date.parse(date_text_end) rescue nil

      if date_start.present? && date_end.present?
        all_date << Range.new(date_start, date_end)
      end

      # if date_start.present?
      #   if date_end.present?
      #     all_date << Range.new(date_start, date_end)
      #   else
      #     all_date << Range.new(date_start, date_start)
      #   end
      # end
    end
    write_attribute(:dates, all_date)
  end

  def get_price(depart_date)
    self.dates.map{|e| e.inject([]){|k, date| k << date}}.flatten.any?{|d|d == depart_date.to_date}
  end

  def final_price(total_qty, category, foc = false, minus_dta = false)
    # total_qty is total adult
    if self.prices.present?
      price = self.prices.map{|k,v| [(k.split('..')[0].to_i)..(k.split('..')[1].to_i), v]}.map{|q|q[0].include?(total_qty) ? q[1].to_f : 0}.first
      if category == "adult"
        price
      elsif category == "infant"
        price = self.land_tour.child_prices["infant"]
      else
        child_percentage = self.land_tour.child_prices[category]
        if child_percentage.present? && child_percentage > 0
          price = (price * child_percentage.to_f / 100.to_f).round(2)
        else
          price = 0
        end
      end
    else
      price = 0
    end

    if minus_dta # user.collect_dta? == true
      if foc
        price
      else
        price.to_f - self.dta(price, category)
      end
    else
      price
    end
  end

  def dta(price, category)
    if category == "infant"
      0
    else
      dta_prices = self.land_tour.dta_prices
      if dta_prices[:dta_type] == "Fixed"
        if category == "adult"
          dta_prices[:dta_adult]
        else
          dta_prices[:dta_child]
        end
      else
        if category == "adult"
          (price * dta_prices[:dta_adult].to_f / 100.to_f).round(2)
        else
          (price * dta_prices[:dta_child].to_f / 100.to_f).round(2)
        end
      end
    end
  end
end
class FlightBooking < ApplicationRecord
  include TravelPortApi

  belongs_to :flight_booking_group
  belongs_to :agent_user, class_name: "User", optional: true
  belongs_to :initiator_user, class_name: "User", optional: true
  has_one :brb_booking

  typed_store :price_breakdown, coder: PostgresCoder do |s|
    s.decimal :collect_dta, default: 0
    s.decimal :commission_percentage, default: 0
    s.decimal :commission, default: 0
    s.decimal :base, default: 0
    s.decimal :taxes, default: 0
    s.decimal :brb_price, default: 0
  end
  
  scope :in_between, ->(inverval) { where(created_at:(inverval))}
  
  enum status: [:active, :withdrawn]

  def name
    "#{last_name}/#{first_name}"
  end

  def self.category_mapping(category_abbr)
    if category_abbr == "ADT"
      "Adult"
    elsif category_abbr == "CNN"
      "Child"
    elsif category_abbr == "INT"
      "Infant"
    else
      category_abbr
    end
  end

end

class ActivityBookingGroup < ApplicationRecord
  include ActivityValidator

  belongs_to :last_edited_user, class_name: "User", optional: true
  has_one :payment, as: :taggable
  has_many :etravel_invoices, as: :taggable
  has_many :activity_bookings, -> { order("id") }
  accepts_nested_attributes_for :activity_bookings, allow_destroy: true

  enum payment_method: [:online, :offline]

  scope :in_between, ->(inverval) { where(created_at:(inverval))}

  validates_associated :activity_bookings
  validates_with FpxAmountValidator

  def activity
    if activity_bookings.present?
      activity_bookings.first.activity
    else
      nil
    end
  end

  def reference_id
    "#{self.id.to_s.rjust(10, '0')}A"
  end

  def booking_ref
    if activity_bookings.present?
      activity_bookings.map { |e| e.code }.join(', ')
    else
      ""
    end
  end

  def product_name
    if activity_bookings.first.activity.present?
      activity_bookings.first.activity.title
    else
      ""
    end
  end

  def product_description
    "Ice Holidays Activity Booking"
  end

  def booking_quantity
    if activity_bookings.present?
      activity_bookings.count
    else
      0
    end
  end

  def buyer_email
    if activity_bookings.first.agent_user.present?
      activity_bookings.first.agent_user.email
    else
      ""
    end
  end

  def total_quantity
    activity_bookings.map(&:quantity).inject(:+)
  end

  # def total_price
  #   activity_bookings.first.price
  # end

  def calculate_qty(category)
    booking = activity_bookings.select { |p| p.category == category }.first 
    if booking.present? 
      booking.quantity 
    else
      0
    end
  end

  def unit_price(category)
    activity_bookings.select { |p| p.category == category }.first
  end

  def redirect_url
    Rails.application.routes.url_helpers.thank_you_activity_booking_groups_path(:id => self.id)
  end

  def show_url
    Rails.application.routes.url_helpers.activity_booking_group_path(:id => self.id)
  end

  def agent_user
    if activity_bookings.present?
      activity_bookings.first.agent_user
    else
      nil
    end
  end

  def send_notify
    agent = self.agent_user
    if agent.present?
      if agent.emails.present?
        ActivityMailer.notify(agent, self, agent.emails.join(', ')).deliver_now
        if self.has_payment?
          ActivityMailer.receipt(agent, self, agent.emails.join(', ')).deliver_now
        end
      end
      ActivityMailer.notify_admin(agent, self).deliver_now
    end
  end

  def has_payment?
    # self.payments.paid.map(&:amount).inject(:+) >= 1
    self.payment.paid? && self.payment.amount >= 1
  end

  def ref_no
    "BACT#{self.id.to_s.rjust(9, '0')}"
  end

end
class TourLeader < ApplicationRecord

has_many :booking_rooms
has_many :tours

mount_uploader :photo, LogoUploader
mount_uploader :passport_copy, LogoUploader
mount_uploader :insurance_copy, LogoUploader

validates :designation ,:last_name, :first_name, :mobile, :emergency_number, :languages, :nationality, :gender, :date_of_birth, presence: true
validates :insurance_copy, :insurance_expiry_date, presence: { message: "can't be blank, when Travel Insurance is Yes" }, if: -> { insurance_confirmation? }
validates :passport_expiry_date, :passport_copy, :passport_country, presence: { message: "can't be blank, when Passport Number is  Key in " }, if: -> { passport_number? }

enum gender: [:male, :female]

  DESIGNATION = [
    :Miss, 
    :Mr, 
    :Mrs, 
    :Ms, 
    :Mstr, 
    :Mdm
    # :Dr, 
    # :Dato,
    # :Datin
  ]

  LANGUAGES =[ 
    "chinese",
    "english",
    "malay"
  ]

  def name
    " #{self.last_name} #{self.first_name} (Tour Leader)"
  end

  def name_index
    " #{self.last_name} #{self.first_name}"
  end 

  def first_name=(val)
    if val.blank?
      write_attribute(:first_name, nil)
    else
      write_attribute(:first_name, val.upcase)
    end
  end

  def last_name=(val)
    if val.blank?
      write_attribute(:last_name, nil)
    else
      write_attribute(:last_name, val.upcase)
    end
  end

  def passport_number_present
    if self.passport_number.present?
      "Yes"
    else
      "No"
    end
  end

  def insurance_no_present
    if self.insurance_confirmation.present?
      "Yes"
    else
      "No"
    end
  end

  def visa_no_present
    if self.visa_no.present?
      "Yes"
    else
      "No"
    end
  end 
end

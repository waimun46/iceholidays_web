class RoamingmanPackage < ApplicationRecord
  belongs_to :created_user, class_name: "User", optional: true
  belongs_to :last_edited_user, class_name: "User", optional: true
  has_many :roamingman_bookings
  has_many :roamingman_booking_groups, through: :roamingman_bookings
  has_many :covers, as: :taggable
  
  accepts_nested_attributes_for :covers, reject_if: proc { |attributes| attributes['image'].blank? }, allow_destroy: true

  enum category: [:standard, :premium]
  enum status: [:active, :pending]

  scope :search, ->(keyword) { where("roamingman_packages.name ILIKE ? or roamingman_packages.code ILIKE ? or array_to_string(countries, ',') ILIKE ?", "%#{keyword}%", "%#{keyword}%", "%#{keyword}%") }
  scope :search_country, ->(keyword) { where("array_to_string(countries, ',') ILIKE ?", "%#{keyword}%") }
  scope :visible, -> { where("roamingman_packages.status != ?", RoamingmanPackage.statuses[:pending]) }
  scope :with_visible, -> (only=true) { where("roamingman_packages.status != ?", RoamingmanPackage.statuses[:pending]) if only }

  def join_countries
    if self.countries.present?
      self.countries.join(',')
    else
      ''
    end
  end

  def self.duplicate(package, code, price, type)
    new_package = package.dup
    new_package.code = code
    new_package.retail_price = price
    new_package.package_type = type
    new_package.status = 1

    if new_package.save
      if package.covers.present?
        package.covers.each do |cover|
          if cover.image.present?
            new_package_cover = cover.dup
            new_package_cover.taggable = new_package
            new_package_cover.image = cover.image
            new_package_cover.priority = cover.priority
            new_package_cover.save
          end
        end
      end
    end
    new_package
  end

  def channel_price_markup(user = nil)
    self.channel_price + channel_markup(user)
  end

  def channel_markup(user = nil)
    if user.present? && user.username == "ktic"
      ((self.retail_price - self.channel_price) * 0.2) # 20% markup
    else
      0
    end
  end

  def channel_price=(val)
    if val.present?
      write_attribute(:channel_price, val.to_f.round_to_half(2))
    else
      write_attribute(:channel_price, 0)
    end
  end

  def retail_price=(val)
    if val.present?
      write_attribute(:retail_price, val.to_f.round_to_half(2))
    else
      write_attribute(:retail_price, 0)
    end
  end

  def dta(user = nil)
    if self.retail_price > self.channel_price_markup(user)
      self.retail_price - self.channel_price_markup(user)
    else
      0
    end
  end

end
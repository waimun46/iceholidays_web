class GiftTrackingDetail < ApplicationRecord
  include GiftValidator

  belongs_to :gift_tracking
  belongs_to :gift

  validates :quantity, presence: true, numericality: { greater_than: 0 }
  validates_with GiftQuantityValidator

  after_commit :calculate_balance

  def calculate_balance
    Gift.find(gift.id).calculate_balance
  end

  def admin_available_gifts
    if self.gift.present?
      Gift.admin_available.to_a.push(self.gift).uniq
    else
      Gift.admin_available
    end
  end
  def available_gifts
    if self.gift.present?
      Gift.available.to_a.push(self.gift).uniq
    else
      Gift.available
    end
  end

  def unit_points
    if gift.present?
      gift.points
    else
      0
    end
  end

end

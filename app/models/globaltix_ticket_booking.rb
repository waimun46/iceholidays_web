class GlobaltixTicketBooking < ApplicationRecord

  belongs_to :globaltix_ticket_booking_group
  belongs_to :globaltix_ticket
  belongs_to :agent_user, class_name: "User", optional: true
  belongs_to :initiator_user, class_name: "User", optional: true

  typed_store :price_breakdown, coder: PostgresCoder do |s|
    s.decimal :collect_dta, default: 0
    s.decimal :commission_percentage, default: 0
    s.decimal :commission, default: 0
  end
  
  enum status: [:active, :withdrawn]

  scope :in_between, ->(inverval) { where(created_at:(inverval))}

  def detail
    if globaltix_ticket_booking_group.detail.present?
      globaltix_ticket_booking_group.detail.find{|element| element["id"] == self.globaltix_ticket_booking_group_detail_id }
    else
      nil
    end
  end

  def export_params
    if self.globaltix_ticket_booking_group.export_params.present? && self.globaltix_ticket_booking_group.export_params["ticketTypes"].present?
      self.globaltix_ticket_booking_group.export_params["ticketTypes"].find{|element| element["id"] == self.globaltix_ticket.ticket_id }
    else
      nil
    end
  end

  def quantity
    if self.detail.present?
      self.detail["quantity_total"]
    elsif self.export_params.present?
      self.export_params["quantity"]
    else
      0
    end
  end

  def all_quantity
    qty = []
    if globaltix_ticket_booking_group.detail.present?
      self.globaltix_ticket_booking_group.detail.each do |booking|
        qty << { name: booking['name'], variation: booking['variation']['name'], quantity: booking['quantity_total'] }
      end
      qty.map{|v| "#{v[:quantity]} #{v[:name]} (#{v[:variation]})"}.join(', ')
    else
      nil
    end
  end
  
  def total_price
    self.globaltix_ticket.final_price * self.quantity
  end
end

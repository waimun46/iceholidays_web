class TBOBooking < ApplicationRecord  
  enum booking_status:  %w( Pending Failed Confirmed Vouchered Cancelled Rejected CancellationInProgress ) 
  enum amendment_status:  %w( NotRequested RequestSent Unprocessed InProgress AgentApprovalPending PendingWithSupplier Amended Withdrawn Rejected Cancelled), _prefix: :amendment
  enum cancellation_status:  %w( UnProcessed Pending Processed Rejected ), _prefix: :cancellation
  has_one :amendment_request, class_name:'TBOAmendmentRequest', dependent: :destroy
  has_many :guests, class_name: 'TBOGuest', dependent: :destroy
  belongs_to :hotel, class_name: 'TBOHotel', foreign_key: :hotel_code, primary_key: :hotel_code, optional: true
  has_one :lead_guest, -> {where(lead_guest: true)}, class_name: 'TBOGuest'
  has_one :payment, as: :taggable
  has_many :etravel_invoices, as: :taggable
  belongs_to :agent_user, class_name: "User", optional: true

  typed_store :address_info, coder: PostgresCoder do |s|
    s.string :address_line_1, default: ''
    s.string :address_line_2, default: ''
    s.string :country_code, default: ''
    s.string :area_code, default: ''
    s.string :phone_no, default: ''
    s.string :email, default: ''
    s.string :city, default: ''
    s.string :state, default: ''
    s.string :country, default: ''
    s.string :zip_code, default: ''
  end

  def product_name
    if hotel.present?
      hotel.hotel_name
    else
      ""
    end
  end

  def booking_quantity
    if guests.present?
      guests.count
    else
      0
    end
  end

  def send_notify
    TBOHotelMailer.send_booking_confirmation(self).deliver_later
  end

  def redirect_url
    Rails.application.routes.url_helpers.thankyou_tbo_holidays_bookings_path(:id => self.id)
  end

  def show_url
    Rails.application.routes.url_helpers.tbo_holidays_booking_path(:id => self.id)
  end

  def reference_id
    self.id.to_s.rjust(10, '0')
  end

  def cancellable?    
    if last_cancellation_deadline.blank? 
      if booking_status.in?(%w( Vouchered Confirmed ))
        true
      else
        false
      end
    else
      last_cancellation_deadline > DateTime.current && booking_status.in?(%w( Vouchered Confirmed ))
    end
  end

  def amendable?    
    amendment_status == 'NotRequested' && booking_status.in?(%w( Vouchered Confirmed ))
  end

  def amendment_request
    super || build_amendment_request
  end

  def ref_no
    "BHTL#{self.id.to_s.rjust(9, '0')}"
  end

  def generate_invoice
    # default voucher = false
    payment_info = HotelBooker::PaymentInfo.new(voucher_booking: true)
    resp = HotelBooker[:tbo_holidays].generate_invoice!(payment_info: payment_info, confirmation_no: self.confirmation_no)

    if resp.success?
      self.update_columns(booking_status: 2)
    else
      false
    end
  end
end
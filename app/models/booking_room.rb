class BookingRoom < ApplicationRecord
  belongs_to :tour
  belongs_to :tour_leader, optional: true
  has_many :bookings

  def self.create_booking_room(tour, booking_ids, params)
    if params.present?

      params[:booking_rooms_attributes].each do |key, booking_room|
        if booking_room[:room_type] == "single_room" && !booking_room[:bookings].key?("guest_1")
          return false
        elsif (booking_room[:room_type] == "twin_room" || booking_room[:room_type] == "double_room") && !booking_room[:bookings].key?("guest_1") && !booking_room[:bookings].key?("guest_2")
          return false
        elsif booking_room[:room_type] == "triple_room" && !booking_room[:bookings].key?("guest_1") && !booking_room[:bookings].key?("guest_2") && !booking_room[:bookings].key?("guest_3")
          return false
        end
      end

      tour_leader = nil
      params[:booking_rooms_attributes].each do |key, booking_room|
        if booking_room[:bookings].present? 
          new_booking_room = self.find_or_initialize_by(id: booking_room[:id])
          if booking_room[:_destroy] == "1" 
            if new_booking_room.present? && new_booking_room.id.present?
              new_booking_room.destroy
            end
          else
            new_booking_room.room_type = booking_room[:room_type]
            new_booking_room.tour_id = tour.id
            new_booking_room.tour_leader_id = nil
            if new_booking_room.save
              booking_room[:bookings].each do |guest, booking_id|
                if booking_id.present?
                  if booking_id.include?('tour_leader/')
                    tour_leader = TourLeader.find(booking_id.split('/')[1])
                    new_booking_room.update_columns(tour_leader_id: tour_leader.id)
                  else
                    booking = Booking.find(booking_id)
                    booking.update_columns(booking_room_id: new_booking_room.id, room_type: new_booking_room.booking_room_type(guest))
                    booking_ids.delete booking.id
                  end
                end
              end
            end
          end
         end 
      end
      booking_ids.each do |booking_id|
        Booking.find(booking_id).update_columns(booking_room_id: nil, room_type: '')
      end
      true

    else
      false
    end
  end

  def guest_1
    if room_type == 'single_room' || room_type == 'double_room' || room_type == 'twin_room' || room_type == 'triple_room'
      if self.bookings.exclude_extra_room_type.present?
        self.bookings.exclude_extra_room_type.first
      elsif self.tour_leader.present? 
        self.tour_leader
      else
        nil
      end
    else
      nil
    end
  end

  def guest_2
    if room_type == 'double_room' || room_type == 'twin_room' || room_type == 'triple_room'
      if self.bookings.exclude_extra_room_type.present? && self.bookings.exclude_extra_room_type.second.present?
        self.bookings.exclude_extra_room_type.second 
      elsif self.tour_leader.present? && self.bookings.length == 1
        self.tour_leader
      else
        nil
      end
    else
      nil
    end
  end

  def guest_3
    if room_type == 'double_room' || room_type == 'twin_room'
      if self.bookings.visible.where(room_type: 'Child With Bed')
        self.bookings.visible.where(room_type: 'Child With Bed').first
      else
        nil
      end
    elsif room_type == 'triple_room'
      if self.bookings.exclude_extra_room_type.present?  && self.bookings.exclude_extra_room_type.third.present? 
        self.bookings.exclude_extra_room_type.third
      elsif self.tour_leader.present? && self.bookings.length == 2
        self.tour_leader
      else
        nil
      end
    else
      nil
    end
  end

  def guest_4
    if room_type == 'double_room' || room_type == 'twin_room'
      if self.bookings.visible.where(room_type: 'Child No Bed')
        self.bookings.visible.where(room_type: 'Child No Bed').first
      else
        nil
      end
    else
      nil
    end
  end

  def booking_room_adult_count
    booking_room_adult = self.bookings.adult.count
    if booking_room_adult.present?
      booking_room_adult
    else
      ""
    end
  end

  def booking_room_child_count
    booking_room_child = self.bookings.child_no_bed.count  + self.bookings.child_with_bed.count +  self.bookings.child_twin.count
    if booking_room_child.present?
    booking_room_child
    else
      ""
    end
  end

  def guest_count
    if self.tour_leader.present?
     self.bookings.visible.count + 1
  else
      self.bookings.visible.count
    end
  end

  def booking_room_adult_count
    booking_room_adult = self.bookings.adult.count
    if booking_room_adult.present?
      booking_room_adult
    else
      ""
    end
  end

  def booking_room_child_count
    booking_room_child = self.bookings.child_no_bed.count  + self.bookings.child_with_bed.count +  self.bookings.child_twin.count
    if booking_room_child.present?
    booking_room_child
    else
      ""
    end
  end

  def booking_room_type(guest)
    if room_type == "single_room"
      "Single"
    elsif room_type == "double_room" || room_type == "twin_room"
      if guest == "guest_1" or guest == "guest_2"
        if room_type == "double_room"
          "Double Bed"
        elsif room_type == "twin_room"
          "Twin Sharing"
        end
      elsif guest == "guest_3"
        "Child With Bed"
      elsif guest == "guest_4"
        "Child No Bed"
      end
    elsif room_type == "triple_room"
      "Triple Sharing"
    end
  end
  
  def booking_room_shortform
    if room_type == "single_room"
      "SGL"
    elsif room_type == "double_room"
      "DBL"
    elsif room_type == "twin_room"
      "TWN"    
    elsif room_type == "triple_room"
      "TRP"
    end
  end

end
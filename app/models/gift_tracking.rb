class GiftTracking < ApplicationRecord
  include GiftValidator

  belongs_to :gift_tracking_group, inverse_of: :gift_trackings
  belongs_to :booking_group, optional: true
  has_many :gift_tracking_details, dependent: :destroy
  accepts_nested_attributes_for :gift_tracking_details, allow_destroy: true

  validates :booking_group_id, check_agent: true
  validates :point_topup, numericality: { greater_than_or_equal_to: Proc.new {|o| o.gift_tracking_details.map{|d| d._destroy ? 0 : d.total_points}.inject(:+)}, message: 'are not sufficient' }, if: -> { gift_tracking_details.present? }
end

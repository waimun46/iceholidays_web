class Cover < ApplicationRecord
  belongs_to :taggable, polymorphic: true, optional: true
  mount_uploader :image, CoverUploader
  scope :domain, -> (domain) { where(domain: domain) if domain.present? }
  scope :image_visible, -> { where.not(image: nil) }

  enum category: [:others, :iceb2b_login, :travelb2b_login, :travelb2b_search]
  scope :not_others, -> { where("covers.category != ?", Cover.categories[:others]) }

  # DOMAIN = [
  #   'iceb2b',
  #   'travelb2b'
  # ]

  # def self.homepage(domain = '')
  #   if domain.present?
  #     Cover.where(taggable: nil).domain(domain).where.not(image: nil).order(updated_at: :desc)
  #   else
  #     Cover.where(taggable: nil).where.not(image: nil).order(updated_at: :desc)
  #   end
  # end

end

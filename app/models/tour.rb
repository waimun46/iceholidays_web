class Tour < ApplicationRecord
  include TourValidator

  attr_accessor :edited_user_id

  ADMIN_TOURS_PER_PAGE = 50
  TOURS_PER_PAGE = 20

  mount_uploader :letter_head_image, LogoUploader
  # belongs_to :fair, optional: true
  belongs_to :created_user, class_name: "User", optional: true
  belongs_to :last_edited_user, class_name: "User", optional: true
  belongs_to :itinerary, optional: true, touch: true
  belongs_to :tour_leader, optional: true
  has_many :tour_accomodations, -> { order(:check_in_date) }, inverse_of: :tour, dependent: :destroy
  has_many :tour_flights, -> { order(:departure) }, inverse_of: :tour, dependent: :destroy
  has_many :reminders, as: :taggable, dependent: :destroy
  has_many :bookings
  has_many :booking_groups, through: :bookings
  has_many :booking_rooms
  has_many :booking_cruise_rooms, -> { distinct }, through: :bookings
  accepts_nested_attributes_for :tour_accomodations, reject_if: proc { |attributes| attributes['check_in_date'].blank? || attributes['check_out_date'].blank?}, allow_destroy: true
  accepts_nested_attributes_for :tour_flights, reject_if: proc { |attributes| attributes['departure'].blank? || attributes['arrival'].blank?}, allow_destroy: true
  accepts_nested_attributes_for :reminders, reject_if: proc { |attributes| attributes['reminder_date'].blank? || attributes['email'].blank?}, allow_destroy: true
  accepts_nested_attributes_for :booking_rooms, reject_if: proc { |attributes| attributes['room_type'].blank? }, update_only: true, allow_destroy: true

  SEATS_TYPES = [
    'total_seats',
    'normal1_seats',
    'normal2_seats',
    'normal3_seats',
    'normal4_seats',
    'normal5_seats',
    'normal6_seats',
    'normal7_seats',
    'normal8_seats',
    'normal9_seats',
    'early_bird_seats',
    'specialoffer_seats',
    'specialdeal_seats',
    'superpromo_seats',
    'promo_seats'
  ]

  TUPLE_SEATS_TYPES = [
    'single_twin',
    'triple',
    'quad'
  ]

  ROOM_TYPES = [
    'interior_cabin',
    'oceanview',
    'balcony',
    'suite'
  ]

  PRICE_TYPES = [
    'normal_price',
    'normal1_price',
    'normal2_price',
    'normal3_price',
    'normal4_price',
    'normal5_price',
    'normal6_price',
    'normal7_price',
    'normal8_price',
    'normal9_price',
    'early_bird_price',
    'specialoffer_price',
    'specialdeal_price',
    'superpromo_price',
    'promo_price'
  ]

  LANGUAGES = [
    'English Speaking Guide (英文导游)',
    'Chinese Speaking Guide (中文导游)',
    'Bilingual Speaking Guide (双语导游）'
  ]

  TUPLE_PRICE_TYPES = [
    'single',
    'twin',
    'triple',
    'quad'
  ]

  ALL_FARE_TYPES = [
    'adult',
    'child_with_bed',
    'child_no_bed',
    'child_twin',
    'infant_price'
  ]

  CHILD_FARE_TYPES = [
    'child_with_bed_percentage',
    'child_no_bed_percentage',
    'child_twin_percentage',
    'infant_price'
  ]

  EXCLUDING_TOUR_CODE = [
    "CHHA",
    "CHHB",
    "CHHC",
    "CHHD",
    "CHHO", 
    "CHHU",
    "CHHU-2",
    "CHHV",
    "CHHV-2",
    "CHHY", 
    "CHHX",
    "CHB-SP"
  ]

  ALL_SEATS_TYPE = SEATS_TYPES +
  TUPLE_SEATS_TYPES.product(ROOM_TYPES).map{|x| x.join('_')}.product(SEATS_TYPES).map{|x| x.join('_')}

  ALL_TOTAL_SEATS = Tour::ALL_SEATS_TYPE.select{|x| x.include?('total_seats')}

  ALL_PRICE_TYPE = PRICE_TYPES +
  TUPLE_PRICE_TYPES.product(ROOM_TYPES).map{|x| x.join('_')}.product(PRICE_TYPES).map{|x| x.join('_')}

  DTA_PRICE_TYPE = PRICE_TYPES.select{|key| !key.match?(/normal\d/)}.map{|price_type| "dta_#{price_type.sub("_price", '')}"}
  DTA_CATEGORY = DTA_PRICE_TYPE.product(ALL_FARE_TYPES - ['infant_price']).map{|x| x.join('_')}

  CRUISE_ROOM_TYPE = TUPLE_SEATS_TYPES.product(ROOM_TYPES).map{|x| x.join('_')}
  CRUISE_ROOM_TYPE_DETAIL = TUPLE_PRICE_TYPES.product(ROOM_TYPES).map{|x| x.join('_')}

  CHILD_FARE_APPLY_PRICE_TYPES = PRICE_TYPES.select{|key| !key.match?(/normal\d/)}.map{|pt| "child_fare_apply_#{pt}"}

  typed_store :max_seats, coder: PostgresCoder do |s|
    ALL_SEATS_TYPE.each do |st|
      s.integer st, blank: false, default: 0
    end
  end
  typed_store :prices, coder: PostgresCoder do |p|
    ALL_PRICE_TYPE.each do |pt|
      p.integer pt
    end
    p.integer :highest_sold_price
    CRUISE_ROOM_TYPE_DETAIL.each do |cr|
      p.integer "#{cr}_highest_sold_price"
    end
  end
  typed_store :child_fares, coder: PostgresCoder do |c|
    CHILD_FARE_TYPES.each do |ct|
      c.integer ct, default: 0
    end
    CHILD_FARE_APPLY_PRICE_TYPES.each do |pt|
      c.boolean pt, default: true, null: false
    end
  end
  typed_store :dta_prices, coder: PostgresCoder do |d|
    DTA_CATEGORY.each do |dc|
      d.integer dc, default: 0
    end
  end
  typed_store :subtract_fees, coder: PostgresCoder do |d|
    d.integer :visa_fee, default: 0
    d.integer :subtract_others, default: 0
  end
  typed_store :additional_fees, coder: PostgresCoder do |d|
    d.integer :compulsory_additional_fee, default: 0
    d.integer :addon_others, default: 0
  end

  ZONE = [
    'Zone 1',
    'Zone 2',
    'Zone 3',
    'Domestic'
  ]

  SORTING = {
    "Code" => "code",
    "Caption" => "caption",
    "Highlight" => "highlight",
    "Contient Country" => "contient_country",
    "Category" => "category",
    "Prices" => "prices",
    "Max Seats" => "max_seats",
    "Departure" => "departure_date",
    "Guaranteed Flag" => "guaranteed_departure_flag",
    "Active" => "active"
  }

  validates :code, :caption, :other_caption, :category, :contient_country, :departure_date, :deposit, presence: true
  validates :max_booking_seats, numericality: { greater_than: 0 }
  validates :single_supplement_price, :total_seats, numericality: { greater_than: 0 }, if: -> {!is_cruise? }
  validates :dta_normal_adult, numericality: { greater_than: 0, message: "must be greater than 0,  when you key in Normal Price." }, if: -> { active? && normal_price.to_i > 0}
  validates :dta_promo_adult, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Promo Price" }, if: -> { active? && promo_price.to_i > 0}
  validates :dta_superpromo_adult, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Super Promo Price" }, if: -> { active? && superpromo_price.to_i > 0}
  validates :dta_specialdeal_adult, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Special Deal Price" }, if: -> { active? && specialdeal_price.to_i > 0}
  validates :dta_specialoffer_adult, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Special Offer Price" }, if: -> { active? && specialoffer_price.to_i > 0}
  validates :dta_early_bird_adult, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Early Bird Price" }, if: -> { active? && early_bird_price.to_i > 0}
  validates :dta_normal_child_twin, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (Twin)% & Normal Price" }, if: -> { active? && child_twin_percentage.to_i > 0 && normal_price.to_i > 0 }
  validates :dta_normal_child_with_bed, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (With Extra Bed)% & Normal Price" },  if: -> { active? && child_with_bed_percentage.to_i > 0 && normal_price.to_i > 0 }
  validates :dta_normal_child_no_bed, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (With Extra Bed)% & Normal Price" },  if: -> { active? && child_no_bed_percentage.to_i > 0 && normal_price.to_i > 0 }
  validates :dta_promo_child_twin, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (Twin)% & Promo Price" }, if: -> { active? && child_twin_percentage.to_i > 0 && promo_price.to_i > 0 }
  validates :dta_promo_child_with_bed, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (With Extra Bed)% & Promo Price" },  if: -> { active? && child_with_bed_percentage.to_i > 0 && promo_price.to_i > 0 }
  validates :dta_promo_child_no_bed, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (With Extra Bed)% & Promo Price" },  if: -> { active? && child_no_bed_percentage.to_i > 0 && promo_price.to_i > 0 }
  validates :dta_superpromo_child_twin, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (Twin)% & Super Promo Price" }, if: -> { active? && child_twin_percentage.to_i > 0 && superpromo_price.to_i > 0 }
  validates :dta_superpromo_child_with_bed, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (Twin)% & Super Promo Price" }, if: -> { active? && child_with_bed_percentage.to_i > 0 && superpromo_price.to_i > 0 }
  validates :dta_superpromo_child_no_bed, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (Twin)% & Super Promo Price" }, if: -> { active? && child_no_bed_percentage.to_i > 0 && superpromo_price.to_i > 0 }
  validates :dta_specialdeal_child_twin, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (Twin)% & Speical Deal Promo Price" }, if: -> { active? && child_twin_percentage.to_i > 0 && specialdeal_price.to_i > 0 }
  validates :dta_specialdeal_child_with_bed, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (Twin)% & Speical Deal Promo Price" }, if: -> { active? && child_with_bed_percentage.to_i > 0 && specialdeal_price.to_i > 0 }
  validates :dta_specialdeal_child_no_bed, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (Twin)% & Speical Deal Promo Price" }, if: -> { active? && child_no_bed_percentage.to_i > 0 && specialdeal_price.to_i > 0 }
  validates :dta_specialoffer_child_twin, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (Twin)% & Special Offer Price" }, if: -> { active? && child_twin_percentage.to_i > 0 && specialoffer_price.to_i > 0 }
  validates :dta_specialoffer_child_with_bed, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (Twin)% & Special Offer Price" }, if: -> { active? && child_with_bed_percentage.to_i > 0 && specialoffer_price.to_i > 0 }
  validates :dta_specialoffer_child_no_bed, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (Twin)% & Special Offer Price" }, if: -> { active? && child_no_bed_percentage.to_i > 0 && specialoffer_price.to_i > 0 }
  validates :dta_early_bird_child_twin, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (Twin)% & Early Bird Price" }, if: -> { active? && child_twin_percentage.to_i > 0 && early_bird_price.to_i > 0 }
  validates :dta_early_bird_child_with_bed, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (Twin)% & Early Bird Price" }, if: -> { active? && child_with_bed_percentage.to_i > 0 && early_bird_price.to_i > 0 }
  validates :dta_early_bird_child_no_bed, numericality: { greater_than: 0, message: "must be greater than 0, when you key in Child Price (Twin)% & Early Bird Price" }, if: -> { active? && child_no_bed_percentage.to_i > 0 && early_bird_price.to_i > 0 }
  validates :code, uniqueness: { case_sensitive: false }
  validates :code, length: { maximum: 25 }
  validates :caption, length: { maximum: 100 }
  validates :other_caption, length: { maximum: 100 }
  validates :contient_country, :departure_date, :max_booking_seats, :deposit, presence: true, if: :active?
  # validates :early_bird_date, early_bird_date: true
  validates :guaranteed_departure_seats, presence: { message: "can't be blank, when Guaranteed Departure Flag is Yes" }, if: -> { guaranteed_departure_flag? && !is_cruise? }
  validates :ground_price, presence: true, if: -> { ground_price_flag? }
  validates :itinerary_id, presence: true, if: :active?

  validates_with TourPriceSeatsValidator, if: :active?
  if !Rails.configuration.crawling
    validates :deposit, tour_promo_price: true, if: :active?
  end
  validates :highlight, length: { maximum: 50 }

  # enum category: [:GD_Luxury_5, :GD_Premium_4_5, :GD_Premium_5, :GD_Standard_4, :GD_Standard_4_5, :GD_Value_3, :GD_Value_3_4, :GD_Value_3_5, :FS_3, :FS_4, :FS_5, :FS_3_4, :FS_3_5, :FS_4_5]
  enum category: [:GD_Luxury_5, :GD_Premium_5, :GD_Premium_5_4, :GD_Premium_4_5, :GD_Standard_4, :GD_Standard_3, :GD_Standard_4_3, :GD_Standard_4_5, :GD_Value_3, :GD_Value_3_4, :GD_Value_3_5, :FS_3, :FS_4, :FS_5, :FS_3_4, :FS_3_5, :FS_4_5, :FIT, :GT, :CRUISE, :GD_Standard_5, :GD_FLY_CRUISE, :GD_Premium_4]

  scope :search, ->(keyword) { where("tours.code ILIKE ? or tours.caption ILIKE ? or tours.other_caption ILIKE ? or tours.contient_country ILIKE ?", "%#{keyword}%", "%#{keyword}%", "%#{keyword}%",  "%#{keyword}%") }
  scope :destination, ->(keyword) { where("tours.contient_country ILIKE ?", "%#{keyword}%") }
  scope :caption, ->(keyword) { where("tours.caption ILIKE ?", "%#{keyword}%") }
  scope :caption_uniq, -> { select("distinct on (tours.caption) tours.*") }
  scope :visible, -> { where("tours.active = true") }
  scope :with_visible, -> (only=true) { where("tours.active = true") if only }
  scope :bookable, ->(interval) { where("tours.vacant_seats_available = true").where(departure_date: (interval)).where("cut_off_date > ?", Time.now)}
  scope :before_departure, ->(time) { where("tours.departure_date > :time", time: time) }
  scope :in_between_departure, ->(interval) { where(departure_date:(interval))}
  scope :in_between_cut_off, ->(interval) { where(cut_off_date:(interval))}
  scope :with_bookings, -> (only=true) { includes(:bookings).where.not(bookings: {id: nil}).where(bookings: {status: 0}) if only }
  scope :in_between_bookings, -> (interval) { includes(:bookings).where(bookings: {created_at: interval, status: :active})}
  scope :uncertainty, -> { where("(is_cruise is false and guaranteed_departure_flag is true and guaranteed_departure_seats > 0 and (max_seats ->> 'total_seats')::int - (vacant_seats ->> 'total_seats')::int < guaranteed_departure_seats) or (is_cruise is true and guaranteed_departure is false)")}
  scope :certainty, -> { where("(guaranteed_departure_flag is true and guaranteed_departure_seats > 0 and payment_seats >= guaranteed_departure_seats)")}
  scope :in_between_cut_off, ->(interval) { where(cut_off_date:(interval))}
  scope :only_cruise, -> { where("tours.contient_country ILIKE '%CRUISE%' or tours.is_cruise = true") }
  scope :exlcuded_cruise, -> { where("tours.contient_country NOT ILIKE '%CRUISE%' or tours.is_cruise != true") }
  scope :only_fs, -> { where(category: [:FS_3, :FS_4, :FS_5, :FS_3_4, :FS_3_5, :FS_4_5]) }
  scope :only_gd, -> { where(category: [:GD_Standard_5, :GD_Standard_4, :GD_Standard_3, :GD_Standard_4_3, :GD_Standard_4_5, :GD_Value_3, :GD_Value_3_4, :GD_Value_3_5]) }
  scope :only_premium, -> { where(category: [:GD_Luxury_5, :GD_Premium_5, :GD_Premium_5_4, :GD_Premium_4_5, :GD_Premium_4]) }
  scope :only_gtrip, -> { where(category: [:GT, :FIT]) }
  scope :with_itinerary, -> (itinerary_id) { includes(:itinerary).where(tours: {itinerary_id: itinerary_id}) if itinerary_id.present? }
  scope :non_expired, -> { where("departure_date > ?", Time.now)}

  after_commit :calculate_vacant_seats, on: [:create, :update]
  after_save :update_autocomplete, if: -> { saved_change_to_contient_country? }
  before_save :set_cut_off_date, if: :active?
  before_save :set_early_bird_date, if: :active?
  before_save :update_last_edited_user


 ['operator_remarks','pnrs'].each do |f|
    define_method "join_#{f}" do
      if self.send(f).present?
        self.send(f).join(',')
      else
        ''
      end
    end
  end
  
  def able_to_purcharse?(user, check_list, booked_from = 'iceb2b')
    # when user is nil, it is from admin
    if (self.departure_date > Time.now && self.able_to_cut_off_date_purchase?(booked_from) ) || user == nil
      available = self.available_vacant_seats(user)
      check_list.each do |type, detail|

        if !available.has_key?(type)
          return false
        end
        detail.each do |price_type, seats|
          if !available[type].has_key?(price_type) || seats <= 0 || available[type][price_type][:seats] < seats
            return false
          end
        end
      end
      true
    else
      false
    end
  end

  def able_to_cut_off_date_purchase?(booked_from)
    if booked_from == 'travelb2b'
      if self.cut_off_date - 3.days > Time.now
        true
      elsif self.cut_off_date - 3.days >= Time.now && self.cut_off_date >= Time.now
        self.guaranteed_departure?
      else
        false
      end
    else
      self.cut_off_date > Time.now
    end
  end

  def final_price(price_type, category, single_supplement = false, booked_from = 'iceb2b')
    final_price = 0

    child_fare_price_type = price_type.sub(/normal\d/, 'normal')

    if self.misc_fee.present? && category.include?("child_") && self.child_fares["child_fare_apply_#{child_fare_price_type}"].present? && self.child_fares["child_fare_apply_#{child_fare_price_type}"] && self.child_fares["#{category}_percentage"].present? && self.child_fares["#{category}_percentage"] > 0
      final_price = (((self.prices["#{price_type}"] - self.misc_fee) * (self.child_fares["#{category}_percentage"].to_f / 100)) + self.misc_fee).round
    elsif category == "infant"
      final_price = self.child_fares["infant_price"]
    else
      final_price = self.prices["#{price_type}"]
    end

    if single_supplement && booked_from == 'travelb2b' && self.single_supplement_price.present? && self.single_supplement_price > 0
      final_price += self.single_supplement_price
    end

    final_price
  end

  def final_addon
    addon = 0
    self.compulsory_additional_fee.present? ? addon += self.compulsory_additional_fee : addon
    self.addon_others.present? ? addon += self.addon_others : addon
  end

  def final_subtract(price_type, category, minus_dta, user, total_price_after_dta)
    list = {
      visa_fee: 0, 
      subtract_others: 0,
      insurance_rebate: 0,
      dta: 0
    }
    if category != "infant"
      list[:visa_fee] = self.visa_fee
      list[:subtract_others] = self.subtract_others

      if minus_dta
        list[:insurance_rebate] = self.insurance_rebate
        list[:dta] = self.dta(price_type.remove_price_seats, category)
        list.merge!(self.pre_commisions(user, total_price_after_dta))
      end
    end

    list
  end

  def pre_commisions(user, total_price_after_dta)
    list = {}

    if user.present? && total_price_after_dta.present?
      pre_commisions = PreCommision.ongoing.active.allow_user(user).allow_itinerary(self.itinerary)
      pre_commisions.each do |pre_commision|
        pre_commisions_amount = pre_commision.commision_category == 'fixed' ? pre_commision.commisions : pre_commision.commision_category == 'percentage' ? total_price_after_dta * pre_commision.commisions / 100 : 0
        list.merge!({pre_commision.title.to_sym => pre_commisions_amount})
      end
    end
    
    list
  end

  def dta(price_type, category)

    price_type = price_type.sub(/normal\d/, 'normal')

    if category.include?("child_") && self.child_fares["#{category}_percentage"].present? && self.child_fares["#{category}_percentage"] > 0 && self.dta_prices["dta_#{price_type}_#{category}"].present?
      self.dta_prices["dta_#{price_type}_#{category}"]
    elsif category == "infant"
      0
    elsif self.dta_prices["dta_#{price_type}_#{category}"].present?
      self.dta_prices["dta_#{price_type}_adult"]
    else
      0
    end
  end

  def dta_price_text(price_type, category)
    "DTA : #{self.dta(price_type, category)}"
  end

  def child_price_available?
    self.child_fares.except("infant_price").except(*Tour::CHILD_FARE_APPLY_PRICE_TYPES).values.all?{|v| v.present? && v > 0} && 
      self.child_fares.except("infant_price").except(*Tour::CHILD_FARE_TYPES).values.any?{|v| v.present? && v }
  end

  def admin_able_to_purcharse?(check_list)
    able_to_purcharse?(nil, check_list)
  end

  def active=(val)
    if val.present? && val && self.first_activated_at.blank?
      write_attribute(:first_activated_at, Time.now)
    end
    write_attribute(:active, val)
  end

  def calculate_normal_seat
    if self.total_seats.present?
      normal_seats = self.total_seats-(self.early_bird_seats+self.specialoffer_seats+self.specialdeal_seats+self.superpromo_seats+self.promo_seats)
    else
       ''
    end
  end
  
  def available_vacant_seats(user)
    available_vacant_list = {}
    total_seats = {}

    if self.vacant_seats.present? && self.vacant_seats_available?
      # select only (vacant > 0) keys
      available_vacant_seats_count = self.vacant_seats.clone

      if self.is_cruise?
        # split the single_twin into two records
        single_twin_seats = available_vacant_seats_count.extract!(*available_vacant_seats_count.keys.select{|key| key.include?('single_twin')})
        single_twin_seats.each do |k,v|
          available_vacant_seats_count[k.sub('single_twin', 'single')] = v
          available_vacant_seats_count[k.sub('single_twin', 'twin')] = v
        end
        available_types = available_vacant_seats_count.keys.map {|k| k.sub(/(#{Tour::SEATS_TYPES.join("|")})/, '').chomp('_')}.uniq
      else
        available_types = ['']
      end

      # early_bird_available = user == nil || (self.early_bird_date.present? && self.early_bird_date >= Time.now)
      early_bird_available = false
      # promo_available = user == nil || user.fair_available?('promo')
      # superpromo_available = user == nil || user.fair_available?('superpromo')
      # specialdeal_available = user == nil || user.fair_available?('specialdeal')
      promo_available = user == nil || (self.itinerary_id.present? ? user.fair_available?('promo', self.itinerary_id) : false)
      superpromo_available = user == nil || (self.itinerary_id.present? ? user.fair_available?('superpromo', self.itinerary_id) : false)
      specialdeal_available = user == nil || (self.itinerary_id.present? ? user.fair_available?('specialdeal', self.itinerary_id) : false)
      specialoffer_available = true

      available_types.each do |type|
        type_ = if type == ''
          type
        else
          type + '_'
        end

        seats_type = if type.include?('single')
          type_.sub("twin", "single_twin")
        elsif type.include?('twin')
          type_.sub("single", "single_twin")
        else
          type_
        end


        unless early_bird_available &&
          self.send("#{seats_type}early_bird_price").to_i > 0 &&
          self.send("#{type_}early_bird_price").present? && self.send("#{type_}early_bird_price") > 0
          available_vacant_seats_count.except!("#{type_}early_bird_seats")
        end
        unless promo_available &&
          self.send("#{seats_type}promo_price").to_i > 0 &&
          self.send("#{type_}promo_price").present? && self.send("#{type_}promo_price") > 0
          available_vacant_seats_count.except!("#{type_}promo_seats")
        end
        unless superpromo_available &&
          self.send("#{seats_type}superpromo_price").to_i > 0 &&
          self.send("#{type_}superpromo_price").present? && self.send("#{type_}superpromo_price") > 0
          available_vacant_seats_count.except!("#{type_}superpromo_seats")
        end
        unless specialdeal_available &&
          self.send("#{seats_type}specialdeal_price").to_i > 0 &&
          self.send("#{type_}specialdeal_price").present? && self.send("#{type_}specialdeal_price") > 0
          available_vacant_seats_count.except!("#{type_}specialdeal_seats")
        end
        unless specialoffer_available &&
          self.send("#{seats_type}specialoffer_price").to_i > 0 &&
          self.send("#{type_}specialoffer_price").present? && self.send("#{type_}specialoffer_price") > 0
          available_vacant_seats_count.except!("#{type_}specialoffer_seats")
        end

        total_seats[type] = available_vacant_seats_count.delete("#{type_}total_seats") || 0
        other_seats = available_vacant_seats_count.select{|k,v| k.include?(type_) && v > 0}.values.inject(:+) || 0

        if total_seats[type] > other_seats
          available_vacant_seats_count["#{type_}normal_seats"] = total_seats[type] - other_seats
        end

        available_vacant_seats_count.select{|k,v| k.include?(type_) && v > 0 && self.prices[k.seats_to_price].present? && self.prices[k.seats_to_price] > 0 }.each do |key, value|
          if !available_vacant_list.has_key?(type)
            available_vacant_list[type] = {}
          end
          available_vacant_list[type].store(key.remove_price_seats.sub(type_, ''), {
            "seats": available_vacant_seats_count[key], 
            "price": self.prices[key.seats_to_price],
            "child_with_bed_price": self.final_price(key.seats_to_price, "child_with_bed"),
            "child_no_bed_price": self.final_price(key.seats_to_price, "child_no_bed"),
            "child_twin_price": self.final_price(key.seats_to_price, "child_twin"),
            "infant_price": self.final_price(key.seats_to_price, "infant_price")
          })
        end
      end

      available_vacant_list.keys.each do |type|
        available_vacant_list[type] = available_vacant_list[type].sort_by{|k,v| v[:price]}.to_h

        available_vacant_list[type].delete_if do |key,value|

          if total_seats[type] == 0
            true
          elsif total_seats[type] > value[:seats]
            total_seats[type] -= value[:seats]
            false
          elsif value[:seats] > total_seats[type]
            value[:seats] = total_seats[type]
            total_seats[type] = 0
            false
          end 
        end
      end
      available_vacant_list.select!{|k,v| v.present?}
    end
    if available_vacant_list.blank?
      if self.is_cruise?
        available_vacant_list = self.all_normal_prices.map{|k,v| [k.sub("_normal_price", ''), {"normal" => {"seats": 0, "price": v}}]}.to_h
      else
        available_vacant_list = {"" => {"normal"=> {"seats": 0, "price": self.normal_price.present? ? self.normal_price : 0 }}}
      end
    end
    available_vacant_list
  end

  def available_vacant_seats_sorted(user)
    avs = self.available_vacant_seats(user)
    if avs.length > 1
      avs.sort_by{|k,v| Tour::CRUISE_ROOM_TYPE_DETAIL.index(k)}.to_h
    else
      avs
    end
  end

  def all_normal_prices
    all_available_normal_prices = self.prices.select do |k,v|
      vacant_seat_type = k.sub(/(single|twin)_/ , 'single_twin_').sub('_normal_price', '_total_seats')
      k.include?('normal') && v.present? && v > 0 && self.vacant_seats[vacant_seat_type].present? && self.vacant_seats[vacant_seat_type] > 0
    end
    if all_available_normal_prices.blank?
      all_available_normal_prices = self.prices.select{|k,v| k.include?('normal') && v.present? && v > 0}
    end
    all_available_normal_prices
  end

  def all_total_seats
    if self.is_cruise?
      sum = 0
      ALL_TOTAL_SEATS.each do |type_seats|
        if self.send(type_seats) > 0
          sum = sum + self.send(type_seats) * Tour.type_pax(type_seats)
        end
      end
      sum
    else
      self.total_seats
    end
      
  end

  def all_total_rooms
    if self.is_cruise?
      sum = 0
      ALL_TOTAL_SEATS.each do |type_seats|
        if self.send(type_seats) > 0
          sum = sum + self.send(type_seats)
        end
      end
      sum
    else
      0
    end
  end

  def all_vacant_total_seats
    if self.is_cruise?
      sum = 0
      ALL_TOTAL_SEATS.each do |type_seats|
        if self.vacant_seats[type_seats].present? && self.vacant_seats[type_seats] > 0
          sum = sum + self.vacant_seats[type_seats] * Tour.type_pax(type_seats)
        end
      end
      sum
    else
      self.vacant_seats['total_seats']
    end
  end

  def all_vacant_total_rooms
    if self.is_cruise?
      sum = 0
      ALL_TOTAL_SEATS.each do |type_seats|
        if self.vacant_seats[type_seats].present? && self.vacant_seats[type_seats] > 0
          sum = sum + self.vacant_seats[type_seats]
        end
      end
      sum
    else
      0
    end
  end

  def bookable?
    if self.active? && self.departure_date > Time.now && self.cut_off_date > Time.now
      true
    else
      false
    end
  end

  def brand
    if self.is_cruise?
      "cruise"
    elsif self.premium?
      "premium"
    elsif self.gtrip?
      "gtrip"
    elsif self.four_seasons?
      "fs"
    else
      "gd"
    end
  end

  def price_type_list
    self.prices.select{|k,v| v.present? && v > 0}.keys.map{ |x| x.remove_price_seats}
  end

  def category_display
    if category.present?
      category.gsub(/_/, ' ').gsub(/(\d)/, '(\1)')
    else
      ''
    end
  end

  def category_brand
    if self.category.present?
      self.category.gsub("_", " ").tr("0-9", "").rstrip
    else
      ""
    end
  end

  def hotel_rating
    if category == nil
     html = "-"
    elsif category.present?
      category.scan(/\d/).join('+')
    else
      ''
    end
  end

  def cheapest_promo_price(user)
    cheapest_promo = self.cheapest_promo(user)
    if cheapest_promo.present?
      cheapest_promo.values.last.values.last[:price]
    else
      '-'
    end
  end

  def cheapest_promo_type(user)
    cheapest_promo = self.cheapest_promo(user)
    if cheapest_promo.present?
      cheapest_promo.values.last.keys.first
    else
      ''
    end
  end

  def cheapest_promo(user)
    cheapest_seat = self.cheapest_seat(user)
    if cheapest_seat.present? && !cheapest_seat.values.last.keys.first.include?("normal")
      cheapest_seat
    else
      nil
    end
  end

  def cheapest_normal_price
    if self.is_cruise?
      self.all_normal_prices.values.min
    else
      cheapest_normal = self.vacant_seats.select{|key, seats| key.include?('normal') && seats > 0}.keys.first
      if cheapest_normal.present?
        self.send(cheapest_normal.seats_to_price)
      else
        self.normal_price
      end
    end
  end

  def min_price(user)
    [
      [self.cheapest_normal_price.to_i].min, 
      [self.cheapest_promo_price(user).to_i].min
    ]
    .reject do |value| value == 0 end.min
  end

  def cheapest_seat(user)
    available = self.available_vacant_seats(user)
    if available.present?
      if available.length == 1 && available.keys == [""]
        {""=> [available[""].first].to_h}
      else
        [available.min_by {|k,v| v.values.first[:price]}].to_h
      end
    else
      nil
    end
  end

  def calculate_payment_seats
    update_column(:payment_seats, self.bookings.visible.with_receipt_or_fair_booked.count)
  end

  def calculate_vacant_seats
    if self.active

      # group the active booking as sold seats
      sold_seats = self.sold_seats

      # if self.is_cruise?
      #   available_types = Tour::CRUISE_ROOM_TYPE
      # else
      #   available_types = ['']
      # end

      # set max_seats as defalt vacant seats
      vacant_seats = self.max_seats_clear
      if sold_seats.present?
        if self.is_cruise?
          Tour::CRUISE_ROOM_TYPE.each do |type|
            sold_seats["#{type}_total_seats"] = sold_seats.select{|key,value| key.include?(type)}.values.inject(:+).to_i
          end
        else
          sold_seats["total_seats"] = sold_seats.values.inject(:+).to_i
        end
        valid_key_seats = vacant_seats.keys
        sold_seats.delete_if{|key, value| !valid_key_seats.include?(key)}
        vacant_seats.merge!(sold_seats){|key, oldval, newval| oldval - newval}
      end
      # vacant_seats if value <0 means overbooking
      vacant_seats.select!{|key, value| value != nil }

      # calculate the highest sold prices
      if self.is_cruise?
        highest_sold_prices = booking_cruise_rooms.visible.only_normal.group(:room_type).maximum(:original_price)
        CRUISE_ROOM_TYPE_DETAIL.each do |cruise_room_type|
          if highest_sold_prices.has_key?(cruise_room_type)
            self.send("#{cruise_room_type}_highest_sold_price=", highest_sold_prices[cruise_room_type])
          elsif self.send("#{cruise_room_type}_highest_sold_price").present?
            self.send("#{cruise_room_type}_highest_sold_price=", 0)
          end
        end
      else
        highest_sold_price = bookings.visible.only_normal.maximum(:original_price)
        if highest_sold_price.present?
          self.highest_sold_price = highest_sold_price
        else
          self.highest_sold_price = 0
        end
      end

      # Release(move) those price lower than highest_sold_price vacant_seats to highest_sold_price seats
      if sold_seats.present?
        if self.is_cruise?
          Tour::CRUISE_ROOM_TYPE.each do |type|
            type_ = if type == ''
              type
            else
              type + '_'
            end

            if type.include?('single_twin')
              single_highest_sold_price = self.send("#{type_.sub('single_twin', 'single')}highest_sold_price").present? ? self.send("#{type_.sub('single_twin', 'single')}highest_sold_price") : 0
              twin_highest_sold_price = self.send("#{type_.sub('single_twin', 'twin')}highest_sold_price").present? ? self.send("#{type_.sub('single_twin', 'twin')}highest_sold_price") : 0
            else
              highest_sold_price = self.send("#{type_}highest_sold_price").present? ? self.send("#{type_}highest_sold_price") : 0
            end

            release_vacants = 0

            self.prices.select do |price_type, price|
              # WORKAROUND: if the type is single_twin, just treat it as twin, sort the twin prices (ignore the single prices sorting)
              (price_type.start_with?("#{type_.sub('single_twin', 'twin')}specialoffer") || price_type.start_with?("#{type_.sub('single_twin', 'twin')}normal")) && price_type != "#{type_.sub('single_twin', 'twin')}normal_price" && price.present? && price > 0 
            end.sort_by(&:last).each do |price_type, price|

              price_type_to_vacant_type = price_type.sub('twin_', 'single_twin_').price_to_seats

              if vacant_seats[price_type_to_vacant_type].present?
                

                if type.include?('single_twin') && 
                  # Check the twin highest_sold_price with current price tier and get the single price tier to compare with the single highest sold price
                  (price >= twin_highest_sold_price && ( self.send(price_type.sub('twin_', 'single_')).blank? || self.send(price_type.sub('twin_', 'single_')).zero? || self.send(price_type.sub('twin_', 'single_')) >= single_highest_sold_price) ) && 
                  vacant_seats[price_type_to_vacant_type] > 0 && (vacant_seats[price_type_to_vacant_type] + release_vacants) > 0
                  vacant_seats[price_type_to_vacant_type] += release_vacants
                  break
                elsif !type.include?('single_twin') && (price >= highest_sold_price) && vacant_seats[price_type_to_vacant_type] > 0 && (vacant_seats[price_type_to_vacant_type] + release_vacants) > 0
                  vacant_seats[price_type_to_vacant_type] += release_vacants
                  break
                else
                  release_vacants += vacant_seats[price_type_to_vacant_type]
                  vacant_seats[price_type_to_vacant_type] = 0
                end
              end
            end
          end
        else
          release_vacants = 0
          self.prices.select do |price_type, price|
            (price_type.start_with?('specialoffer') || price_type.start_with?('normal')) && price_type != 'normal_price' && price.present? && price > 0 
          end.sort_by(&:last).each do |price_type, price|

            price_type_to_vacant_type = price_type.price_to_seats

            if vacant_seats[price_type_to_vacant_type].present?
              if price >= self.highest_sold_price && vacant_seats[price_type_to_vacant_type] > 0 && (vacant_seats[price_type_to_vacant_type] + release_vacants) > 0
                vacant_seats[price_type_to_vacant_type] += release_vacants
                break
              else
                release_vacants += vacant_seats[price_type_to_vacant_type]
                vacant_seats[price_type_to_vacant_type] = 0
              end
            end
          end
        end
      end

      if self.itinerary.present?
        self.itinerary.touch
      end
      # update_columns(vacant_seats: vacant_seats)
      update_columns(vacant_seats: vacant_seats, vacant_seats_available: vacant_seats.present? && vacant_seats.select{|key, value| key.include?('total_seats')}.values.inject(:+).to_i > 0, prices: self.prices)
    end
  end

  def cut_off?
    if self.active? && self.cut_off_date.present? && self.cut_off_date < Time.now
      true
    else
      false
    end
  end

  def etravel_dept_code
    if itinerary.present? && itinerary.department.present?
      itinerary.department.etravel_dept_code
    else
      ""
    end
  end

  def four_seasons?
    FS_3? || FS_4? || FS_5? || FS_3_4? || FS_3_5? || FS_4_5?
  end

  def standard?
    GD_Standard_5? || GD_Standard_4? || GD_Standard_3? || GD_Standard_4_3? || GD_Standard_4_5? || GD_Value_3? || GD_Value_3_4? || GD_Value_3_5?
  end

  def premium?
    GD_Luxury_5? || GD_Premium_5? || GD_Premium_5_4? || GD_Premium_4_5? || GD_Premium_4?
  end

  def gtrip?
    GT? || FIT?
  end

  def itinerary_code
    if itinerary.present?
      itinerary.code
    else
      ''
    end
  end

  def itinerary_code=(val)
    itinerary = Itinerary.find_by_code(val)
    if itinerary.present?
      write_attribute(:itinerary_id, itinerary.id)
    else
      write_attribute(:itinerary_id, nil)
    end
  end

  def set_cut_off_date
    if self.active && self.cut_off_date.blank? && self.departure_date.present?
      self.cut_off_date = self.departure_date - 28.days
    end
  end

  def set_early_bird_date
    if self.active && self.early_bird_date.blank? && self.departure_date.present?
      self.early_bird_date = self.departure_date - 6.months
    end
  end

  def early_bird_available?
    if self.early_bird_date.present? && self.early_bird_date >= Time.now
      true
    else
      false
    end
  end

  def status_text
    text = ""
    if self.vacant_seats? && self.vacant_seats_available?
      if self.first_activated_at.present? && self.first_activated_at > (Date.today - 5.days)
        text += "New"
      end

      if self.guaranteed_departure_flag && self.guaranteed_departure_seats.present? && self.payment_seats >= self.guaranteed_departure_seats
        if text != ""
          text += " / "
        end
        text += "Guaranteed"
      elsif self.guaranteed_departure_flag && self.guaranteed_departure_seats.present? && (self.payment_seats + self.max_booking_seats) >= self.guaranteed_departure_seats
        if text != ""
          text += " / "
        end
        text += "#{self.guaranteed_departure_seats - self.payment_seats} to Guaranteed"
      end
    else
      text = "Sold Out"
    end
    text
  end

  def vacant_seats?
    self.vacant_seats_available
  end

  def fair_display
    if fair.present?
      "#{fair.code} #{fair.title}"
    else
      ''
    end
  end

  def update_autocomplete
    if self.contient_country.present?
      ac = Autocomplete.find_by_name(:contient_country)
      keywords = ac.keywords.map(&:downcase)
      if ac.keywords.present?
        unless keywords.include?(contient_country.downcase)
          ac.keywords << contient_country
          ac.save
        end
      else
        ac.keywords = [contient_country]
        ac.save
      end
    end
  end

  def update_last_edited_user
    if self.edited_user_id.present? && self.changed?
      self.last_edited_user_id = edited_user_id
      self.last_edited_at = Time.now
    end
  end

def self.with_sorting_type(sorting_type)
  order_by =  if sorting_type.include?(" ASC")
                'ASC'
              elsif sorting_type.include?(" DESC")
                'DESC'
              else
                ''
              end
  sort_by = sorting_type.sub(/\s(ASC|DESC)\z/, '')

  if order_by.present? && Tour::SORTING.has_key?(sort_by)
    includes(:bookings).order("#{Tour::SORTING[sort_by]} #{order_by}")
  else
    where('')
  end
end

  def self.brand_filter(brand)
    if brand == 'fs'
      only_fs
    elsif brand == 'premium'
      only_premium
    elsif brand == 'cruise'
      only_cruise
    elsif brand == 'gtrip'
      only_gtrip.exlcuded_cruise
    else
      only_gd
    end
  end

  def department
    self.itinerary.department
  end

  def self.duplicate(tour, departure_date, code)
    new_tour = tour.dup
    new_tour.departure_date = departure_date
    new_tour.cut_off_date = nil
    new_tour.code = code
    new_tour.active = false
    new_tour.payment_seats = 0

    if tour.itinerary.present? && tour.itinerary.archived?
      new_tour.itinerary_id = nil
    end

    if new_tour.save
      days_offset = new_tour.departure_date - tour.departure_date
      tour.tour_flights.each do |tour_flight|
        new_tour_flight = tour_flight.dup
        new_tour_flight.tour = new_tour
        new_tour_flight.departure = tour_flight.departure + days_offset
        new_tour_flight.arrival = tour_flight.arrival + days_offset
        new_tour_flight.save
      end
    end
    new_tour
  end

  def self.search_agent(keyword)
    if keyword.present?
      users_id = User.where("username ILIKE ? or alias_name ILIKE ? ", "%#{keyword}%", "%#{keyword}%").to_a
      joins(:bookings).where("bookings.agent_user_id IN (?) or bookings.sale_rep ILIKE ?", users_id, "%#{keyword}%")
    else
      where('true')
    end
  end

  def self.category_filter(keyword)
    if keyword.present?
      categories = Tour.categories.reject{|k, v| !k.include?(keyword)}
      where(category: categories.keys)
    else
      where('true')
    end
  end

  def self.type_pax(type)
    if type.include?('quad')
      4
    elsif type.include?('triple')
      3
    elsif type.include?('twin') || type.include?('single_twin')
      2
    elsif type == '' || type.include?('single')
      1
    else
      1
    end
  end

  def pay_later?(current_time = Time.now)
    (self.departure_date - 60.days >= current_time)
  end

  def summary(booking_info = false)
    text = ""
    text += "#{self.caption}\n"
    text += "#{self.other_caption}\n"
    text += "Tour Code: #{self.code}\n"
    text += "Departure: #{self.departure_date.try(:strftime, "%d-%b-%Y (%a)")}\n"
    text += "Price: #{self.normal_price}\n"
    text += "Remark: #{self.remark}\n"
    text += "Flight:\n"
    tour_flights.each do |tour_flight|
      text += "#{tour_flight.departure.try(:strftime, "%d/%m/%Y %R %a")} | #{tour_flight.arrival.try(:strftime, "%d/%m/%Y %R %a")} | #{tour_flight.from_to} | #{tour_flight.flight_no} | #{tour_flight.internal_remark}\n"
    end

    if booking_info
      text += "Seats: #{self.all_total_seats}\n"
      if self.is_cruise?
        text += "Cabins: #{self.all_total_rooms}\n"
      end
      bookings_count = bookings.visible.count
      text += "Booked: #{bookings_count}\n"
      if self.is_cruise?
        text += "Booked Cabins: #{self.booking_cruise_rooms.visible.count}\n"
      end
      text += "Vacant: #{self.all_vacant_total_seats}\n"
      if self.is_cruise?
        text += "Vacant Cabins: #{self.all_vacant_total_rooms}\n"
      end
      if self.is_cruise?
        text += "Guaranteed Departure: #{self.guaranteed_departure ? 'Yes' : 'No'}\n"
      else
        text += "Guaranteed Departure Seats: #{self.guaranteed_departure_flag ? self.guaranteed_departure_seats : ''}\n"
      end
    end

    text
  end
    
  def cut_off_start
    self.cut_off_date
  end

  def calendar_start
    self.departure_date
  end

  def departure
    tour_flights.first.departure
  end

  def departure_airline
    if tour_flights.present? && tour_flights.first.flight_no.present?
      tour_flights.first.flight_no[0..1]
    else
      ""
    end
  end

  def arrival
    tour_flights.order(:arrival).last.arrival
  end

  def flights_summary
    if tour_flights.present?
      tour_flights.map{|flight| "#{flight.from_to}  #{flight.flight_no}  #{flight.departure.strftime('%H%M')}-#{flight.arrival.strftime('%H%M')}"}.join("\n")
    else
      ""
    end
  end

  def insurance_available?
    self.code[0..2] != "FIT" && self.tour_flights.present? &&  !self.code.match?(/(#{Tour::EXCLUDING_TOUR_CODE.join('|')})\z/)
  end

  def guaranteed_departure?
    self.guaranteed_departure_flag && self.guaranteed_departure_seats.present? && self.payment_seats >= self.guaranteed_departure_seats
  end

  def insurance_purchased
    if self.bookings.visible.present?
      if self.bookings.visible.all?{|b| b[:insurance_exported] == true}
        "(A)"
      elsif self.bookings.visible.any?{|b| b[:insurance_exported] == true}
        "(P)"
      else
        "(N)"
      end
    else
      "(N)"
    end
  end

  def insurance_rebate
    if self.zone.present? && self.insurance_available?
      days = (self.arrival.to_date - self.departure.to_date).to_i + 1
      rebase = InsuranceRebate.where(zone: zone_number).query_days(days).first
      if rebase.present?
        rebase.price * 0.25
      else
        0
      end
    else
      0
    end
  end

  def zone_number
    self.zone.scan(/\d+/).first.to_i
  end

  def max_available_booking_seats
    [self.max_booking_seats, self.all_vacant_total_seats].min
  end

  def max_seats_clear
    max_seats.select{|key, value| value != nil && value != 0 }
  end

  def sold_seats
    if self.is_cruise?
      booking_cruise_rooms.visible.group(:room_price_type).count.map{|k, v| ["#{k}_seats", v]}.to_h
    else
      bookings.visible.group(:price_type).count.map{|k, v| ["#{k}_seats", v]}.to_h
    end
  end
end

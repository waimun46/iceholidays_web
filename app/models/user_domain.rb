class UserDomain
  attr_reader :user, :new_domain

  def initialize(user, new_domain)
    @user = user
    @new_domain = new_domain
  end
end
class ActivityPrice < ApplicationRecord
  belongs_to :activity

  def dta
    if self.price.present?
      (self.price * Setting.activity_dta_markup.to_f / 100.to_f).round_to_half(2)
    else
      0
    end
  end

  def blockout_dates_text
    if blockout_dates.present?
      blockout_dates.map{|blockout_date| "#{blockout_date.begin.strftime("%d/%m/%Y")}-#{blockout_date.max.strftime("%d/%m/%Y")}"}.join(', ')
    else
      ""
    end
  end

  def blockout_dates_text=(val)
    dates_text = val.split(',')
    all_date = []
    dates_text.each do |date_text|
      date_text_start, date_text_end = date_text.split('-')

      date_start = Date.parse(date_text_start) rescue nil
      date_end = Date.parse(date_text_end) rescue nil

      if date_start.present? && date_end.present?
        all_date << Range.new(date_start, date_end)
      end
    end

    write_attribute(:blockout_dates,all_date)
  end

  def final_price(minus_dta = false)
    price = if self.price.present?
      self.price
    else
      0
    end

    if minus_dta
      price
    else
      price + self.dta
    end
  end

end

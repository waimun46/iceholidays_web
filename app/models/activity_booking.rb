class ActivityBooking < ApplicationRecord
  include ActivityValidator

  belongs_to :activity, optional: true
  belongs_to :activity_booking_group, optional: true
  belongs_to :agent_user, class_name: "User", optional: true
  belongs_to :last_edited_user, class_name: "User", optional: true

  typed_store :price_breakdown, coder: PostgresCoder do |s|
    s.decimal :collect_dta, default: 0
    s.decimal :commission_percentage, default: 0
    s.decimal :commission, default: 0
  end
  
  enum status: [:active, :withdrawn]

  before_save :update_last_edited_user
  after_save :generate_code

  # validates_with BookingCountValidator
  
  def update_last_edited_user
    if self.last_edited_user_id.present? && self.changed?
      self.last_edited_user_id = last_edited_user_id
      self.last_edited_at = Time.now
    end
  end

  def generate_code
    update_column :code, "#{(0...12).map{ ('a'..'z').to_a[rand(26)] }.join.upcase}"
  end

  def self.bulk_create(activity, price_qty, user, travel_date, booking_params = [])

    group = ActivityBookingGroup.new(:travel_date => travel_date, :payment_method => 0)
    
    index = 0
    commission_percentage = user.collect_dta? ? 0 : user.commission_percentage("activity_commission")

    price_qty.each do |category, qty|
      collect_dta = user.collect_dta? ? 0 : activity.dta(category) * qty
      commission = collect_dta * commission_percentage / 100

      if booking_params[index].present?
        group.activity_bookings << ActivityBooking.new(booking_params[index].merge(
          activity_id: activity.id, 
          quantity: qty, 
          category: category, 
          price: activity.final_price(category, user.collect_dta?) * qty, 
          collect_dta: collect_dta,
          commission_percentage: commission_percentage,
          commission: commission,
          agent_user: user))
      else
        group.activity_bookings << ActivityBooking.new(
          activity_id: activity.id, 
          quantity: qty, 
          category: category, 
          price: activity.final_price(category, user.collect_dta?) * qty, 
          collect_dta: collect_dta,
          commission_percentage: commission_percentage,
          commission: commission,
          agent_user: user)
      end
      index += 1
    end

    group.price = group.activity_bookings.map(&:price).inject(:+)

    group.save
    
    group
  end

  def all_quantity
    qty = []
    self.activity.activity_prices.each do |price|
      if self.activity_booking_group.calculate_qty(price.category) > 0
        qty << "#{self.activity_booking_group.calculate_qty(price.category)} #{price.category.include?("Adult") ? "ADT" : price.category.include?("Child") ? "CHD" : price.category}"
        # qty << "#{self.activity_booking_group.calculate_qty(price.category)} #{price.category}"
      end
    end
    # qty.join(', ')
    qty.sort_by do |a|
      a.split
    end.join(', ')
  end
  def total_price
    self.price * self.quantity
  end
end
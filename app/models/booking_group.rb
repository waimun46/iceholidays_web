class BookingGroup < ApplicationRecord
  include ExportEcard
  include ExportChubb

  ADMIN_BOOKINGGROUPS_PER_PAGE = 50
  MonthMap = ("A".."M").to_a
  SORTING = {
    "Code" => "Booking_groups.code",
    "Deposit" => "Booking_groups.deposit",
    "Tour Code" => "tours.code",
    "Cut Off" => "tours.cut_off_date",
    "Departure Date" => "tours.departure_date",
    "Destination" => "tours.contient_country",
    "Sales Person" => "Bookings.sub_sale_rep",
    "Booked At" => "Booking_groups.created_at"
  }

  attr_accessor :edited_user_id

  belongs_to :last_edited_user, class_name: "User", optional: true
  belongs_to :exported_user, class_name: "User", optional: true
  belongs_to :insurance_exporter, class_name: "User", optional: true
  has_one :gift_tracking
  has_one :gift_tracking_group, through: :gift_tracking
  has_many :bookings, -> { order("id") }
  has_many :booking_rooms, through: :bookings
  # has_many :payments
  has_many :payments, as: :taggable
  has_many :etravel_invoices, as: :taggable
  has_many :booking_cruise_rooms, -> { distinct("booking_cruise_room_id") }, through: :bookings
  has_many :infant_bookings, through: :bookings
  has_many :receipts, -> { order(:id) }, dependent: :destroy
  has_many :booking_alterations, class_name: 'BookingAlteration', foreign_key: 'original_booking_group_id'
  accepts_nested_attributes_for :bookings, update_only: true
  accepts_nested_attributes_for :receipts, reject_if: proc { |attributes| attributes['number'].blank? || attributes['amount'].blank?}, allow_destroy: true
  

  scope :search_code, ->(keyword) { joins(:bookings).where("booking_groups.code ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  
  # scope :search_destination, ->(keyword) { includes(bookings: [:tour]).where(bookings: {tours: {contient_country: keyword}}) if keyword.present? }
  scope :search_destination, ->(keyword) { joins(bookings: [:tour]).where("tours.contient_country ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  scope :search_tour_code, ->(keyword) { joins(bookings: [:tour]).where("tours.code ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  scope :search_visible_tour, ->(keyword) { joins(bookings: [:tour]).where("tours.active = ?", keyword == 'Active').distinct if keyword.present? }
  scope :search_name, ->(keyword) { joins(:bookings).where("bookings.name ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  # scope :search_agent, ->(keyword) { includes(bookings: [:agent_user]).where(bookings: {users: {username: keyword, alias_name: keyword}}) if keyword.present? }
  # scope :search_agent, ->(keyword) { includes(bookings: [:agent_user]).where("booking_groups.bookings.name ILIKE ?", "%#{keyword}%") if keyword.present? }
  # scope :search_agent, ->(keyword) { joins(bookings: [:agent_user]).where("users.username ILIKE ? or users.alias_name ILIKE ? or bookings.sale_rep ILIKE ?", "%#{keyword}%", "%#{keyword}%", "%#{keyword}%") if keyword.present?}
  scope :visible, -> { includes(:bookings).where.not(bookings: {id: nil})} # include withdrawn bookings
  scope :with_booking, -> { includes(:bookings).where.not(bookings: {status: Booking.statuses[:withdrawn]}).distinct }
  scope :seach_booking_id, ->(keyword) { includes(:bookings).where("CAST(bookings.id AS TEXT) LIKE ?", "%#{keyword}%").distinct if keyword.present? }
  scope :with_receipt, -> { includes(:receipts).where.not(receipts: {id: nil} ) }
  scope :without_receipt, -> { includes(:receipts).where(receipts: {id: nil} ) }
  scope :overpaid, -> { where("booking_groups.deposit < booking_groups.received_payment") }
  scope :non_overpaid, -> { where("booking_groups.deposit >= booking_groups.received_payment") }
  scope :in_between, ->(inverval) { where(created_at:(inverval))}
  scope :in_between_booked, ->(interval) {joins(:bookings).where("bookings.created_at" => interval).distinct if interval.present?}
  scope :in_between_departure_date, ->(interval) { joins(bookings: [:tour]).where("tours.departure_date" => interval).distinct if interval.present? }
  scope :insurance_exported_at, ->(interval) { where("bookings.insurance_exported_at" => interval).distinct if interval.present? }
  scope :kiv, -> { where('keep_in_view is true') }
  scope :outstanding, -> { where("booking_groups.deposit > booking_groups.received_payment") }
  scope :search_sub_sales, ->(keyword) { joins(:bookings).where("bookings.sub_sale_rep ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  #scope :search_user_origin_state, ->(keyword) { joins(:bookings).where("bookings.sub_sale_rep ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  scope :search_user_origin_state, ->(keyword) { joins(bookings: [:agent_user]).where("users.state ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  scope :search_price_type, ->(keyword) { joins(:bookings).where("bookings.price_type IN (?)", keyword).distinct if keyword.present? }
  scope :search_category, ->(keyword) { joins(:bookings).where("bookings.category IN (?)", keyword).distinct if keyword.present? }
  scope :travelb2b_booking, -> { where(booked_from: 'travelb2b') }
  scope :not_travelb2b_booking, -> { where.not(booked_from: 'travelb2b') }
  before_save :update_last_edited_user
  after_create :generate_code

  typed_store :proforma_invoice_details, coder: PostgresCoder do |i|
    i.string :attention, default: ""
    i.string :consultant, default: ""
    i.integer :tour_id, default: 0
    i.string :tour_code, default: ""
    i.datetime :tour_departure_date, default: 0
    i.integer :bookings_count, default: 0
    i.string :bookings_names, array: true, default: []
    i.integer :bookings_prices, array: true, default: []
    i.integer :bookings_deposits, default: 0
    i.integer :invoice_exported_count, default: 0
    i.datetime :invoice_exported_date, default: ""
  end

  def reference_id
    self.code
  end

  def self.with_insurance_confirmation(boolean)
    if boolean == 'true'
      joins(:bookings).where("bookings.insurance_confirmation = true").where("bookings.status != ?", Booking.statuses[:withdrawn])
    elsif boolean == 'false'
      joins(:bookings).where("bookings.insurance_confirmation = false").where("bookings.status != ?", Booking.statuses[:withdrawn])
    elsif boolean == 'na'
      joins(:bookings).where("bookings.insurance_confirmation is null").where("bookings.status != ?", Booking.statuses[:withdrawn])
    else
      where('')
    end
  end

  def self.with_insurance_exported(boolean)
    if boolean == 'true'
      joins(:bookings).where("bookings.insurance_exported = true").where("bookings.status != ?", Booking.statuses[:withdrawn])
    elsif boolean == 'false'
      joins(:bookings).where("bookings.insurance_exported = false").where("bookings.status != ?", Booking.statuses[:withdrawn])
    else
      where('')
    end
  end

def self.with_sorting_type(sorting_type)
  order_by =  if sorting_type.include?(" ASC")
                'ASC'
              elsif sorting_type.include?(" DESC")
                'DESC'
              else
                ''
              end
  sort_by = sorting_type.sub(/\s(ASC|DESC)\z/, '')

  if order_by.present? && BookingGroup::SORTING.has_key?(sort_by)
    includes(bookings:[:tour]).order("#{BookingGroup::SORTING[sort_by]} #{order_by}")
  else
    where('')
  end
end

  def self.after_crawling
    BookingGroup.all.each do |bg|
      bg.update_deposit
      b = bg.bookings.order("last_edited_at").first
      if b.present?
        bg.update_columns(last_edited_at: b.last_edited_at, last_edited_user_id: b.last_edited_user_id)
      end
    end
    "Done"
  end

  def self.search_agent(keyword)
    if keyword.present?
      users_id = User.where("username ILIKE ? or alias_name ILIKE ? ", "%#{keyword}%", "%#{keyword}%").to_a
      joins(:bookings).where("bookings.initiator_user_id IN (?) or bookings.agent_user_id IN (?) or bookings.sale_rep ILIKE ?", users_id, users_id, "%#{keyword}%")
    else
      where('true')
    end
  end

  def calculate_qty(category)
    bookings.select { |p| p.category == category }.count
  end

  def all_receipt_no
    receipts.map(&:number).join(', ')
  end

  def booked_from_travelb2b?
    self.booked_from == 'travelb2b'
  end

  def due_date
    # Starting from 1 January 2019, the due date changes to 5 business days
    if self.created_at >= "1 November 2019".in_time_zone
      due = 3.business_days.after(self.created_at.to_date)
    elsif self.created_at >= "1 January 2019".in_time_zone
      due = 5.business_days.after(self.created_at.to_date)
    else
      due = 7.business_days.after(self.created_at.to_date)
    end

    cut_off_date = tour.cut_off_date
    if cut_off_date < due
      cut_off_date
    else
      due
    end
  end

  def exported_by_id=(val)
    if val.present?
      write_attribute(:exported_user_id, val)
      write_attribute(:exported_at, Time.now)
      increment(:exported_count)
    end
  end

  def exported?
    if exported_remark.present? || exported_at.present?
      true
    else
      false
    end
  end

  def exported_info
    if exported_remark.present? || exported_at.present?
      if exported_user.present?
        "Exported on #{exported_at.strftime('%d/%m/%Y %H:%M')}, by #{exported_user.username}, ##{exported_count}"
      elsif exported_remark.present?
        exported_remark
      else
        ""
      end
    else
      ""
    end
  end

  def export(user)
    # Pass false if udpate
    # if export_to_etravel(self)

    if export_to_ecard(self)
      self.update_column(:exported_user_id, user.id)
      self.increment!(:exported_count)
      self.touch(:exported_at)
      true
    else
      false
    end
  end

  def generate_code
    if code.blank?
      update_column :code, "#{self.created_at.year.to_s[3]}#{MonthMap[self.created_at.month-1]}#{self.created_at.strftime("%d%H%M%S")}#{(self.id%100).to_s.rjust(2, '0')}"
    end
  end

  def tour
    if bookings.present?
      bookings.first.tour
    else
      nil
    end
  end

  def pay_later?
    tour.present? && tour.pay_later?(self.created_at)
  end

  def receipts_changed?
    self.receipts.any?(&:changed?) || self.receipts.any?(&:new_record?) || self.receipts.any?(&:marked_for_destruction?)
  end

  def agent_user
    if bookings.present?
      bookings.first.agent_user
    else
      nil
    end
  end

  def update_deposit
    if self.tour.present?
      deposit = self.tour.deposit * bookings.visible.count
      update_column :deposit, deposit if self.deposit != deposit
    else
      update_column :deposit, 0 if self.deposit != 0
    end
  end

  def update_kiv
    if bookings.present?
      # total_booking_amount = bookings.visible.map(&:price).inject(:+)
      # if total_booking_amount.blank? || (total_booking_amount.present? && total_booking_amount <= 0) ||(total_payment.present? && total_booking_amount.present? && total_payment >= total_booking_amount)
      #   update_column :keep_in_view, false if self.keep_in_view == true
      # else
      #   update_column :keep_in_view, true if !self.keep_in_view
      # end
      # KIV is false when receive 1 buck
      if deposit.blank? || deposit <= 0 || (self.received_payment.present? && self.received_payment >= 1)
        update_column :keep_in_view, false if self.keep_in_view == true
      else
        update_column :keep_in_view, true if !self.keep_in_view
      end
    else
      update_column :keep_in_view, false if self.keep_in_view == true
    end
  end

  def update_received_payment
    if self.booked_from_travelb2b?
      total_payment = self.payments.paid.map(&:amount).inject(:+).to_i
    else
      total_payment = receipts.map(&:amount).inject(:+).to_i
    end
    update_column :received_payment, total_payment if self.received_payment != total_payment
  end

  def update_last_edited_user
    if self.edited_user_id.present? && (self.changed? || self.receipts_changed?)
      self.last_edited_user_id = edited_user_id
      self.last_edited_at = Time.now
    end
  end

  def confirm_export?
    self.bookings.visible.any?{|b|b.able_to_export?} || self.infant_bookings.any?{|i| i.able_to_export?}
  end

  def export_count
    self.bookings.visible.select{|b|b.insurance_confirmation == true && b.nationality == "malaysian" && b.designation.present? && b.mobile.present? && b.name.present? && b.passport_number.present? && b.date_of_birth.present? && b.insurance_exported == false && b.passport_expiry_date.present? && b.passport_photocopy.present? && b.policy_holder_available? }
  end

  def infant_count
    self.infant_bookings.select{ |i| i.insurance_confirmation == true && i.nationality == "malaysian" && i.name.present? && i.passport_number.present? && i.passport_expiry_date.present? && i.passport_photocopy.present? && i.date_of_birth.present? && i.insurance_exported == false && i.policy_holder_available? }
  end

  def non_export_count
    self.bookings.visible.select{|b| !b.able_to_export? }
  end

  def non_infant_count
    self.infant_bookings.select{ |i| !i.able_to_export? }
  end

  def insurance_exported?
    self.bookings.visible.all?{|b| b[:insurance_exported] == true} && self.infant_bookings.all?{|i| i[:insurance_exported] == true}
  end

  def export_insurance(user, status)
    status_array = []
    if status == "new"
      self.bookings.visible.each do |booking|
        if booking.able_to_export?
          ex_status = booking.export_insurance(user, status)
          status_array.push(ex_status)
        else
          status_array.push(false)
        end

        if self.infant_bookings.present?
          self.infant_bookings.each do |infant|
            if infant.able_to_export?
              ex_status = infant.export_insurance(user, status)
              status_array.push(ex_status)
            else
              status_array.push(false)
            end
          end
        end
      end
    else
      self.bookings.visible.each do |booking|
        if booking.able_to_endorse?
          ex_status = booking.export_insurance(user, status)
          status_array.push(ex_status)
        else
          status_array.push(false)
        end

        if self.infant_bookings.present?
          self.infant_bookings.each do |infant|
            if infant.able_to_endorse?
              ex_status = infant.export_insurance(user, status)
              status_array.push(ex_status)
            else
              status_array.push(false)
            end
          end
        end
      end
    end

    status_array.any?
  end

  def able_to_endorse?
    Time.now < self.tour.departure_date && (self.bookings.any? { |b| b.able_to_endorse? } || self.infant_bookings.any? { |i| i.able_to_endorse? })
  end

  def endorse_booking_count
    self.bookings.visible.select{|b|b.able_to_endorse? }
  end

  def endorse_infant_count
    self.infant_bookings.select{|i|i.able_to_endorse? }
  end

  def able_to_export?
    Rails.env.testb2b? || self.receipts.first.present?
  end

  def all_guardian(booking_id)
    self.bookings.visible.reject { |b| b.id == booking_id || !b.over_18? }.map.with_index { |j, i| ["Guest #{i+1} - #{j.name}", j.id] }.push(["None", "0"])
  end

  def price
    bookings.visible.map(&:price).inject(:+) || 0
  end

  def editable?
    !self.tour.cut_off? && !self.bookings.all?{|b| b.withdrawn?}
    # current_time = Time.now
    # more_than_10days_of_cut_off = (self.tour.cut_off_date - 10.days) > self.created_at
    # ((more_than_10days_of_cut_off && record.created_at + 10.days >= current_time) || (!more_than_10days_of_cut_off && record.created_at + 1.day >= current_time))
  end

  def export_proforma_invoice(user, attention, consultant)
    if self.tour_id != self.tour.id || self.tour_code != self.tour.code || self.tour_departure_date != self.tour.departure_date || self.bookings_count != self.bookings.visible.count ||
      self.bookings_names != self.bookings.visible.map(&:name) || self.bookings_prices != self.bookings.visible.map(&:price) || self.bookings_deposits != self.deposit

      self.attention = attention
      self.consultant = consultant
      self.tour_id = self.tour.id
      self.tour_code = self.tour.code
      self.tour_departure_date = self.tour.departure_date
      self.bookings_count = self.bookings.visible.count
      self.bookings_names = self.bookings.visible.map(&:name)
      self.bookings_prices = self.bookings.visible.map(&:price)
      self.bookings_deposits = self.deposit
      self.invoice_exported_date = Time.now
      self.invoice_exported_count = self.invoice_exported_count + 1

      self.update_columns(proforma_invoice_details: self.proforma_invoice_details)
      # self.update_columns( proforma_invoice_details: { attention: attention, consultant: consultant} )

    end
  end

  def assign_agent?
    self.bookings.visible.all?{|b|!b.agent_user_id.present?}
  end

  def update_agent_user(agent_user)
    if agent_user.present?
      self.bookings.visible.each do |booking|
        booking.update_columns(agent_user_id: agent_user.id)
      end 
    end
  end

  def redirect_url
    Rails.application.routes.url_helpers.thankyou_tour_bookings_path(tour, :booking_group_id => self.id)
  end

  def show_url
    Rails.application.routes.url_helpers.booking_group_path(:id => self.id)
  end

  def agent_user
    if bookings.present?
      bookings.first.agent_user
    else
      nil
    end
  end

  def product_name
    if bookings.first.tour.present?
      bookings.first.tour.caption
    else
      ""
    end
  end

  def product_description
    "Ice Holidays Series Booking"
  end

  def booking_quantity
    if bookings.present?
      bookings.count
    else
      0
    end
  end

  def buyer_email
    if bookings.first.agent_user.present?
      bookings.first.agent_user.email
    else
      ""
    end
  end

  def send_notify
    agent = self.agent_user
    if agent.present?
      if agent.emails.present?
        # Send notify on first time
        if self.payments.count <= 1
          TourMailer.notify(agent, self, agent.emails.join(', ')).deliver_now
        end
        if self.has_payment?
          TourMailer.receipt(agent, self, agent.emails.join(', ')).deliver_now
        end
      end
    end
  end

  def has_payment?
    self.payments.paid.present? && self.payments.paid.map(&:amount).inject(:+) >= 1
  end

  def full_payment?
    self.payments.paid.present? && self.payments.paid.map(&:amount).inject(:+) >= self.price
  end

  def paid_amount
    if self.payments.paid.present?
      self.payments.paid.map(&:amount).inject(:+)
    else
      0
    end
  end

  def balance_amount
    self.price - self.paid_amount
  end

  def fair_booked?
    if self.bookings.present? && Fair.active.allow_user(self.bookings.first.agent_user_id).allow_itinerary(self.tour.itinerary_id).search_date(self.created_at).length > 0
      true
    else
      false
    end
  end

  def has_points?
    self.redemption_points.present?
  end

  def created_in_week?(value)
    self.created_at >= value.week.ago 
  end

  def ref_no
    "BSRS#{self.id.to_s.rjust(9, '0')}"
  end

  def payment_status
    if self.bookings.withdrawn.present?
      "Cancelled"
    elsif self.payments.pending.present?
      "Pending"
    elsif self.full_payment?
      "Paid"
    elsif self.has_payment?
      "Partial Paid"
    elsif self.payments.pay_later.present?
      "Awaiting"
    elsif self.payments.refunded.present?
      "Refunded"
    else
      ""
    end
  end

  def calculate_gift_points
    if self.fair_booked?
      points = 0
      self.bookings.visible.each do |booking|
        if booking.tour.premium? && (booking.tour.department.name == "East Asia" || booking.tour.department.name == "Western And Exotic" || booking.tour.department.name == "Mainland China")
          if booking.price_type == "superpromo" || booking.price_type == "specialdeal"
            if self.price.to_f < 3001
              points += 60
            else
              points += 60
            end
          elsif booking.price_type == "promo"
            if self.price.to_f < 3001
              points += 200
            else
              points += 400
            end
          elsif booking.price_type.include?("normal")
            if self.price.to_f < 3001
              points += 300
            else
               points += 600
            end
          end
        elsif booking.tour.standard? && (booking.tour.department.name == "Asean" || booking.tour.department.name == "East Asia" || booking.tour.department.name == "Western And Exotic" || booking.tour.department.name == "Mainland China")
          if booking.price_type == "superpromo" || booking.price_type == "specialdeal"
            if self.price.to_f < 3001
              points += 30
            else
              points += 30
            end
          elsif booking.price_type == "promo"
            if self.price.to_f < 3001
              points += 100
            else
              points += 200
            end
          elsif booking.price_type.include?("normal")
            if self.price.to_f < 3001
              points += 150
            else
              points += 300
            end
          end
        end
      end
      update_column :redemption_points, points
    end
  end

end
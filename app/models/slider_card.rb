class SliderCard < ApplicationRecord
  mount_uploader :cover, CoverUploader
  mount_uploader :logo1, LogoUploader
  mount_uploader :logo2, LogoUploader
end

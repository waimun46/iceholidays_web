class Gift < ApplicationRecord
  has_many :gift_stocks, dependent: :destroy
  has_many :gift_tracking_details, dependent: :destroy
  accepts_nested_attributes_for :gift_stocks, reject_if: proc { |attributes| attributes['quantity'].blank? || attributes['quantity'].blank?}, allow_destroy: true

  mount_uploader :image, ImageUploader
  validates :threshold, presence: true

  enum status: [:active, :archived]

  scope :visible, -> { where("gifts.status != ?", Gift.statuses[:archived]) }
  scope :available, -> { where("gifts.balance > ? and gifts.status = ? and gifts.points > ?", 0, Gift.statuses[:active], 0) }
  scope :admin_available, -> { where("gifts.balance > ? and gifts.status = ? and gifts.points >= ?", 0, Gift.statuses[:active], 0) }
  scope :in_between_created_at, ->(interval) { joins(:gift_tracking_details).where("gift_tracking_details.created_at" => interval).distinct if interval.present? }

  after_commit :calculate_balance, on: [:create, :update]

  def quantity
    gift_stocks.map(&:quantity).inject(:+).to_i
  end

  def dispatched_out
    gift_tracking_details.map(&:quantity).inject(:+).to_i
  end

  def calculate_balance
    update_columns(balance: quantity - dispatched_out)
  end
end

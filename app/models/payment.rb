class Payment < ApplicationRecord
  require 'openssl'
  require 'base64'

  include FpxPayment
  include ActionView::Helpers::NumberHelper

  belongs_to :user, class_name: "User", optional: true
  belongs_to :taggable, polymorphic: true, optional: true
  has_one :credit_refund
  has_one :etravel_invoice

  enum status: [:pending, :paid, :failed, :refunded]
  # enum gateway: [:public_bank, :credit_transaction, :fpx_b2c]
  enum gateway: [:public_bank, :credit_transaction, :fpx_b2c, :fpx_b2b, :pay_later]
  enum callback: [:no, :required, :done]

  scope :taggable, -> (keyword) { where("payments.taggable_type ILIKE ?", "#{keyword}") }
  scope :with_invoice, -> { includes(:etravel_invoice).where.not(etravel_invoices: {id: nil} ) }
  scope :previous_paid, -> (id) { paid.where("payments.amount > 0").where("payments.id < ?","#{id}") }

  after_create :fillful_net_amount_and_redirect_url, :perform_delay_job
  
  if Rails.env.iceb2b?
    PBB_URL = Rails.application.credentials.pbb[:url]
    SECRET_CODE = Rails.application.credentials.pbb[:secret_code]
    MERCHANT_ID_VISA = Rails.application.credentials.pbb[:merchant_id_visa]
    MERCHANT_ID_MC = Rails.application.credentials.pbb[:merchant_id_mc]
  else
    PBB_URL = "https://uattds2.pbebank.com/PGW/Pay/Detail"
    SECRET_CODE = "APPLE001"
    MERCHANT_ID_VISA = "3300004454"
    MERCHANT_ID_MC = "5500003437"
  end

  PBB_CC_PERCENTAGE = 0.9

  if Rails.env.iceb2b?
    FPX_URL = "#{Rails.application.credentials.fpx[:url]}/seller2DReceiver.jsp"
  else
    FPX_URL = "https://uat.mepsfpx.com.my/FPXMain/seller2DReceiver.jsp"
  end
  FPX_VERSION = 7.0
  if Rails.env.iceb2b?
    FPX_SELLER_EX_ID = "EX00010479"
    FPX_SELLER_ID = "SE00033448"
  else
    FPX_SELLER_EX_ID = "EX00009833"
    FPX_SELLER_ID = "SE00011243"
  end
  FPX_MSG_TOKEN = "01" # B2C Model

  ACCEPT_OBJECT = {
    "ActivityBookingGroup" => "ACT",
    "FlightBookingGroup" => "FLT",
    # "HotelBookingGroup" => "HTL",
    "TBOBooking" => "HTL",
    "RoamingmanBookingGroup" => "RMM",
    "GlobaltixTicketBookingGroup" => "GTX",
    "BookingGroup" => "SRS",
    # "Cruise" => "CRU",
    "LandTourBookingGroup" => "LDT",
  }

  CLASS_GROUP_CATEGORY = {
    "BookingGroup" => "Series",
    "ActivityBookingGroup" => "Activity",
    "GlobaltixTicketBookingGroup" => "Activity",
    "RoamingmanBookingGroup" => "Wifi",
    "FlightBookingGroup" => "Flight"
  }

  FPX_STAGING_BANKLIST = {
    "TEST0021" => "SBI Bank A",
    "TEST0022" => "SBI Bank B",
    "TEST0023" => "SBI Bank C",
    "TEST0001" => "Test Bank A",
    "TEST0002" => "Test Bank B",
    "TEST0003" => "Test Bank C",
    "TEST0004" => "Test Bank D",
    "TEST0005" => "Test Bank E",
    "ABB0233" => "Affin Bank",
    "ABB0234" => "Affin B2C - Test ID",
    "ABMB0212" => "Alliance Bank (Personal)",
    "AMBB0209" => "AmBank",
    "BIMB0340" => "Bank Islam",
    "BKRM0602" => "Bank Rakyat",
    "BMMB0341" => "Bank Muamalat",
    "BSN0601" => "BSN",
    "BCBB0235" => "CIMB Clicks",
    "HLB0224" => "Hong Leong Bank",
    "HSBC0223" => "HSBC Bank",
    "KFH0346" => "KFH",
    "MB2U0227" => "Maybank2U",
    "MBB0228" => "Maybank2E",
    "OCBC0229" => "OCBC Bank",
    "PBB0233" => "Public Bank",
    "RHB0218" => "RHB Bank",
    "SCB0216" => "Standard Chartered",
    "UOB0226" => "UOB Bank",
    "UOB0229" => "UOB Bank - Test ID"
  }

  FPX_PROD_BANKLIST = {
    "ABB0233" => "Affin Bank",
    "ABMB0212" => "Alliance Bank (Personal)",
    "AMBB0209" => "AmBank",
    "BIMB0340" => "Bank Islam",
    "BMMB0341" => "Bank Muamalat",
    "BKRM0602" => "Bank Rakyat",
    "BSN0601" => "BSN",
    "BCBB0235" => "CIMB Clicks",
    "CIT0219" => "Citibank",
    "HLB0224" => "Hong Leong Bank",
    "HSBC0223" => "HSBC Bank",
    "KFH0346" => "KFH",
    "MBB0228" => "Maybank2E",
    "MB2U0227" => "Maybank2U",
    "OCBC0229" => "OCBC Bank",
    "PBB0233" => "Public Bank",
    "RHB0218" => "RHB Bank",
    "SCB0216" => "Standard Chartered",
    "UOB0226" => "UOB Bank"
  }

  BANK_STATUS = {
    "A" => "Online",
    "B" => "Offline",
  }

  PBB_METHOD = [
    ['Visa', 'visa'], 
    ['Mastercard', 'mastercard']
  ]

  # validates :method, presence: true, if: :fpx_b2c?

  def self.all_gateways
    gateways = {}
    Payment.gateways.each do |g|
      if g[0].include?("fpx")
        gateways[g[0]] = g[0].humanize.upcase
      else
        gateways[g[0]] = g[0].humanize
      end
    end
    gateways
  end

  def order_id
    if taggable_type == "RoamingmanBookingGroup"
      taggable.roamingman_bookings.first.oms_order_id
    elsif taggable_type == "GlobaltixTicketBookingGroup"
      taggable.globaltix_ticket_bookings.first.globaltix_ticket_booking_group.reference_number 
    elsif taggable_type == "ActivityBookingGroup"
      taggable.activity_bookings.first.code
    elsif taggable_type == "BookingGroup"
      taggable.code
    elsif taggable_type == "LandTourBookingGroup"
      taggable.code
    elsif taggable_type == "FlightBookingGroup"
      taggable.host_code
    elsif taggable_type == "TBOBooking"
      taggable.confirmation_no   
    else
      ""
    end
  end

  def convert_fpx_amount
    if self.amount.present?
      number_with_precision(self.amount, precision: 2)
    else
      number_with_precision(0, precision: 2)
    end
  end

  def convert_pbb_amount
    if self.amount.present?
      "#{(self.amount*100).to_i}".rjust(12, "0")
    else
      "000000000000"
    end
  end

  def pbb_merchant_id(method)
    if method == "visa"
      Payment::MERCHANT_ID_VISA
    else
      Payment::MERCHANT_ID_MC
    end
  end

  def generate_invoice_no
    if Payment::ACCEPT_OBJECT.has_key?(self.taggable.class.name)
      # invoice = "#{Payment::ACCEPT_OBJECT[self.taggable.class.name]}#{self.taggable.id}#{Time.now.in_time_zone.strftime("%Y%m%d")}"
      # sub_str = 20 - self.taggable.id.to_s.size
      # new_invoice = invoice.ljust(sub_str, "0")
      # "#{new_invoice}#{self.taggable.id}"

      # Change to use payment id
      invoice = "#{Payment::ACCEPT_OBJECT[self.taggable.class.name]}#{self.created_at.strftime("%Y%m%d")}"
      sub_str = 20 - self.id.to_s.size
      new_invoice = invoice.ljust(sub_str, "0")
      if self.public_bank?
        self.update_columns(transaction_no: "#{new_invoice}#{self.id}")
      end
      "#{new_invoice}#{self.id}"
    else
      nil
    end
  end

  def self.find_by_invoice(invoice_no)
    if Payment::ACCEPT_OBJECT.key(invoice_no.first(3)).present?
      Payment.find_by_id(invoice_no[11..-1])
    else
      nil
    end
  end

  def encode_pbb_key(invoice_no, amount, secret_code, merchant_id)
    Digest::SHA1.base64digest("#{invoice_no}#{amount}#{secret_code}#{merchant_id}")
  end

  def etravel_payment_type
    if self.gateway.include?("fpx")
      "FPX"
    elsif self.public_bank?
      "CreditCard"
    else
      "Others"
    end
  end

  def self.find_taggable(object, id)
    if Payment::ACCEPT_OBJECT.has_key?(object)
      "#{object}".constantize.find(id)
    else
      nil
    end
  end

  # def self.get_taggable(invoice_no)
  #   object = Payment::ACCEPT_OBJECT.key(invoice_no.first(3))
  #   id = invoice_no[/#{invoice_no.first(3)}(.*?)#{Time.now.in_time_zone.strftime("%Y%m%d")}/m, 1]

  #   "#{object}".constantize.find(id)
  # end

  def self.pbb_verify(data, security_key_res)
    source_string = data.values.join('')
    key = Digest::SHA1.base64digest(source_string)
    if key == security_key_res
      true
    else
      false
    end
  end

  def pay_later_available?
    if self.pay_later?
      if taggable.present? && taggable_type == "BookingGroup" && amount == 0 && taggable.payments.present? && taggable.payments.count == 1 && taggable.pay_later?
        true
      else
        false
      end
    else
      true
    end
  end

  def get_amount
    if status == "paid"
      amount
    else
      "0"
    end
  end

  def get_net_amount
    if status == "paid"
      net_amount
    else
      "0"
    end
  end

  def get_method
    if self.gateway.include?("fpx")
      if Rails.env.iceb2b? && self.method.present?
        Payment::FPX_PROD_BANKLIST[self.method]
      elsif !Rails.env.iceb2b? && self.method.present?
        Payment::FPX_STAGING_BANKLIST[self.method]
      else
        ""
      end
    elsif self.public_bank? && self.method.present?
      self.method.capitalize
    else
      ""
    end
  end

  def gateway_text
    if self.gateway.include?("fpx")
      "#{self.gateway.humanize.upcase} (#{self.get_method})"
    elsif self.public_bank?
      "Credit Card (#{self.method})"
    elsif self.pay_later?
      "Book now, pay later"  
    else
      "#{self.gateway.humanize}"
    end
  end

  def get_category_name
    if(Payment::CLASS_GROUP_CATEGORY.has_key?(self.taggable.class.name))
      "#{Payment::CLASS_GROUP_CATEGORY[self.taggable.class.name]}"
    else
      "-"
    end
  end

  def self.fpx_banklist_checksum(msg_token, msg_type, seller_ex_id, version)
    source_string = "#{msg_token}|#{msg_type}|#{seller_ex_id}|#{version}"
    if Rails.env.iceb2b?
      pkey = OpenSSL::PKey::read(Rails.application.credentials.fpx[:key])
    else
      pkey = OpenSSL::PKey::read(File.read("config/fpx/#{Payment::FPX_SELLER_EX_ID}.key"))
    end
    pkey.sign(OpenSSL::Digest::SHA1.new, source_string).unpack("H*").first.upcase     
  end

  def self.fpx_generate_checksum(data)
    source_string = data.values.join('|')
    if Rails.env.iceb2b?
      pkey = OpenSSL::PKey::read(Rails.application.credentials.fpx[:key])
    else
      pkey = OpenSSL::PKey::read(File.read("config/fpx/#{Payment::FPX_SELLER_EX_ID}.key"))
    end
    pkey.sign(OpenSSL::Digest::SHA1.new, source_string).unpack("H*").first.upcase  
  end

  def self.fpx_verify_checksum(checksum, data)
    source_string = data.values.join('|')
    signature = [checksum].pack('H*')
    if Rails.env.iceb2b?
      cert = OpenSSL::X509::Certificate.new(Rails.application.credentials.fpx[:cer])
    else
      cert = OpenSSL::X509::Certificate.new(File::read("config/fpx/#{Payment::FPX_SELLER_EX_ID}.cer"))
    end
    public_key = cert.public_key
    public_key.verify(OpenSSL::Digest::SHA1.new, signature, source_string)
  end

  def fpx_banklist
    banklist = get_fpx_banklist(self)
  end

  def fpx_status
    status = get_fpx_status(self)
  end

  def generate_banklist(banklist)
    list = {}
    banklist.each do |bank_id, status|
      if Rails.env.iceb2b? && Payment::FPX_PROD_BANKLIST[bank_id].present?
        bank_name = "#{Payment::FPX_PROD_BANKLIST[bank_id]}"
      elsif !Rails.env.iceb2b? && Payment::FPX_STAGING_BANKLIST[bank_id].present?
        bank_name = "#{Payment::FPX_STAGING_BANKLIST[bank_id]}"
      else
        bank_name = "#{bank_id}"
      end
      
      if Payment::BANK_STATUS[status] == "Offline"
        bank_name += " (#{Payment::BANK_STATUS[status]})"
      end
      list[bank_name] = bank_id
    end
    list.sort_by{ |e, h| e.downcase }
  end

  def offline_banklist(banklist)
    list = []
    banklist.each do |key, value|
      if key.include?("Offline")
        list << value
      end 
    end
    list.sort_by{ |e, h| e.downcase }
  end


  def fpx_update_status_and_action!(response, transaction_no)
    self.update_columns(transaction_no: transaction_no)
    if self.pending?
      if response == "00"
        self.paid_and_action!
      elsif response == "09"
        self.pending_and_action!
      else
        self.failed_and_action!
      end
    end
  end

  def paid_and_action!
    self.paid!

    if self.taggable_type == "FlightBookingGroup"
      if self.taggable.brb_bookings.present?
        self.taggable.brb_bookings.first.brb_booking_group.export_brb_create
      end
    elsif self.taggable_type == "GlobaltixTicketBookingGroup"
      if self.taggable.able_to_export?
        self.taggable.export_globaltix
      end
    elsif self.taggable_type == "RoamingmanBookingGroup"
      if self.taggable.able_to_export?
        self.taggable.export_roamingman_create
      end
    elsif self.taggable_type == "TBOBooking"
      self.taggable.generate_invoice
    elsif self.taggable_type == "BookingGroup"
      self.taggable.update_received_payment
      self.taggable.tour.calculate_payment_seats
    elsif self.taggable_type == "CreditPurchase"
      if self.taggable.pending?
        self.taggable.purchase_credits
      end
    # elsif self.taggable_type == "ActivityBookingGroup"
    elsif self.taggable_type == "LandTourBookingGroup"
      self.taggable.update_received_payment
    end

    if self.taggable.present?
      self.taggable.send_notify
      if self.amount.present? && self.amount > 0
        Etravel::BookExport.create(self.taggable, self)
      end
    end
  end

  def failed_and_action
    # if self.taggable_type == "FlightBookingGroup"
    # elsif self.taggable_type == "GlobaltixTicketBookingGroup"
    # elsif self.taggable_type == "RoamingmanBookingGroup"
    if self.taggable_type == "BookingGroup"
      if !self.taggable.payments.paid.present?
        BookingAlteration.bulk_withdraw(self.taggable.bookings, {alter_type: :withdrawn, reason: "Auto-cancellation: traveb2b payment failed"}, nil)
      end
    elsif self.taggable_type == "ActivityBookingGroup"
      self.taggable.activity_bookings.map(&:withdrawn!)
    elsif self.taggable_type == "LandTourBookingGroup"
      self.taggable.land_tour_bookings.map(&:cancelled!)
    end

  end

  def failed_and_action!(action_remark = "")
    self.failed_and_action
    if action_remark.present?
      self.action_remark = action_remark
    end
    self.failed!
  end

  def pending_and_action!
    self.pending!
  end

  def refunded_and_action!
    if self.credit_refund.id.present?
      self.credit_refund.refund!
    end
  end

  def amount=(val)
    if val.present?
      write_attribute(:amount, val.to_f.round(2))
    else
      write_attribute(:amount, 0)
    end
  end

  def previous_paid_amount
    self.taggable.payments.previous_paid(self.id).map(&:amount).inject(:+).to_f
  end

  def fillful_net_amount_and_redirect_url
    if self.amount.present?
      amount = self.amount
    else
      amount = 0
    end

    if self.fpx_b2c?
      net_amount = amount - 0.5
      redirect_url = Rails.application.routes.url_helpers.fpx_direct_path
    elsif self.public_bank?
      net_amount = amount - (amount * (Payment::PBB_CC_PERCENTAGE) / 100).to_f.round(2)
      redirect_url = Rails.application.routes.url_helpers.pbb_direct_path
    else
      net_amount = amount
      redirect_url = nil
    end

    self.update_columns(net_amount: net_amount.round(2), redirect_url: redirect_url)
  end

  def perform_delay_job
    # set failed if it is pending after 15 minutes
    if !self.credit_transaction?
      PaymentCheckingJob.set(wait: 15.minutes).perform_later(self)
    end
  end

end
class Department < ApplicationRecord
  
mount_uploader :fs_logo, LogoUploader
mount_uploader :logo, LogoUploader
has_many :itineraries

NAME = [
  "Western And Exotic",
  "Premium", 
  "Mainland China", 
  "Asean", 
  "East Asia", 
  "G-Trip",  
  "Cruise"]

  scope :search, ->(keyword) { where("departments.name ILIKE ? or departments.email ILIKE ? or departments.phone ILIKE ?", "%#{keyword}%", "%#{keyword}%", "%#{keyword}%") }
  # scope :with_tours, -> (interval) { includes(:tours).references(:tours).where("tours.active is true and tours.vacant_seats_available = true").where(tours: {departure_date: interval}).where("cut_off_date > ?", Time.now).distinct if interval.present? }

  def etravel_dept_code
    if name == "Western And Exotic"
      "WESTERN"
    elsif name == "Premium"
      "PREMIUM"
    elsif name == "Mainland China"
      "CHINA"
    elsif name == "Asean"
      "ASEAN"
    elsif name == "East Asia"
      "ASIA"
    elsif name == "G-Trip" 
      "GA"
    elsif name == "Cruise"
      "CRUISE"
    else
      ""
    end
  end

end
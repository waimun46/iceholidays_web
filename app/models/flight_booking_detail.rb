class FlightBookingDetail < ApplicationRecord
  belongs_to :flight_booking_group

  def airline_name
    if self.carrier.present?
      "#{Airline.find_by_iata_code(self.carrier).name} (#{self.carrier})"
    end
  end

  def flight_code
    if self.carrier.present? && self.flight_number.present?
      "#{self.carrier}#{self.flight_number}"
    end
  end
end

class Slider < ApplicationRecord
  has_many :slider_tabs, -> { order(:priority) }

  accepts_nested_attributes_for :slider_tabs, reject_if: proc { |attributes| attributes['title'].blank? }, allow_destroy: true

  enum category: [:banners, :tab_cards, :tab_sliders, :sliders]
  enum status: [:pending, :active]

  scope :order_by_priority, -> { order(priority: :asc) }
  scope :active, -> { where("sliders.status = ?", Slider.statuses[:active]) }

  ADMIN_SLIDER_PER_PAGE = 50

end

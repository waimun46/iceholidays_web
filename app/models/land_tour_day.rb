class LandTourDay < ApplicationRecord
  belongs_to :land_tour, inverse_of: :land_tour_days
  has_many :land_tour_activities
  has_many :land_tour_cities
  accepts_nested_attributes_for :land_tour_cities, allow_destroy: true
  accepts_nested_attributes_for :land_tour_activities, allow_destroy: true

  typed_store :inclusion, coder: PostgresCoder do |s|
    s.boolean :inc_breakfast, default: false
    s.boolean :inc_lunch, default: false
    s.boolean :inc_dinner, default: false
  end

  validates :day_no, :title, presence: true

  def inclusion_text
    self.inclusion.map{|k,v| v == true ? k : '' }.reject(&:blank?).map{|i|i.sub("inc_", "").capitalize}.join(", ")
  end
end
class GiftTrackingGroup < ApplicationRecord
  has_many :gift_trackings, dependent: :destroy
  has_many :gift_tracking_details, through: :gift_trackings, dependent: :destroy
  belongs_to :agent, class_name: "User", optional: true
  belongs_to :booking_group, optional: true

  enum category: [:no_category, :compliment, :sponsorship, :staff_use, :loan_item]

  enum status: [:preparing, :delivering, :received, :rejected, :submitted]

  typed_store :status_detail, coder: PostgresCoder do |s|
    s.integer :total_boxes, default: 0
    s.integer :received_boxes, default: 0
    s.string :reason
    s.string :status_history, array: true, default: []
    s.string :status_changed_at, array: true, default: []
  end

  accepts_nested_attributes_for :gift_trackings, allow_destroy: true

  scope :in_between_tracking, ->(interval) { where(gift_tracking_groups: {created_at:(interval)}) }

  scope :search_agent, ->(agent_id) { includes(gift_trackings: [:booking]).where(gift_trackings:{bookings:{initiator_user_id: agent_id}}).or(GiftTrackingGroup.includes(gift_trackings: [:booking]).where(gift_trackings:{bookings:{agent_user_id: agent_id}})).or(GiftTrackingGroup.includes(gift_trackings: [:booking]).where(gift_trackings:{bookings:{sale_rep: User.find(agent_id).username }})).or(GiftTrackingGroup.includes(gift_trackings: [:booking]).where(gift_trackings:{gift_tracking_groups:{agent_id: agent_id}})) if agent_id.present? }

  validates_associated :gift_trackings, if: :no_category?

  def overdue?
    if self.cut_off_date.present?
      if self.cut_off_date <= Date.today
        true
      else
        false
      end
    end
  end

  def dispatched_out
    gift_tracking_details.map(&:quantity).inject(:+).to_i
  end

  def deliverable?
    if self.status == "preparing" || self.status == "rejected"
      true
    else
      false
    end
  end

  def submittable?
    self.status == "preparing"
  end

  ['delivering', 'received', 'rejected'].each do |status|
    define_method "#{status}!" do
      self.status = status
      if self.status_changed?
        self.status_history.push(status_was)
        self.status_changed_at.push(Time.now)
        self.save
      end
    end
  end

end

class CreditRefund < ApplicationRecord
  belongs_to :user
  belongs_to :payment

  enum status: [:pending, :refunded, :archived]

  def refund!
    self.user.topup_credits(self.credits, self.payment, "Refund Credits #{self.credits}: #{self.remark}")
    self.refunded!
  end

  def archive!
    self.archived!
  end

end
class BrbBooking < ApplicationRecord
  PRICE = 26

  belongs_to :brb_booking_group
  belongs_to :agent_user, class_name: "User", optional: true
  belongs_to :initiator_user, class_name: "User", optional: true
  belongs_to :flight_booking

  enum status: [:active, :withdrawn]

end

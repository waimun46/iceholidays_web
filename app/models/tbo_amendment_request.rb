class TBOAmendmentRequest < ApplicationRecord 
  enum status: %w( Unprocessed RequestSent InProgress AgentApprovalPending PendingWithSupplier Amended Withdrawn Rejected Cancelled )
  belongs_to :booking, class_name: 'TBOBooking', foreign_key: :tbo_booking_id


  def withdrawable?
    !status.in?( %w( Amended Withdrawn Rejected Cancelled ) )
  end
end 
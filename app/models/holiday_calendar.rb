class HolidayCalendar < ApplicationRecord

  ADMIN_HOLIDAY_CALENDAR_PER_PAGE = 50

  CALENDAR_NAME =[
  'School Holidays',
  'Happy New Year',
  'Chinese New Year Day',
  'Thaipusam Day',
  'Labour Day',
  'Wesak Day',
  'Nurul Ai Quran',
  'Hari Raya',
  'Yang Di-Pertuan Agong',
  'Hari Raya Haji', 
  'Awal Muharram ',
  'National Day',  
  'Malaysia Day',  
  'Prophet Muhammad',
  'Deepavali',
  'Christmas'   
  ]

  validates :date, presence: true
  validates :name, presence: true

  def date_start
    if date.present?
      date.begin.in_time_zone
    else
      nil
    end
  end

  def date_end
    if date.present?
      date.end.in_time_zone-1.days
    else
      nil
    end
  end

  def holiday_status
    @current_time ||= Time.now
    if self.date.present?
      if self.date.include?(@current_time)
        "Ongoing"
      elsif self.date.begin > @current_time
        "Upcoming"
      elsif self.date.end < @current_time
        "Passed"
      else
        ""
      end
    else
      ""
    end
  end
end


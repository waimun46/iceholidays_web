class BrbBookingGroup < ApplicationRecord
  has_many :brb_bookings

  def self.create_booking_by_flight_bookings(flight_bookings_list, user, agent_rep = nil)

    if agent_rep == nil
      agent_user = user
    else
      agent_user = User.find_by_username(agent_rep)
    end

    booking_group = BrbBookingGroup.new(
      service_number: "",
      price: flight_bookings_list.count * BrbBooking::PRICE,
      product_code: "GOLD",
      is_international: true,
      status_code: ""
    )

    flight_bookings_list.each_with_index do |booking, index|
      booking_group.brb_bookings.new(
        first_name: booking.last_name,
        last_name: booking.first_name,
        flight_booking: booking,
        agent_user: agent_user,
        sale_rep: agent_rep,
        initiator_user: user
      )
    end

    booking_group.save
    booking_group
  end

  def export_brb_create

    if self.service_number == "" && self.status_code == ""
      params = {
        "ProductCode" => "GOLD",
        "IsInternational" => true,
        "PassengerList" => []
      }

      self.brb_bookings.each_with_index do |brb_booking, index|
        params["PassengerList"] << {
          "OrderSequence" => index+1,
          "LastName" => brb_booking.flight_booking.last_name,
          "FirstName" => brb_booking.flight_booking.first_name,
          "Email" => "jasonlam@ice-holidays.com",
          "AirlineId" => Brb::DataService.get_airline_id(brb_booking.flight_booking.flight_booking_group.flight_booking_details.first.carrier).to_i,
          "AirlineConfirmationNumber" => brb_booking.flight_booking.flight_booking_group.host_code
        }
      end

      data, status_code = Brb::PaymentService.purchase(params)
      if data.present?
        self.service_number = data["ServiceNumber"]
        self.status_code = status_code
        self.save
      end
    end
  end

end

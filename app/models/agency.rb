class Agency < ApplicationRecord
  DOMAINS = [
    'iceb2b',
    'travelb2b'
  ]

  validates :name, :code, presence: true
  validates :name, format: { with: /\A[A-Za-z0-9_\-]+\z/, message: "only allows letters" }
  validates :name, :code, uniqueness: { case_sensitive: false }

  # TODO: validates max subagent
  validates :domains, array: { inclusion: { in: Agency::DOMAINS } }

  has_many :users

end
class SliderTab < ApplicationRecord
  has_many :slider_cards, -> { order(:priority) }
  accepts_nested_attributes_for :slider_cards, reject_if: proc { |attributes| attributes['priority'].blank? }, allow_destroy: true
end

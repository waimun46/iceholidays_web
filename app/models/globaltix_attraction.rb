class GlobaltixAttraction < ApplicationRecord
  enum markup_category: [:percentage, :fixed]
  enum status: [:pending, :active, :archived, :n_a]

  enum cancellation: [:free_cancellation, :non_cancellation]
  enum e_ticketing: [:e_ticket, :printed_ticket, :printed_or_e_ticket]
  enum travel_date_type: [:no_specific_date, :open_date, :fixed_date]
  enum ticket_type: [:no_specific_ticket_type, :enter_directly, :collect_physical, :collect_physical_ticket_at_dedicated_point]
  enum transfer: [:private_transfer, :sit_in_coach, :no_transfer]
  enum meeting_point: [:meet_up_with_guide, :hotel_pick_up, :meet_up_at_location, :no_specific_meeting_point]
  enum confirmation_time: [:instant_confirmation, :_24_hours_confirmation, :_48_hours_confirmation, :_72_hours_confirmation]
  enum category: [:sightseeing, :attraction]

  validates :attraction_id, uniqueness: true

  scope :countries, -> { distinct.pluck(:country) }
  scope :active_tickets, -> { includes(:globaltix_tickets).where(globaltix_tickets: { status: :active }) }
  scope :search_keyword, ->(keyword) { where("globaltix_attractions.country ILIKE :keyword OR globaltix_attractions.title ILIKE :keyword", keyword: "%#{keyword}%")  if keyword.present? }
  scope :search_category, ->(category) { where("category = ?", GlobaltixAttraction.categories[category]) if category.present? }
  scope :search_country, ->(keyword) { where("country ILIKE ?", keyword) if keyword.present? }
  scope :search_title, ->(keyword) { where("title ILIKE ?", "%#{keyword}%") if keyword.present? }

  has_many :covers, as: :taggable
  has_many :globaltix_tickets, foreign_key: 'globaltix_attraction_id'

  accepts_nested_attributes_for :covers, reject_if: proc { |attributes| attributes['image'].blank? }, allow_destroy: true
  accepts_nested_attributes_for :globaltix_tickets

  LANGUAGES = [
    'English',
    'Chinese',
    'Malay'
  ]

  SGD_CONVERTED_TO_MYR = 3.0

  def min_price
    globaltix_tickets.active.map(&:final_price).min
  end

  ['languages'].each do |f|
    define_method "join_#{f}" do
      if self.send(f).present?
        self.send(f).join(', ')
      else
        ''
      end
    end
  end

end
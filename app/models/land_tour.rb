class LandTour < ApplicationRecord
  include LandTourValidator

  has_many :covers, as: :taggable
  has_many :land_tour_categories
  has_many :land_tour_prices, through: :land_tour_categories
  has_many :land_tour_days
  has_many :land_tour_cities, through: :land_tour_days
  has_many :land_tour_activities, through: :land_tour_days
  has_many :land_tour_bookings, -> { order("id") }
  has_many :land_tour_booking_groups, through: :land_tour_bookings
  accepts_nested_attributes_for :covers, allow_destroy: true
  accepts_nested_attributes_for :land_tour_days, allow_destroy: true
  accepts_nested_attributes_for :land_tour_categories, allow_destroy: true
  
  enum category: [:private_tour, :seat_in_coach]
  enum status: [:pending, :active, :archived]
  enum deposit_type: [:fixed, :percentage]

  GUIDE_LANGUAGES = [
    'Malay',
    'English',
    'Chinese'
  ]

  CHARGE_TYPE = [
    'Pax',
    'Group'
  ]

  DTA_TYPE = [
    'Fixed',
    'Percentage'
  ]

  typed_store :child_prices, coder: PostgresCoder do |c|
    c.integer :child_with_bed, default: 0
    c.integer :child_without_bed, default: 0
    c.integer :child_twin, default: 0
    c.integer :infant, default: 0
  end

  typed_store :dta_prices, coder: PostgresCoder do |d|
    d.string :dta_type, default: ''
    d.integer :dta_adult, default: 0
    d.integer :dta_child, default: 0
  end

  typed_store :inclusion, coder: PostgresCoder do |s|
    s.boolean :inc_hotel, default: false
    s.boolean :inc_flight, default: false
    s.boolean :inc_half_board_meals, default: false
    s.boolean :inc_full_board_meals, default: false
    s.boolean :inc_entrance_ticket, default: false
    s.boolean :inc_transportation, default: false
    s.boolean :inc_shopping_stop, default: false
  end

  typed_store :supplier_details, coder: PostgresCoder do |s|
    s.string :supplier_tour_code, default: ""
    s.string :supplier_name, default: ""
    s.string :supplier_address, default: ""
    s.string :supplier_pic_name, default: ""
    s.string :supplier_pic_phone_no, default: ""
    s.string :supplier_email, array: true, default: {}
  end

  validates :code, presence: true
  validates :code, uniqueness: { case_sensitive: false }
  validates :code, length: { maximum: 25 }
  validates_associated :land_tour_days, if: :active?
  validates_associated :land_tour_categories, if: :active?

  scope :visible, -> { where("land_tours.status = ?", LandTour.statuses[:active]) }
  scope :with_visible, -> (only=true) { where("land_tours.status = ?", LandTour.statuses[:active]) if only }
  scope :search_keyword, ->(keyword) { where("land_tours.code ILIKE ? or land_tours.title ILIKE ?", "%#{keyword}%", "%#{keyword}%") }
  scope :search_travel_date, ->(interval_start, interval_end) { where("travel_date && tsrange(?, ?)", interval_start, interval_end) if interval_start.present? && interval_end.present? }

  after_commit :set_travel_date, on: [:create, :update]
  
  def set_travel_date
    travel_date = self.land_tour_prices.map(&:dates).flatten.map{ |d| d.to_a }.flatten

    if travel_date.present?
      update_columns(travel_date: travel_date.min.in_time_zone..travel_date.max.in_time_zone)
    else
      true
    end
  end

  def bookable?
    dates = []
    self.blockout_dates.each do |date|
      date_str = date.split('..').map{|i| i.to_date }
      date_range = date_str[0]..date_str[1]
      dates << date_range.to_a
    end
    !dates.include?(Date.today) # return true if today is excluded from blockout dates
  end

  ['guide_languages', 'depart_day', 'supplier_email'].each do |f|
    define_method "join_#{f}" do
      if self.send(f).present?
        self.send(f).join(', ')
      else
        ''
      end
    end
  end

  def blockout_dates_text
    if blockout_dates.present?
      blockout_dates.map{|blockout_date| "#{blockout_date.begin.strftime("%d/%m/%Y")}-#{blockout_date.end.strftime("%d/%m/%Y")}"}.join(', ')
    else
      ""
    end
  end

  def blockout_dates_text=(val)
    dates_text = val.split(',')
    all_date = []
    dates_text.each do |date_text|
      date_text_start, date_text_end = date_text.split('-')

      date_start = Date.parse(date_text_start) rescue nil
      date_end = Date.parse(date_text_end) rescue nil

      if date_start.present? && date_end.present?
        all_date << Range.new(date_start, date_end)
      end
    end

    write_attribute(:blockout_dates, all_date)
  end

  def travel_date_start
    if travel_date.present?
      travel_date.begin.in_time_zone
    else
      nil
    end
  end

  def travel_date_end
    if travel_date.present?
      travel_date.end.in_time_zone
    else
      nil
    end
  end

  def min_price
    self.land_tour_prices.map(&:prices).map{|pr| pr.values}.flatten.map{|p| p.to_i}.min
  end

  def default_total_price
    self.compulsory_charges.map{|c| c["price"].to_i }.sum
  end

  def country
    land_tour_cities.map(&:city).reject(&:blank?).map(&:country).uniq.join(', ')
  end

  def quantity_text(qty)
    qty.gsub("..", "-")
  end

  def inclusion_text
    self.inclusion.map{|k,v| self.respond_to?(k.to_sym) ? v == true ? k : '' : '' }.reject(&:blank?).map{|i|i.sub("inc_", "").humanize.capitalize}.join(", ")
  end

  def pax_charges_text
    self.compulsory_charges.select{|k,v| k["type"] == "Pax"}.map {|k,v| [k["title"]] }.join(", ")
  end

  def group_charges_text
    self.compulsory_charges.select{|k,v| k["type"] == "Group"}.map {|k,v| [k["title"]] }.join(", ")
  end
end

class City < ApplicationRecord
  has_many :land_tour_cities

  scope :search_keyword, ->(keyword) { where("cities.country ILIKE ? or cities.name ILIKE ?", "%#{keyword}%", "%#{keyword}%") if keyword.present? }

  def self.all_city
    cities = []
    City.all.each do |c|
      city = {}
      city["id"] = c.id
      city["name"] = c.name
      city["country"] = c.country

      cities << city
    end
    cities
  end
end

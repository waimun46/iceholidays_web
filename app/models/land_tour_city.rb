class LandTourCity < ApplicationRecord
  belongs_to :land_tour_day, inverse_of: :land_tour_cities
  belongs_to :city, optional: true

  validates :city_id, presence: true

  scope :search_city, ->(city_ids) { where("land_tour_cities.city_id IN (?)", city_ids) if city_ids.present? }

  def city_name
    if self.city.present?
      "#{self.city.name}, #{self.city.country}"
    else
      ""
    end
  end
end
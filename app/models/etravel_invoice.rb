class EtravelInvoice < ApplicationRecord
  belongs_to :taggable, polymorphic: true
  belongs_to :payment, optional: true


  scope :search, ->(keyword) { where("etravel_invoices.taggable_type  IN (?)", keyword).distinct if keyword.present? }
  
  ADMIN_PER_PAGE = 50

  CATEOGRY = [
    ['Roamingman','RoamingmanBookingGroup'],
    ['Globaltix','GlobaltixTicketBookingGroup'],
    ['Series','BookingGroup'],
    ['Activity','ActivityBookingGroup'],
    ['Flight','FlightBookingGroup'],
    ['Hotel','TBOBooking'],
    ['Land Tour','LandTourBookingGroup']
  ]

  def self.generate(booking_group, payment, always_create = false)
    invoice = nil
    if payment.present? && payment.amount.present? && payment.amount > 0
      if always_create || payment.etravel_invoice.blank?
        invoice = EtravelInvoice.create(payment: payment)
        booking_group.etravel_invoices << invoice
      else
        invoice = payment.etravel_invoice
      end
    end
    invoice

  end

  def invoice_no
    "DIB2B9#{self.id.to_s.rjust(8, '0')}"
  end

  def receipt_no
    "SRB2B9#{self.id.to_s.rjust(8, '0')}"
  end

  def departure
    if taggable_type == "RoamingmanBookingGroup"
      taggable.roamingman_bookings.first.depart_date.strftime("%Y-%m-%d")
    elsif taggable_type == "GlobaltixTicketBookingGroup"
      if taggable.globaltix_ticket_bookings.first.globaltix_ticket_booking_group.detail.present?
        taggable.globaltix_ticket_bookings.first.globaltix_ticket_booking_group.detail.first['visitDate']
      else
        ""
      end
    elsif taggable_type == "ActivityBookingGroup"
      taggable.activity_bookings.first.activity_booking_group.travel_date.strftime("%Y-%m-%d")
    elsif taggable_type == "BookingGroup"
      taggable.bookings.first.tour.tour_flights.first.departure.strftime("%Y-%m-%d")
    elsif taggable_type == "LandTourBookingGroup"
      taggable.departure_date.strftime("%Y-%m-%d")
    elsif taggable_type == "FlightBookingGroup"
      taggable.flight_booking_details.first.departure_time.strftime("%Y-%m-%d")
    elsif taggable_type == "TBOBooking"
      taggable.check_in.strftime("%Y-%m-%d")
    else
      taggable_type
    end
  end

  def return
    if taggable_type == "RoamingmanBookingGroup"
      taggable.roamingman_bookings.first.return_date.strftime("%Y-%m-%d")
    elsif taggable_type == "GlobaltixTicketBookingGroup" 
      ""
    elsif taggable_type == "ActivityBookingGroup"
      ""
    elsif taggable_type == "BookingGroup"
      taggable.bookings.first.tour.tour_flights.last.arrival.strftime("%Y-%m-%d")
    elsif taggable_type == "LandTourBookingGroup"
      taggable.return_date.strftime("%Y-%m-%d")
    elsif taggable_type == "FlightBookingGroup"
      taggable.flight_booking_details.last.arrival_time.strftime("%Y-%m-%d")
    elsif taggable_type == "TBOBooking"
      taggable.check_out.strftime("%Y-%m-%d")
    else
      taggable_type
    end
  end

  def total_price
    if taggable_type == "RoamingmanBookingGroup"
      taggable.roamingman_bookings.first.price 
    elsif taggable_type == "GlobaltixTicketBookingGroup"
      taggable.globaltix_ticket_bookings.first.globaltix_ticket_booking_group.price 
    elsif taggable_type == "ActivityBookingGroup"
      taggable.activity_bookings.first.price 
    elsif taggable_type == "BookingGroup"
      (taggable.bookings.first.booking_group.deposit + taggable.bookings.first.booking_group.received_payment)
    elsif taggable_type == "LandTourBookingGroup"
      (taggable.land_tour_bookings.first.land_tour_booking_group.deposit + taggable.land_tour_bookings.first.land_tour_booking_group.received_payment)
    elsif taggable_type == "FlightBookingGroup"
      taggable.payment.amount
    elsif taggable_type == "TBOBooking"
      taggable.payment.amount
    else
      taggable_type
    end
  end

  def name
    if taggable_type == "RoamingmanBookingGroup"
      taggable.roamingman_bookings.first.name 
    elsif taggable_type == "GlobaltixTicketBookingGroup"
      taggable.globaltix_ticket_bookings.first.name
    elsif taggable_type == "ActivityBookingGroup"
      taggable.activity_bookings.first.name
    elsif taggable_type == "BookingGroup"
      taggable.bookings.first.name
    elsif taggable_type == "LandTourBookingGroup"
      taggable.land_tour_bookings.first.name
    elsif taggable_type == "FlightBookingGroup"
      (taggable.flight_bookings.first.last_name + " " + taggable.flight_bookings.first.first_name)
    elsif taggable_type == "TBOBooking"
      (taggable.guests.first.last_name + " " + taggable.guests.first.first_name )
    else
      taggable_type
    end
  end

  def product
    if taggable_type == "RoamingmanBookingGroup"
      (taggable.roamingman_bookings.first.roamingman_package.name + " " + taggable.roamingman_bookings.first.roamingman_package.category)
    elsif taggable_type == "GlobaltixTicketBookingGroup"
      (taggable.globaltix_ticket_bookings.first.globaltix_ticket.title)
    elsif taggable_type == "ActivityBookingGroup"
      taggable.activity.title
    elsif taggable_type == "BookingGroup"
      (taggable.bookings.first.tour.caption + " " + taggable.bookings.first.tour.other_caption)
    elsif taggable_type == "LandTourBookingGroup"
      (taggable.land_tour_bookings.first.land_tour.title + " " + taggable.land_tour_bookings.first.land_tour.cn_title)
    elsif taggable_type == "FlightBookingGroup"
      (taggable.flight_booking_details.first.origin + "-" +taggable.flight_booking_details.first.destination  + " " + taggable.flight_booking_details.first.carrier )
    elsif taggable_type == "TBOBooking"
      taggable.hotel.hotel_name
    else
      ""
    end
  end

  def agent
    if taggable_type == "RoamingmanBookingGroup"
      taggable.roamingman_bookings.first.agent_user.username
    elsif taggable_type == "GlobaltixTicketBookingGroup"
      taggable.globaltix_ticket_bookings.first.agent_user.username
    elsif taggable_type == "ActivityBookingGroup"
      taggable.activity_bookings.first.agent_user.username 
    elsif taggable_type == "BookingGroup"
      taggable.bookings.first.agent_user.username
    elsif taggable_type == "LandTourBookingGroup"
      taggable.land_tour_bookings.first.agent_user.username
    elsif taggable_type == "FlightBookingGroup"
      taggable.flight_bookings.first.agent_user.username
    elsif taggable_type == "TBOBooking"
      taggable.agent_user.try(:username)
    else
      ""
    end
  end

  def invoice
    if taggable_type == "RoamingmanBookingGroup"
      invoice_no 
    elsif taggable_type == "GlobaltixTicketBookingGroup"
      invoice_no
    elsif taggable_type == "ActivityBookingGroup"
      invoice_no
    elsif taggable_type == "BookingGroup"
      invoice_no
    elsif taggable_type == "LandTourBookingGroup"
    elsif taggable_type == "FlightBookingGroup"
      invoice_no
    elsif taggable_type == "TBOBooking"
      invoice_no
    else
      ""
    end
  end

  def order_id
    if taggable_type == "RoamingmanBookingGroup"
      taggable.roamingman_bookings.first.oms_order_id
    elsif taggable_type == "GlobaltixTicketBookingGroup"
      taggable.globaltix_ticket_bookings.first.globaltix_ticket_booking_group.reference_number 
    elsif taggable_type == "ActivityBookingGroup"
      taggable.activity_bookings.first.code
    elsif taggable_type == "BookingGroup"
      taggable.code
    elsif taggable_type == "LandTourBookingGroup"
      taggable.code
    elsif taggable_type == "FlightBookingGroup"
      taggable.host_code
    elsif taggable_type == "TBOBooking"
      taggable.confirmation_no   
    else
      ""
    end
  end

end
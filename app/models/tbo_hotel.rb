class TBOHotel < ApplicationRecord  
  serialize :hotel_facilities, Array
  serialize :attractions, Array

  has_many :covers, as: :taggable
  accepts_nested_attributes_for :covers, allow_destroy: true
  enum markup_type: [:percentage, :fixed]
  
  belongs_to :city, class_name:'TBOCity', foreign_key: :city_name, primary_key: :code, optional:true

  scope :search_by_hotel, ->(hotel_keyword) { where("hotel_name ILIKE ?", "%#{hotel_keyword}%") if hotel_keyword.present? }
  scope :search_by_city, ->(city_keyword, city_code) { where("city_code ILIKE ? or city_name ILIKE ? or country_name ILIKE ? ", "#{city_code}", "%#{city_keyword}%", "%#{city_keyword}%") if city_keyword.present? || city_code.present? }
  scope :search_by_city_code, ->(city_keyword) { where("city_code ILIKE ?", "%#{city_keyword}%") if city_keyword.present? }
  scope :search_by_rating, ->(rating_keyword) { where("hotel_rating ILIKE ?", "%#{rating_keyword}%") if rating_keyword.present? }
  # scope :search_with_city, ->(hotel_keyword, city_keyword) { where("hotel_name ILIKE ? and city_name ILIKE ?", "%#{hotel_keyword}%", "%#{city_keyword}%") }
  scope :active, -> { where(is_active: true) }

  validates_uniqueness_of :hotel_code
  default_scope -> {order(:hotel_name)}
  USD_CONVERTED_TO_MYR = 4
  RATING_MAPPING = {
      "OneStar"   => 1,
      "TwoStar"   => 2,
      "ThreeStar" => 3,       
      "FourStar"  => 4,
      "FiveStar"  => 5
  }

  def rating_int      
      RATING_MAPPING[self.hotel_rating] || 0
    end

  def create_image(url)
    # carrier missing extension remote upload workaround
    uri = URI.parse(url)
    filename = "#{uri.query.gsub(/[^0-9A-Z]/i, '_')}.jpg"
    if not_duplicate_image?(filename) 
      tempfile = Down.download(url)
      filepath = "/tmp/#{filename}"
      FileUtils.mv(tempfile.path, filepath)    

      new_image = Cover.new
      new_image.image = File.open(filepath)
      new_image.taggable = self
      new_image.inspect      
      new_image.save
    end
  end

  def delete_images(image_ids)
    covers = Cover.where(id: image_ids)
    covers.delete_all
  end

  def not_duplicate_image?(filename)
    images = self.covers
    images.detect{|x|x.image_identifier == filename}.nil?
  end

  def image_urls
    covers.map{|x| x.image_url}
  end

  def self.rate_with_markup(rate, markup, currency)
    markup = general_markup unless markup.amount.present?  
    rate_in_myr = 0
    if currency == 'USD'
      exchange_rate = ExchangeRate.find_by(from_currency: 'USD', to_currency: 'MYR').try(:rate) || USD_CONVERTED_TO_MYR
      rate_in_myr = (rate * exchange_rate).ceil
    else
      rate_in_myr = rate
    end
    markup.apply(rate_in_myr)[:total]
  end

  def self.rate_in_myr(rate, currency)
    rate_in_myr = 0
    if currency == 'MYR'
      rate_in_myr = rate     
    else
      exchange_rate = ExchangeRate.find_by(from_currency: currency, to_currency: 'MYR').try(:rate) || USD_CONVERTED_TO_MYR
      rate_in_myr = (rate * exchange_rate).ceil      
    end
    rate_in_myr
  end

  def hotel_markup
    HotelBooker::HotelMarkup.new(amount: markup_amount, type: markup_type)
  end

  def hotel_markup=(markup)
    self.markup_amount = markup.amount
    self.markup_type = markup.type
  end

  def self.general_markup_holder
    where(hotel_code: nil).first_or_create(markup_type:'fixed', markup_amount:0)
  end

  def self.general_markup
    general_markup_holder.hotel_markup
  end

  def activate!
    self.update_columns(is_active: true)
  end

  def deactivate!
    self.update_columns(is_active: false)
  end

end
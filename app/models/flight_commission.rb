class FlightCommission < ApplicationRecord

  ADMIN_FLIGHT_COMMISSION_PER_PAGE = 50

  COMMISSION_TYPES = [
    'percentage',
    'fixed'
  ]

  validates :name, :airline_iata, presence: true
  validates :commission, presence: { message: "can't be empty, when commission category is show" }, if: -> { commission_category?  }
  validates_format_of :origin_airport_iata, :with => /\D/, message: "must be present as Alphabets" 


  def origin_airport_iata=(value)
    write_attribute(:origin_airport_iata, value.upcase)
  end

  def calculate_commission(original_price)
    commission_val = 0
    if self.send("commission_category").present? && self.send("commission") != 0
      commission_by = self

      if commission_by.send("commission_category") == "percentage"
        commission_val = 0.0 - (original_price * commission_by.send("commission") / 100).floor
      elsif commission_by.send("commission_category") == "fixed"
        commission_val = 0.0 - commission_by.send("commission")
      end 
    end
    commission_val
  end

  def markup_display
    if self.send("commission_category").present?
      if self.send("commission_category") == "percentage"
        '+' + self.send("commission").to_s + '%'
      elsif self.send("commission_category") == "fixed"
        '+' + self.send("commission").to_s
      else
        ''
      end
    else
      ''
    end
  end

end

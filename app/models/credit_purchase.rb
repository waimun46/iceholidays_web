class CreditPurchase < ApplicationRecord
  belongs_to :user
  has_many :payments, as: :taggable

  enum status: [:pending, :topped_up]

  before_create :calculate_extra_credits

  CREDIT_PACKAGES = [
    {
      credits: 2000,
      extra_credits_percentage: 0.8,
      amount: 2000
    },
    {
      credits: 4000,
      extra_credits_percentage: 1,
      amount: 4000
    },
    {
      credits: 10000,
      extra_credits_percentage: 1.5,
      amount: 10000
    },
    {
      credits: 20000,
      extra_credits_percentage: 2,
      amount: 20000
    },
  ]

  def product_description
    "Ice Holidays Credit Purchase"
  end

  def buyer_email
    if self.user.email.present?
      self.user.email
    else
      ""
    end
  end

  def total_credits
    if self.credits.present? && self.extra_credits.present?
     self.credits + self.extra_credits
    else
      0
    end
  end

  def calculate_extra_credits
    CREDIT_PACKAGES.sort_by {|package| package[:amount]}.each do |cp|
      if self.amount >= cp[:amount]
        self.extra_credits = self.amount * cp[:extra_credits_percentage] / 100
        break
      end
    end
  end

  def purchase_credits
    self.user.topup_credits(self.total_credits.to_i, self, "Purchase Credits #{self.credits}")
    self.topped_up!
  end

  def send_notify
    # TODO: send receipt or confirmation
  end

end
class Session < ApplicationRecord
  belongs_to :user

  scope :non_expired, ->(time) { where("sessions.updated_at >= ? AND sessions.token != ''", time) }
  scope :expired, ->(time) { where("sessions.updated_at < ? OR sessions.token = ''", time)}

  validate :verify_token_changable, on: :update

  def verify_token_changable
    if self.token != '' && self.token_was != '' && self.updated_at >= Session.expired_time
      errors.add(:base, "Session is just reuse.")
    end
  end

  def self.expired_time
    
    if Rails.env.iceb2b? || Rails.env.testb2b?
      Time.now - 15.minutes
    else
      Time.now - 10.hours
    end

  end

  def expired?
    !self.non_expired?
  end

  def non_expired?
    if self.token != '' && self.updated_at >= Session.expired_time
      true
    else
      false
    end
  end

  def remove_token!
    self.token = ''
    self.save
  end

end

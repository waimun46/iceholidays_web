class BookingAlteration < ApplicationRecord
  belongs_to :original_tour, class_name: "Tour", optional: true
  belongs_to :original_booking_group, class_name: "BookingGroup", optional: true
  
  if Rails.configuration.crawling
    belongs_to :created_user, class_name: "User", optional: true
  else
    belongs_to :created_user, class_name: "User"
  end


  belongs_to :prev, class_name: "BookingAlteration", optional: true
  has_one :next, class_name: 'BookingAlteration', foreign_key: 'prev_id'
  has_many :bookings
  has_many :booking_cruise_rooms, -> { distinct(:booking_cruise_room_id) }, through: :bookings
  belongs_to :restore_withdrawn_user, class_name: "User", optional: true

  enum alter_type: [:withdrawn, :transferred]

  validates :original_tour, presence: true, if: -> { transferred? }

  scope :search, ->(keyword) { where("booking_alterations.reason ILIKE ? or tours.code ILIKE ? or booking_groups.code ILIKE ? or bookings.name ILIKE ?", "%#{keyword}%", "%#{keyword}%", "%#{keyword}%", "%#{keyword}%") }
  scope :filter_user, -> (keyword) { joins(:created_user).where("users.username ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  scope :visible, -> { includes(:bookings).where.not(bookings: {id: nil})}
  scope :filter_type, ->(type) { send(type) if BookingAlteration.alter_types.include?(type) }

  TRANSFERRED_REASON = [
    'Tour not materialized',
    'Pax request transfer tour',
    'Medical reason',
    'Flight cancelled',
    'Covid-19',
    'Others'
  ]

  WITHDRAWN_REASON = [
    'Clear KIV',
    'Tour not materialized',
    'Open selling',
    'Forfeit deposit',
    'Medical reason',
    'Flight cancelled',
    'Cancellation fee applies',
    'Others'
  ]

  def auto_cancellation?
    reason[0..16] == "Auto-cancellation" && created_user_id.blank? && self.withdrawn?
  end

  def new_tour
    if self.transferred? && bookings.present?
      bookings.first.tour
    elsif self.transferred? && self.next.present? && self.next.original_tour.present?
      self.next.original_tour
    else
      nil
    end
  end

  def new_booking_group
    if self.transferred? && !self.whole_group && bookings.present?
      bookings.first.booking_group
    elsif self.transferred? && self.next.present? && self.next.original_booking_group.present?
      self.next.original_booking_group
    else
      nil
    end
  end

  def restore_withdrawn(user)
    if self.withdrawn?

      if self.original_tour.is_cruise?
        restore_seats = {}
        self.booking_cruise_rooms.group(:room_price_type).count.each do |k, v|
          sk = k.split(/_(#{BookingCruiseRoom::PRICE_TYPES.join("|")})/)
          type = sk[0].sub('single_twin', 'twin')
          if !restore_seats.has_key?(type)
            restore_seats[type] = {}
          end
          restore_seats[type].store(sk[1], v)
        end
      else
        restore_seats = {"" => self.bookings.group(:price_type).count.map{|k, v| [k, v]}.to_h}
      end

      if self.original_tour.admin_able_to_purcharse?(restore_seats)
        prev_ba = self.prev
        booking_group = self.bookings.first.booking_group
        self.bookings.each do |booking|
          booking.booking_alteration = prev_ba
          booking.restore_withdrawn_alteration_id.present? ? booking.restore_withdrawn_alteration_id.push(self.id) : booking.restore_withdrawn_alteration_id = [self.id]
          booking.active!
        end

        if self.original_tour.is_cruise?
          self.original_tour.booking_cruise_rooms.each do |booking_cruise_room|
            if booking_cruise_room.bookings.map(&:status).all?{ |s| s == 'active'}
              booking_cruise_room.active!
            end
          end
        end

        booking_group.update_deposit
        booking_group.update_kiv
        self.original_tour.calculate_vacant_seats
        self.original_tour.calculate_payment_seats
        self.update_columns(restore_withdrawn_user_id: user.try(:id), restore_withdrawn_at: Time.now)
        true
      else
        false
      end
        
    else
      false
    end
  end

  def self.bulk_withdraw(bookings, alter_params, user)
    booking_group = bookings.first.booking_group
    if booking_group.bookings.count == bookings.count
      alter_params.merge!(whole_group: true)
    end
    alter = BookingAlteration.create(alter_params.merge(original_tour_id: bookings.first.tour_id, original_booking_group_id: booking_group.id, amount: bookings.map(&:price).inject(:+), created_user_id: user.try(:id)))

    bookings.each do |booking|
      booking.mighty = true
      booking.booking_alteration = alter
      booking.withdrawn!

    end

    if alter.original_tour.is_cruise?
      alter.original_tour.booking_cruise_rooms.each do |booking_cruise_room|
        if booking_cruise_room.bookings.map(&:status).all?{ |s| s == 'withdrawn'}
          booking_cruise_room.withdrawn!
        end
      end
    end

    booking_group.update_deposit
    booking_group.update_kiv
    alter.original_tour.calculate_vacant_seats
    alter.original_tour.calculate_payment_seats
    alter
  end

  def self.bulk_transfer(bookings, alter_params, new_tour, user)
    booking_group = bookings.first.booking_group
    if booking_group.bookings.count == bookings.count
      alter_params.merge!(whole_group: true)
      new_booking_group = booking_group
    else
      new_booking_group =  BookingGroup.create(deposit: bookings.count * new_tour.deposit)
    end

    if bookings.first.booking_alteration_id.present?
      alter_params.merge!(prev_id: bookings.first.booking_alteration_id)
    end

    alter = BookingAlteration.create(alter_params.merge(original_tour_id: bookings.first.tour_id, original_booking_group_id: booking_group.id, amount: bookings.map(&:price).inject(:+), created_user_id: user.try(:id)))

    bookings.each do |booking|
      booking.mighty = true
      booking.booking_alteration = alter
      booking.booking_group = new_booking_group
      booking.tour = new_tour

      # if booking.price_type != 'normal' && new_tour.send("#{booking.price_type}_price").present? && new_tour.send("#{booking.price_type}_seats") > 0
      #   booking.price = new_tour.send("#{booking.price_type}_price")
      # else
      #   booking.price_type = 'normal'
      #   booking.price = new_tour.normal_price
      #   booking.price_offered = false
      # end
      booking.active!
    end
    booking_group.update_deposit
    booking_group.update_kiv
    alter.original_tour.calculate_vacant_seats
    alter.original_tour.calculate_payment_seats
    new_tour.calculate_payment_seats
    alter
  end

end

class FlightDetail < ApplicationRecord
	belongs_to :flight_inventory
	has_many :flight_receipts
	accepts_nested_attributes_for :flight_receipts, reject_if: proc { |attributes| attributes['number'].blank? }, allow_destroy: true
end
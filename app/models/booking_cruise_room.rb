class BookingCruiseRoom < ApplicationRecord
  include BookingValidator

  has_many :bookings, -> { order(:id) }

  enum status: [:active, :withdrawn]

  TUPLE_TYPES = [
    'single_twin',
    'triple',
    'quad'
  ]

  ROOM_TYPES = [
    'interior_cabin',
    'oceanview',
    'balcony',
    'suite'
  ]

  PRICE_TYPES = [
    'normal',
    'normal1',
    'normal2',
    'normal3',
    'normal4',
    'normal5',
    'normal6',
    'normal7',
    'normal8',
    'normal9',
    'early_bird',
    'specialoffer',
    'specialdeal',
    'superpromo',
    'promo'
  ]

  ALL_ROOM_PRICE_TYPE = TUPLE_TYPES.product(ROOM_TYPES).map{|x| x.join('_')}.product(PRICE_TYPES).map{|x| x.join('_')}

  validates :room_price_type, inclusion: { in: ALL_ROOM_PRICE_TYPE }
  scope :visible, -> { where("booking_cruise_rooms.status != ?", BookingCruiseRoom.statuses[:withdrawn]) }
  scope :only_normal, -> { where("booking_cruise_rooms.price_type ILIKE '%normal%'")}

  def tour
    if bookings.present?
      bookings.first.tour
    else
      nil
    end
  end

  def room_type_text
    rpt = self.room_price_type
    if rpt.include?('single_twin')
      if bookings.visible.count == 1
        rpt.sub!('single_twin', 'single')
      else
        rpt.sub!('single_twin', 'twin')
      end
    end
    rpt.sub(/(#{Booking::PRICE_TYPES.map{|x| '_'+x}.join('|')})/, '').titleize
  end

end
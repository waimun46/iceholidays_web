class Airline < ApplicationRecord

  mount_uploader :logo, ImageUploader

  MARKUP_TYPES = [
    'percentage',
    'fixed'
  ]

  validates :name, :iata_code, presence: true
  validates :name, uniqueness: { case_sensitive: false }
  validates :iata_code, uniqueness: { case_sensitive: false }
  validates :economy_markup_category, :business_markup_category, :first_markup_category, inclusion: { in: MARKUP_TYPES }, allow_blank: true
  scope :search, ->(keyword) { where("airlines.name ILIKE ? or airlines.iata_code ILIKE ? or airlines.country ILIKE ?", "%#{keyword}%", "%#{keyword}%",  "%#{keyword}%") }

  typed_store :markup_categories, coder: PostgresCoder do |s|
    s.string :economy_markup_category, default: ""
    s.string :business_markup_category, default: ""
    s.string :first_markup_category, default: ""
  end

  typed_store :markups, coder: PostgresCoder do |s|
    s.decimal :economy_markup, default: 0
    s.decimal :business_markup, default: 0
    s.decimal :first_markup, default: 0
  end

  def final_price(original_price, cabin)
    if self.send("#{cabin}_markup_category").present? && self.send("#{cabin}_markup") > 0
      markup_by = self
    else
      markup_by = Airline.find_by_name('Unknown')
    end

    if markup_by.send("#{cabin}_markup_category").present? && markup_by.send("#{cabin}_markup") != 0
      if markup_by.send("#{cabin}_markup_category") == "percentage"
        (original_price * (1 + markup_by.send("#{cabin}_markup") / 100)).ceil + 0.0
      elsif markup_by.send("#{cabin}_markup_category") == "fixed"
        (original_price + markup_by.send("#{cabin}_markup")).ceil + 0.0
      else
        original_price
      end
    else
      original_price
    end
  end

  def markup_display(cabin)
    if self.send("#{cabin}_markup_category").present?
      if self.send("#{cabin}_markup_category") == "percentage"
        '+' + self.send("#{cabin}_markup").to_s + '%'
      elsif self.send("#{cabin}_markup_category") == "fixed"
        '+' + self.send("#{cabin}_markup").to_s
      else
        ''
      end
    else
      ''
    end
  end

end

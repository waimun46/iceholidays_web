class FlightInventory < ApplicationRecord
	has_many :reminders, as: :taggable
	has_many :flight_details
	has_many :flight_seats
	accepts_nested_attributes_for :flight_details, reject_if: proc { |attributes| attributes['airline_iata'].blank? }, allow_destroy: true
	accepts_nested_attributes_for :flight_seats, reject_if: proc { |attributes| attributes['total_seat'].blank? }, allow_destroy: true
	accepts_nested_attributes_for :reminders, reject_if: proc { |attributes| attributes['reminder_date'].blank? || attributes['email'].blank?}, allow_destroy: true
end

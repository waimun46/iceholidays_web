class Fair < ApplicationRecord
  has_many :tours

  scope :search, ->(keyword) { where("fairs.code ILIKE ? or fairs.title ILIKE ? or fairs.other_title ILIKE ? ", "%#{keyword}%", "%#{keyword}%",  "%#{keyword}%") }
  scope :ongoing, -> { where("duration::tsrange @> ?::timestamp", Time.now) }
  scope :active, -> { where(active: true) }
  scope :allow_user, ->(user_id) { where("? = ANY(allow_user_ids)", user_id) }
  # scope :allow_user, ->(user_id) { where("allow_user_ids @> ARRAY[?]", user_id) }
  scope :allow_itinerary, ->(itinerary_id) { where("? = ANY(allow_itinerary_ids)", itinerary_id) }
  scope :pre_and_ongoing, -> { where("duration::tsrange && tsrange(?,?)", Time.now, Time.now + 1.day)}
  scope :pre_ongoing, -> { where("tsrange(?,?) @> lower(duration)", Time.now, Time.now + 1.hour) }
  scope :search_date, ->(date) { where("duration::tsrange @> ?::timestamp", date) }  

  FAIR_TYPES = [
    'promo',
    'superpromo',
    'specialdeal'
  ]

  validates :code, uniqueness: true
  validates :code, presence: true
  validates :duration, presence: true, if: :active?
  validates :fair_type, presence: true, if: :active?
  validates :fair_type, inclusion: { in: FAIR_TYPES }, allow_blank: true

  def duration_start
    if duration.present?
      duration.begin.in_time_zone
    else
      nil
    end
  end

  def duration_end
    if duration.present?
      duration.end.in_time_zone
    else
      nil
    end
  end

  def join_allow_users
    if self.allow_user_ids.present?
      User.where(id: self.allow_user_ids).map(&:username).join(', ')
    else
      ''
    end
  end

  def join_allow_itineraries
    if self.allow_itinerary_ids.present?
      Itinerary.where(id: self.allow_itinerary_ids).map(&:code).join(', ')
    else
      ''
    end
  end

  def allow_users_to_json
    if self.allow_user_ids.present?
      User.where(id: self.allow_user_ids).select(:id, :username).to_json
    else
      ''
    end
  end

  def self.messages
    Fair.pre_and_ongoing.active.map(&:messages).uniq.reject(&:empty?)
  end

  def fair_status
    @current_time ||= Time.now
    if self.duration.present?
      if self.duration.include?(@current_time)
        "Ongoing"
      elsif self.duration.begin > @current_time
        "Upcoming"
      elsif self.duration.end < @current_time
        "Passed"
      else
        ""
      end
    else
      ""
    end
  end

end

class Holiday < ApplicationRecord
  include HolidayValidator

  attr_accessor :dates_preset, :by_preset
  validates :year, uniqueness: true
  validates :year, numericality: { greater_than: 2000, less_than: 2100 }
  # validates :dates_formated, format: { with: }
  # validates :dates_preset, date_list: true
  validates_with DateListValidator, if: -> { by_preset }

  before_save :set_dates, if: -> { by_preset }

  def set_dates
    if dates_preset.blank?
      self.dates = []
    else
      dates = dates_preset.split("\n").uniq
      self.dates = dates.map do |date|
        Date.parse("#{date} #{self.year}")
      end
    end
  end

  def dates_formated
    dates.map{|date| date.strftime("%-d %b")}
  end

  def self.recent_holidays
    current = Date.current
    holidays = []
    years = []
    years << current.year

    if current.month <=2
      years << (current.year - 1)
    end

    years.each do |year|
      holiday = Holiday.find_by_year(year)
      if holiday.present? && holiday.dates.present?
        holidays += holiday.dates
      end
    end
    holidays
  end

  # def dates_formated=(val)
  #   if val.blank?
  #     write_attribute(dates: [])
  #   else
  #     write_attribute(dates: val.split("\n"))
  #   end
  # end
  
end

class RoamingmanBookingGroup < ApplicationRecord
  SHIPPING_FEE = 8.00

  belongs_to :last_edited_user, class_name: "User", optional: true
  belongs_to :exported_user, class_name: "User", optional: true
  has_many :roamingman_bookings, -> { order("id") }
  has_one :payment, as: :taggable
  has_many :etravel_invoices, as: :taggable
  accepts_nested_attributes_for :roamingman_bookings, allow_destroy: true, limit: 1
  validates_associated :roamingman_bookings

  enum deposit_mode: [:prepaid, :payment_on_the_spot]

  scope :visible, -> { includes(:roamingman_bookings).where.not(roamingman_bookings: {id: nil})} # include cancelled bookings
  scope :in_between, ->(interval) { where(created_at:(interval))}
  scope :in_between_departure, ->(interval) { includes(:roamingman_bookings).where(roamingman_bookings: {depart_date:(interval)})}
  scope :search_country, ->(keyword) { joins(roamingman_bookings: [:roamingman_package]).where("array_to_string(roamingman_packages.country, ', ') ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  scope :search_package_code, ->(keyword) { joins(roamingman_bookings: [:roamingman_package]).where("roamingman_packages.code ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  scope :search_name, ->(keyword) { joins(:roamingman_bookings).where("roamingman_bookings.name ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  scope :search_booking_id, ->(keyword) { includes(:roamingman_bookings).where("CAST(roamingman_bookings.id AS TEXT) LIKE ?", "%#{keyword}%").distinct if keyword.present? }

  before_save :update_last_edited_user
  after_create :calculate_price

  def self.search_agent(keyword)
    if keyword.present?
      users_id = User.where("username ILIKE ? or alias_name ILIKE ? ", "%#{keyword}%", "%#{keyword}%").to_a
      joins(:roamingman_bookings).where("roamingman_bookings.agent_user_id IN (?)", users_id)
    else
      where('true')
    end
  end

  def reference_id
    self.id.to_s.rjust(10, '0')
  end

  def agent_user
    if roamingman_bookings.present?
      roamingman_bookings.first.agent_user
    else
      nil
    end
  end

  def calculate_price
    if self.roamingman_bookings.present?
      update_column :price, self.roamingman_bookings.map(&:price).inject(:+)
    else
      update_column :price, 0
    end
  end

  def total_quantity
    roamingman_bookings.map(&:quantity).inject(:+).to_i
  end

  def cancel_booking
    self.roamingman_bookings.each do |booking|
      # not done yet
      # booking.archived!
    end
  end

  def update_last_edited_user
    if self.last_edited_user_id.present? && self.changed?
      self.last_edited_user_id = last_edited_user_id
      self.last_edited_at = Time.now
    end
  end

  def non_editable?
    if self.roamingman_bookings.visible.any?{ |b| b.non_editable? }
      true
    else
      false
    end
  end

  def able_to_export?
    if self.roamingman_bookings.visible.any?{ |b| b.able_to_export? }
      true
    else
      false
    end
  end

  def product_name
    if roamingman_bookings.first.roamingman_package.present?
      roamingman_bookings.first.roamingman_package.name
    else
      ""
    end
  end

  def deliver_point
    if roamingman_bookings.first.roamingman_package.present?
      RoamingmanBooking::DELIVER_POINT.select{|k,v|k == roamingman_bookings.first.deliver_point_id}.values.first
    else
      ""
    end
  end

  def product_description
    "Ice Holidays WIFI Booking"
  end

  def booking_quantity
    if roamingman_bookings.present?
      roamingman_bookings.count
    else
      0
    end
  end

  def buyer_email
    if roamingman_bookings.first.agent_user.present?
      roamingman_bookings.first.agent_user.email
    else
      ""
    end
  end

  def export_count
    self.roamingman_bookings.visible.select{ |b| b.able_to_export? }
  end

  def export_roamingman_create(user = nil)
    ret = []
    self.roamingman_bookings.visible.each do |booking|
      if booking.able_to_export?
        ret << booking.export_roamingman_create(user)
      end
    end
    ret.all?
  end

  def send_notify
    agent = self.agent_user
    if agent.present?
      if agent.emails.present?
        WifiMailer.notify(agent, self, agent.emails.join(', ')).deliver_now
        if self.has_payment?
          WifiMailer.receipt(agent, self, agent.emails.join(', ')).deliver_now
        end
      end
    end
  end

  def has_payment?
    self.payment.paid? && self.payment.amount >= 1
  end

  def redirect_url
    Rails.application.routes.url_helpers.thank_you_roamingman_booking_groups_path(:id => self.id)
  end

  def show_url
    Rails.application.routes.url_helpers.roamingman_packages_path
  end

  def ref_no
    "BRMM#{self.id.to_s.rjust(9, '0')}"
  end
end
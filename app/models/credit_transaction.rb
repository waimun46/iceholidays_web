class CreditTransaction < ApplicationRecord
  belongs_to :user
  belongs_to :taggable, polymorphic: true
  belongs_to :prev, class_name: "CreditTransaction", optional: true

  validates :taggable, presence: true

  after_create :generate_checksum

  def calculate_checksum
    if prev.present?
      combine_key = "#{taggable_type}|#{taggable_id}|#{user_id}|#{prev_id}|#{amount.to_s}|#{user_balance.to_s}|#{created_at}|#{prev.checksum}"
    else
      combine_key = "#{taggable_type}|#{taggable_id}|#{user_id}|#{prev_id}|#{amount.to_s}|#{user_balance.to_s}|#{created_at}|"
    end
    Digest::SHA1.hexdigest combine_key
  end

  def checksum_valid?
    calculate_checksum == self.checksum
  end

  def generate_checksum
    if checksum.blank?
      update_column :checksum, calculate_checksum
    end
  end
  def reference_category
    if self.taggable_type == 'Payment'
      self.taggable.get_category_name
    else
      "Top Up"
    end
  end
  def get_reference_id
    if self.taggable_type == 'Payment'
      if self.taggable.taggable.present?
        self.taggable.taggable.reference_id
      else
        ""
      end
    end
  end
  def total_balance
    self.user_balance + self.amount
  end
end
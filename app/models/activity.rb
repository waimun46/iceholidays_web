class Activity < ApplicationRecord
  enum status: [:active, :archived]

  enum cancellation: [:free_cancellation, :non_cancellation]
  enum e_ticketing: [:e_ticket, :printed_ticket, :printed_or_e_ticket]
  enum travel_date_type: [:no_specific_date, :open_date, :fixed_date]
  enum ticket_type: [:no_specific_ticket_type, :enter_directly, :collect_physical, :collect_physical_ticket_at_dedicated_point]
  enum transfer: [:private_transfer, :sit_in_coach, :no_transfer]
  enum meeting_point: [:meet_up_with_guide, :hotel_pick_up, :meet_up_at_location, :no_specific_meeting_point]
  enum confirmation_time: [:instant_confirmation, :_24_hours_confirmation, :_48_hours_confirmation, :_72_hours_confirmation]
  enum category: [:sightseeing, :attraction]

  validates :title, presence: true

  scope :available, -> { where(id: ActivityPrice.pluck(:activity_id).uniq) }
  scope :visible, -> { where("activities.status != ?", Activity.statuses[:archived]) }
  scope :bookable, -> { where("booking_date && tsrange(?, ?)", Time.now.midnight, Time.now.midnight + 1.day) }
  scope :countries, -> { distinct.pluck(:country) }
  scope :search_keyword, ->(keyword) { where("country ILIKE :keyword OR title ILIKE :keyword", keyword: "%#{keyword}%")  if keyword.present? }
  scope :search_category, ->(category) { where("category = ?", Activity.categories[category]) if category.present? }
  scope :search_country, ->(keyword) { where("country ILIKE ?", "%#{keyword}%") if keyword.present? }
  scope :search_title, ->(keyword) { where("title ILIKE ?", "%#{keyword}%") if keyword.present? }
  # scope :search_from_to_date, ->(from_date, to_date) { where("travelling_date && tsrange(?, ?)", from_date, to_date) if from_date.present? && to_date.present? }

  has_many :covers, as: :taggable
  has_many :activity_prices, dependent: :destroy
  has_many :activity_bookings, -> { order("id") }
  has_many :activity_booking_groups, through: :activity_bookings

  accepts_nested_attributes_for :covers, reject_if: proc { |attributes| attributes['image'].blank? }, allow_destroy: true
  accepts_nested_attributes_for :activity_prices, reject_if: proc { |attributes| attributes['price'].blank? }, allow_destroy: true

  LANGUAGES = [
    'English',
    'Chinese',
    'Malay'
  ]

  CATEGORIES = [
    'Attraction',
    'Adventure Tours',
    'Day Tours',
    'Water Tours',
    'Theme Tours',
    'Aerial Tours',
    'Gourmet Tours',
    'Overnight Excursion',
    'Nature & Wildlife'
  ]

  # def self.all_languages
  #   languages = {}
  #   Activity::LANGUAGES.each do |language|
  #     languages[language] = language
  #   end
  #   languages
  # end

  def price_categories
    self.activity_prices.map { |p| p.category }
  end

  def final_price(category, minus_dta = false)
    activity_prices.select { |p| p.category == category }.map { |e| e.final_price(minus_dta) }.first
  end

  def dta(category)
    activity_prices.select { |p| p.category == category }.map { |e| e.dta }.first
  end

  def min_price
    min_price = self.activity_prices.map(&:final_price).min.round
  end

  # def check_languages(language)
  #   if self.languages.present? && self.languages.include?(language)
  #     true
  #   else
  #     false
  #   end
  # end

  ['operation_days', 'categories', 'languages'].each do |f|
    define_method "join_#{f}" do
      if self.send(f).present?
        self.send(f).join(', ')
      else
        ''
      end
    end
  end

  def booking_date_start
    if booking_date.present?
      booking_date.begin.in_time_zone
    else
      nil
    end
  end

  def booking_date_end
    if booking_date.present?
      booking_date.end.in_time_zone
    else
      nil
    end
  end

  def travelling_date_start
    if travelling_date.present?
      travelling_date.begin.in_time_zone
    else
      nil
    end
  end

  def travelling_date_end
    if travelling_date.present?
      travelling_date.end.in_time_zone
    else
      nil
    end
  end
end
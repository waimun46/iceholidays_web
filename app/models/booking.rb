class Booking < ApplicationRecord
  include BookingValidator
  include ExportChubb
  
  ADMIN_BOOKINGS_PER_PAGE = 50
  CHUBB_LAUNCH_DATE = "01 May 2019".in_time_zone
  attr_accessor :mighty, :update_later, :ignore_overbook, :edited_user_id

  belongs_to :tour
  belongs_to :booking_group, optional: true
  belongs_to :booking_cruise_room, optional: true
  belongs_to :booking_room, optional: true
  if Rails.configuration.crawling
    belongs_to :agent_user, class_name: "User", optional: true
    belongs_to :initiator_user, class_name: "User", optional: true
  else
    belongs_to :agent_user, class_name: "User"
    belongs_to :initiator_user, class_name: "User"
  end
  belongs_to :booking_alteration, optional: true
  belongs_to :last_edited_user, class_name: "User", optional: true

  has_many :receipts, through: :booking_group
  has_many :infant_bookings
  accepts_nested_attributes_for :infant_bookings, reject_if: proc { |attributes| attributes['date_of_birth'].blank? }, allow_destroy: true

  has_many :gift_trackings

  typed_store :exported_details, coder: PostgresCoder do |s|
    s.decimal :tour_fee, default: 0
    s.decimal :infant_fee, default: 0
    s.decimal :DTA, default: 0
    s.decimal :PRE, default: 0
    s.decimal :visa_fee, default: 0
    s.decimal :KL_tax, default: 0
    s.decimal :insurance_fee, default: 0
  end

  typed_store :policy_holder, coder: PostgresCoder do |j|
    j.string :ph_name
    j.datetime :ph_dob
    j.string :ph_designation
    j.string :ph_id_number
  end

  typed_store :insurance_export_remark, coder: PostgresCoder do |r|
    r.string :exported_name, default: ""
    r.datetime :exported_date_of_birth, default: ""
    r.string :exported_designation, default: ""
    r.string :exported_mobile, default: ""
    r.string :exported_passport_number, default: ""
    r.datetime :exported_passport_expiry_date, default: ""
    r.string :exported_tour_code, default: ""
    r.boolean :exported_insurance_nomination_flag, default: true
    r.any :exported_insurance_nomination, default: ""
  end

  typed_store :witness_details, coder: PostgresCoder do |w|
    w.string :witness_surname, default: ""
    w.string :witness_given_name, default: ""
    w.string :witness_passport_number, default: ""
  end

  typed_store :price_breakdown, coder: PostgresCoder do |s|
    s.decimal :collect_dta, default: 0
    s.decimal :commission_percentage, default: 0
    s.decimal :commission, default: 0
    s.any :subtract_breakdown, default: ""
  end

  # validates :admin_remark, length: { maximum: (MAX_NAME_LENGTH = 14) }
  validates :infant_bookings, length: {maximum: 2, message: "exceed the max 2 infants per guest"}
  validates :ph_dob, policy_holder_date_of_birth: true

  enum status: [:active, :withdrawn]
  enum gender: [:male, :female]
  enum category: [:adult, :child_twin, :child_with_bed, :child_no_bed]

  PRICE_TYPES = [
    'normal',
    'normal1',
    'normal2',
    'normal3',
    'normal4',
    'normal5',
    'normal6',
    'normal7',
    'normal8',
    'normal9',
    'early_bird',
    'specialoffer',
    'specialdeal',
    'superpromo',
    'promo'
  ]

  ROOM_TYPES = [
    'Single',
    'Double Bed',
    'Triple Sharing',
    'Twin Sharing',
    'Child No Bed',
    'Child With Bed',
    'Child Twin Share'
    # 'Request Roomie'
  ]

  PASSPORT_RECEIVED = [
    'Awaiting or N/A',
    'Photocopied',
    'Original',
    'Returned'
  ]

  SUMMARY_GROUP = [
    'hour',
    'day',
    'week',
    'month',
    'quarter',
    'year'
  ]

  SALES_GROUP = [
    'Sales Force',
    'State',
    'Destination'
  ]

  DESIGNATION = [
    :Miss, 
    :Mr, 
    :Mrs, 
    :Ms, 
    :Mstr, 
    :Mdm
    # :Dr, 
    # :Dato,
    # :Datin
  ]

  RELATIONSHIP = [
    :Spouse,
    :Father,
    :Mother,
    :Brother,
    :Sister,
    :Son,
    :Daughter,
    :Grandmother,
    :Grandfather,
    :Grandson,
    :Granddaughter,
    :Friend,
    :Others
  ]

  PERCENTAGE_SHARE = [
    0,
    25,
    50,
    75,
    100
  ]

  scope :search_code, ->(keyword) {joins(:booking_group).where("booking_groups.code ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  scope :type, -> (type) { where(price_type: type) }
  scope :visible, -> { where("bookings.status != ?", Booking.statuses[:withdrawn]) }
  scope :in_between_booked, -> (interval) { where(bookings: {created_at: interval})}
  scope :agent_user, -> (user) {where(agent_user: user)}
  scope :with_receipt, -> { includes({booking_group: [:receipts]}).where.not(booking_group: { receipts: {id: nil} }) }
  scope :with_receipt_or_fair_booked, -> { joins("LEFT OUTER JOIN fairs on bookings.agent_user_id = any(fairs.allow_user_ids) LEFT OUTER JOIN tours on tours.id = bookings.tour_id LEFT OUTER JOIN booking_groups on booking_groups.id = bookings.booking_group_id LEFT OUTER JOIN receipts ON receipts.booking_group_id = booking_groups.id").where("(fairs.active = true and fairs.duration::tsrange @> bookings.created_at::timestamp and tours.itinerary_id = ANY(fairs.allow_itinerary_ids) ) or (receipts.id IS NOT NULL) or (booking_groups.received_payment > 0)").distinct }

  scope :without_receipt, -> { includes({booking_group: [:receipts]}).where(booking_group: { receipts: {id: nil} }) }
  scope :search_destination, ->(keyword) { joins(:tour).where("tours.contient_country ILIKE ?", "%#{keyword}%") if keyword.present? }
  scope :search_tour_code, ->(keyword) { joins(:tour).where("tours.code ILIKE ?", "%#{keyword}%") if keyword.present? }
  scope :search_visible_tour, ->(keyword) { joins(:tour).where("tours.active is ?", keyword == 'Active') if keyword.present? }
  scope :seach_booking_id, ->(keyword) { where("CAST(bookings.id AS TEXT) LIKE ?", "%#{keyword}%") if keyword.present? }
  scope :search_name, ->(keyword) { where("name ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  scope :in_between_departure_date, ->(interval) { joins(:tour).where("tours.departure_date" => interval) if interval.present? }
  scope :search_sub_sales, ->(keyword) { where("sub_sale_rep ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  scope :search_user_origin_state, ->(keyword) { joins(:agent_user).where("users.state ILIKE ?", "%#{keyword}%").distinct if keyword.present? }
  scope :search_price_type, ->(keyword) { where("bookings.price_type IN (?)", keyword).distinct if keyword.present? }
  scope :search_category, ->(keyword) { where("bookings.category IN (?)", keyword).distinct if keyword.present? }
  scope :insurance_exported_at, ->(interval) { where(bookings: {insurance_exported_at: interval})}

  # scope :chargable, -> { where("created_at >= ? and status = ? and charged = false", Invoice::CHARGE_STARTED, Booking.statuses[:active]) }
  # user:globalitetravel and tour:"MH-______-CH" booking is not chargable
  # scope :chargable, -> { joins(:tour).where("bookings.created_at >= ? and bookings.status = ? and bookings.charged = false", Invoice::CHARGE_STARTED, Booking.statuses[:active]).where.not("tours.code ILIKE '%MH-______-CH%' and bookings.agent_user_id = ? and agent_user_id is not null", User.find_by_username('globalitetravel')) }
  # scope :refundable, -> { where("created_at >= ? and status = ? and charged = true", Invoice::CHARGE_STARTED, Booking.statuses[:withdrawn]) }
  scope :chargable, -> (datetime_range) { joins(:tour).where("bookings.status = ? and bookings.charged = false", Booking.statuses[:active]).where(tours: { departure_date: datetime_range}).where.not("tours.code ILIKE '%MH-______-CH%' and bookings.agent_user_id = ? and agent_user_id is not null", User.find_by_username('globalitetravel')) }
  scope :exported, -> { where("bookings.status != ? and insurance_exported = true", Booking.statuses[:withdrawn]) }
  scope :non_exported, -> { where("bookings.status != ? and insurance_exported = false", Booking.statuses[:withdrawn]) }

  scope :exclude_extra_room_type, -> { visible.where(room_type: ['Single', 'Double Bed', 'Triple Sharing', 'Twin Sharing', 'Child Twin Share']) }
  scope :adult_child_twin, -> { where("category = ? or category = ? ", Booking.categories[:adult], Booking.categories[:child_twin]) }
  scope :only_normal, -> { where("bookings.price_type ILIKE '%normal%'")}

  validates :tour, :price, :price_type, :category, presence: true
  # validates :agent_user, :initiator_user, presence: true
  validates :booking_group, presence: true, if: -> { update_later.blank? }
  # validates :name, :passport_number, :passport_expiry_date, :passport_issuing_place, :passport_photocopy, :date_of_birth, presence: true, if: -> { update_later.blank? && mighty.blank? }
  validates :price_type, inclusion: { in: PRICE_TYPES }
  validates :room_type, inclusion: { in: ROOM_TYPES }, allow_blank: true
  validates :passport_received, inclusion: { in: PASSPORT_RECEIVED }, allow_blank: true
  validates :sub_sale_rep, format: { with: /\A[A-Za-z]{1}[A-Za-z0-9\s\-\.]{2,}\z/, message: "invalid name" }, on: :create, if: -> { mighty.blank? && !mighty }
  validates_with BookingOverbookValidator, on: :create, if: -> { ignore_overbook.blank? }

  mount_uploader :passport_photocopy, PassportUploader

  # before_save :agent_confirmation_and_rebate, if: -> { update_later.blank? && mighty.blank? && !agent_confirmation }
  before_save :update_last_edited_user
  after_commit :calculate_tour_vacant_seats, if: -> { update_later.blank? }

  # after_update :update_booking_group_kiv, if: -> { saved_change_to_price? }

  def self.with_insurance_confirmation(boolean)
    if boolean == 'true'
      where("insurance_confirmation = true")
    elsif boolean == 'false'
      where("insurance_confirmation = false")
    elsif boolean == 'na'
      where("insurance_confirmation is null")
    else
      where('')
    end
  end

  def self.with_insurance_exported(boolean)
    if boolean == 'true'
      where("insurance_exported = true")
    elsif boolean == 'false'
      where("insurance_exported = false")
    else
      where('')
    end
  end

  def self.search_insurance_endorse_count(count)
    if count.present?
      if count.to_i == 0
        where("bookings.insurance_endorse_count = 0 or bookings.insurance_endorse_count is null")
      else
        where("bookings.insurance_endorse_count = ?", count)
      end
    else
      where('')
    end 
  end

  def self.with_insurance_include(boolean)
    if boolean == 'true' 
      where("bookings.policy_number is not null")  
    elsif boolean == 'false'
      where("bookings.policy_number is null") 
  else
      where('')
    end
  end

  def calculate_tour_vacant_seats
    tour.calculate_vacant_seats
  end

  def agent_confirmation_and_rebate
    self.agent_confirmation = true
    if self.name.present? && self.passport_number.present? && self.passport_expiry_date && self.passport_issuing_place && self.passport_photocopy && self.date_of_birth
      self.rebate = true
    end
  end

  def agent_rebatable
    if self.name.present? && self.passport_number.present? && self.passport_expiry_date && self.passport_issuing_place && self.passport_photocopy && self.date_of_birth
      "Yes"
    else
      "No"
    end
  end

  def price=(val)
    if val.present?
      write_attribute(:price, val.to_f)
    else
      write_attribute(:price, 0)
    end
  end

  def self.bulk_create(tour, booking_seats, user, agent_rep = nil, sub_sale_rep = nil, booking_params = [], booked_from = 'iceb2b')
    bookings = []
    if agent_rep == nil
      agent_user = user
      mighty = false
    else
      agent_user = User.find_by_username(agent_rep)
      mighty = true
    end

    in_fair = agent_user.fair_available?('', tour.itinerary_id)

    index = 0
    single_supplement = booking_seats.length == 1 && booking_seats.values.first.length == 1 && booking_seats.values.first.values.inject(:+) == 1
    ActiveRecord::Base.transaction do
      booking_seats.each do |type, detail|

        type_ = if type == ''
          type
        else
          type + '_'
        end

        bcr_type = if type.include?('single_')
          type.sub('single', 'single_twin')
        elsif type.include?('twin_')
          type.sub('twin', 'single_twin')
        else
          type
        end

        bcr = nil

        detail.each do |price_type, seats|
          (seats * Tour.type_pax(type)).times do |pax_index|
            if tour.is_cruise?
              if pax_index % Tour.type_pax(type) == 0
                bcr = BookingCruiseRoom.create!(room_price_type: "#{bcr_type}_#{price_type}", room_type: type, price_type: price_type)
              end
            end

            booking_category = if booking_params[index].present?
              booking_params[index][:category]
            else
              'adult'
            end

            collect_dta = user.collect_dta? ? 0 : tour.dta(price_type, booking_category)
            commission_percentage = user.collect_dta? ? 0 : user.commission_percentage("series_commission")
            commission = collect_dta * commission_percentage / 100
            total_price_after_dta = (tour.prices["#{type_}#{price_type}_price"] - tour.dta(price_type, booking_category)).to_f
            final_subtract_list = booked_from == "iceb2b" ? {} : tour.final_subtract("#{type_}#{price_type}_price", booking_category, user.collect_dta?, user, total_price_after_dta)

            bookings_setup_params = {
              tour: tour, 
              price: tour.final_price("#{type_}#{price_type}_price", booking_category, single_supplement, booked_from) - final_subtract_list.values.inject(:+).to_f,
              original_price: tour.send("#{type_}#{price_type}_price"),
              collect_dta: collect_dta,
              commission_percentage: commission_percentage,
              commission: commission,
              subtract_breakdown: final_subtract_list,
              price_type: price_type, 
              price_offered: price_type == 'normal' ? false : true,
              agent_user: agent_user, 
              sale_rep: agent_rep, 
              sub_sale_rep: sub_sale_rep, 
              initiator_user: user, 
              booking_cruise_room: bcr, 
              update_later: true,
              in_fair: in_fair,
              mighty: mighty
            }

            if booking_params[index].present?
              bookings << Booking.create!(booking_params[index].merge(bookings_setup_params))
            else
              bookings << Booking.create!(bookings_setup_params)
            end
            index += 1
          end
        end
      end
    end
    tour.calculate_vacant_seats
    tour.calculate_payment_seats
    group = BookingGroup.create(deposit: bookings.count * tour.deposit, booked_from: booked_from)

    bookings.each_with_index do |booking, index|
      booking.update_column :booking_group_id, group.id
    end

    group.calculate_gift_points

    bookings
  rescue ActiveRecord::RecordInvalid => exception
    tour.calculate_vacant_seats
    tour.calculate_payment_seats
    false
  end

  def self.first_book
    DateTime.new(2013,7,23,0,0,0)
  end

  def self.search_agent(keyword)
    if keyword.present?
      users_id = User.where("username ILIKE ? or alias_name ILIKE ? ", "%#{keyword}%", "%#{keyword}%").to_a
      where("bookings.initiator_user_id IN (?) or bookings.agent_user_id IN (?) or bookings.sale_rep ILIKE ?", users_id, users_id, "%#{keyword}%")
    else
      where('true')
    end
  end

  def self.search_summary_agent(agent_id)
    if agent_id.present?
      users = User.find(agent_id)
      where("bookings.initiator_user_id IN (?) or bookings.agent_user_id IN (?) or bookings.sale_rep ILIKE ?", agent_id, agent_id, "%#{users.username}%")
    else
      where('true')
    end
  end

  def booked_by_detail
    if agent_user.present?
      if agent_user == initiator_user
        agent_user.username
      else
        if initiator_user.present?
          "#{agent_user.username}(#{initiator_user.username})"
        else
          "#{agent_user.username}"
        end
      end
    elsif sale_rep.present?
      if initiator_user.present?
        "#{sale_rep}(#{initiator_user.username})"
      else
        "#{sale_rep}"
      end
    else
      ''
    end
  end

  def booked_by
    if agent_user.present?
      agent_user.username
    elsif sale_rep.present?
      sale_rep
    elsif initiator_user.present?
      initiator_user.username
    else
      ''
    end
  end

  def insurance_endorse
    if insurance_endorse_count.present?
      insurance_endorse_count
    elsif !insurance_endorse_count.present?
      insurance_endorse_count ="0"
    else
      ''
    end
  end

  def cruise_room_type
    if booking_cruise_room.present?
      booking_cruise_room.room_type_text
    else
      ''
    end
  end

  def export_price
    if exported_details.present?
      (self.tour_fee.to_f - self.DTA.to_f - self.PRE.to_f - self.visa_fee.to_f - self.KL_tax.to_f - self.insurance_fee.to_f).round(2)
    else
      price
    end
  end

  def name=(val)
    if val.blank?
      write_attribute(:name, nil)
    else
      write_attribute(:name, val.upcase)
    end
  end

  def self.search_agent(keyword)
    if keyword.present?
      users_id = User.where("username ILIKE ? or alias_name ILIKE ? ", "%#{keyword}%", "%#{keyword}%").to_a
      where("bookings.agent_user_id IN (?) or bookings.sale_rep ILIKE ?", users_id, "%#{keyword}%")
    else
      where('true')
    end
  end

  def update_booking_group_kiv
    booking_group.update_kiv
  end

  def charge!(datetime)
    unless charged
      if self.charged_at.blank?
        charged_at_arr = [datetime]
      else
        charged_at_arr = self.charged_at << datetime
      end
      self.update_columns(charged: true, charged_at: charged_at_arr)
    end
  end

  def refund!(datetime)
    if charged
      if self.refunded_at.blank?
        refunded_at_arr = [datetime]
      else
        refunded_at_arr = self.refunded_at << datetime
      end
      self.update_columns(charged: false, refunded_at: refunded_at_arr)
    end
  end

  def update_last_edited_user
    if self.edited_user_id.present? && self.changed?
      self.last_edited_user_id = edited_user_id
      self.last_edited_at = Time.now
    end
  end

  def insurance_available?
    self.created_at >= Booking::CHUBB_LAUNCH_DATE
  end

  def over_18?
    self.date_of_birth.present? && self.date_of_birth < self.tour.departure_date - 18.years
  end

  def policy_holder_available?
    self.over_18? || (self.guardian_id.present? && self.guardian_id != 0) || (self.ph_name.present? && self.ph_dob.present? && self.ph_dob < self.tour.departure_date - 18.years)
  end

  def passport_valid?
    self.passport_expiry_date.present? && self.passport_expiry_date > self.tour.departure_date + 195.days
  end

  def able_to_export?
    self.insurance_available? && self.insurance_confirmation == true && self.nationality == "malaysian" && self.designation.present? && self.mobile.present? && self.name.present? && self.passport_number.present? && self.passport_valid? && self.date_of_birth.present? && self.insurance_exported == false && self.policy_holder_available? && self.passport_photocopy.present? && self.room_type.present?
  end

  def export_error_message
    error = []
    if !self.insurance_available?
      error << "Insurance export is not available as for today's date."
    end
    if self.insurance_confirmation != true
      error << "Insurance export confirmation must be checked."
    end
    if self.insurance_exported != false
      error << "Insurance already exported."
    end
    if !self.name.present?
      error << "Guest's name must be present."
    end
    if self.nationality != "malaysian"
      error << "Guest's nationality must be Malaysian."
    end
    if !self.designation.present?
      error << "Guest's designation must be present."
    end
    if !self.mobile.present?
      error << "Guest's mobile number must be present."
    end
    if !self.passport_number.present?
      error << "Guest's passport number must be present."
    end
    if !self.passport_valid?
      error << "Guest's passport expiry date is not valid. The date must be present and valid 6 month from departure date."
    end
    if !self.date_of_birth.present?
      error << "Guest's date of birth must be present."
    end
    if !self.policy_holder_available?
      error << "Guardian information is not valid."
    end
    if !self.passport_photocopy.present?
      error << "Passport photocopy must be uploaded."
    end
    error
  end

  def policy_holder_detail
    if self.over_18?
      { name: self.name, dob: self.date_of_birth, designation: self.designation, passport_number: self.passport_number }
    elsif self.guardian_id.present? && self.guardian_id != 0
      policy_holder = Booking.find(self.guardian_id)
      { name: policy_holder.name, dob: policy_holder.date_of_birth, designation: policy_holder.designation, passport_number: policy_holder.passport_number }
    elsif self.ph_name.present?
      { name: self.ph_name, dob: self.ph_dob, designation: self.ph_designation, passport_number: ph_id_number }
    end
  end

  def export_insurance(user, status)
    policy_details = export_to_chubb(self, status)
    if policy_details.present? && policy_details != false
      self.exported_name = self.name
      self.exported_date_of_birth = self.date_of_birth
      self.exported_designation = self.designation
      self.exported_mobile = self.mobile
      self.exported_passport_number = self.passport_number
      self.exported_passport_expiry_date = self.passport_expiry_date
      self.exported_tour_code = self.tour.code
      self.exported_insurance_nomination_flag = self.insurance_nomination_flag
      self.exported_insurance_nomination = self.insurance_nomination

      if status == "new"
        new_policy_no = policy_details["policy_no"]
      else
        self.increment!(:insurance_endorse_count)
        
        if self.policy_number[-2] == "E"
          new_policy_no = self.policy_number.sub(self.policy_number.last(2), "E#{self.insurance_endorse_count}")
        else
          new_policy_no = "#{self.policy_number}E#{self.insurance_endorse_count}"
        end
      end

      self.update_columns(insurance_cert: policy_details["insurance_cert"], policy_number: new_policy_no, insurance_exporter_id: user.id, insurance_exported: true, insurance_exported_at: Time.now, insurance_export_remark: self.insurance_export_remark)
      true
    else
      false
    end
  end

  def check_user(user)
    user != "admin"
  end

  def able_to_endorse?
    ([:name, :date_of_birth, :mobile, :designation, :passport_number, :passport_expiry_date, :insurance_nomination_flag, :insurance_nomination].any? { |e| self.send(e) != self.send("exported_#{e}") } || (self.exported_tour_code != "" && self.tour.code != 
      self.exported_tour_code)) && self.insurance_exported == true
  end

  def all_insurance_purchased?
    self.infant_bookings.all?{|i| i[:insurance_exported] == true}
  end

  def partial_insurance_purchased?
    self.infant_bookings.any?{|i| i[:insurance_exported] == true}
  end

  def all_quantity
    qty = []
    self.booking_group.bookings.each do |booking|
      index = qty.find_index{|qty|qty[:category] == booking.category}
      if index.present?
        qty[index][:quantity] += 1
      else
        qty << { category: booking.category, quantity: 1 }
      end
    end
    qty.map{|v| "#{v[:quantity]} #{v[:category]}"}.join(', ')
  end

  def designation_name
    text = ""
    if self.designation.present? 
      text += self.designation + ". "
    end
    if self.name.present? 
      text += self.name
    end
    text
  end

  def construct_insurance_nomination
    if self.insurance_nomination.blank?
      self.insurance_nomination = []
      3.times do
        self.insurance_nomination << {
          "nominee_share": "",
          "nominee_surname": "",
          "nominee_given_name": "",
          "nominee_relationship": "",
          "nominee_date_of_birth": "",
          "nominee_passport_number": ""
        }
      end
    end
  end

  def price_type_display
    price_type.sub(/normal\d/, 'normal')
  end

end

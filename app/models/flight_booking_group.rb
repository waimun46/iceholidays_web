class FlightBookingGroup < ApplicationRecord
  has_many :flight_booking_details
  has_many :flight_bookings
  has_many :brb_bookings, through: :flight_bookings
  has_one :payment, as: :taggable
  has_many :etravel_invoices, as: :taggable

  enum payment_method: [:offline, :credits]

  scope :in_between, ->(inverval) { where(created_at:(inverval))}
  scope :search_code, ->(keyword) { where("flight_booking_groups.host_code ILIKE ?", "%#{keyword}%").distinct }
  scope :search_name, ->(keyword) { joins(:flight_bookings).where("flight_bookings.first_name ILIKE ? or flight_bookings.last_name ILIKE ?", "%#{keyword}%", "%#{keyword}%").distinct if keyword.present? }

  def reference_id
    self.id.to_s.rjust(10, '0')
  end

  def flight_detail_places
    ret = ""
    self.flight_booking_details.each do |flight|
      if ret.blank?
        ret += "#{flight.origin}->#{flight.destination}"
      else
        ret += " / #{flight.origin}->#{flight.destination}"
      end
    end
    ret
  end

  def agent_user
    if flight_bookings.present?
      flight_bookings.first.agent_user
    else
      nil
    end
  end

  def buyer_email
    if flight_bookings.first.agent_user.present?
      flight_bookings.first.agent_user.email
    else
      ""
    end
  end

  def product_name
    if flight_booking_details.present? 
      flight_detail_places
    else
      ""
    end
  end

  def booking_quantity
    if flight_bookings.present?
      flight_bookings.count
    else
      0
    end
  end 

  def product_description
    "Ice Holidays Flight Booking"
  end

  def send_notify
    agent = self.agent_user
    if agent.present?
      if agent.emails.present?
        FlightMailer.notify(agent, self, agent.emails.join(', ')).deliver_now
        if self.has_payment?
          FlightMailer.receipt(agent, self, agent.emails.join(', ')).deliver_now
        end
      end
    end
  end

  def has_payment?
    self.payment.paid? && self.payment.amount >= 1
  end

  def depart_flight_booking_details
    last_flight_connection = false
    self.flight_booking_details.to_a.delete_if do |flight|
      drop = last_flight_connection
      last_flight_connection = flight.connection
      drop
    end
  end

  def redirect_url
    Rails.application.routes.url_helpers.thankyou_flights_path(:flight_booking_group_id => self.id)
  end

  def show_url
    Rails.application.routes.url_helpers.flight_booking_group_path(:id => self.id)
  end

  def ref_no
    "BFLT#{self.id.to_s.rjust(9, '0')}"
  end
end

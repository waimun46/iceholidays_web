class GlobaltixTicket < ApplicationRecord
  enum markup_category: [:percentage, :fixed]
  enum status: [:pending, :active, :archived, :n_a]

  belongs_to :globaltix_attraction
  validates :ticket_id, uniqueness: true

  scope :countries, -> { distinct.pluck(:country) }
  scope :search_status, ->(status) { where("status = ?", GlobaltixTicket.statuses[status]) if status.present? }
  scope :search_keyword, ->(keyword) { where("country ILIKE :keyword OR title ILIKE :keyword", keyword: "%#{keyword}%")  if keyword.present? }
  scope :search_country, ->(keyword) { where("country ILIKE ?", keyword) if keyword.present? }
  scope :search_title, ->(keyword) { where("title ILIKE ?", "%#{keyword}%") if keyword.present? }
  scope :order_by_priority, -> { order("priority ASC") }

  has_many :covers, as: :taggable
  
  accepts_nested_attributes_for :covers, reject_if: proc { |attributes| attributes['image'].blank? }, allow_destroy: true

  typed_store :details, coder: PostgresCoder do |s|
    # s.string :title, default: ''
    s.string :name, default: ''
    s.string :currency, default: ''
    s.decimal :price, default: 0
    s.string :variation, default: ''
  end

  SGD_CONVERTED_TO_MYR = 3.0

  def final_price(minus_dta = false)
    price_in_myr
    if minus_dta
      price_in_myr
    else
      price_in_myr + self.dta
    end
  end

  def price_in_myr
    if currency == 'SGD'
      rate = ExchangeRate.find_by(from_currency: 'SGD', to_currency: 'MYR').try(:rate) || GlobaltixTicket::SGD_CONVERTED_TO_MYR
      myr_price = price * rate
      if markup.present?
        if self.percentage?
          myr_price = myr_price * (1 + markup / 100)
        elsif self.fixed?
          myr_price = myr_price + markup
        end
      end
      myr_price.ceil
    else
      nil
    end    
  end

  def dta
    if self.price_in_myr.present?
      (self.price_in_myr * (Setting.activity_dta_markup.to_f / 100.to_f)).round_to_half(2)
    else
      0
    end
  end

  # def dta_amount
  #   (self.final_price * (Setting.activity_dta_markup.to_f / 100.to_f)).round
  # end

  # def self.dta_min_price(tickets = [])
  #   (self.min_price(tickets) * (1 + Setting.activity_dta_markup.to_f / 100.to_f)).round
  # end

  # def self.min_price(tickets = [])
  #   tickets.map { |d| d.final_price }.min
  # end

  def markup_display
    if self.markup.present?
      if self.percentage?
        '+' + self.markup.to_s + '%'
      elsif self.fixed?
        '+' + self.markup.to_s
      else
        ''
      end
    else
      ''
    end
  end

  def ticket
    ticket, ticket_err = Globaltix::Ticket.find(self.ticket_id)
    ticket
  end

  def title_name
    "#{self.title}-#{self.name}"
  end

  def self.create_ticket(globaltix_ticket_id)
    @ticket, ticket_err = Globaltix::Ticket.find(globaltix_ticket_id)
    @globaltix_attraction = GlobaltixAttraction.find_by_attraction_id(@ticket.attraction.id)
    ticket = self.new(ticket_id: @ticket.id, globaltix_attraction_id: @globaltix_attraction.id, attraction_id: @ticket.attraction.id, title: @ticket.attraction.title, name: @ticket.name, variation: @ticket.variation, currency: @ticket.currency, price: @ticket.settlement_rate, country: @ticket.country, description: @ticket.description, terms_and_conditions: @ticket.terms_and_conditions)
    ticket.save
    ticket
  end

  def blockout_dates_text
    if blockout_dates.present?
      blockout_dates.map{|blockout_date| "#{blockout_date.begin.strftime("%d/%m/%Y")}-#{blockout_date.max.strftime("%d/%m/%Y")}"}.join(', ')
    else
      ""
    end
  end

  def blockout_dates_text=(val)
    dates_text = val.split(',')
    all_date = []
    dates_text.each do |date_text|
      date_text_start, date_text_end = date_text.split('-')

      date_start = Date.parse(date_text_start) rescue nil
      date_end = Date.parse(date_text_end) rescue nil

      if date_start.present? && date_end.present?
        all_date << Range.new(date_start, date_end)
      end
    end

    write_attribute(:blockout_dates,all_date)
  end

end
class GiftStock < ApplicationRecord
  belongs_to :gift
 
  validates :quantity, presence: true, numericality: { greater_than: 0 }

  after_commit :calculate_balance

  def calculate_balance
    gift.calculate_balance
  end
end

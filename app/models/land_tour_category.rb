class LandTourCategory < ApplicationRecord
  belongs_to :land_tour, inverse_of: :land_tour_categories
  has_many :land_tour_prices
  has_many :land_tour_booking_groups
  accepts_nested_attributes_for :land_tour_prices, allow_destroy: true

  validates :name, presence: true

  def final_price(category, depart_date, total_qty, foc = false, minus_dta = false)
    # total_qty is total adult
    self.land_tour_prices.select { |p| p.dates.map{|e| e.inject([]){|k, date| k << date}}.flatten.any?{|d|d == depart_date.to_date} }.map { |e| e.final_price(total_qty, category, foc, minus_dta) }.first
  end

  def dta(price, category, foc = false)
    if foc
      0
    else
      self.land_tour_prices.map { |e| e.dta(price, category) }.first
    end
  end
end
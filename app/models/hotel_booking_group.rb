class HotelBookingGroup < ApplicationRecord

  BOOKING_STATUSES = ["Success", "Cancelled"]

  has_one :cancelled_hotel_booking_group
  has_many :hotel_bookings
  has_one :payment, as: :taggable

  accepts_nested_attributes_for :hotel_bookings

  typed_store :hotel_details, coder: PostgresCoder do |s|
    s.string :hotel_name, default: ''
    s.string :hotel_address, default: ''
    s.string :room, default: ''
    s.string :currency, default: ''
    s.boolean :is_promotion, default: 0
    s.string :cancellation_policy, default: ''
    s.string :important_information, default: ''
    s.string :additional_information, default: ''
    s.datetime :cancel_upto, default: ''
    s.boolean :free_cancellation, default: 0
  end

  typed_store :customer_details, coder: PostgresCoder do |s|
    s.string :customer_name, default: ''
    s.string :customer_email, default: ''
  end

  def self.create_booking(params, user, hotel_name, room, cancel_upto, free_cancellation)
    booking, travellers = TravelPrologue::Booking.create(params, user)

    if booking.present? && booking.booking_status == "Success"
      hotel_booking_group = HotelBookingGroup.create(
        hotel_name: hotel_name, 
        hotel_address: params[:hotel_address], 
        room: room, 
        cancel_upto: cancel_upto.to_datetime, 
        free_cancellation: free_cancellation, 
        avail_token: params[:avail_token], 
        check_in: params[:check_in], 
        check_out: params[:check_out], 
        cancellation_policy: booking.cancellation_policy, 
        important_information: booking.important_information, 
        additional_information: booking.additional_information, 
        unique_txn_id: booking.unique_txn_id, 
        amount: booking.amount, 
        customer_name: booking.customer_name, 
        customer_email: travellers.first.email, 
        booking_status: booking.booking_status, 
        booking_id: booking.booking_id, 
        currency: booking.currency, 
        error_msg: booking.error_msg, 
        product_type: booking.product_type, 
        booking_ref: booking.booking_ref, 
        product_booking_id: booking.product_booking_id, 
        payment_id: booking.payment_id, 
        payment_type: booking.payment_type, 
        card_number: booking.card_number, 
        expiry_date: booking.expiry_date, 
        transaction_id: booking.transaction_id, 
        source_application: booking.source_application, 
        is_promotion: booking.is_promotion, 
        agent_id: booking.agent_id, 
        paymenttypemsg: booking.paymenttypemsg, 
        trans_pk: booking.trans_pk, 
        provider: booking.provider
      )

      travellers.each do |traveller|
        HotelBooking.create(hotel_booking_group: hotel_booking_group, salutation: traveller.salutation, first_name: traveller.first_name, last_name: traveller.last_name, email: traveller.email, phone_number: traveller.phone_number, country_code: traveller.country_code, traveller_type: traveller.traveller_type, in_room_no: traveller.in_room_no)
      end
    else
      Rails.logger.debug "Booking not successful"
    end

    [booking, hotel_booking_group]
  end

  def agent_user
    if hotel_bookings.present?
      hotel_bookings.first.agent_user
    else
      nil
    end
  end

  def product_description
    "Ice Holidays Hotel Booking"
  end

  def buyer_email
    if hotel_bookings.first.agent_user.present?
      hotel_bookings.first.agent_user.email
    else
      ""
    end
  end

  def cancelled!
    self.update(booking_status: BOOKING_STATUSES[1])
  end

  def send_notify
    # HotelMailer.send_booking_confirmation(self).deliver_now
    agent = self.agent_user
    if agent.present?
      agent.emails.each do |email|
        HotelMailer.notify(agent, self, email).deliver_now
        if self.has_payment?
          HotelMailer.receipt(agent, self, email).deliver_now
        end
      end
    end
  end

  def has_payment?
    self.payment.paid? && self.payment.amount >= 1
  end

  def redirect_url
    Rails.application.routes.url_helpers.admin_hotel_booking_groups_path
  end

  def show_url
    Rails.application.routes.url_helpers.hotels_path
  end

end

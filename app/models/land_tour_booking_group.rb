class LandTourBookingGroup < ApplicationRecord
  include LandTourValidator

  MonthMap = ("A".."M").to_a
  
  belongs_to :last_edited_user, class_name: "User", optional: true
  belongs_to :land_tour_category
  has_many :land_tour_bookings, -> { order("id") }
  has_many :payments, as: :taggable
  has_many :etravel_invoices, as: :taggable
  accepts_nested_attributes_for :land_tour_bookings, allow_destroy: true

  typed_store :price_breakdown, coder: PostgresCoder do |s|
    s.decimal :group_charges, default: 0
  end

  typed_store :flight_departure, coder: PostgresCoder do |d|
    d.datetime :flight_departure_date, default: ""
    d.string :departure_origin, default: ""
    d.string :departure_destination, default: ""
    d.string :departure_flight_no, default: ""
    d.datetime :departure_depart_time, default: ""
    d.datetime :departure_arrival_time, default: ""
    d.string :flight_departure_details, default: ""
  end

  typed_store :flight_return, coder: PostgresCoder do |a|
    a.datetime :flight_return_date, default: ""
    a.string :return_origin, default: ""
    a.string :return_destination, default: ""
    a.string :return_flight_no, default: ""
    a.datetime :return_departure_time, default: ""
    a.datetime :return_arrive_time, default: ""
    a.string :flight_return_details, default: ""
  end

  scope :visible, -> { includes(:land_tour_bookings).where.not(land_tour_bookings: {id: nil})} # include withdrawn bookings
  scope :with_booking, -> { includes(:land_tour_bookings).where.not(land_tour_bookings: {status: LandTourBooking.statuses[:withdrawn]}).distinct }
  scope :in_between, ->(interval) { where(created_at:(interval))}
  scope :in_between_departure_date, ->(interval) { where(departure_date:(interval))}
  scope :in_between_booked, ->(interval) {joins(:land_tour_bookings).where("land_tour_bookings.created_at" => interval).distinct if interval.present?}
  scope :search_name, ->(keyword) { joins(:land_tour_bookings).where("land_tour_bookings.name ILIKE ?", "%#{keyword}%").distinct if keyword.present? }

  validates_associated :land_tour_bookings
  validates_with FpxAmountValidator

  before_save :update_last_edited_user
  after_create :generate_code

  def self.search_agent(keyword)
    if keyword.present?
      users_id = User.where("username ILIKE ? or alias_name ILIKE ? ", "%#{keyword}%", "%#{keyword}%").to_a
      joins(:land_tour_bookings).where("land_tour_bookings.agent_user_id IN (?)", users_id)
    else
      where('true')
    end
  end

  def self.bulk_withdraw(bookings, user)
    booking_group = bookings.first.land_tour_booking_group
    bookings.each do |booking|
      booking.withdrawn_user_id = user.id
      booking.withdrawn_at = Time.now
      booking.withdrawn!
    end
    booking_group.update_deposit
    booking_group
  end

  def update_deposit
    if self.land_tour.present?
      if self.land_tour.deposit.present? && self.land_tour.deposit > 0
        if self.land_tour.deposit_type === "fixed"
          deposit = (self.deposit / self.land_tour_bookings.count) * self.land_tour_bookings.visible.count
        else
          new_price = self.land_tour_bookings.visible.map(&:price).inject(&:+).to_f
          deposit = new_price * (self.land_tour.deposit.to_f / 100.to_f)
        end
      else
        deposit = 0
      end
      update_column :deposit, deposit if self.deposit != deposit
    else
      update_column :deposit, 0 if self.deposit != 0
    end
  end

  def reference_id
    self.code
  end

  def ref_no
    "BLDT#{self.id.to_s.rjust(9, '0')}"
  end

  def generate_code
    if code.blank?
      update_column :code, "GA#{self.created_at.year.to_s[3]}#{MonthMap[self.created_at.month-1]}#{self.created_at.strftime("%d%H%M%S")}#{(self.id%100).to_s.rjust(2, '0')}"
    end
  end

  def cut_off?(depart_date)
    # return true if created_at > cut_off_date (full payment)
    self.created_at > depart_date.to_date - land_tour.cut_off_day.days
  end

  def editable?
    true
  end

  def confirmed?
    self.departure_date <= Time.now - 7.days
  end

  def land_tour
    if land_tour_bookings.present?
      land_tour_bookings.first.land_tour
    else
      nil
    end
  end

  def update_last_edited_user
    if self.last_edited_user_id.present? && self.changed?
      self.last_edited_user_id = last_edited_user_id
      self.last_edited_at = Time.now
    end
  end

  def redirect_url
    Rails.application.routes.url_helpers.thankyou_land_tour_land_tour_bookings_path(land_tour, :land_tour_booking_group_id => self.id)
  end

  def show_url
    Rails.application.routes.url_helpers.land_tour_booking_group_path(:id => self.id)
  end

  def agent_user
    if land_tour_bookings.present?
      land_tour_bookings.first.agent_user
    else
      nil
    end
  end

  def has_payment?
    self.payments.paid.present? && self.payments.paid.map(&:amount).inject(:+) >= 1
  end

  def send_notify(hotel = false)
    agent = self.agent_user
    if agent.present?
      if hotel
        LandTourMailer.notify(agent, self, agent.emails.join(', '), hotel).deliver_now
      else
        # Send notify on first time
        if self.payments.count <= 1
          LandTourMailer.notify(agent, self, agent.emails.join(', ')).deliver_now

          if self.land_tour.supplier_email.present?
            LandTourMailer.notify_supplier(agent, self, self.land_tour.supplier_email.join(', ')).deliver_now
          end
        end
        if !hotel && self.has_payment?
          LandTourMailer.receipt(agent, self, agent.emails.join(', ')).deliver_now
        end
      end
    end
  end

  def full_payment?
    self.payments.paid.present? && self.payments.paid.map(&:amount).inject(:+) >= self.total_price
  end

  def all_quantity
    qty = []
    self.land_tour_bookings.each do |booking|
      index = qty.find_index{|qty|qty[:category] == booking.category}
      if index.present?
        qty[index][:quantity] += 1
      else
        qty << { category: booking.category, quantity: 1 }
      end
    end
    qty.map{|v| "#{v[:quantity]} #{v[:category]}"}.join(', ')
  end

  def adult_count
    self.land_tour_bookings.select{|b| b.category == "adult" }.count
  end

  def child_count
    self.land_tour_bookings.select{|b| b.category != "adult" }.count
  end

  def foc_count
    self.land_tour_bookings.select{|b| b.free_of_charge == true }.count
  end

  def foc_booking
    self.land_tour_bookings.select{|b| b.free_of_charge == true }.first
  end

  def qty_without_foc
    category_mapping = {
      "adult" => "ADT",
      "child_with_bed" => "CHD(WITH BED)",
      "child_without_bed" => "CHD(NO BED)",
      "child_twin" => "CHD(TWIN)",
      "infant" => "IFT"
    }

    qty = []
    bookings = self.land_tour_bookings.select{|b| b.free_of_charge == false } # select non foc booking
    bookings.each do |booking|
      index = qty.find_index{|qty|qty[:category] == booking.category}
      if index.present?
        qty[index][:quantity] += 1
      else
        qty << { category: booking.category, quantity: 1, category_code: category_mapping[booking.category], price: booking.price - booking.pax_charges, pax_charges: booking.pax_charges }
      end
    end
    qty
  end

  def qty_overall
    self.qty_without_foc.map{|v| "#{v[:quantity]} #{v[:category_code]}"}.join(' + ')
  end

  def pax_charge_without_foc
    self.qty_without_foc.map{|b| [b[:pax_charges].to_f * b[:quantity]] }.flatten.sum
  end

  def return_date
    duration = land_tour.land_tour_days.map(&:day_no).max - 1
    self.departure_date + duration.days
  end

  def payment_status
    if self.land_tour_bookings.withdrawn.present?
      "Cancelled"
    elsif self.payments.pending.present?
      "Pending"
    elsif self.full_payment?
      "Paid"
    elsif self.has_payment?
      "Partial Paid"
    elsif self.payments.pay_later.present?
      "Awaiting"
    elsif self.payments.refunded.present?
      "Refunded"
    else
      ""
    end
  end

  def booking_status
    if self.land_tour_bookings.withdrawn.present?
      "Withdrawn"
    else
      "Active"
    end
  end

  def paid_amount
    if self.payments.paid.present?
      self.payments.paid.map(&:amount).inject(:+)
    else
      0
    end
  end

  def balance_amount
    self.total_price - self.paid_amount
  end

  def all_meals
    meals = []
    inclusion = self.land_tour.land_tour_days.map(&:inclusion)
    meals << { code: "B", category: "Breakfast", quantity: inclusion.select{|k,v| k[:inc_breakfast] == true}.count }
    meals << { code: "L", category: "Lunch", quantity: inclusion.select{|k,v| k[:inc_lunch] == true}.count }
    meals << { code: "D", category: "Dinner", quantity: inclusion.select{|k,v| k[:inc_dinner] == true}.count }
    meals
  end

  def meals
    self.all_meals.reject {|e| e[:quantity] == 0}.map{|v| "#{v[:quantity]} #{v[:category]}"}.join(', ')
  end

  def meal_summary
    self.all_meals.reject {|e| e[:quantity] == 0}.map{|v| "0#{v[:quantity]}#{v[:code]}"}.join(' + ')
  end

  def due_date
    (self.departure_date - self.land_tour.cut_off_day.days) - 2.days
  end

  def update_received_payment
    total_payment = self.payments.paid.map(&:amount).inject(:+)
    update_column :received_payment, total_payment if self.received_payment != total_payment
  end

  def rooms_text
    rooms.split(", ").reject{|r|r.include?("0")}
  end

  ['special_request'].each do |f|
    define_method "join_#{f}" do
      if self.send(f).present?
        self.send(f).join(',')
      else
        ''
      end
    end
  end

  def include_tipping?
    self.land_tour.compulsory_charges.map{|c| c["title"].downcase}.include?("tip")
  end

  def cut_off_date
    self.departure_date - self.land_tour.cut_off_day.days
  end
end
class PreCommision < ApplicationRecord
  enum commision_category: [:percentage, :fixed]

  scope :ongoing, -> { where("duration::tsrange @> ?::timestamp", Time.now) }
  scope :active, -> { where(active: true) }  
  scope :allow_user, ->(user_id) { where("? = ANY(allow_user_ids)", user_id) }
  scope :allow_itinerary, ->(itinerary_id) { where("? = ANY(allow_itinerary_ids)", itinerary_id) }
  
  validates :code, uniqueness: true
  validates :code, presence: true
  validates :duration, presence: true, if: :active?

  def duration_start
    if duration.present?
      duration.begin.in_time_zone
    else
      nil
    end
  end

  def duration_end
    if duration.present?
      duration.end.in_time_zone
    else
      nil
    end
  end

  def join_allow_users
    if self.allow_user_ids.present?
      User.where(id: self.allow_user_ids).map(&:username).join(', ')
    else
      ''
    end
  end

  def join_allow_itineraries
    if self.allow_itinerary_ids.present?
      Itinerary.where(id: self.allow_itinerary_ids).map(&:code).join(', ')
    else
      ''
    end
  end
end

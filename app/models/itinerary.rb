class Itinerary < ApplicationRecord
  ADMIN_ITINERARY_PER_PAGE = 50
  ITINERARIES_PER_PAGE = 20

  has_many :tours
  has_many :covers, as: :taggable
  belongs_to :department, optional: true
  accepts_nested_attributes_for :covers, reject_if: proc { |attributes| attributes['image'].blank? }, allow_destroy: true

  mount_uploader :file, ItineraryUploader
  mount_uploader :cn_file, ItineraryUploader
  mount_uploader :editable_file, ItineraryUploader
  mount_uploader :en_editable_file, ItineraryUploader
  mount_uploader :cn_editable_file, ItineraryUploader

  enum status: [:active, :archived]
  enum old_department_enum: [:western_and_exotic, :premium, :mainland_china, :asean, :east_asia, :g_trip, :cruise]

  validates :code, uniqueness: { case_sensitive: false }
  validates :code, presence: true
  validates :duration, format: { with: /[0-9]+[D][0-9]+[N]\z/, message: "is invalid format, must insert 'digitDdigitN' without space. Example: '6D5N'" }
  # validates :department_id, presence: true
  validates :description, presence: true
  validates :caption, presence: true
  validates :other_caption, presence: true
  validates :zone, presence: true
  validates :covers,  presence: { message: "image can't be blank" }{}
  validates :temperature_end, presence: { message: "can't be blank, when temperature start is Present" }, if: -> { temperature_start.present?}
  validates :temperature_start, presence: { message: "can't be blank, when temperature end is Present" }, if: -> { temperature_end.present?}

  scope :visible, -> { where("itineraries.status != ?", Itinerary.statuses[:archived]) }
  scope :search, ->(keyword) { where("itineraries.code ILIKE ? or itineraries.remark ILIKE ? or itineraries.category ILIKE ?", "%#{keyword}%", "%#{keyword}%", "%#{keyword}%") }
  scope :search_department_id, ->(department_id) { where("itineraries.department_id = ?", department_id) if department_id.present? }
  # scope :with_tours, -> (interval) { includes(:tours).references(:tours).where("tours.active is true and tours.vacant_seats_available = true").where(tours: {departure_date: interval}).where("cut_off_date > ?", Time.now).distinct if interval.present? }
  scope :order_by_departure, -> { order("tours.departure_date") }
  scope :order_by_code, -> { order("tours.code")}
  # scope :exclude_cruise, -> { where("tours.is_cruise = false") }
  # scope :exclude_grip, -> { where.not("tours.code ILIKE ?","FIT%") }
  # scope :exclude_cruise, -> { where.not("itineraries.department_id = ?", Department.find_by_name("Cruise").id ) }
  scope :exclude_cruise, -> { includes(:tours).references(:tours).where("itineraries.department_id != ? or (itineraries.department_id = ? and tours.category = ?)", Department.find_by_name("Cruise").id, Department.find_by_name("Cruise").id, Tour::categories[:GD_FLY_CRUISE] ) }
  scope :exclude_grip, -> { where.not("itineraries.department_id = ?", Department.find_by_name("G-Trip").id ) }
  scope :exclude_tour_code, -> { where.not("tours.code ILIKE ?","%#{Tour::EXCLUDING_TOUR_CODE}") }
  scope :with_tours, -> (interval) { includes(:tours).references(:tours).where("tours.active = true").where("tours.vacant_seats_available = true").where(tours: {departure_date: (interval)}).where("cut_off_date > ?", Time.now).distinct if interval.present? }
  scope :with_available_tours, -> (interval) { includes(:tours).references(:tours).where("tours.active = true").where("tours.vacant_seats_available = true").where(tours: {departure_date: (interval)}).where("cut_off_date > ?", Time.now).where("tours.cut_off_date > ? or ( tours.cut_off_date <= ? and tours.guaranteed_departure_flag is true and tours.guaranteed_departure_seats > 0 and tours.payment_seats >= tours.guaranteed_departure_seats)", Time.now + 3.days, Time.now + 3.days).distinct if interval.present? }
  scope :search_keyword, ->(keyword) { includes(:tours).where("itineraries.code ILIKE ? or tours.caption ILIKE ? or tours.other_caption ILIKE ? or tours.contient_country ILIKE ?", "%#{keyword}%","%#{keyword}%","%#{keyword}%", "%#{keyword}%") }
  scope :only_fs, -> { includes(:tours).references(:tours).where('tours.category': [:FS_3, :FS_4, :FS_5, :FS_3_4, :FS_3_5, :FS_4_5]) }
  scope :only_gd, -> { includes(:tours).references(:tours).where('tours.category': [:GD_Standard_5, :GD_Standard_4, :GD_Standard_3, :GD_Standard_4_3, :GD_Standard_4_5, :GD_Value_3, :GD_Value_3_4, :GD_Value_3_5]) }

  typed_store :including, coder: PostgresCoder do |d|
    d.boolean :including_airport_taxes, default: false
    d.boolean :group_departure, default: false
    d.boolean :including_tour_leader, default: false
    d.boolean :including_luggage, default: false
    d.boolean :including_wifi, default: false
    d.boolean :including_meal_onboard, default: false
    d.boolean :including_hotel, default: false
    d.boolean :including_gratuities, default: false
    d.boolean :including_acf, default: false
  end

  TOUR_STATUS_CATEGORY = [
    "Four Season, Western And Exotic",
    "Four Season, Mainland China",
    "Four Season, Asean",
    "Four Season, East Asia",
    "GD Standard, Western And Exotic",
    "GD Standard, Mainland China",
    "GD Standard, Asean",
    "GD Standard, East Asia",
    "Premium, All",
    "G-Trip, All",
    "Cruise, All"
  ]

    ZONE = [
    'Zone 1',
    'Zone 2',
    'Zone 3',
    'Domestic'
  ]

  def qrcode
    qrcode = RQRCode::QRCode.new(self.file.url)
  end

  def self.create_by_url(url)
    code = url.match(/\/([\w\-\+\%]+)\.(?:pdf|doc)/)
    if code.present? && code.length == 2
      code = code[1]
      code_i = 1
      while Itinerary.find_by_code(code).present?
        code = code + "(#{code_i})"
        code_i += 1
      end

      Itinerary.create(code: code, remote_file_url: url, url_source: url)
    end
  end

  def temperature_start
    if temperature.present?
      temperature.begin
    else
      nil
    end
  end

  def temperature_end
    if temperature.present?
      temperature.end
    else
      nil
    end
  end

  def join_countries
    if self.countries.present?
      self.countries.join(', ')
    else
      ''
    end
  end

  def country_name
    country = ISO3166::Country[self.countries]
    country.translations[I18n.locale.to_s] || country.name
  end

  def self.duplicate(itinerary, code)
    new_itinerary = itinerary.dup
    new_itinerary.code = code
    new_itinerary.file = itinerary.file
    if new_itinerary.save
      itinerary.covers.order(:id).each do |itinerary_cover|
        new_itinerary_cover = itinerary_cover.dup
        new_itinerary_cover.image = itinerary_cover.image
        new_itinerary_cover.taggable = new_itinerary
        new_itinerary_cover.save
        if Rails.env.iceb2b?
          new_itinerary.remote_file_url = itinerary.file.url
          new_itinerary.remote_cn_file_url = itinerary.cn_file.url
          new_itinerary.remote_editable_file_url = itinerary.editable_file.url
          new_itinerary.remote_en_editable_file_url = itinerary.en_editable_file.url
          new_itinerary.remote_cn_editable_file_url = itinerary.cn_editable_file.url
        end
      end
    end
    new_itinerary
  end

  def self.with_tour_status_category(category)
    if category == "Four Season, Western And Exotic"
      search_department_id(Department.find_by_name("western_and_exotic".titleize).id).only_fs  
    elsif category == "Four Season, Mainland China"
      search_department_id(Department.find_by_name("mainland_china".titleize).id).only_fs
    elsif category == "Four Season, Asean"
      search_department_id(Department.find_by_name("asean".titleize).id).only_fs
    elsif category == "Four Season, East Asia"
      search_department_id(Department.find_by_name("east_asia".titleize).id).only_fs
    elsif category == "GD Standard, Western And Exotic"
      search_department_id(Department.find_by_name("western_and_exotic".titleize).id).only_gd
    elsif category == "GD Standard, Mainland China"
      search_department_id(Department.find_by_name("mainland_china".titleize).id).only_gd
    elsif category == "GD Standard, Asean"
      search_department_id(Department.find_by_name("asean".titleize).id).only_gd
    elsif category == "GD Standard, East Asia"
      search_department_id(Department.find_by_name("east_asia".titleize).id).only_gd
    elsif category == "Premium, All"
      search_department_id(Department.find_by_name("premium".titleize).id).all
    elsif category == "G-Trip, All"
      search_department_id(Department.find_by_name("G-Trip").id).all
    elsif category =="Cruise, All"
      search_department_id(Department.find_by_name("cruise".titleize).id).all
    else
      where('')
    end
  end

  def self.tour_category_filter(keyword)
    if keyword.present?
      categories = Tour.categories.reject{|k, v| !k.include?(keyword)}
      includes(:tours).references(:tours).where(tours: {category:  categories.keys})
    else
      where('true')
    end
  end

  def caption_with_duration
    if self.duration.present?
      "#{self.duration} #{self.caption}"
   else
     ""
    end
  end

  def other_caption_with_duration
    if self.duration.present?
      "#{self.duration.sub('D', '天').sub('N', '晚')} #{self.other_caption}"
    else self.duration ==nil
     ""
    end
  end

  def other_caption_chinese
    if self.duration.present?
      "#{self.duration.sub('D', '天').sub('N', '晚')}"
    else self.duration ==nil
     ""
    end
  end

  def join_currency_english
    if self.currency_english.present?
      self.currency_english.join(', ')
    else
      ''
    end
  end

    
  ['currency_english', 'currency_chinese'].each do |f|
    define_method "join_#{f}" do
      if self.send(f).present?
        self.send(f).join(',')
      else
        ''
      end
    end
  end
   
  # def min_price(user) 
  #   [self.tours.map{|v|v.cheapest_normal_price.to_i}.min, self.tours.map{|v|v.cheapest_promo_price(user).to_i}.min].reject do |value| value == 0 end.min
  # end

end

class LandTourActivity < ApplicationRecord
  belongs_to :land_tour_day
  belongs_to :taggable, polymorphic: true, optional: true

  def get_taggable
    "#{self.taggable_type}".constantize.find(self.taggable_id)
  end
end
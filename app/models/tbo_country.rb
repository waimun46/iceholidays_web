class TBOCountry < ApplicationRecord
  self.primary_key = :code
  validates_presence_of :name, :code

  has_many :cities, class_name:'TBOCity', foreign_key: :country_code, primary_key: :code, dependent: :destroy

  scope :active, -> { where(is_active: true) }
end
class Receipt < ApplicationRecord
  # belongs_to :booking_group
  belongs_to :booking_group, optional: true

  validates :number, :amount, presence: true
  if !Rails.configuration.crawling
    validates :amount, numericality: { greater_than: 0 }
  end

  after_commit :calculate_tour_payment_seats, :update_booking_group_kiv_and_payment

  def calculate_tour_payment_seats
    if booking_group.tour.present?
      booking_group.tour.calculate_payment_seats
    end
  end

  def update_booking_group_kiv_and_payment
    if booking_group.present?
      booking_group.update_received_payment
      booking_group.update_kiv
    end
  end

end

class TourFlight < ApplicationRecord
  belongs_to :tour
  validates :from_to, format: { with: /[A-Z]+\-[A-Z]+\z/, message: "is invalid format, must insert '-' to seperate the airport iata. Example: 'KUL-SIN'" }

  def from
    if from_to.include? "-"
      from_to.split('-').first.strip
    # elsif from_to.include? "/"
    #   from_to.split('/').first.strip
    else
      ''
    end
  end

  def to
    if from_to.include? "-"
      from_to.split('-').last.strip
    # elsif from_to.include? "/"
    #   from_to.split('/').last.strip
    else
      ''
    end
  end

  def arrival_date
    departure = self.departure.strftime("%d-%m-%Y")
    arrival = self.arrival.strftime("%d-%m-%Y")
    if departure == arrival
      " "
    else 
      "+1"
    end
  end

  def airline
    Airline.find_by_iata_code(self.flight_no[0..1])
  end

  def departure_airport
    Airports.find_by_iata_code(self.from)
  end

  def arrival_airport
    Airports.find_by_iata_code(self.to)
  end

end

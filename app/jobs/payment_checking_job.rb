class PaymentCheckingJob < ApplicationJob
  queue_as :default

  def perform(payment)
    if payment.pending? && payment.fpx_b2c?
      payment.fpx_status
    end

    if payment.pending?
      payment.failed_and_action!
    end
  end
end

$(document).on("ready turbolinks:load", function() {
// $(document).ready(function() {

  // Listen for click on toggle checkbox
  $('#select-all-includings').click(function(event) {   
      if(this.checked) {
          // Iterate each checkbox
          $('.includings').each(function() {
              this.checked = true;
          });
      } else {
          $('.includings').each(function() {
              this.checked = false;                       
          });
      }
  });

  $(".btn-add-ticket").on('click',function(event) {
      event.preventDefault(); 
      var ticketId = $(this).data('id');
      console.log(ticketId)
      Rails.ajax({
          type: "POST",
          url: "/admin/globaltix_attractions/"+ ticketId +"/add_ticket",
          dataType: "json",
          success: function(response){
            if (response.notice) {
              // alert(response.notice)
              $('#btn-add-ticket-'+ticketId).val("Added");
              $('#btn-add-ticket-'+ticketId).addClass("disabled");
              $('#admin-new-ticket-modal').on('hidden.bs.modal', function () {
                location.reload();
              })

            } else {
              // alert(response.error)
            }
          }
      });
      return false;
  });

  if($('#tour_search').length > 0){
    $('#booking_alteration_alter_type').on('change', function() {
      if(this.value == 'withdrawn'){
        $('.btn-submit-alteration').val('Withdraw')
        $('.btn-submit-alteration').prop('disabled', false);
        $('#tour_search').prop('disabled', true)
        $('.withdrawn_reason_options').removeClass('hide');
        $('.withdrawn_reason_options').prop('disabled', false);
        $('.empty_reason_options').addClass('hide');
        $('.empty_reason_options').prop('disabled', true);
        $('.transferred_reason_options').addClass('hide');
        $('.transferred_reason_options').prop('disabled', true);
        $('.withdrawn_reason_options').trigger("change");
      }else if(this.value == 'transferred'){
        $('.btn-submit-alteration').val('Transfer');
        if($('#tour_id').val() == ""){
          $('.btn-submit-alteration').prop('disabled', true);
        } else{
          $('.btn-submit-alteration').prop('disabled', false);
        }
        $('#tour_search').prop('disabled', false)
        $('.transferred_reason_options').removeClass('hide');
        $('.transferred_reason_options').prop('disabled', false);
        $('.empty_reason_options').addClass('hide');
        $('.empty_reason_options').prop('disabled', true);
        $('.withdrawn_reason_options').addClass('hide');
        $('.withdrawn_reason_options').prop('disabled', true);
        $('.transferred_reason_options').trigger("change");
      } else{
        $('.btn-submit-alteration').val('Withdraw/Transfer')
        $('.btn-submit-alteration').prop('disabled', true);
        $('#tour_search').prop('disabled', true)
        $('.empty_reason_options').removeClass('hide');
        $('.empty_reason_options').prop('disabled', false);
        $('.transferred_reason_options').addClass('hide');
        $('.transferred_reason_options').prop('disabled', true);
        $('.withdrawn_reason_options').addClass('hide');
        $('.withdrawn_reason_options').prop('disabled', true);
        $('.reason_option-others').addClass('hide');
        $('.reason_option-others').prop('disabled', true);
      }
    });
    $('#booking_alteration_alter_type').trigger("change");

    $('.reason_options').on('change', function() {
      if(this.value == 'Others'){
        $('.reason_option-others').removeClass('hide');
        $('.reason_option-others').prop('disabled', false);
      } else {
        $('.reason_option-others').addClass('hide');
        $('.reason_option-others').prop('disabled', true);
      }
    });

    search_tour($("#tour_search"), function(){
      $('.btn-submit-alteration').prop('disabled', false);
    });

    $('#new_booking_alteration').on('submit', function(event){
      var ids = [];

      $('.check-checkbox:checked').map(function() {
        ids = ids.concat($(this).val().split(','))

      });
      $('#booking_ids').val(ids);

      if($('#booking_ids').val() != "")
      {
        if(confirm('Are you sure you want to ' + $("#booking_alteration_alter_type").val() +' selected?')){
          ;
        } else{
          event.preventDefault();
          return false;
        }
      }else{
        event.preventDefault();
        alert("Please select the " + $("#booking_alteration_alter_type").val() + " booking");
        return false;
      }
    });
  }

  $('#ground_booking_withdraw').on('submit', function(event){
    var ids = [];

    $('.check-checkbox:checked').map(function() {
      ids = ids.concat($(this).val().split(','))

    });
    $('#booking_ids').val(ids);

    if($('#booking_ids').val() != "")
    {
      if(confirm('Are you sure you want to withdraw selected booking(s)?')){
        ;
      } else{
        event.preventDefault();
        return false;
      }
    }else{
      event.preventDefault();
      alert("Please select the withdraw booking");
      return false;
    }
  });

  if($("#agent_rep_user").length > 0){
    $.get("/admin/search/user-tag.json", function(data){
      $("#agent_rep_user").typeahead({ 
        source: data,
        displayText: function(item){ return item.value;}
      });
    },'json');
  }

  if($("#country_tag").length > 0){
    tagsinput = $("#country_tag").tagsinput({
      typeahead: {
        source: $.get("/admin/search/country-tag.json"),
        afterSelect: function(item, element){
          this.$element.val('')
        }
      },
      freeInput: false
    });
    $.each(tagsinput, function(index, taginput){
      if(taginput.$element.hasClass('custom-form-control-tagsinput')){
        taginput.$container.addClass(taginput.$element.attr('class'));
        taginput.$container.css('display', 'block');
      } 
    });
  }

  if($("#nationality").length > 0){
    $('#nationality').typeahead({
      source: function(query, process){
        return $.get('/admin/search/country-tag.json', function (data){
          return process(data);
        })
      },
      afterSelect: function(item, element){
        this.$element.val(item.id);
      }
    });
  }

  if($("#tour_itinerary_code").length > 0){
    $.get("/admin/search/itinerary-tag.json", function(data){
      $("#tour_itinerary_code").typeahead({ 
        source: data,
        displayText: function(item){ return item.value;}
      });
    },'json');
  }

  if($("#tour-booking-form").length > 0){
    $('#tour-booking-form #booking_seats').on('change', function() {
      if($("#booking_seats").val().length > 0){
        $('#booking-create').prop('disabled', false);
      }else{
        $('#booking-create').prop('disabled', true);
      }
    });
  }

  if($("#duplicate-tour-form").length > 0){
    $('#duplicate-tour-form #departure_date_id').on('change', function() {
      departure_date = moment($("#departure_date_id").val()).format("YYMMDD")
      code = $("#code_id").val().replace(/(\w{2}\-)(\d{6})(\-)/, "$1"+departure_date+"$3")
      $("#code_id").val(code)
      if($("#departure_date_id").val().length > 0){
        $('#tour-duplicate').prop('disabled', false);
      }else{
        $('#tour-duplicate').prop('disabled', true);
      }
    });
  }

  if($("#fair_allow_user_ids").length > 0){
    tagsinput = $("#fair_allow_user_ids").tagsinput({
      itemValue: 'id',
      itemText: 'username',
      typeahead: {
        displayKey: 'text',
        source: function(query) {
          return $.get("/admin/search/user-tag.json?term="+query)
        },
        afterSelect: function(item, element){
          this.$element.val('')
        }
      }

    });
    $.each(tagsinput, function(index, taginput){
      if(taginput.$element.hasClass('custom-form-control-tagsinput')){
        taginput.$container.addClass(taginput.$element.attr('class'));
        taginput.$container.css('display', 'block');
      } 
    });
    if($("#fair_allow_user_ids").data('value') != ''){
      $.each($("#fair_allow_user_ids").data('value'), function(index, value){
        $("#fair_allow_user_ids").tagsinput('add', value);
      })
    }
  }

  if($('#fair_search').length > 0){
    $('#fair_search').typeahead({
      source: function(query, process){
        return $.get('/admin/search/fair-tag.json?term=' + query, function (data){
          return process(data);
        })
      },
      afterSelect: function(item, element){
        this.$element.parent().find('#tour_fair_id').val(item.id);
      }
    });
    $('#fair_search').change(function(){
      if($('#fair_search').val() == ''){
        $('#tour_fair_id').val('');
      }
    })
  }
  
  if($('.activities-empty').length > 0){
    $('.attraction-collapse').on('show.bs.collapse', function () {
      element = $(this).find('.activities-empty');
      
      if(element.length > 0)
      {

        $.ajax({
          url: element.data('url'),
          dataType: "json",
          type: 'GET',
          success: function(data){
            element.replaceWith(data.html)
          }
        });
      }
    });
  } 

  reminder_departure_form = $('.reminder-departure-form');
  reminder_departure = $('.reminder-departure-form .reminder-departure');

  if($(reminder_departure_form).length > 0){
    var depart_date = $(reminder_departure).val().split('T')[0];
    // Show total days on page load
    $('.day').each(function () {
      
      $('.reminder_date').each(function () {
        var day = $(this).parent().parent().find('.day');
        var reminder_date = $(this);

        var date1 = new Date(depart_date);
        var date2 = new Date(reminder_date.val());  
        var diff = date1 - date2;
        // get days
        var newday = diff/1000/60/60/24;

        day.val(newday);
      });
    });

    $(reminder_departure).change(function(){
      depart_date = $(reminder_departure).val().split('T')[0];

      $('.day').each(function () {
        var reminder_date = $(this).parent().parent().find('.reminder_date');
        var date = new Date(depart_date); // e.g. '2017-11-21'
        var newdate = new Date(date.getTime() - 24*60*60*1000 * $(this).val()); // days is the number of days you want to shift the date by

        reminder_date.val(convert(newdate));
      });
    });
  
    $(reminder_departure_form).on('change', '.reminder_date', function(){
      var day = $(this).parent().parent().find('.day');
      var reminder_date = $(this);

      var date1 = new Date(depart_date);
      var date2 = new Date(reminder_date.val());  
      var diff = date1 - date2;
      // get days
      var newday = diff/1000/60/60/24;

      day.val(newday);
    }); 

    $(reminder_departure_form).on('keyup', '.day', function(){
      var day = $(this);
      var reminder_date = $(this).parent().parent().find('.reminder_date');

      var date = new Date(depart_date); // e.g. '2017-11-21'
      var newdate = new Date(date.getTime() - 24*60*60*1000 * day.val()); // days is the number of days you want to shift the date by

      reminder_date.val(convert(newdate));
    });
  }

  // if($('input.autocomplete').length > 0){
  //   keywords = $('input.autocomplete').data('autocomplete')
  //   if(keywords.length > 0){
  //    $('input.autocomplete').typeahead({
  //      source: keywords
  //    }); 
  //   }
  // }

  // Multi Select JS for Price Type
  $('#price-type-multiselect').multiselect({
    includeSelectAllOption: true,
    buttonWidth: '100%',
    nonSelectedText: 'Select Price Type'
  });

  // Render Column Name into the List
    var columnName = [];
    $('#booking-groups-table thead th').each(function() {
      var list = $(this).text();
      columnName.push(list);
    });
    for (i = 1; i < columnName.length; i++) {
      var opt = document.createElement("option");
      document.getElementById("column-showhide-multiselect").innerHTML += '<option value="'+ columnName[i] + '">' + columnName[i] + '</option>';
  }

  $('#column-showhide-multiselect').multiselect({
    includeSelectAllOption: true,
    nonSelectedText: 'Show / Hide Column',
    buttonWidth: '100%',
    buttonText: function(options, select) {
                return 'Show / Hide Column';
            },
    onChange: function() { 
        var $tbl = $("#booking-groups-table");
        var $tblhead = $("#booking-groups-table thead th");
        
          $("#column-showhide-multiselect option").each(function(){
            var optionValue = $(this).val();             
              var colToHide = $tblhead.filter("[data-name='" + optionValue + "']");
              var index = $(colToHide).index();
            if($(this).is(':checked')){
              $tbl.find('tr :nth-child(' + (index + 1) + ')').show();
            } else {
              $tbl.find('tr :nth-child(' + (index + 1) + ')').hide();
            }
          });
       }
  }).multiselect('selectAll', false);

  $('#category-multiselect').multiselect({
    includeSelectAllOption: true,
    buttonWidth: '100%',
    nonSelectedText: 'Select Category'
  });


  if($('.pre-commision-form, .fair-form').length > 0){
    $('.iti_all').change(function(){
      var all_checkbox = $(this).parent().parent().find('input[type="checkbox"]');
      if(this.checked) {
        all_checkbox.prop("checked", true);
      }
      else {
        all_checkbox.prop("checked", false);
      }
    });
  }

  if($('.activate-hotel').length > 0){
    $('.activate-hotel').change(function(){
      var ele = $(this)
      var id = $(this).attr("id");
      
      if (this.checked) {
        var url = "/admin/tbo_hotels/"+ id +"/activate"
      }
      else {
        var url = "/admin/tbo_hotels/"+ id +"/deactivate"
      }

      Rails.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        success: function(response){
          if (response.notice) {
            console.log(response.notice)
            if (url.includes("de")){
              ele.parent().find(".active-text").text("Inactive")
            } else {
              ele.parent().find(".active-text").text("Active")
            }
          } else {
            alert(response.error)
          }
        }
      });
    });
  }

  if($('.active-all').length > 0){
    $('.active-all').change(function(){
      if (this.checked) {
        $(".activate-hotel").each(function() {
          $(this).prop("checked", true);
          $(this).change();
        });
      }
      else {
        $(".activate-hotel").each(function() {
          $(this).prop("checked", false);
          $(this).change();
        });
      }
    });
  }

  if ($(".land-tour-form").length > 0){
    $.get("/admin/cities", function(data){
      cities_fuse = new Fuse(data, {
        shouldSort: true,
        threshold: 0.3,
        location: 0,
        distance: 5,
        maxPatternLength: 32,
        minMatchCharLength: 1,
        keys: [
          "id",
          "name",
          "country"
        ]
      });
    },'json');
    init_location_autocomplete();

    $('.land-tour-day-fields').on('cocoon:after-insert', function() {  
      $('.land-tour-city-fields').on('cocoon:after-insert', function() {
        init_location_autocomplete();    
      });
    });

    $('.land-tour-city-fields').on('cocoon:after-insert', function() {
      init_location_autocomplete();    
    });

    $('.land-tour-category-fields').on('cocoon:after-insert', function() {
      $('.land-tour-date-fields').on('cocoon:after-insert', function() {
        initialize_tagsinput();
        land_tour_price_clone_field();    
      });
    });

    $('.land-tour-date-fields').on('cocoon:after-insert', function() {
      initialize_tagsinput();
      land_tour_price_clone_field();    
    });

    $('.land-tour-form').on('click', '.add-quantity, .edit-add-quantity', function(event){
      event.preventDefault();
      var ele = $(this).parent().parent().find('.quantity-field').last().clone(true);
      $(this).parent().parent().find('.quantity-field').last().after(ele);
      $(".remove-quantity, .edit-remove-quantity").show();

      land_tour_qty_clone_field($('.quantity-field'));
    });

    $('.land-tour-form').on('click', '.remove-quantity, .edit-remove-quantity', function(event){
      event.preventDefault();
      var length = $('.quantity-field').length;
      if (length > 1) {
        $(this).parent().remove();
        if (length == 2) {
          $(".remove-quantity, .edit-remove-quantity").hide();
        }

        $('.prices-field').each(function(i, obj) {
          var price_ele = $(this).find('.price-field').last().remove();
        });
      }
      else {
        $(".remove-quantity, .edit-remove-quantity").hide();
      }
    });

    $(".add-charge, .edit-add-charge").on('click', function(event){
      event.preventDefault();
      var ele = $(this).parent().parent().find('.charges-field').last().clone(true);
      $(this).parent().parent().find('.charges-field').last().after(ele);
      $(".remove-charge, .edit-remove-charge").show();
    });

    $(".remove-charge, .edit-remove-charge").on('click', function(event){
      event.preventDefault();
      var length = $(this).parent().parent().find('.charges-field').length;
      if (length > 1) {
        $(this).parent().remove();
        if (length == 2) {
          $(".remove-charge, .edit-remove-charge").hide();
        }
      }
      else {
        $(".remove-charge, .edit-remove-charge").hide();
      }
    });

    $('.land-tour-form').on('change', '.activity-select', function(){
      var value = $(this).val();
      var res = value.split(", ");
      $(this).parent().find('.taggable_id').val(res[0]);
      $(this).parent().find('.taggable_type').val(res[1]);
    })
  }

  if ($(".land-tour-form").length > 0){
    $(".add-contact, .edit-add-contact").on('click', function(event){
      event.preventDefault();
      var ele = $(this).parent().parent().find('.contacts-field').last().clone(true);
      $(this).parent().parent().find('.contacts-field').last().after(ele);
      $(".remove-contact, .edit-remove-contact").show();
    });

    $(".remove-contact, .edit-remove-contact").on('click', function(event){
      event.preventDefault();
      var length = $(this).parent().parent().find('.contacts-field').length;
      if (length > 1) {
        $(this).parent().remove();
        if (length == 2) {
          $(".remove-contact, .edit-remove-contact").hide();
        }
      }
      else {
        $(".remove-contact, .edit-remove-contact").hide();
      }
    });
  }
  
});

function init_location_autocomplete(){
  $(".location-autocomplete").typeahead({
    source: function(query, process){
      return process(cities_fuse.search(query));
    },
    matcher: function (item) {
      return true;
    },
    displayText: function(item) {
      return item.name + ', ' + item.country;
    },
    afterSelect: function(item, element){
      this.$element.parent().find('.city_id').val(item.id);
    }
  });
}

function search_tour(element, after_select_function)
{
  element.typeahead({
    source: function(query, process){
      exclude_id = this.$element.data('exclude')
      return $.get('/admin/search/tour-tag.json?term=' + query + '&exclude_id=' + exclude_id, function (data){
      // return $.get('/admin/search/tour-tag.json?term=' + query , function (data){
        return process(data);
      })
    },
    afterSelect: function(item, element){
      this.$element.parent().find('.tour_id').val(item.id);
      after_select_function();
    }
  });
}

// Convert Raw Date format to dd/mm/yyyy
function convert(str) {
  var date = new Date(str),
    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
    day = ("0" + date.getDate()).slice(-2);
  return [date.getFullYear(), mnth, day].join("-");
}

function gift_calculate_points(element) {
  total_points = 0;
  points = $(element).parent().parent().find('.gift_select').find(":selected").data('points');
  quantity = $(element).parent().parent().find('.gift_quantity').val();

  if (quantity > 0) {
    total_points = quantity * points;
  }
  else {
    total_points = points;
  }

  $(element).parent().parent().find('.gift_points').val(total_points);
}

function initialize_tagsinput() {
  $.each($("input[data-role=tagsinput]").tagsinput(), function(index, taginput){
    if(taginput.$element.hasClass('custom-form-control-tagsinput')){
      taginput.$container.addClass(taginput.$element.attr('class'));
      taginput.$container.css('display', 'block');
    }
  });
}

function land_tour_qty_clone_field(element) {
  var qty_length = element.length;

  $('.prices-field').each(function(i, obj) {
    var qty_ele = $(this).find('.price-field').length;
    if (qty_ele < qty_length) {
      var price_ele = $(this).find('.price-field').last().clone(true);
      $(this).find('.price-field').last().after(price_ele);
    }
  });
}

function land_tour_price_clone_field() {
  var qty_length = $('.quantity-field').length;

  $('.prices-field').each(function(i, obj) {
    var qty_ele = $(this).find('.price-field').length;
    var diff = qty_length - qty_ele;
    for (var j = 0; j < diff; j++) {
      var price_ele = $(this).find('.price-field').last().clone(true);
      $(this).find('.price-field').last().after(price_ele);
    }
  });
}

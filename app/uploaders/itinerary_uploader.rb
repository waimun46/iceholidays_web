class ItineraryUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  # include CarrierWave::MiniMagick


  def store_dir
    if Rails.env.iceb2b? || Rails.env.testb2b?
      "#{model.class.to_s.underscore}/#{model.id}/#{mounted_as}"
      # if mounted_as.to_s.include?("editable_file")
      #   # original editable store_dir
      #   # "#{model.class.to_s.underscore}/#{model.id}/edit" 
      #   "#{model.class.to_s.underscore}/#{model.id}/#{mounted_as}"
      # else
      #   # original store_dir
      #   # "#{model.class.to_s.underscore}/#{model.id}"
      #   "#{model.class.to_s.underscore}/#{model.id}/#{mounted_as}"
      # end
    else
      if mounted_as.to_s.include?("editable_file")
        "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}/edit"  
      else
        "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
      end
    end
  end

  def old_store_dir
    if Rails.env.iceb2b? || Rails.env.testb2b?
      if mounted_as.to_s.include?("editable_file")
        "#{model.class.to_s.underscore}/#{model.id}/edit" 
      else
        "#{model.class.to_s.underscore}/#{model.id}"
      end
    end
  end

  def extension_whitelist
    if mounted_as.to_s.include?("editable_file")
      %w(doc docx)
    else
      %w(jpg jpeg gif png pdf)
    end
  end

end

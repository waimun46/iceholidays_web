module BookingValidator
  class BookingOverbookValidator < ActiveModel::Validator
    def validate(record)

      if record.tour.is_cruise?
        room_price_type = record.booking_cruise_room.room_price_type
        merged_room_type = room_price_type.sub(/(#{Booking::PRICE_TYPES.map{|x| '_'+x}.join('|')})\z/, '')
        room_type = record.booking_cruise_room.room_type
        price_type = record.booking_cruise_room.price_type
        sold_seats = record.tour.booking_cruise_rooms.visible.group(:room_price_type).count

        if price_type == 'normal'
          # booking cruise room has been created already
          if ( sold_seats.values.present? && sold_seats.select{|key,value| key.include?(merged_room_type)}.values.inject(:+).to_i > record.tour.send("#{merged_room_type}_total_seats"))
            record.errors[:base] << "Booking for #{room_price_type.gsub('_', ' ')} seat is overbooking."
          end
        else
          # booking cruise room has been created already
          if price_type.include?('normal')
            current_fare_left_cabin = record.tour.max_seats.select do |seat_type, seats| 
              if Tour.type_pax(room_type) == 1
                seat_type_of_price_type = seat_type.sub('single_twin_', 'single_').seats_to_price
              elsif Tour.type_pax(room_type) == 2
                seat_type_of_price_type = seat_type.sub('single_twin_', 'twin_').seats_to_price
              else
                seat_type_of_price_type = seat_type.seats_to_price
              end
              (seat_type.start_with?("#{merged_room_type}_specialoffer") || seat_type.start_with?("#{merged_room_type}_normal")) && seats > 0 && record.tour.send(seat_type_of_price_type).present? && record.tour.send(seat_type_of_price_type) != 0 && record.original_price >= record.tour.send(seat_type_of_price_type)
            end.values.inject(:+) || 0
            specialoffer_normal_sold_cabins_values = sold_seats.select{|rmt, value| rmt.include?(merged_room_type) && Fair::FAIR_TYPES.none?{|fair_type| rmt.include?(fair_type)} }.values
          
            # booking cruise room has been created already
            if ( sold_seats.values.present? && sold_seats.select{|key,value| key.include?(merged_room_type)}.values.inject(:+).to_i > record.tour.send("#{merged_room_type}_total_seats")) || 
              ( sold_seats.values.blank? && 0 >= record.tour.send("#{merged_room_type}_total_seats")) || 
              (specialoffer_normal_sold_cabins_values.present? && specialoffer_normal_sold_cabins_values.inject(:+) > current_fare_left_cabin) ||
              (specialoffer_normal_sold_cabins_values.blank? && 0 > current_fare_left_cabin) ||
              (record.tour.send("#{room_type}_highest_sold_price").present? && !record.tour.send("#{room_type}_highest_sold_price").zero? && record.tour.send("#{room_type}_highest_sold_price") > record.original_price)
              record.errors[:base] << "Booking for #{room_price_type.gsub('_', ' ')} seat is overbooking."
            end
          else
            # booking cruise room has been created already
            if ( sold_seats.values.present? && sold_seats.select{|key,value| key.include?(merged_room_type)}.values.inject(:+).to_i > record.tour.send("#{merged_room_type}_total_seats")) || 
              ( sold_seats.values.blank? && 0 >= record.tour.send("#{merged_room_type}_total_seats")) || 
              (sold_seats[room_price_type].present? && sold_seats[room_price_type] > record.tour.send("#{room_price_type}_seats")) ||
              (sold_seats[room_price_type].blank? && 0 >= record.tour.send("#{room_price_type}_seats")) ||
              (price_type == "specialoffer" && record.tour.send("#{room_type}_highest_sold_price").present? && !record.tour.send("#{room_type}_highest_sold_price").zero? && record.tour.send("#{room_type}_highest_sold_price") > record.original_price)
              record.errors[:base] << "Booking for #{room_price_type.gsub('_', ' ')} seat is overbooking."
            end
          end
        end
        # booking hasn't been created yet
        if record.booking_cruise_room.bookings.visible.count >= Tour.type_pax(room_price_type)
          record.errors[:base] << "Booking for #{room_price_type.gsub('_', ' ')} seat is overbooking."
        end
      else
        if record.price_type == 'normal'
          # booking hasn't been created yet
          if record.tour.bookings.visible.count >= record.tour.total_seats
            record.errors[:base] << "Booking for #{record.price_type} seat is overbooking."
          end
        else
          sold_seats = record.tour.bookings.visible.group(:price_type).count
          # highest_sold_price = record.tour.bookings.visible.only_normal.maximum(:original_price)

          # booking hasn't been created yet
          if record.price_type.include?('normal')

            current_fare_left_seats = record.tour.max_seats.select do |seat_type, seats| 
              (seat_type.start_with?('specialoffer') || seat_type.start_with?('normal')) && seats > 0 && record.tour.send(seat_type.seats_to_price).present? && record.tour.send(seat_type.seats_to_price) != 0 && record.original_price >= record.tour.send(seat_type.seats_to_price)
            end.values.inject(:+) || 0
            specialoffer_normal_sold_seats_values = sold_seats.except(*Fair::FAIR_TYPES).values

            if ( sold_seats.values.present? && sold_seats.values.inject(:+) >= record.tour.total_seats) || 
              ( sold_seats.values.blank? && 0 >= record.tour.total_seats ) ||
              ( specialoffer_normal_sold_seats_values.present? && specialoffer_normal_sold_seats_values.inject(:+) >= current_fare_left_seats) || 
              ( specialoffer_normal_sold_seats_values.blank? && 0 >= current_fare_left_seats) ||
              ( record.tour.highest_sold_price.present? && !record.tour.highest_sold_price.zero? && record.tour.highest_sold_price > record.original_price )
              record.errors[:base] << "Booking for #{record.price_type} seat is overbooking."
            end
          else
            if ( sold_seats.values.present? && sold_seats.values.inject(:+) >= record.tour.total_seats) || 
              ( sold_seats.values.blank? && 0 >= record.tour.total_seats ) ||
              ( sold_seats[record.price_type].present? && sold_seats[record.price_type] >= record.tour.send("#{record.price_type}_seats")) ||
              ( sold_seats[record.price_type].blank? && 0 >= record.tour.send("#{record.price_type}_seats")) ||
              ( record.price_type == "specialoffer" && record.tour.highest_sold_price.present? && !record.tour.highest_sold_price.zero? && record.tour.highest_sold_price > record.original_price )
              record.errors[:base] << "Booking for #{record.price_type} seat is overbooking."
            end
          end
        end
      end
    end
  end

  class InfantDateOfBirthValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      if value.present?
        if value > Date.today + 1.day
          record.errors[attribute] << 'is invalid.'
        elsif value < record.booking.tour.departure_date - 2.years
          record.errors[attribute] << 'is over 2 years old.'
        end
      end
    end
  end

  class PolicyHolderDateOfBirthValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      if value.present?
        if value > record.booking_group.tour.departure_date - 18.years
          record.errors[attribute] << 'Guardian date of birth is under 18 years old.'
        end
      end
    end
  end

  class InfantPolicyHolderDateOfBirthValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      if value.present?
        if value > record.booking.tour.departure_date - 18.years
          record.errors[attribute] << 'Guardian date of birth is under 18 years old.'
        end
      end
    end
  end
end
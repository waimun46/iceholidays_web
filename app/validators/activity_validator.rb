module ActivityValidator
  class BookingCountValidator < ActiveModel::Validator
    def validate(record)
      if record.quantity > record.activity.max_booking
        record.errors[:base] << "Maximum booking quantity exceeded."
      elsif record.quantity < record.activity.min_booking
        record.errors[:base] << "Minimum booking quantity is not met."
      end
    end
  end

  class FpxAmountValidator < ActiveModel::Validator
    def validate(record)
      # if record.activity_booking_group.payment.gateway == "fpx"
        if record.price > 30000
          record.errors[:base] << "Maximum FPX amount payment exceeded."
        elsif record.price < 1
          record.errors[:base] << "Minimum FPX amount payment is not met."
        end
      # end
    end
  end
end
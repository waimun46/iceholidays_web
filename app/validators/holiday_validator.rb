module HolidayValidator
  class DateListValidator < ActiveModel::Validator
    def validate(record)
      if record.dates_preset.present?
        record.dates_preset.split("\n").each_with_index do |date, index|
          if !(/^(0?[1-9]|[12][0-9]|3[01])\s*(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s*$/i === date)
            record.errors[:base] << "\"#{date}\" is invalid date format. "
          elsif ((Date.parse("#{date} #{record.year}") rescue ArgumentError) == ArgumentError)
            record.errors[:base] << "\"#{date}\" is invalid date."
          end          
        end
      end
    end
  end
end
module LandTourValidator
  class PriceValidator < ActiveModel::Validator
    def validate(record)
      price = record.prices
      price.each do |qty, p|
        if (!(!!(qty =~ /[0-9]+\W[..][0-9]+\z/)))
        # if !(!!(qty =~ /[0-9]+[-][0-9]+\z/))
          record.errors[:base] << "Invalid quantity format, must insert 'digit-digit' without space."
        end
      end 
    end
  end

  class BookingCountValidator < ActiveModel::Validator
    def validate(record)
      if record.land_tour_bookings.count > record.land_tour.max_booking
        record.errors[:base] << "Maximum booking quantity exceeded."
      elsif record.land_tour_bookings.count < record.land_tour.min_booking
        record.errors[:base] << "Minimum booking quantity is not met."
      end
    end
  end

  class FpxAmountValidator < ActiveModel::Validator
    def validate(record)
      # if record.land_tour_booking_group.payment.gateway == "fpx"
        if record.total_price > 30000
          record.errors[:base] << "Maximum FPX amount payment exceeded."
        elsif record.total_price < 1
          record.errors[:base] << "Minimum FPX amount payment is not met."
        end
      # end
    end
  end
end
module GiftValidator
  class GiftQuantityValidator < ActiveModel::Validator
    def validate(record)
      gift = Gift.find(record.gift_id)
      if record.new_record? || record.gift_id_changed?
        if gift.dispatched_out >= gift.quantity
          record.errors[:base] << "Gift is exceed the quantity."
        end
      else
        if gift.dispatched_out > gift.quantity
          record.errors[:base] << "Gift is exceed the quantity."
        end
      end
    end
  end

  class CheckAgentValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      if value.present?
        if (record.gift_tracking_group.agent_id != record.booking_group.agent_user.id)
          record.errors[attribute] << 'ID does not belong to selected agent.'
        end
      end
    end
  end
end
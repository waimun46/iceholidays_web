module PaymentValidator
  class FpxAmountValidator < ActiveModel::Validator
    def validate(record)
      if record.amount > 30000
        record.errors[:base] << "Maximum FPX amount payment exceeded."
      elsif record.amount < 1
        record.errors[:base] << "Minimum FPX amount payment is not met."
      end
    end
  end
end
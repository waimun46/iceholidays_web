module TourValidator

  class TourPriceSeatsValidator < ActiveModel::Validator
    def validate(record)
      Tour::ALL_SEATS_TYPE.each_slice(Tour::SEATS_TYPES.count).each do |each_type|
        total_seats = record.send(each_type.shift)
        if total_seats.present?
          if total_seats < each_type.map{|type| record.send(type)}.select(&:present?).inject(:+).to_i
            record.errors[:base] << "Total seats are lesser than the sum total of all promo/offer/normal seats"
          end
        end
      end

      Tour::ALL_PRICE_TYPE.each_slice(Tour::PRICE_TYPES.count).each do |each_type|
        normal_price_type = each_type.shift
        normal_price = record.send(normal_price_type)
        if normal_price.present?
          previous_normalx_price = nil
          previous_normalx_price_type = nil

          each_type.each do |price_type|
            price = record.send(price_type)
            if price.present?
              # Check if the price is lower than normal
              if price > normal_price
                record.errors[price_type] << "is higher than #{normal_price_type.humanize(capitalize: false)}"
              end
              # Check if the normal n is higher than normal n+1 
              if price_type.include?('normal') && previous_normalx_price.present? && previous_normalx_price > price
                record.errors[price_type] << "is lower than #{previous_normalx_price_type.humanize(capitalize: false)}"
              end
              if price_type.include?('twin') && (record.send(price_type.sub('twin', 'single')).blank? || record.send(price_type.sub('twin', 'single')) <= 0)
                record.errors[price_type] << "is set but the #{price_type.sub('twin', 'single').humanize(capitalize: false)} is empty"
              end


              if price_type.include?('normal')
                previous_normalx_price = price
                previous_normalx_price_type = price_type
              end
            end
          end
        end
      end

      total_seats = 0
      Tour::ALL_SEATS_TYPE.select{|x| x.include?('total_seats')}.each do |seat_type|
        if record.send(seat_type) > 0
          total_seats += 1
          if seat_type.include?('single_twin')
            price_type = seat_type.sub('total_seats', 'normal_price')
            st_price_type = price_type.sub('single_twin', 'single')
            normal_price = record.send(st_price_type)
            if normal_price.blank? || normal_price <= 0
              record.errors[st_price_type] << "should greater than 0, if #{seat_type.humanize(capitalize: false)} isn't 0"
            end
            st_price_type = price_type.sub('single_twin', 'twin')
            normal_price = record.send(st_price_type)
            if normal_price.blank? || normal_price <= 0
              record.errors[st_price_type] << "should greater than 0, if #{seat_type.humanize(capitalize: false)} isn't 0"
            end
          else
            price_type = seat_type.sub('total_seats', 'normal_price')
            normal_price = record.send(price_type)
            if normal_price.blank? || normal_price <= 0
              record.errors[price_type] << "should greater than 0, if #{seat_type.humanize(capitalize: false)} isn't 0"
            end
          end
        end
      end

      if total_seats == 0
        record.errors[:base] << "There isn't any total seats in this tour."
      end
    end
  end

  class EarlyBirdDateValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      if value.present?
        if record.departure_date.present? && value > record.departure_date - 6.months
          record.errors[attribute] << 'is less than 6 months to departure date'
        end
      end
    end
  end

  # class TourPromoPriceValidator < ActiveModel::EachValidator
  #   def validate_each(record, attribute, value)
  #     if value.present? && record.normal_price.present? && value > record.normal_price
  #       record.errors[attribute] << 'is higher than normal price'
  #     end
  #   end
  # end

end
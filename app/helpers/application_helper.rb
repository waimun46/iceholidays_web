module ApplicationHelper
  def flash_class(level)
    case level
      when 'success' then "alert alert-success fade in"
      when 'notice' then "alert alert-success fade in"
      when 'alert' then "alert alert-danger fade in"
      when 'error' then "alert alert-danger fade in"
      when 'warning' then "alert alert-warning fade in"
    end
  end


  def render_pricing_available(tour, user)
    html = ""

    if true
      tour.available_vacant_seats_sorted(user).each_with_index do |(seats_type, details), seats_index|

        if seats_type != ""
          html += "#{seats_type.titleize}"
        end

        current_pax = 1
        price_by_type = if tour.is_cruise?
          'Cabin'
        else
          'Pax'
        end 

        html += content_tag(:table, class: 'table table-bordered table-condensed') do
          sub_html = content_tag(:thread) do

            content_tag(:tr) do
            sub1_html = content_tag(:th, 'Price Type', class: 'tour_column_head')

              details.each_with_index do |(price_type, seats_price), price_index|
                if current_pax > tour.max_booking_seats
                  break
                end
                sub1_html += content_tag(:th, class: 'tour_column_price') do
                  # sub2_html = "#{price_type.titleize} "
                  # sub2_html += content_tag(:span, "#{seats_price[:seats]}", class: "label label-#{price_type == 'normal' ? 'primary' : 'success'} lb-sm").html_safe
                  sub2_html = render_price_by_pax(current_pax, [seats_price[:seats], tour.max_booking_seats - current_pax + 1].min, price_by_type)
                  if !price_type.include?('normal')
                    sub2_html += " "
                    sub2_html += content_tag(:span, "#{price_type.titleize}", class: "label label-success lb-sm").html_safe
                  end
                  current_pax += [seats_price[:seats], tour.max_booking_seats - current_pax + 1].min
                  sub2_html.html_safe
                end
              end

              sub1_html.html_safe
            end
          end
          sub_html += content_tag(:tbody) do

            sub1_html = content_tag(:tr) do
              sub3_html = content_tag(:td, "Adult", class: 'tour_column_head')
              total_seats = 0
                details.each do |price_type, seats_price|

                  if total_seats >= tour.max_booking_seats
                    break
                  end

                  sub3_html += content_tag(:td, class: 'tour_column_price') do
                    content_tag(:div, title: tour.dta_price_text(price_type, "adult"), :data => {:toogle => 'tooltip', :placement => 'top'}) do
                      normal_html = content_tag(:span, "#{seats_price[:price]}", class: "label label-#{price_type.include?('normal') ? 'primary' : 'success' }").html_safe
                    end
                  end
                  total_seats += seats_price[:seats]
                end
              sub3_html.html_safe
            end
            if tour.child_price_available?
              Tour::CHILD_FARE_TYPES.each do |child_type|
                child_type = child_type.sub('_percentage', '').sub('_price', '')
                sub1_html += content_tag(:tr) do
                  sub3_html = content_tag(:td, child_type.titleize, class: 'tour_column_price')
                  total_seats = 0
                  details.each do |price_type, seats_price|

                    if total_seats >= tour.max_booking_seats
                      break
                    end

                    total_seats += seats_price[:seats]

                    seat_price_type = seats_type == "" ? "#{price_type}_price" : "#{seats_type}_#{price_type}_price"
                    price = tour.final_price(seat_price_type, child_type)
                    if child_type == "infant_price" && (price.blank? || price <= 0 )
                      sub3_html += content_tag(:td, "-")
                    else
                      sub3_html += content_tag(:td) do
                          sub5_html = content_tag(:div, title: tour.dta_price_text(price_type, child_type), :data => {:toogle => 'tooltip', :placement => 'top'}) do
                          child_html = content_tag(:span, "#{price}", class: "label label-danger").html_safe
                        end
                        sub5_html.html_safe
                      end
                    end
                  end
                  sub3_html.html_safe
                end
              end
            end
            sub1_html.html_safe

          end
          sub_html.html_safe

        end
        if tour.ground_price_flag && tour.guaranteed_departure_flag && (tour.bookings.visible.count > tour.guaranteed_departure_seats)
          html += tag(:br).html_safe
          html += "#{tour.ground_price}" + " " + content_tag(:span, "Ground", class: "label label-success").html_safe
        end
        html.html_safe
      end
    end
    html.html_safe
  end

  def render_price_by_pax(current_pax, max_seats, type = "Pax")
    if max_seats <= 1
      "#{current_pax.ordinalize} #{type}"
    else
      "#{current_pax.ordinalize} - #{(current_pax + max_seats - 1).ordinalize} #{type}"
    end
  end

  def render_booking_options(max_booking_seats, type, available_vacant_list)
    options = []
    term = if type == ''
      'seats'
    else
      'cabins'
    end
    for i in 1..max_booking_seats do
      ret = render_one_max_booking_seats(i, available_vacant_list)
      # {booking_params: booking_params, display_text: display_text, total_seats: total_seats, total_price: total_price}
      if ret[:total_seats] != i
        break
      end
      # [text, total_seats, total_price]
      options << ["#{pluralize(ret[:total_seats], term)}(#{ret[:total_seats]}) => #{ret[:display_text]}", ret[:booking_params], {data: {'total': ret[:total_price], 'seats': ret[:total_seats], 'pax': ret[:total_seats] * Tour.type_pax(type), type: type}}]
    end
    options_for_select(options)
  end

  def render_booking_selects(tour, user, max_seats = tour.max_booking_seats)
    html = ''
    if tour.is_cruise
      avs = tour.available_vacant_seats_sorted(user)
      avs.each do |type, value|
        if value.present?
          html += content_tag(:div, class: 'custom-form-group form-group col-sm-6', id: 'cruise_booking_selected', data: {max_booking_seats: max_seats, remote_path: cruise_options_tour_bookings_path(tour)}) do
            sub_html = label_tag "#{type.titleize}"
            sub_html += select_tag "booking_seats[#{type}]", render_booking_options(max_seats, type, value), {class: "form-control", include_blank: "", data: {type: type}}
            sub_html.html_safe
          end
        end
      end
    else
      html += content_tag(:div, class: 'custom-form-group form-group col-sm-12') do
        sub_html = label_tag 'Number of Seats'
        sub_html += select_tag :booking_seats, render_booking_options(max_seats, "", tour.available_vacant_seats(user)[""]), {class: "form-control"}
        sub_html.html_safe
      end
    end
    html.html_safe
  end

  def render_booking_group_payment(booking_group)
    paid_amount = booking_group.payments.paid.map(&:amount).inject(:+) || 0
    if paid_amount >= booking_group.price && booking_group.price != 0
      "Paid"
    elsif paid_amount >= 1
      "Partial Paid(#{number_to_currency(paid_amount, precision: 2, unit: 'RM ')})"
    elsif booking_group.payments.all?(&:failed?)
      "Failed"
    elsif booking_group.payments.all?(&:pending?)
      "Pending"
    elsif booking_group.payments.count == 1 && booking_group.payments.last.pay_later?
      "Pay Later"
    else
      ""
    end
  end

  def render_department_class(department)
    if  department.present?
      department.name.parameterize
    else
      ""
    end
      
  end

  def child_count
    total_child_count = @tour.bookings.child_twin.active.count + @tour.bookings.child_no_bed.active.count + @tour.bookings.child_with_bed.active.count
    if total_child_count == 0
      html = ""
    else
      html ="+ #{total_child_count} Child"
    end
  end

  def infant_count(resource, without_html = false)
    count = resource.infant_bookings.count
    html = ""
    if count > 0
      html += "#{count}"
      if without_html
        html += ""
      else
        html = "+ #{count} Infant"
      end
    end
  end

  def header_images
    if @tour.letter_head_image.present?
      image_tag @tour.letter_head_image.url, class: 'image-head'
    else
      image_tag("gd-letterhead.jpg", class: 'image-head')
    end
  end

  def promo_label(promo_type)
    if promo_type == 'early_bird'
      content_tag(:span, "EB", class: "label label-warning")
    elsif promo_type == 'specialoffer'
      content_tag(:span, "SO", class: "label label-primary")
    elsif promo_type == 'specialdeal'
      content_tag(:span, "SD", class: "label label-purple")
    elsif promo_type == 'superpromo'
      content_tag(:span, "SP", class: "label label-info")
    elsif promo_type == 'promo'
      content_tag(:span, "P", class: "label label-magenta")
    end
  end

  def render_status_text(tour, user, time = Time.now)
    
    html = ""

    if !tour.active?
      html = content_tag(:span, "Inactive", class: "label label-danger")
    elsif tour.vacant_seats_available?
      available_cheapest_promo_type = tour.cheapest_promo_type(user)
      if tour.first_activated_at.present? && tour.first_activated_at > (time - 5.days)
        html += content_tag(:span, "New", class: "label label-danger")
      end
      if available_cheapest_promo_type.present?
        html += promo_label(available_cheapest_promo_type)
      end
      if tour.max_booking_seats > tour.all_vacant_total_seats
        if html != ''
          html += " "
        end
        html += content_tag(:span, "L", class: "label label-warning")
      end
      if tour.guaranteed_departure_flag && tour.guaranteed_departure_seats.present? && tour.payment_seats >= tour.guaranteed_departure_seats
        if html != ''
          html += " "
        end
        html += content_tag(:span, "G", class: "label label-success")
      elsif tour.guaranteed_departure_flag && tour.guaranteed_departure_seats.present? && (tour.payment_seats + tour.max_booking_seats) >= tour.guaranteed_departure_seats
        if html != ''
          html += " "
        end

        html += content_tag(:span, "#{tour.guaranteed_departure_seats - tour.payment_seats} to G", class: 'label label-success')
      end
      if tour.ground_price_flag && tour.guaranteed_departure_flag && (tour.bookings.visible.count > tour.guaranteed_departure_seats)
        if html != ""
          html += " "
        end

        html += content_tag(:span, "GA", class: "label label-chocolate") + " "
      end
    else
      
      if tour.guaranteed_departure_flag && tour.guaranteed_departure_seats.present? && tour.payment_seats >= tour.guaranteed_departure_seats
        html += content_tag(:span, "G", class: "label label-success") + " "
      end
      
      html += content_tag(:span, "Sold Out", class: "label label-danger") + " "
      if tour.ground_price_flag && tour.guaranteed_departure_flag && (tour.bookings.visible.count > tour.guaranteed_departure_seats)
        if html != ""
          html += " "
        end

        html += content_tag(:span, "GA", class: "label label-chocolate") + " "
      end
    end

    html.html_safe
  end
  
 def render_tour_status_price_seat(tour, type, dta)
    if tour.send("#{type}_price").present? && tour.send("#{type}_price") > 0 && tour.send("#{type}_price") > 0 && (type == "normal" || (tour.vacant_seats["#{type}_seats"].present? && tour.vacant_seats["#{type}_seats"]  > 0))
      html = number_with_delimiter(tour.send("#{type}_price"))
      if type != "normal" && tour.send("#{type}_seats").present? && tour.vacant_seats["#{type}_seats"]  > 0
        html += tag(:br)
        html += " #{tour.vacant_seats["#{type}_seats"]}"
      end
      if dta && tour.send("dta_#{type}_adult").present? && tour.send("dta_#{type}_adult") > 0
        if type == "normal"
          html += tag(:br)
        end
        html += " (#{tour.send("dta_#{type}_adult")})"
      end
    else
      html = "-"
    end
    html.html_safe
  end

  def render_tours_flights_text(tours)
    flights_departures = []
    flights_summaries = []
    tours.each do |tour|
      if tour.tour_flights.present?
        flights_summary = tour.flights_summary
        found_index = flights_summaries.index(flights_summary)
        if found_index
          flights_departures[found_index] += " / #{tour.departure.strftime("%d %b %Y")}"
        else
          flights_summaries << flights_summary
          flights_departures << tour.departure.strftime("%d %b %Y")
        end
      end
    end
    html = ""
    flights_departures.each_with_index do |flight_departure, index|
      html += content_tag(:span ,flight_departure, class: 'underline-text')
      html += tag(:br)
      html += flights_summaries[index].gsub("\n", "<br>")
      html += tag(:br)
      html += tag(:br)
    end
    html.html_safe


    # itinerary.tours.map(&:flights_summary).uniq.join("\n\n").gsub("\n", "<br>").html_safe

  end

  def invoice_number(etravel)
    html = ""
    if etravel.taggable_type == "RoamingmanBookingGroup"
      html += link_to etravel.invoice_no, invoice_roamingman_booking_group_path(etravel.taggable_id), target: '_blank'
    elsif etravel.taggable_type == "GlobaltixTicketBookingGroup"
      html += link_to etravel.invoice_no, invoice_globaltix_ticket_booking_group_path(etravel.taggable_id), target: '_blank'
    elsif etravel.taggable_type == "ActivityBookingGroup"
      html += link_to etravel.invoice_no, invoice_activity_booking_group_path(etravel.taggable_id), target: '_blank'
    elsif etravel.taggable_type == "BookingGroup"
      html += link_to etravel.invoice_no, invoice_booking_group_path(etravel.taggable_id), target: '_blank'
    elsif etravel.taggable_type == "FlightBookingGroup"
      html += link_to etravel.invoice_no, invoice_flight_booking_group_path(etravel.taggable_id), target: '_blank'
    elsif etravel.taggable_type == "TBOBooking"
      html += link_to etravel.invoice_no, invoice_tbo_holidays_booking_path(etravel.taggable_id), target: '_blank'
    elsif etravel.taggable_type == "LandTourBookingGroup"
      html += link_to etravel.invoice_no, invoice_land_tour_booking_group_path(etravel.taggable_id), target: '_blank'
    else
      ""
    end
     html.html_safe
  end

  def category_link(etravel)
    html = ""
    if etravel.taggable_type == "RoamingmanBookingGroup"
      html += link_to "Roamingman" ,  admin_roamingman_booking_group_path(etravel.taggable_id), target: '_blank'
    elsif etravel.taggable_type == "GlobaltixTicketBookingGroup"
      html += link_to "Globaltix" ,  admin_globaltix_ticket_booking_group_path(etravel.taggable_id), target: '_blank'
    elsif etravel.taggable_type == "ActivityBookingGroup"
      html += link_to "Activity" ,  admin_activity_booking_group_path(etravel.taggable_id), target: '_blank'
    elsif etravel.taggable_type == "BookingGroup"
      html += link_to "Series",  admin_booking_group_path(etravel.taggable_id), target: '_blank'
    elsif etravel.taggable_type == "FlightBookingGroup"
      html += link_to "Flight",  admin_flight_booking_group_path(etravel.taggable_id), target: '_blank'
    elsif etravel.taggable_type == "TBOBooking"
      html += link_to "Hotel",   admin_tbo_booking_path(etravel.taggable_id), target: '_blank' 
    elsif etravel.taggable_type == "LandTourBookingGroup"
      html += link_to "Land Tour",   admin_land_tour_booking_group_path(etravel.taggable_id), target: '_blank'      
    else
      ""
    end
     html.html_safe
  end

  def render_tour_created_by(tour,user)
    if tour.created_user_id.present?
      html = user.username 
    else
      ''
    end
  end

  def render_brb_display(flight_booking)
    html = ""
    if flight_booking.brb_booking.present?
      html = "Yes"
      if flight_booking.brb_booking.brb_booking_group.service_number.present?
        html += " (Exported)"
      end
    else
      html = "No"
    end
    html
  end

  def render_itinerary_files(itinerary)
    html = ""
    if itinerary.file.present?
      html += link_to 'View version', @itinerary.file.url, target: '_blank'
    end
    if itinerary.cn_file.present?
      if html.present?
        html += " | "
      end
      html += link_to 'View version (CN)', @itinerary.cn_file.url, target: '_blank'
    end
    if itinerary.editable_file.present?
      if html.present?
        html += " | "
      end
      html += link_to 'Editable version', @itinerary.editable_file.url, target: '_blank'
    end
    if itinerary.en_editable_file.present?
      if html.present?
        html += " | "
      end
      html += link_to 'Other editable version (EN)', @itinerary.en_editable_file.url, target: '_blank'
    end
    if itinerary.cn_editable_file.present?
      if html.present?
        html += " | "
      end
      html += link_to 'Other editable version (CN)', @itinerary.cn_editable_file.url, target: '_blank'
    end

    html.html_safe
  end

  def render_itinerary_temperature(itinerary)
    if  itinerary.temperature.present?
      "#{itinerary.temperature.first} °C - #{itinerary.temperature.last} °C"
    else
     ""
    end
  end

  def render_insurance_icon(resource)
    bookings = if resource.class.name == "BookingGroup" || resource.class.name =="BookingAlteration"
      resource.bookings
    elsif resource.class.name == "Booking"
      [resource]
    end

    html = ""
    if bookings.present?
     count  = bookings.map(&:policy_number).compact.count
      if count > 0
        html = "#{count}#{content_tag(:span, '' , class: 'fa fa-shield fa-lg ')}"
      end
    end
    html.html_safe
  end

  def render_passport_icon(resource)
    bookings = if resource.class.name == "BookingGroup"
      resource.bookings
    elsif resource.class.name == "Booking"
      [resource]
    end
    
    html = ""
    if bookings.present?
      count = bookings.map(&:passport_photocopy_url).compact.count
      if count > 0
        html = "#{count}#{content_tag(:i, '', class: 'fa fa-id-card fa-lg ')}"
      end
    end
    html.html_safe
  end

  def render_blink_insurance(tour, user, time = Time.now)
    if tour.insurance_available?
      content_tag(:span, 'Free Travel Insurance', class: 'blink blink-insurance-confirm ').html_safe
    else
      ""
    end
  end

  def render_cheapest_promo(cheapest_promo)
    if cheapest_promo.present?
      render_price_color(cheapest_promo.values.last.values.last[:price], cheapest_promo.values.last.keys.first)
    else
      '-'
    end
  end

  def render_price_color(price, type)
    content_tag(:span, price, class: "#{type}-price")
  end

  def render_one_max_booking_seats(booking_seats, available_vacant_list)
    booking_params = ""
    display_text = ""
    total_seats = 0
    total_price = 0
    available_vacant_list.each do |k, v|
      if booking_params != ""
        booking_params += ", "
        display_text += ", "
      end
      if booking_seats <= v[:seats]
        booking_params += "#{booking_seats} #{k}(#{v[:price]})"
        if k.include?('normal')
          display_text += "#{booking_seats} x #{v[:price]}"
        else
          display_text += "#{booking_seats} x #{v[:price]} (#{k})"
        end
        total_seats += booking_seats
        total_price += booking_seats * v[:price]
        break
      else
        booking_params += "#{v[:seats]} #{k}(#{v[:price]})"
        if k.include?('normal')
          display_text += "#{v[:seats]} x #{v[:price]}"
        else
          display_text += "#{v[:seats]} x #{v[:price]} (#{k})"
        end

        booking_seats -= v[:seats]
        total_seats += v[:seats]
        total_price += v[:seats] * v[:price]
      end
    end
    {booking_params: booking_params, display_text: display_text, total_seats: total_seats, total_price: total_price}
  end

  def render_seats(tour, seats, type)
    if seats.present? && tour.vacant_seats.present? && tour.vacant_seats[type].present?
      "#{tour.vacant_seats[type]}"
    else
      "0"
    end
  end

  def render_seats_indicator(tour, seats, type)
    if seats.present? && tour.vacant_seats.present? && tour.vacant_seats[type].present?
      if tour.vacant_seats[type] >= 0
        content_tag(:span, "#{tour.vacant_seats[type]} available", class: 'label label-default custom-label')
      else
        content_tag(:span, "#{tour.vacant_seats[type].abs} overbooking", class: 'label label-default custom-label')
      end
    else
      ""
    end
  end



  def render_remark_infant_indicator(resource, without_html = false)
    count = resource.infant_bookings.count
    html = ""
    if count > 0
      html += "#{count}"
      if without_html
        html += " #{'infant'.pluralize(count)}"
      else
        html += content_tag(:i, '', class: 'fa fa-child')
      end
    end

    if resource.class.name == "BookingGroup"
      if resource.remark.present?
        html += " #{resource.remark}"
      end
      if resource.bookings.present?
        html += " #{resource.bookings.select{|b| b.active? && b.remark.present?}.map(&:remark).join(' | ')}"
      end
    elsif resource.class.name == "Booking" && resource.remark.present?
      html += " #{resource.remark}"
    end

    html.html_safe
  end

  def render_child_category(resource)
    html = ""
    count = resource.bookings.visible.where(category: ["child_twin", "child_with_bed", "child_no_bed"]).count
    if count == 0
      html += ""
    else
      html += "#{count}"
      html += content_tag(:i, '', class: 'fa fa-child', style: "color:red;")
    end
    html.html_safe
  end

  def render_cruise_seat_price_fields(tour, form = nil)
    html = ''
    
    content_tag(:div, class: 'panel-group', id: "accordion-cruise") do
      Tour::CRUISE_ROOM_TYPE.each do |key|
        html += content_tag(:div, class: 'panel panel-default') do
          sub_html = content_tag(:div, class: 'panel-heading') do
            content_tag(:p, class: 'panel-title') do
              link_to key.sub('single_twin', 'single / twin').titleize, "##{key}", class: "accordion-toggle", data: {toggle: "collapse", parent: '#accordion--cruise'}
            end
          end
          key_total_seats = if form.present?
            form.object.send(key+'_total_seats')
          else
            tour.send(key+'_total_seats')
          end
          sub_html += content_tag(:div, class: "panel-collapse collapse #{key_total_seats > 0 ? 'in' : '' }", id: key) do
            content_tag(:div, class: 'panel-body') do
              sub1_html = ''
              (0..Tour::SEATS_TYPES.count - 1).each do |i|
                sub1_html += content_tag(:div, class: 'row') do
                  sub2_html = content_tag(:div, class: 'custom-form-group form-group col-sm-6') do
                    if form.present?
                      sub3_html = form.label "#{key.sub('single_twin', 'single / twin')}_#{Tour::SEATS_TYPES[i]}"
                      sub3_html += form.number_field "#{key}_#{Tour::SEATS_TYPES[i]}", class: 'custom-form-control form-control'
                    else
                      sub3_html = label_tag "#{key.sub('single_twin', 'single / twin')}_#{Tour::SEATS_TYPES[i]}"
                      sub3_html += render_seats_indicator(tour, tour.send("#{key}_#{Tour::SEATS_TYPES[i]}"), "#{key}_#{Tour::SEATS_TYPES[i]}")
                      sub3_html += text_field_tag '', tour.send("#{key}_#{Tour::SEATS_TYPES[i]}"), class: 'custom-form-control form-control', disabled: true
                    end
                    sub3_html.html_safe
                  end
                  f_key = key
                  if key.include?('single_twin')
                    f_key = key.sub('single_twin', 'single')
                    sub2_html += content_tag(:div, class: 'custom-form-group form-group col-sm-6') do
                      if form.present?
                        sub3_html = form.label "#{f_key}_#{Tour::PRICE_TYPES[i]}"
                        sub3_html += form.number_field "#{f_key}_#{Tour::PRICE_TYPES[i]}", class: 'custom-form-control form-control'
                      else
                        sub3_html = label_tag "#{f_key}_#{Tour::PRICE_TYPES[i]}"
                        sub3_html += text_field_tag '', tour.send("#{f_key}_#{Tour::PRICE_TYPES[i]}"), class: 'custom-form-control form-control', disabled: true
                      end
                      sub3_html.html_safe
                    end
                    f_key = key.sub('single_twin', 'twin')
                  end
                  sub2_html += content_tag(:div, class: 'custom-form-group form-group col-sm-6') do
                    if form.present?
                      sub3_html = form.label "#{f_key}_#{Tour::PRICE_TYPES[i]}"
                      sub3_html += form.number_field "#{f_key}_#{Tour::PRICE_TYPES[i]}", class: 'custom-form-control form-control'
                    else
                      sub3_html = label_tag "#{f_key}_#{Tour::PRICE_TYPES[i]}"
                      sub3_html += text_field_tag '', tour.send("#{f_key}_#{Tour::PRICE_TYPES[i]}"), class: 'custom-form-control form-control', disabled: true
                    end
                    sub3_html.html_safe
                  end
                  sub2_html.html_safe
                end
              end
              sub1_html.html_safe
            end
          end
          sub_html.html_safe
        end
      end
      html.html_safe
    end
  end

  # def render_dta_fields(tour, form = nil)
  #   html = ''
  #   Tour::DTA_PRICE_TYPE.each do |dta_price_type|
  #     field_bg = if dta_price_type == 'dta_normal'
  #       'grey_fields'
  #     elsif dta_price_type == 'dta_specialdeal' || dta_price_type == 'dta_superpromo' || dta_price_type == 'dta_promo'
  #       'pink_fields'
  #     else
  #       ''
  #     end
  #     html += content_tag(:div, class: "row #{field_bg}") do
  #       sub_html = ''
        
  #       (Tour::ALL_FARE_TYPES - ['infant_price']).map{|price| "#{dta_price_type}_#{price}" }.each do |key|
  #         sub_html += content_tag(:div, class: 'custom-form-group form-group col-sm-3') do
  #           content_tag(:hr).html_safe
  #           sub3_html = ''
  #           if form.present?
  #             sub3_html += form.label "#{key}"
  #             sub3_html += form.number_field "#{key}", class: 'custom-form-control form-control'
  #           else
  #             sub3_html += label_tag "#{key}"
  #             sub3_html += text_field_tag '', tour.send(key), class: 'custom-form-control form-control', disabled: true
  #           end
  #           sub3_html.html_safe
  #         end
  #       end
  #       sub_html.html_safe
  #     end
  #   end
  #   html.html_safe
  # end

  def airline_image_url(airsegment)
    if airsegment.airline.present? && airsegment.airline.logo.present?
      airsegment.airline.logo_url
    else
      "http://pics.avs.io/200/200/#{airsegment.Carrier}@2x.png"
    end
  end

  def passenger_print(code, number)
    "#{passenger_code(code)} x #{number}"
  end

  def passenger_code(code)
    if code == 'ADT'
      'Adult'
    elsif code == 'CNN'
      'Child'
    elsif code == 'INF'
      'Infant'
    end
  end

  def render_travel_time(seconds)
    mm, ss = seconds.divmod(60)
    hh, mm = mm.divmod(60)
    dd, hh = hh.divmod(24)
    text = ""
    if dd > 0
      text +="#{dd}D "
    end
    if hh > 0
      text +="#{hh}H "
    end

    text += "#{mm}M"
    if ss > 0
      " #{ss}S"
    end
    text
  end

  def render_flight_title(type, solution)
    text = ""
    if type == "single_trip"
      text += "Flight from #{solution.journeys.first.departure_segment.origin_airport.iata} to #{solution.journeys.first.arrival_segment.destination_airport.iata}"
    elsif type == "round_trip"
      text += "Round trip from #{solution.journeys.first.departure_segment.origin_airport.iata} to #{solution.journeys.first.arrival_segment.destination_airport.iata}"
    else
      text += "Flight: #{solution.journeys.map(&:airports).join(', ')}"
    end
    text
  end

  def render_flight_dates(solution)
    "#{solution.journeys.first.departure_segment.departure_time.strftime("%b %d %Y")} - #{solution.journeys.last.arrival_segment.arrival_time.strftime("%b %d %Y")}"
  end

  def render_flight_passengers(solution)
    solution.air_pricing_infos.map{|air_pricing_info| "#{passenger_print(air_pricing_info.passenger_code, air_pricing_info.passenger_count)}"}.join(', ')
  end

  def render_gift_status_detail(gift_tracking_group)
    html = ''

    html += content_tag(:div, class: 'custom-form-group form-group col-sm-6') do
      if gift_tracking_group.status == "delivering"
        sub_html = label_tag :total_boxes
        sub_html += text_field_tag :total_boxes, gift_tracking_group.total_boxes,  class: 'custom-form-control form-control', disabled: true
        sub_html.html_safe
      elsif gift_tracking_group.status == "received"
        sub_html = label_tag :received_boxes
        sub_html += text_field_tag :received_boxes, gift_tracking_group.received_boxes,  class: 'custom-form-control form-control', disabled: true
        sub_html.html_safe
      elsif gift_tracking_group.status == "rejected"
        sub_html = label_tag :reason
        sub_html += text_field_tag :reason, gift_tracking_group.reason,  class: 'custom-form-control form-control', disabled: true
        sub_html.html_safe
      else
        sub_html = ""
        sub_html.html_safe
      end
    end
    html.html_safe
  end

  def render_insurance_view(resource)
    if resource.insurance_confirmation == true
      "Yes, please include with no additional charge"
    elsif resource.insurance_confirmation == false
      "No, I don't want to include insurance and understand the cost remain the same"
    else
      ""
    end
  end

  def render_insurance_available(tour, booking, page_type, insurance = nil)
    html = ""
    html += content_tag(:div, class: 'field custom-form-group form-group col-sm-6') do
      sub_html = label_tag :insurance_confirmation, 'Free Chubb Travel Insurance'
      sub_html += tag(:br)
      sub_html += content_tag(:small, '') do 
        details_html = content_tag(:i, 'Our packages bundle up with Chubb Executive travel insurance that is underwritten by Chubb Insurance Malaysia Berhad. Please choose your option below.')
        details_html += tag(:br)
        details_html += content_tag(:i) do
          link_html = "Click #{ link_to('here', asset_path('insurance_policy.pdf'), target: '_blank') } to view the benefits and policy wording"
          link_html += tag(:br)
          link_html.html_safe
        end 
        details_html.html_safe
      end

      if page_type == "show"
        sub_html += text_field_tag '', render_insurance_view(booking), class: 'custom-form-control form-control', disabled: true
      else
        if insurance == "exported_yes"
          sub_html += booking.radio_button(:insurance_confirmation, "true", {class: 'insurance-option', id: 'yes', disabled: true})
          sub_html += " Yes, please include with no additional charge"
          sub_html += tag(:br)
          sub_html += booking.radio_button(:insurance_confirmation, "false", {class: 'insurance-option', id: 'no', disabled: true})
          sub_html += " No, I don't want free insurance and understand the cost remain the same"
          sub_html += tag(:br)
          sub_html += content_tag(:small, '') do
            disclaimer_html = content_tag(:i, class: 'disclaimer') do 
              disclaimer_text = "Certificate will only be issued to the customer upon full payment and policy confirmation"
              disclaimer_text.html_safe
            end
            disclaimer_html.html_safe
          end
        else
          sub_html += booking.radio_button(:insurance_confirmation, "true", {class: 'insurance-option', id: 'yes', :disabled => booking.object.insurance_exported?})
          sub_html += " Yes, please include with no additional charge"
          sub_html += tag(:br)
          sub_html += booking.radio_button(:insurance_confirmation, "false", {class: 'insurance-option', id: 'no', :disabled => booking.object.insurance_exported?})
          sub_html += " No, I don't want free insurance and understand the cost remain the same"
          sub_html += tag(:br)
          sub_html += content_tag(:small, '') do
            disclaimer_html = content_tag(:i, class: 'disclaimer') do 
              disclaimer_text = "Certificate will only be issued to the customer after clicking the “Confirm Insurance“"
              disclaimer_text.html_safe
            end
            disclaimer_html.html_safe
          end
        end
      end
      sub_html.html_safe
    end
    html.html_safe
  end

  def render_gift_status_button(gift_tracking_group)
    html = ''

    if gift_tracking_group.status == "delivering"
      html += "#{ link_to('Gift Received', '', class: 'btn btn-success', :'data-toggle' => 'modal', :'data-target' => '#gift-receive-modal') }"
      html += "&nbsp;&nbsp;"
      html += "#{ link_to('Reject Gift', '', class: 'btn btn-danger', :'data-toggle' => 'modal', :'data-target' => '#gift-reject-modal') }"
    elsif gift_tracking_group.status == "rejected"
      html += "#{ link_to('Gift Received', '', class: 'btn btn-success', :'data-toggle' => 'modal', :'data-target' => '#gift-receive-modal') }"
    end
    html.html_safe  
  end

  def render_gift_changed_at(gift_tracking_group)
    html = ''
 
    if gift_tracking_group.status == "preparing"
      html += "#{gift_tracking_group.created_at.strftime("%d/%m/%Y %H:%M")}"
    else
      html += "#{gift_tracking_group.updated_at.strftime("%d/%m/%Y %H:%M")}"
    end
    html.html_safe
  end

  def render_gift_item(gift_tracking_group)
    html = ''

    title = []
    gift_tracking_group.gift_tracking_details.each do |details|
      title.push("#{details.gift.title} (#{details.quantity})")
    end

    html += title.join(", ")
    html.html_safe
  end

  def render_activity_tickets(tickets)
    if tickets.present?
      html = content_tag(:ul, class: 'list-group') do
        sub_html = ''
        tickets.each do |ticket|
          sub_html += content_tag(:li, class: 'list-group-item') do
            content_tag(:div, class: 'row') do
              sub2_html = content_tag(:div, class: 'col-xs-8') do
                sub3_html = content_tag(:p, ticket.variation)
                sub3_html += content_tag(:p, ticket.name)
                sub3_html += content_tag(:p, "Usual price: #{ticket.merchant_currency} #{ticket.original_price}")
                sub3_html.html_safe
              end
              sub2_html += content_tag(:div, class: 'col-xs-4 text-right') do
                sub3_html = content_tag(:p, "#{ticket.currency} #{ticket.price}")
                sub3_html += link_to('Select', activity_path(ticket.id), class: "btn btn-success btn-sm", target: '_blank')
                sub3_html.html_safe
              end
              sub2_html.html_safe
            end
          end
        end
        sub_html.html_safe
      end

      html.html_safe
    else
      "<div class='panel-body'><h4>No packages currently</h4></div>".html_safe
    end
  end

  def render_admin_activity_tickets(tickets)
    if tickets.present?
      html = content_tag(:ul, class: 'list-group') do
        sub_html = ''
        tickets.each do |ticket|
          sub_html += content_tag(:li, class: 'list-group-item') do
            content_tag(:div, class: 'row') do
              sub2_html = content_tag(:div, class: 'col-xs-8') do
                sub3_html = content_tag(:p, ticket.variation)
                sub3_html += content_tag(:p, ticket.name)
                sub3_html += content_tag(:p, "Usual price: #{ticket.merchant_currency} #{ticket.original_price}")
                sub3_html.html_safe
              end
              sub2_html += content_tag(:div, class: 'col-xs-4 text-right') do
                sub3_html = content_tag(:p, "#{ticket.currency} #{ticket.price}")
                sub3_html += link_to('Select', new_admin_globaltix_ticket_path(ticket_id: ticket.id), class: "btn btn-success btn-sm", target: '_blank')
                sub3_html.html_safe
              end
              sub2_html.html_safe
            end
          end
        end
        sub_html.html_safe
      end

      html.html_safe
    else
      "<div class='panel-body'><h4>No packages currently</h4></div>".html_safe
    end
  end

  def render_globaltix_question(question, params)
    if question.present?
      html = hidden_field_tag 'ticketTypes[]questionList[][id]', question.id
      html += content_tag(:div, class: 'custom-form-group form-group col-sm-12') do
        sub_html = label_tag question.question.upcase_first
        if question.type == "FREETEXT"
          sub_html += text_field_tag 'ticketTypes[]questionList[][answer]', params.present? ? params[:answer] : '', class: 'custom-form-control form-control', required: true
        elsif question.type == "OPTION"
          sub_html += select_tag 'ticketTypes[]questionList[][answer]', options_for_select(question.options.collect { |s| [s] }, selected: params.present? ? params[:answer] : ''), { include_blank: true, class: 'custom-form-control form-control', required: true }
        elsif question.type == "DATE"
          sub_html += date_field_tag 'ticketTypes[]questionList[][answer]', params.present? ? params[:answer] : '', { class: 'custom-form-control form-control', min: Date.current, required: true }
        end
        sub_html.html_safe
      end
      html.html_safe
    end
  end

  def render_roamingman_export_count(booking)
    html = ""
    html += "Roamingman export confirmation for:"
    html += tag(:br)

    i = 0
    if booking.present?
      booking.each do |booking|
        i = i+ 1
        html += "#{i}. #{booking.name}"
        html += tag(:br)
      end
    end

    html += tag(:br)
    html += "Confirm to proceed?"
    html += tag(:br)
    html += tag(:br)
    html.html_safe
  end
  
  def render_export_count(booking, infant, booking_group)
    html = ""
    html += "Insurance confirmation for:"
    html += tag(:br)

    i = 0
    if booking.present?
      booking.each do |booking|
        i = i+ 1
        html += "#{i}. #{booking.designation} #{booking.name}"
        html += tag(:br)
      end
    end

    if infant.present?
      infant.each do |infant|
        i = i+ 1
        html += "#{i}. #{infant.name}"
        html += tag(:br)
      end
    end

    non_booking = booking_group.non_export_count
    non_infant = booking_group.non_infant_count
    if non_booking.present? || non_infant.present?
      html += tag(:br)
      html += "Insurance confirmation is NOT available for:"
      html += tag(:br)
    end

    j = 0
    if non_booking.present?
      non_booking.each do |nb|
        j = j+ 1
        html += "#{j}. #{nb.designation} #{nb.name}"
        html += tag(:br)
        if nb.export_error_message.present?
          html += "&emsp;Reason :"
          html += tag(:br)
          k = 0
          nb.export_error_message.each do |error|
            k = k + 1
            html += "&emsp;#{k}. #{error}"
            html += tag(:br)
          end
        end
      end
    end

    if non_infant.present?
      non_infant.each do |nif|
        j = j+ 1
        html += "#{j}. #{nif.name}"
        html += tag(:br)
        if nif.export_error_message.present?
          html += "&emsp;Reason :"
          html += tag(:br)
          m = 0
          nif.export_error_message.each do |error|
            m = m + 1
            html += "&emsp;#{m}. #{error}"
            html += tag(:br)
          end
        end
      end
    end

    html += tag(:br)
    html += "Confirm to proceed?"
    html += tag(:br)
    html += tag(:br)
    html += "Please make sure all required fields are filled out correctly before you click the “Yes” button."
    if !booking_group.insurance_receiver.present?
      html += content_tag(:div, class: 'insurance-disclaimer') do
        sub_html = tag(:br)
        sub_html += "Please enter receiver's email address before you export."
        sub_html.html_safe
      end
    end
    html += tag(:br)
    html += tag(:br)
    html.html_safe
  end

  def render_guardian_fields(b, page_type, guardian = nil, insurance = nil)
    if page_type == "show" || (guardian == "guardian_yes" && page_type == "show") || (guardian == "guardian_yes" && page_type == "edit" && insurance == "exported_yes") || (guardian == "guardian_yes" && page_type == "edit")
      field_class = "guardian_details"
    else
      field_class = "guardian_none"
    end

    html = ""
    html += content_tag(:div, class: field_class) do
      guardian_html = content_tag(:div, class: 'field custom-form-group form-group col-sm-12') do
        sub_html = label_tag :policy_holder, 'Guardian'
        sub_html.html_safe
      end

      if page_type == "edit"
        if insurance == "exported_yes"
          guardian_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-2') do
            designation_html = label_tag :ph_designation, "Designation", class: 'required'
            designation_html += b.select :ph_designation, options_for_select(Booking::DESIGNATION, selected: b.object.ph_designation), {include_blank: true}, {class: 'custom-form-control form-control', disabled: true}
            designation_html.html_safe
          end  

          guardian_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-4') do
            name_html = label_tag :ph_name, "Full Name", class: 'required'
            name_html += b.text_field :ph_name, class: 'custom-form-control form-control', disabled: true
            name_html.html_safe
          end
          
          guardian_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-3') do
            dob_html = label_tag :ph_dob, "Date of Birth", class: 'required'
            dob_html += b.date_field :ph_dob, class: 'custom-form-control form-control', disabled: true
            dob_html.html_safe
          end

          guardian_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-3') do
            id_number_html = label_tag :ph_id_number, "Passport Number", class: 'required'
            id_number_html += b.text_field :ph_id_number, class: 'custom-form-control form-control', disabled: true
            id_number_html.html_safe
          end
        else
          guardian_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-2') do
            designation_html = label_tag :ph_designation, "Designation", class: 'required'
            designation_html += b.select :ph_designation, options_for_select(Booking::DESIGNATION, selected: b.object.ph_designation), {include_blank: true}, {class: 'custom-form-control form-control', :disabled => b.object.insurance_exported? }
            designation_html.html_safe
          end  

          guardian_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-4') do
            name_html = label_tag :ph_name, "Full Name", class: 'required'
            name_html += b.text_field :ph_name, class: 'custom-form-control form-control', :disabled => b.object.insurance_exported?
            name_html.html_safe
          end
          
          guardian_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-3') do
            dob_html = label_tag :ph_dob, "Date of Birth", class: 'required'
            dob_html += b.date_field :ph_dob, class: 'custom-form-control form-control', :disabled => b.object.insurance_exported?
            dob_html.html_safe
          end

          guardian_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-3') do
            id_number_html = label_tag :ph_id_number, "Passport Number", class: 'required'
            id_number_html += b.text_field :ph_id_number, class: 'custom-form-control form-control', :disabled => b.object.insurance_exported?
            id_number_html.html_safe
          end
        end
      else
        guardian_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-2') do
          designation_html = label_tag :ph_designation, "Designation"
          designation_html += text_field_tag '', b.ph_designation, class: 'custom-form-control form-control', disabled: true
          designation_html.html_safe
        end 

        guardian_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-4') do
          name_html = label_tag :ph_name, "Full Name"
          name_html += text_field_tag '', b.ph_name, class: 'custom-form-control form-control', disabled: true
        end

        guardian_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-3') do
          dob_html = label_tag :ph_dob, "Date of Birth"
          dob_html += text_field_tag '', b.ph_dob.try(:strftime, "%d/%m/%Y"), class: 'custom-form-control form-control', disabled: true
          dob_html.html_safe
        end

        guardian_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-3') do
          id_number_html = label_tag :ph_id_number, "Passport Number"
          id_number_html += text_field_tag '', b.ph_id_number, class: 'custom-form-control form-control', disabled: true
          id_number_html.html_safe
        end
      end

      guardian_html.html_safe
    end
    html.html_safe
  end

  def render_tour_code_calendar(tour)
    html = ""
    html += content_tag(:div, class: 'trim cut-off', title: "#{tour.code}") do
      if tour.active?
        code_html = "#{tour.insurance_purchased} #{ link_to(tour.code, admin_tour_path(tour), target: '_blank', class: 'active_tour') }"
      else
        code_html = "#{tour.insurance_purchased} #{ link_to(tour.code, admin_tour_path(tour), target: '_blank', class: 'inactive_tour') }"
      end
      code_html.html_safe
    end
    html.html_safe
  end

  def render_marvel_booking_date(activity)
    html = text_field_tag '', "#{activity.booking_date_start.try(:strftime, '%F %H:%M %p')} - #{activity.booking_date_end.try(:strftime, '%F %H:%M %p')}", class: 'custom-form-control form-control', disabled: true
    html.html_safe
  end

  def render_marvel_travelling_date(activity)
    html = text_field_tag '', "#{activity.travelling_date_start.try(:strftime, '%F %H:%M %p')} - #{activity.travelling_date_end.try(:strftime, '%F %H:%M %p')}", class: 'custom-form-control form-control', disabled: true
    html.html_safe
  end

  def render_tour_data(resource)
    html = ""
    if resource.bookings.first.tour.is_cruise?
      html += "Max Seats: #{resource.bookings.first.tour.all_total_seats } / (#{resource.bookings.first.tour.all_total_rooms }) | "
      html += "Total Booked: #{resource.bookings.first.tour.bookings.visible.count } / (#{resource.bookings.first.tour.booking_cruise_rooms.visible.count }) | "
      html += "Vacant Seats: #{ resource.bookings.first.tour.all_total_seats - resource.bookings.first.tour.bookings.visible.count } / (#{ resource.bookings.first.tour.all_total_rooms - resource.bookings.first.tour.booking_cruise_rooms.visible.count })"
    else
      html += "Max Seats: #{resource.bookings.first.tour.all_total_seats } | "
      html += "Total Booked: #{resource.bookings.first.tour.bookings.visible.count } | "
      html += "Vacant Seats: #{ resource.bookings.first.tour.all_total_seats - resource.bookings.first.tour.bookings.visible.count }"
    end
    html.html_safe
  end

  def render_room_combination_booking_btn(hotel,rooms, room_combination, booking_options, booking_params, hotel_markup, search_params)
    html  = ""    
    room_descriptions = []       
    no_of_rooms = room_combination.room_index.count
    nights, currency, meal_type, inclusion = nil     
    total_cost = 0
    selected_rooms = []
    
    room_combination.room_index.each do |idx|  
      room = rooms[idx.to_i - 1]      
      selected_rooms << room
      currency ||= room.room_rate.currency  
      nights ||= room.room_rate.day_rates.count
      meal_type ||= room.meal_type.try(:humanize) # put in supplement
      inclusion ||= room.inclusion.try(:humanize) # put in supplement

      total_cost +=  room.room_rate.total_fare.to_f
      room_description = "<a href='#' class='tbo-room-type-name-link'
                          data-room-name='#{room.room_type_name}'
                          data-meal-type='#{room.meal_type.try(:humanize)}'
                          data-inclusion='#{room.inclusion.try(:humanize)}'>#{room.room_type_name}</a>
                        "
      room_descriptions << room_description unless room_description.in?(room_descriptions)      
    end
    total_cost = TBOHotel.rate_with_markup(total_cost, hotel_markup, currency)
    html += "<td>#{room_descriptions.join(' | ')}</td>"
    html += "
      <td>
        #{number_to_currency(total_cost, precision: 2, unit: "MYR ") } (for #{no_of_rooms} rooms,    
        #{nights} nights)<br/><br/>
      </td>
    "     
    html += "<td></td>"

    booking_btn = form_tag(new_tbo_holidays_booking_path, method: :get) do      
      concat hidden_field_tag "search_criteria[checkin_checkout]", search_params[:checkin_checkout] 
      concat hidden_field_tag "search_criteria[city_id]", search_params[:city_id] 
      concat hidden_field_tag "search_criteria[country]", search_params[:country] 
      concat hidden_field_tag "search_criteria[country_code]", search_params[:country_code]
      concat hidden_field_tag "search_criteria[place]", search_params[:place]
      concat hidden_field_tag "search_criteria[rooms]", search_params[:rooms]
      concat hidden_field_tag "search_criteria[sort_by]", search_params[:sort_by]
      search_params[:room_adults].each do |room_adult|
        concat hidden_field_tag "search_criteria[room_adults][]", room_adult 
      end
      search_params[:room_childs].each do |room_child|
        concat hidden_field_tag "search_criteria[room_childs][]", room_child 
      end
      search_params[:childs_ages].each do |child_age|
        concat hidden_field_tag "search_criteria[childs_ages][]", child_age 
      end
      # search_params[:childs_beds].each do |childs_bed|
      #   concat hidden_field_tag "search_criteria[childs_beds][]", childs_bed 
      # end
      concat hidden_field_tag "booking_options[fixed_format]", booking_options.fixed_format
      room_combination.room_index.each do |idx|
        concat hidden_field_tag "booking_options[room_combination][]", idx
      end
      
      concat hidden_field_tag "booking_form[session_id]", booking_params[:session_id]
      concat hidden_field_tag "booking_form[hotel_code]", hotel.hotel_code
      concat hidden_field_tag "booking_form[hotel_name]", booking_params[:hotel_name]
      concat hidden_field_tag "booking_form[hotel_address]", hotel.address
      concat hidden_field_tag "booking_form[hotel_description]",  hotel.description
      concat hidden_field_tag "booking_form[check_in]", booking_params[:check_in]
      concat hidden_field_tag "booking_form[check_out]", booking_params[:check_out]
      concat hidden_field_tag "booking_form[guest_nationality]", booking_params[:guest_nationality]
      concat hidden_field_tag "booking_form[no_of_rooms]", no_of_rooms
      concat hidden_field_tag "booking_form[result_index]", booking_params[:result_index]
      concat hidden_field_tag "booking_form[total_cost]", total_cost

      selected_rooms.each_with_index do |room, idx|
        concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][room_index]", room.room_index
        concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][room_type_name]", room.room_type_name
        concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][room_type_code]", room.room_type_code
        concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][rate_plan_code]", room.rate_plan_code
        concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][room_rate_attributes][room_fare]", room.room_rate.room_fare
        concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][room_rate_attributes][agent_mark_up]", room.room_rate.agent_mark_up
        concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][room_rate_attributes][currency]", room.room_rate.currency
        concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][room_rate_attributes][pref_currency]", room.room_rate.pref_currency
        concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][room_rate_attributes][room_tax]", room.room_rate.room_tax
        concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][room_rate_attributes][total_fare]", room.room_rate.total_fare 
        concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][room_rate_attributes][pref_price]", room.room_rate.pref_price
        concat "<div class='col-md-12'><b>Room ##{idx+1}</b></div>".html_safe
        concat "<div class='col-md-12'>#{room.room_promtion}</div>".html_safe
        concat "<div class='col-md-12'><b>Meal type</b>: #{room.meal_type.try(:humanize)}</div>".html_safe
        concat "<div class='col-md-12'><b>Inclusion</b>: #{room.inclusion.try(:humanize)}</div>".html_safe
        concat "<div class='col-md-12'>Room ##{idx+1} Supplements: <br>".html_safe
        (room.supplements || []).each_with_index do |supp,sidx|
          concat label_tag "","#{supp.supp_name} - #{supp.currency_code} #{supp.price}"       
          concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][supplements_attributes][#{sidx}][supp_id]", supp.supp_id
          concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][supplements_attributes][#{sidx}][price]", supp.price
          concat check_box_tag "booking_form[hotel_rooms_attributes][#{idx}][supplements_attributes][#{sidx}][supp_is_selected]", true, true,  disabled: true          
          concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][supplements_attributes][#{sidx}][supp_is_mandatory]", supp.supp_is_mandatory
          concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][supplements_attributes][#{sidx}][supp_charge_type]", supp.supp_charge_type
          concat hidden_field_tag "booking_form[hotel_rooms_attributes][#{idx}][supplements_attributes][#{sidx}][currency_code]", supp.currency_code
        end
        concat "</div>".html_safe
      end
      concat "<div class='col-md-12'>".html_safe
      concat submit_tag 'Book Now', class: 'btn btn-success'
      concat "</div>".html_safe
    end

    html += "<td>#{booking_btn}</td>"
    html.html_safe  
  end

  def render_room_cancellation_policies(cancellation_policies)
    html  = ""    
    if cancellation_policies.present? 
      cancellation_policies.hotel_norms.each do |norm| 
        html += norm.html_safe
        html += "<br>"
      end         

      html += "#{cancellation_policies.cancel_policies.default_policy}<br>"
      html += "Last cancellation deadline: " 
      html += cancellation_policies.cancel_policies.last_cancellation_deadline.strftime("%Y-%m-%d") 
      html += "<br>"
      cancellation_policies.cancel_policies.cancel_policy.group_by{|x| x.room_index}.each_with_index do |cp_by_room|         
        html += "Room ##{cp_by_room[0]} "
        cp_by_room[1].each do |cp|
          html += "- #{cp.room_type_name} "
          html += "From: #{cp.from_date} "
          html += "To: #{cp.to_date} "
          html += "Charge: "
          html += if cp.charge_type === "Percentage"
            if cp.pref_cancellation_charge
              "#{cp.pref_cancellation_charge}% of total cost"
            else
              "#{cp.cancellation_charge}% of total cost"
            end
          else
            if cp.pref_cancellation_charge
              number_to_currency(cp.pref_cancellation_charge, precision: 0, unit: cp.pref_currency + " ") 
            else
              number_to_currency(cp.cancellation_charge, precision: 0, unit: cp.currency + " ")
            end
          end
          html += "<br>"
        end
      end
    end
    html.html_safe
  end

  def render_nominee_section(b, age_type)
    if b.object.insurance_nomination_flag || (age_type == "18_no")
      field_class = "nominee_none"
    else
      field_class = "nominee_details"
    end

    html = ""
    html += content_tag(:div, class: 'field custom-form-group form-group col-sm-12 nominee-section') do
      sub_html = b.check_box :insurance_nomination_flag, {class: 'nominee_confirm'}, true
      sub_html += " Note: Death benefit will be paid to insured Person's estate. Please uncheck if the insured person wishes to make a NOMINEE/NOMINEES."
      sub_html.html_safe
    end

    if b.object.id.present? 
      id = b.object.id
    else
      id = b.object.booking.infant_bookings.count
    end
    
    html += content_tag(:div, class: field_class) do
      details_html = ""
      for i in 0..2
        details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-12') do
          label = label_tag :nominee_details, "Nominee Details #{i+1}"
          label.html_safe
        end

        details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-2') do
          surname_html = label_tag :nominee_surname, "Surname"
          surname_html += text_field_tag "nominee_details[#{id}][nominee_surname][]", b.object.insurance_nomination[i].present? ? b.object.insurance_nomination[i]["nominee_surname"] : '', class: 'custom-form-control form-control'
          surname_html.html_safe
        end  

        details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-2') do
          given_name_html = label_tag :nominee_given_name, "Given Name"
          given_name_html += text_field_tag "nominee_details[#{id}][nominee_given_name][]", b.object.insurance_nomination[i].present? ? b.object.insurance_nomination[i]["nominee_given_name"] : '', class: 'custom-form-control form-control'
          given_name_html.html_safe
        end

        details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-3') do
          passport_num_html = label_tag :nominee_passport_number, "Passport/NRIC Number"
          passport_num_html += text_field_tag "nominee_details[#{id}][nominee_passport_number][]", b.object.insurance_nomination[i].present? ? b.object.insurance_nomination[i]["nominee_passport_number"] : '', class: 'custom-form-control form-control'
          passport_num_html.html_safe
        end
        
        details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-2') do
          dob_html = label_tag :nominee_date_of_birth, "Date of Birth"
          dob_html += date_field_tag "nominee_details[#{id}][nominee_date_of_birth][]", b.object.insurance_nomination[i].present? ? b.object.insurance_nomination[i]["nominee_date_of_birth"] : '', class: 'custom-form-control form-control'
          dob_html.html_safe
        end

        details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-2') do
          relationship_html = label_tag :nominee_relationship, "Relationship"
          relationship_html += select_tag "nominee_details[#{id}][nominee_relationship][]", options_for_select(Booking::RELATIONSHIP, selected: b.object.insurance_nomination[i].present? ? b.object.insurance_nomination[i]["nominee_relationship"] : ''), include_blank: true, class: 'custom-form-control form-control'
          relationship_html.html_safe
        end 

        details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-1') do
          share_html = label_tag :nominee_share, "Share"
          share_html += select_tag "nominee_details[#{id}][nominee_share][]", options_for_select(Booking::PERCENTAGE_SHARE, selected: b.object.insurance_nomination[i].present? ? b.object.insurance_nomination[i]["nominee_share"] : ''), include_blank: true, class: 'custom-form-control form-control'
          share_html.html_safe
        end
      end

      details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-12') do
        label = label_tag :witness_details, "Witness Details"
        label.html_safe
      end

      details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-4') do
        w_surname_html = label_tag :witness_surname, "Surname"
        w_surname_html += b.text_field :witness_surname, class: 'custom-form-control form-control'
        w_surname_html.html_safe
      end

      details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-4') do
        w_given_name_html = label_tag :witness_given_name, "Given Name"
        w_given_name_html += b.text_field :witness_given_name, class: 'custom-form-control form-control'
        w_given_name_html.html_safe
      end

      details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-4') do
        w_passport_number_html = label_tag :witness_passport_number, "Passport/NRIC Number"
        w_passport_number_html += b.text_field :witness_passport_number, class: 'custom-form-control form-control'
        w_passport_number_html.html_safe
      end

      details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-12') do
        witness_html = "By typing in my name and Passport/NRIC No., I confirm that:"
        witness_html += content_tag(:ol) do
          list_html = ""
          list_html += content_tag(:li) do
            det_html = "I am of sound mind and have attained the age of 18 years old;"
            det_html.html_safe
          end
          list_html += content_tag(:li) do
            det1_html = "I am not a nominee named by the Insured Person(s);"
            det1_html.html_safe
          end
          list_html += content_tag(:li) do
            det2_html = "I am the witness to the nomination(s) made, as the nomination(s) was(were) made to me by the respective Insured Person(s); and"
            det2_html.html_safe
          end
          list_html += content_tag(:li) do
            det3_html = "I have informed the Insured Person(s) that if the nomination does not create a trust policy, and he/she intends his/her nominee(s) to receive the policy benefits beneficially and not as an executor, he/she has to assign the policy benefits to his/her nominee(s)."
            det3_html.html_safe
          end
          list_html.html_safe
        end
        witness_html.html_safe
      end
      details_html.html_safe
    end
    html.html_safe
  end

  def render_qrcode(link)
    raw RQRCode::QRCode.new(link).as_html
  end

  def render_nominee_show(b)
    if b.insurance_nomination_flag
      field_class = "nominee_none"
    else
      field_class = "nominee_details"
    end

    html = ""
    html += content_tag(:div, class: 'field custom-form-group form-group col-sm-12') do
      sub_html = check_box_tag :insurance_nomination_flag, {class: 'nominee_confirm'}, b.insurance_nomination_flag, disabled: true
      sub_html += " Note: Death benefit will be paid to insured Person's estate. Please uncheck if the insured person wishes to make a NOMINEE/NOMINEES."
      sub_html.html_safe
    end
    
    html += content_tag(:div, class: field_class) do
      details_html = ""
      for i in 0..2
        details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-12') do
          label = label_tag :nominee_details, "Nominee Details #{i+1}"
          label.html_safe
        end

        details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-2') do
          surname_html = label_tag :nominee_surname, "Surname"
          surname_html += text_field_tag "nominee_details[nominee_surname][]", b.insurance_nomination[i].present? ? b.insurance_nomination[i]["nominee_surname"] : '', class: 'custom-form-control form-control', disabled: true
          surname_html.html_safe
        end  

        details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-2') do
          given_name_html = label_tag :nominee_given_name, "Given Name"
          given_name_html += text_field_tag "nominee_details[nominee_given_name][]", b.insurance_nomination[i].present? ? b.insurance_nomination[i]["nominee_given_name"] : '', class: 'custom-form-control form-control', disabled: true
          given_name_html.html_safe
        end

        details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-3') do
          passport_num_html = label_tag :nominee_passport_number, "Passport/NRIC Number"
          passport_num_html += text_field_tag "nominee_details[nominee_passport_number][]", b.insurance_nomination[i].present? ? b.insurance_nomination[i]["nominee_passport_number"] : '', class: 'custom-form-control form-control', disabled: true
          passport_num_html.html_safe
        end
        
        details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-2') do
          dob_html = label_tag :nominee_date_of_birth, "Date of Birth"
          dob_html += text_field_tag "nominee_details[nominee_date_of_birth][]", b.insurance_nomination[i].present? ? b.insurance_nomination[i]["nominee_date_of_birth"] : '', class: 'custom-form-control form-control', disabled: true
          dob_html.html_safe
        end

        details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-2') do
          relationship_html = label_tag :nominee_relationship, "Relationship"
          relationship_html += text_field_tag "nominee_details[nominee_relationship][]", b.insurance_nomination[i].present? ? b.insurance_nomination[i]["nominee_relationship"] : '', class: 'custom-form-control form-control', disabled: true
          relationship_html.html_safe
        end 

        details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-1') do
          share_html = label_tag :nominee_share, "Share"
          share_html += text_field_tag "nominee_details[nominee_share][]", b.insurance_nomination[i].present? ? b.insurance_nomination[i]["nominee_share"] : '', class: 'custom-form-control form-control', disabled: true
          share_html.html_safe
        end
      end

      details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-12') do
        label = label_tag :witness_details, "Witness Details"
        label.html_safe
      end

      details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-4') do
        w_surname_html = label_tag :witness_surname, "Surname"
        w_surname_html += text_field_tag '', b.witness_surname, class: 'custom-form-control form-control', disabled: true
        w_surname_html.html_safe
      end

      details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-4') do
        w_given_name_html = label_tag :witness_given_name, "Given Name"
        w_given_name_html += text_field_tag '', b.witness_given_name, class: 'custom-form-control form-control', disabled: true
        w_given_name_html.html_safe
      end

      details_html += content_tag(:div, class: 'field custom-form-group form-group col-sm-4') do
        w_passport_number_html = label_tag :witness_passport_number, "Passport/NRIC Number"
        w_passport_number_html += text_field_tag '', b.witness_passport_number, class: 'custom-form-control form-control', disabled: true
        w_passport_number_html.html_safe
      end
      details_html.html_safe
    end
    html.html_safe
  end

  def render_invoice_header
    html = ''
    html += content_tag(:table, class: 'table invoice-header-table') do 
      sub_html = ''
      sub_html += content_tag(:tr) do
        sub_html1 = ''
        sub_html1 += content_tag(:td) do
          text_html = image_tag('logo_iceholidays.png', class: "logo-img")
          text_html += content_tag(:div, " KPL/LN : 2441", class: 'pl10')
          text_html.html_safe
        end
        sub_html1 += content_tag(:td) do
          # text_html = content_tag(:h4, 'ICE HOLIDAYS SDN. BHD.')
          # text_html += content_tag(:small, '230643-X')
          text_html = '<h4>ICE HOLIDAYS SDN. BHD. <small>230643-X</small></h4>'
          text_html += content_tag(:span, 'Lot 346, Wisma MPL, Jalan Raja Chulan,')
          text_html += tag(:br)
          text_html += content_tag(:span, '50200 Kuala Lumpur, Malaysia')
          text_html += tag(:br)
          text_html += content_tag(:span, 'Tel: (603) 2770 0700(Hunting)  Fax: (603) 2144 6716, 2143 0888')
          text_html += tag(:br)
          text_html += content_tag(:span, 'Email: enquiry@ice-holidays.com')
          text_html += "&nbsp;&nbsp;".html_safe
          text_html += content_tag(:span, 'Website: www.ice-holidays.com')
          text_html.html_safe
        end
        sub_html1 += content_tag(:td, align: "right") do
          text_html = image_tag('iata-logo.png', class: "logo-img").html_safe
        end
        sub_html1.html_safe
      end
      sub_html.html_safe
    end
    html.html_safe
  end

  def render_invoice_footer
    content_tag(:table, width: '100%') do
      sub_html = content_tag(:tr) do
        sub_html1 = content_tag(:td, width: '50%') do
          "RECEIVED BY :"
        end
        sub_html1 += content_tag(:td, width: '20%') do
          ""
        end
        sub_html1 += content_tag(:td, width: '30%') do
          "ICE HOLIDAYS SDN. BHD."
        end
        sub_html1.html_safe
      end
      sub_html += content_tag(:tr) do
        sub_html1 = content_tag(:td) do
          ""
        end
        sub_html1 += content_tag(:td) do
          ""
        end
        sub_html1 += content_tag(:td) do
          image_tag("iceholidays_chop.jpg", class: "chop-img pl10")
        end
      end
      sub_html += content_tag(:tr) do
        sub_html1 = content_tag(:td) do
          "_____________________________________"
        end
        sub_html1 += content_tag(:td) do
          ""
        end
        sub_html1 += content_tag(:td) do
          "_____________________________________"
        end
      end
      sub_html += content_tag(:tr) do
        sub_html1 = content_tag(:td) do
          "CUSTOMER SIGNATURE & CHOP"
        end
        sub_html1 += content_tag(:td) do
          ""
        end
        sub_html1 += content_tag(:td) do
          "AUTHORISED SIGNATURE"
        end
      end
      sub_html.html_safe
    end
  end

  def render_land_tour_travel_date(land_tour)
    if land_tour.travel_date.present?
      travel_date = "#{land_tour.travel_date_start.try(:strftime, '%F %H:%M %p')} - #{land_tour.travel_date_end.try(:strftime, '%F %H:%M %p')}"
    else
      travel_date = ""
    end
    
    html = text_field_tag '', travel_date, class: 'custom-form-control form-control', disabled: true
    html.html_safe
  end

  def render_land_tour_activity(activity)
    taggable = activity.get_taggable
    html = text_field_tag '', "#{taggable.title}", class: 'custom-form-control form-control', disabled: true
    html.html_safe
  end

  def render_show_land_tour_activity(activity)
    taggable = activity.get_taggable
    html = "#{taggable.title}"
    html.html_safe
  end

  def render_land_tour_child_and_dta_pricing(lt, page_type = "")
    html = ""

    if page_type == "show"
      html += content_tag(:div, class: "custom-form-group form-group col-sm-12") do
        prices_html = ""
        prices_html += label_tag :child_prices
        prices_html += tag(:br)
        prices_html += content_tag(:div, class: 'custom-form-group form-group col-sm-3') do
          bed_html = label_tag :child_with_bed, "Child with bed (%)"
          bed_html += text_field_tag '', lt.child_with_bed, class: 'custom-form-control form-control', disabled: true
          bed_html.html_safe
        end
        prices_html += content_tag(:div, class: 'custom-form-group form-group col-sm-3') do
          no_bed_html = label_tag :child_without_bed, "Child without bed (%)"
          no_bed_html += text_field_tag '', lt.child_without_bed, class: 'custom-form-control form-control', disabled: true
          no_bed_html.html_safe
        end
        prices_html += content_tag(:div, class: 'custom-form-group form-group col-sm-3') do
          twin_bed_html = label_tag :child_twin, "Child twin (%)"
          twin_bed_html += text_field_tag '', lt.child_twin, class: 'custom-form-control form-control', disabled: true
          twin_bed_html.html_safe
        end
        prices_html += content_tag(:div, class: 'custom-form-group form-group col-sm-3') do
          infant_html = label_tag :infant, "Infant (MYR)"
          infant_html += text_field_tag '', lt.infant, class: 'custom-form-control form-control', disabled: true
          infant_html.html_safe
        end
        prices_html += label_tag :dta_prices, "DTA prices"
        prices_html += tag(:br)
        prices_html += content_tag(:div, class: 'custom-form-group form-group col-sm-3') do
          dta_type_html = label_tag :dta_type, "DTA type"
          dta_type_html += text_field_tag '', lt.dta_type, class: 'custom-form-control form-control', disabled: true
          dta_type_html.html_safe
        end
        prices_html += content_tag(:div, class: 'custom-form-group form-group col-sm-3') do
          dta_adult_html = label_tag :dta_adult, "DTA adult"
          dta_adult_html += text_field_tag '', lt.dta_adult, class: 'custom-form-control form-control', disabled: true
          dta_adult_html.html_safe
        end
        prices_html += content_tag(:div, class: 'custom-form-group form-group col-sm-3') do
          dta_child_html = label_tag :dta_child, "DTA child"
          dta_child_html += text_field_tag '', lt.dta_child, class: 'custom-form-control form-control', disabled: true
          dta_child_html.html_safe
        end
        prices_html.html_safe
      end
    else
      html += content_tag(:div, class: "custom-form-group form-group col-sm-12") do
        prices_html = ""
        prices_html += lt.label :child_prices
        prices_html += tag(:br)
        prices_html += content_tag(:div, class: 'custom-form-group form-group col-sm-3') do
          bed_html = lt.label :child_with_bed, "Child with bed (%)"
          bed_html += lt.number_field :child_with_bed, class: 'custom-form-control form-control'
          bed_html.html_safe
        end
        prices_html += content_tag(:div, class: 'custom-form-group form-group col-sm-3') do
          no_bed_html = lt.label :child_without_bed, "Child without bed (%)"
          no_bed_html += lt.number_field :child_without_bed, class: 'custom-form-control form-control'
          no_bed_html.html_safe
        end
        prices_html += content_tag(:div, class: 'custom-form-group form-group col-sm-3') do
          twin_bed_html = lt.label :child_twin, "Child twin (%)"
          twin_bed_html += lt.number_field :child_twin, class: 'custom-form-control form-control'
          twin_bed_html.html_safe
        end
        prices_html += content_tag(:div, class: 'custom-form-group form-group col-sm-3') do
          infant_html = lt.label :infant, "Infant (MYR)"
          infant_html += lt.number_field :infant, class: 'custom-form-control form-control'
          infant_html.html_safe
        end
        prices_html += lt.label :dta_prices, "DTA prices"
        prices_html += tag(:br)
        prices_html += content_tag(:div, class: 'custom-form-group form-group col-sm-3') do
          dta_type_html = lt.label :dta_type, "DTA type"
          dta_type_html += lt.select :dta_type, options_for_select(LandTour::DTA_TYPE.collect { |dta_type| [dta_type, dta_type] }, selected: lt.object.dta_type), {}, { class: 'custom-form-control form-control' }
          dta_type_html.html_safe
        end
        prices_html += content_tag(:div, class: 'custom-form-group form-group col-sm-3') do
          dta_adult_html = lt.label :dta_adult, "DTA adult"
          dta_adult_html += lt.number_field :dta_adult, class: 'custom-form-control form-control'
          dta_adult_html.html_safe
        end
        prices_html += content_tag(:div, class: 'custom-form-group form-group col-sm-3') do
          dta_child_html = lt.label :dta_child, "DTA child"
          dta_child_html += lt.number_field :dta_child, class: 'custom-form-control form-control'
          dta_child_html.html_safe
        end
        prices_html.html_safe
      end
    end
    html.html_safe
  end

  def render_land_tour_pricing_quantity(land_tour, page_type = "")
    html = ""

    if page_type == "show"
      html += content_tag(:div, class: "nested-fields col-sm-8") do
        qty_html = ""
        if land_tour.land_tour_prices.present?
          land_tour.land_tour_categories.first.land_tour_prices.first.prices.each do |qty, price|
            qty_html += content_tag(:div, class: 'custom-form-group form-group col-sm-2 quantity-field') do
              price_html = label_tag :quantity
              price_html += text_field_tag '', land_tour.quantity_text(qty), class: 'custom-form-control form-control', disabled: true
              price_html.html_safe
            end
          end
        end
        qty_html.html_safe
      end
    else
      if land_tour.land_tour_prices.present?
        land_tour.land_tour_categories.first.land_tour_prices.first.prices.each do |qty, price|
          html += content_tag(:div, class: "custom-form-group form-group col-sm-2 quantity-field") do
            price_html = label_tag :quantity
            price_html += text_field_tag 'land_tour_prices[quantity][]', land_tour.quantity_text(qty), class: 'custom-form-control form-control'
            price_html += link_to('Remove', '', class: "edit-remove-quantity")
            price_html.html_safe
          end
        end
      else
        html += content_tag(:div, class: 'custom-form-group form-group col-sm-2 quantity-field') do
          price_html = label_tag :quantity
          price_html += text_field_tag 'land_tour_prices[quantity][]', '', class: 'custom-form-control form-control'
          price_html += link_to('Remove', '', class: "remove-quantity")
          price_html.html_safe
        end
      end
    end
    html.html_safe
  end

  def render_land_tour_prices(lt, land_tour)
    html = ""
    lt.fields_for :prices do |b|
      lt.object.prices.each do |qty, price|
        html += content_tag(:div, class: "field custom-form-group form-group col-sm-2 price-field") do
          price_html = tag(:br)
          price_html += b.number_field '', value: price, class: 'custom-form-control form-control'
          price_html.html_safe
        end
      end
    end
    html.html_safe
  end

  def render_booking_room_selection(form, booking, guest_no, candidates, tour_leader = nil)
    if tour_leader.present?
      booking.select "guest_#{guest_no}", 
      options_for_select(candidates.order(:agent_user_id).map{|b| ["#{b.name} (#{b.booked_by_detail}) (#{b.booking_group.code})", b.id] } + [["#{tour_leader.name}", "tour_leader/#{tour_leader.id}"]], 
        selected: form.object.send("guest_#{guest_no}").present? && form.object.send("guest_#{guest_no}").class.to_s == 'TourLeader' ? "tour_leader/#{form.object.send("guest_#{guest_no}").try(:id)}" : form.object.send("guest_#{guest_no}").try(:id)), {include_blank: true},{ class: 'custom-form-control form-control guest_selection' }
    else
      booking.select "guest_#{guest_no}", 
      options_for_select(candidates.order(:agent_user_id).map{|b| ["#{b.name}" " (#{b.booked_by_detail}) (#{b.booking_group.code})", b.id] }, 
        selected: form.object.send("guest_#{guest_no}").try(:id)), {include_blank: true},{ class: 'custom-form-control form-control guest_selection' }
    end
  end

end


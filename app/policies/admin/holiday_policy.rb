class Admin::HolidayPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end
end
class Admin::InvoicePolicy < ApplicationPolicy
  def index?
    user.boss?
  end
end
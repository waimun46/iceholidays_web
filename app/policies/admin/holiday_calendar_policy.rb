class Admin::HolidayCalendarPolicy < ApplicationPolicy
  def index?
    user.admin? || user.boss?
  end

  def new?
    create?
  end

  def create?
    user.boss? || user.admin? 
  end

  def show?
    index?
  end

  def edit?
    update?
  end
end



class Admin::HotelBookingGroupPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end
end
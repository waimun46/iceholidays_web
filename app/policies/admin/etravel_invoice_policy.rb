class Admin::EtravelInvoicePolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin? 
  end
end
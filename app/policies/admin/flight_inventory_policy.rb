class Admin::FlightInventoryPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end
end
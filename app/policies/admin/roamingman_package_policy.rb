class Admin::RoamingmanPackagePolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end

  def show?
    index?
  end

  def new_booking?
    create_booking?
  end

  def create_booking?
    user.boss? || user.admin?
  end
end
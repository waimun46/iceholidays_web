class Admin::ExchangeRatePolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end
end
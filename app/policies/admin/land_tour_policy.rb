class Admin::LandTourPolicy < ApplicationPolicy
  def index?
    user.admin? || user.boss?
  end
end
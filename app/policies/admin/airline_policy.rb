class Admin::AirlinePolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end
end
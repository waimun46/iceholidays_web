class Admin::FairPolicy < ApplicationPolicy
  def index?
    user.boss?
  end
end
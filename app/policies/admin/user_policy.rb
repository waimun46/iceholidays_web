class Admin::UserPolicy < ApplicationPolicy
  def index?
    user.boss?
  end

  def new?
    create?
  end

  def create?
    user.boss?
  end

  def show?
    index?
  end

  def edit?
    update?
  end

  def update?
    user.boss?
  end

  def reset_password?
    update?
  end

  def edit_password?
    update_password?
  end

  def update_password?
    record == user
  end

  def topup_credits?
    user.boss?
  end

  def access_log?
    index?
  end

  def online_list?
    user.boss? || user.admin?
  end
end
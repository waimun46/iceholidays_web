class Admin::FlightCommissionPolicy < ApplicationPolicy
  def index?
    user.admin? || user.boss?
  end
end
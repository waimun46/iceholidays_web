class Admin::GiftPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin? || user.distributer? || user.warehouse?
  end

  def new?
    create?
  end

  def create?
    user.boss? || user.admin? || user.distributer?
  end

  def show?
    index?
  end

  def edit?
    update?
  end

  def update?
    user.boss? || user.admin? || user.distributer? || user.warehouse?
  end

  def destroy?
    user.boss? || user.admin? || user.distributer? 
  end

  def gift_stock_report?
    user.boss? || user.admin? || user.distributer? 
  end

  def permitted_attributes
    if user.boss? || user.admin? || user.distributer?
      [:title, :color, :image, :remove_image, :points, :price, :sku, :threshold, :description, gift_stocks_attributes: [:id, :quantity, :_destroy]]
    elsif user.warehouse?
      [gift_stocks_attributes: [:quantity]]
    end
  end
end
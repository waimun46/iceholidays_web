class Admin::GiftTrackingGroupPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin? || user.distributer? || user.warehouse?
  end

  def new?
    create?
  end

  def create?
    user.boss? || user.admin? || user.distributer?
  end

  def show?
    index?
  end

  def edit?
    update?
  end

  def update?
    user.boss? || user.admin? || user.distributer?
  end

  def deliver?
    update?
  end

  def tracking_summary?
    index?
  end

  def summary_detail?
    index?
  end
end
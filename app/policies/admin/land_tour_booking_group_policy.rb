class Admin::LandTourBookingGroupPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end

  def show?
    index?
  end

  def edit?
    update?
  end

  def update?
    user.boss? || user.admin?
  end

  def resend_confirmation?
    user.boss? || user.admin?
  end

  def new_withdraw?
    create_withdraw?
  end

  def create_withdraw?
    user.boss? || user.admin?
  end

  def voucher?
    user.boss? || user.admin?
  end
end
class Admin::GlobaltixTicketBookingGroupPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end
end
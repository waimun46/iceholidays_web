class Admin::TourLeaderPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end

end
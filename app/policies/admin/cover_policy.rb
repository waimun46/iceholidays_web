class Admin::CoverPolicy < ApplicationPolicy

  def index?
    user.boss? || user.admin?
  end

  def update?
    user.boss? || user.admin?
  end
end
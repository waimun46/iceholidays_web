class Admin::SliderPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end
end
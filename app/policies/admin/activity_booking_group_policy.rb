class Admin::ActivityBookingGroupPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end

  def show?
    index?
  end

  def edit?
    update?
  end

  def update?
    user.boss? || user.admin?
  end
end
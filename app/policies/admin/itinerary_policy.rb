class Admin::ItineraryPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end
  def tour_summary?
    user.boss? || user.admin?
  end
end
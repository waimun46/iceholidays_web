class Admin::TourPolicy < ApplicationPolicy
  def index?
    user.admin? || user.boss?
  end
end
class Admin::DepartmentPolicy < ApplicationPolicy 
  def index?
    user.boss? || user.admin?
  end

end
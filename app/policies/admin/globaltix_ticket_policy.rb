class Admin::GlobaltixTicketPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end
end
class Admin::BookingAlterationPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end

  def show?
    index?
  end

  def restore?
    user.boss? || record.auto_cancellation?
  end

end
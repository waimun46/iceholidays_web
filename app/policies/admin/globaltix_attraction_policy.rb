class Admin::GlobaltixAttractionPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end
end
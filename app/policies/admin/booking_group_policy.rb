class Admin::BookingGroupPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin? || user.distributer?
  end

  def show?
    index?
  end

  def edit?
    update?
  end

  def update?
    user.boss? || user.admin?
  end

  def new_alter?
    create_alter?
  end

  def create_alter?
    user.boss? || user.admin?
  end

  def new_export?
    create_export?
  end

  def create_export?
    (user.boss? || user.admin?) && (record.exported_count.blank? || record.exported_count == 0)
  end

  def create_insurance_export?
    (user.boss? || user.admin?) && (record.bookings.first.insurance_available?)
  end

  def create_insurance_endorse?
    user.boss?
  end

  def received_deposit?
    index?
  end

  def tour_booking_summary?
    index?
  end

  def booking_statistic?
    index?
  end

  def sales_statistic?
    index?
  end

  def insurance_changes?
    index?
  end

  def proforma_invoice?
    index?
  end

  def assign_agent?
    user.boss?
  end

end
class Admin::FlightBookingGroupPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end
end
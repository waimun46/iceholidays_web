class Admin::HotelPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end
end
class Admin::CreditRefundPolicy < ApplicationPolicy 
  
  def index?
    user.boss? || user.admin?
  end

  def show?
    index?
  end

  def update_status?
    index?
  end

end
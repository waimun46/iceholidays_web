class Admin::RoamingmanBookingGroupPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end

  def show?
    index?
  end

  def edit?
    update?
  end

  def update?
    user.boss? || user.admin?
  end

  def create_export?
    user.agent? || user.boss? || user.admin?
  end
end
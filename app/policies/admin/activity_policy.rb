class Admin::ActivityPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end
end
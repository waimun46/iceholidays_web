class BookingGroupPolicy < ApplicationPolicy
  def index?
    (user.guest? || user.subagent? || user.agent? || user.admin? || user.boss?)
  end

  def show?
    index?
  end

  def edit?
    update?
  end

  def update?
    record.editable? && (user.subagent? || user.agent? || user.admin? || user.boss?)
  end

  def update_bookings?
    update?
  end

  def edit_booking_rooms?
    update_booking_rooms?
  end

  def update_booking_rooms?
    (user.agent? || user.boss? || user.admin?) && record.booking_rooms.blank?
  end

  def create_insurance_export?
    (user.agent? || user.boss? || user.admin?) && (record.bookings.first.insurance_available?)
  end

  def access_gift_tracking?
    (user.agent? || user.boss? || user.admin?) && (record.gift_tracking_group.present? && !record.gift_tracking_group.overdue?)
  end

  def create_gift_tracking?
    (user.agent? || user.boss? || user.admin?) && (record.has_points? && record.gift_tracking_group.blank? && record.created_in_week?(2))
  end
end
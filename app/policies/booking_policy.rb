class BookingPolicy < ApplicationPolicy

  def new?
    (user.agent? || user.subagent? || user.admin? || user.boss?) && !new_domain
  end

  def create?
    new?
  end

  def thankyou?
    (user.agent? || user.subagent? || user.admin? || user.boss?)
  end

  def cruise_options?
    new?
  end

end
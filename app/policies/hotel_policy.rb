class HotelPolicy < ApplicationPolicy
  def index?
    user.boss? || user.admin?
  end
end
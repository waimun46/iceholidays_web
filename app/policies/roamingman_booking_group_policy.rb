class RoamingmanBookingGroupPolicy < ApplicationPolicy
  def index?
    (user.admin? || user.boss?) || (user.access_wifi? && new_domain)
  end

  def show?
    index?
  end

  def edit?
    update?
  end

  def update?
    user.admin? || user.boss?
  end

  def create_export?
    (user.admin? || user.boss?) && record.export_status == "not_exported"
  end

  def invoice?
    (record.agent_user == user) || (user.admin? || user.boss?)
  end
end
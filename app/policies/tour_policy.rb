class TourPolicy < ApplicationPolicy
  def index?
    (user.guest? || user.subagent? || user.agent? || user.admin? || user.boss?) && !new_domain
  end
end
class PaymentPolicy < ApplicationPolicy
  def index?
    user.subagent? || user.agent? || user.boss? || user.admin?
  end

  def new?
    index?
  end

  def proceed_to_pbb?
    user.boss? || user.admin?
  end

  def proceed_to_fpx?
    user.boss? || user.admin?
  end
end
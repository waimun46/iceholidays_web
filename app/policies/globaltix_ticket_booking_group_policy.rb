class GlobaltixTicketBookingGroupPolicy < ApplicationPolicy
  def index?
    (user.admin? || user.boss?) || (user.access_activity? && new_domain)
  end

  def invoice?
    (record.agent_user == user) || (user.admin? || user.boss?)
  end
end
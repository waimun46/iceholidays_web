class GiftTrackingGroupPolicy < ApplicationPolicy
  def index?
    (user.agent? || user.admin? || user.boss?) && !new_domain
  end

  def show?
    index?
  end

  def edit?
    (user.agent? || user.boss? || user.admin?) && (!record.overdue? && record.preparing?)
  end

  def update?
    edit?
  end

  def submit?
    (user.agent? || user.admin? || user.boss?) && !record.overdue? &&record.submittable? && !new_domain
  end

  def acknowledge?
    deliver?
  end

  def reject?
    deliver?
  end
end
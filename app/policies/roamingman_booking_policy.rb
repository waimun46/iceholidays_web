class RoamingmanBookingPolicy < ApplicationPolicy
  def new?
    create?
  end

  def create?
    user.admin? || user.boss?
  end
end
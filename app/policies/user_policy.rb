class UserPolicy < ApplicationPolicy
  def edit?
    record == user
  end

  def update_password?
    record == user
  end
end
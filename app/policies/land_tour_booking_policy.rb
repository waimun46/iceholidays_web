class LandTourBookingPolicy < ApplicationPolicy
  def new?
    (user.agent? || user.subagent? || user.admin? || user.boss?) && !new_domain
  end

  def create?
    new?
  end

  def thankyou?
    (user.agent? || user.subagent? || user.admin? || user.boss?)
  end
end
class ApplicationPolicy
  attr_reader :user, :record, :new_domain

  def initialize(user_domain, record)
    @user = user_domain.user
    @record = record.is_a?(Array) ? record.last : record
    @new_domain = user_domain.new_domain
  end

  # def index?
  #   false
  # end

  # def show?
  #   scope.where(:id => record.id).exists?
  # end

  # def create?
  #   false
  # end

  # def new?
  #   create?
  # end

  # def update?
  #   false
  # end

  # def edit?
  #   update?
  # end

  # def destroy?
  #   false
  # end

  # def scope
  #   Pundit.policy_scope!(user, record.class)
  # end

  # class Scope
  #   attr_reader :user, :scope

  #   def initialize(user, scope)
  #     @user = user
  #     @scope = scope
  #   end

  #   def resolve
  #     scope
  #   end
  # end
end

class ActivityBookingPolicy < ApplicationPolicy

  def new?
    create?
  end

  def create?
    (user.agent? || user.admin? || user.boss?) && !new_domain
  end

end
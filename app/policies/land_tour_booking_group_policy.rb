class LandTourBookingGroupPolicy < ApplicationPolicy
  def index?
    (user.admin? || user.boss?) || (user.access_land_tour? && new_domain)
  end

  def show?
    index?
  end

  def edit?
    update?
  end

  def update?
    (user.admin? || user.boss?) || (user.access_land_tour? && new_domain)
  end

  def invoice?
    (record.agent_user == user) || (user.admin? || user.boss?)
  end
end
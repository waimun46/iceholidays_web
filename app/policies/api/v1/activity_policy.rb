class Api::V1::ActivityPolicy < ApplicationPolicy
  def index?
    (user.admin? || user.boss?) || (user.access_activity? && new_domain)
  end
end
class Api::V1::TourPolicy < ApplicationPolicy
  def index?
    (user.admin? || user.boss?) || (user.access_series? && new_domain)
  end
end
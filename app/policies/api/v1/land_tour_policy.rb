class Api::V1::LandTourPolicy < ApplicationPolicy
  def index?
    (user.admin? || user.boss?) || (user.access_land_tour? && new_domain)
  end
end
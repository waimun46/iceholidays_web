class Api::V1::RoamingmanBookingGroupPolicy < ApplicationPolicy
  def index?
    (user.admin? || user.boss?) || (user.access_wifi? && new_domain)
  end
end
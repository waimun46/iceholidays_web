class Api::V1::CreditPurchasePolicy < ApplicationPolicy
  def index?
    (user.admin? || user.boss?)
  end
end
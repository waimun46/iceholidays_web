class Api::V1::HotelPolicy < ApplicationPolicy
  def index?
    (user.admin? || user.boss?) || (user.access_hotel? && new_domain)
  end
end
class Api::V1::FlightBookingGroupPolicy < ApplicationPolicy
  def index?
    (user.admin? || user.boss?) || (user.access_flight? && new_domain)
  end
end
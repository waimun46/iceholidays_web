module HotelBooker
  class AmendmentForm
    include ActiveModel::Model
    attr_accessor :type,
                  :price_change,
                  :remarks,                  
                  :check_in,
                  :check_out,
                  :rooms,
                  :booking
    validates_presence_of :type,
                          :remarks,                         
                          :booking
    validate :valid_guests
    
    REQUEST_TYPES = {
      'offline_amendment': 'OfflineAmendment',
      'check_status': 'CheckStatus',
      'price_approved': 'PriceApproved',
      'withdraw_request': 'WithdrawRequest'
    }

    def initialize(args)      
      args[:check_in] = args[:check_in].to_date if args[:check_in].present?
      args[:check_out] = args[:check_out].to_date if args[:check_out].present?
      super args
    end

    def rooms_attributes=(attributes)
      @rooms ||= []
      attributes.each do |idx,room_params|        
        @rooms << HotelBooker::RoomReq.new(idx.is_a?(Hash) ? idx : room_params)      
      end
    end

    def submit
      return false if invalid?
      resp = amendment_request
      if resp.success?                  
        create_amendment_request!(resp) 
        resp    
      else
        errors.add(:base, resp.message)
        false
      end
    end

    private

    def rooms_params
      filtered = []
      rooms.each do |room|
        guests = []
        room.guest.each do |guest| 
          guests << guest if guest.action != 'Retain' 
        end
        if guests.any?
          obj = room.dup
          obj.guest = guests
          filtered << obj
        end
      end
      filtered
    end

    def amendment_request
      HotelBooker[:tbo_holidays].amend!(
        confirmation_no: booking.confirmation_no,
        remarks: remarks,
        type: type,
        amend_information: amend_information,
        price_change: price_change || 'InformationRequired'
      )
    end

    def amend_information
      params = Hash.new.tap do |h|
        h[:check_in] = check_in if booking.check_in != check_in
        h[:check_out] = check_out if booking.check_out != check_out
        h[:rooms] = rooms_params if rooms.present?
      end
      HotelBooker::AmendInformation.new(params)
    end

    def create_amendment_request!(resp)
      request = booking.amendment_request
      request.status = resp.request_status
      request.amendment_message = resp.amendment_message
      request.amendment_requested = resp.amendment_requested.to_h if resp.amendment_requested.present?
      request.approval_information = resp.approval_information if resp.approval_information.present?
      request.save
    end

    def valid_guests  
      errors.add(:base, 'At least ONE adult guest must present.') if rooms.present? && rooms.map(&:guest).flatten.any?{|g| g.lead_guest == "true" && g.guest_type == "Adult" && g.action == "Delete" }
    end
  end
end
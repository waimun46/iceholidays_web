module HotelBooker
  class DayRate < Model
    attr_accessor :date,  
                  :base_fare              
    def self.from_xml_h(hash)
      return new unless hash.present?
      new(
        date: hash[:@date],
        base_fare: hash[:@base_fare],
      )     
    end

    def to_h
      {
        date: date,
        base_fare: base_fare
      }
    end
  end
end
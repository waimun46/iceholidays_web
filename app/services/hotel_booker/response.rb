module HotelBooker
  class Response    
    attr_accessor :error_code,
                  :message,                      
                  :success,
                  :data
    def initialize(success, message, response_body, error_code = '')
      @success, @message, @error_code, =  success, message, error_code 
      generate_data_from(response_body) if success?
    end

    def generate_data_from(response_body)
      @data = response_body
    end

    def success?
      success
    end
    
    def source
    end
  end
end
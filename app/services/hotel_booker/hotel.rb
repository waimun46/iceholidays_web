module HotelBooker
  class Hotel < Model
    attr_accessor :address, 
                  :hotel_code,
                  :hotel_name,
                  :hotel_rating,
                  :fax_number,
                  :phone_number,
                  :country_name,
                  :description,
                  :hotel_facilities,
                  :map,
                  :pin_code,
                  :hotel_website_url,
                  :trip_advisor_rating,
                  :trip_advisor_review_url,
                  :city_name,
                  :hotel_location,
                  :attractions,
                  :image_urls

    def to_h
      {
        address: address,
        hotel_code: hotel_code,
        hotel_name: hotel_name,
        hotel_rating: hotel_rating,
        fax_number: fax_number,
        phone_number: phone_number,
        country_name: country_name,
        description: description,
        hotel_facilities: hotel_facilities,
        map: map,
        pin_code: pin_code,
        hotel_website_url: hotel_website_url,
        trip_advisor_rating: trip_advisor_rating,
        trip_advisor_review_url: trip_advisor_review_url,
        city_name: city_name,
        hotel_location: hotel_location,
        attractions: attractions,
        image_urls: image_urls
      }
    end

    def self.from_xml_h(hash)      
      return new unless  hash.present?        
      new(
        hotel_code: hash[:@hotel_code],
        hotel_name: hash[:@hotel_name],        
        hotel_rating: hash[:@hotel_rating],
        address: hash[:address],
        hotel_location: hash[:hotel_location],
        attractions: Array.wrap(hash.dig(:attractions,:attraction)),
        fax_number: hash[:fax_number],
        phone_number: hash[:phone_number],
        country_name: hash[:country_name],
        description: hash[:description],
        hotel_facilities: Array.wrap(hash.dig(:hotel_facilities,:hotel_facility)),
        map: hash[:map],
        pin_code: hash[:pin_code],
        hotel_website_url: hash[:hotel_website_url],
        trip_advisor_rating: hash[:trip_advisor_rating],
        trip_advisor_review_url: hash[:trip_advisor_review_url],
        city_name: hash[:city_name],
        image_urls: Array.wrap(hash.dig(:image_urls, :image_url))
      )
    end

  end
end
module HotelBooker
  class AmendGuestReq < Model
    attr_accessor :action,
                  :existing_name,
                  :guest_type,
                  :lead_guest,
                  :title,
                  :first_name,
                  :last_name,
                  :age

                  
    def to_xml_h
      {
        '@Action': action,
        '@ExistingName': existing_name,
        '@GuestType': guest_type,
        '@LeadGuest': lead_guest,
        '@Title': title,
        '@FirstName': first_name,
        '@LastName': last_name,
        '@Age': age
      }
    end
  end
end
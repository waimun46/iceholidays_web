module HotelBooker
  class BookingForm
    include ActiveModel::Model
    attr_accessor :client_reference_number,
                  :guest_nationality,
                  :address_info,
                  :session_id,
                  :no_of_rooms,
                  :result_index,
                  :hotel_code,
                  :hotel_name,
                  :hotel_address,
                  :hotel_description,
                  :guests,
                  :hotel_rooms,
                  :check_in,
                  :check_out,
                  :adult_count,
                  :child_count,
                  :rooms,
                  :country_code,
                  :city_id,
                  :total_cost,                  
                  :cancellation_policies,
                  :last_cancellation_deadline,
                  :agent_user,
                  :remark
    validate :valid_guests
                  
    def guests_attributes=(attributes)
      @guests ||= []
      attributes.each do |idx,guest_params|
        @guests << HotelBooker::Guest.new(idx.is_a?(Hash) ? idx : guest_params)      
      end
    end

    def hotel_rooms_attributes=(attributes)
      @hotel_rooms ||= []
      attributes.each do |idx,room_params|        
        @hotel_rooms << HotelBooker::HotelRoom.new(idx.is_a?(Hash) ? idx : room_params)
      end
    end

    def address_info_attributes=(attributes)
      @address_info = HotelBooker::AddressInfo.new(attributes)
    end


    def submit(voucher = true)
      return [false, nil] if invalid?
      resp = book_request(voucher)
      if resp.success?
        booking = create_booking!(resp)      
        [resp, booking]
      else
        errors.add(:base, resp.message)
        [false, nil]
      end
    end

    private
    def book_request(voucher)
      HotelBooker[:tbo_holidays].book!(
        client_reference_number: client_reference_number,
        guest_nationality: guest_nationality,
        guests: guests,
        address_info: address_info,
        payment_info: payment_info(voucher),
        session_id: session_id,
        no_of_rooms: no_of_rooms,      
        result_index: result_index,
        hotel_code: hotel_code,
        hotel_name: hotel_name,
        hotel_rooms: hotel_rooms
      )
    end
    
    def create_booking!(resp)
      original_total_fare, fare_with_markup, markup = get_total_fare_and_markup
      invoice_number   = nil
      amendment_status = 'NotRequested'

      
      # if booking_details = get_booking_details
      #   booking_status = booking_details.booking_status
      #   voucher_status = booking_details.voucher_status
      #   invoice_number = booking_details.invoice_number
      #   amendment_status = booking_details.amendment_status
      # end
      booking_status = resp.booking_status
      voucher_status = booking_status == 'Vouchered'
      booking = TBOBooking.create(
        hotel_code: hotel_code,
        no_of_rooms: no_of_rooms,
        no_of_guests: guests.size,
        rooms: Array.wrap(hotel_rooms).map{|x| x.to_h},
        check_in: check_in,
        check_out: check_out,
        original_total_fare: original_total_fare,
        total_fare: fare_with_markup,        
        markup: markup,        
        currency: 'MYR',
        booking_status: booking_status,
        voucher_status: voucher_status,
        booking_id: resp.booking_id,
        invoice_number: invoice_number,
        reference_no: client_reference_number,
        confirmation_no: resp.confirmation_no,
        message: resp.message,
        address_info: address_info.to_h,
        trip_id: resp.trip_id, 
        cancellation_policies: cancellation_policies,
        amendment_status: amendment_status,
        last_cancellation_deadline: last_cancellation_deadline,
        agent_user_id: agent_user.id,
        remark: remark
      )  
      booking.guests.create(guests.map{|x| x.to_h})
      TBOHotelMailer.send_booking_confirmation(booking).deliver
      booking
      # TBOHotelMailer.send_booking_confirmation(booking).deliver_later
    end

    def get_total_fare_and_markup
      total_original_fare = 0
      currency = nil
      hotel_markup  = TBOHotel.where(hotel_code: hotel_code ).first_or_initialize.hotel_markup
      hotel_markup  = TBOHotel.general_markup if hotel_markup.amount.nil?
      Array.wrap(hotel_rooms).each do |hotel_room|
        currency ||= hotel_room.room_rate.currency

        total_original_fare += hotel_room.room_rate.total_fare.to_f
      end        
      total_original_fare_in_myr = TBOHotel.rate_in_myr(total_original_fare, currency)      
      with_markup = hotel_markup.apply(total_original_fare_in_myr)
      [total_original_fare_in_myr, with_markup[:total], with_markup[:markup]]
    end

    def get_booking_details      
      response = HotelBooker[:tbo_holidays].booking_details(client_reference_number: "281019204111779#ICE")                      
      response.booking_detail
    end

    def payment_info(voucher)
      payment_info = HotelBooker::PaymentInfo.new(voucher_booking: voucher)
    end


    def valid_guests        
      errors.add(:base, 'Same guest name is not allowed.') if @guests.map{|x| "#{x.first_name}#{x.last_name}" }.uniq.length != @guests.length
      errors.add(:base, 'First name is invalid') if @guests.map{|x| !!(x.first_name =~ /^[A-Za-z]{2,}[\s]?[A-Za-z]+$/) }.include?(false)
      errors.add(:base, 'Last name is invalid') if @guests.map{|x| !!(x.last_name =~ /^[A-Za-z]{2,}[\s]?[A-Za-z]+$/) }.include?(false)
      # errors.add(:base, 'Age is invalid') if @guests.map{|x| x.guest_type == "Adult" && x.age.to_i > 0 }.include?(false)
    end
  end
end
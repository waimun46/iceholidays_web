module HotelBooker
  class City < Model
    attr_accessor :name,
                  :code                               
    def self.all(client: :tbo_holidays, country_code:nil)
      response = HotelBooker.city_list(client, country_code: country_code)
    end
  end
end
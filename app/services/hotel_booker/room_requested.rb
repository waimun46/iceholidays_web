module HotelBooker
  class RoomRequested < Model
    ROOM_IDS = ["", 
            'FirstRoom',
            'SecondRoom',
            'ThirdRoom',
            'FourthRoom',
            'FifthRoom',
            'SixthRoom',
            'SeventhRoom',
            'EighthRoom',
            'NinthRoom;'
          ].freeze
      
    def self.[](key)
      if key.is_a?(Integer)
        ROOM_IDS[key]
      else        
        ROOM_IDS.index(key)
      end
    end
  end
end
module HotelBooker
  class AmendInformation < Model
    attr_accessor :check_in,
                  :check_out,
                  :rooms

                  
    def to_xml_h
      Hash.new.tap do |h|
        h['hot:CheckIn'] = {
          "@Date": check_in
        } if check_in.present?
        h['hot:CheckOut'] = {
          "@Date": check_out
        } if check_out.present?
        h['hot:Rooms'] = Array.wrap(rooms).map{|x|  x.to_xml_h } if rooms.present?        
      end
    end
  end
end
module HotelBooker
  class Client
    STANDARD_ERROR_CODE = {
      request_error: 'request_error',
      login_error: 'login_error',
      validation_error: 'validation_error',
      processing_error: 'processing_error',
      system_error: 'system_error',
    }
    def httpreq(*args)             
    end

    def search_hotels(*args)        
    end
    def book!(*args)             
    end
    def cancel_hotel_booking(*args)             
    end
    def hotel_details(*args)             
    end
    def country_list(*args)             
    end
    def destination_city_list(*args)             
    end     
  end
end
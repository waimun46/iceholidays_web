module HotelBooker
  class RoomRes < Model
    attr_accessor :guest,
                  :amended
    AMENDED_TO_INT = {
      "FirstRoom"   => 1,
      "SecondRoom"  => 2,
      "ThirdRoom"   => 3,
      "FourthRoom"  => 4,
      "FifthRoom"   => 5,
      "SixthRoom"   => 6,
      "SeventhRoom" => 7,
      "EighthRoom"  => 8,
      "NinthRoom"   => 9
    }
    def self.from_xml_h(hash)
      new(
        guest: Array.wrap(hash[:guest]).map{|x| HotelBooker::AmendGuestRes.from_xml_h(x) },
        amended: hash[:@amended]
      )
    end

    def to_h
      {
        guest: Array.wrap(guest).map{|x| x.to_h },
        amended: amended
      }
    end
  end
end
module HotelBooker
  class AgencyDetails < Model
    
    attr_accessor :agency_reference, 
                  :name,
                  :address_line_1,
                  :address_line_2,
                  :email,
                  :phone,
                  :fax,
                  :city,
                  :pin,
                  :booking_member_name             



    def self.from_xml_h(hash)
      return new unless  hash.present?         
      new(
        agency_reference: hash[:@agency_reference],
        name: hash[:name],
        address_line_1: hash[:address_line1],
        address_line_2: hash[:address_line2],
        email: hash[:email],
        phone: hash[:phone],
        fax: hash[:fax],
        city: hash[:city],
        pin: hash[:pin],
        booking_member_name: hash[:booking_member_name]
      )
    end
    
  end
end
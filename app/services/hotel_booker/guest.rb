module HotelBooker
  class Guest < Model
    attr_accessor :lead_guest, # bool
                  :guest_type, # adult/ child
                  :guest_in_room, # which room
                  :title,
                  :first_name,
                  :last_name,
                  :age,
                  :index                  
    def to_h
      {
        lead_guest: lead_guest,
        guest_type: guest_type,
        title: title,
        first_name: first_name,
        last_name: last_name,
        age: age,
        guest_in_room: guest_in_room
      }
    end

    def self.from_xml_h(hash)
      new(
        title: hash[:title],
        first_name: hash[:first_name],
        last_name: hash[:last_name],
        age: hash[:age],
        lead_guest: hash[:@lead_guest],
        guest_type: hash[:@guest_type],
        guest_in_room: hash[:@guest_in_room]
      ) 
    end

    def to_xml_h 
      {
        '@LeadGuest': lead_guest,
        '@GuestType': guest_type,
        '@GuestInRoom': guest_in_room,
        'hot:Title': title,
        'hot:FirstName': first_name,
        'hot:LastName': last_name,
        'hot:Age': age
      }
    end
  end
end
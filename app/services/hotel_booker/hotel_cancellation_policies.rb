module HotelBooker
  class HotelCancellationPolicies < Model
    
    attr_accessor :hotel_norms, 
                  :cancel_policies

    def self.from_xml_h(hash)
      new(
        cancel_policies: CancelPolicies.from_xml_h(hash[:cancel_policies]),
        hotel_norms: Array.wrap(hash.dig(:hotel_norms, :string))
      )
    end
  end
end
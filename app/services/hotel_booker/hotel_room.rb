module HotelBooker
  class HotelRoom < Model
    attr_accessor :room_index,
                  :room_type_name,
                  :inclusion,
                  :room_type_code,
                  :rate_plan_code,
                  :room_rate,
                  :room_promtion,
                  :room_additional_info,
                  :room_instructions,
                  :amenities,
                  :room_instructions,
                  :supplements,
                  :meal_type
    def room_rate_attributes=(attributes)
      @room_rate = HotelBooker::RoomRate.new(attributes)
    end

    def supplements_attributes=(attributes)
      @supplements ||= []
      attributes.each do |idx,supplement_params|        
        @supplements << HotelBooker::Supplement.new(supplement_params)
      end
    end

    def to_h
      {
        room_index: room_index,
        room_type_name: room_type_name,
        room_type_code:room_type_code,
        rate_plan_code:rate_plan_code,
        room_rate: room_rate.to_h,
        supplements: Array.wrap(supplements).map{ |x| x.to_h },
        inclusion: inclusion,
        room_promtion: room_promtion,
        room_instructions: room_instructions,        
        amenities: amenities,
        meal_type: meal_type,
        room_additional_info: room_additional_info.to_h
      }
    end

    def to_xml_h  
      obj = {        
        'hot:RoomIndex': room_index,
        'hot:RoomTypeName': room_type_name,
        'hot:RoomTypeCode': room_type_code,
        'hot:RatePlanCode': rate_plan_code,         
        'hot:RoomRate': room_rate.to_xml_h      
      }
      obj.merge!({'hot:Supplements':  supplements.map{|supplement| {'hot:SuppInfo/': supplement.to_xml_h}} }) if supplements.present?   
      obj  
    end

    def self.from_xml_h(hash)
      return new unless hash.present?
      obj = new(
        hash.slice(
          :room_index,
          :room_type_name,
          :inclusion,
          :meal_type,
          :room_type_code,
          :rate_plan_code,
          :room_promtion,
          :amenities,
          :room_instructions
        )
      )
      
      obj.room_rate            = HotelBooker::RoomRate.from_xml_h(hash[:room_rate])
      obj.room_additional_info = HotelBooker::RoomAdditionalInfo.from_xml_h(hash[:room_additional_info])      
      obj.supplements          = Array.wrap(hash.dig(:supplements,:supplement)).map do|supplement|
        HotelBooker::Supplement.from_xml_h(supplement)
      end
      obj
    end 
    
                  
    alias_method :room_promotion, :room_promtion # Response data typo
  end
end
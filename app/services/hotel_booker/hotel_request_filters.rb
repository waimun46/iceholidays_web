module HotelBooker
  class HotelRequestFilters < Model
    attr_accessor :star_rating,
                  :order_by,
                  :result_count,
                  :hotel_code_list
                

    def to_xml_h 
      Hash.new.tap do |h|
        h["hot:OrderBy"] = order_by if order_by.present?
        h["hot:StarRating"] = star_rating || 'All'
        h["hot:HotelCodeList"] = hotel_code_list if hotel_code_list.present?
      end
    end
  end
end
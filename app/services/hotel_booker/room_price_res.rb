module HotelBooker
  class RoomPriceRes < Model
    attr_accessor :room_index,
                  :room_name,
                  :before_amendment_price,
                  :after_amendment_price    
  end
end
require 'gyoku'
require 'nori'
module HotelBooker
  module Clients
    module TBOHolidays

      if Rails.env.iceb2b? || Rails.env.testb2b?
        BASE_URL = Rails.application.credentials.tbo_holidays[:url]
        ACTION_BASE_URL = Rails.application.credentials.tbo_holidays[:base_url]
        USERNAME = Rails.application.credentials.tbo_holidays[:username]
        PASSWORD = Rails.application.credentials.tbo_holidays[:password]
      else
        BASE_URL           =  'https://api.tbotechnology.in/hotelapi_v7/hotelservice.svc'
        ACTION_BASE_URL    =  'http://TekTravel/HotelBookingApi'
        USERNAME           =  'GoldendTest'
        PASSWORD           =  'Gol@75288938'
      end

      class Client < HotelBooker::Client
        SUCCESS = '01'
        STANDARD_ERROR_CODE_MAPPING = {
          '02' => STANDARD_ERROR_CODE[:login_error],
          '03' => STANDARD_ERROR_CODE[:validation_error],
          '05' => STANDARD_ERROR_CODE[:processing_error],
        }

        attr_accessor :host,
                      :port,
                      :username,
                      :password,
                      :logger,
                      :timeout

        def initialize(host=nil, port=nil, username=nil, password=nil, logger=nil, timeout=0)
          @host = host || TBOHolidays::BASE_URL
          @port = port || 80
          @username = username || TBOHolidays::USERNAME
          @password = password || TBOHolidays::PASSWORD
          @logger = logger || Rails.logger
          @timeout = timeout
        end
        ##
        # do an HTTP REQUEST to the remote server given the message, and return
        # an array of responses.
        #
        def httpreq(request, timeout=0)
            xml = request.to_xml    
            log_request(request.class.name, xml)     
            conn = Faraday.new(:url=>@host) do |faraday|
              faraday.response :logger
              faraday.adapter  Faraday.default_adapter
            end
            response  = conn.post do |request|
              request.headers['Content-Type'] = 'application/soap+xml'
              request.body = xml               
            end 

            log_response(request.class.name, response.body) 
            xml_parser = Nori.new(:convert_tags_to => lambda { |tag| tag.snakecase.to_sym })        
            response_hash = xml_parser.parse(response.body)
            message = error_code = response_id = body = ''
            success = false

            if response.status == 200              
              response_id, body  = body_from(response_hash)
              status_code = body.dig(:status, :status_code)
              message = body.dig(:status, :description)
              success = status_code ==  SUCCESS
              error_code = standard_error_code_from(status_code) unless success
            else   
              success = false
              message = "Request Error"
              error_code = STANDARD_ERROR_CODE[:request_error]
            end
            
            klass = response_id.in?(Response::RESPONSE_IDS) ?  Response::RESPONSE_IDS[response_id] : Response
            obj = klass.new(success, message, body, error_code)
            return obj
        end

        def get_hotels(params = {})    
          request = GiataHotelCodesRequest.new(params.merge(username: username,password: password))
          return Response.new(false, request.errors.full_messages.to_sentence, nil, 'invalid_request_params') unless request.valid?
          response = httpreq(request, @timeout)
        end

        def search_hotels(params = {})
          request = HotelSearchRequest.new(params.merge(username: username,password: password))
          return Response.new(false, request.errors.full_messages.to_sentence, nil, 'invalid_request_params') unless request.valid?
          response = httpreq(request, @timeout)
        end

        def hotel_room_availability(params = {})
          request = AvailableHotelRooms.new(params.merge(username: username,password: password))
          return Response.new(false, request.errors.full_messages.to_sentence, nil, 'invalid_request_params') unless request.valid?
          response = httpreq(request, @timeout)
        end

        def hotel_cancellation_policies(params = {})
          request = HotelCancellationPolicyRequest.new(params.merge(username: username,password: password))
          return Response.new(false, request.errors.full_messages.to_sentence, nil, 'invalid_request_params') unless request.valid?
          response = httpreq(request, @timeout)          
        end

        def availability_and_pricing(params = {})
          request = AvailabilityAndPricing.new(params.merge(username: username,password: password))
          return Response.new(false, request.errors.full_messages.to_sentence, nil, 'invalid_request_params') unless request.valid?
          response = httpreq(request, @timeout)          
        end

        def book!(params = {})
          request = HotelBookRequest.new(params.merge(username: username,password: password))
          return Response.new(false, request.errors.full_messages.to_sentence, nil, 'invalid_request_params') unless request.valid?
          response = httpreq(request, @timeout)          
        end

        def generate_invoice!(params = {})
          request = GenerateInvoiceRequest.new(params.merge(username: username,password: password))
          return Response.new(false, request.errors.full_messages.to_sentence, nil, 'invalid_request_params') unless request.valid?
          response = httpreq(request, @timeout)          
        end

        def booking_details(params = {})
          request = HotelBookingDetailRequest.new(params.merge(username: username,password: password))
          return Response.new(false, request.errors.full_messages.to_sentence, nil, 'invalid_request_params') unless request.valid?
          response = httpreq(request, @timeout)    
        end

        def cancel!(params = {}) 
          request = HotelCancelRequest.new(params.merge(username: username,password: password, request_type: 'HotelCancel'))
          return Response.new(false, request.errors.full_messages.to_sentence, nil, 'invalid_request_params') unless request.valid?
          response = httpreq(request, @timeout)    
        end

        def cancel?(params = {}) 
          request = HotelCancelRequest.new(params.merge(username: username,password: password, remarks:'-', request_type: 'CheckStatus'))
          return Response.new(false, request.errors.full_messages.to_sentence, nil, 'invalid_request_params') unless request.valid?
          response = httpreq(request, @timeout)    
        end 
        
        
        def amend!(params = {}) 
          request = AmendmentRequest.new(params.merge(username: username,password: password))
          return Response.new(false, request.errors.full_messages.to_sentence, nil, 'invalid_request_params') unless request.valid?
          response = httpreq(request, @timeout)    
        end


        def amend?(params = {}) 
          request = AmendmentRequest.new(params.merge(username: username,password: password, type: 'CheckStatus'))
          return Response.new(false, request.errors.full_messages.to_sentence, nil, 'invalid_request_params') unless request.valid?
          response = httpreq(request, @timeout)    
        end

        def hotel_details(params = {}) 
          request = HotelDetailsRequest.new(params.merge(username: username,password: password))
          return Response.new(false, request.errors.full_messages.to_sentence, nil, 'invalid_request_params') unless request.valid?
          response = httpreq(request, @timeout)
        end

        def get_countries(params = {})
          request = CountryListRequest.new(params.merge(username: username,password: password))
          return Response.new(false, request.errors.full_messages.to_sentence, nil, 'invalid_request_params') unless request.valid?
          response = httpreq(request, @timeout)
        end


        def get_cities(params = {})
          request = DestinationCityListRequest.new(params.merge(username: username,password: password))
          return Response.new(false, request.errors.full_messages.to_sentence, nil, 'invalid_request_params') unless request.valid?                        
          response = httpreq(request, @timeout)
        end

        private
        def log_request(action, body = nil)
          return unless Rails.env.development? || Rails.env.testb2b?
          logger.info "\n========================================================="            
          logger.info "Requested Action: " + action
          logger.info "\nRequest Body: \n" + body if body.present?
          logger.info "=========================================================" 
        end

        def log_response(action, body = nil)
          return unless Rails.env.development? || Rails.env.testb2b?
          logger.info "\n========================================================="            
          logger.info "Requested Action: " + action
          logger.info "\nResponse Body: \n" + body if body.present?
          logger.info "=========================================================" 
        end
        
        def body_from(response)
          response_id = (response.dig(:"s:envelope",:"s:body") || {}).keys.first
          body = response.dig(:"s:envelope",:"s:body",response_id) || {}
          [response_id, body]
        end
        def standard_error_code_from(code)
          STANDARD_ERROR_CODE_MAPPING[code] || STANDARD_ERROR_CODE[:system_error]
        end
      end

      # REQUEST CLASSES
      class Request
        include ActiveModel::Model
        SOAP_ENVELOPE_URL  = 'http://www.w3.org/2003/05/soap-envelope'
        ADDRESSING_URL     = 'http://www.w3.org/2005/08/addressing'        
        
        attr_accessor :username,
                      :password  
        def to_xml
          Gyoku.xml({
            'soap:Envelope':{
              '@xmlns:soap': SOAP_ENVELOPE_URL,
              '@xmlns:hot': TBOHolidays::ACTION_BASE_URL,
              'soap:Header': header,
              'soap:Body': body,
            }
          }, {unwrap: true} )
        end

        private
        def header
          {          
            '@xmlns:wsa': ADDRESSING_URL,
            'hot:Credentials':{
              '@UserName': username,
              '@Password': password 
            },
            'wsa:Action': action,
            'wsa:To': BASE_URL        
          }
        end
        
        def action   
          ACTION_BASE_URL + '/' + self.class.name.demodulize.gsub('Request', '')
        end

        def body
          {}
        end
      end

      class HotelSearchRequest < Request        
        attr_accessor :check_in,
                      :check_out,
                      :country_name,
                      :city_name,
                      :city_id,
                      :is_nearby_search_allowed,
                      :no_of_rooms,
                      :room_guests,
                      :guest_nationality,
                      :pref_currency,
                      :limit,
                      :filters

        validates_presence_of :check_in,
                              :check_out,
                              :no_of_rooms,
                              :room_guests,
                              :guest_nationality,
                              :city_id
        validate :valid_check_in_out
                
        private
        def body
          { 
            'hot:HotelSearchRequest': Hash.new.tap do |h|      
              h['hot:CheckInDate']  =  check_in
              h['hot:CheckOutDate'] =  check_out
              h['hot:CountryName']  =  country_name if country_name.present?
              h['hot:CityName']     =  city_name if city_name.present?
              h['hot:CityId']       = city_id
              h['hot:NoOfRooms']    = no_of_rooms
              h['hot:RoomGuests']   = room_guests.map{|x| x.to_xml_h}
              h['hot:GuestNationality'] = guest_nationality
              h['hot:PreferredCurrencyCode'] = pref_currency || HotelBooker::DEFAULT_CURRENCY
              h['hot:IsNearBySearchAllowed'] = is_nearby_search_allowed || true
              h['hot:ResultCount'] = limit if limit.present?
              h['hot:Filters'] =  filters.to_xml_h if filters.present?
              # h['hot:IsRoomInfoRequired'] = true
              h['ResponseTime'] = 10
            end    
          }
        end

        def valid_check_in_out
          errors.add(:base, 'Invalid date selection. Check-out must be greater than check-in.') unless check_in.is_a?(Date) && check_out.is_a?(Date) && (check_in < check_out)
        end
      end

      class HotelDetailsRequest < Request        
        attr_accessor :hotel_code                  
        validates_presence_of :hotel_code                
        private
        def body
          { 
            'hot:HotelDetailsRequest': Hash.new.tap do |h|      
              h['hot:HotelCode']  =  hotel_code
            end    
          }
        end
      end

      class AvailableHotelRooms < Request        
        attr_accessor :hotel_code,
                      :session_id,
                      :response_time,
                      :result_index                
        validates_presence_of :hotel_code, :session_id, :result_index                
        private
        def body
          { 
            'hot:HotelRoomAvailabilityRequest': Hash.new.tap do |h|      
              h['hot:HotelCode'] =  hotel_code
              h['hot:SessionId'] = session_id
              h['hot:ResultIndex'] = result_index
              # h['hot:ResponseTime'] = response_time
              h['IsCancellationPolicyRequired'] = true
            end    
          }
        end
      end

      class HotelCancellationPolicyRequest < Request        
        attr_accessor :session_id,
                      :result_index,
                      :booking_options

        validates_presence_of :session_id,
                              :result_index,
                              :booking_options               
        private
        def body
          { 
            'hot:HotelCancellationPolicyRequest': Hash.new.tap do |h|                    
              h['hot:SessionId']         = session_id
              h['hot:ResultIndex']       = result_index
              h['hot:OptionsForBooking'] = booking_options.to_xml_h
            end    
          }
        end
      end


      class AvailabilityAndPricing < HotelCancellationPolicyRequest             
        private
        def body
          { 
            'hot:AvailabilityAndPricingRequest': Hash.new.tap do |h|                    
              h['hot:SessionId'] = session_id
              h['hot:ResultIndex'] = result_index
              h['hot:OptionsForBooking'] = booking_options.to_xml_h
            end    
          }
        end 
      end 
      
      class HotelBookRequest < Request        
        attr_accessor :client_reference_number,
                      :guest_nationality,
                      :guests,
                      :address_info,
                      :payment_info,
                      :session_id,
                      :flight_info,
                      :no_of_rooms,
                      :result_index,
                      :hotel_code,
                      :hotel_name,
                      :hotel_rooms,
                      :special_requests

        validates_presence_of :session_id,
                              :result_index,
                              :guest_nationality,
                              :client_reference_number,
                              :address_info,
                              :no_of_rooms,
                              :payment_info,
                              :hotel_code,
                              :hotel_name,
                              :hotel_rooms

        private
        def body
          { 
            'hot:HotelBookRequest': Hash.new.tap do |h|                    
              h['hot:ClientReferenceNumber'] = client_reference_number              
              h['hot:GuestNationality']      = guest_nationality
              h['hot:Guests']                = guests.map{|x| {'hot:Guest': x.to_xml_h}}
              h['hot:AddressInfo']           = address_info.to_xml_h
              h['hot:PaymentInfo']           = payment_info.to_xml_h
              h['hot:SessionId']             = session_id
              h['hot:NoOfRooms']             = no_of_rooms
              h['hot:ResultIndex']           = result_index
              h['hot:HotelCode']             = hotel_code
              h['hot:HotelName']             = hotel_name
              h['hot:HotelRooms']            = hotel_rooms.map{|x| {'hot:HotelRoom': x.to_xml_h} }
            end    
          }
        end
      end

      class GenerateInvoiceRequest < Request        
        attr_accessor :payment_info,
                      :confirmation_no
                      # :booking_id // No longer in use

        validates_presence_of :payment_info,
                              :confirmation_no

        private
        def body
          { 
            'hot:GenerateInvoiceRequest': Hash.new.tap do |h|                    
              h['hot:PaymentInfo']    = payment_info.to_xml_h
              h['hot:ConfirmationNo'] = confirmation_no
            end    
          }
        end
      end

      class HotelBookingDetailRequest < Request        
        attr_accessor :client_reference_number
                      # :confirmation_no 
                      # :booking_id // No longer in use

        validates_presence_of :client_reference_number

        private
        def body
          { 
            'hot:HotelBookingDetailRequest': Hash.new.tap do |h|                                  
              h['hot:ClientReferenceNumber'] = client_reference_number
            end    
          }
        end
      end      

      class HotelCancelRequest < Request        
        attr_accessor :confirmation_no,
                      :request_type,
                      :remarks

        validates_presence_of :confirmation_no,
                              :request_type,
                              :remarks

        private
        def body
          { 
            'hot:HotelCancelRequest': Hash.new.tap do |h|                                  
              h['hot:ConfirmationNo'] = confirmation_no
              h['hot:RequestType']    = request_type
              h['hot:Remarks']        = remarks
            end    
          }
        end
      end 

      class AmendmentRequest < Request        
        attr_accessor :type,
                      :price_change,
                      :remarks,
                      :confirmation_no,
                      :amend_information

        validates_presence_of :type, 
                              :price_change, 
                              :confirmation_no,
                              :amend_information                
        private
        def body
          { 
            'hot:AmendmentRequest': Hash.new.tap do |h|      
              h['hot:Request/'] = {
                '@Type': type,
                '@PriceChange': price_change, 
                '@Remarks': remarks,
              }
              h['hot:ConfirmationNo'] = confirmation_no
              h['hot:AmendInformation'] = amend_information.to_xml_h
            end    
          }
        end
      end

      class GiataHotelCodesRequest < Request        
        attr_accessor :city_code,
                      :is_detailed_response

        validates_presence_of :city_code

        private
        def body
          { 
            'hot:GiataHotelCodesRequest': Hash.new.tap do |h|                                  
              h['hot:CityCode']           = city_code
              h['hot:IsDetailedResponse'] = is_detailed_response || true
            end    
          }
        end
      end

      class CountryListRequest < Request                      
        private
        def body     
          {'hot:CountryListRequest/': ''}
        end
      end

      class DestinationCityListRequest < Request   
        attr_accessor :country_code                  
        private
        def body     
          { 
            'hot:DestinationCityListRequest': {      
              'hot:CountryCode':  country_code ||  HotelBooker::DEFAULT_COUNTRY_CODE,              
              'hot:ReturnNewCityCodes': true
            }    
          }
        end
      end



      # RESPONSE CLASSES
      class Response < HotelBooker::Response 
        RESPONSE_IDS = {}
        def source
          :tbo_holidays
        end
      end

      # Returns request params, session id and hotels
      class HotelSearchResponse < Response        
        RESPONSE_IDS[:hotel_search_response] = self
        attr_reader :response_time,
                    :session_id,
                    :city_id,
                    :check_in,
                    :check_out,
                    :rooms_requested,
                    :room_guests,
                    :hotel_result_list  
        private
        def generate_data_from(response_body)          
          @response_time   = response_body[:response_time]
          @session_id      = response_body[:session_id]
          @city_id         = response_body[:city_id]
          @check_in        = response_body[:check_in_date]
          @check_out       = response_body[:check_out_date]
          @rooms_requested = response_body[:no_of_rooms_requested]
          @room_guests     = get_room_guests_from(response_body.dig(:room_guests, :room_guest)) || []
          @hotel_result_list = get_hotel_result_list_from(response_body.dig(:hotel_result_list, :hotel_result)) || []              
        end


        def get_hotel_result_list_from(obj) 
          hotels = Array.wrap(obj).map do |hr|   
            hotel_result = HotelBooker::HotelResult.new(
              hr.slice(
                :result_index,
                :is_pkg_property,
                :is_package_rate,
                :mapped_hotel
              )      
            ) 
            hotel_result.hotel_info      = get_hotel_info_from(hr[:hotel_info])
            hotel_result.min_hotel_price = get_min_hotel_price_from(hr[:min_hotel_price])            
            hotel_result
          end
        end

        def get_hotel_info_from(obj)
          HotelBooker::HotelInfo.new(
            obj.slice(
              :hotel_code, 
              :hotel_name,
              :hotel_picture,
              :hotel_description,
              :hotel_address,
              :contact_no,
              :rating,
              :latitude,
              :longitude,
              :trip_advisor_rating,
              :trip_advisor_review_url,
              :hotel_promotion
            )
          )
        end

        # def get_rooms_from(obj)
        #   return [] if obj.nil?
        #   ((obj.is_a?(Array) ? obj : [obj]) || []).map do |room| 
        #     HotelBooker::Room.new(
        #       type:            room[:@room_type],
        #       rate_plan_code:  room[:@rate_plan_code],
        #       inclusion:       room[:@inclusion]
        #   )
        #   end       
        # end
        
        def get_room_guests_from(obj)
          Array.wrap(obj).map do |rg| 
            HotelBooker::RoomGuest.new(
              adult_count: rg[:@adult_count],
              child_count: rg[:@child_count],
              child_age:   rg.dig(:child_age, :int)
            )
          end          
        end

        def get_min_hotel_price_from(obj)
          HotelBooker::MinHotelPrice.new(
            total_price:    obj[:@total_price],
            currency:       obj[:@currency],
            pref_price:     obj[:@pref_price],
            pref_currency:  obj[:@pref_currency],
            b2c_rates:      obj[:@b2c_rates],
            original_price: obj[:@original_price]
          )
        end
      end 

      # Returns booking_options and rooms available
      class HotelRoomAvailabilityResponse < Response
        RESPONSE_IDS[:hotel_room_availability_response] = self   
        
        attr_reader :result_index,
                    :hotel_rooms,
                    :booking_options
      

        private
        def generate_data_from(response_body)      
          @result_index    = response_body[:result_index]
          @hotel_rooms     = Array.wrap(response_body.dig(:hotel_rooms, :hotel_room)).map{|hotel_room| HotelBooker::HotelRoom.from_xml_h(hotel_room)}   
          @booking_options = HotelBooker::BookingOptions.from_xml_h(response_body[:options_for_booking])
        end
      end

      # Returns cancellation policies,last_cancellation_deadline, default_policy
      class HotelCancellationPolicyResponse < Response        
        RESPONSE_IDS[:hotel_cancellation_policy_response] = self  
        attr_reader :cancel_policies,
                    :hotel_norms
        private        
        def generate_data_from(response_body)                    
          @cancel_policies = HotelBooker::CancelPolicies.from_xml_h(response_body[:cancel_policies])
          @hotel_norms     = Array.wrap(response_body.dig(:hotel_norms, :string))
        end
      end 

    
      class AvailabilityAndPricingResponse < Response        
        RESPONSE_IDS[:availability_and_pricing_response] = self 
        attr_reader :result_index,
                    :available_for_book,
                    :available_for_confirm_book,
                    :cancellation_policies_available,
                    :hotel_cancellation_policies,
                    :price_verification,
                    :account_info,
                    :hotel_details_verification,
                    :is_flight_details_mandatory,
                    :hotel_details      
        private
        def generate_data_from(response_body)                 
          @result_index = response_body[:result_index]
          @available_for_book = response_body[:available_for_book]
          @available_for_confirm_book = response_body[:available_for_confirm_book]
          @cancellation_policies_available = response_body[:cancellation_policies_available]
          @hotel_cancellation_policies = HotelBooker::HotelCancellationPolicies.from_xml_h(response_body[:hotel_cancellation_policies]) if cancellation_policies_available
          @price_verification = HotelBooker::PriceVerification.from_xml_h(response_body[:price_verification])
          @account_info = HotelBooker::AccountInfo.from_xml_h(response_body[:account_info])
          @hotel_details_verification = HotelBooker::HotelDetailsVerification.from_xml_h(response_body[:hotel_details_verification])
          @is_flight_details_mandatory = response_body[:is_flight_details_mandatory]
          @hotel_details = HotelBooker::Hotel.from_xml_h(response_body[:hotel_details])
        end 
      
      end 
    
      class HotelBookResponse < Response        
        RESPONSE_IDS[:hotel_book_response] = self 
        attr_reader :booking_status,
                    :booking_id,
                    :confirmation_no,
                    :trip_id,
                    :price_change
                    
        private
        def generate_data_from(response_body)      
          @booking_status  = response_body[:booking_status]
          @booking_id      = response_body[:booking_id]
          @confirmation_no = response_body[:confirmation_no]
          @trip_id         = response_body[:trip_id]
          @price_change    = HotelBooker::PriceChange.from_xml_h(response_body[:price_change])          
        end       
      end 

      class GenerateInvoiceResponse < Response        
        RESPONSE_IDS[:generate_invoice_response] = self 
        attr_reader :invoice_no      
                    
        private
        def generate_data_from(response_body)                            
          @invoice_no = response_body[:invoice_no]          
        end       
      end 

      class HotelBookingDetailResponse < Response        
        RESPONSE_IDS[:hotel_booking_detail_response] = self 
        attr_reader :booking_detail      
                    
        private
        def generate_data_from(response_body)     
          @booking_detail = HotelBooker::BookingDetail.from_xml_h(response_body[:booking_detail])          
        end       
      end
      
      class AmendmentResponse < Response        
        RESPONSE_IDS[:amendment_response] = self 
        attr_reader :request_status,
                    :amendment_message,
                    :approval_information,      
                    :amendment_requested
                    
        private
        def generate_data_from(response_body) 
          @request_status    = response_body[:request_status]    
          @amendment_message    = response_body[:amendment_message]    
          @approval_information = response_body[:approval_information]   
          @amendment_requested  = HotelBooker::AmendmentRequested.from_xml_h(response_body[:amendment_requested]) if response_body[:amendment_requested].present?
        end       
      end

      class HotelCancelResponse < Response        
        RESPONSE_IDS[:hotel_cancel_response] = self 
        attr_reader :booking_id,
                    :cancellation_charge,
                    :refund_amount,
                    :request_status      
                    
        private
        def generate_data_from(response_body)  
          @booking_id     = response_body[:booking_id]
          @refund_amount  = response_body[:refund_amount]
          @request_status = response_body[:request_status]
          @cancellation_charge = response_body[:cancellation_charge]        
        end       
      end 
      
      class GiataHotelCodesResponse < Response        
        RESPONSE_IDS[:giata_hotel_codes_response] = self 
        attr_reader :hotels  
        private
        def generate_data_from(response_body)
            hotels  = response_body.dig(:hotel_details,:hotel) || []
            @hotels = Array.wrap(hotels).map do |hotel| 
              HotelBooker::Hotel.from_xml_h(hotel)
            end       
        end
      end 

      class HotelDetailsResponse < Response        
        RESPONSE_IDS[:hotel_details_response] = self 
        attr_reader :hotel_details  
        private
        def generate_data_from(response_body)
          @hotel_details = HotelBooker::Hotel.from_xml_h(response_body[:hotel_details])
        end
      end 

      class CountryListResponse < Response        
        RESPONSE_IDS[:country_list_response] = self 
        attr_reader :countries  
        private
        def generate_data_from(response_body)
          @countries = (response_body.dig(:country_list,:country) || []).map{|country| HotelBooker::Country.new(
            name: country[:@country_name],
            code: country[:@country_code], 
          )}
        end
      end 
      
      class DestinationCityListResponse < Response        
        RESPONSE_IDS[:destination_city_list_response] = self 
        attr_reader :cities,
                    :country_name
        private
        def generate_data_from(response_body)
          @country_name = response_body[:country_name]
          cities = response_body.dig(:city_list, :city) || []       
          @cities =  Array.wrap(cities).map do |city| 
            HotelBooker::City.new(
              name: city[:@city_name],
              code: city[:@city_code], 
            )
          end       
        end
      end 
    end       
  end
end
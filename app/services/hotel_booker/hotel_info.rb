module HotelBooker
  class HotelInfo < Model
    RATING_MAPPING = {
      "OneStar"   => 1,
      "TwoStar"   => 2,
      "ThreeStar" => 3,       
      "FourStar"  => 4,
      "FiveStar"  => 5
    }
    attr_accessor :hotel_code, 
                  :hotel_name,
                  :hotel_picture,
                  :hotel_description,
                  :hotel_address,
                  :contact_no,
                  :rating,
                  :latitude,
                  :longitude,
                  :trip_advisor_rating,
                  :trip_advisor_review_url,
                  :hotel_promotion

    def rating_int      
      RATING_MAPPING[rating] || 0
    end
  end
end
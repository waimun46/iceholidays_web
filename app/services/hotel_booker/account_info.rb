module HotelBooker
  class AccountInfo < Model
    attr_accessor :agency_balance,
                  :agency_blocked
    def self.from_xml_h(hash)
      new(
        agency_balance: hash[:@agency_balance],
        agency_blocked: hash[:@agency_blocked]
      )
    end
  end
end
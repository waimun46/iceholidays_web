module HotelBooker
  class RoomInformation < Model
    attr_accessor :result_index,
                  :hotel_info,
                  :min_hotel_price,
                  :is_pkg_property    
  end
end
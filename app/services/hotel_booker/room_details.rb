module HotelBooker
  class RoomDetails < Model
    attr_accessor :adult_count,
                  :child_count,
                  :room_name,
                  :guest_info,
                  :room_promotion,
                  :supplements,
                  :meal_type,
                  :room_rate,
                  :ameneties
    def self.from_xml_h(hash)
      new(
        adult_count: hash[:adult_count],
        child_count: hash[:child_count],
        room_name: hash[:room_name],
        meal_type: hash[:meal_type],
        guest_info: Array.wrap(hash.dig(:guest_info, :guest)).map{|x| HotelBooker::Guest.from_xml_h(x)},
        room_promotion: hash[:room_promtion],
        supplements:  Array.wrap(hash.dig(:supplements, :supp_info)).map{|x| HotelBooker::Supplement.from_xml_h(x)},
        room_rate: Array.wrap(hash[:room_rate]).map{|x| HotelBooker::RoomRate.from_xml_h(x)},
        ameneties: Array.wrap(hash[:ameneties])
      )
    end  
  end
end
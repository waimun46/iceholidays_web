module HotelBooker
  class PaymentInfo < Model
    attribute :voucher_booking, :boolean, default: true
    attribute :payment_mode_type, :string, default: 'Limit'

    def to_xml_h
      {        
        '@VoucherBooking': voucher_booking,
        '@PaymentModeType': payment_mode_type
      }    
    end
  end
end
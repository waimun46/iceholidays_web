module HotelBooker
  class CancelPolicy < Model
    attr_accessor :room_type_name,
                  :room_index,
                  :from_date,
                  :to_date,
                  :charge_type,
                  :cancellation_charge,
                  :currency,
                  :pref_currency,
                  :pref_cancellation_charge
    def self.from_xml_h(hash)
      new(
        room_type_name: hash[:@room_type_name],
        room_index:     hash[:@room_index],
        from_date:      hash[:@from_date],
        to_date:        hash[:@to_date],
        charge_type:    hash[:@charge_type],              
        currency:       hash[:@currency],
        pref_currency:  hash[:@pref_currency],
        cancellation_charge:      hash[:@cancellation_charge],
        pref_cancellation_charge: hash[:@pref_cancellation_charge]
      )   
    end
  end
end
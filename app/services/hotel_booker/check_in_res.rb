module HotelBooker
  class CheckInRes < Model
    attr_accessor :date,
                  :date_action
                
    def self.from_xml_h(hash)
      new(
       date: hash[:@date],
       date_action: hash[:@date_action]
      )   
    end

    def to_h
      {
        date: date.to_date,
        date_action: date_action
      }
    end
  end
end
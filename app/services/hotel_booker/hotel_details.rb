module HotelBooker
  class HotelDetails < Model
    
    attr_accessor :address, 
                  :hotel_code,
                  :hotel_name,
                  :hotel_rating,
                  :fax_number,
                  :phone_number,
                  :country_name,
                  :description,
                  :hotel_facilities,#map_this
                  :map,
                  :pin_code,
                  :hotel_website_url,
                  :trip_advisor_rating,
                  :trip_advisor_review_url,
                  :city_name,
                  :hotel_location,
                  :attractions #map_this
        




    def self.from_xml_h(hash)
      return new unless  hash.present?              
      new(
        hotel_code: hash[:@hotel_code],
        hotel_name: hash[:@hotel_name],        
        hotel_rating: hash[:@hotel_rating],
        address: hash[:address],
        hotel_location: hash[:hotel_location],
        attractions: hash[:attractions],
        fax_number: hash[:fax_number],
        country_name: hash[:country_name],
        description: hash[:description],
        hotel_facilities: hash[:hotel_facilities],
        map: hash[:map],
        pin_code: hash[:pin_code],
        hotel_website_url: hash[:hotel_website_url],
        trip_advisor_rating: hash[:trip_advisor_rating],
        trip_advisor_review_url: hash[:trip_advisor_review_url],
        city_name: hash[:city_name]
      )
    end
    
  end
end
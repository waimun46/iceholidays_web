module HotelBooker
  class PriceVerification < Model
    attr_accessor :hotel_rooms,
                  :status,
                  :price_changed,
                  :available_on_new_price

    def self.from_xml_h(hash)
      new(
        status: hash[:@status],
        price_changed: hash[:@price_changed],
        available_on_new_price: hash[:@available_on_new_price]
      )
    end  
                  
  end
end
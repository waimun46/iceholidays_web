module HotelBooker
  class BookingOptions < Model
    attr_accessor :fixed_format,  
                  :room_combinations

    def self.from_xml_h(hash)
      return new unless hash.present?
      obj = new(
        fixed_format: hash[:fixed_format]
      )
      obj.room_combinations = Array.wrap(hash[:room_combination]).map do |room_combination| 
        HotelBooker::RoomCombination.from_xml_h(room_combination)
      end if hash[:room_combination].present?
      obj
    end
                  
    def to_xml_h
      {
        'hot:FixedFormat': fixed_format,
        'hot:RoomCombination': Array.wrap(room_combinations).map{|x| {'hot:RoomIndex': x.room_index }}
      }
    end

  end
end
module HotelBooker
  class Supplement < Model
    attr_accessor :type,
                  :supp_id,
                  :supp_name,
                  :supp_is_mandatory,
                  :supp_charge_type,
                  :price,
                  :supp_is_selected,
                  :currency_code
    def self.from_xml_h(hash)
      return new unless hash.present?
      new(
        type: hash[:@type],
        supp_id: hash[:@supp_id],
        supp_name: hash[:@supp_name],
        supp_is_mandatory: hash[:@supp_is_mandatory],
        price: hash[:@price],
        currency_code: hash[:@currency_code] ||  hash[:@currency],
        supp_charge_type: hash[:@supp_charge_type]
      )
    end
    
    def to_h
      {
        type: type,
        supp_id: supp_id,
        supp_name: supp_name,
        supp_is_mandatory: supp_is_mandatory,
        supp_charge_type: supp_charge_type,
        price: price,
        supp_is_selected: supp_is_selected,
        currency_code: currency_code
      }
    end

    def to_xml_h
      {        
        '@SuppID': supp_id,
        '@SuppChargeType': supp_charge_type,
        '@Price': price,
        '@SuppIsSelected': supp_is_selected || true      
      }    
    end
  end
end
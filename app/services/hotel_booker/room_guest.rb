module HotelBooker
  class RoomGuest < Model
    attr_accessor :child_age,
                  :child_count,
                  :adult_count

    def to_xml_h
      {
        'hot:RoomGuest':
        {
          '@AdultCount': adult_count,
          '@ChildCount': child_count,
          'hot:ChildAge': ( child_age || []).map{|x| {'hot:int': x.to_i }}
        }
      }
    end

  end
end
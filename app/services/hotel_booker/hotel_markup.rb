module HotelBooker
  class HotelMarkup < Model
    attr_accessor :amount,
                  :type

    def apply(input_amount)
      markup = percentage_based? ? percentage_markup(input_amount.to_f) : amount
      total = input_amount.to_f + markup
      {amount: amount, type: type, total: total, markup:markup}
    end

    def percentage_based?
      type == "percentage"
    end

    def percentage_markup(input_amount)      
      input_amount.to_f * (amount.to_f / 100)
    end
  end
end
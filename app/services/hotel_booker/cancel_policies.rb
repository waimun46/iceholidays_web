module HotelBooker
  class CancelPolicies < Model
    attr_accessor :last_cancellation_deadline,
                  :cancel_policy,
                  :text_policy,
                  :default_policy,
                  :policy_format,
                  :auto_cancellation_text
    def self.from_xml_h(hash)
      new(
        default_policy: hash[:default_policy],            
        policy_format:  hash[:@policy_format],
        cancel_policy:  get_cancel_policy_from(hash[:cancel_policy]),
        text_policy:    Array.wrap(hash.dig(:text_policy, :string)),
        auto_cancellation_text:     hash[:auto_cancellation_text],
        last_cancellation_deadline: hash[:last_cancellation_deadline]
      )
    end

    def self.get_cancel_policy_from(obj)      
      Array.wrap(obj).map{|x| HotelBooker::CancelPolicy.from_xml_h(x)}
    end
  end
end
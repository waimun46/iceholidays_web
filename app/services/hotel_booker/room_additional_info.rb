module HotelBooker
  class RoomAdditionalInfo < Model
    
    attr_accessor :hotel_code, 
                  :description,
                  :image_urls

    def self.from_xml_h(hash)
      return new unless hash.present?
      new(
        description: hash[:description],
        image_urls:  Array.wrap(hash[:image_url_s]).map{|x| x[:url]}
      )
    end

    def to_h
      {
        hotel_code: hotel_code,
        description: description,
        image_urls: image_urls
      }
    end

  end
end
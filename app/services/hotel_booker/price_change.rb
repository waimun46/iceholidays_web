module HotelBooker
  class PriceChange < Model
    attr_accessor :status,
                  :available_on_new_price

    def self.from_xml_h(hash)
      new(
        status: hash[:@status],
        available_on_new_price: hash[:@available_on_new_price]
      )
    end                    
  end
end
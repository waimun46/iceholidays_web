module HotelBooker
  class RoomRate < Model
    attr_accessor :is_package_rate,
                  :room_fare,
                  :room_tax,
                  :agent_mark_up,
                  :currency,
                  :total_fare,
                  :pref_currency,
                  :pref_price,
                  :b2c_rates,
                  :extra_guest_charges,
                  :child_charges,
                  :discount,
                  :other_charges,
                  :service_tax,
                  :day_rates

    def self.from_xml_h(hash)      
      return new unless hash.present?      
      obj = new(
        is_package_rate: hash[:@is_package_rate],
        room_fare:       hash[:@room_fare],
        room_tax:        hash[:@room_tax],            
        agent_mark_up:   hash[:@agent_mark_up],
        currency:        hash[:@currency],
        total_fare:      hash[:@total_fare],  
        pref_currency:   hash[:@pref_currency],
        pref_price:      hash[:@pref_price],
        b2c_rates:       hash[:@b2_c_rates],        
        child_charges:   hash[:child_charges],
        discount:        hash[:discount],
        other_charges:   hash[:other_charges],
        service_tax:     hash[:service_tax],
        extra_guest_charges: hash[:extra_guest_charges],
        day_rates:      Array.wrap(hash.dig(:day_rates,:day_rate)).map{ |day_rate|HotelBooker::DayRate.from_xml_h(day_rate) }   
      )
    end

    def to_h
      {
        is_package_rate: is_package_rate,
        room_fare:       room_fare,
        room_tax:        room_tax ,    
        agent_mark_up:   agent_mark_up,
        currency:        currency,
        total_fare:      total_fare,  
        pref_currency:   pref_currency,
        pref_price:      pref_price,
        b2c_rates:       b2c_rates,        
        child_charges:   child_charges,
        discount:        discount,
        other_charges:   other_charges,
        service_tax:     service_tax, 
        extra_guest_charges: extra_guest_charges,
        day_rates:      Array.wrap(day_rates).map{|x| x.to_h}
      }    
    end

    def to_xml_h
      {        
        '@RoomFare': room_fare,
        '@Currency': currency,
        '@PrefCurrency': pref_currency,
        '@PrefPrice': pref_price,
        '@AgentMarkUp': agent_mark_up,
        '@RoomTax': room_tax,
        '@TotalFare': total_fare,
      }    
    end
  end
end
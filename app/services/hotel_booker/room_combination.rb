module HotelBooker
  class RoomCombination < Model
    attr_accessor :room_index

    def self.from_xml_h(hash)
      new(room_index: hash[:room_index].is_a?(Array) ? hash[:room_index] : [hash[:room_index]])
    end
  end
end
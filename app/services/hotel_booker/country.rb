module HotelBooker
  class Country < Model
    attr_accessor :name,
                  :code,
                  :id
    def self.all(client = :tbo_holidays)
      HotelBooker.country_list(client)
    end
  end
end
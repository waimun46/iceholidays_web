module HotelBooker
  class AmendmentRequested < Model
    attr_accessor :check_in,
                  :check_out,
                  :rooms 
    
    def self.from_xml_h(hash)
      new(
        check_in: HotelBooker::CheckInRes.from_xml_h(hash[:check_in]),
        check_out: HotelBooker::CheckInRes.from_xml_h(hash[:check_out]),
        rooms: Array.wrap(hash.dig(:rooms,:room_res)).map{|x|  HotelBooker::RoomRes.from_xml_h(x)}    
      ) 
    end

    def to_h
      {
        check_in: check_in.to_h,
        check_out: check_out.to_h,
        rooms: Array.wrap(rooms).map{|x| x.to_h}
      }
    end
  end
end
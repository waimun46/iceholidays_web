module HotelBooker
  class HotelDetailsVerification < Model
    
    attr_accessor :status, 
                  :remarks

    def self.from_xml_h(hash)
      new(
        status: hash[:@status],
        remarks: hash[:@remarks]
      )
    end 
  end
end
module HotelBooker
  class AmendGuestRes < Model
    attr_accessor :action,                  
                  :guest_type,
                  :title,
                  :first_name,
                  :last_name,
                  :age

                  
    def to_xml_h
      {
        '@Action': action,
        '@GuestType': guest_type,
        '@Title': title,
        '@FirstName': first_name,
        '@LastName': last_name,
        '@Age': age
      }
    end

    def to_h
      {
        action: action,
        guest_type: guest_type,
        title: title,
        first_name: first_name,
        last_name: last_name,
        age: age
      }
    end

    def self.from_xml_h(hash)
      new(
        action: hash[:@action],
        guest_type: hash[:@guest_type],
        title: hash[:@title],
        first_name: hash[:@first_name],
        last_name: hash[:@last_name],
        age: hash[:@age]
      ) 
    end
  end
end
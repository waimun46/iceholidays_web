module HotelBooker
  class RoomReq < Model
    attr_accessor :amend,
                  :guest

    def guest_attributes=(attributes)
      @guest ||= []
      attributes.each do |idx,guest_params|        
        @guest << HotelBooker::AmendGuestReq.new(idx.is_a?(Hash) ? idx : guest_params)
      end
    end

    def to_xml_h
      {        
        'hot:RoomReq': {
          '@Amend': amend,
          :content! => Array.wrap(guest).map{|x| {'hot:Guest/': x.to_xml_h} }
        }
      }    
    end
  end
end
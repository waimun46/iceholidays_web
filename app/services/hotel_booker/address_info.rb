module HotelBooker
  class AddressInfo < Model
    attr_accessor :address_line_1,
                  :address_line_2,
                  :country_code,
                  :area_code,
                  :phone_no,
                  :email,
                  :city,
                  :state,
                  :country,
                  :zip_code
    def to_h             
      {        
        address_line_1: address_line_1 || '',
        address_line_2: address_line_2|| '',
        country_code: country_code|| '',
        area_code: area_code || '',
        phone_no: phone_no || '',
        email: email || '',
        city: city || '',
        state: state || '',
        country: country || '',
        zip_code: zip_code || '',
      }
    end

    def to_xml_h             
      {        
        'hot:AddressLine1': address_line_1 || '',
        'hot:AddressLine2': address_line_2|| '',
        'hot:CountryCode': country_code|| '',
        'hot:AreaCode': area_code || '',
        'hot:PhoneNo': phone_no || '',
        'hot:Email': email || '',
        'hot:City': city || '',
        'hot:State': state || '',
        'hot:Country': country || '',
        'hot:ZipCode': zip_code || '',
      }
    end
  end
end
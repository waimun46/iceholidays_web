module Travelport
  class FlightDetails < Base
    attr_accessor :key,
                  :origin,
                  :destination,
                  :departure_time,
                  :arrival_time,
                  :flight_time,
                  :travel_time,
                  :distance, #air_price
                  :equipment, #low_price_search
                  :origin_terminal, #low_price_search
                  :destination_terminal, #low_price_search
                  :automated_checkin #air_create_reservation

  end

end
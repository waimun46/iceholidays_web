module Travelport
  class Journey < Base
    attr_accessor :travel_time,
                  :air_segments

    def initialize(args = {})
      if args[:'@travel_time'].present?
        self.travel_time = parse_travel_time(args)
      end

      # low_price_search
      if args[:air_segment_list].present?
        self.air_segments = parse_air_segments(args)
      else
        self.air_segments = []
      end

    end

    def parse_travel_time(args = {})
      match_ret = args.fetch(:'@travel_time').match(/P(\d+)DT(\d+)H(\d+)M(\d+)S/)
      if match_ret.present?
        match_ret[1].to_i * 3600 * 24 + match_ret[2].to_i * 3600 + match_ret[3].to_i*60 + match_ret[4].to_i
      else
        nil
      end
    end

    def stop
      self.air_segments.count - 1
    end

    def parse_air_segments(args = {})
      air_segments = []
      air_segment_list = args.fetch(:air_segment_list, {})

      if air_segment_list.present?
        prev_air_segment = nil
        Array.wrap(args.fetch(:air_segment_ref, [])).each do |ref|
          key = ref.dig(:'@key')
          if key.present? && air_segment_list.has_key?(key)
            air_segment = air_segment_list[key].dup
            air_segments << air_segment
            air_segment.air_pricing_infos = args.fetch(:air_pricing_infos, nil)

            if prev_air_segment.present?
              prev_air_segment.next_air_segment = air_segment
              prev_air_segment.transit_time = air_segment.departure_time.to_time - prev_air_segment.arrival_time.to_time
            end

            prev_air_segment = air_segment
          end
        end
      end
      air_segments
    end

    def departure_segment
      self.air_segments.first
    end

    def arrival_segment
      self.air_segments.last
    end

    def add_air_segment(air_segment)
      self.air_segments << air_segment
    end

    def airports
      "#{self.departure_segment.origin_airport.iata} - #{self.arrival_segment.destination_airport.iata}"
    end

  end
end
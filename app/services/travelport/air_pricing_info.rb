module Travelport
  class AirPricingInfo < Base
    attr_accessor :key,
                  :total_price,
                  :base_price,
                  :approximate_total_price,
                  :approximate_base_price,
                  :equivalent_base_price, #air_create_reservation 
                  :taxes,
                  :approximate_taxes,
                  :latest_ticketing_time,
                  :true_last_date_to_ticket, #air_create_reservation 
                  :pricing_method,
                  :exchangeable, #air_create_reservation 
                  :includes_vat, #air_price
                  :baggage_allowance_infos, #air_price
                  :carry_on_allowance_infos, #air_price
                  :e_ticketability,
                  :plating_carrier,
                  :provider_code,
                  :cat35_indicator, #low_fare_search
                  :booking_infos, #low_fare_search, each segment has one
                  :passenger_code, #custom
                  :passenger_count, #custom
                  :change_penalty,
                  :cancel_penalty,
                  :final_base_price, #custom
                  :final_taxes, #custom
                  :final_price, #custom
                  :final_total_price, #custom
                  :final_commission #custom


    def initialize(args = {})
      super(args)

      # air_price / air_create_reservation
      if args[:fare_info].present?
        fare_info_list = parse_fare_info_list(args)
        self.booking_infos = parse_booking_infos(args.merge!(fare_info_list: fare_info_list))
      else #low_fare_search
        self.booking_infos = parse_booking_infos(args)
      end

      if args[:baggage_allowances].present?
        self.baggage_allowance_infos = parse_baggage_alloances(args)
      end

      calculate_passengers(args)
      calculate_final_price

    end

    def parse_fare_info_list(args = {})
      fare_info_list = Hash.new
      Array.wrap(args.fetch(:fare_info, [])).each do |fare_info|
        fare_info_list[fare_info[:@key]] = FareInfo.new(fare_info)
      end
      fare_info_list
    end

    def parse_booking_infos(args = {})
      fare_info_list = args.fetch(:fare_info_list, {})
      Array.wrap(args.fetch(:booking_info, [])).map{ |booking_info| BookingInfo.new(booking_info.merge!(fare_info_list: fare_info_list)) }
    end

    def parse_baggage_alloances(args = {})
      Array.wrap(args[:baggage_allowances].fetch(:baggage_allowance_info, [])).map{ |baggage_allowance_info| BaggageAllowanceInfo.new(baggage_allowance_info) }
    end

    def calculate_passengers(args = {})
      passenger_arr = Array.wrap(args.fetch(:passenger_type, []))
      if passenger_arr.present?
        self.passenger_code = passenger_arr.first[:'@code']
        self.passenger_count = passenger_arr.count
      end
    end

    def airline
      Airline.find_by_iata_code(self.plating_carrier)
    end

    def flight_commission
      FlightCommission.find_by_airline_iata_and_origin_airport_iata(self.plating_carrier, self.booking_infos.first.fare_info.origin )
    end 

    def calculate_final_price
      if self.approximate_base_price.present?

        self.final_commission = if flight_commission.present?
          flight_commission.calculate_commission(self.approximate_base_price.gsub(/[^\d\.]/, '').to_f)
        else
          0
        end

        self.final_base_price = self.approximate_base_price.gsub(/[^\d\.]/, '').to_f + self.final_commission

        self.final_base_price = if airline.present?
          airline.final_price(self.final_base_price, booking_infos.first.cabin_class.downcase)
        else
          Airline.find_by_name('Unknown').final_price(self.final_base_price, booking_infos.first.cabin_class.downcase)
        end
        
        self.final_taxes = (self.approximate_taxes || self.taxes).gsub(/[^\d\.]/, '').to_f.ceil
        self.final_price = self.final_base_price + self.final_taxes
        self.final_total_price = self.final_price * self.passenger_count
      end
    end

    def baggage_allowance(carrier, origin, destination)
      if self.baggage_allowance_infos.present?
        found = self.baggage_allowance_infos.find{|bai| bai.traveler_type == "ADT" && bai.carrier == carrier && (bai.origin == origin || bai.destination == destination) }
        if found.present? && found.texts.present?
          if found.texts.first.include?('P')
            piece = found.texts.first.scan(/\d/).join('').to_i
            if piece == 0
              found.texts.first.sub('P', 'PC')
            else
              found.texts.first.sub('P', "#{'PC'.pluralize(piece)} (23KG/PC)")
            end
          else
            found.texts.first.sub('K', 'KG')
          end
        else
          nil
        end
      else
        nil
      end
    end

  end
end
module Travelport
  class FareInfo < Base
    attr_accessor :key,
                  :fare_basis,
                  :passenger_type_code,
                  :origin,
                  :destination,
                  :effective_date,
                  :departure_date,
                  :amount,
                  :negotiated_fare,
                  :not_valid_before,
                  :not_valid_after,
                  :tax_amount, #air_price
                  :baggage_number_of_pieces, #low_price_search
                  :baggage_max_weight, #low_price_search
                  :baggage_unit, #low_price_search
                  :number_of_pieces, #low_price_search
                  :brand
                  # :private_fare="AirlinePrivateFare"

    def initialize(args = {})
      super(args)

      if args[:baggage_allowance].present?
        self.baggage_number_of_pieces = args[:baggage_allowance].dig(:number_of_pieces)
        self.baggage_max_weight = args[:baggage_allowance].dig(:max_weight, :@value)
        self.baggage_unit = args[:baggage_allowance].dig(:max_weight, :@unit)
      end

      if args[:brand].present?
        self.brand = Brand.new(args[:brand])
      end

    end

    def baggage_allowance
      if self.baggage_max_weight.present?
        "#{self.baggage_max_weight} #{self.baggage_unit.sub('Kilograms', 'KG')}"
      elsif self.baggage_number_of_pieces.present?
        if self.baggage_number_of_pieces.to_i > 0
          "#{self.baggage_number_of_pieces} #{'PC'.pluralize(self.baggage_number_of_pieces.to_i)} (23KG/PC)"
        else
          "#{self.baggage_number_of_pieces} PC"
        end
      else
        ""
      end

    end

  end
end
module Travelport
  class BaggageAllowanceInfo < Base
    attr_accessor :carrier,
                  :origin,
                  :destination,
                  :traveler_type,
                  :texts

    def initialize(args = {})
      super(args)

      if args[:text_info].present? && args[:text_info][:text].present?
        self.texts = args[:text_info][:text]
      else
        self.texts = []
      end

    end
  end
end
module Travelport
  class AirSolution
  # include Caching

    def self.where(params = {})
      # Rails.logger.info params
      response = Travelport::Connection.low_fare_search(params)
      if response.present? && response.success && response.solutions.present?
        [response.solutions.dup, response.airlines.dup]
      else
        [nil, nil]
      end
    end

    def self.find(params = {})
      # Rails.logger.info params
      response = Travelport::Connection.air_price(params)
      if response.present? && response.success && response.solutions.present?
        response.solutions.first
      else
        nil
      end
    end

    def self.reservation(params = {})
      response = Travelport::Connection.air_create_reservation(params)
      if response.success && response.reservation.present?

        solution = params[:solution]
        agent_rep = params[:agent_rep]
        user = params[:user]


        if agent_rep == nil
          agent_user = user
        else
          agent_user = User.find_by_username(agent_rep)
        end

        flight_booking_group = FlightBookingGroup.new(
                                universal_code: response.universal_code,
                                host_code: response.host_code,
                                uAPI_code: response.uAPI_code,
                                price: solution.final_total_price
                              )

        response.reservation.air_segments.each do |air_segment|
          flight_booking_group.flight_booking_details << FlightBookingDetail.new(
                                                           group: air_segment.group,
                                                           carrier: air_segment.carrier,
                                                           cabin_class: air_segment.cabin_class,
                                                           flight_number: air_segment.flight_number,
                                                           provider_code: air_segment.provider_code,
                                                           origin: air_segment.origin,
                                                           origin_terminal: air_segment.flight_details.first.origin_terminal,
                                                           destination: air_segment.destination,
                                                           destination_terminal: air_segment.flight_details.last.destination_terminal,
                                                           departure_time: air_segment.departure_time,
                                                           arrival_time: air_segment.arrival_time,
                                                           travel_time: air_segment.travel_time,
                                                           class_of_service: air_segment.class_of_service,
                                                           e_ticketability: air_segment.e_ticketability,
                                                           equipment: air_segment.equipment,
                                                           status: air_segment.status,
                                                           change_of_plane: air_segment.change_of_plane,
                                                           guaranteed_payment_carrier: air_segment.guaranteed_payment_carrier,
                                                           # provider_reservation_info_ref: air_segment.provider_reservation_info_ref,
                                                           travel_order: air_segment.travel_order,
                                                           provider_segment_order: air_segment.provider_segment_order,
                                                           optional_services_indicator: air_segment.optional_services_indicator,
                                                           participant_level: air_segment.participant_level,
                                                           link_availability: air_segment.link_availability,
                                                           el_stat: air_segment.el_stat,
                                                           automated_checkin: air_segment.flight_details.first.automated_checkin,
                                                           connection: air_segment.connection,
                                                           sell_message: air_segment.sell_messages.present? ? air_segment.sell_messages.join("\n") : '',
                                                        )
        end

        brb_purchase_list = []
        brb_price = 0
        response.reservation.booking_travelers.each_with_index do |traveler, index|
          
          flight_booking = FlightBooking.new(
                                              designation: traveler.prefix,
                                              first_name: traveler.first_name,
                                              last_name: traveler.last_name,
                                              mobile: traveler.mobile,
                                              category: FlightBooking.category_mapping(traveler.traveler_type)
                                            )

          if params[:flight_booking_params][index][:first_name] == flight_booking.first_name &&
              params[:flight_booking_params][index][:last_name] == flight_booking.last_name &&
              FlightBooking.category_mapping(params[:flight_booking_params][index][:category]) == flight_booking.category

            flight_booking.country = params[:flight_booking_params][index][:country]
            flight_booking.passport_number = params[:flight_booking_params][index][:passport_number]
            flight_booking.passport_issue_date = params[:flight_booking_params][index][:passport_issue_date]
            flight_booking.passport_expiry_date = params[:flight_booking_params][index][:passport_expiry_date]
            flight_booking.date_of_birth = params[:flight_booking_params][index][:date_of_birth]
            flight_booking.remark = params[:flight_booking_params][index][:remark]
          end
          air_price_info = solution.air_pricing_infos.find {|p| p.passenger_code == traveler.traveler_type }
          if air_price_info.present?
            flight_booking.base = air_price_info.final_base_price
            flight_booking.taxes = air_price_info.final_taxes
            flight_booking.price = air_price_info.final_base_price + air_price_info.final_taxes
          end
          flight_booking.agent_user = agent_user
          flight_booking.sale_rep = agent_rep
          flight_booking.initiator_user = user

          if params[:flight_booking_params][index][:brb] == "Yes"
            flight_booking.brb_price = BrbBooking::PRICE
            brb_purchase_list << flight_booking
            brb_price += BrbBooking::PRICE
          end

          flight_booking_group.flight_bookings << flight_booking
        end

        flight_booking_group.price += brb_price

        flight_booking_group.save

        if brb_purchase_list.present?
          BrbBookingGroup.create_booking_by_flight_bookings(brb_purchase_list, user, agent_rep)
        end

        flight_booking_group
      else
        nil
      end
    end

  end
end
module Travelport
  class AirReservation < Base
    attr_accessor :locator_code,
                  :create_date,
                  :modified_date,
                  :air_segments,
                  :booking_travelers,
                  :air_pricing_infos

    def initialize(args = {})
      super(args)

      if args[:air_pricing_info].present?
        self.air_pricing_infos = parse_air_pricing_infos(args)
      end

      if args[:air_segment].present?
        self.air_segments = parse_air_segments(args)
      end

      if args[:booking_traveler_ref].present?
        self.booking_travelers = parse_booking_travelers(args)
      end

    end

    def parse_air_segments(args={})
      Array.wrap(args.fetch(:air_segment, [])).map do |air_segment|
        AirSegment.new(air_segment.merge!(air_pricing_infos: self.air_pricing_infos))
      end
    end

    def parse_air_pricing_infos(args = {})
      Array.wrap(args.fetch(:air_pricing_info, [])).map do |air_pricing_info|
        AirPricingInfo.new(air_pricing_info)
      end
    end

    def parse_booking_travelers(args = {})
      booking_travelers = []
      booking_traveler_list = args.fetch(:booking_traveler_list, {})

      if booking_traveler_list.present?
        Array.wrap(args.fetch(:booking_traveler_ref, [])).each do |ref|
          key = ref.dig(:'@key')
          if key.present? && booking_traveler_list.has_key?(key)
            booking_travelers << booking_traveler_list[key]
          end
        end
      end
      booking_travelers
    end

  end
end
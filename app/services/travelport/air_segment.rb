module Travelport
  class AirSegment < Base
    attr_accessor :key,
                  :group,
                  :carrier,
                  :cabin_class, # air_create_reservation
                  :flight_number,
                  :origin,
                  :destination,
                  :departure_time,
                  :arrival_time,
                  :flight_time,
                  :travel_time, #air_price / air_create_reservation
                  :distance,
                  :codeshare_info, #air_price
                  :class_of_service, #air_price / air_create_reservation
                  :e_ticketability, #low_price_search
                  :equipment,
                  :status, #air_create_reservation
                  :guaranteed_payment_carrier, #air_create_reservation
                  :travel_order, #air_create_reservation
                  :provider_segment_order, #air_create_reservation
                  :optional_services_indicator, #air_create_reservation
                  :el_stat, #air_create_reservation
                  :change_of_plane,
                  :participant_level,
                  :link_availability,
                  :polled_availability_option,
                  :optional_services_indicator,
                  :availability_source, #low_price_search
                  :availability_display_type,
                  :provider_code,
                  :operating_carrier, #air_price
                  :flight_details,
                  :sell_messages, #air_create_reservation
                  :air_pricing_infos, #custom
                  :next_air_segment, #custom
                  :transit_time, #custom
                  :connection #custom


    def initialize(args = {})
      super(args)

      #low_price_search / provider_code
      if args[:air_avail_info].present?
        super(args[:air_avail_info])
      end

      # air_price / operating_carrier
      if args[:codeshare_info].present?
        # super(args[:codeshare_info])
        self.class_of_service = args.fetch(:codeshare_info, :class_of_service)
        self.codeshare_info = args.fetch(:codeshare_info)
      end

      # low_price_search
      if args[:flight_details_ref].present?
        self.flight_details = []
        flight_details_list = args[:flight_details_list]
        Array.wrap(args[:flight_details_ref]).each do |flight_details_ref|
          flight_key = flight_details_ref.dig(:'@key')
          if flight_key.present? && flight_details_list.has_key?(flight_key)
            self.flight_details << flight_details_list[flight_key]
          end
        end
      end

      # air_price
      if args[:flight_details].present?
        self.flight_details = []
        Array.wrap(args[:flight_details]).each do |flight_detail|
          self.flight_details << FlightDetails.new(flight_detail)
        end
      end


      if self.departure_time.present?
        self.departure_time = DateTime.parse(self.departure_time)
      end

      if self.arrival_time.present?
        self.arrival_time = DateTime.parse(self.arrival_time)
      end

      # air_price / connection
      if args.has_key?(:connection)
        self.connection = true
      end

      # air_create_reservation
      if args[:sell_message].present?
        self.sell_messages = Array.wrap(args.fetch(:sell_message, []))
      end


    end

    def booking_info
      if self.air_pricing_infos.present?
        self.air_pricing_infos.select{|air_pricing_info| air_pricing_info.passenger_code == 'ADT' || air_pricing_info.passenger_code == 'CNN'}.first
          .booking_infos.select{|booking_info| booking_info.segment_ref == self.key }.first
      else
        nil
      end
    end

    def airline
      Airline.find_by_iata_code(self.carrier)
    end

    def airline_name
      if airline.present? && airline.logo.present?
        airline.name
      elsif self.codeshare_info.present?
        self.codeshare_info
      else
        self.carrier
      end
    end

    def airline_image_url
      if airline.present? && airline.logo.present?
        airline.logo_url
      else
        "http://pics.avs.io/200/200/#{self.carrier}@2x.png"
      end
    end

    def origin_airport
      Airports.find_by_iata_code(self.origin)
    end

    def destination_airport
      Airports.find_by_iata_code(self.destination)
    end

    def aircraft
      Aircraft.aircraft_name(self.equipment)
    end

    def baggage_allowance
      # air_price
      baggage_allowance = self.air_pricing_infos.first.baggage_allowance(self.carrier, self.origin, self.destination)
      if baggage_allowance.blank?
        # low_price_search
        baggage_allowance = self.booking_info.fare_info.baggage_allowance
      end
      baggage_allowance
    end

  end
end
module Travelport
  class Connection

    if Rails.env.iceb2b?
      URL_PATH = Rails.application.credentials.travelport[:url]
      AUTH = {:username => Rails.application.credentials.travelport[:username], :password => Rails.application.credentials.travelport[:password]}
      TARGET_BRANCH = Rails.application.credentials.travelport[:target_branch]
    else
      URL_PATH = "https://apac.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/"
      AUTH = {:username => "Universal API/uAPI5095459404-60379594", :password => "aN&7L5x*%b"}
      TARGET_BRANCH = "P7082441"
    end

    class << self
      def httpreq(request, timeout=0)
        xml = request.to_xml
        log_request(request.class.name, xml)     
        conn = Faraday.new(:url=> request.url ) do |faraday|
          faraday.response :logger
          faraday.adapter  Faraday.default_adapter
        end
        conn.basic_auth(AUTH[:username], AUTH[:password])

        cache_data = REDIS.get request.cache_key
        if cache_data != nil
          response = Marshal.load(cache_data)
        else
          response  = conn.post do |request|
            request.headers['Content-Type'] = 'text/xml; charset=utf-8'
            # request.headers['Accept-Encoding'] = 'gzip,deflate'
            request.headers['SOAPAction'] = ''
            request.body = xml
          end
        end

        xml_parser = Nori.new(:convert_tags_to => lambda { |tag| tag.snakecase.to_sym }, strip_namespaces: true)
        response_hash = xml_parser.parse(response.body)
        # Rails.logger.info response.body
        Rails.logger.info response_hash

        message = error_code = response_id = body = ''
        success = false

        if response.status == 200
          response_id, body  = body_from(response_hash)
          error_code = body.dig(:fault, :faultcode)
          if error_code.present?
            error_message = body.dig(:fault, :faultstring)
            error_detail = body.dig(:fault, :detail)
            success = false
          else
            success = true

            if cache_data == nil && request.cache_duration > 0
              REDIS.setex(request.cache_key, request.cache_duration, Marshal.dump(response))
            end

          end
        else
          success = false
          error_code = "Request Error"
        end
        
        klass = response_id.in?(Response::RESPONSE_IDS) ?  Response::RESPONSE_IDS[response_id] : Response
        obj = klass.new(body.dig(response_id) || {}, success, {code: error_code, message: error_message, detail: error_detail})
        return obj
      end

      def ping
        request = PingRequest.new
        response = httpreq(request)
      end

      def low_fare_search(params = {})
        trips = params.delete(:trips)
        adults = params.delete(:adults).to_i || 1
        children = params.delete(:children).to_i || 0
        infants = params.delete(:infants).to_i || 0
        cabin_class = params.delete(:cabin_class) || 'Economy'

        trips.each do |trip|
          if trip.has_key?('depart')
            trip[:depart] = Date.parse(trip[:depart]).strftime("%F")
          end
        end

        Rails.logger.info "#{self.class.name}-#{trips}-#{adults}-#{children}-#{infants}-#{cabin_class}"

        request = LowFareSearchRequest.new(
          trips: trips,
          adults: adults,
          children: children,
          infants: infants,
          cabin_class: cabin_class
        )

        response = httpreq(request)
      end

      def air_price(params = {})
        trips = params.delete(:trips)
        adults = params.delete(:adults).to_i || 1
        children = params.delete(:children).to_i || 0
        infants = params.delete(:infants).to_i || 0
        cabin_class = params.delete(:cabin_class) || 'Economy'
        key = params.delete(:id) || nil

        trips.each do |trip|
          if trip.has_key?('depart')
            trip[:depart] = Date.parse(trip[:depart]).strftime("%F")
          end
        end

        result = Connection.low_fare_search({trips: trips, adults: adults, children: children, infants: infants, cabin_class: cabin_class})

        if result.solutions.present?
          solution = result.solutions.find {|s| s.key == key }
          if solution.present?

            request = AirPriceRequest.new(
              trips: trips,
              adults: adults,
              children: children,
              infants: infants,
              cabin_class: cabin_class,
              solution: solution
            )

            response = httpreq(request)
          else
            nil
          end
        else
          nil
        end

      end

      def air_create_reservation(params = {})
        solution = params[:solution] || nil
        flight_booking_params = params[:flight_booking_params] || []

        if solution.present?
          request = AirCreateReservationRequest.new(
            solution: solution,
            flight_booking_params: flight_booking_params
          )

          response = httpreq(request)
        else
          nil
        end
      end

      private
      def log_request(action, body = nil)
        return unless Rails.env.development?
        Rails.logger.info "\n========================================================="            
        Rails.logger.info "Requested Action: " + action
        Rails.logger.info "\nRequest Body: \n" + body if body.present?
        Rails.logger.info "=========================================================" 
      end

      def body_from(response)
        response_id = (response.dig(:envelope, :body) || {}).keys.first
        body = response.dig(:envelope, :body) || {}
        [response_id, body]
      end
    end

    class Request
      include ActiveModel::Model
      SOAP_ENVELOPE_URL  = 'http://schemas.xmlsoap.org/soap/envelope/'
      ADDRESSING_URL     = 'http://www.w3.org/2005/08/addressing'        
      
      attr_accessor :username,
                    :password  
      def to_xml
        Gyoku.xml({
          'soapenv:Envelope':{
            '@xmlns:soapenv': SOAP_ENVELOPE_URL,
            'soapenv:Body': body,
          }
        }, {key_converter: :none, unwrap: [:AirPricingSolution, :AirPricingCommand, :AirItinerary, :AirPricingInfo]} )
      end

      private
      def action   
        self.class.name.demodulize.gsub('Request', '')
      end

      def url
        URL_PATH
      end

      def body
        {}
      end
    end

    class Response
        RESPONSE_IDS = {}        
        attr_accessor :success,
                      :error
        def initialize(response_body, success, error = {})
          @success, @error, =  success, error 
          generate_data_from(response_body) if success
        end
      end

    class PingRequest < Request

      def url
        URL_PATH+'SystemService'
      end

      def cache_key
        "#{self.class.name}"
      end

      def cache_duration
        0
      end

      private
      def body
        {
          PingReq: { 
            '@TraceId': 'Test', 
            '@xmlns': 'http://www.travelport.com/schema/system_v32_0', 
            'BillingPointOfSaleInfo/': {
              '@xmlns': 'http://www.travelport.com/schema/common_v32_0', 
              '@OriginApplication':'uAPI', 
              content!: ''},
            Payload: 'User and password are correct.'
          }
        }
      end

      
    end

    class PingResponse < Response        
      RESPONSE_IDS[:ping_rsp] = self  
      attr_accessor :response_time,
                    :data

      private
      def generate_data_from(response_body)
        @response_time = response_body.dig(:'@response_time')
        @data = response_body.dig(:payload)
      end
    end

    class LowFareSearchRequest < Request
      attr_accessor :trips,
                    :adults,
                    :children,
                    :infants,
                    :cabin_class

      def url
        URL_PATH+'AirService'
      end

      def cache_key
        "#{self.class.name}-#{trips}-#{adults}-#{children}-#{infants}-#{cabin_class}"
      end

      def cache_duration
        if Rails.env.iceb2b?
          600
        else
          14400
        end
      end

      private
      def body
        {
          LowFareSearchReq: { 
            '@TraceId': 'Test', 
            '@xmlns': 'http://www.travelport.com/schema/air_v46_0', 
            '@TargetBranch': TARGET_BRANCH,
            '@SolutionResult': 'true',
            'BillingPointOfSaleInfo/': {
              '@xmlns': 'http://www.travelport.com/schema/common_v46_0', 
              '@OriginApplication':'uAPI', 
              content!: ''},
            SearchAirLeg: search_air_leg,
            AirSearchModifiers: {
              PreferredProviders: {
                'Provider/': {
                  '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
                  '@Code': '1G',
                  content!: ''
                }
              }
            },
            'SearchPassenger/': passengers,
            AirPricingModifiers: {
              AccountCodes: {
                'AccountCode/': {
                  '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
                  '@Code': '-',
                  content!: ''
                }
              }
            }
          }
        }
      end

      def search_air_leg
        search_air_leg = []
        trips.each do |trip|
          search_air_leg << {
            SearchOrigin: {
              'CityOrAirport/': {
              # 'Airport/': {
                '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
                '@Code': trip[:from].split(',')[0],
                '@PreferCity': 'true',
                content!: ''
              }
            },
            SearchDestination: {
              'CityOrAirport/': {
              # 'Airport/': {
                '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
                '@Code': trip[:to].split(',')[0],
                '@PreferCity': 'true',
                content!: ''
              }
            },
            'SearchDepTime/': {
              '@PreferredTime': trip[:depart],
              content!: ''
            },
            AirLegModifiers: {
              PreferredCabins: {
                'CabinClass/': {
                  '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
                  '@Type': cabin_class,
                  content!: ''
                }
              }
              # 'FlightType/': {
              #   '@NonStopDirects': 'true',
              #   content!: ''
              # }
            }

          }
        end
        search_air_leg
      end

      def passengers
        passengers = []
        adults.times do
          passengers << {
            '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
            '@Code': 'ADT',
            content!: ''
          }
        end
        children.times do
          passengers << {
            '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
            '@Code': 'CNN',
            '@Age': '8',
            content!: ''
          }
        end
        infants.times do
          passengers << {
            '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
            '@Code': 'INF',
            '@Age': '1',
            content!: ''
          }
        end
        passengers
      end
    end

    class LowFareSearchResponse < Response        
      RESPONSE_IDS[:low_fare_search_rsp] = self  
      attr_accessor :response_time,
                    :distance_units,
                    :currency_type,
                    :airlines,
                    :solutions

      private
      def generate_data_from(response_body)
        @response_time = response_body.dig(:'@response_time')
        @distance_units = response_body.dig(:'@distance_units')
        @currency_type = response_body.dig(:'@currency_type')
        @airlines = Set.new
        @solutions = []

        flight_details_list = Hash.new
        air_segment_list = Hash.new
        fare_info_list = Hash.new
        # route_list = Hash.new
        # brand_list = Hash.new

        Array.wrap(response_body.dig(:flight_details_list, :flight_details) || []).each do |flight_details|
          flight_details_list[flight_details[:@key]] = FlightDetails.new(flight_details)
        end

        Array.wrap(response_body.dig(:air_segment_list, :air_segment) || []).each do |air_segment|
          air_segment_list[air_segment[:@key]] = AirSegment.new(air_segment.merge!(flight_details_list: flight_details_list))
        end

        Array.wrap(response_body.dig(:fare_info_list, :fare_info) || []).each do |fare_info|
          fare_info_list[fare_info[:@key]] = FareInfo.new(fare_info)
        end

        Array.wrap(response_body.dig(:air_pricing_solution) || []).each do |solution|
          air_pricing_solution = AirPricingSolution.new(solution.merge!(air_segment_list: air_segment_list, fare_info_list: fare_info_list))
          airline = air_pricing_solution.airline
          if airline.blank? || (airline.present? && !airline.block?)
            @airlines << airline if airline.present?
            @solutions << air_pricing_solution
          end
        end

      end
    end

    class AirPriceRequest < Request
      attr_accessor :trips,
                    :adults,
                    :children,
                    :infants,
                    :cabin_class,
                    :solution

      def url
        URL_PATH+'AirService'
      end

      def cache_key
        "#{self.class.name}-#{trips}-#{solution.key}-#{adults}-#{children}-#{infants}-#{cabin_class}"
      end

      def cache_duration
        if Rails.env.iceb2b?
          60
        else
          14400
        end
      end

      private
      def body
        {
          AirPriceReq: { 
            '@TraceId': 'Test', 
            '@xmlns': 'http://www.travelport.com/schema/air_v46_0', 
            '@TargetBranch': TARGET_BRANCH,
            # '@CheckFlightDetails': 'true',
            'BillingPointOfSaleInfo/': {
              '@xmlns': 'http://www.travelport.com/schema/common_v46_0', 
              '@OriginApplication':'uAPI', 
              content!: ''},
            AirItinerary: air_segments,
            AirPricingModifiers: {
              '@InventoryRequestType': 'DirectAccess'
            },
            'SearchPassenger/': passengers,
            AirPricingCommand: air_segment_pricing
          }
        }
      end

      def passengers
        passengers = []
        adults.times do |index|
          passengers << {
            '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
            '@Code': 'ADT',
            '@BookingTravelerRef': "ADT#{index+1}",
            content!: ''
          }
        end
        children.times do |index|
          passengers << {
            '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
            '@Code': 'CNN',
            '@BookingTravelerRef': "CNN#{index+1}",
            '@Age': '8',
            content!: ''
          }
        end
        infants.times do |index|
          passengers << {
            '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
            '@Code': 'INF',
            '@BookingTravelerRef': "INF#{index+1}",
            '@Age': '1',
            content!: ''
          }
        end
        passengers
      end

      def air_segments
        air_segments = []
        solution.journeys.each do |journey|
          journey.air_segments.each_with_index do |air_segment, index|
            air_segments << {
              'AirSegment/': air_segment.construct_hash([
                :key,
                :group,
                :carrier,
                :flight_number,
                :origin,
                :destination,
                :departure_time,
                :arrival_time,
                :flight_time,
                :distance,
                :equipment,
                :provider_code
              ]).merge!({
                '@ClassOfService': air_segment.booking_info.booking_code,
                content!: index == 0 ? '' : { 'Connection/': {content!: ''} }
              })
              # {
              #   '@Key': air_segment.key,
              #   '@Group': air_segment.group,
              #   '@Carrier': air_segment.carrier,
              #   '@FlightNumber': air_segment.flight_number,
              #   '@Origin': air_segment.origin,
              #   '@Destination': air_segment.destination,
              #   '@DepartureTime': air_segment.departure_time,
              #   '@ArrivalTime': air_segment.arrival_time,
              #   '@FlightTime': air_segment.flight_time,
              #   '@Distance': air_segment.distance,
              #   '@Equipment': air_segment.equipment,
              #   '@ProviderCode': air_segment.provider_code,
              #   # '@ETicketability': air_segment.e_ticketability,
              #   # '@ChangeOfPlane': air_segment.change_of_plane,
              #   # '@ParticipantLevel': air_segment.participant_level,
              #   # '@LinkAvailability': air_segment.link_availability,
              #   # '@PolledAvailabilityOption': air_segment.polled_availability_option,
              #   # '@OptionalServicesIndicator': air_segment.optional_services_indicator,
              #   # '@AvailabilitySource': air_segment.availability_source,
              #   # '@AvailabilityDisplayType': air_segment.availability_display_type,
              #   '@ClassOfService': air_segment.booking_info.booking_code,
              #   content!: index == 0 ? '' : { 'Connection/': {content!: ''} }
              # }
            }
          end
        end
        air_segments
      end

      def air_segment_pricing
        air_segment_pricing = []
        solution.journeys.each do |journey|
          journey.air_segments.each do |air_segment|
            air_segment_pricing << {
              AirSegmentPricingModifiers: {
                '@AirSegmentRef': air_segment.key,
                PermittedBookingCodes: {
                  'BookingCode/': {
                    '@Code': air_segment.booking_info.booking_code,
                    content!: ''
                  }
                }
              }
            }
          end
        end
        air_segment_pricing
      end
    end

    class AirPriceResponse < Response        
      RESPONSE_IDS[:air_price_rsp] = self  
      attr_accessor :response_time,
                    :solutions

      private
      def generate_data_from(response_body)
        @response_time = response_body.dig(:'@response_time')
        @solutions = []
        air_segment_list = Hash.new

        Array.wrap(response_body.dig(:air_itinerary, :air_segment) || []).each do |air_segment|
          air_segment_list[air_segment[:@key]] = AirSegment.new(air_segment)
        end

        Array.wrap(response_body.dig(:air_price_result, :air_pricing_solution) || []).each do |solution|
          air_pricing_solution = AirPricingSolution.new(solution.merge!(air_segment_list: air_segment_list))
          airline = air_pricing_solution.airline
          if airline.blank? || (airline.present? && !airline.block?)
            @solutions << air_pricing_solution
          end
        end

      end
    end

    class AirCreateReservationRequest < Request
      attr_accessor :solution,
                    :flight_booking_params,
                    :traveler_keys

      def url
        URL_PATH+'AirService'
      end

      def cache_key
        "#{self.class.name}-#{solution.key}"
      end

      def cache_duration
        0
      end

      private
      def body
        {
          AirCreateReservationReq: { 
            '@TraceId': 'Test', 
            '@xmlns': 'http://www.travelport.com/schema/universal_v46_0',
            '@TargetBranch': TARGET_BRANCH,
            '@RetainReservation': Rails.env.iceb2b? ? 'None' : 'Schedule',
            '@RestrictWaitlist': 'true',
            'BillingPointOfSaleInfo/': {
              '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
              '@OriginApplication':'uAPI',
              content!: ''},
            BookingTraveler: travelers,
            AirPricingSolution: 
              solution.construct_hash([
                :key,
                :total_price,
                :base_price,
                :approximate_total_price,
                :approximate_base_price,
                :taxes,
                :approximate_taxes,
                :quote_date
              ]).merge!({
                '@xmlns': 'http://www.travelport.com/schema/air_v46_0',
                content!: air_segments + air_pricing_infos
              }),
            # {
            #   '@Key': solution.key,
            #   '@TotalPrice': solution.total_price,
            #   '@BasePrice': solution.base_price,
            #   '@ApproximateTotalPrice': solution.approximate_total_price,
            #   '@ApproximateBasePrice': solution.approximate_base_price,
            #   '@Taxes': solution.taxes,
            #   '@ApproximateTaxes': solution.approximate_taxes,
            #   '@QuoteDate': solution.quote_date,
            #   '@xmlns': 'http://www.travelport.com/schema/air_v46_0',
            #   content!: air_segments + air_pricing_infos
            # },
            ActionStatus: {
              '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
              '@ProviderCode': '1G',
              '@Type': 'ACTIVE'
            }
          }
        }
      end

      def travelers
        travelers = []
        now = Time.now
        self.traveler_keys = []

        flight_booking_params.each_with_index do |flight_booking, index|
          age = ((now - Date.parse(flight_booking[:date_of_birth]).to_time)/ 1.year.seconds).floor
          self.traveler_keys << "#{flight_booking[:category]}#{index+1}"
          traveler = {
            '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
            '@TravelerType': flight_booking[:category],
            '@Key': "#{flight_booking[:category]}#{index+1}",
            '@Age': age,
            '@DOB': Date.parse(flight_booking[:date_of_birth]).strftime('%F'),
            content!: '',
            BookingTravelerName: {
              '@First': flight_booking[:first_name],
              '@Last': flight_booking[:last_name],
              '@Prefix': flight_booking[:designation],
              content!: ''
            },
            PhoneNumber: {
              '@Number': flight_booking[:mobile],
              content!: ''
            }
          }

          if flight_booking[:passport_number].present? && flight_booking[:passport_expiry_date].present?
            traveler[:SSR] = {
              '@Type': 'DOCS',
              '@Status': 'HK',
              '@Carrier': "#{solution.journeys.first.air_segments.first.carrier}",
              '@FreeText': "P/#{flight_booking[:country]}/#{flight_booking[:passport_number]}/#{flight_booking[:country]}/#{Date.parse(flight_booking[:date_of_birth]).strftime("%d%^b%y")}/#{gender_mapping[flight_booking[:designation]]}/#{Date.parse(flight_booking[:passport_expiry_date]).strftime("%d%^b%y")}/#{flight_booking[:first_name]}/#{flight_booking[:last_name]}",
              # '@FreeText': "P////#{flight_booking[:passport_number]}/#{flight_booking[:country]}/#{Date.parse(flight_booking[:date_of_birth]).strftime("%d%^b%y")}/#{gender_mapping[flight_booking[:designation]]}/#{Date.parse(flight_booking[:passport_expiry_date]).strftime("%d%^b%y")}/#{flight_booking[:first_name]}/#{flight_booking[:last_name]}",
              content!: ''
            }
          end

          travelers << traveler

          if flight_booking["category"] == 'CNN'
            travelers.last[:NameRemark] = {
              '@Category': 'AIR',
              RemarkData: {
                content!: "P-C#{age}"
              }
            }
          end

        end

        travelers
      end

      def gender_mapping
        {
          "Miss" => "F",
          "Mr" => "M",
          "Mrs" => "F",
          "Ms" => "F",
          "Mstr" => "M",
          "Mdm" => "F"
        }
      end

      def air_segments
        air_segments = []
        solution.journeys.each do |journey|
          journey.air_segments.each_with_index do |air_segment, index|
            air_segments << {
              'AirSegment/': air_segment.construct_hash([
                :key,
                :group,
                :carrier,
                :flight_number,
                :origin,
                :destination,
                :departure_time,
                :arrival_time,
                :flight_time,
                :distance,
                :equipment,
                :polled_availability_option,
                :provider_code
              ]).merge!({
                '@ClassOfService': air_segment.booking_info.booking_code,
                content!: index == 0 ? '' : { 'Connection/': {content!: ''} }
              })

              # {
              #   '@Key': air_segment.key,
              #   '@Group': air_segment.group,
              #   '@Carrier': air_segment.carrier,
              #   '@FlightNumber': air_segment.flight_number,
              #   '@Origin': air_segment.origin,
              #   '@Destination': air_segment.destination,
              #   '@DepartureTime': air_segment.departure_time,
              #   '@ArrivalTime': air_segment.arrival_time,
              #   '@FlightTime': air_segment.flight_time,
              #   '@Distance': air_segment.distance,
              #   '@Equipment': air_segment.equipment,
              #   '@PolledAvailabilityOption': air_segment.polled_availability_option,
              #   '@ProviderCode': air_segment.provider_code,
              #   '@ClassOfService': air_segment.booking_info.booking_code,
              #   # '@ETicketability': air_segment.e_ticketability,
              #   # '@ChangeOfPlane': air_segment.change_of_plane,
              #   # '@ParticipantLevel': air_segment.participant_level,
              #   # '@LinkAvailability': air_segment.link_availability,
              #   # '@OptionalServicesIndicator': air_segment.optional_services_indicator,
              #   # '@AvailabilitySource': air_segment.availability_source,
              #   # '@AvailabilityDisplayType': air_segment.availability_display_type,
              #   content!: index == 0 ? '' : { 'Connection/': {content!: ''} }
              # }
            }
          end
        end
        air_segments
      end

      def air_pricing_infos
        air_pricing_infos = []
        solution.air_pricing_infos.each do |air_pricing|
          air_pricing_infos << {
            'AirPricingInfo': air_pricing.construct_hash([
              :key,
              :total_price,
              :base_price,
              :approximate_total_price,
              :approximate_base_price,
              :taxes,
              :approximate_taxes,
              :latest_ticketing_time,
              :pricing_method,
              :plating_carrier
            ]).merge!({
              content!: booking_infos_and_fare_infos(air_pricing) + passenger_types(air_pricing.passenger_code, air_pricing.passenger_count)
            })
            # {
            #   '@Key': air_pricing.key,
            #   '@TotalPrice': air_pricing.total_price,
            #   '@BasePrice': air_pricing.base_price,
            #   '@ApproximateTotalPrice': air_pricing.approximate_total_price,
            #   '@ApproximateBasePrice': air_pricing.approximate_base_price,
            #   '@Taxes': air_pricing.taxes,
            #   '@ApproximateTaxes': air_pricing.approximate_taxes,
            #   '@LatestTicketingTime': air_pricing.latest_ticketing_time,
            #   '@PricingMethod': air_pricing.pricing_method,
            #   '@PlatingCarrier': air_pricing.plating_carrier,
            #   content!: booking_infos_and_fare_infos(air_pricing) + passenger_types(air_pricing.passenger_code, air_pricing.passenger_count)
            # }
          }
        end
        air_pricing_infos
      end

      def booking_infos_and_fare_infos(air_pricing)
        booking_infos = []
        fare_infos = []
        air_pricing.booking_infos.each do |booking_info|
          booking_infos << {
            'BookingInfo/': booking_info.construct_hash([
              :booking_code, 
              :cabin_class, 
              :segment_ref
            ]).merge!({
              '@FareInfoRef': booking_info.fare_info.key
            })
            # {
            #   '@BookingCode': booking_info.booking_code,
            #   '@CabinClass': booking_info.cabin_class,
            #   '@FareInfoRef': booking_info.fare_info.key,
            #   '@SegmentRef': booking_info.segment_ref
            # }
          }
          fare_infos << {
            'FareInfo/': booking_info.fare_info.construct_hash([
              :key,
              :fare_basis,
              :passenger_type_code,
              :origin,
              :destination,
              :effective_date,
              :departure_date,
              :amount,
              :tax_amount,
            ])
            # {
            #   '@Key': booking_info.fare_info.key,
            #   '@FareBasis': booking_info.fare_info.fare_basis,
            #   '@PassengerTypeCode': booking_info.fare_info.passenger_type_code,
            #   '@Origin': booking_info.fare_info.origin,
            #   '@Destination': booking_info.fare_info.destination,
            #   '@EffectiveDate': booking_info.fare_info.effective_date,
            #   '@DepartureDate': booking_info.fare_info.departure_date,
            #   '@Amount': booking_info.fare_info.amount,
            #   '@TaxAmount': booking_info.fare_info.tax_amount
            # }
          }
        end
        fare_infos + booking_infos
      end

      def passenger_types(code, count)
        passenger_types = []
        count.times do |index|
          traveler_key = self.traveler_keys.index{|key| key.include?(code)}
          passenger_types << {
            'PassengerType/': {
              '@Code': code,
              '@BookingTravelerRef': traveler_keys.delete_at(traveler_key),
              content!: ''
            } 
          }
        end

        passenger_types
      end

    end

    class AirCreateReservationResponse < Response        
      RESPONSE_IDS[:air_create_reservation_rsp] = self  
      attr_accessor :response_time,
                    :universal_code,
                    :host_code,
                    :uAPI_code,
                    :reservation

      private

      def generate_data_from(response_body)
        @response_time = response_body.dig(:'@response_time')
        @universal_code = response_body.dig(:universal_record, :'@locator_code')
        @host_code = response_body.dig(:universal_record, :provider_reservation_info, :'@locator_code')
        @uAPI_code = response_body.dig(:universal_record, :air_reservation, :'@locator_code')
        booking_traveler_list = Hash.new

        Array.wrap(response_body.dig(:universal_record, :booking_traveler) || []).each do |booking_traveler|
          booking_traveler_list[booking_traveler[:@key]] = BookingTraveler.new(booking_traveler)
        end

        air_reservation = response_body.dig(:universal_record, :air_reservation) || {}

        @reservation = AirReservation.new(air_reservation.merge!(booking_traveler_list: booking_traveler_list))

      end
    end

  end

end
module Travelport
  class Brand < Base
    attr_accessor :key,
                  :brand_id,
                  :carrier,
                  :name,
                  :optional_services

    def initialize(args = {})
      super(args)

      if args[:optional_services].present?
        self.optional_services = parse_optional_services(args)
      else
        self.optional_services = []
      end
    end

    def parse_optional_services(args = {})
      Array.wrap(args[:optional_services].fetch(:optional_service, [])).map{ |optional_service| OptionalService.new(optional_service) }
    end

  end
end
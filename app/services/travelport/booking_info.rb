module Travelport
  class BookingInfo < Base
    attr_accessor :booking_code,
                  :booking_count,
                  :cabin_class,
                  :fare_info,
                  :segment_ref

    def initialize(args = {})
      super(args)

      self.fare_info = parse_fare_info(args)

    end

    def parse_fare_info(args = {})
      fare_info_list = args.fetch(:fare_info_list, {})
      key = args.fetch(:'@fare_info_ref', nil)

      if fare_info_list.present? && key.present? && fare_info_list.has_key?(key)
        fare_info_list[key]
      else
        nil
      end
    end

  end
end
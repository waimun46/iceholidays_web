module Travelport
  class AirPricingSolution < Base
    attr_accessor :key,
                  :total_price,
                  :base_price,
                  :approximate_total_price,
                  :approximate_base_price,
                  :taxes,
                  :fees, #air_price
                  :approximate_taxes,
                  :quote_date, #air_price
                  :journeys, #low_price_search / custom in #air_price
                  :air_pricing_infos,
                  :final_total_price #custom


    def initialize(args = {})
      super(args)

      self.air_pricing_infos = parse_air_pricing_infos(args)

      #low_price_search
      if args[:journey].present?
        self.journeys = parse_journeys(args.merge!(air_pricing_infos: self.air_pricing_infos))
      else
        self.journeys = []
      end


      # air_price
      if args[:air_segment_ref].present?
        parse_air_segments(args)
      end

      self.final_total_price = self.air_pricing_infos.map(&:final_total_price).inject(:+)

    end

    def parse_journeys(args = {})
      air_segment_list = args.fetch(:air_segment_list, {})
      Array.wrap(args.fetch(:journey, [])).map{ |journey| Journey.new(journey.merge!(air_segment_list: air_segment_list, air_pricing_infos: self.air_pricing_infos)) }
    end

    def parse_air_segments(args = {})
      air_segment_list = args.fetch(:air_segment_list, {})
      if air_segment_list.present?
        prev_air_segment = nil
        journey = nil
        Array.wrap(args.fetch(:air_segment_ref, [])).each do |ref|
          key = ref.dig(:'@key')
          if key.present? && air_segment_list.has_key?(key)
            air_segment = air_segment_list[key].dup

            if air_segment.connection.blank?
              journey = Journey.new
              self.journeys << journey
              prev_air_segment = nil
            end

            if prev_air_segment.present?
              prev_air_segment.next_air_segment = air_segment
              prev_air_segment.transit_time = air_segment.departure_time.to_time - prev_air_segment.arrival_time.to_time
            end

            air_segment.air_pricing_infos = self.air_pricing_infos

            journey.add_air_segment(air_segment)
            prev_air_segment = air_segment
          end
        end
      end
    end

    def parse_air_pricing_infos(args = {})
      if args[:air_pricing_info].present?
        fare_info_list = args.fetch(:fare_info_list, {})
        Array.wrap(args.fetch(:air_pricing_info, [])).map{ |air_pricing_info| AirPricingInfo.new(air_pricing_info.merge!(fare_info_list: fare_info_list)) }
      end
    end

    def airline
      if self.air_pricing_infos.present?
        self.air_pricing_infos.first.airline
      else
        nil
      end
    end

  end
end
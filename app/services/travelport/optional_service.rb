module Travelport
  class OptionalService < Base
    attr_accessor :key,
                  :chargeable,
                  :display_order,
                  :tag,
                  :type,
                  :titles,
                  :texts

    def initialize(args = {})
      super(args)

      if args[:title].present?
        self.titles = args[:title]
      else
        self.titles = []
      end

      if args[:text].present?
        self.texts = args[:text]
      else
        self.texts = []
      end
    end

  end
end
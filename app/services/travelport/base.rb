module Travelport
  class Base
    attr_accessor :errors

    def initialize(args = {})
      args.each do |name, value|
        attr_name = name.to_s.underscore.sub('@', '')
        send("#{attr_name}=", value) if respond_to?("#{attr_name}=")
      end
    end

    def construct_hash(attribute_names = [])
      object_hash = {}
      attribute_names.each do |attr_name|
        object_hash["@#{attr_name.to_s.camelize}"] = send(attr_name) if respond_to?("#{attr_name}")
      end
      object_hash
    end
  end
end
module Travelport
  class BookingTraveler < Base
    attr_accessor :key,
                  :traveler_type,
                  :age,
                  :dob,
                  :prefix,
                  :first_name,
                  :last_name,
                  :location,
                  :mobile

    def initialize(args = {})
      super(args)

      if args[:booking_traveler_name].present?
        self.prefix = args[:booking_traveler_name].dig(:'@prefix')
        self.first_name = args[:booking_traveler_name].dig(:'@first')
        self.last_name = args[:booking_traveler_name].dig(:'@last')
      end

      if args[:phone_number].present?
        self.location = args[:phone_number].dig(:'@location')
        self.mobile = args[:phone_number].dig(:'@number')
      end

    end

  end
end
module Globaltix
  class Booking < Base
    attr_accessor :id,
                  :reference_number,
                  :e_ticket_url,
                  :email,
                  :customer_name,
                  :currency,
                  :amount,
                  :tickets


    def self.create(params)
      response = Request.create("api/transaction/create", params)
      if response.fetch('success', false)
        Booking.new(response.fetch('data', nil))
      else
        nil
      end
    end

  end
end

module Globaltix
  class Base
    attr_accessor :errors

    def initialize(args = {})
      if args.present?
        args.each do |name, value|
          attr_name = name.to_s.underscore
          send("#{attr_name}=", value) if respond_to?("#{attr_name}=")
        end
      end
    end
  end
end
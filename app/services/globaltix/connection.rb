module Globaltix
  require 'faraday'
  require 'json'

  class Connection
    
    if Rails.env.iceb2b? || Rails.env.testb2b?
      BASE = Rails.application.credentials.globaltix[:url]
      USERNAME = Rails.application.credentials.globaltix[:username]
      PASSWORD = Rails.application.credentials.globaltix[:password]
    else
      BASE = 'https://uat-api.globaltix.com'
      USERNAME = "reseller@globaltix.com"
      PASSWORD = '12345'
    end

    def self.get_access_token
      conn = Faraday.new(url: BASE) do |faraday|
        faraday.response :logger
        faraday.adapter Faraday.default_adapter
        faraday.headers['Content-Type'] = 'application/json'
      end

      response = conn.post '/api/auth/login', { username: USERNAME, password: PASSWORD }.to_json
      if response.status == 200
        JSON.parse(response.body)["data"]["access_token"]
      else
        nil
      end
    end

    def self.api
      token = REDIS.get 'GlobaltixToken'
      if token.blank?
        token = Connection.get_access_token
        REDIS.setex 'GlobaltixToken', 3600, token
      end

      Faraday.new(url: BASE) do |faraday|
        faraday.response :logger
        faraday.adapter Faraday.default_adapter
        faraday.headers['Accept-Version'] = '1.0'
        faraday.headers['Authorization'] = "Bearer #{token}"
      end
    end
  end
end
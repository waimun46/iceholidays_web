module Globaltix
  class Country < Base
    attr_accessor :id,
                  :code,
                  :name

    def self.all
      response = Request.where('api/country/getAllListingCountry')
      countries = response.fetch('data', []).map { |country| Country.new(country) }
      [ countries, response[:errors] ]
    end
  end
end
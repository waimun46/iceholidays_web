module Globaltix
  class Question < Base
    attr_accessor :id,
                  :question,
                  :options,
                  :optional,
                  :image_path,
                  :type

    def type=(val)
      @type = val.fetch('name', nil)
    end

  end
end
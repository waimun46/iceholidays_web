module Globaltix
  class Request
    class << self
      def where(resource_path, query = {})
        response, status = get_json(resource_path, query)
        if status == 200
          if response["success"]
            response
          else
            response.merge({errors: response["error"] })
          end
        else
          response.merge({errors: { status: status }} )
        end
        # status == 200 ? response f: response.merge({ errors: { status: status } })
      end

      # def errors(response)
      #   error = { errors: { status: response["status"], message: response["message"] } }
      #   response.merge(error)
      # end

      def get_json(root_path, query = {})
        query_string = query.map{|k,v| "#{k}=#{v}"}.join("&")
        path = query.empty?? root_path : "#{root_path}?#{query_string}"
        response = Rails.cache.fetch("Globaltix/#{path}", expires_in: 3.hours, force: false) do
          api.get(path)
        end
        # response = api.get(path)

        [JSON.parse(response.body), response.status]
      end

      def post_json(path, params = {})
        response = api.post do |req|
          req.url path
          req.headers['Content-Type'] = 'application/json'
          req.body = params.to_json
        end

        [JSON.parse(response.body), response.status]
      end

      def create(resource_path, params = {})
        response, status = post_json(resource_path, params)
        if status == 200
          if response["success"]
            response
          else
            response.merge({errors: response["error"] })
          end
        else
          response.merge({errors: { status: status }} )
        end
      end

      def api
        Globaltix::Connection.api
      end
    end
  end
end
module Globaltix
  class Attraction < Base
    attr_accessor :id,
                  :title,
                  :description,
                  :hours_of_operation,
                  :image_path,
                  :country

    # Globaltix::Attraction.where({countryId: 1, page: 1})
    def self.where(query = {})
      response = Request.where("api/attraction/list", query.merge(excludeTicketTypes: true))
      attractions = response.fetch('data', []).map { |attraction| Attraction.new(attraction) }

      # define kaminari required atrributes 
      if query[:page].present?
        attractions.define_singleton_method(:limit_value) do
          30
        end
        
        attractions.define_singleton_method(:total_pages) do
          response.fetch('size', 0) / attractions.limit_value + 1
        end

        attractions.define_singleton_method(:current_page) do
          query[:page].to_i
        end
      end

      [ attractions, response[:errors] ]
    end

    def self.find(id)
      response = Request.where("api/attraction/get", {id: id})
      Attraction.new(response.fetch('data', nil))
    end

    def initialize(args = {})
      if args.present?
        super(args)

        if args["country"].present?
          self.country = args["country"].fetch('name', '')
        end
      else
        nil
      end
    end

    def image_url(type)
      if image_path.present?
        url = "#{Connection::BASE}/api/image?name=#{image_path}"

        if type == :thumb
          "#{url}_thumb"
        elsif type == :banner
          "#{url}_banner"
        else
          url
        end
      else
        ""
      end
    end

  end
end
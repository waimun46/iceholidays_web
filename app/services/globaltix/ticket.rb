module Globaltix
  class Ticket < Base
    attr_accessor :id,
                  :name,
                  :currency,
                  :price, #(currency, list)
                  :settlement_rate, #(currency, get)
                  :payable_amount, #(currency, get)
                  :merchant_currency,
                  :original_price, #(merchant_currency)
                  :settlement_price, #(merchant_currency, list)
                  :source_title,
                  :source_name,
                  :variation,
                  # (get)
                  :description,
                  :terms_and_conditions,
                  :sku,
                  :is_request_visit_date,
                  :is_visit_date_compulsory,
                  :is_advance_booking,
                  :advance_booking_days,
                  :apply_capacity,
                  :questions,
                  :attraction,
                  :country_name,
                  # (create)
                  :quantity

    # Globaltix::Ticket.where({attraction_id: 1})
    def self.where(query = {})
      response = Request.where("api/ticketType/list", query)
      tickets = response.fetch('data', []).map { |ticket| Ticket.new(ticket) }
      [ tickets, response[:errors] ]
    end

    def self.find(id)
      response = Request.where("api/ticketType/get", {id: id})
      Ticket.new(response.fetch('data', nil))
    end

    def self.create(params)
      response = Request.create("api/transaction/create", params)
    end

    def initialize(args = {})
      if args.present?
        super(args)

        if args["visitDate"].present?
          super(args["visitDate"])
        end

        if args["variation"].present?
          self.variation = args["variation"].fetch('name', '')
        end

        self.questions = parse_questions(args)

        if args["attraction"].present?
          self.attraction = Attraction.new(args['attraction'])
        end
      else
        nil
      end
    end

    def parse_questions(args = {})
      args.fetch("questions", []).map{ |question| Question.new(question) }
      # args.fetch("questions", []).map{ |question| Question.new(id: 1, question: 'aaa?') }
    end

    def is_visit_date_compulsory?
      self.is_request_visit_date.present? && self.is_visit_date_compulsory.present? && self.is_request_visit_date && self.is_visit_date_compulsory
    end

    def the_advance_booking_days
      if self.is_visit_date_compulsory? && self.is_advance_booking.present? && self.is_advance_booking && self.advance_booking_days.present?
        self.advance_booking_days.to_i
      else
        0
      end
    end

    def country
      if self.country_name.blank?
        attraction, attraction_err = Globaltix::Attraction.find(self.attraction.id)
        if attraction.country.present?
          self.country_name = attraction.country
        end
      end
      self.country_name
    end
  end
end
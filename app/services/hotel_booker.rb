module HotelBooker
  DEFAULT_COUNTRY = 'Malaysia'
  DEFAULT_COUNTRY_CODE = 'MY'
  DEFAULT_CURRENCY = 'MYR'
  

# Get Hotels
# HotelBooker[:tbo_holidays].get_hotels(city_code: 151679)
def self.get_hotels(client, params = {})
  self[client].get_hotels(params)
end

  # Sample usage:

  # Hote Search Request

  # Single Room
  # params = {
  #   check_in: '2019-10-01',
  #   check_out: '2019-10-10',
  #   city_id:115936,
  #   rooms:1,
  #   nationality: 'MY',
  #   room_guests:[ HotelBooker::RoomGuest.new(adult_count:1, child_count:0) ],
  #   pref_currency: 'MYR',
  #   limit:5
  # }
  # Multiple Rooms 
  # params = {
  #   check_in: '2019-10-01',
  #   check_out: '2019-10-10',
  #   city_id:115936,
  #   rooms:2,
  #   nationality: 'MY',
  #   room_guests:[
  #       HotelBooker::RoomGuest.new(adult_count:2, child_count:2, child_age:[5,2]),
  #       HotelBooker::RoomGuest.new(adult_count:1, child_count:0)
  #     ],
  #   pref_currency: 'MYR',
  #   limit:5
  # }
  # hotel_search = HotelBooker[:tbo_holidays].search_hotels(params)
  # hotel_search = HotelBooker.search_hotels(params)


  def self.search_hotels(params = {})
    data = []
    success = false
    messages = []
    # We will request for hotel list in each client api 
    # will treat it a success call if any of the clients return a successful response    
    clients.each do |key, client|     
      response = client.new.search_hotels(params)
      if response.success? 
        success ||= true
        data += response.data 
      end

      messages << { source: response.source, message: response.message }
    end
    Response.new(success, messages, data)
  end


  # HotelBooker[:tbo_holidays].hotel_details(code: 1255024)
  def self.hotel_details(client, params = {})
    self[client].hotel_details(params)
  end

  # selected_hotel_code = hotel_search.hotel_result_list.first.hotel_info.hotel_code
  # selected_hotel_result_index = hotel_search.hotel_result_list.first.result_index
  # selected_hotel_name = hotel_search.hotel_result_list.first.hotel_info.hotel_name

  # session_id = hotel_search.session_id
  # params = {
  #   hotel_code: selected_hotel_code,
  #   result_index: selected_hotel_result_index,
  #   session_id: session_id,
  # }

  # room_availability = HotelBooker[:tbo_holidays].hotel_room_availability(params)
  def self.hotel_room_availability(client, params = {})
    self[client].hotel_room_availability(params)
  end

  
  
  # Hotel Cancellation Policy Request
  # selected_rooms =  [room_availability.hotel_rooms.first]
  # selected_room_combination = room_availability.booking_options.room_combinations.first
  # booking_options = HotelBooker::BookingOptions.new(
  #   fixed_format:true,
  #   room_combinations: selected_room_combination
  # )

  # params = {
  #   session_id: session_id,
  #   result_index: selected_hotel_result_index,
  #   booking_options: booking_options
  # }
  # cancellation_policies = HotelBooker[:tbo_holidays].hotel_cancellation_policies(params) 

  def self.hotel_cancellation_policies(client, params = {})
    self[client].hotel_cancellation_policies(params)
  end

  # Availability And Pricing Request
  # same with cancellation policy request params
  # availability_and_pricing = HotelBooker[:tbo_holidays].availability_and_pricing(params)  
  
  def self.availability_and_pricing(client, params = {})
    self[client].availability_and_pricing(params)
  end

  # Hotel Book Request

  # payment_info = HotelBooker::PaymentInfo.new
  # reference_no = "#{Time.current.strftime("%d%m%y%H%M%S%3N")}#ICEH"
  # params = {
  #   client_reference_number: reference_no,
  #   guest_nationality: 'MYR',
  #   guests: [
  #     HotelBooker::Guest.new(
  #       guest_type: 'Adult',
  #       lead_guest: true,
  #       guest_in_room:1,
  #       title:'Mr.',
  #       first_name:'Vhilly',
  #       last_name:'Santiago',
  #       age: '32'
  #     )
  #   ],
  #   address_info: HotelBooker::AddressInfo.new(
  #     address_line_1: 'Bazar Majlis Daerah 3 Jln Kuala Balah 17600 Jeli Jeli Kelantan 17600 Malaysia Jeli Kelantan 17600 Ma',
  #     address_line_2: '',
  #     country_code:'MY',
  #     area_code:'+60',
  #     phone_no:'09944-0861',
  #     email:'',
  #     city:'Kuala Lumpur',
  #     state:'',
  #     country:'Malaysia',
  #     zip_code:'17600'
  #   ),
  #   payment_info: payment_info,
  #   session_id: session_id,
  #   no_of_rooms:1 ,
  #   result_index: selected_hotel_result_index,
  #   hotel_code: selected_hotel_code,
  #   hotel_name: selected_hotel_name,
  #   hotel_rooms: selected_rooms
  # }
  # booking = HotelBooker[:tbo_holidays].book!(params)
  def self.book!(client, params = {})
    self[client].book!(params)
  end

  # Generate Invoice

  # params = {
  #   payment_info: HotelBooker::PaymentInfo.new(voucher_booking:true),
  #   confirmation_no:booking.confirmation_no
  # }
  # HotelBooker[:tbo_holidays].generate_invoice!(params)
  def self.generate_invoice!(client, params = {})
    self[client].generate_invoice!(params)
  end

  # Booking Detail

  # HotelBooker[:tbo_holidays].booking_details(client_reference_number: reference_no)
  def self.booking_details(client, params = {})
    self[client].booking_details(params)
  end

  # Cancel Booking

  # params = {
  #   confirmation_no:  booking.confirmation_no,
  #   remarks: 'Change of plan.'
  # }
  # HotelBooker[:tbo_holidays].cancel!(params)
  def self.cancel!(client, params = {})
    self[client].cancel!(params)
  end

  # Cancel Booking Status

  # HotelBooker[:tbo_holidays].cancel?(confirmation_no:  booking.confirmation_no)
  def self.cancel?(client, params = {})
    self[client].cancel?(params)
  end

  # Sample usage:
  # HotelBooker.get_countries(:tbo_holidays)
  # HotelBooker[:tbo_holidays].get_countries
  def self.get_countries(client, params = {})
    self[client].get_countries(params)
  end

  # Sample usage:
  # HotelBooker.get_cities(:tbo_holidays)
  # HotelBooker[:tbo_holidays].get_cities
  def self.get_cities(client, params = {})
    self[client].get_cities(params)
  end


  def self.clients
    @clients ||= {}
  end

  def self.register_client(key, client)
    fail ArgumentError, 'Class required' unless client.is_a?(Class)
    clients[key.to_sym] = client
  end

  # Get client by key
  def self.[](key)
    fail ArgumentError, 'Client does not exist' unless key.in?(clients)
    clients[key].new
  end
end


# Register Clients
# TBO Holidays
require_relative 'hotel_booker/clients/tbo_holidays'
HotelBooker.register_client :tbo_holidays, HotelBooker::Clients::TBOHolidays::Client

# TODO
# Travel Prologue Client
# require_relative 'hotel_booker/clients/travel_prologue'
# HotelBooker.register_client :travel_prologue, HotelBooker::Clients::TravelPrologue::Client
module Etravel
  require 'faraday'
  require 'json'

  class Connection
    if Rails.env.iceb2b?
      BASE  = 'http://13.229.12.58'
    else
      BASE  = ''
    end

    def self.api
      conn = Faraday.new(url: BASE) do |faraday|
        faraday.response :logger
        faraday.adapter Faraday.default_adapter
        faraday.headers['Content-Type'] = 'application/json'
      end
    end
  end

end

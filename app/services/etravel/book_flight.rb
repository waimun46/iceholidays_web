module Etravel
  class BookFlight < Base

    attr_accessor :sys_id,
                  :ibe_book_ref_no,
                  :prod_code,
                  :stock_code,
                  :stock_no_from,
                  :stock_no_to,
                  :airline,
                  :iss_date,
                  :ior_d,
                  :dest_city,
                  :tkt_class,
                  :tkt_sector,
                  :pnr_no,
                  :pax_name,
                  :aci,
                  :tour_code,
                  :tkt_gross,
                  :tkt_comm,
                  :tkt_nett,
                  :tkt_cost,
                  :tkt_sell,
                  :a_tax_code1,
                  :a_tax_amt1,
                  :a_tax_code2,
                  :a_tax_amt2,
                  :a_tax_code3,
                  :a_tax_amt3,
                  :tot_sell,
                  :tot_cost,
                  :tax_sell,
                  :tax_cost,
                  :gst_amt,
                  :gst_tax_code,
                  :gst_tax_rmks,
                  :gst_tax_rate,
                  :pcc_no,
                  :sign_on_code,
                  :supp_code,
                  :supp_name,
                  :last_update,
                  :s_key,
                  :sell_curr_code,
                  :sell_ex_rate

    def self.all
      response = Request.where('/etravelapi/api/BookFlight')

      book_flights = response.map{|r| BookFlight.new(r)}

      book_flights
    end

    def self.create(booking_group, booking)

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        # :prodCode=>,
        # :stockCode=>,
        # :stockNoFrom=>,
        # :stockNoTo=>,
        :airline=>booking_group.flight_booking_details.first.carrier,
        :issDate=>booking.created_at.strftime("%d-%b-%Y"),
        # :iorD=>,
        :destCity=>booking_group.flight_booking_details.last.destination,
        :tktClass=>booking_group.flight_booking_details.first.cabin_class,
        # :tktSector=>,
        :pnrNo=>booking_group.host_code,
        :paxName=>"#{booking.last_name}, #{booking.first_name}",
        :aci=>booking.category,
        # :tourCode=>,
        # :tktGross=>,
        # :tktComm=>,
        # :tktNett=>,
        :tktCost=>booking.base.to_f,
        :tktSell=>booking.base.to_f,
        # :aTaxCode1=>,
        # :aTaxAmt1=>,
        # :aTaxCode2=>,
        # :aTaxAmt2=>,
        # :aTaxCode3=>,
        # :aTaxAmt3=>,
        :totSell=>booking.price.to_f,
        :totCost=>booking.price.to_f,
        :taxSell=>booking.taxes.to_f,
        :taxCost=>booking.taxes.to_f,
        # :gstAmt=>,
        # :gstTaxCode=>,
        # :gstTaxRmks=>,
        # :gstTaxRate=>,
        # :pccNo=>,
        # :signOnCode=>,
        # :suppCode=>,
        # :suppName=>,
        # :lastUpdate=>,
        # :sellCurrCode=>,
        # :sellExRate=>
      }

      response = Request.create('/etravelapi/api/BookFlight', params)

      if response.split(", ").last.downcase.include?("sucessful")
        Rails.logger.info "Book Flight API Successfully Created!"
        return true
      else
        Rails.logger.info "Book Flight API Response: #{response}"
        return false
      end
    end

    def self.find(ibebookref)
      response = Request.find('/etravelapi/api/BookFlight', ibebookref)

      book_flight = BookFlight.new(response[0])
    end

    def self.update(booking_group, booking)

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        # :prodCode=>,
        # :stockCode=>,
        # :stockNoFrom=>,
        # :stockNoTo=>,
        :airline=>booking_group.flight_booking_details.first.carrier,
        :issDate=>booking.created_at.strftime("%d-%b-%Y"),
        # :iorD=>,
        :destCity=>booking_group.flight_booking_details.last.destination,
        :tktClass=>booking_group.flight_booking_details.first.cabin_class,
        # :tktSector=>,
        :pnrNo=>booking_group.host_code,
        :paxName=>"#{booking.last_name}, #{booking.first_name}",
        :aci=>booking.category,
        # :tourCode=>,
        # :tktGross=>,
        # :tktComm=>,
        # :tktNett=>,
        # :tktCost=>,
        # :tktSell=>,
        # :aTaxCode1=>,
        # :aTaxAmt1=>,
        # :aTaxCode2=>,
        # :aTaxAmt2=>,
        # :aTaxCode3=>,
        # :aTaxAmt3=>,
        :totSell=>booking_group.price.to_f,
        :totCost=>booking_group.price.to_f
        # :taxSell=>,
        # :taxCost=>,
        # :gstAmt=>,
        # :gstTaxCode=>,
        # :gstTaxRmks=>,
        # :gstTaxRate=>,
        # :pccNo=>,
        # :signOnCode=>,
        # :suppCode=>,
        # :suppName=>,
        # :lastUpdate=>,
        # :sellCurrCode=>,
        # :sellExRate=>
      }

      response = Request.update('/etravelapi/api/BookFlight', params)

      if response[0].split(", ").last.downcase.include?("sucessful")
        Rails.logger.info "Book Flight API Successfully Updated!"
        return true
      else
        Rails.logger.info "Book Flight API Response: #{response}"
        return false
      end
    end

  end
end

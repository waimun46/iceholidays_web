module Etravel
  class BookFlightItinerary < Base

    attr_accessor :sys_id,
                  :ibe_book_ref_no,
                  :pnr_no,
                  :seg_no,
                  :from_city,
                  :to_city,
                  :airline,
                  :flight_no,
                  :flt_class,
                  :dep_date,
                  :dep_time,
                  :arrv_date,
                  :arrv_time,
                  :flt_status,
                  :air_craft,
                  :seat_no,
                  :flt_time

    def self.all
      response = Request.where('/etravelapi/api/BookFltItin')

      book_flights = response.map{|r| BookFlightItinerary.new(r)}

      book_flights
    end

    def self.create(booking_group, flight_detail)

      case booking_group.class.to_s
      when "BookingGroup"
        params = BookFlightItinerary.generate_srs_params(booking_group, flight_detail)
      when "FlightBookingGroup"
        params = BookFlightItinerary.generate_flt_params(booking_group, flight_detail)
      else
        false
      end

      response = Request.create('/etravelapi/api/BookFltItin', params)

      if response.split(", ").last.downcase.include?("sucessful")
        Rails.logger.info "Book Flight Itinerary API Successfully Created!"
        return true
      else
        Rails.logger.info "Book Flight Itinerary API Response: #{response}"
        return false
      end
    end

    def self.find(ibebookref)
      response = Request.find('/etravelapi/api/BookFltItin', ibebookref)

      book_flight = BookFlightItinerary.new(response[0])
    end

    def self.update(booking_group, flight_detail)

      case booking_group.class.to_s
      when "BookingGroup"
        params = BookFlightItinerary.generate_srs_params(booking_group, flight_detail)
      when "FlightBookingGroup"
        params = BookFlightItinerary.generate_flt_params(booking_group, flight_detail)
      else
        false
      end

      response = Request.update('/etravelapi/api/BookFltItin', params)

      if response[0].split(", ").last.downcase.include?("sucessful")
        Rails.logger.info "Book Flight Itinerary API Successfully Updated!"
        return true
      else
        Rails.logger.info "Book Flight Itinerary API Response: #{response}"
        return false
      end
    end

    def self.generate_srs_params(booking_group, tour_flight)
      params = {
        :IBEBookRefNo=>booking_group.ref_no,
        :FromCity=>tour_flight.from,
        :ToCity=>tour_flight.to,
        :FlightNo=>tour_flight.flight_no,
        :DepDate=>tour_flight.departure.strftime("%d-%b-%Y"),
        :DepTime=>tour_flight.departure.strftime("%H%M"),
        :ArrvDate=>tour_flight.arrival.strftime("%d-%b-%Y"),
        :ArrvTime=>tour_flight.arrival.strftime("%H%M")
      }
    end

    def self.generate_flt_params(booking_group, flight_booking_detail)
      booking = flight_booking_detail.flight_booking_group.flight_bookings.first

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        # :pnrNo=>,
        # :segNo=>,
        :fromCity=>flight_booking_detail.origin,
        :toCity=>flight_booking_detail.destination,
        # :airline=>,
        :flightNo=>flight_booking_detail.flight_number,
        :fltClass=>flight_booking_detail.cabin_class,
        :depDate=>flight_booking_detail.departure_time.strftime("%d-%b-%Y"),
        :depTime=>flight_booking_detail.departure_time.strftime("%H%M"),
        :arrvDate=>flight_booking_detail.arrival_time.strftime("%d-%b-%Y"),
        :arrvTime=>flight_booking_detail.arrival_time.strftime("%H%M"),
        :fltStatus=>flight_booking_detail.status
        # :airCraft=>,
        # :seatNo=>,
        # :fltTime=>
        # :sKey=>"sample string 18"
      }
    end

  end
end

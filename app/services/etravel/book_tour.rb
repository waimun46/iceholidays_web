module Etravel
  class BookTour < Base

    attr_accessor :sys_id,
                  :ibe_book_ref_no,
                  :item_no,
                  :prod_code,
                  :tour_code,
                  :dep_date,
                  :dest_city,
                  :tot_night,
                  :deadline,
                  :flt_code,
                  :htl_code,
                  :supp_code,
                  :supp_name,
                  :supp_ref_no,
                  :desp,
                  :rmks1,
                  :rmks2,
                  :sell1_a,
                  :sell2_a,
                  :sell3_a,
                  :sell4_a,
                  :sell_ep,
                  :sell_c_twn,
                  :sell_cw_bed,
                  :sell_cn_bed,
                  :sell_senior,
                  :sell_inf,
                  :cost1_a,
                  :cost2_a,
                  :cost3_a,
                  :cost4_a,
                  :cost_ep,
                  :cost_c_twn,
                  :cost_cw_bed,
                  :cost_cn_bed,
                  :cost_senior,
                  :cost_inf,
                  :qty1_a,
                  :qty2_a,
                  :qty3_a,
                  :qty4_a,
                  :qty_ep,
                  :qty_c_twn,
                  :qty_cw_bed,
                  :qty_cn_bed,
                  :qty_senior,
                  :qty_inf,
                  :tot_sell,
                  :tot_cost,
                  :gst_amt,
                  :gst_tax_code,
                  :gst_tax_rmks,
                  :gst_tax_rate,
                  :last_upd,
                  :s_key,
                  :tour_name

    def self.all
      response = Request.where('/etravelapi/api/BookTour')

      book_tours = response.map{|r| BookTour.new(r)}

      book_tours
    end

    def self.create(booking_group, booking)
      case booking_group.class.to_s
      when "BookingGroup"
        params = BookTour.generate_srs_params(booking_group, booking)
      when "LandTourBookingGroup"
        params = BookTour.generate_ldt_params(booking_group, booking)
      else
        false
      end

      response = Request.create('/etravelapi/api/BookTour', params)

      if response.class == String && response.split(", ").last.downcase.include?("sucessful")
        Rails.logger.info "Book Tour API Successfully Created!"
        return true
      else
        Rails.logger.info "Book Tour API Response: #{response}"
        return false
      end
    end

    def self.find(ibebookref)
      response = Request.find('/etravelapi/api/BookTour', ibebookref)

      book_tour = BookTour.new(response[0])
    end

    def self.update(booking_group, booking)
      case booking_group.class.to_s
      when "BookingGroup"
        params = BookTour.generate_srs_params(booking_group, booking)
      when "LandTourBookingGroup"
        params = BookTour.generate_ldt_params(booking_group, booking)
      else
        false
      end

      response = Request.update('/etravelapi/api/BookTour', params)

      if response[0].split(", ").last.downcase.include?("sucessful")
        Rails.logger.info "Book Tour API Successfully Updated!"
        return true
      else
        Rails.logger.info "Book Tour API Response: #{response}"
        return false
      end
    end

    def self.generate_srs_params(booking_group, booking)
      tour = booking.tour
      flights = tour.tour_flights
      total_price_and_cost_params = Etravel::BookTour.get_srs_total_price_and_cost(booking)

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :tourCode=>tour.code,
        :tourName=>tour.caption,
        :desp=>"#{booking.category.try(:titleize)} #{booking.price_type_display.try(:titleize)}",
        :depDate=>tour.departure_date.strftime("%d-%b-%Y"),
        :rmks1=>tour.respond_to?(:remark) ? tour.remark[0..249] : '',
        :rmks2=>tour.respond_to?(:remark) ? tour.remark.length > 250 ? tour.remark[250..499] : '' : '',
        :destCity=>flights.first.from_to.split("-").last,
        
        :totSell=>booking_group.bookings.map(&:price).inject(:+).to_f,
        :totCost=>booking_group.bookings.map(&:price).inject(:+).to_f,
        # :totSell=>booking.price.to_f,
        # :totCost=>booking.price.to_f,
        :fltCode=>tour.tour_flights.first.flight_no.split(" ").last

        # :itemNo=>"01",
        # :prodCode=>"OBT",
        # :totNight=>4,
        # :htlCode=>"5",
        # :suppCode=>"GD",
        # :suppName=>"Golden Destination",
        # :suppRefNo=>"GD-191222-5D4NBKK",
        # :desp=>"2 ADT(TWIN SHARING) x $1,988",
        # :sell2A=>1.0,
        # :sell3A=>1.0,
        # :sell4A=>1.0,
        # :sellEP=>1.0,
        # :sellCTwn=>1.0,
        # :sellCWBed=>1.0,
        # :sellCNBed=>1.0,
        # :sellSenior=>1.0,
        # :cost1A=>1.0,
        # :cost2A=>1.0,
        # :cost3A=>1.0,
        # :cost4A=>1.0,
        # :costEP=>1.0,
        # :costCTwn=>1.0,
        # :costCWBed=>1.0,
        # :costCNBed=>1.0,
        # :costSenior=>1.0,
        # :costInf=>1.0,
        # :qty1A=>1.0,
        # :qty2A=>1.0,
        # :qty3A=>1.0,
        # :qty4A=>1.0,
        # :qtyEP=>1.0,
        # :qtyCTwn=>1.0,
        # :qtyCWBed=>1.0,
        # :qtyCNBed=>1.0,
        # :qtySenior=>1.0,
        # :qtyInf=>1.0,
      }

      params.merge(total_price_and_cost_params)
    end

    def self.generate_ldt_params(booking_group, booking)
      tour = booking.land_tour
      total_price_and_cost_params = Etravel::BookTour.get_ldt_total_price_and_cost(booking)

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :tourCode=>tour.code,
        :tourName=>tour.title,
        :desp=>"#{booking.category.try(:titleize)}",
        :depDate=>booking_group.departure_date.strftime("%d-%b-%Y"),
        :rmks1=>'',
        :destCity=>booking_group.land_tour.country,
        
        :totSell=>booking_group.land_tour_bookings.map(&:price).inject(:+).to_f,
        :totCost=>booking_group.land_tour_bookings.map(&:price).inject(:+).to_f,
        # :totSell=>booking.price.to_f,
        # :totCost=>booking.price.to_f,
        :fltCode=>""

        # :itemNo=>"01",
        # :prodCode=>"OBT",
        # :totNight=>4,
        # :htlCode=>"5",
        # :suppCode=>"GD",
        # :suppName=>"Golden Destination",
        # :suppRefNo=>"GD-191222-5D4NBKK",
        # :desp=>"2 ADT(TWIN SHARING) x $1,988",
        # :sell2A=>1.0,
        # :sell3A=>1.0,
        # :sell4A=>1.0,
        # :sellEP=>1.0,
        # :sellCTwn=>1.0,
        # :sellCWBed=>1.0,
        # :sellCNBed=>1.0,
        # :sellSenior=>1.0,
        # :cost1A=>1.0,
        # :cost2A=>1.0,
        # :cost3A=>1.0,
        # :cost4A=>1.0,
        # :costEP=>1.0,
        # :costCTwn=>1.0,
        # :costCWBed=>1.0,
        # :costCNBed=>1.0,
        # :costSenior=>1.0,
        # :costInf=>1.0,
        # :qty1A=>1.0,
        # :qty2A=>1.0,
        # :qty3A=>1.0,
        # :qty4A=>1.0,
        # :qtyEP=>1.0,
        # :qtyCTwn=>1.0,
        # :qtyCWBed=>1.0,
        # :qtyCNBed=>1.0,
        # :qtySenior=>1.0,
        # :qtyInf=>1.0,
      }

      params.merge(total_price_and_cost_params)
    end

    def self.get_srs_total_price_and_cost(booking)
      # if booking.room_type.present?
      #   case booking.room_type
      #   when 'Single'
      #     total_price_and_cost_params = {
      #       :sell1A=>booking.price.to_f,
      #       :cost1A=>booking.price.to_f,
      #       :qty1A=>1,
      #     }
      #   when 'Twin Sharing'
      #     total_price_and_cost_params = {
      #       :sell2A=>booking.price.to_f,
      #       :cost2A=>booking.price.to_f,
      #       :qty2A=>1,
      #     }
      #   when 'Triple Sharing'
      #     total_price_and_cost_params = {
      #       :sell3A=>booking.price.to_f,
      #       :cost3A=>booking.price.to_f,
      #       :qty3A=>1,
      #     }
      #   when 'Child No Bed'
      #     total_price_and_cost_params = {
      #       :sellCNBed=>booking.price.to_f,
      #       :costCNBed=>booking.price.to_f,
      #       :qtyCNBed=>1,
      #     }
      #   when 'Child With Bed'
      #     total_price_and_cost_params = {
      #       :sellCWBed=>booking.price.to_f,
      #       :costCWBed=>booking.price.to_f,
      #       :qtyCNBed=>1,
      #     }
      #   when 'Child Twin Share'
      #     total_price_and_cost_params = {
      #       :sellCTwn=>booking.price.to_f,
      #       :costCTwn=>booking.price.to_f,
      #       :qtyCNBed=>1,
      #     }
      #   when 'Double Bed'
      #     total_price_and_cost_params = {
      #       :sell2A=>booking.price.to_f,
      #       :cost2A=>booking.price.to_f,
      #       :qty2A=>1,
      #     }
      #   end
      # end
      if booking.category.to_i == Booking.categories[:adult]
        total_price_and_cost_params = {
          :sell2A=>booking.price.to_f,
          :cost2A=>booking.price.to_f,
          :qty2A=>1,
        }
      elsif booking.category.to_i == Booking.categories[:child_twin]
        total_price_and_cost_params = {
          :sellCTwn=>booking.price.to_f,
          :costCTwn=>booking.price.to_f,
          :qtyCTwn=>1,
        }
      elsif booking.category.to_i == Booking.categories[:child_with_bed]
        total_price_and_cost_params = {
          :sellCWBed=>booking.price.to_f,
          :costCWBed=>booking.price.to_f,
          :qtyCWBed=>1,
        }
      elsif booking.category.to_i == Booking.categories[:child_no_bed]
        total_price_and_cost_params = {
          :sellCNBed=>booking.price.to_f,
          :costCNBed=>booking.price.to_f,
          :qtyCNBed=>1,
        }
      end

      total_price_and_cost_params
    end

    def self.get_ldt_total_price_and_cost(booking)
      if booking.category == "adult"
        total_price_and_cost_params = {
          :sell2A=>booking.price.to_f,
          :cost2A=>booking.price.to_f,
          :qty2A=>1,
        }
      elsif booking.category == "child_twin"
        total_price_and_cost_params = {
          :sellCTwn=>booking.price.to_f,
          :costCTwn=>booking.price.to_f,
          :qtyCTwn=>1,
        }
      elsif booking.category == "child_with_bed"
        total_price_and_cost_params = {
          :sellCWBed=>booking.price.to_f,
          :costCWBed=>booking.price.to_f,
          :qtyCWBed=>1,
        }
      elsif booking.category == "child_without_bed"
        total_price_and_cost_params = {
          :sellCNBed=>booking.price.to_f,
          :costCNBed=>booking.price.to_f,
          :qtyCNBed=>1,
        }
      end

      total_price_and_cost_params
    end

  end
end

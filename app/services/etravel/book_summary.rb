module Etravel
  class BookSummary < Base

    attr_accessor :sys_id,
                  :ibe_book_ref_no,
                  :ibe_book_date,
                  :status,
                  :book_no,
                  :cust_code,
                  :cust_name,
                  :branch_code,
                  :dept_code,
                  :addr1,
                  :addr2,
                  :addr3,
                  :addr4,
                  :attn,
                  :fax,
                  :email_contact,
                  :staff,
                  :salesman,
                  :salesman2,
                  :tkt_steaff,
                  :term,
                  :your_ref,
                  :our_ref,
                  :remarks,
                  :first_pax,
                  :tour_code,
                  :dep_date,
                  :tot_adt,
                  :tot_chd,
                  :tot_inf,
                  :tot_senior,
                  :sell_curr_code,
                  :curr_ex_rate,
                  :tot_amt,
                  :tot_gst_amt,
                  :tot_cost,
                  :gst_rmks,
                  :last_update,
                  :s_key,
                  :postal_code,
                  :city_name,
                  :state,
                  :country,
                  :contact_no

    def self.all
      response = Request.where('/etravelapi/api/BookSummary')

      book_summaries = response.map{|r| BookSummary.new(r)}

      book_summaries
    end

    def self.create(booking_group, invoice)

      case booking_group.class.to_s
      when "BookingGroup"  
        params = BookSummary.generate_srs_params(booking_group, invoice)
      when "RoamingmanBookingGroup"
        params = BookSummary.generate_rmm_params(booking_group, invoice)
      when "GlobaltixTicketBookingGroup"
        params = BookSummary.generate_gtx_params(booking_group, invoice)
      when "FlightBookingGroup"
        params = BookSummary.generate_flt_params(booking_group, invoice)
      when "ActivityBookingGroup"
        params = BookSummary.generate_act_params(booking_group, invoice)
      when "TBOBooking"
        params = BookSummary.generate_htl_params(booking_group, invoice)
      when "LandTourBookingGroup"
        params = BookSummary.generate_ldt_params(booking_group, invoice)
      else
        false
      end

      response = Request.create('/etravelapi/api/BookSummary', params)

      if response.split(", ").last.downcase.include?("sucessful")
        Rails.logger.info "Book Summary API Successfully Created!"
        return true
      else
        Rails.logger.info "Book Summary API Response: #{response}"
        return false
      end
    end

    def self.find(ibebookref)
      response = Request.find('/etravelapi/api/BookSummary', ibebookref)

      book_summary = BookSummary.new(response[0])
    end

    def self.update(booking_group, invoice)

      case booking_group.class.to_s
      when "BookingGroup"  
        params = BookSummary.generate_srs_params(booking_group, invoice)
      when "RoamingmanBookingGroup"
        params = BookSummary.generate_rmm_params(booking_group, invoice)
      when "GlobaltixTicketBookingGroup"
        params = BookSummary.generate_gtx_params(booking_group, invoice)
      when "FlightBookingGroup"
        params = BookSummary.generate_flt_params(booking_group, invoice)
      when "ActivityBookingGroup"
        params = BookSummary.generate_act_params(booking_group, invoice)
      when "TBOBooking"
        params = BookSummary.generate_htl_params(booking_group, invoice)
      when "LandTourBookingGroup"
        params = BookSummary.generate_ldt_params(booking_group, invoice)
      else
        false
      end

      response = Request.update('/etravelapi/api/BookSummary', params)

      if response[0].split(", ").last.downcase.include?("sucessful")
        Rails.logger.info "Book BookSummary API Successfully Updated!"
        return true
      else
        Rails.logger.info "Book BookSummary API Response: #{response}"
        return false
      end
    end

    def self.generate_srs_params(booking_group, invoice)
      tour    = booking_group.tour
      booking = booking_group.bookings.first

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :ibeBookDate=>booking.created_at.strftime("%d-%b-%Y"),
        :status=>booking.status.titleize,
        :custName=>booking.name,
        :custCode=>booking.agent_user.code,
        :attn=>booking.agent_user.username.upcase,
        :contactNo=>booking.mobile,
        :tourCode=>tour.code,
        :deptCode=>tour.etravel_dept_code,
        :depDate=>tour.departure_date.strftime("%d-%b-%Y"),
        :totAmt=>booking_group.bookings.map(&:price).inject(:+).to_f,
        :totCost=>booking_group.bookings.map(&:price).inject(:+).to_f,
        :invNo=>invoice.invoice_no,
        :invDate=>invoice.created_at.strftime("%d-%b-%Y")
      }

      params
    end

    def self.generate_ldt_params(booking_group, invoice)
      tour    = booking_group.land_tour
      booking = booking_group.land_tour_bookings.first

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :ibeBookDate=>booking.created_at.strftime("%d-%b-%Y"),
        :status=>booking.status.titleize,
        :custName=>booking.name,
        :custCode=>booking.agent_user.code,
        :attn=>booking.agent_user.username.upcase,
        :contactNo=>booking.mobile,
        :tourCode=>tour.code,
        :deptCode=>"GA",
        :depDate=>booking_group.departure_date.strftime("%d-%b-%Y"),
        :totAmt=>booking_group.land_tour_bookings.map(&:price).inject(:+).to_f,
        :totCost=>booking_group.land_tour_bookings.map(&:price).inject(:+).to_f,
        :invNo=>invoice.invoice_no,
        :invDate=>invoice.created_at.strftime("%d-%b-%Y")
      }

      params
    end

    def self.generate_rmm_params(booking_group, invoice)
      booking            = booking_group.roamingman_bookings.first
      roamingman_package = booking_group.roamingman_bookings.first.roamingman_package

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :ibeBookDate=>booking.created_at.strftime("%d-%b-%Y"),
        :status=>booking.status.titleize,
        :custName=>booking.name,
        :custCode=>booking.agent_user.code,
        :attn=>booking.agent_user.username.upcase,
        :addr1=>booking.address,
        :deptCode=>"GA",
        :contactNo=>booking.mobile,
        :tourCode=>"RMB2B",
        :country=>booking.country_code,
        :emailContact=>booking.email,
        :totAmt=>booking_group.price.to_f,
        :totCost=>booking_group.price.to_f,
        :invNo=>invoice.invoice_no,
        :invDate=>invoice.created_at.strftime("%d-%b-%Y"),
        :depDate=>booking.depart_date.strftime("%d-%b-%Y")
      }

      params
    end

    def self.generate_flt_params(booking_group, invoice)
      booking = booking_group.flight_bookings.first

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :ibeBookDate=>booking.created_at.strftime("%d-%b-%Y"),
        :status=>booking.status.titleize,
        :custName=>"#{booking.last_name} #{booking.first_name}",
        :custCode=>booking.agent_user.code,
        :attn=>booking.agent_user.username.upcase,
        :deptCode=>"TKT",
        :tourCode=>"FLTB2B",
        # :emailContact=>booking.email,
        :totAmt=>invoice.payment.amount.to_f,
        :totCost=>invoice.payment.amount.to_f,
        :invNo=>invoice.invoice_no,
        :invDate=>invoice.created_at.strftime("%d-%b-%Y")
      }

      params
    end

    def self.generate_gtx_params(booking_group, invoice)
      booking   = booking_group.globaltix_ticket_bookings.first
      globaltix = booking_group.globaltix_ticket_bookings.first.globaltix_ticket

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :ibeBookDate=>booking.created_at.strftime("%d-%b-%Y"),
        :status=>booking.status.titleize,
        :custName=>booking.name,
        :custCode=>booking.agent_user.code,
        :attn=>booking.agent_user.username.upcase,
        :deptCode=>"GA",
        :emailContact=>booking.email,
        :tourCode=>"ACTB2B",
        :totAmt=>booking_group.price.to_f,
        :totCost=>booking_group.price.to_f,
        :invNo=>invoice.invoice_no,
        :invDate=>invoice.created_at.strftime("%d-%b-%Y")
      }

      params
    end

    def self.generate_act_params(booking_group, invoice)
      booking = booking_group.activity_bookings.first

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :ibeBookDate=>booking.created_at.strftime("%d-%b-%Y"),
        :custName=>booking.name,
        :custCode=>booking.agent_user.code,
        :attn=>booking.agent_user.username.upcase,
        :deptCode=>"GA",
        :emailContact=>booking.email,
        :tourCode=>"ACTB2B",
        :totAmt=>booking_group.price.to_f,
        :totCost=>booking_group.price.to_f,
        :invNo=>invoice.invoice_no,
        :invDate=>invoice.created_at.strftime("%d-%b-%Y")
      }

      params
    end

    def self.generate_htl_params(booking_group, invoice)
      booking = booking_group.lead_guest
      
      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :ibeBookDate=>booking.created_at.strftime("%d-%b-%Y"),
        :custName=>"#{booking.last_name} #{booking.first_name}",
        :custCode=>booking_group.agent_user.code,
        :attn=>booking_group.agent_user.username.upcase,
        :deptCode=>"GA",
        :tourCode=>"HTLB2B",
        :totAmt=>booking_group.total_fare.floor.to_f,
        :totCost=>booking_group.total_fare.floor.to_f,
        :invNo=>invoice.invoice_no,
        :invDate=>invoice.created_at.strftime("%d-%b-%Y")
      }

      params
    end

  end
end

module Etravel
  class BookExport

    def self.create(booking_group, payment)

      invoice = EtravelInvoice.generate(booking_group, payment)

      if invoice.present?
        case booking_group.class.to_s
        when "BookingGroup"
          ret = export_tour(booking_group, invoice)
        when "FlightBookingGroup"
          ret = export_flight(booking_group, invoice)
        when "ActivityBookingGroup"
          ret = export_activity(booking_group, invoice)
        when "TBOBooking"
          ret = export_hotel(booking_group, invoice)
        when "RoamingmanBookingGroup"
          ret = export_wifi(booking_group, invoice)
        when "GlobaltixTicketBookingGroup"
          ret = export_globaltix(booking_group, invoice)
        when "LandTourBookingGroup"
          ret = export_land_tour(booking_group, invoice)
        else
          ret = false
        end

        if ret
          if invoice.exported_at.blank?
            invoice.exported_at = [Time.now]
          else
            invoice.exported_at << Time.now
          end
          invoice.save
        end

        ret
      else
        false
      end
    end

    def self.update(booking_group, invoice)

      case booking_group.class.to_s
      when "BookingGroup"
        ret = update_tour(booking_group, invoice)
      when "FlightBookingGroup"
        ret = update_flight(booking_group, invoice)
      when "ActivityBookingGroup"
        ret = update_activity(booking_group, invoice)
      when "TBOBooking"
        ret = update_hotel(booking_group, invoice)
      when "RoamingmanBookingGroup"
        ret = update_wifi(booking_group, invoice)
      when "GlobaltixTicketBookingGroup"
        ret = update_globaltix(booking_group, invoice)
      when "LandTourBookingGroup"
        ret = update_land_tour(booking_group, invoice)
      else
        ret = false
      end

      if ret
        if invoice.exported_at.blank?
          invoice.exported_at = [Time.now]
        else
          invoice.exported_at << Time.now
        end
        invoice.save
      end

      ret
    end

    def self.export_tour(booking_group, invoice)
      etravel_responses = []
      export_tour_successful = true

      if booking_group.present?
        booking_group.bookings.each_with_index do |booking, index|
          export_tour = Etravel::BookTour.create(booking_group, booking)
          etravel_responses << ["Book Tour #{index+1}", export_tour]

          export_pax = Etravel::BookPaxInfo.create(booking_group, booking)
          etravel_responses << ["Book Pax Info #{index+1}", export_pax]
        end

        booking_group.tour.tour_flights.each_with_index do |tour_flight, index|
          export_tour_flight = Etravel::BookFlightItinerary.create(booking_group, tour_flight)

          etravel_responses << ["Book Flight Itinerary #{index+1}", export_tour_flight]
        end

        export_tour_summary = Etravel::BookSummary.create(booking_group, invoice)
        etravel_responses << ['Book Summary', export_tour_summary]

        export_tour_payment = Etravel::BookPayment.create(booking_group, invoice)
        etravel_responses << ['Book Payment', export_tour_payment]

        etravel_responses.each do |response|
          if response.last == false
            export_tour_successful = false
          end
        end

        export_tour_successful
      else
        false
      end
    end

    def self.export_land_tour(booking_group, invoice)
      etravel_responses = []
      export_land_tour_successful = true

      if booking_group.present?
        booking_group.land_tour_bookings.each_with_index do |booking, index|
          export_tour = Etravel::BookTour.create(booking_group, booking)
          etravel_responses << ["Book Land Tour #{index+1}", export_tour]

          export_pax = Etravel::BookPaxInfo.create(booking_group, booking)
          etravel_responses << ["Book Pax Info #{index+1}", export_pax]
        end

        export_misc = Etravel::BookMisc.create(booking_group, booking_group.land_tour_bookings.first)
        etravel_responses << ["Book Misc", export_misc]

        export_tour_summary = Etravel::BookSummary.create(booking_group, invoice)
        etravel_responses << ['Book Summary', export_tour_summary]

        export_tour_payment = Etravel::BookPayment.create(booking_group, invoice)
        etravel_responses << ['Book Payment', export_tour_payment]

        etravel_responses.each do |response|
          if response.last == false
            export_land_tour_successful = false
          end
        end

        export_land_tour_successful
      else
        false
      end
    end

    def self.export_flight(booking_group, invoice)
      etravel_responses = []
      export_flight_successful = true

      if booking_group.present?

        booking_group.flight_bookings.each_with_index do |booking, index|
          export_flight = Etravel::BookFlight.create(booking_group, booking)
          etravel_responses << ["Book Flight #{index+1}", export_flight]

          if booking.brb_booking.present?
            export_misc = Etravel::BookMisc.create(booking_group, booking.brb_booking)
            etravel_responses << ["Book Misc #{index+1}", export_misc]
          end
          
          export_flight_booking = Etravel::BookPaxInfo.create(booking_group, booking)
          etravel_responses << ["Book Pax Info #{index+1}", export_flight_booking]
        end

        booking_group.flight_booking_details.each_with_index do |flight_detail, index|
          export_flight_itin = Etravel::BookFlightItinerary.create(booking_group, flight_detail)
          etravel_responses << ["Book Flight Itinerary #{index+1}", export_flight_itin]
        end

        export_flight_summary = Etravel::BookSummary.create(booking_group, invoice)
        etravel_responses << ['Book Summary', export_flight_summary]

        export_flight_payment = Etravel::BookPayment.create(booking_group, invoice)
        etravel_responses << ['Book Payment', export_flight_payment]

        etravel_responses.each do |response|
          if response.last == false
            export_flight_successful = false
          end
        end

        export_flight_successful

      else
        false
      end
    end

    def self.export_activity(booking_group, invoice)
      etravel_responses = []
      export_activity_successful = true

      if booking_group.present?

        booking_group.activity_bookings.each_with_index do |booking, index|
          export_misc = Etravel::BookMisc.create(booking_group, booking)
          etravel_responses << ["Book Misc", export_misc]
        end

        export_activity_booking = Etravel::BookPaxInfo.create(booking_group, booking_group.activity_bookings.first)
        etravel_responses << ["Book Pax Info", export_activity_booking]

        export_activity_summary = Etravel::BookSummary.create(booking_group, invoice)
        etravel_responses << ["Book Summary", export_activity_summary]

        export_activity_payment = Etravel::BookPayment.create(booking_group, invoice)
        etravel_responses << ["Book Payment", export_activity_payment]

        etravel_responses.each do |response|
          if response.last == false
            export_activity_successful = false
          end
        end

        export_activity_successful
      else
        false
      end
    end

    def self.export_hotel(booking_group, invoice)
      etravel_responses = []
      export_hotel_successful = true

      if booking_group.present?
        export_hotel = Etravel::BookHotel.create(booking_group)
        etravel_responses << ["Book Hotel", export_hotel]

        booking_group.guests.each_with_index do |booking, index|
          export_hotel_booking = Etravel::BookPaxInfo.create(booking_group, booking)
          etravel_responses << ["Book Pax Info #{index+1}", export_hotel_booking]
        end

        export_hotel_summary = Etravel::BookSummary.create(booking_group, invoice)
        etravel_responses << ["Book Summary", export_hotel_summary]

        export_hotel_payment = Etravel::BookPayment.create(booking_group, invoice)
        etravel_responses << ["Book Payment", export_hotel_payment]

        etravel_responses.each do |response|
          if response.last == false
            export_hotel_successful = false
          end
        end

        export_hotel_successful
      else
        false
      end
    end

    def self.export_wifi(booking_group, invoice)
      etravel_responses = []
      export_wifi_successful = true

      if booking_group.present?
        export_wifi = Etravel::BookMisc.create(booking_group, booking_group.roamingman_bookings.first)
        etravel_responses << ["Book Misc", export_wifi]

        export_wifi_booking = Etravel::BookPaxInfo.create(booking_group, booking_group.roamingman_bookings.first)
        etravel_responses << ["Book Pax Info", export_wifi_booking]

        export_wifi_summary = Etravel::BookSummary.create(booking_group, invoice)
        etravel_responses << ["Book Summary", export_wifi_summary]

        export_wifi_payment = Etravel::BookPayment.create(booking_group, invoice)
        etravel_responses << ["Book Payment", export_wifi_payment]

        etravel_responses.each do |response|
          if response.last == false
            export_wifi_successful = false
          end
        end

        export_wifi_successful
      else
        false
      end
    end

    def self.export_globaltix(booking_group, invoice)
      etravel_responses = []
      export_wifi_successful = true

      if booking_group.present?

        booking_group.globaltix_ticket_bookings.each_with_index do |booking, index|
          export_globaltix = Etravel::BookMisc.create(booking_group, booking)
          etravel_responses << ["Book Misc #{index+1}", export_globaltix]
        end

        export_globaltix_booking = Etravel::BookPaxInfo.create(booking_group, booking_group.globaltix_ticket_bookings.first)
        etravel_responses << ["Book Pax Info", export_globaltix_booking]

        export_globaltix_summary = Etravel::BookSummary.create(booking_group, invoice)
        etravel_responses << ["Book Summary", export_globaltix_summary]
        
        export_globaltix_payment = Etravel::BookPayment.create(booking_group, invoice)
        etravel_responses << ["Book Payment", export_globaltix_payment]

        etravel_responses.each do |response|
          if response.last == false
            export_wifi_successful = false
          end
        end

        export_wifi_successful
      else
        false
      end
    end


    # UPDATE Functions

    def self.update_tour(booking_group, invoice)
      etravel_responses = []
      update_tour_successful = true

      if booking_group.present?
        booking_group.bookings.each_with_index do |booking, index|
          update_tour = Etravel::BookTour.update(booking_group, booking)
          etravel_responses << ["Book Tour #{index+1}", update_tour]

          update_pax = Etravel::BookPaxInfo.update(booking_group, booking)
          etravel_responses << ["Book Pax Info #{index+1}", update_pax]
        end

        booking_group.tour.tour_flights.each_with_index do |tour_flight, index|
          update_tour_flight = Etravel::BookFlightItinerary.update(booking_group, tour_flight)
          etravel_responses << ["Book Flight Itinerary #{index+1}", update_tour_flight]
        end

        update_tour_summary = Etravel::BookSummary.update(booking_group, invoice)
        etravel_responses << ['Book Summary', update_tour_summary]

        update_tour_payment = Etravel::BookPayment.update(booking_group, invoice)
        etravel_responses << ['Book Payment', update_tour_payment]

        etravel_responses.each do |response|
          if response.last == false
            update_tour_successful = false
          end
        end

        update_tour_successful
      else
        false
      end
    end

    def self.update_land_tour(booking_group, invoice)
      etravel_responses = []
      update_tour_successful = true

      if booking_group.present?
        booking_group.bookings.each_with_index do |booking, index|
          update_tour = Etravel::BookTour.update(booking_group, booking)
          etravel_responses << ["Book Land Tour #{index+1}", update_tour]

          update_pax = Etravel::BookPaxInfo.update(booking_group, booking)
          etravel_responses << ["Book Pax Info #{index+1}", update_pax]
        end

        update_misc = Etravel::BookMisc.update(booking_group, invoice)
        etravel_responses << ["Book Misc", update_misc]

        update_tour_summary = Etravel::BookSummary.update(booking_group, invoice)
        etravel_responses << ['Book Summary', update_tour_summary]

        update_tour_payment = Etravel::BookPayment.update(booking_group, invoice)
        etravel_responses << ['Book Payment', update_tour_payment]

        etravel_responses.each do |response|
          if response.last == false
            update_tour_successful = false
          end
        end

        update_land_tour_successful
      else
        false
      end
    end

    def self.update_flight(booking_group, invoice)
      etravel_responses = []
      update_flight_successful = true

      if booking_group.present?
        update_flight = Etravel::BookFlight.update(booking_group)
        etravel_responses << ['Book Flight', update_flight]

        booking_group.flight_booking_details.each_with_index do |flight_detail, index|
          update_flight_itin = Etravel::BookFlightItinerary.update(booking_group, flight_detail)
          etravel_responses << ["Book Flight Itinerary #{index+1}", update_flight_itin]
        end

        booking_group.flight_bookings.each_with_index do |booking, index|
          update_flight_booking = Etravel::BookPaxInfo.update(booking_group, booking)
          etravel_responses << ["Book Pax Info #{index+1}", update_flight_booking]
        end

        update_flight_summary = Etravel::BookSummary.update(booking_group, invoice)
        etravel_responses << ['Book Summary', update_flight_summary]

        update_flight_payment = Etravel::BookPayment.update(booking_group, invoice)
        etravel_responses << ['Book Payment', update_flight_payment]

        etravel_responses.each do |response|
          if response.last == false
            update_flight_successful = false
          end
        end

        update_flight_successful

      else
        false
      end
    end

    def self.update_activity(booking_group, invoice)
      etravel_responses = []
      update_activity_successful = true

      if booking_group.present?

        booking_group.activity_bookings.each_with_index do |booking, index|
          update_misc = Etravel::BookMisc.update(booking_group, booking)
          etravel_responses << ["Book Misc", update_misc]
        end

        update_activity_booking = Etravel::BookPaxInfo.update(booking_group, booking_group.activity_bookings.first)
        etravel_responses << ["Book Pax Info", update_activity_booking]

        update_activity_summary = Etravel::BookSummary.update(booking_group, invoice)
        etravel_responses << ["Book Summary", update_activity_summary]

        update_activity_payment = Etravel::BookPayment.update(booking_group, invoice)
        etravel_responses << ["Book Payment", update_activity_payment]

        etravel_responses.each do |response|
          if response.last == false
            update_activity_successful = false
          end
        end

        update_activity_successful
      else
        false
      end
    end

    def self.update_hotel(booking_group, invoice)
      etravel_responses = []
      update_hotel_successful = true

      if booking_group.present?
        update_hotel = Etravel::BookHotel.update(booking_group)
        etravel_responses << ["Book Hotel", update_hotel]

        booking_group.guests.each_with_index do |booking, index|
          update_hotel_booking = Etravel::BookPaxInfo.update(booking_group, booking)
          etravel_responses << ["Book Pax Info #{index+1}", update_hotel_booking]
        end

        update_hotel_summary = Etravel::BookSummary.update(booking_group, invoice)
        etravel_responses << ["Book Summary", update_hotel_summary]

        update_hotel_payment = Etravel::BookPayment.update(booking_group, invoice)
        etravel_responses << ["Book Payment", update_hotel_payment]

        etravel_responses.each do |response|
          if response.last == false
            update_hotel_successful = false
          end
        end

        update_hotel_successful
      else
        false
      end
    end

    def self.update_wifi(booking_group, invoice)
      etravel_responses = []
      update_wifi_successful = true

      if booking_group.present?
        update_wifi = Etravel::BookMisc.update(booking_group)
        etravel_responses << ["Book Misc", update_wifi]

        update_wifi_booking = Etravel::BookPaxInfo.update(booking_group, booking_group.roamingman_bookings.first)
        etravel_responses << ["Book Pax Info", update_wifi_booking]

        update_wifi_summary = Etravel::BookSummary.update(booking_group, invoice)
        etravel_responses << ["Book Summary", update_wifi_summary]

        update_wifi_payment = Etravel::BookPayment.update(booking_group, invoice)
        etravel_responses << ["Book Payment", update_wifi_payment]

        etravel_responses.each do |response|
          if response.last == false
            update_wifi_successful = false
          end
        end

        update_wifi_successful
      else
        false
      end
    end

    def self.update_globaltix(booking_group, invoice)
      etravel_responses = []
      update_wifi_successful = true

      if booking_group.present?

        booking_group.globaltix_ticket_bookings.each_with_index do |booking, index|
          update_globaltix = Etravel::BookMisc.update(booking_group, booking)
          etravel_responses << ["Book Misc #{index+1}", update_globaltix]
        end

        update_globaltix_booking = Etravel::BookPaxInfo.update(booking_group, booking_group.globaltix_ticket_bookings.first)
        etravel_responses << ["Book Pax Info", update_globaltix_booking]

        update_globaltix_summary = Etravel::BookSummary.update(booking_group, invoice)
        etravel_responses << ["Book Summary", update_globaltix_summary]
        
        update_globaltix_payment = Etravel::BookPayment.update(booking_group, invoice)
        etravel_responses << ["Book Payment", update_globaltix_payment]

        etravel_responses.each do |response|
          if response.last == false
            update_wifi_successful = false
          end
        end

        update_wifi_successful
      else
        false
      end
    end


  end
end
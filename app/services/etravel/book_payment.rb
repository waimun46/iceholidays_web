module Etravel
  class BookPayment < Base

    attr_accessor :ibe_book_ref_no,
                  :receipt_no,
                  :receipt_date,
                  :item_no,
                  :status,
                  :cust_code,
                  :cust_name,
                  :desp,
                  :rmks1,
                  :rmks2,
                  :pymt_type,
                  :curr_code,
                  :curr_rate,
                  :cr_card_no,
                  :cr_card_amt,
                  :or_rmks,
                  :total_amt,
                  :total_f_amt,
                  :total_adt,
                  :total_chd,
                  :total_inf,
                  :total_senior,
                  :tour_code,
                  :dep_date,
                  :last_update,
                  :s_key

    def self.all
      response = Request.where('/etravelapi/api/BookPymt')

      book_summaries = response.map{|r| BookPayment.new(r)}

      book_summaries
    end

    def self.create(booking_group, invoice)

      if invoice.payment.present? && invoice.payment.paid? && invoice.payment.amount.present? && invoice.payment.amount > 0
        case booking_group.class.to_s
        when "BookingGroup"  
          params = BookPayment.generate_srs_params(booking_group, invoice)
        when "RoamingmanBookingGroup"
          params = BookPayment.generate_rmm_params(booking_group, invoice)
        when "GlobaltixTicketBookingGroup"
          params = BookPayment.generate_gtx_params(booking_group, invoice)
        when "FlightBookingGroup"
          params = BookPayment.generate_flt_params(booking_group, invoice)
        when "ActivityBookingGroup"
          params = BookPayment.generate_act_params(booking_group, invoice)
        when "TBOBooking"
          params = BookPayment.generate_htl_params(booking_group, invoice)
        when "LandTourBookingGroup"
          params = BookPayment.generate_ldt_params(booking_group, invoice)
        else
          false
        end

        response = Request.create('/etravelapi/api/BookPymt', params)

        if response.split(", ").last.downcase.include?("sucessful")
          Rails.logger.info "Book Payment API Successfully Created!"
          return true
        else
          Rails.logger.info "Book Payment API Response: #{response}"
          return false
        end
      else
        return true
      end
    end

    def self.find(ibebookref)
      response = Request.find('/etravelapi/api/BookPymt', ibebookref)

      book_summary = BookPayment.new(response[0])
    end

    def self.update(booking_group, invoice)

      if invoice.payment.present? && invoice.payment.paid? && invoice.payment.amount.present? && invoice.payment.amount > 0
        case booking_group.class.to_s
        when "BookingGroup"  
          params = BookPayment.generate_srs_params(booking_group, invoice)
        when "RoamingmanBookingGroup"
          params = BookPayment.generate_rmm_params(booking_group, invoice)
        when "GlobaltixTicketBookingGroup"
          params = BookPayment.generate_gtx_params(booking_group, invoice)
        when "FlightBookingGroup"
          params = BookPayment.generate_flt_params(booking_group, invoice)
        when "ActivityBookingGroup"
          params = BookPayment.generate_act_params(booking_group, invoice)
        when "TBOBooking"
          params = BookPayment.generate_htl_params(booking_group, invoice)
        when "LandTourBookingGroup"
          params = BookPayment.generate_ldt_params(booking_group, invoice)
        else
          false
        end

        response = Request.update('/etravelapi/api/BookPymt', params)

        if response.split(", ").last.downcase.include?("sucessful")
          Rails.logger.info "Book Payment API Successfully Updated!"
          return true
        else
          Rails.logger.info "Book Payment API Response: #{response}"
          return false
        end
      else
        return true
      end
    end

    def self.generate_srs_params(booking_group, invoice)
      tour = booking_group.tour
      booking = booking_group.bookings.first

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :receiptNo=>invoice.receipt_no,
        :receiptDate=>invoice.created_at.strftime("%d-%b-%Y"),
        :custCode=>booking.agent_user.code,
        :custName=>booking.name,
        :desp=>"SERIES PAYMENT",
        :pymtType=>invoice.payment.etravel_payment_type,
        :currCode=>"MYR",
        :currRate=>"1",
        :totalAmt=>invoice.payment.amount.to_f,
        :totalFAmt=>invoice.payment.amount.to_f,
        :tourCode=>tour.code,
        :depDate=>tour.departure_date.strftime("%d-%b-%Y"),
        :or_rmks=>invoice.payment.gateway_text
      }
      
    end

    def self.generate_ldt_params(booking_group, invoice)
      tour = booking_group.land_tour
      booking = booking_group.land_tour_bookings.first

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :receiptNo=>invoice.receipt_no,
        :receiptDate=>invoice.created_at.strftime("%d-%b-%Y"),
        :custCode=>booking.agent_user.code,
        :custName=>booking.name,
        :desp=>"GROUND TOUR PAYMENT",
        :pymtType=>invoice.payment.etravel_payment_type,
        :currCode=>"MYR",
        :currRate=>"1",
        :totalAmt=>invoice.payment.amount.to_f,
        :totalFAmt=>invoice.payment.amount.to_f,
        :tourCode=>tour.code,
        :depDate=>booking_group.departure_date.strftime("%d-%b-%Y"),
        :or_rmks=>invoice.payment.gateway_text
      }
      
    end

    def self.generate_rmm_params(booking_group, invoice)
      booking            = booking_group.roamingman_bookings.first
      roamingman_package = booking_group.roamingman_bookings.first.roamingman_package

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :receiptNo=>invoice.receipt_no,
        :receiptDate=>invoice.created_at.strftime("%d-%b-%Y"),
        :custCode=>booking.agent_user.code,
        :custName=>booking.name,
        :desp=>"WIFI PAYMENT",
        :pymtType=>invoice.payment.etravel_payment_type,
        :currCode=>"MYR",
        :currRate=>"1",
        :totalAmt=>invoice.payment.amount.to_f,
        :totalFAmt=>invoice.payment.amount.to_f,
        :depDate=>booking.depart_date.strftime("%d-%b-%Y"),
        :or_rmks=>invoice.payment.gateway_text
      }      
      
    end

    def self.generate_flt_params(booking_group, invoice)
      booking = booking_group.flight_bookings.first

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :receiptNo=>invoice.receipt_no,
        :receiptDate=>invoice.created_at.strftime("%d-%b-%Y"),
        :custCode=>booking.agent_user.code,
        :custName=>"#{booking.last_name} #{booking.first_name}",
        :desp=>"FLIGHT PAYMENT",
        :pymtType=>invoice.payment.etravel_payment_type,
        :currCode=>"MYR",
        :currRate=>"1",
        :totalAmt=>invoice.payment.amount.to_f,
        :totalFAmt=>invoice.payment.amount.to_f,
        :depDate=>booking_group.flight_booking_details.first.departure_time.strftime("%d-%b-%Y"),
        :or_rmks=>invoice.payment.gateway_text
      }  
    end

    def self.generate_gtx_params(booking_group, invoice)
      booking   = booking_group.globaltix_ticket_bookings.first
      globaltix = booking_group.globaltix_ticket_bookings.first.globaltix_ticket

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :receiptNo=>invoice.receipt_no,
        :receiptDate=>invoice.created_at.strftime("%d-%b-%Y"),
        :custCode=>booking.agent_user.code,
        :custName=>booking.name,
        :desp=>"GLOBALTIX PAYMENT",
        :pymtType=>invoice.payment.etravel_payment_type,
        :currCode=>"MYR",
        :currRate=>"1",
        :totalAmt=>invoice.payment.amount.to_f,
        :totalFAmt=>invoice.payment.amount.to_f,
        :or_rmks=>invoice.payment.gateway_text
      }  
    end

    def self.generate_act_params(booking_group, invoice)
      booking = booking_group.activity_bookings.first

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :receiptNo=>invoice.receipt_no,
        :receiptDate=>invoice.created_at.strftime("%d-%b-%Y"),
        :custCode=>booking.agent_user.code,
        :custName=>booking.name,
        :desp=>"ACTIVITY PAYMENT",
        :pymtType=>invoice.payment.etravel_payment_type,
        :currCode=>"MYR",
        :currRate=>"1",
        :totalAmt=>invoice.payment.amount.to_f,
        :totalFAmt=>invoice.payment.amount.to_f,
        :or_rmks=>invoice.payment.gateway_text
      }  
    end

    def self.generate_htl_params(booking_group, invoice)
      booking = booking_group.lead_guest

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :receiptNo=>invoice.receipt_no,
        :receiptDate=>invoice.created_at.strftime("%d-%b-%Y"),
        :custName=>"#{booking.last_name} #{booking.first_name}",
        :desp=>"HOTEL PAYMENT",
        :pymtType=>invoice.payment.etravel_payment_type,
        :currCode=>"MYR",
        :currRate=>"1",
        :totalAmt=>invoice.payment.amount.to_f,
        :totalFAmt=>invoice.payment.amount.to_f,
        :or_rmks=>invoice.payment.gateway_text
      }  
    end

  end
end

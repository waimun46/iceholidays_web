module Etravel
  class BookHotel < Base

    attr_accessor :ibe_book_ref_no,
                  :item_no,
                  :htl_code,
                  :htl_name,
                  :in_date,
                  :in_day,
                  :out_date,
                  :out_day,
                  :type,
                  :city_code,
                  :city_name,
                  :country_name,
                  :room_type,
                  :qty,
                  :ex_bed,
                  :tot_night,
                  :meal,
                  :curr_code,
                  :ex_rate,
                  :rm_price,
                  :total_f_price,
                  :total_price,
                  :supp_ref_no,
                  :desp,
                  :vch_rmk,
                  :prod_code,
                  :total_adt,
                  :total_chd,
                  :total_inf,
                  :total_senior,
                  :gst_rate,
                  :gst_amt,
                  :gst_total_amt

    def self.all
      response = Request.where('/etravelapi/api/BookHotel')

      book_hotels = response.map{|r| BookHotel.new(r)}

      book_hotels
    end

    def self.create(booking_group)
      hotel      = booking_group.hotel
      guest_hash = booking_group.guests.group(:guest_type).count

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        # :itemNo=>,
        :htlCode=>hotel.hotel_code,
        :htlName=>hotel.hotel_name,
        :inDate=>booking_group.check_in.strftime("%d-%b-%Y"),
        # :inDay=>,
        :outDate=>booking_group.check_out.strftime("%d-%b-%Y"),
        # :outDay=>,
        # :type=>,
        :cityCode=>hotel.city_code,
        :cityName=>hotel.city_name,
        :countryName=>hotel.country_name,
        :roomType=>booking_group.rooms[0]["room_type_name"],
        :qty=>booking_group.no_of_rooms,
        # :exBed=>,
        # :totNight=>,
        # :meal=>,
        # :currCode=>,
        # :exRate=>,
        # :rmPrice=>,
        # :totalFPrice=>,
        :totalPrice=>booking_group.total_fare.floor.to_f,
        # :suppRefNo=>,
        # :desp=>,
        # :vchRmk=>,
        # :prodCode=>,
        :totalAdt=>guest_hash["Adult"],
        :totalChd=>guest_hash["Child"],
        # :totalInf=>,
        # :totalSenior=>,
        # :gstRate=>,
        # :gstAmt=>,
        # :gstTotalAmt=>
      }

      response = Request.create('/etravelapi/api/BookHotel', params)

      if response.split(", ").last.downcase.include?("sucessful")
        Rails.logger.info "Book Hotel API Successfully Created!"
        return true
      else
        Rails.logger.info "Book Hotel API Response: #{response}"
        return false
      end
      
    end



    def self.update(booking_group)
      hotel      = booking_group.hotel
      guest_hash = booking_group.guests.group(:guest_type).count

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        # :itemNo=>,
        :htlCode=>hotel.hotel_code,
        :htlName=>hotel.hotel_name,
        :inDate=>booking_group.check_in.strftime("%d-%b-%Y"),
        # :inDay=>,
        :outDate=>booking_group.check_out.strftime("%d-%b-%Y"),
        # :outDay=>,
        # :type=>,
        :cityCode=>hotel.city_code,
        :cityName=>hotel.city_name,
        :countryName=>hotel.country_name,
        :roomType=>booking_group.rooms[0]["room_type_name"],
        :qty=>booking_group.no_of_rooms,
        # :exBed=>,
        # :totNight=>,
        # :meal=>,
        # :currCode=>,
        # :exRate=>,
        # :rmPrice=>,
        # :totalFPrice=>,
        :totalPrice=>booking_group.total_fare.to_f,
        # :suppRefNo=>,
        # :desp=>,
        # :vchRmk=>,
        # :prodCode=>,
        :totalAdt=>guest_hash["Adult"],
        :totalChd=>guest_hash["Child"],
        # :totalInf=>,
        # :totalSenior=>,
        # :gstRate=>,
        # :gstAmt=>,
        # :gstTotalAmt=>
      }

      response = Request.update('/etravelapi/api/BookHotel', params)

      if response.split(", ").last.downcase.include?("sucessful")
        Rails.logger.info "Book Hotel API Successfully Updated!"
        return true
      else
        Rails.logger.info "Book Hotel API Response: #{response}"
        return false
      end
      
    end

  end
end
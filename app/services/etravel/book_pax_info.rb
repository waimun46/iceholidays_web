module Etravel
  class BookPaxInfo < Base

    attr_accessor :sys_id,
                  :ibe_book_ref_no,
                  :item_no,
                  :pax_grp_no,
                  :pax_title,
                  :first_name,
                  :middle_name,
                  :last_name,
                  :pax_type,
                  :dob,
                  :ptc,
                  :pax_sex,
                  :passport_no,
                  :passport_validity,
                  :nationality,
                  :tkt_code,
                  :tkt_no_fr,
                  :tkt_no_to,
                  :org_tkt_no

    def self.all
      response = Request.where('/etravelapi/api/BookPaxInfo')

      book_pax_infos = response.map{|r| BookPaxInfo.new(r)}

      book_pax_infos
    end

    def self.create(booking_group, booking)

      case booking_group.class.to_s
      when "BookingGroup"
        params = BookPaxInfo.generate_srs_params(booking_group, booking)
      when "FlightBookingGroup"
        params = BookPaxInfo.generate_ftl_params(booking_group, booking)
      when "RoamingmanBookingGroup"
        params = BookPaxInfo.generate_rmm_params(booking_group, booking)
      when "GlobaltixTicketBookingGroup"
        params = BookPaxInfo.generate_gtx_params(booking_group, booking)
      when "ActivityBookingGroup"
        params = BookPaxInfo.generate_act_params(booking_group, booking)
      when "TBOBooking"
        params = BookPaxInfo.generate_hlt_params(booking_group, booking)
      when "LandTourBookingGroup"
        params = BookPaxInfo.generate_ldt_params(booking_group, booking)
      end

      response = Request.create('/etravelapi/api/BookPaxInfo', params)

      if response.split(", ").last.downcase.include?("sucessful")
        Rails.logger.info "Book Pax API Successfully Created!"
        return true
      else
        Rails.logger.info "Book Pax API Response: #{response}"
        return false
      end
    end

    def self.find(ibebookref)
      response = Request.find('/etravelapi/api/BookPaxInfo', ibebookref)

      book_pax_info = BookPaxInfo.new(response[0])
    end

    def self.update(booking_group, booking)

      case booking_group.class.to_s
      when "BookingGroup"
        params = BookPaxInfo.generate_srs_params(booking_group, booking)
      when "FlightBookingGroup"
        params = BookPaxInfo.generate_ftl_params(booking_group, booking)
      when "RoamingmanBookingGroup"
        params = BookPaxInfo.generate_rmm_params(booking_group, booking)
      when "GlobaltixTicketBookingGroup"
        params = BookPaxInfo.generate_gtx_params(booking_group, booking)
      when "ActivityBookingGroup"
        params = BookPaxInfo.generate_act_params(booking_group, booking)
      when "TBOBooking"
        params = BookPaxInfo.generate_hlt_params(booking_group, booking)
      when "LandTourBookingGroup"
        params = BookPaxInfo.generate_ldt_params(booking_group, booking)
      end

      response = Request.update('/etravelapi/api/BookPaxInfo', params)

      if response[0].split(", ").last.downcase.include?("sucessful")
        Rails.logger.info "Book Pax API Successfully Updated!"
        return true
      else
        Rails.logger.info "Book Pax API Response: #{response}"
        return false
      end
    end

    def self.generate_srs_params(booking_group, booking)
      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :paxTitle=>booking.designation,
        :firstName=>booking.name,
        # :paxSex=>booking.gender == 'male' ? 'M' : 'F',
        :dob=>booking.date_of_birth.strftime("%d-%b-%Y")
      }
    end

    def self.generate_ldt_params(booking_group, booking)
      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :paxTitle=>booking.designation,
        :firstName=>booking.name,
        # :paxSex=>booking.gender == 'male' ? 'M' : 'F',
        :dob=>booking.date_of_birth.strftime("%d-%b-%Y")
      }
    end

    def self.generate_ftl_params(booking_group, booking)
      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :paxTitle=>booking.designation,
        :firstName=>booking.first_name,
        :lastName=>booking.last_name,
        # :paxSex=>booking.designation == 'Mr' ? 'M' : 'F',
        :dob=>booking.date_of_birth.strftime("%d-%b-%Y")
      }
    end

    def self.generate_rmm_params(booking_group, booking)
      roamingman_package = booking_group.roamingman_bookings.first.roamingman_package

      params =   {
        :ibeBookRefNo=>booking_group.ref_no,
        # :paxTitle=>booking.designation,
        :firstName=>booking.name,
        # :paxSex=>booking.gender == 'male' ? 'M' : 'F',
        # :dob=>booking.date_of_birth.strftime("%d-%b-%Y")
      }
    end

    def self.generate_gtx_params(booking_group, booking)
      globaltix = booking_group.globaltix_ticket_bookings.first.globaltix_ticket

      params =   {
        :ibeBookRefNo=>booking_group.ref_no,
        :firstName=>booking.name,
      }
      
    end

    def self.generate_act_params(booking_group, booking)
      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :firstName=>booking.name,
      }
    end

    def self.generate_hlt_params(booking_group, booking)
      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        :paxTitle=>booking.title,
        :firstName=>booking.first_name,
        :lastName=>booking.last_name,
        # :paxSex=>booking.title == 'Mr' ? 'M' : 'F'
      }
    end

  end
end

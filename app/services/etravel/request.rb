module Etravel
  class Request
    class << self

      def get_json(path, params = {})
        response = api.get(path)

        [JSON.parse(response.body), response.status]
      end

      def update(path, params = {})
        response = api.put do |req|
          req.url path
          req.headers['Content-Type'] = 'application/json'
          req.body = params.to_json
        end

        [JSON.parse(response.body), response.status]
      end

      def where(resource_path, params = {})
        response, status = get_json(resource_path, params)
        if status == 200
          response
        else
          JSON.parse(response).merge({ status: status, errors: "No Result Found." })
        end
      end

      def post_json(path, params = {})
        response = api.post do |req|
          req.url path
          req.headers['Content-Type'] = 'application/json'
          req.body = params.to_json
        end

        [response.body, response.status]
      end

      def create(resource_path, params = {})
        if Rails.env.iceb2b?
          response, status = post_json(resource_path, params)
          if status == 200
            if response.split(", ").last.downcase.include?("sucessful")
              response
            else
              response
            end
          else
            JSON.parse(response).merge({errors: { status: status }} )
          end
        else
          "sucessful"
        end
      end

      def get_individual_json(path, ibebookref)
        response = api.get do |req|
          req.url path
          req.params['tIBEBookRef'] = ibebookref
        end

        [JSON.parse(response.body), response.status]
      end

      def find(resource_path, ibebookref)
        response, status = get_individual_json(resource_path, ibebookref)
        if status == 200 && response.present?
          response
        else
          response << { status: status, errors: "No Result Found." }
        end
      end

      def api
        Etravel::Connection.api
      end

    end
  end
end

module Etravel
  class BookMisc < Base

    attr_accessor :sys_id,
                  :ibe_book_ref_no,
                  :item_no,
                  :prod_code,
                  :prod_name,
                  :status,
                  :deadline,
                  :city_code,
                  :country_code,
                  :supp_code,
                  :supp_name,
                  :supp_ref_no,
                  :desp,
                  :rmks1,
                  :rmks2,
                  :in_date,
                  :out_date,
                  :service_type,
                  :tot_day,
                  :cnfm_no,
                  :vch_no,
                  :cust_rmks,
                  :vch_rmks,
                  :supp_rmks,
                  :adt_no,
                  :chd_no,
                  :senior_no,
                  :tot_qty,
                  :unit_price,
                  :unit_cost,
                  :unit_comm,
                  :tot_sell,
                  :tot_cost,
                  :gst_amt,
                  :gst_tax_code,
                  :gst_tax_rmks,
                  :gst_tax_rate,
                  :last_update

    def self.all
      response = Request.where('/etravelapi/api/BookMisc')

      book_miscs = response.map{|r| BookMisc.new(r)}

      book_miscs
    end

    def self.create(booking_group, booking)

      case booking_group.class.to_s
      when "RoamingmanBookingGroup"
        params = BookMisc.generate_rmm_params(booking_group)
      when "GlobaltixTicketBookingGroup"
        params = BookMisc.generate_gtx_params(booking_group, booking)
      when "ActivityBookingGroup"
        params = BookMisc.generate_act_params(booking_group, booking)
      when "FlightBookingGroup" #BRB
        params = BookMisc.generate_flt_params(booking_group)
      when "LandTourBookingGroup"
        params = BookMisc.generate_ldt_params(booking_group)
      else
        false
      end

      response = Request.create('/etravelapi/api/BookMisc', params)

      if response.split(", ").last.downcase.include?("sucessful")
        Rails.logger.info "Book Misc Successfully Created!"
        return true
      else
        Rails.logger.info "Book Misc Response: #{response}"
        return false
      end
    end

    def self.update(booking_group, booking)

      case booking_group.class.to_s
      when "RoamingmanBookingGroup"
        params = BookMisc.generate_rmm_params(booking_group)
      when "GlobaltixTicketBookingGroup"
        params = BookMisc.generate_gtx_params(booking_group, booking)
      when "ActivityBookingGroup"
        params = BookMisc.generate_act_params(booking_group, booking)
      when "LandTourBookingGroup"
        params = BookMisc.generate_ldt_params(booking_group)
      else
        false
      end

      response = Request.update('/etravelapi/api/BookMisc', params)

      if response.split(", ").last.downcase.include?("sucessful")
        Rails.logger.info "Book Misc Successfully Updated!"
        return true
      else
        Rails.logger.info "Book Misc Response: #{response}"
        return false
      end
    end

    def self.find(ibebookref)
      response = Request.find('/etravelapi/api/BookMisc', ibebookref)

      book_misc = BookMisc.new(response[0])
    end

    def self.generate_rmm_params(booking_group)
      booking            = booking_group.roamingman_bookings.first
      roamingman_package = booking_group.roamingman_bookings.first.roamingman_package

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        # :itemNo=>,
        :prodCode=>roamingman_package.code,
        :prodName=>roamingman_package.name,
        :status=>booking.status,
        # :cityCode=>,
        # :countryCode=>booking.country_code,
        # :suppCode=>,
        # :suppName=>,
        # :suppRefNo=>,
        :desp=>"Data Rules: #{roamingman_package.data_rules}",
        # :rmks1=>,
        # :rmks2=>,
        :inDate=>booking.depart_date.strftime("%d-%b-%Y"),
        :outDate=>booking.return_date.strftime("%d-%b-%Y"),
        # :serviceType=>,
        :totDay=>(booking.return_date - booking.depart_date).to_f,
        # :cnfmNo=>,
        # :vchNo=>,
        :custRmks=>booking_group.remark,
        # :vchRmks=>,
        # :suppRmks=>,
        # :adtNo=>,
        # :chdNo=>,
        # :seniorNo=>,
        :totQty=>booking.quantity,
        :unitPrice=>booking.price.to_f/booking.quantity.to_f,
        :unitCost=>booking.price.to_f/booking.quantity.to_f,
        # :unitComm=>,
        :totSell=>booking.price.to_f,
        :totCost=>booking.price.to_f,
      }

      params
    end

    def self.generate_ldt_params(booking_group)
      booking   = booking_group.land_tour_bookings.first
      land_tour = booking_group.land_tour_bookings.first.land_tour

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        # :itemNo=>,
        :prodCode=>land_tour.code,
        :prodName=>land_tour.title,
        :status=>booking.status,
        # :cityCode=>,
        # :countryCode=>,
        # :suppCode=>,
        # :suppName=>,
        # :suppRefNo=>,
        # :desp=>"sample string",
        # :rmks1=>,
        # :rmks2=>,
        :inDate=>booking_group.departure_date.strftime("%d-%b-%Y"),
        # :outDate=>,
        # :serviceType=>,
        # :totDay=>,
        # :cnfmNo=>,
        # :vchNo=>,
        :custRmks=>booking_group.remark,
        # :vchRmks=>,
        # :suppRmks=>,
        # :adtNo=>,
        # :chdNo=>,
        # :seniorNo=>,
        :totQty=>booking_group.land_tour_bookings.count,
        :unitPrice=>booking_group.group_charges.to_f,
        :unitCost=>booking_group.group_charges.to_f,
        # :unitComm=>,
        :totSell=>booking_group.group_charges.to_f,
        :totCost=>booking_group.group_charges.to_f,
      }

      params
    end

    def self.generate_gtx_params(booking_group, booking)
      globaltix = booking.globaltix_ticket
      country_code = ISO3166::Country.find_country_by_name(globaltix.country).try(:alpha2) || ""

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        # :prodCode=>,
        :prodName=>"#{globaltix.title} - #{globaltix.name}",
        :status=>booking.status.titleize,
        :countryCode=>country_code,
        # :suppCode=>"sample string 10",
        # :suppName=>"sample string 11",
        # :suppRefNo=>"sample string 12",
        # :desp=>"sample string 13",
        # :rmks1=>"sample string 14",
        # :rmks2=>"sample string 15",
        # :serviceType=>"sample string 18",
        # :totDay=>1.0,
        # :cnfmNo=>"sample string 19",
        # :vchNo=>"sample string 20",
        # :custRmks=>"sample string 21",
        # :vchRmks=>"sample string 22",
        # :suppRmks=>"sample string 23",
        # :adtNo=>1.0,
        # :chdNo=>1.0,
        # :seniorNo=>1.0,
        :totQty=>booking.quantity,
        :unitPrice=>booking.price.to_f/booking.quantity.to_f,
        :unitCost=>booking.price.to_f/booking.quantity.to_f,
        # :unitComm=>1.0,
        :totSell=>booking.price.to_f,
        :totCost=>booking.price.to_f,
        # :gstAmt=>1.0,
        # :gstTaxCode=>"sample string 24",
        # :gstTaxRmks=>"sample string 25",
        # :gstTaxRate=>1.0,
        # :lastUpdate=>"2019-11-10T00:31:15.1255768+08:00"
      }

      params
    end

    def self.generate_act_params(booking_group, booking)
      activity = booking.activity
      guest_hash = booking_group.activity_bookings.unscoped.group(:category).count
      country_code = ISO3166::Country.find_country_by_name(activity.country).try(:alpha2) || ""

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        # :prodCode=>activity.title,
        :prodName=>activity.title,
        # :status=>"sample string 6",
        # :cityCode=>"sample string 8",
        :countryCode=>country_code,
        # :suppCode=>"sample string 10",
        # :suppName=>"sample string 11",
        # :suppRefNo=>"sample string 12",
        # :desp=>activity.description,
        # :rmks1=>booking_group.remark,
        # :rmks2=>"sample string 15",
        # :inDate=>"sample string 16",
        # :outDate=>"sample string 17",
        # :serviceType=>"sample string 18",
        # :totDay=>1.0,
        # :cnfmNo=>"sample string 19",
        # :vchNo=>"sample string 20",
        # :custRmks=>"sample string 21",
        # :vchRmks=>"sample string 22",
        # :suppRmks=>"sample string 23",
        :adtNo=>guest_hash["Adult"],
        # :chdNo=>1.0,
        # :seniorNo=>1.0,
        :totQty=>booking.quantity,
        :unitPrice=>booking.price.to_f/booking.quantity.to_f,
        :unitCost=>booking.price.to_f/booking.quantity.to_f,
        # :unitComm=>1.0,
        :totSell=>booking.price.to_f,
        :totCost=>booking.price.to_f,

        # :totCost=>booking_group.activity_bookings.first.price.to_f,
        # :gstAmt=>1.0,
        # :gstTaxCode=>"sample string 24",
        # :gstTaxRmks=>"sample string 25",
        # :gstTaxRate=>1.0,
        # :lastUpdate=>"2019-11-10T00:31:15.1255768+08:00"
      }

      params
    end

    def self.generate_flt_params(booking_group)

      params = {
        :ibeBookRefNo=>booking_group.ref_no,
        # :prodCode=>activity.title,
        :prodName=>"Blue Ribbon Bag",
        # :status=>"sample string 6",
        # :cityCode=>"sample string 8",
        # :countryCode=>country_code,
        # :suppCode=>"sample string 10",
        # :suppName=>"sample string 11",
        # :suppRefNo=>"sample string 12",
        # :desp=>activity.description,
        # :rmks1=>booking_group.remark,
        # :rmks2=>"sample string 15",
        # :inDate=>"sample string 16",
        # :outDate=>"sample string 17",
        # :serviceType=>"sample string 18",
        # :totDay=>1.0,
        # :cnfmNo=>"sample string 19",
        # :vchNo=>"sample string 20",
        # :custRmks=>"sample string 21",
        # :vchRmks=>"sample string 22",
        # :suppRmks=>"sample string 23",
        # :adtNo=>guest_hash["Adult"],
        # :chdNo=>1.0,
        # :seniorNo=>1.0,
        :totQty=>1,
        :unitPrice=>BrbBooking::PRICE.to_f,
        :unitCost=>BrbBooking::PRICE.to_f,
        # :unitComm=>1.0,
        :totSell=>BrbBooking::PRICE.to_f,
        # :totCost=>booking_group.activity_bookings.first.price.to_f,
        # :gstAmt=>1.0,
        # :gstTaxCode=>"sample string 24",
        # :gstTaxRmks=>"sample string 25",
        # :gstTaxRate=>1.0,
        # :lastUpdate=>"2019-11-10T00:31:15.1255768+08:00"
      }

      params
    end



  end
end

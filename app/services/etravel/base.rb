module Etravel
  class Base
    attr_accessor :errors

    ITEM_TYPES = {tour:0, flight:1, activity:2, hotel:3, wifi:4, land_tour:5}

    def initialize(args = {})
      args.each do |name, value|
        attr_name = name.to_s.underscore
        send("#{attr_name}=", value) if respond_to?("#{attr_name}=")
      end
    end
  end
end

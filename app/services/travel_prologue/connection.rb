module TravelPrologue
  require 'faraday'
  require 'json'

  class Connection

    if Rails.env.iceb2b?
      BASE = Rails.application.credentials.travelprologue[:url]
      TOKEN = { Token: Rails.application.credentials.travelprologue[:token] }
    else
      BASE = 'http://api-dev.travelprologue.com'
      TOKEN = { Token: 'CRDNGRTUHYKASKICE' }
    end

    def self.api
      conn = Faraday.new(url: BASE) do |faraday|
        faraday.response :logger
        faraday.adapter Faraday.default_adapter
        faraday.headers['Content-Type'] = 'application/json'
      end
    end
  end

end

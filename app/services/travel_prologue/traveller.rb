module TravelPrologue
  class Traveller < Base

    attr_accessor :first_name,
                  :last_name,
                  :email,
                  :country_code,
                  :phone,
                  :phone_number,
                  :traveller_type,
                  :salutation,
                  :nationality,
                  :in_room_no

                  
  end
end

module TravelPrologue
  class Image < Base

    attr_accessor :title,
                  :url

    def image_url
      if url.present?
        url
      else
        ""
      end
    end

  end
end

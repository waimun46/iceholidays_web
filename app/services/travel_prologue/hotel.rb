module TravelPrologue
  class Hotel < Base

    attr_accessor :sequence,
                  :check_in,
                  :check_out,
                  :room_format,
                  :adults,
                  :childs,
                  :rooms,
                  :currency,
                  :destination,
                  :tp_hotel_id,
                  :hotel_id,
                  :hotel_name,
                  :description,
                  :hotel_image,
                  :star_rating,
                  :average_nightly_rate,
                  :average_nightly_strike_rate,
                  :latitude,
                  :longitude,
                  :promotion_text,
                  :hotel_code,
                  :provider,
                  :avail_token,
                  :room_no,
                  :free_cancellable,
                  :is_free_internet,
                  :total_records,
                  :room_desc,
                  :total_cost_inclusive,
                  :request_code, #hotel ends here
                  :city,
                  :country,
                  :street1,
                  :posttal_code,
                  :review_scores,
                  :review_count,
                  :recommendation_count,
                  :total_cost,
                  :no_of_nights,
                  :lst_hotel_images,
                  :lst_rooms,
                  :check_in_time,
                  :check_out_time,
                  :g_i_a_t_a_i_d


    def self.search(params, room_adults, room_childs, childs_ages, childs_beds, initial, request_id)
      rooms = params[:rooms].to_i
      room_format = []
      (1..rooms).each_with_index do |room, index|
        room_format << "room#{room}=#{room_adults[index]},0"
      end

      room_childs.each_with_index do |child, index|
        child = child.to_i
        if child.to_i >= 1
          (1..child).each_with_index do |c, index2|
            room_format[index] = room_format[index]+",#{childs_ages[index2]}-#{childs_beds[index2] == 'true' ? 1 : 0}"
          end

          childs_ages.slice!(0..(child-1))
          childs_beds.slice!(0..(child-1))
        end
      end

      if params[:checkin_checkout].present?
        range = params[:checkin_checkout].split(' - ')
        checkin_date = range[0].to_date
        checkout_date = range[1].to_date
      else
        checkin_date = params[:check_in_date]
        checkout_date = params[:check_out_date]
      end

      params = {
        CheckIn: checkin_date,
        CheckOut: checkout_date,
        RoomFormat: room_format.join('&'),
        roomBedFormat: room_format.join('&'),
        PassportIssuanceCountry: params[:country],
        CityID: params[:city_id].to_i,
        Initial: initial,
        RequestID: request_id
      }

      response = Request.where('/api/MSFHotels/Search', params)

      hotels = response["HotelsList"].map { |hotel| Hotel.new(hotel) } if response["HotelsList"].present?
      search_criteria = SearchCriteria.new(response["SearchCriteria"].merge({"StatusR"=> response["StatusR"]})) if response["SearchCriteria"].present?

      [ hotels, response[:errors], search_criteria ]
    end

    def self.getDetail(params)
      params = {
        "RequestCode": params["request_code"]
      }

      response = Request.where('/api/MSFHotels/GetDetail', params)

      hotel_details = Hotel.new(response)
    end

    def self.getDetailByRequestCode(code)
      params = {
        "RequestCode": code
      }

      response = Request.where('/api/MSFHotels/GetDetail', params)

      hotel_details = Hotel.new(response)
    end

    def self.getDetailByHotelID(hotel)
      cities = File.read("#{Rails.root}/vendor/assets/javascripts/citymapping.json")
      cities_hash = JSON.parse(cities)

      city = cities_hash.select {|city| city["city_name"] == hotel.city_name }

      if city.present?
        city_id = city.first["city_id"]
      else
        city_id = 0
      end

      params = {
        "TpHotelID": hotel.tp_hotel_id,
        "CheckIn": (Date.today + 2.days).strftime("%Y-%m-%d"),
        "CheckOut": (Date.today + 4.days).strftime("%Y-%m-%d"),
        "RoomFormat": "room1=2,0",
        "roomBedFormat": "room1=2,0",
        "Adults": 2,
        "Childs": 0,
        "Rooms": 1, 
        "Currency": "MYR", 
        "PassportIssuanceCountry": "MY",
        "CityID": city_id
      }

      response = Request.where('/api/MSFHotels/GetDetailByHotelID', params)

      hotel_detail = Hotel.new(response)
    end

    def self.getRateRules(request_code)
      params = {
        "RequestCode": request_code
      }

      response = Request.where('/api/MSFHotels/GetRateRules', params)
      rate_rule = TravelPrologue::RateRule.new(Hash[*response])
    end

    def image_url
      if hotel_image.present?
        hotel_image
      else
        ""
      end
    end
  end
end

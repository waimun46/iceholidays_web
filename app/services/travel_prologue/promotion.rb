module TravelPrologue
  class Promotion < Base

    attr_accessor :code,
                  :amount,
                  :end_date,
                  :start_date,
                  :short_marketing_text,
                  :nights_required,
                  :nights_fee,
                  :is_pos_exclusive


  end
end

module TravelPrologue
  class RateRule < Base

    attr_accessor :hotel_name,
                  :hotel_image,
                  :star_rating,
                  :chaincode,
                  :hotel_id,
                  :tp_hotel_id,
                  :city,
                  :state,
                  :country,
                  :postalcode,
                  :bf_type,
                  :price_break_down,
                  :purchase_token,
                  :agency_reference,
                  :is_own_product,
                  :total_cost_payable,
                  :actual_total_cost_inclusive,
                  :other_total_cost_payable,
                  :credit_exchange_rate,
                  :display_exchange_rate,
                  :require_one_name_per_room


  end
end

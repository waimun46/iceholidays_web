module TravelPrologue
  class CancellationPolicy < Base

    attr_accessor :description,
                  :orbitz_penalty,
                  :hotel_penalty,
                  :penalty_type_code,
                  :penalty_type,
                  :as_of_date


  end
end

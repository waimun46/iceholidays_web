module TravelPrologue
  class SearchCriteria < Base

    attr_accessor :check_in,
                  :check_out,
                  :currency,
                  :destination,
                  :request_id,
                  :status_r

  end
end

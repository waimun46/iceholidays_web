module TravelPrologue
  class Request
    class << self

      def post_json(path, params = {})
        response = api.post do |req|
          req.url path
          req.headers['Content-Type'] = 'application/json'
          req.body = params.merge(TravelPrologue::Connection::TOKEN).to_json
          req.options[:open_timeout] = 120
          req.options[:timeout] = 120
        end

        [response.body.present? ? JSON.parse(response.body) : { errors: "" }, response.status]
      end

      def where(resource_path, params = {})
        response, status = post_json(resource_path, params)
        if status == 200
          response
        else
          response.merge({ status: status, errors: "No Result Found." })
        end
      end

      def api
        TravelPrologue::Connection.api
      end

    end
  end
end

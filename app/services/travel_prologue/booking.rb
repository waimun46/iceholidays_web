module TravelPrologue
  class Booking < Base
    DEFAULT_EMAIL = 'g-trip@ice-holidays.com'

    attr_accessor :update_purchased_product_detail_request,
                  :unique_txn_id,
                  :amount,
                  :customer_name,
                  :customer_email,
                  :booking_status,
                  :booking_id,
                  :currency,
                  :error_msg,
                  :product_type,
                  :booking_ref,
                  :product_booking_id,
                  :obj_activity_search,
                  :status,
                  :is_pay_link,
                  :payment_id,
                  :payment_type,
                  :card_number,
                  :expiry_date,
                  :transaction_id,
                  :source_application,
                  :pnr_number,
                  :is_promotion,
                  :agent_id,
                  :paymenttypemsg,
                  :trans_pk,
                  :provider,
                  :cancellation_policy,
                  :important_information,
                  :additional_information


    def self.create(hotel_booking_params, user)
      travellers = []

      hotel_booking_params[:hotel_bookings].each_with_index do |traveller, index|
        travellers << {
                        Index: traveller[:index].to_i,
                        FirstName: traveller[:first_name],
                        LastName: traveller[:last_name],
                        Email: index == 0 ? DEFAULT_EMAIL : traveller[:email],
                        CountryCode: traveller[:country_code],
                        Phone: "+ 60",
                        PhoneNumber: traveller[:phone_number],
                        TravellerType: traveller[:traveller_type],
                        Salutation: traveller[:salutation],
                        InRoomNo: traveller[:in_room_no].to_s
                      }
      end

      params = {
        BookingSource: hotel_booking_params[:avail_token],
        Travellers: travellers,
        BillingAddressDetails:
        {
          BillingLine1: user.address,
          BillingLine2: '',
          BillingCity: user.city,
          BillingState: user.state,
          BillingCountry: user.country,
          BillingZipCode: user.postal
        }
      }

      response = Request.where('/api/MSFHotels/Book', params)

      booking = Booking.new(response)
      travellers = hotel_booking_params[:hotel_bookings].map { |t| TravelPrologue::Traveller.new(t) }

      [booking, travellers]
    end

    def self.cancel(booking)
      
      params = {
        BookingRef: booking.booking_ref, 
        Email: booking.customer_email
      }

      response = Request.where('/api/MSFHotels/Cancel/', params)

      cancelled_booking = TravelPrologue::CancelledBooking.new(response)

      if cancelled_booking.status == true
        cancelled_hotel_booking_group = CancelledHotelBookingGroup.create(
          cancellation_number: cancelled_booking.cancellation_number,
          message: cancelled_booking.message,
          status: cancelled_booking.status,
          hotel_booking_group_id: booking.id
        )
      else
        cancelled_booking
      end
    end

  end
end

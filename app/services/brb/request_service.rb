module Brb
  class RequestService < Brb::BaseService
    class << self
      def post_json(path, params = {})
        response = api.post do |req|
          req.url(path)
          req.headers['Content-Type'] = 'application/json'
          req.body = params.to_json
        end

        [JSON.parse(response.body), response.status]
      end

      def api
        Brb::ConnectionService.api
      end
    end
  end
end

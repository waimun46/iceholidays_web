module Brb
  class DataService < Brb::BaseService
    class << self
      def where(path, params={})
        response = Brb::RequestService.post_json("Data/"+ path, params)
      end

      def airlines_list
        airlines = where("GetAirlineList")
        airlines.first["Data"].each do |airline|
          Airline.rdb[:brb_mapping][airline['AirlineIATACode']].set airline['AirlineId']
        end
      end

      def products_list
        where("GetProductList")
      end

      def get_airline_id(iata)
        airline_id = Airline.rdb[:brb_mapping][iata].get
        if airline_id.blank?
          airlines_list
          airline_id = Airline.rdb[:brb_mapping][iata].get
        end
        airline_id
      end

    end
  end
end

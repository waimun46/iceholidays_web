module Brb
  class PaymentService < Brb::BaseService
    class << self
      def purchase(params={})

        if Rails.env.iceb2b?
          response = Brb::RequestService.post_json("Service/Purchase", params)
          Rails.logger.info response
          if response.first.fetch("Status", true)
            [response.first.fetch("Data"), response.first.fetch("StatusCode")]
          else
            [nil, nil]
          end
        else
          ['testing_export', 'testing_export']
        end
      end

      def update(params={})
        response = Brb::RequestService.post_json("Service/Edit", params)

        if response.first.fetch("Status", true)
          response.first.fetch("Data")
        else
          nil
        end
      end

      def cancel(params={})
        response = Brb::RequestService.post_json("Service/Cancel", params)
        Rails.logger.info response
        if response.first.fetch("Status", true)
          response.first.fetch("Data")
        else
          response
        end
      end

      def promo_code(params={})
        response = Brb::RequestService.post_json("Service/ValidatePromoCode", params)
        if response.first.fetch("Status", true)
          # Success response
          # Update Record
        else

        end
      end
    end
  end
end

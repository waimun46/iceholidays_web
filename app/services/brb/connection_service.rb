module Brb
  class ConnectionService < Brb::BaseService

    class << self
      if Rails.env.iceb2b?
        BRB_URL = Rails.application.credentials.brb[:url]
        BRB_TOKEN = Rails.application.credentials.brb[:token]
      else
        BRB_URL = "http://validation-api.blueribbonbags.com/api"
        BRB_TOKEN = "MzdkMTU3YjgtNjExOS00MTk2LWE0ZDAtNDJjODM2MDM2YjUy"
      end

      def api
        Faraday.new(url: BRB_URL) do |faraday|
          faraday.response :logger
          faraday.adapter Faraday.default_adapter
          faraday.headers['Accept-Version'] = '1.0'
          faraday.headers['Authorization'] = "Authorization #{BRB_TOKEN}"
        end
      end
    end
  end
end

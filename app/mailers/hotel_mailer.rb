class HotelMailer < ApplicationMailer
  # layout 'mailer'
  layout 'foundation_mailer'
  default from: "Travelb2b Team<no-reply@travelb2b.my>"

  if Rails.env.iceb2b?
    self.delivery_method = :new_domain
  end

  # def send_booking_confirmation(hotel_booking_group)
  #   @hotel_booking_group = hotel_booking_group
  #   first_guest_email = hotel_booking_group.hotel_bookings.first.email
  #   mail(to: first_guest_email, bcc: "ecomm@ice-holidays.com", subject: "Hotel Booking Confirmation")
  # end

  def notify(user, hotel_booking_group, email)
    @user = user
    @hotel_booking_group = hotel_booking_group
    @hotel_booking = @hotel_booking_group.hotel_bookings.first
    @payment = @hotel_booking_group.payment
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: "ecomm@ice-holidays.com", subject: "Hotel Booking Receipt")
  end

  def receipt(user, hotel_booking_group, email)
    @user = user
    @hotel_booking_group = hotel_booking_group
    @hotel_booking = @hotel_booking_group.hotel_bookings.first
    @payment = @hotel_booking_group.payment
    @paid_amount = @payment.amount
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: "ecomm@ice-holidays.com", subject: "Hotel Booking Receipt")
  end

end
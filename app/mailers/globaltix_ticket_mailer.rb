class GlobaltixTicketMailer < ApplicationMailer
  layout 'foundation_mailer'
  default from: "Travelb2b Team<no-reply@travelb2b.my>"

  if Rails.env.iceb2b?
    self.delivery_method = :new_domain
  end

  def notify(user, globaltix_ticket_booking_group, email)
    @user = user
    @globaltix_ticket_booking_group = globaltix_ticket_booking_group
    @globaltix_ticket_booking = @globaltix_ticket_booking_group.globaltix_ticket_bookings.first
    @payment = @globaltix_ticket_booking_group.payment
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: "ecomm@ice-holidays.com", subject: "Activity Ticket Booking Details")
    # mail(to: email, bcc: 'jasonlam@ice-holidays.com', subject: "Flight Booking Details")
  end

  def receipt(user, globaltix_ticket_booking_group, email)
    @user = user
    @globaltix_ticket_booking_group = globaltix_ticket_booking_group
    @globaltix_ticket_booking = @globaltix_ticket_booking_group.globaltix_ticket_bookings.first
    @payment = @globaltix_ticket_booking_group.payment
    @paid_amount = @payment.amount
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: "ecomm@ice-holidays.com", subject: "Activity Booking Receipt")
  end

  def notify_ticket_na(globaltix_ticket)
    @globaltix_ticket = globaltix_ticket
    mail(to: "ecomm@ice-holidays.com", cc: "ga@ice-holidays.com", subject: "Globaltix Ticket set to NA")
  end

end
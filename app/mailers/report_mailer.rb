class ReportMailer < ApplicationMailer
  # layout 'mailer'
  layout 'foundation_mailer'

  def daily_sales_report(recipients, payments)
    @payments = payments
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: recipients, bcc: 'achilles.tee@gmail.com', subject: "TravelB2B Daily Sales Report")
  end
end

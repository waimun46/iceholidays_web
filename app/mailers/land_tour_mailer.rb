class LandTourMailer < ApplicationMailer
  # layout 'mailer'
  layout 'foundation_mailer'
  default from: "Travelb2b Team<no-reply@travelb2b.my>"

  if Rails.env.iceb2b?
    self.delivery_method = :new_domain
  end

  def notify(user, land_tour_booking_group, email, hotel = false)
    @user = user
    @land_tour_booking_group = land_tour_booking_group
    @land_tour_booking = @land_tour_booking_group.land_tour_bookings.first
    @payments = @land_tour_booking_group.payments
    @land_tour = @land_tour_booking_group.land_tour
    attachments.inline['gtrip-header.jpg'] = File.read("#{Rails.root}/app/assets/images/gtrip-header.jpg")
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")

    if hotel
      subject = "TRAVELB2B: Ground Arrangement Booking Confirmation Update"
    else
      subject = "TRAVELB2B: Ground Arrangement Booking Confirmation"
    end

    mail(to: email, bcc: "ga@ice-holidays.com", subject: "#{subject}")
  end

  def notify_supplier(user, land_tour_booking_group, email)
    @user = user
    @land_tour_booking_group = land_tour_booking_group
    @land_tour_booking = @land_tour_booking_group.land_tour_bookings.first
    @payments = @land_tour_booking_group.payments
    @land_tour = @land_tour_booking_group.land_tour
    attachments.inline['gtrip-header.jpg'] = File.read("#{Rails.root}/app/assets/images/gtrip-header.jpg")
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")

    mail(to: email, subject: "TRAVELB2B: Ground Arrangement Booking Confirmation")
  end

  def receipt(user, land_tour_booking_group, email)
    @user = user
    @land_tour_booking_group = land_tour_booking_group
    @land_tour_booking = @land_tour_booking_group.land_tour_bookings.first
    @payments = @land_tour_booking_group.payments
    @paid_amount = @payments.paid.map(&:amount).inject(:+)
    attachments.inline['gtrip-header.jpg'] = File.read("#{Rails.root}/app/assets/images/gtrip-header.jpg")
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: "ga@ice-holidays.com", subject: "TRAVELB2B: Ground Arrangement Booking Receipt")
  end

  def payment_reminder(user, land_tour_booking_group, email)
    @user = user
    @land_tour_booking_group = land_tour_booking_group
    attachments.inline['gtrip-header.jpg'] = File.read("#{Rails.root}/app/assets/images/gtrip-header.jpg")
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: 'ga@ice-holidays.com', subject: "Payment Reminder (Ground Tour: #{@land_tour_booking_group.land_tour.code})")
  end

  def auto_cancellation(user, land_tour_booking_group, email)
    @user = user
    @land_tour_booking_group = land_tour_booking_group
    mail(to: email, bcc: 'ga@ice-holidays.com', subject: "Auto-Cancellation of Ground Arrangement Reservation")
  end

  def voucher(user, land_tour_booking_group, email)
    @user = user
    @land_tour_booking_group = land_tour_booking_group
    @land_tour_booking = @land_tour_booking_group.land_tour_bookings.first
    @land_tour = @land_tour_booking_group.land_tour
    attachments.inline['gtrip-header.jpg'] = File.read("#{Rails.root}/app/assets/images/gtrip-header.jpg")
    mail(to: email, bcc: 'ga@ice-holidays.com', subject: "Ground Arrangement Voucher")
  end
end

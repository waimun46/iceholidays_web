class ActivityMailer < ApplicationMailer
  layout 'foundation_mailer'
  default from: "Travelb2b Team<no-reply@travelb2b.my>"

  if Rails.env.iceb2b?
    self.delivery_method = :new_domain
  end

  def notify(user, activity_booking_group, email)
    @user = user
    @activity_booking_group = activity_booking_group
    @activity_booking = @activity_booking_group.activity_bookings.first
    @payment = @activity_booking_group.payment
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: "ecomm@ice-holidays.com", subject: "Activity Booking Details")
    # mail(to: email, bcc: 'jasonlam@ice-holidays.com', subject: "Flight Booking Details")
  end

  def receipt(user, activity_booking_group, email)
    @user = user
    @activity_booking_group = activity_booking_group
    @activity_booking = @activity_booking_group.activity_bookings.first
    @payment = @activity_booking_group.payment
    @paid_amount = @payment.amount
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: "ecomm@ice-holidays.com", subject: "Activity Booking Receipt")
    # mail(to: email, bcc: 'jasonlam@ice-holidays.com', subject: "Flight Booking Details")
  end

  def notify_admin(user, activity_booking_group)
    @user = user
    @activity_booking_group = activity_booking_group
    @activity_booking = @activity_booking_group.activity_bookings.first
    @payment = @activity_booking_group.payment
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: "ecomm@ice-holidays.com", cc: 'ga@ice-holidays.com', subject: "Activity Order - ##{activity_booking_group.id} (#{@activity_booking.name})")
  end

end
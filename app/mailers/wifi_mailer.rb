class WifiMailer < ApplicationMailer
  layout 'foundation_mailer'
  default from: "Travelb2b Team<no-reply@travelb2b.my>"

  if Rails.env.iceb2b?
    self.delivery_method = :new_domain
  end

  def notify(user, wifi_booking_group, email)
    @user = user
    @wifi_booking_group = wifi_booking_group
    @wifi_booking = @wifi_booking_group.roamingman_bookings.first
    @payment = @wifi_booking_group.payment
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: "ecomm@ice-holidays.com", subject: "Wifi Booking Details")
  end

  def receipt(user, wifi_booking_group, email)
    @user = user
    @wifi_booking_group = wifi_booking_group
    @wifi_booking = @wifi_booking_group.roamingman_bookings.first
    @payment = @wifi_booking_group.payment
    @paid_amount = @payment.amount
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: "ecomm@ice-holidays.com", subject: "Wifi Booking Receipt")
  end

end
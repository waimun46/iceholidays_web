class TBOHotelMailer < ApplicationMailer
  layout 'mailer'
  default from: "Travelb2b Team<no-reply@travelb2b.my>"

  if Rails.env.iceb2b?
    self.delivery_method = :new_domain
  end

  def send_booking_confirmation(booking)
    @booking = booking  
    mail(to: booking.email, subject: "Hotel Booking Confirmation")
  end
end
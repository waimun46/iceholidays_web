class BookingMailer < ApplicationMailer
  # layout 'mailer'
  layout 'foundation_mailer'

  def reminder(user, booking_group, email)
    @user = user
    @booking_group = booking_group
    mail(to: email, bcc: 'icersvn@gmail.com', subject: "Reservation Deposit Reminder")
  end

  def last_edit_reminder(user, booking_group, email)
    @user = user
    @booking_group = booking_group
    mail(to: email, subject: "Editable Time Reminder - #{ @booking_group.tour.code }")
  end

  def auto_cancellation(user, booking_group, email)
    @user = user
    @booking_group = booking_group
    mail(to: email, bcc: 'icersvn@gmail.com', subject: "Auto-Cancellation of Reservation")
  end
end
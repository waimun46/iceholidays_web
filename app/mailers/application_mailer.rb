class ApplicationMailer < ActionMailer::Base
  default from: "Iceb2b Team<no-reply@iceb2b.my>"
end

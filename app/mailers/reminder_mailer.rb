class ReminderMailer < ApplicationMailer
  layout 'mailer'

  def reminder(reminder)
    @reminder_date = reminder.reminder_date
    @description = reminder.description
    @taggable = reminder.taggable
    @email = reminder.email
    mail(to: @email, subject: "#{reminder.taggable_type.titleize} Reminder")
  end
end

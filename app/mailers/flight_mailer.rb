class FlightMailer < ApplicationMailer
  layout 'foundation_mailer'
  default from: "Travelb2b Team<no-reply@travelb2b.my>"

  if Rails.env.iceb2b?
    self.delivery_method = :new_domain
  end

  def notify(user, flight_booking_group, email)
    @user = user
    @flight_booking_group = flight_booking_group
    @flight_booking = @flight_booking_group.flight_bookings.first
    @payment = @flight_booking_group.payment
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: ['jasonlam@ice-holidays.com', 'gdflight@ice-holidays.com', 'ecomm@ice-holidays.com'], subject: "Flight Booking Details")
  end

  def receipt(user, flight_booking_group, email)
    @user = user
    @flight_booking_group = flight_booking_group
    @payment = @flight_booking_group.payment
    @paid_amount = @payment.amount
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: ['jasonlam@ice-holidays.com', 'gdflight@ice-holidays.com', 'ecomm@ice-holidays.com'], subject: "Flight Booking Receipt")
  end

end
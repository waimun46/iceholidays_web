class TourMailer < ApplicationMailer
  # layout 'mailer'
  layout 'foundation_mailer'
  default from: "Travelb2b Team<no-reply@travelb2b.my>"

  if Rails.env.iceb2b?
    self.delivery_method = :new_domain
  end

  def notify(user, tour_booking_group, email)
    @user = user
    @tour_booking_group = tour_booking_group
    @tour_booking = @tour_booking_group.bookings.first
    @payments = @tour_booking_group.payments
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: "ecomm@ice-holidays.com", subject: "TRAVELB2B: Series Tour Booking Details")
  end

  def receipt(user, tour_booking_group, email)
    @user = user
    @tour_booking_group = tour_booking_group
    @tour_booking = @tour_booking_group.bookings.first
    @payments = @tour_booking_group.payments
    @paid_amount = @payments.paid.map(&:amount).inject(:+)
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: "ecomm@ice-holidays.com", subject: "Tour Booking Receipt")
  end

  def payment_reminder(user, tour_booking_group, email, type = '')
    @user = user
    @tour_booking_group = tour_booking_group
    @type = type
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: "ecomm@ice-holidays.com", subject: "#{type.present? ? type.titleize : ''} Payment Reminder (Tour: #{@tour_booking_group.tour.code})")
  end

  def auto_cancellation(user, tour_booking_group, email)
    @user = user
    @tour_booking_group = tour_booking_group
    attachments.inline['travelb2b-logo.png'] = File.read("#{Rails.root}/app/assets/images/travelb2b-logo.png")
    mail(to: email, bcc: "ecomm@ice-holidays.com", subject: "Cancellation On (Tour: #{@tour_booking_group.tour.code})")
  end
end

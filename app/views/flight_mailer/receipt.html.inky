<container>
  <style>
    .price-list-header .columns{
      padding: 0 !important;
      background-color: #000;
    }
    .price-list .columns{
      padding: 2px !important;
      word-break: break-all;
    }

    .price-list-header .columns p{
      margin: 5px 1px;
    }

    .price-list-header .columns .text-2 {
      color: #fff;
    }

    .text-2 {
      font-size: .8em;
    }
  </style>

  <row>
    <columns>
      <row>
        <columns>
          <center><%= image_tag attachments['travelb2b-logo.png'].url, size: "120x120" %></center>
        </columns>
      </row>
      <row>
        <columns>
          <center><h1>Travelb2b.my</h1></center>
          <br />
          <center><h2>Confirmation</h2></center>
        </columns>
      </row>
      <row>
        <columns>
          <hr />
        </columns>
      </row>
    </columns>
  </row>

  <row>
    <columns small="6" large="4">Order Number</columns>
    <columns small="6" large="8">:#<%= @flight_booking_group.id %></columns>
  </row>
  <row>
    <columns small="6" large="4">GDS Reference</columns>
    <columns small="6" large="8">:#<%= @flight_booking_group.host_code %></columns>
  </row>
  <row>
    <columns small="6" large="4">User ID</columns>
    <columns small="6" large="8">:<%= @user.username %></columns>
  </row>
  <row>
    <columns small="6" large="4">Booking Date</columns>
    <columns small="6" large="8">:<%= @flight_booking_group.created_at.strftime("%m/%d/%Y") %></columns>
  </row>
  <row>
    <columns small="6" large="4">Order Total</columns>
    <columns small="6" large="8">:<%= number_to_currency(@flight_booking_group.price ,precision: 2, unit: 'RM ') %></columns>
  </row>
  <row>
    <columns small="6" large="4">Payment Method</columns>
    <columns small="6" large="8">:<%= @payment.gateway_text %></columns>
  </row>
  <row>
    <columns small="6" large="4">Ticket Detail</columns>
    <columns small="6" large="8">
      <% @flight_booking_group.flight_booking_details.each_with_index do |flight, index| %>
        <% destination_detail = "" %>
        <% if index > 0 && @flight_booking_group.flight_booking_details[index-1].connection %>
          <% next %>
        <% end %>
        <% if flight.connection %>
          <% stop = 0 %>
          <% while @flight_booking_group.flight_booking_details[index+stop].connection %>
            <% stop += 1 %>
          <% end %>
          <% destination_detail = "#{@flight_booking_group.flight_booking_details[index+stop].destination} (#{stop} stop)" %>
        <% else %>
          <% destination_detail = flight.destination %>
        <% end %>
        <%= "#{flight.origin} #{destination_detail}  #{flight.departure_time.strftime("%F %R")}" %><br>
      <% end %>
    </columns>
  </row>

  <row>
    <columns small="6" large="4">Ticket Detail</columns>
    <columns small="6" large="8">
      <% @flight_booking_group.flight_bookings.each do |booking| %>
        <%= booking.name %> <%= booking.designation %> (<%= booking.category %>)<br>
      <% end %>
    </columns>
  </row>
  <spacer size="15"></spacer>

  <row>
    <columns>
      <callout>
        <row class="price-list-header">
          <columns large="6" small="5">
            <p class="text-2">Description</p>
          </columns>
          <columns large="1" small="1">
            <p class="text-2">Unit</p>
          </columns>
          <columns large="2" small="3">
            <p class="text-2">Price</p>
          </columns>
          <columns large="3" small="3">
            <p class="text-2 text-right">Total</p>
          </columns>
        </row>

        <% booking_array = [] %>
        <% @flight_booking_group.flight_bookings.each do |booking| %>
          <% index = booking_array.find_index{|booking_array|booking_array[:category] == booking.category} %>
          <% if index.present? %>
            <% booking_array[index][:quantity] += 1 %>
          <% else %>
            <% booking_array << { category: booking.category, quantity: 1, unit_price: booking.price } %>
          <% end %>
        <% end %>

        <% booking_array.each do |booking| %>
          <row class="price-list">
            <columns large="6" small="5" valign="middle">
              <p class="text-2"><%= booking[:category] %></p>
            </columns>
            <columns large="1" small="1" valign="middle">
              <p class="text-2"><%= booking[:quantity] %></p>
            </columns>
            <columns large="2" small="3" valign="middle">
              <p class="text-2"><%= number_to_currency(booking[:unit_price], precision: 2, unit: 'RM ') %></p>
            </columns>
            <columns large="3" small="3" valign="middle">
              <p class="text-2 text-right"><%= number_to_currency(booking[:unit_price] * booking[:quantity], precision: 2, unit: 'RM ') %></p>
            </columns>
          </row>
          <hr/>
        <% end %>
        <% brb_count =  @flight_booking_group.brb_bookings.count %>
        <% if brb_count > 0 %>
          <row class="price-list">
            <columns large="6" small="5" valign="middle">
              <p class="text-2">BRB</p>
            </columns>
            <columns large="1" small="1" valign="middle">
              <p class="text-2"><%= brb_count %></p>
            </columns>
            <columns large="2" small="3" valign="middle">
              <p class="text-2"><%= number_to_currency(BrbBooking::PRICE, precision: 2, unit: 'RM ') %></p>
            </columns>
            <columns large="3" small="3" valign="middle">
              <p class="text-2 text-right"><%= number_to_currency(BrbBooking::PRICE * brb_count, precision: 2, unit: 'RM ') %></p>
            </columns>
          </row>
          <hr/>
        <% end %>
        <row class="price-list">
          <columns large="8" small="8">
            <p class="text-2 text-right">Total Purchase :</p>
            <p class="text-2 text-right">Payment Amount :</p>
          </columns>
          <columns large="4" small="4">
            <p class="text-2 text-right"><%= number_to_currency(@flight_booking_group.price ,precision: 2, unit: 'RM ') %></p>
            <p class="text-2 text-right"><%= number_to_currency(@paid_amount ,precision: 2, unit: 'RM ') %></p>
          </columns>
        </row>
        
        <% if @payment.gateway == "credit_transaction" %>
          <hr/>
          <row class="price-list">
            <columns large="8" small="8">
              <p class="text-2 text-right">Credit Balance :</p>
            </columns>
            <columns large="4" small="4">
              <p class="text-2 text-right"><%= @user.credits %></p>
            </columns>
          </row>
        <% end %>

      </callout>
    </column>
  </row>
  
  

</container>
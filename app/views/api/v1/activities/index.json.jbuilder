json.activities @activities.each do |activity|
  if activity.present?
    json.taggable_id activity.id
    json.category activity.category.present? ? activity.category.humanize : ''
    json.title activity.title
    if activity.class.name == "Activity"
      json.taggable_type "ACT"
      json.price activity.min_price.ceil
      json.highlight activity.highlight
      json.terms_and_conditions activity.terms_and_conditions
      json.operation_days activity.join_operation_days
      json.categories activity.join_categories
      json.travelling_date_start activity.travelling_date_start.try(:strftime, "%F %H:%M %p")
      json.travelling_date_end activity.travelling_date_end.try(:strftime, "%F %H:%M %p")
      json.images activity.covers.each do |cover|
         json.url cover.image.url
      end
    else
      globaltix_tickets = activity.globaltix_tickets.active
      attraction, attraction_err = Globaltix::Attraction.find(activity.attraction_id)
      json.taggable_type "GT"
      if globaltix_tickets.present?
        json.price activity.min_price.ceil
      else
        json.price 0
      end
      json.terms_and_conditions activity.term_and_condition
      json.operation_days activity.hours_of_operation
      json.images activity.covers.count > 0 ? activity.covers.first.image.url : attraction.image_url(:banner)
    end
    json.country activity.country
    json.description activity.description
  end
end

# json.activities @activities.each do |activity|
#   if activity.present?
#     if activity.first.class.name == "Activity"
#     json.activity activity.each do |data|
#         json.taggable_id data.id
#         json.taggable_type "ACT" # data.class.name
#         json.category data.category.present? ? data.category.humanize : ''
#         json.title data.title
#         if data.min_price.present?
#           json.price data.dta_min_price
#         else
#           json.price 0
#         end
#         json.country data.country
#         json.description data.description
#         json.highlight data.highlight
#         json.terms_and_conditions data.terms_and_conditions
#         json.operation_days data.join_operation_days
#         json.categories data.join_categories
#         json.travelling_date_start data.travelling_date_start.try(:strftime, "%F %H:%M %p")
#         json.travelling_date_end data.travelling_date_end.try(:strftime, "%F %H:%M %p")
#         json.images data.covers.each do |cover|
#            json.url cover.image.url
#         end
#       end
#     else
#     json.globaltix activity.each do |attraction_id, globaltix_tickets|
#         globaltix_attraction = GlobaltixAttraction.includes(:covers).find_by(attraction_id: attraction_id)
#         globaltix_tickets = globaltix_attraction.globaltix_tickets.active
#         tickets, tickets_err = Globaltix::Ticket.where(attraction_id: globaltix_attraction.attraction_id)
#         attraction, attraction_err = Globaltix::Attraction.find(globaltix_attraction.attraction_id)
#         json.taggable_id globaltix_attraction.id
#         json.taggable_type "GT" # globaltix_tickets.first.class.name
#         json.category globaltix_attraction.category.present? ? globaltix_attraction.category.humanize : ''
#         json.title globaltix_attraction.title
#         if globaltix_tickets.present?
#           json.price GlobaltixTicket.dta_min_price(globaltix_tickets)
#         else
#           json.price 0
#         end
#         json.country globaltix_attraction.country
#         json.description globaltix_attraction.description
#         json.terms_and_conditions globaltix_attraction.term_and_condition
#         json.operation_days globaltix_attraction.hours_of_operation
#         json.images globaltix_attraction.covers.count > 0 ? globaltix_attraction.covers.first.image.url : attraction.image_url(:banner)
#       end
#     end
#   end
# end
if @activity.present?
    json.taggable_id @activity.id
    json.taggable_type "ACT" # @activity.class.name
    json.title @activity.title
    json.price @activity.min_price
    json.country @activity.country
    json.description @activity.description
    json.highlight @activity.highlight
    json.terms_and_conditions @activity.terms_and_conditions
    json.operation_days @activity.join_operation_days
    json.categories @activity.join_categories
    json.date_start @activity.travelling_date_start.try(:strftime, "%F")
    json.date_end @activity.travelling_date_end.try(:strftime, "%F")
    json.min_booking @activity.min_booking.present? && @activity.min_booking > 0 ? @activity.min_booking : 1
    json.max_booking @activity.max_booking
    json.advance_booking_days @activity.advance_booking_days
    json.images @activity.covers.each do |cover|
       json.url cover.image.url
    end
    json.tickets @activity.activity_prices.each do |ticket|
      json.id ticket.id
      json.name ticket.category
      json.price ticket.final_price
      json.dta ticket.dta
      json.description ticket.description
      json.terms_and_conditions ticket.terms_and_conditions
      json.blockout_dates ticket.blockout_dates.present? ? ticket.blockout_dates.map(&:to_a).flatten.uniq : ''
    end
    json.languages @activity.join_languages
    json.duration @activity.duration.to_s.humanize
    json.cancellation @activity.cancellation.to_s.humanize
    json.e_ticketing @activity.e_ticketing.to_s.humanize
    json.travel_date_type @activity.travel_date_type.to_s.humanize
    json.ticket_type @activity.ticket_type.to_s.humanize
    json.transfer @activity.transfer.to_s.humanize
    json.meeting_point @activity.meeting_point.to_s.humanize
    json.confirmation_time @activity.confirmation_time.to_s.humanize
elsif @globaltix_attraction.present? && @globaltix_tickets.present?
    json.taggable_id @globaltix_attraction.id
    json.taggable_type "GT" # @globaltix_tickets.first.class.name 
    json.title @globaltix_attraction.title
    if @globaltix_tickets.present?
      json.price @globaltix_attraction.min_price
    else
      json.price 0
    end
    json.min_booking 1
    json.max_booking 20
    json.country @globaltix_attraction.country
    json.description @globaltix_attraction.description
    json.terms_and_conditions @globaltix_attraction.term_and_condition
    json.operation_days @globaltix_attraction.hours_of_operation
    json.is_visit_date_compulsory @globaltix_tickets.map{|v|v.ticket.is_visit_date_compulsory?}.include?(true) || @globaltix_attraction.fixed_date?
    json.images @globaltix_attraction.covers.count > 0 ? @globaltix_attraction.covers.first.image.url : @attraction.image_url(:banner)
    json.tickets @globaltix_tickets.each do |ticket|
      json.id ticket.id
      json.name ticket.name
      json.advance_booking_days ticket.ticket.the_advance_booking_days
      json.variation ticket.variation
      json.price ticket.final_price
      json.dta ticket.dta
      json.description ticket.description
      json.terms_and_conditions ticket.terms_and_conditions
      json.questions ticket.ticket.questions
      json.blockout_dates ticket.blockout_dates.present? ? ticket.blockout_dates.map(&:to_a).flatten.uniq : ''
    end 
    json.languages @globaltix_attraction.join_languages
    json.duration @globaltix_attraction.duration.to_s.humanize
    json.cancellation @globaltix_attraction.cancellation.to_s.humanize
    json.e_ticketing @globaltix_attraction.e_ticketing.to_s.humanize
    json.travel_date_type @globaltix_attraction.travel_date_type.to_s.humanize
    json.ticket_type @globaltix_attraction.ticket_type.to_s.humanize
    json.transfer @globaltix_attraction.transfer.to_s.humanize
    json.meeting_point @globaltix_attraction.meeting_point.to_s.humanize
    json.confirmation_time @globaltix_attraction.confirmation_time.to_s.humanize
end

json.array! @activity_booking_groups.each do |group|
  if group.class.name == "ActivityBookingGroup"
    json.created_at group.activity_bookings.first.created_at.try(:strftime, "%d/%m %H:%M")
    json.order_no group.reference_id
    json.agent_name group.activity_bookings.first.name
    json.package_name group.product_name
    json.total_price number_to_currency(group.price, precision: 2, unit: "RM ")
    json.total_quantity group.total_quantity
    json.payment_status group.payment.present? ? group.payment.status.humanize : ''
    json.visit_date group.travel_date.present? ? group.travel_date.try(:strftime, "%d %b %Y") : ''
    json.country group.activity.country.present? ? group.activity.country.humanize : ''
    json.remark group.activity_bookings.first.remark.present? ? group.activity_bookings.first.remark.humanize : ''
    json.activity_bookings group.activity_bookings.each do |booking|
      json.code booking.code
      json.total_quantity booking.quantity
      json.total_price number_to_currency(booking.price, precision: 2, unit: "RM ")
      json.category booking.category
    end
    json.invoice_url group.payment.present? && group.payment.paid? ? invoice_activity_booking_group_path(group) : ''
  elsif group.class.name == "GlobaltixTicketBookingGroup"
    json.created_at group.globaltix_ticket_bookings.first.created_at.try(:strftime, "%d/%m %H:%M")
    json.order_no group.reference_id
    json.agent_name group.globaltix_ticket_bookings.first.name
    json.package_name group.product_name
    json.total_price number_to_currency(group.price, precision: 2, unit: 'RM ')
    json.total_quantity group.total_quantity
    json.payment_status group.payment.present? ? group.payment.status.humanize : ''
    json.visit_date group.travel_date
    json.country group.globaltix_ticket_bookings.first.globaltix_ticket.country.present? ? group.globaltix_ticket_bookings.first.globaltix_ticket.country.humanize : ''
    json.remark group.remark
    json.activity_bookings group.globaltix_ticket_bookings.each_with_index do |booking, index|
      json.code "-"
      json.guest_name booking.name
      json.mobile "-"
      json.email booking.email
      json.total_quantity booking.quantity
      json.total_price number_to_currency(booking.price, precision: 2, unit: 'RM ')
      json.category booking.globaltix_ticket.name
    end
    json.invoice_url group.payment.present? && group.payment.paid? ? invoice_globaltix_ticket_booking_group_path(group) : ''
  end
end
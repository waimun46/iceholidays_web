html = ""
@cancellation_policies.cancel_policies.cancel_policy.group_by{|x| x.room_index}.each_with_index do |cp_by_room, i|
  html += "Room ##{i+1} "
  cp_by_room[1].each do |cp|
    html += "- #{cp.room_type_name} "
    html += "From: #{cp.from_date.to_date.strftime("%d %b %Y, %I:%M %p")} "
    html += "To: #{cp.to_date.to_date.strftime("%d %b %Y, %I:%M %p")} "
    html += "Charge: "
    html += if cp.charge_type === "Percentage"
      if cp.pref_cancellation_charge
        "#{cp.pref_cancellation_charge}% of total cost"
      else
        "#{cp.cancellation_charge}% of total cost"
      end
    else
      if cp.pref_cancellation_charge
        number_to_currency(cp.pref_cancellation_charge, precision: 0, unit: cp.pref_currency + " ") 
      else
        number_to_currency(cp.cancellation_charge, precision: 0, unit: cp.currency + " ")
      end
    end
    html += "<br/>"
  end
end
html += "<br/>"

hotel_rooms =  {}   
supplement_html = "<b>Room Supplements: </b><br/>"
meal_inclusion_html = "<b>Room Meal Type & Inclusions: </b><br/>"
@selected_rooms.each do |key, room|
  room_rate = {}
  room_rate["room_fare"] = room["room_rate"]["room_fare"]
  room_rate["agent_mark_up"] = room["room_rate"]["agent_mark_up"]
  room_rate["currency"] = room["room_rate"]["currency"]
  room_rate["pref_currency"] = room["room_rate"]["pref_currency"]
  room_rate["room_tax"] = room["room_rate"]["room_tax"]
  room_rate["total_fare"] = room["room_rate"]["total_fare"]
  room_rate["pref_price"] = room["room_rate"]["pref_price"]

  supplements = {}
  if room["supplements"].present?
    room["supplements"].each do |i, supp|
      supp_info = {}
      supp_info["supp_id"] = supp["supp_id"]
      supp_info["supp_name"] = supp["supp_name"]
      supp_info["price"] = supp["price"]
      supp_info["supp_is_mandatory"] = supp["supp_is_mandatory"]
      supp_info["supp_charge_type"] = supp["supp_charge_type"]
      supp_info["currency_code"] = supp["currency_code"]

      supplements["#{key}"] = supp_info
      supplement_html += "Room ##{key.to_i+1} : #{supp["supp_name"]} - #{supp["currency_code"]} #{supp["price"]}"
      supplement_html += "<br/>"
    end
  end

  room_info = {}
  room_info["room_index"] = room[:room_index]
  room_info["room_type_name"] = room[:room_type_name]
  room_info["room_type_code"] = room[:room_type_code]
  room_info["rate_plan_code"] = room[:rate_plan_code]
  room_info["meal_type"] = room[:meal_type].try(:humanize)
  room_info["inclusion"] = room[:inclusion].try(:humanize)
  room_info["room_rate_attributes"] = room_rate
  room_info["supplements_attributes"] = supplements

  meal_inclusion_html += "<b>Room ##{key.to_i+1}</b><br/>"
  meal_inclusion_html += "Meal Type : #{room_info["meal_type"]}<br/>"
  meal_inclusion_html += "Inclusions : #{room_info["inclusion"]}<br/>"

  hotel_rooms["#{key}"] = room_info
end
supplement_html += "<br/>"
meal_inclusion_html += "<br/>"

json.hotel_rooms hotel_rooms
json.room_policy html
json.cancellation_policies_available @cancellation_policies_available
json.booking_confirmable @available_for_confirm_book
json.cancel_deadline @cancellation_policies.cancel_policies.last_cancellation_deadline.to_date.strftime("%d %b %Y, %I:%M %p")
json.default_policy @cancellation_policies.cancel_policies.default_policy
json.hotel_norms @cancellation_policies.hotel_norms.join("<br/>")
json.text_policy @cancellation_policies.cancel_policies.text_policy
json.auto_cancellation_text @cancellation_policies.cancel_policies.auto_cancellation_text
json.supplements supplement_html
json.meal_inclusion meal_inclusion_html
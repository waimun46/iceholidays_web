json.array! @bookings do |booking|
  json.id booking.id
  json.created_at booking.updated_at.try(:strftime, "%d/%m %H:%M")
  json.confirmation_no booking.confirmation_no
  json.hotel_name booking.hotel.hotel_name
  json.hotel_address booking.hotel.address
  json.check_in booking.check_in.strftime("%d %B %Y")
  json.check_out booking.check_out.strftime("%d %B %Y")
  json.room booking.rooms.map{|x| x['room_type_name']}.uniq.join(',')
  json.room_qty booking.no_of_rooms
  json.guest_name booking.guests.present? ? booking.guests.order(:id).first.full_name : ''
  json.total_price number_to_currency(booking.total_fare, precision: 2, unit: "#{booking.currency} ")
  json.total_quantity booking.guests.present? ? booking.guests.count : ''
  json.payment_status booking.payment.present? ? booking.payment.status.humanize : ''
  json.remark booking.remark
  json.address_info booking.address_info
  json.guests booking.guests.each do |guest|
    json.first_name guest.first_name
    json.last_name guest.last_name
    json.age guest.age
    json.guest_type guest.guest_type
  end
  json.invoice_url booking.payment.present? && booking.payment.paid? ? invoice_tbo_holidays_booking_path(booking) : ''
end

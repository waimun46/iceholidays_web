@api_hotel = HotelBooker[:tbo_holidays].hotel_details(hotel_code: @hotel_code).hotel_details.to_h
json.session_id @session_id
json.result_index @result_index
json.hotel_code @hotel_code
json.name @hotel.hotel_name
json.check_in @check_in.to_date.strftime("%b %d %Y")
json.check_out @check_out.to_date.strftime("%b %d %Y")
json.address @hotel.address.present? ? @hotel.address : @api_hotel[:address]
json.description @hotel.description.present? ? @hotel.description : @api_hotel[:description]
json.city_id @city_id
json.city_name @hotel.city_name
json.hotel_location @hotel.hotel_location
json.star_rating (@hotel.rating_int.present? && @hotel.rating_int > 0) ? @hotel.rating_int : (TBOHotel::RATING_MAPPING[@api_hotel[:hotel_rating]] || 0)
# Hotel facilities
facilities = @hotel.hotel_facilities.present? ? @hotel.hotel_facilities : @api_hotel[:hotel_facilities]
json.facilities facilities.each do |facility|
  json.facility facility
end

# Hotel attractions
json.attraction @hotel.attractions.present? ? @hotel.attractions.join('<br>') : @api_hotel[:attractions].join('<br>')
# @hotel.attractions.each do |attraction|
#   json.attraction attraction
# end

# Get guest type and count
guest = []
adult_count = []
child_count = []
ages = []
@room_guests.each do |g|
  adult_count << g.adult_count.to_i
  child_count << g.child_count.to_i

  (1..g.adult_count.to_i).each do |a|
    guest << "Adult"
    ages << ""
  end
  (1..g.child_count.to_i).each_with_index do |c, i|
    guest << "Child"
    ages << g.child_age[i]
  end
end
json.room_guests @room_guests
json.guest_count guest
json.ages ages
json.adult_count adult_count.sum
json.child_count child_count.sum

# Hotel images url
json.images @hotel_images.each do |image|
  json.image_url image
end

# Available room
json.rooms @booking_options.room_combinations.each do |room_combination|
  image_url = []    
  room_descriptions = []    
  meal_type = []   
  no_of_rooms = room_combination.room_index.count
  nights, currency, inclusion = nil     
  total_cost = 0
  selected_rooms = []
  supplements = 
  room_combination.room_index.each do |idx|  
    room = @hotel_rooms[idx.to_i - 1]          
    selected_rooms << room
    supplements
    # use_pref ||= room.room_rate.pref_price.to_f > 0
    # currency ||= use_pref ? room.room_rate.pref_currency  : room.room_rate.currency
    currency ||= room.room_rate.currency
    nights ||= room.room_rate.day_rates.count
    # meal_type ||= room.meal_type.try(:humanize)
    # inclusion ||= room.inclusion.present? ? "Includes #{room.inclusion.try(:humanize).downcase}" : ""
    total_cost +=  room.room_rate.total_fare.to_f
    # total_cost +=  use_pref ? room.room_rate.pref_price.to_f : room.room_rate.total_fare.to_f
    room_description = "#{room.room_type_name}"
    room_descriptions << room_description unless room_description.in?(room_descriptions)
    meal_type << room.meal_type.try(:humanize)
    image_url << room.room_additional_info.image_urls
  end

  json.selected_rooms selected_rooms
  json.promotion selected_rooms.map{|v| v.room_promtion }.uniq.join(', ')
  json.room_index room_combination.room_index
  json.room_type "#{room_descriptions.join(' | ')}"
  json.price TBOHotel.rate_with_markup(total_cost, @hotel_markup, currency).to_f
  json.dta (TBOHotel.rate_with_markup(total_cost, @hotel_markup, currency) * (Setting.hotel_dta_markup.to_i / 100.to_f)).round
  json.price_desc "For #{no_of_rooms} room(s), #{nights} night(s)"
  json.total_cost total_cost
  json.currency currency
  json.image_url image_url.flatten.present? ? image_url.flatten.first : ""

  if meal_type.all? { |word| word.downcase == "breakfast" }
    meal_text = "Breakfast"
  elsif meal_type.any? { |word| word.include?("Breakfast") || word.include?("breakfast") }
    meal_text = "Partial Breakfast"
  else
    meal_text = "Room Only"
  end
  json.meal_type meal_text
end

json.room_booking_count @booking_options.room_combinations.first.room_index.count
json.guest_in_room @guest_in_room
json.array! @hotels.each do |hotel|
  tbo_hotel = TBOHotel.find_by_hotel_code(hotel.hotel_info.hotel_code)
  json.id hotel.result_index
  json.image_url tbo_hotel.present? && tbo_hotel.covers.present? ? tbo_hotel.covers.first.image.url : hotel.hotel_info.hotel_picture
  json.name raw(hotel.hotel_info.hotel_name)
  json.code hotel.hotel_info.hotel_code
  json.star_rating hotel.hotel_info.rating_int
  json.destination hotel.hotel_info.hotel_address
  # json.adults hotel.adults
  # json.childs hotel.childs
  json.price (TBOHotel.rate_with_markup(hotel.min_hotel_price.total_price.to_f, @hotel_markups[hotel.hotel_info.hotel_code].present? ? @hotel_markups[hotel.hotel_info.hotel_code] : @general_markup, hotel.min_hotel_price.currency) * (1 + Setting.hotel_dta_markup.to_i / 100.to_f)).round
  # json.price number_to_currency((TBOHotel.rate_with_markup(hotel.min_hotel_price.total_price.to_f, @hotel_markups[hotel.hotel_info.hotel_code].present? ? @hotel_markups[hotel.hotel_info.hotel_code] : @general_markup, hotel.min_hotel_price.currency) * (1 + Setting.hotel_dta_markup / 100.to_f)).round, precision: 0, unit: "RM ")
  json.session_id @booking_params[:session_id]
  json.guest_in_room @guest_in_room
end
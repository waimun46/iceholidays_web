json.array! @land_tour_booking_groups do |group|
  json.id group.id
  json.departure_date group.departure_date.try(:strftime, "%d/%m/%Y")
  json.created_at group.created_at.try(:strftime, "%d/%m %H:%M")
  json.order_no group.code
  json.agent_name group.land_tour_bookings.order(:id).first.name
  json.code group.land_tour.code
  json.rooms group.rooms_text
  json.total_price number_to_currency(group.total_price, precision: 2, unit: "RM ")
  json.total_quantity group.land_tour_bookings.count
  json.destination group.land_tour.country
  json.language group.language
  json.special_request group.join_special_request
  json.payment_status group.payment_status.titleize
  json.paid_amount number_to_currency(group.paid_amount, precision: 2, unit: "RM ")
  json.hotel_category group.hotel_name.present? ? group.hotel_name : group.land_tour_category.name
  json.bookings group.land_tour_bookings.visible.each do |booking|
    json.name "#{booking.designation} #{booking.name}"
    json.mobile booking.mobile
    json.passport_number booking.passport_number
    json.nationality booking.nationality
    # json.passport_expiry_date booking.passport_expiry_date.try(:strftime, "%d/%m/%Y")
    json.date_of_birth booking.date_of_birth.try(:strftime, "%d/%m/%Y")
    json.category booking.category.humanize.sub('_', '')
    json.total_price booking.free_of_charge? ? "#{number_to_currency(booking.price, precision: 2, unit: "RM ")} (FOC)" : number_to_currency(booking.price, precision: 2, unit: "RM ")
    json.remark booking.remark
  end
  # json.invoice_url group.payments.first.present? && group.payments.first.paid? ? invoice_land_tour_booking_group_path(group) : ''
  if group.has_payment?
    json.invoices group.payments.with_invoice.each do |payment|
      if payment.present? && payment.etravel_invoice.present?
        json.url invoice_land_tour_booking_group_path(id: group, invoice_id: payment.etravel_invoice)
      end
    end
  end
  if group.departure_date <= Date.today + 7.days
    json.voucher_url voucher_land_tour_booking_group_path(group)
  end
end
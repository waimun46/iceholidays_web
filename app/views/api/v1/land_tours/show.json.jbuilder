json.id @land_tour.id
json.code @land_tour.code
json.title @land_tour.title
json.cn_title @land_tour.cn_title
json.highlight @land_tour.highlight
json.category @land_tour.category.humanize
json.description @land_tour.description
json.cn_description @land_tour.cn_description
json.terms_and_conditions @land_tour.terms_and_conditions
json.guide_languages @land_tour.join_guide_languages.gsub(", ", " / ")
json.compulsory_charges @land_tour.compulsory_charges
json.child_prices @land_tour.child_prices
json.dta_prices @land_tour.dta_prices
json.inc_hotel @land_tour.inc_hotel
json.inc_flight @land_tour.inc_flight
json.inc_half_board_meals @land_tour.inc_half_board_meals
json.inc_full_board_meals @land_tour.inc_full_board_meals
json.inc_entrance_ticket @land_tour.inc_entrance_ticket
json.inc_transportation @land_tour.inc_transportation
json.inc_shopping_stop @land_tour.inc_shopping_stop
json.languages @land_tour.guide_languages
json.duration @land_tour.land_tour_days.map(&:day_no).max
qty_map = @land_tour.land_tour_prices.map(&:prices).first.keys.map{|q| (q.split('..')[0]..q.split('..')[1]).inject([]){|j, qty| j << qty}}.flatten.map{|q| q.to_i}
json.max_booking @land_tour.max_booking.present? ? @land_tour.max_booking : qty_map.max
json.min_booking @land_tour.min_booking.present? ? @land_tour.min_booking : qty_map.min
if @land_tour.min_booking.present? && @land_tour.max_booking.present?
  max_quantity = (@land_tour.min_booking..@land_tour.max_booking).inject([]){|j, qty| j << qty}
elsif !@land_tour.min_booking.present? && @land_tour.max_booking.present?
  max_quantity = (qty_map.min..@land_tour.max_booking).inject([]){|j, qty| j << qty}
elsif @land_tour.min_booking.present? && !@land_tour.max_booking.present?
  max_quantity = (@land_tour.min_booking..qty_map.max).inject([]){|j, qty| j << qty}
else
  max_quantity = qty_map
end
json.max_quantity max_quantity
json.cut_off_day @land_tour.cut_off_day
json.free_of_charge @land_tour.free_of_charge.present? ? @land_tour.free_of_charge : 0
# json.free_of_charge (@land_tour.free_of_charge.present? && @land_tour.free_of_charge > 0) ? @land_tour.free_of_charge.ordinalize : 0
json.deposit_type @land_tour.deposit_type.humanize
json.deposit @land_tour.deposit
json.deposit_text @land_tour.deposit_type == "fixed" ? "RM#{@land_tour.deposit} per pax" : "#{@land_tour.deposit}%"
json.single_supplement_price @land_tour.single_supplement_price
json.travel_date "#{@land_tour.travel_date_start.try(:strftime, '%d/%m/%Y')} - #{@land_tour.travel_date_end.try(:strftime, '%d/%m/%Y')}"
json.default_depart_date "#{@interval.begin.try(:strftime, '%Y-%m-%d')}"
json.images @land_tour.covers.map.with_index{|cover,index|cover.image.url}
json.country @land_tour.country
json.itinerary @land_tour_days.each do |day|
  json.day_no "Day #{day.day_no}"
  json.title day.title
  json.inclusion day.inclusion_text
  json.description day.description
  json.cities day.land_tour_cities.each do |city|
    json.city city.city_name
  end
  json.activities day.land_tour_activities.each do |activity|
    json.activity activity.taggable.present? ? activity.taggable.title : ''
  end
end
json.pricing_qty @land_tour.land_tour_prices.map(&:prices).first.keys.each do |qty|
  # reformat qty : 0-5 & 5-5
  qty_filter = (qty.split('..')[0]..qty.split('..')[1]).inject([]){|j, qty| j << qty}.map{|q|q.to_i}
  json.quantity (qty_filter.include?(0) || qty_filter.count == 1) ? qty_filter.max.to_s : qty
end
category_sort = @land_tour.land_tour_prices.sort_by{|obj| obj.prices.first.last.to_i }.map{|pr| pr.land_tour_category}.uniq
json.pricing_categories category_sort.each do |category|
  json.id category.id
  json.category category.name
  departure_dates = []
  land_tour_prices_sort = category.land_tour_prices.sort_by{|obj| obj.prices.first}

  json.price_dates land_tour_prices_sort.each_with_index do |land_tour_price, i| 
    json.dates land_tour_price.dates.each do |date| 
      if date.overlaps?(@interval)
        cut_off_date = date.begin - @land_tour.cut_off_day.days
        json.cut_off_date cut_off_date
        json.full_date "#{date.begin.try(:strftime, '%d %b %Y')} - #{date.max.try(:strftime, '%d %b %Y')}"
        departure_dates << date.inject([]){|k, date| k << date}
        json.date date.inject([]){|k, date| k << date}.flatten.map { |e| e.try(:strftime, '%Y-%m-%d') }.uniq

        json.quantities land_tour_price.prices.each do |qty, price|
          qty_str = (qty.split('..')[0]..qty.split('..')[1]).inject([]){|j, qty| j << qty}.map{|q|q.to_i}
          json.quantity qty_str
          json.quantity_str (qty_str.include?(0) || qty_str.count == 1) ? qty_str.max.to_s : qty
          json.adult price
          json.dta_adult category.dta(price.to_i, "adult")
          json.child_prices @land_tour.child_prices.each do |type, value|
            json.type type
            if type == "infant"
               json.price value
            else
              json.price ((value.to_i/100.0) * price.to_i).round(2)
            end
          end
        end
      end
    end
  end
  json.departure_dates departure_dates.flatten.map { |e| e.try(:strftime, '%Y-%m-%d') }.uniq
end
pricing = []
@land_tour.land_tour_prices.each_with_index do |land_tour_price, i|
  land_tour_price.dates.each do |date|
    if date.overlaps?(@interval)
      pricing << land_tour_price.prices
    end
  end
end
json.pricing_list pricing.uniq
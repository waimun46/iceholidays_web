json.id @land_tour_booking_group.id
json.created_at @land_tour_booking_group.created_at.try(:strftime, "%d/%m/%Y %H:%M")
json.order_no @land_tour_booking_group.reference_id
json.agent_name @land_tour_booking_group.land_tour_bookings.order(:id).first.name
json.code @land_tour_booking_group.land_tour.code
json.total_price number_to_currency(@land_tour_booking_group.total_price, precision: 2, unit: "RM ")
json.total_quantity @land_tour_booking_group.land_tour_bookings.count
json.destination @land_tour_booking_group.land_tour.country
json.deposit number_to_currency(@land_tour_booking_group.deposit, precision: 2, unit: "RM ")
json.balance_amount number_to_currency(@land_tour_booking_group.balance_amount, precision: 2, unit: "RM ")
json.fare_type @land_tour_booking_group.has_payment? ? 'balance' : 'deposit'
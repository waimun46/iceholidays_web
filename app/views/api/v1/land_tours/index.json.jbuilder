json.land_tours @land_tours.uniq.each do |land_tour|
  if land_tour.present? && Time.now < land_tour.travel_date_end - land_tour.cut_off_day.days
    json.image_url land_tour.covers.present? ? land_tour.covers.first.image.url : ''
    json.id land_tour.id
    json.code land_tour.code
    json.title land_tour.title.present? ? land_tour.title : 'Untitled'
    json.cn_title land_tour.cn_title.present? ? land_tour.cn_title : 'Untitled'
    json.highlight land_tour.highlight
    json.description land_tour.description
    json.cn_description land_tour.cn_description
    json.price land_tour.min_price
    json.country land_tour.country
    json.travel_date "#{land_tour.travel_date_start.try(:strftime, '%d/%m/%Y')} - #{land_tour.travel_date_end.try(:strftime, '%d/%m/%Y')}"
    json.min_booking land_tour.min_booking
    json.category land_tour.category.try(:titleize)
    json.depart_day land_tour.depart_day.present? ? land_tour.depart_day.count == 7 ? "Daily Departure" : "Depart every #{land_tour.join_depart_day}" : ""
    json.duration land_tour.land_tour_days.map(&:day_no).max
  end
end

json.max_duration @land_tours.uniq.map{|lt| lt.land_tour_days.map(&:day_no)}.flatten.max
json.categories @land_tours.uniq.map{|lt| lt.category.try(:titleize)}.flatten.uniq
if @solution.present?
  json.title render_flight_title(params[:type], @solution)
  json.airports @solution.journeys.map(&:airports).join(', ')
  json.dates render_flight_dates(@solution)
  json.passenger render_flight_passengers(@solution)
  json.cabin_class if params[:cabin_class].present? ? params[:cabin_class] : ''
  json.journeys @solution.journeys.each do |journey|  
    json.stop journey.stop
    json.travel_time render_travel_time(journey.arrival_segment.arrival_time.to_time - journey.departure_segment.departure_time.to_time)
    json.airline_image journey.departure_segment.airline_image_url
    json.airline_name journey.departure_segment.airline_name
    json.departure_time  journey.departure_segment.departure_time.strftime("%R")
    json.departure_date  journey.departure_segment.departure_time.strftime("%b %d %Y")
    json.departure_airport do
      json.city journey.departure_segment.origin_airport.city
      json.iata journey.departure_segment.origin_airport.iata
      json.name journey.departure_segment.origin_airport.name
    end
    json.arrival_time  journey.arrival_segment.arrival_time.strftime("%R")
    json.arrival_date  journey.arrival_segment.arrival_time.strftime("%b %d %Y")
    json.arrival_airport do
      json.city journey.arrival_segment.destination_airport.city
      json.iata journey.arrival_segment.destination_airport.iata
      json.name journey.arrival_segment.destination_airport.name
    end

    json.segments journey.air_segments.each do |air_segment|
      json.airline_image air_segment.airline_image_url
      json.airline_name air_segment.airline_name
      json.departure_time air_segment.departure_time.strftime("%R")
      json.departure_date air_segment.departure_time.strftime("%b %d %Y")
      json.arrival_time air_segment.arrival_time.strftime("%R")
      json.arrival_date air_segment.arrival_time.strftime("%b %d %Y")
      json.flight_time "#{air_segment.flight_time.to_i / 60}H #{air_segment.flight_time.to_i % 60}M"
      json.carrier air_segment.carrier
      json.flight_number "#{air_segment.carrier}#{air_segment.flight_number}"
      aircraft = Aircraft.aircraft_name(air_segment.equipment)
      json.aircraft aircraft.present? ? aircraft : "#{air_segment.equipment}"
      json.flight_number air_segment.flight_number
      json.cabin_class air_segment.booking_info.cabin_class
      json.baggage_max_weight air_segment.baggage_allowance
      json.transit_time air_segment.next_air_segment.present? ? render_travel_time(air_segment.transit_time) : ""
      json.departure_airport do
        json.city air_segment.origin_airport.city
        json.iata air_segment.origin_airport.iata
        json.name air_segment.origin_airport.name
      end
      json.arrival_airport do
        json.city air_segment.destination_airport.city
        json.iata air_segment.destination_airport.iata
        json.name air_segment.destination_airport.name
      end
    end

  end
  json.total_price @solution.final_total_price
  json.pricings @solution.air_pricing_infos do |air_pricing_info|
    json.code air_pricing_info.passenger_code
    json.count air_pricing_info.passenger_count
    json.total_price air_pricing_info.final_total_price
    json.base air_pricing_info.final_base_price * air_pricing_info.passenger_count
    json.taxes air_pricing_info.final_taxes * air_pricing_info.passenger_count
  end
end
json.array! @flight_booking_groups do |group|
  json.booked_at group.created_at.try(:strftime, "%d/%m %H:%M")
  json.updated_at group.updated_at.try(:strftime, "%d/%m %H:%M")
  json.created_at group.created_at.try(:strftime, "%d/%m %H:%M")
  json.code group.host_code
  json.name group.flight_bookings.present? ? group.flight_bookings.order(:id).first.name : ''
  json.price number_to_currency(group.price, precision: 2, unit: 'RM')
  json.pax group.flight_bookings.count
  json.segment group.flight_booking_details.count
  json.orgin_destination group.flight_detail_places
  json.payment_gateway group.payment.present? ? group.payment.gateway_text : ''
  json.payment_status group.payment.present? ? group.payment.status.humanize : ''
  json.remark group.flight_bookings.map(&:remark).compact.join("|")
  json.flight_details group.flight_booking_details.each do |flight|
    json.carrier "#{flight.carrier} #{flight.flight_number}"
    json.cabin_class flight.cabin_class
    json.origin_terminal "#{flight.origin} / #{flight.origin_terminal}"
    json.destination_terminal "#{flight.destination} / #{flight.destination_terminal}"
    json.departure_time flight.departure_time.strftime("%F %R")
    json.arrival_time flight.arrival_time.strftime("%F %R")
    json.travel_time flight.travel_time
    json.sell_message flight.sell_message
  end
  json.flight_bookings group.flight_bookings.each do |booking|
    json.designation booking.designation
    json.first_name booking.first_name
    json.last_name booking.last_name
    json.country booking.country
    json.mobile booking.mobile
    json.passport_number booking.passport_number
    json.passport_issue_date booking.passport_issue_date.try(:strftime, "%d/%m/%Y")
    json.passport_expiry_date booking.passport_expiry_date.try(:strftime, "%d/%m/%Y")
    json.date_of_birth booking.date_of_birth.try(:strftime, "%d/%m/%Y")
    json.category booking.category
    json.status booking.status
    json.remark booking.remark
    json.brb render_brb_display(booking)
  end
  json.invoice_url group.payment.present? && group.payment.paid? ? invoice_flight_booking_group_path(group) : ''
end

json.itineraries @itineraries do |itinerary|
    # json.render itinerary
    # json.tour_id itinerary.tours.first.id
    # json.tour_ids itinerary.tours.map{|v|v.id}
    json.id itinerary.id
    json.code itinerary.code
    json.description itinerary.description.present? ? itinerary.description.gsub(/\\\\/,'\\').gsub(/\\r?\\n|\\n?\\r|\\r|\\n|\r?\n/, '<br />') : ""
    json.file_url itinerary.file.url
    json.category itinerary.tours.first.category.present? ? itinerary.tours.first.category_brand == "FS" ? "Four Season" : itinerary.tours.first.category_brand : ""
    json.caption itinerary.tours.first.caption
    json.other_caption itinerary.tours.first.other_caption
    json.country itinerary.tours.first.contient_country
    json.price itinerary.tours.map{|v|v.min_price(current_user)}.min
    json.departure_date itinerary.tours.map{|v|v.departure_date.try(:strftime, "%d/%m/%Y")}.uniq
    json.images itinerary.covers.map.with_index{|cover,index|cover.image.url}
end
# if @itineraries.count > 1
#   json.page do
#     json.total_pages @itineraries.total_pages
#     json.current_page params[:page].present? ? params[:page].to_i : 1
#   end
# end
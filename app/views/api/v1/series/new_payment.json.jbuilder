json.id @booking_group.id
json.created_at @booking_group.created_at.try(:strftime, "%d/%m/%Y %H:%M")
json.order_no @booking_group.reference_id
json.agent_name @booking_group.bookings.order(:id).first.name
json.code @booking_group.tour.code
json.total_price number_to_currency(@booking_group.price, precision: 2, unit: "RM ")
json.total_quantity @booking_group.bookings.count
json.destination @booking_group.tour.contient_country
json.deposit number_to_currency(@booking_group.deposit, precision: 2, unit: "RM ")
json.balance_amount number_to_currency(@booking_group.balance_amount, precision: 2, unit: "RM ")
json.fare_type @booking_group.has_payment? ? 'balance' : 'deposit'
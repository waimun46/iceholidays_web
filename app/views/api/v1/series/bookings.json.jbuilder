json.array! @tour_booking_groups do |group|
  json.id group.id
  json.created_at group.created_at.try(:strftime, "%d/%m %H:%M")
  json.order_no group.reference_id
  json.agent_name group.bookings.order(:id).first.name
  json.code group.tour.code
  json.total_price number_to_currency(group.price, precision: 2, unit: "RM ")
  json.total_quantity group.bookings.count
  json.destination group.tour.contient_country
  json.payment_status group.payment_status.titleize
  json.paid_amount number_to_currency(group.paid_amount, precision: 2, unit: "RM ")
  json.insurance_receiver group.insurance_receiver
  json.insurance_confirmation !group.insurance_exported? && !group.tour.cut_off? && group.tour.insurance_available? && group.tour.guaranteed_departure_flag && group.confirm_export?
  json.editable group.editable?
  json.booked_from group.booked_from
  json.bookings group.bookings.each do |booking|
    json.id booking.id
    json.designation booking.designation
    json.name booking.name
    json.price_type booking.price_type.present? ? booking.price_type : ''
    json.mobile booking.mobile
    json.passport_number booking.passport_number
    json.passport_expiry_date booking.passport_expiry_date.present? ? booking.passport_expiry_date : ''
    json.passport_photocopy booking.passport_photocopy.present? ? booking.passport_photocopy : ''
    json.passport_photocopy_image booking.passport_photocopy.url.present? ? booking.passport_photocopy.url : ''
    json.room_type booking.room_type
    json.date_of_birth booking.date_of_birth.present? ? booking.date_of_birth.try(:strftime, "%Y-%m-%d") : ''
    json.category booking.category.humanize.sub('_', '')
    json.total_price number_to_currency(booking.price, precision: 2, unit: "RM ")
    json.remark booking.remark
    json.nationality booking.nationality.present? ? booking.nationality : ''
    json.insurance_confirmation booking.insurance_confirmation
    json.insurance_nomination_flag booking.insurance_nomination_flag
    if booking.insurance_nomination.blank?
      booking.construct_insurance_nomination
    end
    json.insurance_nomination booking.insurance_nomination.each do |insurance|
      json.nominee_surname insurance["nominee_surname"].present? ? insurance["nominee_surname"] : ''
      json.nominee_given_name insurance["nominee_given_name"].present? ? insurance["nominee_given_name"] : ''
      json.nominee_passport_number insurance["nominee_passport_number"].present? ? insurance["nominee_passport_number"] : ''
      json.nominee_date_of_birth insurance["nominee_date_of_birth"].present? ? insurance["nominee_date_of_birth"] : ''
      json.nominee_relationship insurance["nominee_relationship"].present? ? insurance["nominee_relationship"] : ''
      json.nominee_share insurance["nominee_share"].present? ? insurance["nominee_share"] : ''
    end
    # json.insurance_nomination 0..2.each do |index|
    #   json.nominee_surname booking.insurance_nomination[index].insurance["nominee_surname"].present? ? insurance["nominee_surname"] : ''
    #   json.nominee_given_name insurance["nominee_given_name"].present? ? insurance["nominee_given_name"] : ''
    #   json.nominee_passport_number insurance["nominee_passport_number"].present? ? insurance["nominee_passport_number"] : ''
    #   json.nominee_date_of_birth insurance["nominee_date_of_birth"].present? ? insurance["nominee_date_of_birth"] : ''
    #   json.nominee_relationship insurance["nominee_relationship"].present? ? insurance["nominee_relationship"] : ''
    #   json.nominee_share insurance["nominee_share"].present? ? insurance["nominee_share"] : ''
    # end
    json.witness_surname booking.witness_surname
    json.witness_given_name booking.witness_given_name
    json.witness_passport_number booking.witness_passport_number
    # json.invoice_url group.payments.first.present? && group.payments.first.paid? ? invoice_booking_group_path(group) : ''
  end
  # json.invoice_url group.payments.first.present? && group.has_payment? ? invoice_booking_group_path(group) : ''
  if group.has_payment?
    json.invoices group.payments.with_invoice.order(id: 'ASC').each do |payment|
      if payment.present? && payment.etravel_invoice.present?
        json.url invoice_booking_group_path(id: group, invoice_id: payment.etravel_invoice)
      end
    end
  end
end
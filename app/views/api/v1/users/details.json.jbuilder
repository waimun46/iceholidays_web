json.username @user.username
json.credits @user.credits
json.collect_dta @user.collect_dta?
json.access_flight (@user.boss? || @user.admin? || @user.access_flight?)
json.access_wifi (@user.boss? || @user.admin? || @user.access_wifi?)
json.access_series (@user.boss? || @user.admin? || @user.access_series?)
json.access_activity (@user.boss? || @user.admin? || @user.access_activity?)
json.access_hotel (@user.boss? || @user.admin?  || @user.access_hotel?)
json.access_busferry (@user.boss? || @user.admin?  || @user.access_busferry?)
json.busferry_link @user.easybook_access_link
json.access_land_tour (@user.boss? || @user.admin? || @user.access_land_tour?)
json.access_credit_purchase (@user.boss? || @user.admin?)
json.array! @credit_usage_transactions do |credit|
  json.created_at credit.created_at.try(:strftime, "%d/%m %H:%M")
  json.references "#{credit.reference_category} : #{credit.get_reference_id}"
  json.purchase_item 
  json.amount credit.amount
  json.remaining_balance credit.total_balance
end

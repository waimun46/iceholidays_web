json.id @payment.id
json.booking_type @payment.taggable_type
json.booking_id @payment.taggable_id
json.result @payment.status
json.reason @payment.action_remark.present? ? @payment.action_remark : ''
json.order_id @payment.order_id
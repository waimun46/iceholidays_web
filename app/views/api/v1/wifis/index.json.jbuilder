json.array! @packages.each do |package|
  json.image_url package.covers.present? ? package.covers.first.image.url : ''
  json.id package.id
  json.name package.name
  json.price package.retail_price.ceil
  json.country package.join_countries
  json.category package.category.humanize
  json.data_rules package.data_rules
end

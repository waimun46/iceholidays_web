json.id @package.id
json.name @package.name
json.price @package.retail_price.ceil
json.channel_price @package.channel_price_markup(current_user).ceil
# json.price @package.retail_price
# json.channel_price @package.channel_price_markup(current_user)
json.dta @package.dta(current_user)
json.country @package.join_countries
json.category @package.category.humanize
json.data_rules @package.data_rules
json.image_url @package.covers.present? ? @package.covers.first.image.url : ''
json.highlight @package.highlight
json.tnc @package.terms_and_conditions
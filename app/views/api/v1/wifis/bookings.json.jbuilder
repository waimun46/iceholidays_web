json.array! @roamingman_booking_groups do |group|
  json.id group.id
  json.created_at group.updated_at.try(:strftime, "%d/%m %H:%M")
  json.order_no group.reference_id
  json.guest_name group.roamingman_bookings.present? ? group.roamingman_bookings.order(:id).first.name : ''
  json.code group.roamingman_bookings.present? ? group.roamingman_bookings.order(:id).first.roamingman_package.code : ''
  json.package_name group.product_name
  json.pickup_point group.deliver_point
  json.total_price number_to_currency(group.price, precision: 2, unit: 'RM')
  json.total_quantity group.roamingman_bookings.present? ? group.total_quantity : ''
  json.payment_status group.payment.present? ? group.payment.status.humanize : ''
  json.bookings group.roamingman_bookings.each do |booking|
    json.name booking.name
    json.mobile booking.mobile
    json.email booking.email
    json.depart_date booking.depart_date.try(:strftime, "%d/%m/%Y")
    json.return_date booking.return_date.try(:strftime, "%d/%m/%Y")
    json.quantity booking.quantity
    json.remark booking.remark
  end
  json.invoice_url group.payment.present? && group.payment.paid? ? invoice_roamingman_booking_group_path(group) : ''
end

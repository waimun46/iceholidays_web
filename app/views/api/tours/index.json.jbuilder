json.array! @tours do |tour|
  json.departure tour.departure_date.try(:strftime, "%d/%m/%Y")
  json.code tour.code
  json.category_brand tour.category_brand
  json.caption tour.caption
  json.other_caption tour.other_caption
  json.price tour.cheapest_normal_price
  json.status tour.status_text
  if tour.itinerary.present? && tour.itinerary.file.present?
    json.itinerary tour.itinerary.file.url
  else
    json.itinerary ""
  end
  if tour.itinerary.present? && tour.itinerary.cn_file.present?
    json.itinerary_cn tour.itinerary.cn_file.url
  else
    json.itinerary_cn ""
  end
  json.flights tour.tour_flights.order(:departure) do |tour_flight|
    json.departure tour_flight.departure.try(:strftime, "%d/%m/%Y %R %a")
    json.arrival tour_flight.arrival.try(:strftime, "%d/%m/%Y %R %a")
    json.from_to tour_flight.from_to
    json.flight_no tour_flight.flight_no
    json.airline tour_flight.airline.present? ? tour_flight.airline.name : 'Not found'
    json.airline_logo tour_flight.airline.present? ? tour_flight.airline.logo.url.present? ? tour_flight.airline.logo.url : '' : ''
  end
end
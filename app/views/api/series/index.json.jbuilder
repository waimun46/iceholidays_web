json.itineraries @itineraries do |itinerary|
    json.id itinerary.id
    json.code itinerary.code
    json.description itinerary.description.present? ? itinerary.description.gsub(/\\\\/,'\\').gsub(/\\r?\\n|\\n?\\r|\\r|\\n|\r?\n/, '<br />') : ""
    json.file_url itinerary.file.url
    json.category itinerary.tours.first.category.present? ? itinerary.tours.first.category_brand == "FS" ? "Four Season" : itinerary.tours.first.category_brand : ""
    json.caption itinerary.tours.first.caption
    json.other_caption itinerary.tours.first.other_caption
    json.country itinerary.tours.first.contient_country
    json.price itinerary.tours.map{|v|v.min_price(User.new)}.min
    json.departure_date itinerary.tours.map{|v|v.departure_date.try(:strftime, "%d/%m/%Y")}.uniq
    json.images itinerary.covers.map.with_index{|cover,index|cover.image.url}
end
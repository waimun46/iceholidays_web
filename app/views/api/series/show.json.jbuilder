json.id @itinerary.id
json.code @itinerary.code
json.file_url @itinerary.file.url
json.category @itinerary.tours.first.category.present? ? @itinerary.tours.first.category.gsub("_", " ").tr("0-9", "").rstrip : ""
json.caption @itinerary.tours.first.caption
json.other_caption @itinerary.tours.first.other_caption
json.description @itinerary.description.present? ? @itinerary.description.gsub(/\\\\/,'\\').gsub(/\\r?\\n|\\n?\\r|\\r|\\n|\r?\n/, '<br />') : ""
json.remark @itinerary.remark.present? ? @itinerary.remark.gsub(/\\\\/,'\\').gsub(/\\r?\\n|\\n?\\r|\\r|\\n|\r?\n/, '<br />') : ""
json.country @itinerary.tours.first.contient_country
json.max_booking_seats @itinerary.tours.map(&:max_available_booking_seats).max
json.includings do 
  json.airport_taxes @itinerary.including_airport_taxes
  json.group_departure @itinerary.group_departure
  json.tour_leader @itinerary.including_tour_leader
  json.luggage @itinerary.including_luggage
  json.wifi @itinerary.including_wifi
  json.meal_onboard @itinerary.including_meal_onboard
  json.hotel @itinerary.including_hotel
  json.gratuities @itinerary.including_gratuities
  json.acf @itinerary.including_acf
end
json.tours @itinerary.tours.each do |tour|
  json.id tour.id
  json.code tour.code
  json.caption tour.caption
  json.departure_date tour.departure.try(:strftime, "%d/%m/%Y")
  json.arrival_date tour.arrival.try(:strftime, "%d/%m/%Y")
  json.cut_off_date tour.cut_off_date.try(:strftime, "%d/%m/%Y")
  json.insurance tour.insurance_available? ? 'Free Travel Insurance' : ''
  json.guaranteed_departure tour.guaranteed_departure?
  json.price tour.min_price(User.new)
  json.deposit tour.deposit
  json.insurance_rebate tour.insurance_rebate.present? ? tour.insurance_rebate : 0
  json.single_supplement_price tour.single_supplement_price
  json.visa_fee tour.visa_fee
  json.subtract_others tour.subtract_others
  json.compulsory_additional_fee tour.compulsory_additional_fee
  json.addon_others tour.addon_others
  json.max_booking_seats tour.max_available_booking_seats
  json.flights tour.tour_flights.order(:departure).each do |tour_flight|
    json.id tour_flight.id
    json.flight_no tour_flight.flight_no
    json.airline tour_flight.airline.present? ? tour_flight.airline.name : 'Not found'
    json.airline_logo tour_flight.airline.present? ? tour_flight.airline.logo.url.present? ? tour_flight.airline.logo.url : '' : ''
    json.from_airport tour_flight.from.present? ? "#{tour_flight.departure_airport.city} (#{tour_flight.departure_airport.iata})" : 'Not found'
    json.to_airport tour_flight.to.present? ? "#{tour_flight.arrival_airport.city} (#{tour_flight.arrival_airport.iata})" : 'Not found'
    json.departure_date tour_flight.departure.try(:strftime, "%d/%m/%Y")
    json.departure_time tour_flight.departure.try(:strftime, "%H:%M")
    json.arrival_date tour_flight.arrival.try(:strftime, "%d/%m/%Y")
    json.arrival_time tour_flight.arrival.try(:strftime, "%H:%M")
  end
  json.prices tour.available_vacant_seats(User.new).map{|v|v.last}.first do |type, seats_price|
    json.type type
    json.adult seats_price[:price]
    json.child_with_bed seats_price[:child_with_bed_price]
    json.child_no_bed seats_price[:child_no_bed_price]
    json.child_twin seats_price[:child_twin_price]
  end
end
json.images @itinerary.covers.map.with_index{|cover,index|cover.image.url}
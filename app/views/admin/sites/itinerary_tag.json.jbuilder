json.array! @itinerary_tags do |fair|
  json.id fair.id
  json.value "#{fair.code}"
end
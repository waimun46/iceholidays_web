json.array! @tour_tags do |tour|
  json.id tour.id
  json.name "#{tour.code}, #{tour.caption}"
end
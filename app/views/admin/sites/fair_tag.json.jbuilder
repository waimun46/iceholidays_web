json.array! @fair_tags do |fair|
  json.id fair.id
  json.name "#{fair.code}, #{fair.title}"
end
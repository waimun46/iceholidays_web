json.array! @user_tags do |user|
  json.id user.id
  json.value "#{user.username}"
end
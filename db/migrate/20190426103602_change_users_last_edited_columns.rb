class ChangeUsersLastEditedColumns < ActiveRecord::Migration[5.1]
  def self.up
    rename_column :users, :last_edited_user, :last_edited_user_id
    rename_column :users, :last_reset_password_edited, :last_reset_password_edited_user_id
  end
 
  def self.down
    rename_column :users, :last_edited_user_id, :last_edited_user
    rename_column :users, :last_reset_password_edited_user_id, :last_reset_password_edited
  end
end

class AddDescriptionAndTravellingDateToGlobaltix < ActiveRecord::Migration[5.2]
  def up
    add_column :globaltix_tickets, :description, :string, default: ''
    add_column :activity_prices, :description, :string, default: ''

    add_column :globaltix_attractions, :travelling_date, :tsrange

    if Rails.env.iceb2b?
      GlobaltixTicket.all.each do |globaltix_ticket|
        ticket, ticket_err = Globaltix::Ticket.find(globaltix_ticket.ticket_id)
        globaltix_ticket.update_columns(description: ticket.description) if ticket.present? && ticket.description.present?
      end
    end
  end

  def down
    remove_column :globaltix_tickets, :description, :string, default: ''
    remove_column :activity_prices, :description, :string, default: ''

    remove_column :globaltix_attractions, :travelling_date, :tsrange
  end
end

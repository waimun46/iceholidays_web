class AddBookingOperatorRemark < ActiveRecord::Migration[5.2]
  def change
    add_column :bookings, :operator_remark, :string, default: ""
  end
end

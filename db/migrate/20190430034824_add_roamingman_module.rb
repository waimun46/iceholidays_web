class AddRoamingmanModule < ActiveRecord::Migration[5.1]
  def up
  	create_table :roamingman_packages do |t|
      t.string :name
      t.string :code
      t.string :package_type
      t.integer :category
      t.string :countries, array: true
      t.integer :min_rent_days, default: 2
      t.string :data_rules
      t.decimal :channel_price
      t.decimal :retail_price
      t.timestamps
    end

    packages_seed = "#{Rails.root}/lib/records/roamingman_packages_seed.txt"
    lines = File.readlines(packages_seed)
    lines.each do |line|
      row = line.sub("\n", '').split('^')
      if Rails.env.iceb2b? && row[0].include?('Test Product')
        next
      else
        RoamingmanPackage.create(name: row[0], code: row[1], package_type: row[2], category: row[3], countries: row[4], data_rules: row[5], channel_price: row[6], retail_price: row[7])
      end
    end

    create_table :roamingman_booking_groups do |t|
      t.decimal :price
      t.integer :deposit_mode, default: 1
      t.integer :deposit_amount, default: 0
      t.string :remark
      t.integer :last_edited_user_id
      t.datetime :last_edited_at
      t.timestamps
    end

    create_table :roamingman_bookings do |t|
    	t.references :roamingman_booking_group
      t.string :oms_order_id
    	t.string :name
    	t.string :mobile
      t.string :email
    	t.string :address
    	t.string :postcode
    	t.string :country_code, default: "MYS"
    	t.date :depart_date
    	t.date :return_date
    	t.integer :ship_way
    	t.integer :quantity
    	t.string :deliver_point_id
    	t.string :return_point_id
      t.string :is_send_sms
      t.string :is_send_email
      t.integer :status, default: 0
      t.string :remark, default: ""
      t.integer :agent_user_id
      t.datetime :last_edited_at
      t.integer :last_edited_user_id
      t.integer :export_status, default: 0
      t.string :exported_remark
      t.integer :exporter_id
      t.datetime :exported_at
      t.integer :cancel_user_id
      t.datetime :cancelled_at
      t.jsonb :cancelled_details, default: {}
      t.references :roamingman_package
      t.timestamps
    end
  end

  def down
  	drop_table :roamingman_bookings
  	drop_table :roamingman_booking_groups
  	drop_table :roamingman_packages
  end
end

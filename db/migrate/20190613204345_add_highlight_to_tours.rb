class AddHighlightToTours < ActiveRecord::Migration[5.1]
  def change
    add_column :tours, :highlight, :string
  end
end

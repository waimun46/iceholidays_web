class AddColumnsToItinerary < ActiveRecord::Migration[5.2]
  def up
    add_column :itineraries, :category, :string
    add_column :itineraries, :department, :integer
    add_column :itineraries, :status, :integer, default: 0

    departments_seed = "#{Rails.root}/lib/records/itineraries_department_seed.txt"
    lines = File.readlines(departments_seed)
    lines.each do |line|
      row = line.sub("\n", '').split('^')
      itinerary = Itinerary.find_by_code(row[0])
      if itinerary.present?
        itinerary.update_columns(department: row[1].present? ? row[1] : 0, category: row[2].present? ? row[2] : "", status: row[3].present? ? row[3] : 0)
      else
        next
      end
    end

    add_column :fairs, :allow_itinerary_ids, :integer, array: true
  end

  def down
    remove_column :itineraries, :category, :string
    remove_column :itineraries, :department, :integer
    remove_column :itineraries, :status, :integer
    remove_column :fairs, :allow_itinerary_ids, :integer
  end
end

class CreateTourLeaders < ActiveRecord::Migration[5.2]
  def change
    create_table :tour_leaders do |t|
        t.string :first_name
        t.string :last_name
        t.datetime :date_of_birth
        t.string :phone_number
        t.string :emergency_number
        t.integer :gender
        t.string :photo
        t.datetime :passport_expiry_date
        t.text :passport_no
        t.string :passport_copy
        t.string :passport_country
        t.boolean :insurance_confirmation
        t.string :insurance_copy
        t.datetime :insurance_expiry_date
        t.string :nationality
        t.string :languages, default: [], array: true
        t.text :description
        t.string :address
        t.string :experience
        t.string :expertise
        t.string :visa_no
        t.string :designation
        t.boolean :active, default: false
        t.integer :status, default: 0
        t.string :room_type, default: ""
        t.timestamps
      end
        rename_column :tours, :tour_leader, :tour_leader_name
        add_column :tours, :tour_leader_id, :string
    end
  end
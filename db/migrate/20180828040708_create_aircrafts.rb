class CreateAircrafts < ActiveRecord::Migration[5.1]
  def change
    create_table :aircrafts do |t|
      t.string :iata_code
      t.string :manufacturer
      t.string :model
      t.string :iata_group
      t.string :iata_category
      t.string :icao_code
      t.string :nb_engines
      t.string :aircraft_type

      t.timestamps
    end
  end
end

class CreateTours < ActiveRecord::Migration[5.1]
  def change
    create_table :tours do |t|
      t.string :code
      t.string :caption
      t.string :other_caption
      t.string :contient_country
      t.jsonb :max_seats, null: false, default: {}
      t.jsonb :vacant_seats, null: false, default: {}
      t.jsonb :prices, null: false, default: {}
      t.datetime :early_bird_date
      # t.references :fair
      t.datetime :departure_date
      t.datetime :cut_off_date
      t.boolean :vacant_seats_available, default: false
      t.boolean :guaranteed_departure_flag, default: false
      t.integer :guaranteed_departure_seats
      t.integer :payment_seats, default: 0
      t.integer :max_booking_seats
      t.integer :deposit
      t.string :tour_leader
      t.string :remark
      t.string :internal_reference
      t.boolean :active, default: false
      t.datetime :first_activated_at
      # t.string :itinerary
      t.references :itinerary
      t.integer :created_user_id
      t.integer :last_edited_user_id
      t.datetime :last_edited_at

      t.timestamps
    end
    execute "SELECT setval('tours_id_seq', 50000)"
  end
end

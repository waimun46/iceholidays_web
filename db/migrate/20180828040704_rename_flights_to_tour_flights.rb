class RenameFlightsToTourFlights < ActiveRecord::Migration[5.1]
  def self.up
    rename_table :flights, :tour_flights
  end

  def self.down
    rename_table :tour_flights, :flights
  end
end
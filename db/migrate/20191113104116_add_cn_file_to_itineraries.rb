class AddCnFileToItineraries < ActiveRecord::Migration[5.1]
  def change
    add_column :itineraries, :cn_file, :string
  end
end

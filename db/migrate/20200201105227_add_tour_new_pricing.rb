class AddTourNewPricing < ActiveRecord::Migration[5.2]
  def change
    add_column :booking_cruise_rooms, :price_type, :string
    add_column :booking_cruise_rooms, :room_type, :string

    add_column :bookings, :in_fair, :boolean, default: false
    add_column :bookings, :original_price, :decimal

    # Booking.all.each do |booking|
    #   booking.update_columns(original_price: booking.price)
    # end

  end
end
class AddDepartDayToLandTour < ActiveRecord::Migration[5.2]
  def up
    add_column :land_tours, :depart_day, :string, array: true
  end

  def down
    remove_column :land_tours, :depart_day, :string, array: true
  end
end

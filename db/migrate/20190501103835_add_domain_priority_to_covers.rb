class AddDomainPriorityToCovers < ActiveRecord::Migration[5.1]
  def change
    add_column :covers, :domain, :string
    add_column :covers, :priority, :integer
  end
end

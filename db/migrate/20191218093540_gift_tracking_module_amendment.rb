class GiftTrackingModuleAmendment < ActiveRecord::Migration[5.2]
  def change
    remove_column :gift_trackings, :booking_id, :bigint
    add_column :gift_trackings, :booking_group_id, :bigint
    add_column :booking_groups, :redemption_points, :integer 
  end
end

class AddCruiseModule < ActiveRecord::Migration[5.1]
  def change
    create_table :booking_cruise_rooms do |t|
      t.string :room_price_type
      t.integer :status, default: 0

      t.timestamps
    end

    add_column :tours, :is_cruise, :boolean, default: false
    add_column :tours, :guaranteed_departure, :boolean, default: false
    add_column :bookings, :booking_cruise_room_id, :integer
  end
end

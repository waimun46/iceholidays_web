class AddExportedAtToEtravelInvoice < ActiveRecord::Migration[5.1]
  def change
    add_column :etravel_invoices, :exported_at, :datetime, array: true
  end
end

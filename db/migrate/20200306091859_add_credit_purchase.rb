class AddCreditPurchase < ActiveRecord::Migration[5.2]
  def change
    create_table :credit_purchases do |t|
      t.integer :user_id
      t.integer :status, default: 0
      t.decimal :amount
      t.decimal :credits
      t.decimal :extra_credits
      t.string :remark
      t.timestamps
    end
  end
end

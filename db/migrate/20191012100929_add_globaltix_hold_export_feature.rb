class AddGlobaltixHoldExportFeature < ActiveRecord::Migration[5.2]
  def change
    add_column :globaltix_ticket_booking_groups, :export_params, :jsonb
    add_column :globaltix_ticket_booking_groups, :exported_at, :datetime
  end
end

class AddAgentUserToTBOBooking < ActiveRecord::Migration[5.2]
  def up
    add_column :tbo_bookings, :agent_user_id, :integer
    add_column :tbo_hotels, :is_active, :boolean, default: false
  end

  def down
    remove_column :tbo_bookings, :agent_user_id, :integer
    remove_column :tbo_hotels, :is_active, :boolean, default: false
  end
end

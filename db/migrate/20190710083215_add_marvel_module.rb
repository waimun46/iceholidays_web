class AddMarvelModule < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.references :taggable, polymorphic: true, index: true
      t.integer :user_id
      t.integer :amount
      t.integer :net_amount
      t.string :method
      t.integer :gateway
      t.integer :callback
      t.string :redirect_url
      t.integer :status, default: 0
      t.timestamps
    end

    create_table :activities do |t|
      t.string :title
      t.string :country
      t.tsrange :booking_date
      t.tsrange :travelling_date
      t.string :highlight
      t.text :description
      t.text :terms_and_conditions
      t.integer :max_booking
      t.integer :min_booking
      t.integer :status, default: 0
      t.jsonb :operation_days, null: false, default: {}
      t.jsonb :categories, null: false, default: {}
      t.string :supplier
      t.boolean :gen_code, default: false
      t.timestamps
    end

    create_table :activity_prices do |t|
      t.references :activity
      t.string :category
      t.integer :cost_price
      t.integer :price
      t.string :code_type
      t.timestamps
    end

    create_table :activity_booking_groups do |t|
      t.string :remark
      t.integer :payment_method
      t.datetime :travel_date
      t.integer :last_edited_user_id
      t.datetime :last_edited_at
      t.timestamps
    end

    create_table :activity_bookings do |t|
      t.references :activity_booking_group
      t.references :activity
      t.string :code
      t.string :name
      t.string :mobile
      t.string :email
      t.integer :quantity
      t.integer :price
      t.string :category
      t.string :remark
      t.integer :status, default: 0
      t.integer :agent_user_id
      t.integer :last_edited_user_id
      t.datetime :last_edited_at
      t.timestamps
    end
  end
end

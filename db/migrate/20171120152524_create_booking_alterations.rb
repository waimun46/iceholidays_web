class CreateBookingAlterations < ActiveRecord::Migration[5.1]
  def change
    create_table :booking_alterations do |t|
      t.integer :original_tour_id
      t.integer :original_booking_group_id
      t.integer :alter_type
      t.string :reason
      t.integer :amount
      t.boolean :whole_group, default: false
      t.integer :prev_id
      t.integer :created_user_id
      t.integer :exported_user_id
      t.datetime :exported_at
      t.integer :exported_count

      t.timestamps
    end
  end
end

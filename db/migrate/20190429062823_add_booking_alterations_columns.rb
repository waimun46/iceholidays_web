class AddBookingAlterationsColumns < ActiveRecord::Migration[5.1]
  def up
    add_column :bookings, :restore_withdrawn_alteration_id, :string, array: true

    add_column :booking_alterations, :restore_withdrawn_user_id, :integer
    add_column :booking_alterations, :restore_withdrawn_at, :datetime
  end

  def down
    remove_column :bookings, :restore_withdrawn_alteration_id, :string, array: true

    remove_column :booking_alterations, :restore_withdrawn_user_id, :integer
    remove_column :booking_alterations, :restore_withdrawn_at, :datetime
  end
end

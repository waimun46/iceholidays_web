class AddTourLeaderBookingRooms < ActiveRecord::Migration[5.2]
  def change
    add_column :tour_leaders, :visa_expiry_date, :datetime
    rename_column :tour_leaders, :passport_no, :passport_number
    rename_column :tour_leaders, :phone_number, :mobile

    add_column :bookings, :visa_expiry_date, :datetime
    add_column :booking_rooms, :tour_leader_id, :integer

  end
end


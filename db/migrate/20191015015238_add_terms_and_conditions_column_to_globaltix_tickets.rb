class AddTermsAndConditionsColumnToGlobaltixTickets < ActiveRecord::Migration[5.2]
  def up
    add_column :globaltix_tickets, :terms_and_conditions, :string, default: ''
    add_column :activity_prices, :terms_and_conditions, :string, default: ''

    if Rails.env.iceb2b?
      GlobaltixTicket.all.each do |globaltix_ticket|
        ticket, ticket_err = Globaltix::Ticket.find(globaltix_ticket.ticket_id)
        globaltix_ticket.update_columns(terms_and_conditions: ticket.terms_and_conditions) if ticket.present? && ticket.terms_and_conditions.present?
      end
    end
  end

  def down
    remove_column :globaltix_tickets, :terms_and_conditions, :string, default: ''
    remove_column :activity_prices, :terms_and_conditions, :string, default: ''
  end
end

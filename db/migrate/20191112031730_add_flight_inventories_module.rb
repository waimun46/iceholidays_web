class AddFlightInventoriesModule < ActiveRecord::Migration[5.2]
  def change
    create_table :flight_inventories do |t|
    	t.string :title
      t.date :purchased_at
      t.string :purchased_by
      t.integer :materialized_rate
      t.string :remark

      t.timestamps
    end
    create_table :flight_details do |t|
      t.string :airline_iata
      t.string :flight_no
      t.datetime :departure
      t.datetime :arrival
      t.string :origin
      t.string :destination
      t.bigint :flight_inventory_id

      t.timestamps
    end
    create_table :flight_seats do |t|
      t.integer :total_seat
      t.string :pnr
      t.integer :price
      t.integer :tax
      t.integer :fee
      t.integer :bag_seat_fee
      t.string :remark
      t.bigint :flight_inventory_id

      t.timestamps
    end
    create_table :flight_receipts do |t|
      t.string :number
      t.integer :amount
      t.string :remark
      t.bigint :flight_seat_id
      t.datetime :paid_at

      t.timestamps
    end
    rename_table :tour_reminders, :reminders
    rename_column :reminders, :tour_id, :taggable_id
    add_column  :reminders, :taggable_type, :string
    Reminder.all.each do |reminder|
      reminder.update_columns(taggable_type: 'Tour')
    end

  end
end

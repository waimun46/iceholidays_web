class CreateGiftTrackings < ActiveRecord::Migration[5.1]
  def change
    create_table :gift_trackings do |t|
      t.references :gift_tracking_group
      t.references :booking
      t.integer :point_topup
      t.integer :status
      
      t.timestamps
    end
  end
end

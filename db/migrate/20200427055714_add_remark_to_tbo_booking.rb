class AddRemarkToTBOBooking < ActiveRecord::Migration[5.2]
  def up
    add_column :tbo_bookings, :remark, :string
  end

  def down
    remove_column :tbo_bookings, :remark, :string
  end
end

class CreateItineraries < ActiveRecord::Migration[5.1]
  def change
    create_table :itineraries do |t|
      t.string :code
      t.string :file
      t.string :remark
      t.string :url_source
      t.timestamps
    end
  end
end

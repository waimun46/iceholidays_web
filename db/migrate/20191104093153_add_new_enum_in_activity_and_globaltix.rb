class AddNewEnumInActivityAndGlobaltix < ActiveRecord::Migration[5.2]
  def up
    add_column :globaltix_attractions, :confirmation_time, :integer
    add_column :activities, :confirmation_time, :integer
  end
  def down
    remove_column :globaltix_attractions, :confirmation_time, :integer
    remove_column :activities, :confirmation_time, :integer
  end
end

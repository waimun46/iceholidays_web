class CreateInsuranceRebates < ActiveRecord::Migration[5.1]
  def change
    create_table :insurance_rebates do |t|
      t.integer :zone
      t.int4range :days_duration
      t.integer :price
      t.timestamps
    end
		InsuranceRebate.create(zone: 1, days_duration: (1..5), price: 39)
		InsuranceRebate.create(zone: 2, days_duration: (1..5), price: 56)
		InsuranceRebate.create(zone: 3, days_duration: (1..5), price: 68)

		InsuranceRebate.create(zone: 1, days_duration: (6..10), price: 57)
		InsuranceRebate.create(zone: 2, days_duration: (6..10), price: 82)
		InsuranceRebate.create(zone: 3, days_duration: (6..10), price: 100)

		InsuranceRebate.create(zone: 1, days_duration: (11..18), price: 79)
		InsuranceRebate.create(zone: 2, days_duration: (11..18), price: 115)
		InsuranceRebate.create(zone: 3, days_duration: (11..18), price: 140)

		InsuranceRebate.create(zone: 1, days_duration: (19..31), price: 104)
		InsuranceRebate.create(zone: 2, days_duration: (19..31), price: 150)
		InsuranceRebate.create(zone: 3, days_duration: (19..31), price: 183)
  end
end

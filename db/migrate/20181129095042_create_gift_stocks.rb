class CreateGiftStocks < ActiveRecord::Migration[5.1]
  def change
    create_table :gift_stocks do |t|
      t.references :gift
      t.integer :quantity

      t.timestamps
    end
  end
end

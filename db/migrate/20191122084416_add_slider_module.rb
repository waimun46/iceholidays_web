class AddSliderModule < ActiveRecord::Migration[5.2]
  def up
    create_table :sliders do |t|
      t.integer :category
      t.string :title
      t.integer :priority
      t.integer :status, default: 0
      t.timestamps
    end

    create_table :slider_tabs do |t|
      t.string :title
      t.integer :priority
      t.integer :slider_id
      t.timestamps
    end

    create_table :slider_cards do |t|
      t.integer :priority
      t.string :cover
      t.string :link
      # No 1
      t.string :title1, default: ''
      t.string :subtitle1, default: ''
      t.string :highlight1, default: ''
      t.string :logo1, default: ''
      # No 2
      t.string :title2, default: ''
      t.string :subtitle2, default: ''
      t.string :highlight2, default: ''
      t.string :logo2, default: ''
      t.integer :slider_tab_id
      t.timestamps
    end

    add_column :covers, :category, :integer, default: 0
    Cover.where(taggable: nil).each do |cover|
      if cover.domain = "iceb2b"
        cover.update_attributes(category: 1)
      elsif cover.domain = "travelb2b"
        cover.update_attributes(category: 2)
      elsif cover.domain = "travelb2b_search"
        cover.update_attributes(category: 3)
      end
    end
    remove_column :covers, :domain

    # Search cover create
    Cover.create([{:image => Rails.root.join("./app/javascript/images/banner/20.jpg").open, :category => 3, :priority => 1}])

    # Slider create
    Slider.create([
      {:category => 3, :title => "GD Exclusive", :priority => 1, :status => 1},
      {:category => 2, :title => "Let us inspire you", :priority => 2, :status => 1},
      {:category => 0, :title => "Great Deals", :priority => 3, :status => 1},
      {:category => 1, :title => "Hot Destinations", :priority => 4, :status => 1},
      {:category => 3, :title => "Airlines Deals", :priority => 5, :status => 1}
    ])

    # GD Exclusive 
    Slider.find_by_title("GD Exclusive").slider_tabs.create([
      {:title => "Tab 1", :priority => 1}
    ])
    Slider.find_by_title("GD Exclusive").slider_tabs.all.each do |slider_tab|
      slider_tab.slider_cards.create([
        {:priority => 1, :link => "", :cover => Rails.root.join("./app/javascript/images/series_slider/01.jpg").open },
        {:priority => 2, :link => "", :cover => Rails.root.join("./app/javascript/images/series_slider/02.jpg").open },
        {:priority => 3, :link => "", :cover => Rails.root.join("./app/javascript/images/series_slider/03.jpg").open },
        {:priority => 4, :link => "", :cover => Rails.root.join("./app/javascript/images/series_slider/04.jpg").open },
        {:priority => 5, :link => "", :cover => Rails.root.join("./app/javascript/images/series_slider/05.jpg").open }
      ])
    end

    # Let us inspire you 
    Slider.find_by_title("Let us inspire you").slider_tabs.create([
      {:title => "Top Activity", :priority => 1},
      {:title => "Top Wifi", :priority => 2}
    ])
    Slider.find_by_title("Let us inspire you").slider_tabs.all.each do |slider_tab|
      if slider_tab.title == "Top Activity"
        slider_tab.slider_cards.create([
          {:priority => 1, :link => "", :cover => Rails.root.join("./app/javascript/images/top_activity/01.png").open },
          {:priority => 2, :link => "", :cover => Rails.root.join("./app/javascript/images/top_activity/02.png").open },
          {:priority => 3, :link => "", :cover => Rails.root.join("./app/javascript/images/top_activity/03.png").open },
          {:priority => 4, :link => "", :cover => Rails.root.join("./app/javascript/images/top_activity/04.png").open },
          {:priority => 5, :link => "", :cover => Rails.root.join("./app/javascript/images/top_activity/05.png").open }
        ])
      elsif slider_tab.title == "Top Wifi"
        slider_tab.slider_cards.create([
          {:priority => 1, :link => "", :cover => Rails.root.join("./app/javascript/images/roamin/01.png").open },
          {:priority => 2, :link => "", :cover => Rails.root.join("./app/javascript/images/roamin/03.png").open },
          {:priority => 3, :link => "", :cover => Rails.root.join("./app/javascript/images/roamin/03.png").open },
          {:priority => 4, :link => "", :cover => Rails.root.join("./app/javascript/images/roamin/04.png").open }
        ])
      end
    end

    # Great Deals
    Slider.find_by_title("Great Deals").slider_tabs.create([
      {:title => "Tab 1", :priority => 1}
    ])
    Slider.find_by_title("Great Deals").slider_tabs.all.each do |slider_tab|
      slider_tab.slider_cards.create([
        {:priority => 1, :link => "", :cover => Rails.root.join("./app/javascript/images/small_banner/1.png").open }
      ])
    end

    # Hot Destinations
    Slider.find_by_title("Hot Destinations").slider_tabs.create([
      {:title => "Tab 1", :priority => 1}
    ])
    Slider.find_by_title("Hot Destinations").slider_tabs.all.each do |slider_tab|
      slider_tab.slider_cards.create([
        {:priority => 1, :link => "", :cover => Rails.root.join("./app/javascript/images/top_place/asia/01.png").open , :title1 => "Amari Watergate Bangkok", :subtitle1 => "Location: Bangkok", :highlight1 => "Price From RM 456", :logo1 => Rails.root.join("./app/javascript/images/hotel_logo/amari-watergate-bkk-logo.png").open, :title2 => "AVANI Atrium Bangkok", :subtitle2 => "Location: Bangkok", :highlight2 => "Price From RM 219", :logo2 => Rails.root.join("./app/javascript/images/hotel_logo/avani_bangkok.png").open},
        {:priority => 2, :link => "", :cover => Rails.root.join("./app/javascript/images/top_place/asia/05.png").open , :title1 => "Marriott Sydney Harbour at Circular Quay", :subtitle1 => "Location: Sydney", :highlight1 => "Price From RM 1333", :logo1 => Rails.root.join("./app/javascript/images/hotel_logo/marriott_hotel.png").open, :title2 => "Radisson Blu Plaza Hotel Sydney", :subtitle2 => "Location: Sydney", :highlight2 => "Price From RM 1330", :logo2 => Rails.root.join("./app/javascript/images/hotel_logo/radisson.jpg").open},
        {:priority => 3, :link => "", :cover => Rails.root.join("./app/javascript/images/top_place/asia/03.png").open , :title1 => "Venetian Macao Resort", :subtitle1 => "Location: Macau", :highlight1 => "Price From RM 764", :logo1 => Rails.root.join("./app/javascript/images/hotel_logo/The_Venetian_logo.png").open, :title2 => "JW Marriott Macau", :subtitle2 => "Location: Macau", :highlight2 => "Price From RM 695", :logo2 => Rails.root.join("./app/javascript/images/hotel_logo/Jw Marriott Macao.png").open},
        {:priority => 4, :link => "", :cover => Rails.root.join("./app/javascript/images/top_place/asia/04.png").open , :title1 => "W Taipei", :subtitle1 => "Location: Taipei", :highlight1 => "Price From RM 1493", :logo1 => Rails.root.join("./app/javascript/images/hotel_logo/W-hotel.jpg").open, :title2 => "Westgate", :subtitle2 => "Location: Taipei", :highlight2 => "Price From 569", :logo2 => Rails.root.join("./app/javascript/images/hotel_logo/westgate_taipei_logo.jpg").open}
      ])
    end

    # Airlines Deals
    Slider.find_by_title("Airlines Deals").slider_tabs.create([
      {:title => "Tab 1", :priority => 1}
    ])
    Slider.find_by_title("Airlines Deals").slider_tabs.all.each do |slider_tab|
      slider_tab.slider_cards.create([
        {:priority => 1, :link => "", :cover => Rails.root.join("./app/javascript/images/explore_img/01.png").open },
        {:priority => 2, :link => "", :cover => Rails.root.join("./app/javascript/images/explore_img/02.png").open },
        {:priority => 3, :link => "", :cover => Rails.root.join("./app/javascript/images/explore_img/03.png").open },
        {:priority => 4, :link => "", :cover => Rails.root.join("./app/javascript/images/explore_img/04.png").open },
        {:priority => 5, :link => "", :cover => Rails.root.join("./app/javascript/images/explore_img/05.png").open },
        {:priority => 6, :link => "", :cover => Rails.root.join("./app/javascript/images/explore_img/06.png").open },
        {:priority => 7, :link => "", :cover => Rails.root.join("./app/javascript/images/explore_img/07.png").open },
        {:priority => 8, :link => "", :cover => Rails.root.join("./app/javascript/images/explore_img/08.png").open }
      ])
    end

    Setting.tagline = "Your Confidence, Our Commitment!"
  end

  def down
    drop_table :sliders
    drop_table :slider_tabs
    drop_table :slider_cards
    add_column :covers, :domain, :string

    Cover.where(taggable: nil).each do |cover|
      if cover.domain == "iceb2b_login"
        cover.update_attributes(domain: 'iceb2b')
      elsif cover.category == "travelb2b_login"
        cover.update_attributes(domain: 'travelb2b')
      elsif cover.domain == "travelb2b_search"
        cover.update_attributes(domain: 'travelb2b_search')
      end
    end

    remove_column :covers, :category
  end
end

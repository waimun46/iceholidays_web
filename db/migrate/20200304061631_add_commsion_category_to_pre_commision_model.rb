class AddCommsionCategoryToPreCommisionModel < ActiveRecord::Migration[5.2]
  def change
    add_column :pre_commisions, :commision_category, :integer
  end
end

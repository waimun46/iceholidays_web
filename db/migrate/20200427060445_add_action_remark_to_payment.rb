class AddActionRemarkToPayment < ActiveRecord::Migration[5.2]
  def change
    add_column :payments, :action_remark, :string
  end
end

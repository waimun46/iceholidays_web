class UpdateActivitiesModule < ActiveRecord::Migration[5.2]
  
  def up
    rename_column :activities, :tour_guide, :transfer
    rename_column :globaltix_attractions, :tour_guide, :transfer

    add_column :globaltix_tickets, :priority, :integer, default: 0
    add_column :globaltix_tickets, :blockout_dates, :daterange, array: true

    add_column :activity_prices, :blockout_dates, :daterange, array: true
  end

  def down
    rename_column :activities, :transfer, :tour_guide
    rename_column :globaltix_attractions, :transfer, :tour_guide

    remove_column :globaltix_tickets, :priority, :integer, default: 0
    remove_column :globaltix_tickets, :blockout_dates, :daterange, array: true

    remove_column :activity_prices, :blockout_dates, :daterange, array: true
  end

end

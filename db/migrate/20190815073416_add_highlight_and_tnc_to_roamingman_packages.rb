class AddHighlightAndTncToRoamingmanPackages < ActiveRecord::Migration[5.1]
  def change
    add_column :roamingman_packages, :highlight, :string
    add_column :roamingman_packages, :terms_and_conditions, :text
  end
end

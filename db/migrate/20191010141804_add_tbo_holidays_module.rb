class AddTBOHolidaysModule < ActiveRecord::Migration[5.1]
  def up
    create_table :tbo_countries, {:id => false}  do |t|
      t.string :name
      t.string :code
      t.boolean :is_active, default: false
      t.timestamps
    end

    create_table :tbo_cities, {:id => false}  do |t|
      t.string :country_code, index:true
      t.string :name
      t.string :code

      t.timestamps
    end

    create_table :tbo_hotels  do |t|
      t.string :hotel_code, index:true  
      t.string :city_code, index:true      
      t.string :hotel_name
      t.string :address
      t.string :hotel_rating
      t.string :fax_number
      t.string :phone_number
      t.string :country_name
      t.string :description
      t.string :map
      t.string :pin_code
      t.string :hotel_website_url
      t.string :trip_advisor_rating
      t.string :trip_advisor_review_url
      t.string :city_name
      t.string :hotel_location
      t.decimal :markup_amount
      t.integer :markup_type
      t.text   :attractions
      t.text   :hotel_facilities
      t.timestamps
    end

    create_table :tbo_bookings do |t|
      t.date :check_in
      t.date :check_out
      t.string :hotel_code, index:true        
      t.integer :booking_id, index:true
      t.string :reference_no, index:true
      t.string :confirmation_no, index:true
      t.string :invoice_number, index:true
      
      t.string :currency            
      t.decimal :original_total_fare, default: 0
      t.decimal :markup, default: 0   
      t.decimal :total_fare, default: 0      
      t.integer :trip_id
      t.integer :no_of_rooms, default: 0
      t.integer :no_of_guests, default: 0
      t.string :error_code    
      t.string :message         
      t.text :cancellation_policies
      t.date :last_cancellation_deadline
      t.decimal :refund_amount, default: 0  
      t.decimal :cancellation_charge, default: 0  
      t.jsonb :rooms, null: false, default: {}
      t.jsonb :address_info, null: false, default: {}      
      t.integer :booking_status
      t.boolean :voucher_status           
      t.integer :cancellation_status
      t.integer :amendment_status  
      t.boolean :cancellation_requested, default: false
      t.timestamps
    end

    create_table :tbo_guests do |t| 
      t.references :tbo_booking, foreign_key:true
      t.boolean :lead_guest, default: false
      t.integer :guest_type, default: 0
      t.integer :guest_in_room
      t.integer :title, default: 0
      t.string :first_name
      t.string :last_name
      t.integer :age           

      t.timestamps
    end

    create_table :tbo_amendment_requests do |t| 
      t.references :tbo_booking, foreign_key:true 
      t.integer :status  
      t.string :remarks
      t.string :amendment_message      
      t.jsonb :amendment_requested  
      t.jsonb :approval_information              
      t.timestamps
    end

    execute "ALTER TABLE tbo_countries ADD PRIMARY KEY (code);"
    execute "ALTER TABLE tbo_cities ADD PRIMARY KEY (code);"
  end

  def down
    drop_table :tbo_hotels           
    drop_table :tbo_cities   
    drop_table :tbo_countries
    drop_table :tbo_bookings
    drop_table :tbo_amendment_requests 
    drop_table :tbo_guests       
  end
end

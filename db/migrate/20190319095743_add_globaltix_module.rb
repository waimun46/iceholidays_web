class AddGlobaltixModule < ActiveRecord::Migration[5.1]
  def change
    create_table :globaltix_tickets do |t|
      t.integer :attraction_id
      t.integer :ticket_id
      t.integer :markup_category
      t.decimal :markup
      t.string :country
      t.string :title
      t.jsonb :details, null: false, default: {}
      t.string :remark
      t.integer :status, default: 0
      t.timestamps
    end

    create_table :globaltix_ticket_booking_groups do |t|
      t.decimal :price
      t.string :orignal_price
      t.string :reference_number
      t.string :e_ticket_url
      t.jsonb :detail

      t.timestamps
    end

    create_table :globaltix_ticket_bookings do |t|
      t.string :email
      t.string :name
      t.integer :status, default: 0
      t.integer :globaltix_ticket_booking_group_id
      t.integer :globaltix_ticket_booking_group_detail_id
      t.integer :globaltix_ticket_id
      t.integer :agent_user_id
      t.integer :sale_rep
      t.string :initiator_user_id

      t.timestamps
    end

    create_table :exchange_rates do |t|
      t.string :from_currency
      t.string :to_currency
      t.decimal :rate, default: 0

      t.timestamps
    end
  end
end

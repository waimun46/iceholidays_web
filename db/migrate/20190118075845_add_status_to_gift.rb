class AddStatusToGift < ActiveRecord::Migration[5.1]
  def change
    add_column :gifts, :status, :integer, default: 0
  end
end

class AddProformaInvoiceDetailToBookingGroups < ActiveRecord::Migration[5.1]
  def change
    add_column :booking_groups, :proforma_invoice_details, :jsonb
  end
end

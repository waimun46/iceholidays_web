class AddTransactionNoToPayments < ActiveRecord::Migration[5.2]
  def up
    add_column :payments, :transaction_no, :string
  end

  def down
    remove_column :payments, :transaction_no, :string
  end
end

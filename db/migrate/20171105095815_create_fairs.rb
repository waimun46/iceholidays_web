class CreateFairs < ActiveRecord::Migration[5.1]
  def change
    create_table :fairs do |t|
      t.string :code
      t.string :title
      t.string :other_title
      t.tsrange :duration
      t.string :fair_type
      t.string :remark
      t.boolean :active, default: false
      t.string :messages
      t.string :internal_reference
      t.integer :allow_user_ids, array: true

      t.timestamps
    end
  end
end

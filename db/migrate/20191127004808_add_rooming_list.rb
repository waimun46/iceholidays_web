class AddRoomingList < ActiveRecord::Migration[5.2]
  def up
    create_table :booking_rooms do |t|
      t.string :room_type
      t.integer :tour_id

      t.timestamps
    end
    add_column :bookings, :booking_room_id, :integer
  end

  def down
    remove_column :bookings, :booking_room_id, :integer
    drop_table :booking_rooms
  end
end

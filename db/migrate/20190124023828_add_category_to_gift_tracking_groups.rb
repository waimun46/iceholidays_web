class AddCategoryToGiftTrackingGroups < ActiveRecord::Migration[5.1]
  def change
    add_column :gift_tracking_groups, :category, :integer
  end
end

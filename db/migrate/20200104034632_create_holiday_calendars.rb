class CreateHolidayCalendars < ActiveRecord::Migration[5.2]
  def change
    create_table :holiday_calendars do |t|
    t.string    :name
    t.tsrange   :date
    t.datetime  :created_at, null: false
    t.datetime  :updated_at, null: false
    t.timestamps
    end
  end
end

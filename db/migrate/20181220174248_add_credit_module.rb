class AddCreditModule < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :credits, :integer, default: 0

    create_table :credit_transactions do |t|
      t.references :taggable, polymorphic: true, index: true
      t.integer :user_id
      t.integer :prev_id
      t.integer :amount
      t.integer :user_balance
      t.string :checksum
      t.string :remark
      t.timestamps
    end
  end
end

class CreateGifts < ActiveRecord::Migration[5.1]
  def change
    create_table :gifts do |t|
      t.string :title
      t.string :color
      t.string :image
      t.integer :points
      t.string :sku
      t.integer :threshold, default: 0
      t.integer :balance, default: 0
      t.timestamps
    end
  end
end

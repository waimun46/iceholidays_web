class CreateHolidays < ActiveRecord::Migration[5.1]
  def change
    create_table :holidays do |t|
      t.integer :year
      t.date :dates, array: true, default: []

      t.timestamps
    end
  end
end

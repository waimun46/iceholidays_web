class AddRemarkToFlightBookings < ActiveRecord::Migration[5.1]
  def change
    add_column :flight_bookings, :remark, :string
  end
end

class CreateFlightCommissions < ActiveRecord::Migration[5.2]
  def change
    create_table :flight_commissions do |t|
      t.string :name
      t.string :airline_iata 
      t.string :origin_airport_iata
      t.string :destination_airport_iata
      t.decimal :commission, default: 0
      t.string :commission_category
      t.timestamps
    end
  end
end

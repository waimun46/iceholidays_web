class CreateInfantBookings < ActiveRecord::Migration[5.1]
  def change
    create_table :infant_bookings do |t|
      t.references :booking
      t.string :name
      t.integer :gender
      t.string :passport_number
      t.datetime :passport_expiry_date
      t.string :passport_issuing_place
      t.string :passport_photocopy
      t.datetime :date_of_birth
      t.string :passport_received, default: ""
      t.datetime :pp_return_date
      t.boolean :photo_received, default: false
      t.boolean :own_visa, default: false
      t.string :visa_no, default: ""
      t.string :remark, default: ""
      t.string :fp_ref, default: ""
      t.boolean :agent_confirmation, default: false
      t.integer :agent_user_id
      t.integer :initiator_user_id
      t.integer :status, default: 0
      t.integer :last_edited_user_id
      t.datetime :last_edited_at

      t.timestamps
    end
  end
end

class CreateBookingGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :booking_groups do |t|
      t.string :code
      t.integer :deposit, default: 0
      t.integer :received_payment, default: 0
      t.boolean :keep_in_view, default: true
      t.string :remark
      t.string :exported_remark
      t.integer :last_edited_user_id
      t.datetime :last_edited_at
      t.integer :exported_user_id
      t.datetime :exported_at
      t.integer :exported_count

      t.timestamps
    end
  end
end

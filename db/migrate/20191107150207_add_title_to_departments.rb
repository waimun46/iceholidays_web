class AddTitleToDepartments < ActiveRecord::Migration[5.2]
  def change
    add_column :departments, :title, :string
  end
end

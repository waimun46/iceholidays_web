class CreateFlightBookings < ActiveRecord::Migration[5.1]
  def change
    create_table :flight_bookings do |t|
      t.string :designation
      t.string :first_name
      t.string :last_name
      t.string :country
      t.string :mobile
      t.string :passport_number
      t.date :passport_issue_date
      t.date :passport_expiry_date
      t.date :date_of_birth
      t.string :category
      t.integer :status, default: 0
      t.integer :flight_booking_group_id
      t.integer :agent_user_id
      t.string :sale_rep
      t.integer :initiator_user_id

      t.timestamps
    end
  end
end

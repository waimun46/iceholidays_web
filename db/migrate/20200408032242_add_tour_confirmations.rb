class AddTourConfirmations < ActiveRecord::Migration[5.2]
  def change
    add_column :tours, :meet_up_point, :string
    add_column :tours, :assembly_time, :datetime
    add_column :tours, :tour_guide_name, :string
    add_column :tours, :tour_guide_phone, :string
    add_column :tours, :tour_guide_chinese_name, :string
    add_column :tours, :hand_carry_weight, :string
    add_column :tours, :luggage_quantity, :string
    add_column :tours, :luggage_weight, :string
    add_column :tours, :confirmation_emergency_contact, :string

    add_column :itineraries, :temperature, :int4range
    add_column :itineraries, :currency_english, :string, array: true
    add_column :itineraries, :currency_chinese, :string, array: true
    add_column :itineraries, :time_difference_english, :string
    add_column :itineraries, :time_difference_chinese, :string
    add_column :itineraries, :voltage, :string
    add_column :itineraries, :tour_confirmation_remarks, :string
    
  end
end


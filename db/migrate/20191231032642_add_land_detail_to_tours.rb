class AddLandDetailToTours < ActiveRecord::Migration[5.2]
  def change
    add_column :tours, :land_staff, :string
    add_column :tours, :land_company, :string
    add_column :tours, :guide_languages, :string
    add_column :tours, :welcome_board, :string
    add_column :tours, :letter_head_image, :string 
    add_column :tours, :operator_remarks, :string,  array: true
    add_column :tours, :tipping, :string
  end
end
class CreateAutocompletes < ActiveRecord::Migration[5.1]
  def change
    create_table :autocompletes do |t|
      t.string :name
      t.jsonb :keywords, null: false, default: {}

      t.timestamps
    end
    add_index :autocompletes, :name
  end
end

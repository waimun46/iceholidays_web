class CreateAirlines < ActiveRecord::Migration[5.1]
  def change
    create_table :airlines do |t|
      t.string :name
      t.string :iata_code
      t.string :country
      t.string :logo
      t.jsonb :markup_categories, null: false, default: {}
      t.jsonb :markups, null: false, default: {}

      t.timestamps
    end
  end
end

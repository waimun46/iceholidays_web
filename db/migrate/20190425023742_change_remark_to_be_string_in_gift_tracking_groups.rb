class ChangeRemarkToBeStringInGiftTrackingGroups < ActiveRecord::Migration[5.1]
  def up
    change_column :gift_tracking_groups, :remark, :string
  end

  def down
    change_column :gift_tracking_groups, :remark, :text
  end
end

class CreateTourReminders < ActiveRecord::Migration[5.1]
  def change
    create_table :tour_reminders do |t|
      t.string :title
      t.string :description
      t.date :reminder_date
      t.string :email
      t.references :tour

      t.timestamps
    end
  end
end

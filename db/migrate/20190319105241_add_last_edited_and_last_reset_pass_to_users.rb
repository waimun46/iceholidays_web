class AddLastEditedAndLastResetPassToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :last_edited_user, :integer
    add_column :users, :last_edited_at, :datetime
    add_column :users, :last_reset_password_edited, :integer
    add_column :users, :last_reset_password_edited_at, :datetime
  end
end

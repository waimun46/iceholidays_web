class AddAdminRemarkAndSubSaleRepToBookings < ActiveRecord::Migration[5.1]
  def change
    add_column :bookings, :admin_remark, :string, default: ""
    add_column :bookings, :sub_sale_rep, :string
  end
end

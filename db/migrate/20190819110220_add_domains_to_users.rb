class AddDomainsToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :domains, :string, array: true, default: []

    User.all.each do |user|
      user.update_columns(domains: ['iceb2b', 'travelb2b'])
    end
  end
end

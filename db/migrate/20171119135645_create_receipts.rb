class CreateReceipts < ActiveRecord::Migration[5.1]
  def change
    create_table :receipts do |t|
      t.string :number
      t.integer :amount
      t.string :remark
      t.references :booking_group

      t.timestamps
    end
  end
end

class AddBrbModule < ActiveRecord::Migration[5.1]
  def change
    create_table :brb_bookings do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.integer :agent_user_id
      t.string :sale_rep
      t.integer :initiator_user_id
      t.integer :flight_booking_id
      t.integer :brb_booking_group_id
      t.integer :status, default: 0

      t.timestamps
    end

    create_table :brb_booking_groups do |t|
      t.decimal :price
      t.string :service_number
      t.string :product_code
      t.boolean :is_international
      t.string :status_code

      t.timestamps
    end
  end
end

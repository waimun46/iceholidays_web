class AddSubtractFeesToTours < ActiveRecord::Migration[5.1]
  def change
  	add_column :tours, :subtract_fees, :jsonb
  	add_column :tours, :additional_fees, :jsonb
  end
end

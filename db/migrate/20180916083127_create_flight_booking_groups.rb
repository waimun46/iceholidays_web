class CreateFlightBookingGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :flight_booking_groups do |t|
      t.string :universal_code
      t.string :host_code
      t.string :uAPI_code
      t.decimal :price

      t.timestamps
    end
  end
end

class CreateCovers < ActiveRecord::Migration[5.1]
  def change
    create_table :covers do |t|
      t.string :image
      t.references :taggable, polymorphic: true

      t.timestamps
    end
  end
end

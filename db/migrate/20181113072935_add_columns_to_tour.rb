class AddColumnsToTour < ActiveRecord::Migration[5.1]
  def change
    add_column :tours, :ground_price, :integer
    add_column :tours, :ground_price_flag, :boolean, default: false
  end
end

class CreateDepartment < ActiveRecord::Migration[5.2]
  def change

    create_table :departments do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.text :important_note
      t.string :logo
      t.text :description
      t.text :fs_important_note
      t.string :fs_logo
      t.text :fs_description
    end

    department_mapping = {}

    [:western_and_exotic, :premium, :mainland_china, :asean, :east_asia, :g_trip, :cruise].each do |department|
      department_mapping[department] = Department.create(name: department.to_s.titleize).id
    end

    add_column :itineraries, :zone, :string
    rename_column :itineraries, :department, :old_department_enum
    add_column :itineraries, :department_id, :integer

    Itinerary.where.not(old_department_enum: nil).each do |itinerary|
      itinerary.update_columns(department_id: department_mapping[itinerary.old_department_enum.to_sym])
    end

    Itinerary.includes(:tours).active.references(:tours).where.not(tours: {id: nil}).where("tours.zone != '' or tours.zone is null").each do |itinerary|
      zone_list = itinerary.tours.map(&:zone).uniq
      if zone_list.count == 1
        itinerary.update_columns(zone: zone_list[0])
      end
    end

    department = Department.find_by_name('G Trip')
    if department.present?
      department.update_columns(name: 'G-Trip')
    end

    # remove_column :itineraries, :old_department_enum

  end
end

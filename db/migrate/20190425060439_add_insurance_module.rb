class AddInsuranceModule < ActiveRecord::Migration[5.1]
  def up
    add_column :booking_groups, :insurance_receiver, :string

    add_column :bookings, :insurance_cert, :string
    add_column :bookings, :insurance_export_remark, :jsonb, default: {}
    add_column :bookings, :insurance_exporter_id, :integer
    add_column :bookings, :insurance_exported_at, :datetime
    add_column :bookings, :nationality, :string
    add_column :bookings, :insurance_confirmation, :boolean
    add_column :bookings, :insurance_exported, :boolean, default: false
    add_column :bookings, :guardian_id, :integer
    add_column :bookings, :policy_holder, :jsonb, default: {}
    add_column :bookings, :policy_number, :string
    add_column :bookings, :insurance_endorse_count, :integer

    add_column :infant_bookings, :insurance_cert, :string
    add_column :infant_bookings, :insurance_export_remark, :jsonb, default: {}
    add_column :infant_bookings, :insurance_exporter_id, :integer
    add_column :infant_bookings, :insurance_exported_at, :datetime
    add_column :infant_bookings, :insurance_confirmation, :boolean
    add_column :infant_bookings, :insurance_exported, :boolean, default: false
    add_column :infant_bookings, :nationality, :string
    add_column :infant_bookings, :parent_id, :integer
    add_column :infant_bookings, :policy_holder, :jsonb, default: {}
    add_column :infant_bookings, :policy_number, :string
    add_column :infant_bookings, :insurance_endorse_count, :integer

    add_column :tours, :zone, :string

    zone_mapping = {
      "CAMBODIA" => 'Zone 1',
      "CHINA" => 'Zone 1',
      "VIETNAM" => 'Zone 1',
      "JAPAN" => 'Zone 1',
      "SRI LANKA" => 'Zone 1',
      "TAIWAN" => 'Zone 1',
      "INDONESIA" => 'Zone 1',
      "KOREA" => 'Zone 1',
      "VIETNAM / CHINA" => 'Zone 1',
      "MYANMAR" => 'Zone 1',
      "THAILAND" => 'Zone 1',
      "HONG KONG" => 'Zone 1',
      "EUROPE" => 'Zone 2',
      "AUSTRALIA" => 'Zone 1',
      "ITALY" => 'Zone 2',
      "DUBAI" => 'Zone 2',
      "SPAIN" => 'Zone 2',
      "NEPAL" => 'Zone 2',
      "AMERICA" => 'Zone 3',
      "RUSSIA" => 'Zone 2',
      "TURKEY" => 'Zone 2',
      "LAOS" => 'Zone 1',
      "ALASKA" => 'Zone 3',
      "ASIA, CRUISE" => 'Zone 1',
      "SOUTH AMERICA" => 'Zone 3',
      "NEW ZEALAND" => 'Zone 1',
      "PHILIPPINES" => 'Zone 1',
      "TAIWAN / JAPAN" => 'Zone 1',
      "SOUTH AFRICA" => 'Zone 1',
      "U.A.E." => 'Zone 2',
      "SINGAPORE" => 'Zone 1',
      "CRUISE" => '',
      "ASIA" => 'Zone 1',
      "SCANDINAVIA" => 'Zone 2',
      "NORTH AMERICA, CRUISE" => 'Zone 3',
      "MOROCCO" => 'Zone 1',
      "EGYPT" => 'Zone 3',
      "EXOTIC" => 'Zone 2',
      "OCEANIA, CRUISE" => '',
      "TAIWAN / HONG KONG / VIETNAM" => 'Zone 1',
      "HONG KONG / JAPAN" => 'Zone 1',
      "DUBAI & SRI LANKA" => 'Zone 2',
      "COLOMBO & DUBAI" => 'Zone 2',
      "NORTH KOREA" => 'Zone 1',
      " CHINA" => 'Zone 1',
      "ALASKA , CRUISE" => 'Zone 3',
      "ASIA, CRUISE0" => 'Zone 1',
      "MACAO" => 'Zone 1',
      "CHIANGMAI" => 'Zone 1'
    }

    europe_mapping = {
      "SRA" => 'Zone 2',
      "HSA" => 'Zone 3',
      "GRA" => 'Zone 2',
      "LSA" => 'Zone 2',
      "MSA" => 'Zone 2'
    }

    Tour.all.each do |tour|
      if tour.contient_country == "EUROPE, CRUISE"
        tour.update_columns(zone: europe_mapping[tour.code.last(3)])
      else
        tour.update_columns(zone: zone_mapping[tour.contient_country])
      end
    end
  end

  def down
    remove_column :booking_groups, :insurance_receiver, :string

    remove_column :bookings, :insurance_cert, :string
    remove_column :bookings, :insurance_export_remark, :jsonb, default: {}
    remove_column :bookings, :insurance_exporter_id, :integer
    remove_column :bookings, :insurance_exported_at, :datetime
    remove_column :bookings, :nationality, :string
    remove_column :bookings, :insurance_confirmation, :boolean
    remove_column :bookings, :insurance_exported, :boolean, default: false
    remove_column :bookings, :guardian_id, :integer
    remove_column :bookings, :policy_holder, :jsonb, default: {}
    remove_column :bookings, :policy_number, :string
    remove_column :bookings, :insurance_endorse_count, :integer

    remove_column :infant_bookings, :insurance_cert, :string
    remove_column :infant_bookings, :insurance_export_remark, :jsonb, default: {}
    remove_column :infant_bookings, :insurance_exporter_id, :integer
    remove_column :infant_bookings, :insurance_exported_at, :datetime
    remove_column :infant_bookings, :insurance_confirmation, :boolean
    remove_column :infant_bookings, :insurance_exported, :boolean, default: false
    remove_column :infant_bookings, :nationality, :string
    remove_column :infant_bookings, :parent_id, :integer
    remove_column :infant_bookings, :policy_holder, :jsonb, default: {}
    remove_column :infant_bookings, :policy_number, :string
    remove_column :infant_bookings, :insurance_endorse_count, :integer

    remove_column :tours, :zone, :string
  end
end

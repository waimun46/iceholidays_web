class AddStatusToRoamingmanAndPriceToActivityBookingGroup < ActiveRecord::Migration[5.2]
  def up
    add_column :roamingman_packages, :status, :integer, default: 0
    add_column :activity_booking_groups, :price, :integer
  end

  def down
    remove_column :roamingman_packages, :status, :integer
    remove_column :activity_booking_groups, :price, :integer
  end
end

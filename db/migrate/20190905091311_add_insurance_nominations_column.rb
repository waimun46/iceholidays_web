class AddInsuranceNominationsColumn < ActiveRecord::Migration[5.1]
  def up
    add_column :bookings, :insurance_nomination_flag, :boolean, default: true
    add_column :bookings, :insurance_nomination, :jsonb, default: {}
    add_column :bookings, :witness_details, :jsonb, default: {}

    add_column :infant_bookings, :insurance_nomination_flag, :boolean, default: true
    add_column :infant_bookings, :insurance_nomination, :jsonb, default: {}
    add_column :infant_bookings, :witness_details, :jsonb, default: {}
  end

  def down
    remove_column :bookings, :insurance_nomination_flag, :boolean, default: true
    remove_column :bookings, :insurance_nomination, :jsonb, default: {}
    remove_column :bookings, :witness_details, :jsonb, default: {}
    
    remove_column :infant_bookings, :insurance_nomination_flag, :boolean, default: true
    remove_column :infant_bookings, :insurance_nomination, :jsonb, default: {}
    remove_column :infant_bookings, :witness_details, :jsonb, default: {}
  end
end

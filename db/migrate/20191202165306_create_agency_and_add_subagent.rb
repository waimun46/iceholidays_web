class CreateAgencyAndAddSubagent < ActiveRecord::Migration[5.2]
  def up
    add_column :users, :agency_id, :integer
    add_column :users, :commission_details, :jsonb
    add_column :users, :travelb2b_module_access, :jsonb

    remove_index :users, :username
    add_index(:users, [:username, :agency_id], unique: true)

    create_table :agencies do |t|
      t.string   :code
      t.string   :name
      t.string   :full_name
      t.string   :other_name
      t.string   :alias_name
      t.string   :address
      t.string   :city
      t.string   :state
      t.string   :postal
      t.string   :country
      t.string   :phones, array: true
      t.string   :faxes, array: true
      t.string   :emails, array: true
      t.string   :website
      t.string   :remark
      t.integer  :max_users, default: 0
      t.string   :domains, default: [], array: true
      t.string   :logo
      t.string   :registration_no
      t.decimal  :lat, precision: 15, scale: 10
      t.decimal  :lng, precision: 15, scale: 10
      t.integer  :category
      t.integer  :max_sign_in, default: 0
      t.boolean  :is_active, default: true
    end

    User.agent.all.each do |user|
      user.create_agency
    end


    add_column :bookings, :price_breakdown, :jsonb

    add_column :roamingman_bookings, :price, :decimal
    add_column :roamingman_bookings, :price_breakdown, :jsonb
    RoamingmanBooking.all.each do |rb|
      rb.update_columns(price: rb.roamingman_booking_group.price)
    end

    change_column :activity_bookings, :price, :decimal
    change_column :activity_booking_groups, :price, :decimal

    add_column :activity_bookings, :price_breakdown, :jsonb
    ActivityBooking.all.each do |ab|
      ab.update_columns(price: ab.price * ab.quantity)
    end

    add_column :globaltix_ticket_bookings, :price, :decimal
    add_column :globaltix_ticket_bookings, :price_breakdown, :jsonb
    GlobaltixTicketBooking.all.each do |gb|
      gb.update_columns(price: gb.total_price)
    end

    add_column :flight_bookings, :price, :decimal
    add_column :flight_bookings, :price_breakdown, :jsonb
    FlightBooking.all.each do |fb|
      fb.update_columns(price: fb.flight_booking_group.price/fb.flight_booking_group.flight_bookings.count)
    end

    change_column :payments, :amount, :decimal
    change_column :payments, :net_amount, :decimal

    
    change_column :credit_transactions, :amount, :decimal
    change_column :credit_transactions, :user_balance, :decimal
    CreditTransaction.all.order(:id).each do |ct|
      ct.update_column :checksum, ct.calculate_checksum
    end


    rename_column :users, :credits ,:credits_decimal
    User.where("credits_decimal > 0").each do |u|
      u.update_columns(credits_decimal: u.credits_decimal * 100)
    end

  end

  def down
    remove_index :users, [:username, :agency_id]

    remove_column :users, :agency_id, :integer
    remove_column :users, :commission_details, :jsonb
    remove_column :users, :travelb2b_module_access, :jsonb

    add_index :users, :username

    drop_table :agencies

    remove_column :bookings, :price_breakdown, :jsonb

    remove_column :roamingman_bookings, :price, :decimal
    remove_column :roamingman_bookings, :price_breakdown, :jsonb
    
    change_column :activity_bookings, :price, :integer
    change_column :activity_booking_groups, :price, :integer
    remove_column :activity_bookings, :price_breakdown, :jsonb
    
    remove_column :globaltix_ticket_bookings, :price, :decimal
    remove_column :globaltix_ticket_bookings, :price_breakdown, :jsonb

    remove_column :flight_bookings, :price, :decimal
    remove_column :flight_bookings, :price_breakdown, :jsonb


    change_column :payments, :amount, :integer
    change_column :payments, :net_amount, :integer

    # change_column :users, :credits, :integer

    change_column :credit_transactions, :amount, :integer
    change_column :credit_transactions, :user_balance, :integer
    CreditTransaction.all.order(:id).each do |ct|
      ct.update_column :checksum, ct.calculate_checksum
    end

    User.where("credits_decimal > 0").each do |u|
      u.update_columns(credits_decimal: u.credits_decimal / 100)
    end
    rename_column :users ,:credits_decimal, :credits

  end
end

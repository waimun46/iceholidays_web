class AddPaymentMethodToFlightBookingGroups < ActiveRecord::Migration[5.1]
  def change
    add_column :flight_booking_groups, :payment_method, :integer
  end
end

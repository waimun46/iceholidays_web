class CreateTourAccomodations < ActiveRecord::Migration[5.2]
  def change
    create_table :tour_accomodations do |t|
      t.string :hotel_detail
      t.datetime :check_in_date
      t.datetime :check_out_date
      t.datetime :created_at, null: false
      t.datetime :updated_at, null: false
      t.bigint :tour_id
      t.index ["tour_id"], name: "index_tour_accomodations_on_tour_id"
    end
  end
end

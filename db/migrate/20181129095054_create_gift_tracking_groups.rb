class CreateGiftTrackingGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :gift_tracking_groups do |t|
      t.text :remark
      t.integer :agent_id
      t.boolean :agent_acknowledgement, default: false
      t.datetime :acknowledged_at
      t.datetime :cut_off_date

      t.timestamps
    end
  end
end

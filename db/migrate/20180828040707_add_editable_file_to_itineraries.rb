class AddEditableFileToItineraries < ActiveRecord::Migration[5.1]
  def change
    add_column :itineraries, :editable_file, :string
  end
end

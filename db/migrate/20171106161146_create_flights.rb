class CreateFlights < ActiveRecord::Migration[5.1]
  def change
    create_table :flights do |t|
      t.datetime :departure
      t.datetime :arrival
      t.string :from_to
      t.string :flight_no
      t.string :internal_remark
      t.string :agent_remark
      t.references :tour

      t.timestamps
    end
  end
end

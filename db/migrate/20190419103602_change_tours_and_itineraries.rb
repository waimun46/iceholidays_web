class ChangeToursAndItineraries < ActiveRecord::Migration[5.1]
  def self.up
    add_column :itineraries, :en_editable_file, :string
    add_column :itineraries, :cn_editable_file, :string
    add_column :itineraries, :countries, :string, array: true
    add_column :itineraries, :duration, :string
    add_column :itineraries, :caption, :string
    add_column :itineraries, :other_caption, :string
    add_column :itineraries, :description, :text
    add_column :itineraries, :additional_tour, :text
    add_column :itineraries, :optional_tour, :text

    add_column :tours, :single_supplement_price, :integer, default: 0
    add_column :tours, :tour_leader_remark, :text
    add_column :tours, :misc_fee, :integer, default: 0
    add_column :tours, :child_fares, :jsonb, default: {}
    add_column :tours, :dta_prices, :jsonb, default: {}

    change_column :itineraries, :remark, :text
  end
 
  def self.down
    remove_column :itineraries, :en_editable_file, :string
    remove_column :itineraries, :cn_editable_file, :string
    remove_column :itineraries, :countries, :string, array: true
    remove_column :itineraries, :duration, :string
    remove_column :itineraries, :caption, :string
    remove_column :itineraries, :other_caption, :string
    remove_column :itineraries, :description, :text
    remove_column :itineraries, :additional_tour, :text
    remove_column :itineraries, :optional_tour, :text

    remove_column :tours, :single_supplement_price, :integer
    remove_column :tours, :tour_leader_remark, :text
    remove_column :tours, :misc_fee, :integer
    remove_column :tours, :child_fares, :jsonb
    remove_column :tours, :dta_prices, :jsonb

    change_column :itineraries, :remark, :string
  end
end

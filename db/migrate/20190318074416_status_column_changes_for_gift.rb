class StatusColumnChangesForGift < ActiveRecord::Migration[5.1]
  def up
    add_column :gifts, :price, :integer
    add_column :gift_tracking_groups, :status_detail, :jsonb, default: {}
    add_column :gift_tracking_groups, :status, :integer, default: 0

    GiftTrackingGroup.all.each do |gift_tracking_group|
      if gift_tracking_group.gift_trackings.present?
        gift_tracking_group.update_columns(status: gift_tracking_group.gift_trackings.first.status)
      end
    end

    remove_column :gift_trackings, :status, :integer
    remove_column :gift_tracking_groups, :agent_acknowledgement, :boolean
    remove_column :gift_tracking_groups, :acknowledged_at, :datetime
  end

  def down
    remove_column :gifts, :price, :integer
    remove_column :gift_tracking_groups, :status_detail, :jsonb, default: {}
    remove_column :gift_tracking_groups, :status, :integer, default: 0
    add_column :gift_trackings, :status, :integer
    add_column :gift_tracking_groups, :agent_acknowledgement, :boolean, default: false
    add_column :gift_tracking_groups, :acknowledged_at, :datetime
  end
end

class AddLandTourModule < ActiveRecord::Migration[5.1]
  def up
    create_table :cities do |t|
      t.string :name
      t.string :state
      t.string :country
      t.timestamps
    end

    countries = CS.countries
    countries.each do |c_code, country|
      states = CS.states(c_code)
      if states.present?
        states.each do |s_code, state|
          cities = CS.cities(s_code, c_code)
          if cities.present?
            cities.each do |city|
              City.create(name: city, state: state, country: country)
            end
          else
            City.create(name: state, state: state, country: country)
          end
        end
      else
        City.create(name: country, state: country, country: country)
      end
    end

    create_table :land_tours do |t|
      t.string :code, unique: true
      t.string :title
      t.string :cn_title
      t.tsrange :travel_date
      t.integer :category
      t.integer :max_booking
      t.integer :min_booking
      t.string :highlight
      t.text :description
      t.text :cn_description
      t.text :terms_and_conditions
      t.string :guide_languages, array: true
      t.daterange :blockout_dates, array: true
      t.integer :cut_off_day
      t.jsonb :compulsory_charges
      t.jsonb :child_prices
      t.jsonb :dta_prices
      t.integer :free_of_charge
      t.integer :deposit
      t.integer :deposit_type, default: 0
      t.integer :single_supplement_price, default: 0
      t.jsonb :inclusion, default: {}
      t.integer :status, default: 0
      t.timestamps
    end

    create_table :land_tour_categories do |t|
      t.references :land_tour
      t.string :name
      t.timestamps
    end

    create_table :land_tour_prices do |t|
      t.references :land_tour_category
      t.daterange :dates, array: true
      t.jsonb :prices
      t.timestamps
    end

    create_table :land_tour_days do |t|
      t.references :land_tour
      t.integer :day_no
      t.string :title
      t.text :description
      t.jsonb :inclusion, default: {}
      t.timestamps
    end

    create_table :land_tour_cities do |t|
      t.references :land_tour_day
      t.references :city
      t.timestamps
    end

    create_table :land_tour_activities do |t|
      t.references :land_tour_day
      t.references :taggable, polymorphic: true, index: true
      t.timestamps
    end

    create_table :land_tour_booking_groups do |t|
      t.references :land_tour_category
      t.string :code
      t.string :remark
      t.decimal :total_price
      t.decimal :deposit, default: 0.0
      t.decimal :received_payment, default: 0.0
      t.datetime :departure_date
      t.jsonb :price_breakdown, default: {}
      t.string :rooms
      t.string :language
      t.jsonb :flight_departure, default: {}
      t.jsonb :flight_return, default: {}
      t.integer :last_edited_user_id
      t.datetime :last_edited_at
      t.timestamps
    end

    create_table :land_tour_bookings do |t|
      t.references :land_tour_booking_group
      t.references :land_tour
      t.string :designation
      t.string :name
      t.string :mobile
      t.datetime :date_of_birth
      t.string :passport_number
      t.string :email
      t.string :nationality
      t.integer :quantity
      t.decimal :price
      t.string :category
      t.string :remark
      t.jsonb :price_breakdown, default: {}
      t.integer :status, default: 0
      t.integer :agent_user_id
      t.integer :last_edited_user_id
      t.datetime :last_edited_at
      t.timestamps
    end
  end

  def down
    drop_table :land_tour_bookings
    drop_table :land_tour_booking_groups
    drop_table :land_tour_days
    drop_table :land_tour_activities
    drop_table :land_tour_cities
    drop_table :land_tour_prices
    drop_table :land_tour_categories
    drop_table :land_tours
    drop_table :cities
  end
end

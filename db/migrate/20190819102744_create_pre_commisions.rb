class CreatePreCommisions < ActiveRecord::Migration[5.1]
  def change
    create_table :pre_commisions do |t|
      t.integer :allow_user_ids, array: true, default: []
      t.integer :allow_itinerary_ids, array: true, default: []
      t.string :code
      t.string :title
      t.string :remark
      t.tsrange :duration
      t.integer :commisions
      t.boolean :active, default: false
      t.timestamps
    end
  end
end

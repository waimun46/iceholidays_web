class AddSeriesModule < ActiveRecord::Migration[5.2]

  def up
    change_column :bookings, :price, :decimal
    add_column :itineraries, :including, :jsonb, default: {}, null: false
    add_column :booking_groups, :booked_from, :string, default: ""
  end

  def down
    change_column :bookings, :price, :integer
    remove_column :itineraries, :including, :jsonb, default: {}, null: false
    remove_column :booking_groups, :booked_from, :string, default: ""
  end
  
end

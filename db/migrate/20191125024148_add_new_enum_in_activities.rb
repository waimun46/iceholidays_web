class AddNewEnumInActivities < ActiveRecord::Migration[5.2]
  def up
    add_column :globaltix_attractions, :category, :integer, default: 0
    add_column :activities, :category, :integer, default: 0
  end
  def down
    remove_column :globaltix_attractions, :category, :integer
    remove_column :activities, :category, :integer
  end
end

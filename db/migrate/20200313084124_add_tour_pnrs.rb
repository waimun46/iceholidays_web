class AddTourPnrs < ActiveRecord::Migration[5.2]
  def change
    add_column :tours, :pnrs , :string, array: true
  end
end

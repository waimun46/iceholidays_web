class AddInvoiceModule < ActiveRecord::Migration[5.1]
  def change
    create_table :invoices do |t|
      t.string :description
      t.integer :charged_booking_ids, array: true
      t.integer :total_charge, default: 0
      t.integer :refunded_booking_ids, array: true
      t.integer :total_refund, default: 0
      t.integer :last_invoice_id
      t.integer :outstanding, default: 0
      t.integer :balance, default: 0

      t.timestamps
    end

    add_column :bookings, :charged, :boolean, default: false
    add_column :bookings, :charged_at, :datetime, array: true
    add_column :bookings, :refunded_at, :datetime, array: true
  end
end

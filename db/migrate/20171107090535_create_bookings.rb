class CreateBookings < ActiveRecord::Migration[5.1]
  def up
    create_table :bookings do |t|
      t.references :booking_group
      t.integer :category, default: 0
      t.integer :price
      t.string :price_type
      t.boolean :price_offered, default: false
      t.boolean :rebate, default: false
      t.string :designation
      t.string :name
      t.integer :gender
      t.string :mobile
      t.string :passport_number
      t.datetime :passport_expiry_date
      t.string :passport_issuing_place
      t.string :passport_photocopy
      t.datetime :date_of_birth
      t.string :passport_received, default: ""
      t.datetime :pp_return_date
      t.boolean :photo_received, default: false
      t.boolean :own_visa, default: false
      t.string :visa_no, default: ""
      t.string :room_type, default: ""
      t.integer :extra_bed, default: 0
      t.string :remark, default: ""
      t.string :fp_ref, default: ""
      t.boolean :agent_confirmation, default: false
      t.integer :agent_user_id
      t.string :sale_rep
      t.integer :initiator_user_id
      t.integer :status, default: 0
      t.references :tour
      t.integer :booking_alteration_id
      t.integer :last_edited_user_id
      t.datetime :last_edited_at
      t.jsonb :exported_details, null: false, default: {}

      t.timestamps
    end
    execute "SELECT setval('bookings_id_seq', 200000)"
  end
end

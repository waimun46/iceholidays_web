class AddAdvanceBookingDaysToActivities < ActiveRecord::Migration[5.2]
  def up
    add_column :activities, :advance_booking_days, :integer, default: 0
  end

  def down
    remove_column :activities, :advance_booking_days, :integer, default: 0
  end
end

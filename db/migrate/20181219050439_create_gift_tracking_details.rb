class CreateGiftTrackingDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :gift_tracking_details do |t|
      t.references :gift_tracking
      t.references :gift
      t.integer :quantity
      t.integer :total_points

      t.timestamps
    end
  end
end

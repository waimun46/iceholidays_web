class AddGlobalAttractionsModule < ActiveRecord::Migration[5.1]
  def change
      create_table :globaltix_attractions do |t|
        t.integer :attraction_id
        t.string :country
        t.string :title
        t.string :description
        t.string :hours_of_operation
        t.string :term_and_condition
        t.integer :cancellation
        t.integer :e_ticketing
        t.integer :travel_date_type
        t.string :duration
        t.integer :ticket_type
        t.string :languages, array: true, default: '{}'
        t.integer :tour_guide
        t.integer :meeting_point
        t.timestamps
      end

      add_column :globaltix_tickets, :globaltix_attraction_id, :integer
      add_column :activities, :cancellation, :integer
      add_column :activities, :e_ticketing, :integer
      add_column :activities, :travel_date_type, :integer
      add_column :activities, :duration, :string
      add_column :activities, :ticket_type, :integer
      add_column :activities, :languages, :string, array: true, default: '{}'
      add_column :activities, :tour_guide, :integer
      add_column :activities, :meeting_point, :integer

      GlobaltixTicket.all.each do |ticket|
        globaltix_attraction = Globaltix::Attraction.find(ticket.attraction_id)

        db_attraction = GlobaltixAttraction.find_or_create_by(attraction_id: globaltix_attraction.id) do |attraction|
          attraction.attraction_id = globaltix_attraction.id
          attraction.country = globaltix_attraction.country
          attraction.title = globaltix_attraction.title
          attraction.description = globaltix_attraction.description
          attraction.hours_of_operation = globaltix_attraction.hours_of_operation
        end

        ticket.update_columns(globaltix_attraction_id: db_attraction.id)
      end

    reversible do |change|
      change.up do
          rename_column :activities, :operation_days, :operation_days_old
          rename_column :activities, :categories, :categories_old

          add_column :activities, :operation_days, :string, array: true, default: '{}'
          add_column :activities, :categories, :string, array: true, default: '{}'

          Activity.all.each do |activity|
            if activity.operation_days_old.present?
                activity.update_columns(operation_days: JSON.parse(activity.operation_days_old))
              end
            if activity.categories_old.present?
                activity.update_columns(categories: JSON.parse(activity.categories_old))
              end  
          end

          remove_column :activities, :operation_days_old
          remove_column :activities, :categories_old
        end
        
      change.down do
          rename_column :activities, :operation_days, :operation_days_old
          rename_column :activities, :categories, :categories_old

          add_column :activities, :operation_days, :jsonb, default: {}, null: false
          add_column :activities, :categories, :jsonb, default: {}, null: false

          Activity.all.each do |activity|
            if activity.operation_days_old.present?
              activity.update_columns(operation_days: (activity.operation_days_old.to_s))
            end
            if activity.categories_old.present?
              activity.update_columns(categories: (activity.categories_old.to_s))
            end
          end

          remove_column :activities, :operation_days_old
          remove_column :activities, :categories_old
        end
      end
    end

end

class AddBlockToAirlines < ActiveRecord::Migration[5.1]
  def change
    add_column :airlines, :block, :boolean, default: false
  end
end

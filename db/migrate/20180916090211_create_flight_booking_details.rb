class CreateFlightBookingDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :flight_booking_details do |t|
      t.integer :group
      t.string :carrier
      t.string :cabin_class
      t.string :flight_number
      t.string :provider_code
      t.string :origin
      t.string :origin_terminal
      t.string :destination
      t.string :destination_terminal
      t.datetime :departure_time
      t.datetime :arrival_time
      t.integer :travel_time
      t.string :class_of_service
      t.string :e_ticketability
      t.string :equipment
      t.string :status
      t.string :change_of_plane
      t.string :guaranteed_payment_carrier
      t.string :provider_reservation_info_ref
      t.string :travel_order
      t.string :provider_segment_order
      t.string :optional_services_indicator
      t.string :participant_level
      t.string :link_availability
      t.string :el_stat
      t.string :automated_checkin
      t.text :sell_message
      t.boolean :connection
      t.integer :flight_booking_group_id

      t.timestamps
    end
  end
end

class AddCreditRefund < ActiveRecord::Migration[5.2]
  def change
    create_table :credit_refunds do |t|
      t.integer :user_id
      t.integer :payment_id
      t.integer :status, default: 0
      t.decimal :amount
      t.decimal :credits
      t.string :remark
      t.timestamps
    end

    add_column :payments, :credit_refund_id, :integer
  end
end

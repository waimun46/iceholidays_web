class AddEtravelModule < ActiveRecord::Migration[5.2]
  def change
    create_table :etravel_invoices do |t|
      t.references :taggable, polymorphic: true
      t.references :payment

      t.timestamps
    end
  end
end

class AddVoucherDetailsToLandTour < ActiveRecord::Migration[5.2]
  def up
    add_column :land_tours, :supplier_details, :jsonb, default: {}
    add_column :land_tours, :emergency_contact, :jsonb, default: {}
    
    add_column :land_tour_bookings, :withdrawn_user_id, :integer
    add_column :land_tour_bookings, :withdrawn_at, :datetime

    add_column :land_tour_booking_groups, :hotel_name, :string
    add_column :land_tour_booking_groups, :signboard_text, :string
    add_column :land_tour_booking_groups, :special_request, :string, array: true
  end

  def down
    remove_column :land_tours, :supplier_details, :jsonb, default: {}
    remove_column :land_tours, :emergency_contact, :jsonb, default: {}

    remove_column :land_tour_bookings, :withdrawn_user_id, :integer
    remove_column :land_tour_bookings, :withdrawn_at, :datetime

    remove_column :land_tour_booking_groups, :hotel_name, :string
    remove_column :land_tour_booking_groups, :signboard_text, :string
    remove_column :land_tour_booking_groups, :special_request, :string, array: true
  end
end

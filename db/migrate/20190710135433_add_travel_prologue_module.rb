class AddTravelPrologueModule < ActiveRecord::Migration[5.1]
  def change
    create_table :hotel_booking_groups do |t|
      t.string :avail_token
      t.string :unique_txn_id
      t.string :amount
      t.string :booking_status
      t.string :booking_id
      t.string :error_msg
      t.string :product_type
      t.string :booking_ref
      t.string :product_booking_id
      t.string :payment_id
      t.string :payment_type
      t.string :card_number
      t.string :expiry_date
      t.string :transaction_id
      t.string :source_application
      t.string :agent_id
      t.string :paymenttypemsg
      t.string :trans_pk
      t.string :provider
      t.string :check_in
      t.string :check_out
      t.jsonb :hotel_details, null: false, default: {}
      t.jsonb :customer_details, null: false, default: {}

      t.timestamps
    end

    create_table :hotel_bookings do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :country_code
      t.string :phone
      t.string :phone_number
      t.string :traveller_type
      t.string :salutation
      t.string :nationality
      t.string :in_room_no
      t.references :hotel_booking_group, foreign_key: true

      t.timestamps
    end

    create_table :hotels do |t|
      t.string :tp_hotel_id
      t.string  :hotel_name
      t.integer :hotel_source
      t.string  :city_name
      t.integer :markup_category
      t.decimal  :markup

      t.timestamps
    end

    create_table :cancelled_hotel_booking_groups do |t|
      t.string :cancellation_number
      t.string  :message
      t.integer :status
      t.references :hotel_booking_group, foreign_key: true

      t.timestamps
    end
  end
end

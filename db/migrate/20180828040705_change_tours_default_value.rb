class ChangeToursDefaultValue < ActiveRecord::Migration[5.1]
  def up
    change_column :tours, :max_booking_seats, :integer, default: 6
    change_column :tours, :guaranteed_departure_flag, :boolean, default: true
  end
end
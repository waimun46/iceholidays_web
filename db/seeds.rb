# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Autocomplete.create(name: 'contient_country')
Autocomplete.create(name: 'state', keywords:['Johor', 'Kedah', 'Kelantan', 'Melaka', 'Negeri Sembilan', 'Pahang', 'Perak', 'Perlis', 'Pulau Pinang', 'Sabah', 'Sarawak', 'Selangor', 'Terengganu', 'W.P. Kuala Lumpur'])


users_seed = "#{Rails.root}/lib/records/users_seed.txt"
lines = File.readlines(users_seed)
lines.each do |line|
  eval line
end


tours_error_file = File.open("./tours_input_error.txt", 'a')
lines = File.readlines("#{Rails.root}/lib/records/tours_seed.txt")
lines.each do |line|
  t = eval line
  unless t.errors.messages.blank?
    tours_error_file << ["#{t.code}", "#{t.errors.messages}"]
  end
end
tours_error_file.close


bookings_error_file = File.open("./booking_input_error.txt", 'a')
bg = nil
lines = File.readlines("#{Rails.root}/lib/records/bookings_seed.txt")
lines.each do |line|
  b = eval line
  if (b.class.to_s == 'BookingGroup' || b.class.to_s == 'Booking') && b.errors.present? && b.errors.messages.present?
    bookings_error_file.puts ["#{bg.code}", b.id.present? ? b.id : '' ,"#{b.errors.messages}"]
  end
end
bookings_error_file.close

bookings_error_file = File.open("./cancellation_input_error.txt", 'a')
bg = nil
ba = nil
b = nil
lines = File.readlines("#{Rails.root}/lib/records/cancellations_seed.txt")

lines.each do |line|
  begin
    b = eval line    
  rescue ActiveRecord::RecordNotUnique
    ret = line.match /id\:\s\"(\d+)\"/
    new_line1 = "Booking.find(#{ret[1]}).update_columns(booking_alteration_id: #{ba.id}, booking_group_id: #{bg.id})"
    new_line2 = "ba.update_column(:original_booking_group_id, Booking.find(#{ret[1]}).booking_group_id)"
    eval new_line1
    eval new_line2
  end
  
  if b.class.to_s == 'BookingAlteration'
    ba = b
  end
  if b.class.to_s == 'BookingGroup'
    bg = b
  end

  if (b.class.to_s == 'BookingGroup' || b.class.to_s == 'Booking' || b.class.to_s == 'BookingAlteration') && b.errors.present? && b.errors.messages.present?
    bookings_error_file.puts ["#{bg.code}", b.id.present? ? b.id : '' ,"#{b.errors.messages}"]
  end
end


transferreds_error_file = File.open("./transferred_input_error.txt", "a")
lines = File.readlines("#{Rails.root}/lib/records/transferreds_seed_data.txt")
lines.each do |line|
  old_tour_id = amount = reason = created_user_id = created_at = exported_user_id = exported_at = exported_count = booking_ids =  nil

  col = line.split("\t").map{|x| x.gsub("\n", '')}
  old_tour_id = Tour.find_by_code(col[0]).try(&:id)
  amount = col[1].to_i
  reason = col[2]
  if col[3].present?
    created_user_id = User.find_by_alias_name(col[3]).try(&:id)
  end
  created_at = col[4]
  if col[5].present? && col[6].present? && col[7].present?
    exported_user_id = User.find_by_alias_name(col[5]).try(&:id)
    exported_at = col[6]
    exported_count = col[7].to_i
  end
  booking_ids = col[8..-1].map(&:to_i)
  if booking_ids.present?
    ba = BookingAlteration.create(alter_type: :transferred, reason: reason, original_tour_id: old_tour_id, amount: amount, created_user_id: created_user_id, created_at: created_at, exported_user_id: exported_user_id, exported_at: exported_at, exported_count: exported_count)
    Booking.where(id: booking_ids).each do |b|
      if b.booking_alteration.blank?
        b.update_columns(booking_alteration_id: ba.id)
      else
        ba_pointer = b.booking_alteration
        while ba_pointer.prev.present?
          ba_pointer = ba_pointer.prev
        end
        if ba_pointer != ba
          ba_pointer.update_columns(prev_id: ba.id)
        end
      end
    end
  end
end


lines = File.readlines("#{Rails.root}/lib/records/tours_itinerary_seed.txt")
ht = {}
(0..lines.count/2-1).each do |i|
  k = lines[i*2][23..-4]
  v = lines[i*2+1][32..-4]

  if v[-4..-1] == '.doc'
    next
  end
  if v[-3..-1] == '.pd' || v[-3..-1] == '.df'
    v[-3..-1] = ".pdf"
  end
  if v[0..13] == "http://http://"
    v[0..13] = "http://"
  end
  ht[k] = v
end

ht.values.uniq.each do |url|
  Itinerary.create_by_url(url)
end

ht.each do |k, v|
  i = Itinerary.find_by_url_source(v)
  if i.present?
    Tour.find_by_code(k).update_column(:itinerary_id, i.id)
  end
end


# Itinerary.all.each do |i|
#   i.remote_file_url = i.url_source
#   i.save
# end


bg_u = []
BookingGroup.where.not(exported_remark: nil).each do |bg|
  count = 1
  ret = bg.exported_remark.match /(?:exported\son\s)([\w\/\s\(\)\:\-]+)(?:,?\sby\s)([\w\s]+)(?:\s\#)?(\d)?/i
  if ret.length == 4
    if ret[3].present?
      count = ret[3].to_i
    end
    if bg.exported_remark[10..20] == "Exported on"
      date = DateTime.strptime(ret[1], "%d/%m/%y (%a) %H:%M")
    else
      date = DateTime.parse(ret[1])
    end
    bg.update_columns(exported_user_id: User.find_by_alias_name(ret[2].strip).try(:id), exported_at: date, exported_count: count)
  else
    bg_u << bg.id
  end
end ; 0

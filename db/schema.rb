# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_27_060445) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.string "title"
    t.string "country"
    t.tsrange "booking_date"
    t.tsrange "travelling_date"
    t.string "highlight"
    t.text "description"
    t.text "terms_and_conditions"
    t.integer "max_booking"
    t.integer "min_booking"
    t.integer "status", default: 0
    t.string "supplier"
    t.boolean "gen_code", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "cancellation"
    t.integer "e_ticketing"
    t.integer "travel_date_type"
    t.string "duration"
    t.integer "ticket_type"
    t.string "languages", default: [], array: true
    t.integer "transfer"
    t.integer "meeting_point"
    t.string "operation_days", default: [], array: true
    t.string "categories", default: [], array: true
    t.integer "confirmation_time"
    t.integer "category", default: 0
    t.integer "advance_booking_days", default: 0
  end

  create_table "activity_booking_groups", force: :cascade do |t|
    t.string "remark"
    t.integer "payment_method"
    t.datetime "travel_date"
    t.integer "last_edited_user_id"
    t.datetime "last_edited_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "price"
  end

  create_table "activity_bookings", force: :cascade do |t|
    t.bigint "activity_booking_group_id"
    t.bigint "activity_id"
    t.string "code"
    t.string "name"
    t.string "mobile"
    t.string "email"
    t.integer "quantity"
    t.decimal "price"
    t.string "category"
    t.string "remark"
    t.integer "status", default: 0
    t.integer "agent_user_id"
    t.integer "last_edited_user_id"
    t.datetime "last_edited_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "price_breakdown"
    t.index ["activity_booking_group_id"], name: "index_activity_bookings_on_activity_booking_group_id"
    t.index ["activity_id"], name: "index_activity_bookings_on_activity_id"
  end

  create_table "activity_prices", force: :cascade do |t|
    t.bigint "activity_id"
    t.string "category"
    t.integer "cost_price"
    t.integer "price"
    t.string "code_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.daterange "blockout_dates", array: true
    t.string "terms_and_conditions", default: ""
    t.string "description", default: ""
    t.index ["activity_id"], name: "index_activity_prices_on_activity_id"
  end

  create_table "agencies", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "full_name"
    t.string "other_name"
    t.string "alias_name"
    t.string "address"
    t.string "city"
    t.string "state"
    t.string "postal"
    t.string "country"
    t.string "phones", array: true
    t.string "faxes", array: true
    t.string "emails", array: true
    t.string "website"
    t.string "remark"
    t.integer "max_users", default: 0
    t.string "domains", default: [], array: true
    t.string "logo"
    t.string "registration_no"
    t.decimal "lat", precision: 15, scale: 10
    t.decimal "lng", precision: 15, scale: 10
    t.integer "category"
    t.integer "max_sign_in", default: 0
    t.boolean "is_active", default: true
  end

  create_table "aircrafts", force: :cascade do |t|
    t.string "iata_code"
    t.string "manufacturer"
    t.string "model"
    t.string "iata_group"
    t.string "iata_category"
    t.string "icao_code"
    t.string "nb_engines"
    t.string "aircraft_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "airlines", force: :cascade do |t|
    t.string "name"
    t.string "iata_code"
    t.string "country"
    t.string "logo"
    t.jsonb "markup_categories", default: {}, null: false
    t.jsonb "markups", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "block", default: false
  end

  create_table "autocompletes", force: :cascade do |t|
    t.string "name"
    t.jsonb "keywords", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_autocompletes_on_name"
  end

  create_table "booking_alterations", force: :cascade do |t|
    t.integer "original_tour_id"
    t.integer "original_booking_group_id"
    t.integer "alter_type"
    t.string "reason"
    t.integer "amount"
    t.boolean "whole_group", default: false
    t.integer "prev_id"
    t.integer "created_user_id"
    t.integer "exported_user_id"
    t.datetime "exported_at"
    t.integer "exported_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "restore_withdrawn_user_id"
    t.datetime "restore_withdrawn_at"
  end

  create_table "booking_cruise_rooms", force: :cascade do |t|
    t.string "room_price_type"
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "price_type"
    t.string "room_type"
  end

  create_table "booking_groups", force: :cascade do |t|
    t.string "code"
    t.integer "deposit", default: 0
    t.integer "received_payment", default: 0
    t.boolean "keep_in_view", default: true
    t.string "remark"
    t.string "exported_remark"
    t.integer "last_edited_user_id"
    t.datetime "last_edited_at"
    t.integer "exported_user_id"
    t.datetime "exported_at"
    t.integer "exported_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "insurance_receiver"
    t.jsonb "proforma_invoice_details"
    t.string "booked_from", default: ""
    t.integer "redemption_points"
  end

  create_table "booking_rooms", force: :cascade do |t|
    t.string "room_type"
    t.integer "tour_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "tour_leader_id"
  end

  create_table "bookings", force: :cascade do |t|
    t.bigint "booking_group_id"
    t.integer "category", default: 0
    t.decimal "price"
    t.string "price_type"
    t.boolean "price_offered", default: false
    t.boolean "rebate", default: false
    t.string "designation"
    t.string "name"
    t.integer "gender"
    t.string "mobile"
    t.string "passport_number"
    t.datetime "passport_expiry_date"
    t.string "passport_issuing_place"
    t.string "passport_photocopy"
    t.datetime "date_of_birth"
    t.string "passport_received", default: ""
    t.datetime "pp_return_date"
    t.boolean "photo_received", default: false
    t.boolean "own_visa", default: false
    t.string "visa_no", default: ""
    t.string "room_type", default: ""
    t.integer "extra_bed", default: 0
    t.string "remark", default: ""
    t.string "fp_ref", default: ""
    t.boolean "agent_confirmation", default: false
    t.integer "agent_user_id"
    t.string "sale_rep"
    t.integer "initiator_user_id"
    t.integer "status", default: 0
    t.bigint "tour_id"
    t.integer "booking_alteration_id"
    t.integer "last_edited_user_id"
    t.datetime "last_edited_at"
    t.jsonb "exported_details", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "booking_cruise_room_id"
    t.string "admin_remark", default: ""
    t.string "sub_sale_rep"
    t.boolean "charged", default: false
    t.datetime "charged_at", array: true
    t.datetime "refunded_at", array: true
    t.string "insurance_cert"
    t.jsonb "insurance_export_remark", default: {}
    t.integer "insurance_exporter_id"
    t.datetime "insurance_exported_at"
    t.string "nationality"
    t.boolean "insurance_confirmation"
    t.boolean "insurance_exported", default: false
    t.integer "guardian_id"
    t.jsonb "policy_holder", default: {}
    t.string "policy_number"
    t.integer "insurance_endorse_count"
    t.string "restore_withdrawn_alteration_id", array: true
    t.boolean "insurance_nomination_flag", default: true
    t.jsonb "insurance_nomination", default: {}
    t.jsonb "witness_details", default: {}
    t.integer "booking_room_id"
    t.jsonb "price_breakdown"
    t.string "operator_remark", default: ""
    t.boolean "in_fair", default: false
    t.decimal "original_price"
    t.datetime "visa_expiry_date"
    t.index ["booking_group_id"], name: "index_bookings_on_booking_group_id"
    t.index ["tour_id"], name: "index_bookings_on_tour_id"
  end

  create_table "brb_booking_groups", force: :cascade do |t|
    t.decimal "price"
    t.string "service_number"
    t.string "product_code"
    t.boolean "is_international"
    t.string "status_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "brb_bookings", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.integer "agent_user_id"
    t.string "sale_rep"
    t.integer "initiator_user_id"
    t.integer "flight_booking_id"
    t.integer "brb_booking_group_id"
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cancelled_hotel_booking_groups", force: :cascade do |t|
    t.string "cancellation_number"
    t.string "message"
    t.integer "status"
    t.bigint "hotel_booking_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hotel_booking_group_id"], name: "index_cancelled_hotel_booking_groups_on_hotel_booking_group_id"
  end

  create_table "cities", force: :cascade do |t|
    t.string "name"
    t.string "state"
    t.string "country"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "covers", force: :cascade do |t|
    t.string "image"
    t.string "taggable_type"
    t.bigint "taggable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "priority"
    t.integer "category", default: 0
    t.index ["taggable_type", "taggable_id"], name: "index_covers_on_taggable_type_and_taggable_id"
  end

  create_table "credit_purchases", force: :cascade do |t|
    t.integer "user_id"
    t.integer "status", default: 0
    t.decimal "amount"
    t.decimal "credits"
    t.decimal "extra_credits"
    t.string "remark"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "credit_refunds", force: :cascade do |t|
    t.integer "user_id"
    t.integer "payment_id"
    t.integer "status", default: 0
    t.decimal "amount"
    t.decimal "credits"
    t.string "remark"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "credit_transactions", force: :cascade do |t|
    t.string "taggable_type"
    t.bigint "taggable_id"
    t.integer "user_id"
    t.integer "prev_id"
    t.decimal "amount"
    t.decimal "user_balance"
    t.string "checksum"
    t.string "remark"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["taggable_type", "taggable_id"], name: "index_credit_transactions_on_taggable_type_and_taggable_id"
  end

  create_table "departments", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "phone"
    t.text "important_note"
    t.string "logo"
    t.text "description"
    t.text "fs_important_note"
    t.string "fs_logo"
    t.text "fs_description"
    t.string "title"
  end

  create_table "etravel_invoices", force: :cascade do |t|
    t.string "taggable_type"
    t.bigint "taggable_id"
    t.bigint "payment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "exported_at", array: true
    t.index ["payment_id"], name: "index_etravel_invoices_on_payment_id"
    t.index ["taggable_type", "taggable_id"], name: "index_etravel_invoices_on_taggable_type_and_taggable_id"
  end

  create_table "exchange_rates", force: :cascade do |t|
    t.string "from_currency"
    t.string "to_currency"
    t.decimal "rate", default: "0.0"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fairs", force: :cascade do |t|
    t.string "code"
    t.string "title"
    t.string "other_title"
    t.tsrange "duration"
    t.string "fair_type"
    t.string "remark"
    t.boolean "active", default: false
    t.string "messages"
    t.string "internal_reference"
    t.integer "allow_user_ids", array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "allow_itinerary_ids", array: true
  end

  create_table "flight_booking_details", force: :cascade do |t|
    t.integer "group"
    t.string "carrier"
    t.string "cabin_class"
    t.string "flight_number"
    t.string "provider_code"
    t.string "origin"
    t.string "origin_terminal"
    t.string "destination"
    t.string "destination_terminal"
    t.datetime "departure_time"
    t.datetime "arrival_time"
    t.integer "travel_time"
    t.string "class_of_service"
    t.string "e_ticketability"
    t.string "equipment"
    t.string "status"
    t.string "change_of_plane"
    t.string "guaranteed_payment_carrier"
    t.string "provider_reservation_info_ref"
    t.string "travel_order"
    t.string "provider_segment_order"
    t.string "optional_services_indicator"
    t.string "participant_level"
    t.string "link_availability"
    t.string "el_stat"
    t.string "automated_checkin"
    t.text "sell_message"
    t.boolean "connection"
    t.integer "flight_booking_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "flight_booking_groups", force: :cascade do |t|
    t.string "universal_code"
    t.string "host_code"
    t.string "uAPI_code"
    t.decimal "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "payment_method"
  end

  create_table "flight_bookings", force: :cascade do |t|
    t.string "designation"
    t.string "first_name"
    t.string "last_name"
    t.string "country"
    t.string "mobile"
    t.string "passport_number"
    t.date "passport_issue_date"
    t.date "passport_expiry_date"
    t.date "date_of_birth"
    t.string "category"
    t.integer "status", default: 0
    t.integer "flight_booking_group_id"
    t.integer "agent_user_id"
    t.string "sale_rep"
    t.integer "initiator_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "remark"
    t.decimal "price"
    t.jsonb "price_breakdown"
  end

  create_table "flight_commissions", force: :cascade do |t|
    t.string "name"
    t.string "airline_iata"
    t.string "origin_airport_iata"
    t.string "destination_airport_iata"
    t.decimal "commission", default: "0.0"
    t.string "commission_category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "flight_details", force: :cascade do |t|
    t.string "airline_iata"
    t.string "flight_no"
    t.datetime "departure"
    t.datetime "arrival"
    t.string "origin"
    t.string "destination"
    t.bigint "flight_inventory_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "flight_inventories", force: :cascade do |t|
    t.string "title"
    t.date "purchased_at"
    t.string "purchased_by"
    t.integer "materialized_rate"
    t.string "remark"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "flight_receipts", force: :cascade do |t|
    t.string "number"
    t.integer "amount"
    t.string "remark"
    t.bigint "flight_seat_id"
    t.datetime "paid_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "flight_seats", force: :cascade do |t|
    t.integer "total_seat"
    t.string "pnr"
    t.integer "price"
    t.integer "tax"
    t.integer "fee"
    t.integer "bag_seat_fee"
    t.string "remark"
    t.bigint "flight_inventory_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gift_stocks", force: :cascade do |t|
    t.bigint "gift_id"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gift_id"], name: "index_gift_stocks_on_gift_id"
  end

  create_table "gift_tracking_details", force: :cascade do |t|
    t.bigint "gift_tracking_id"
    t.bigint "gift_id"
    t.integer "quantity"
    t.integer "total_points"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gift_id"], name: "index_gift_tracking_details_on_gift_id"
    t.index ["gift_tracking_id"], name: "index_gift_tracking_details_on_gift_tracking_id"
  end

  create_table "gift_tracking_groups", force: :cascade do |t|
    t.string "remark"
    t.integer "agent_id"
    t.datetime "cut_off_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "category"
    t.jsonb "status_detail", default: {}
    t.integer "status", default: 0
  end

  create_table "gift_trackings", force: :cascade do |t|
    t.bigint "gift_tracking_group_id"
    t.integer "point_topup"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "booking_group_id"
    t.index ["gift_tracking_group_id"], name: "index_gift_trackings_on_gift_tracking_group_id"
  end

  create_table "gifts", force: :cascade do |t|
    t.string "title"
    t.string "color"
    t.string "image"
    t.integer "points"
    t.string "sku"
    t.integer "threshold", default: 0
    t.integer "balance", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status", default: 0
    t.integer "price"
    t.string "description"
  end

  create_table "globaltix_attractions", force: :cascade do |t|
    t.integer "attraction_id"
    t.string "country"
    t.string "title"
    t.string "description"
    t.string "hours_of_operation"
    t.string "term_and_condition"
    t.integer "cancellation"
    t.integer "e_ticketing"
    t.integer "travel_date_type"
    t.string "duration"
    t.integer "ticket_type"
    t.string "languages", default: [], array: true
    t.integer "transfer"
    t.integer "meeting_point"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "confirmation_time"
    t.tsrange "travelling_date"
    t.integer "category", default: 0
  end

  create_table "globaltix_ticket_booking_groups", force: :cascade do |t|
    t.decimal "price"
    t.string "orignal_price"
    t.string "reference_number"
    t.string "e_ticket_url"
    t.jsonb "detail"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "export_params"
    t.datetime "exported_at"
  end

  create_table "globaltix_ticket_bookings", force: :cascade do |t|
    t.string "email"
    t.string "name"
    t.integer "status", default: 0
    t.integer "globaltix_ticket_booking_group_id"
    t.integer "globaltix_ticket_booking_group_detail_id"
    t.integer "globaltix_ticket_id"
    t.integer "agent_user_id"
    t.integer "sale_rep"
    t.string "initiator_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "price"
    t.jsonb "price_breakdown"
  end

  create_table "globaltix_tickets", force: :cascade do |t|
    t.integer "attraction_id"
    t.integer "ticket_id"
    t.integer "markup_category"
    t.decimal "markup"
    t.string "country"
    t.string "title"
    t.jsonb "details", default: {}, null: false
    t.string "remark"
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "globaltix_attraction_id"
    t.integer "priority", default: 0
    t.daterange "blockout_dates", array: true
    t.string "terms_and_conditions", default: ""
    t.string "description", default: ""
  end

  create_table "holiday_calendars", force: :cascade do |t|
    t.string "name"
    t.tsrange "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "holidays", force: :cascade do |t|
    t.integer "year"
    t.date "dates", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hotel_booking_groups", force: :cascade do |t|
    t.string "avail_token"
    t.string "unique_txn_id"
    t.string "amount"
    t.string "booking_status"
    t.string "booking_id"
    t.string "error_msg"
    t.string "product_type"
    t.string "booking_ref"
    t.string "product_booking_id"
    t.string "payment_id"
    t.string "payment_type"
    t.string "card_number"
    t.string "expiry_date"
    t.string "transaction_id"
    t.string "source_application"
    t.string "agent_id"
    t.string "paymenttypemsg"
    t.string "trans_pk"
    t.string "provider"
    t.string "check_in"
    t.string "check_out"
    t.jsonb "hotel_details", default: {}, null: false
    t.jsonb "customer_details", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hotel_bookings", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "country_code"
    t.string "phone"
    t.string "phone_number"
    t.string "traveller_type"
    t.string "salutation"
    t.string "nationality"
    t.string "in_room_no"
    t.bigint "hotel_booking_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hotel_booking_group_id"], name: "index_hotel_bookings_on_hotel_booking_group_id"
  end

  create_table "hotels", force: :cascade do |t|
    t.string "tp_hotel_id"
    t.string "hotel_name"
    t.integer "hotel_source"
    t.string "city_name"
    t.integer "markup_category"
    t.decimal "markup"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "infant_bookings", force: :cascade do |t|
    t.bigint "booking_id"
    t.string "name"
    t.integer "gender"
    t.string "passport_number"
    t.datetime "passport_expiry_date"
    t.string "passport_issuing_place"
    t.string "passport_photocopy"
    t.datetime "date_of_birth"
    t.string "passport_received", default: ""
    t.datetime "pp_return_date"
    t.boolean "photo_received", default: false
    t.boolean "own_visa", default: false
    t.string "visa_no", default: ""
    t.string "remark", default: ""
    t.string "fp_ref", default: ""
    t.boolean "agent_confirmation", default: false
    t.integer "agent_user_id"
    t.integer "initiator_user_id"
    t.integer "status", default: 0
    t.integer "last_edited_user_id"
    t.datetime "last_edited_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "insurance_cert"
    t.jsonb "insurance_export_remark", default: {}
    t.integer "insurance_exporter_id"
    t.datetime "insurance_exported_at"
    t.boolean "insurance_confirmation"
    t.boolean "insurance_exported", default: false
    t.string "nationality"
    t.integer "parent_id"
    t.jsonb "policy_holder", default: {}
    t.string "policy_number"
    t.integer "insurance_endorse_count"
    t.boolean "insurance_nomination_flag", default: true
    t.jsonb "insurance_nomination", default: {}
    t.jsonb "witness_details", default: {}
    t.index ["booking_id"], name: "index_infant_bookings_on_booking_id"
  end

  create_table "insurance_rebates", force: :cascade do |t|
    t.integer "zone"
    t.int4range "days_duration"
    t.integer "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "invoices", force: :cascade do |t|
    t.string "description"
    t.integer "charged_booking_ids", array: true
    t.integer "total_charge", default: 0
    t.integer "refunded_booking_ids", array: true
    t.integer "total_refund", default: 0
    t.integer "last_invoice_id"
    t.integer "outstanding", default: 0
    t.integer "balance", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "itineraries", force: :cascade do |t|
    t.string "code"
    t.string "file"
    t.text "remark"
    t.string "url_source"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "editable_file"
    t.string "en_editable_file"
    t.string "cn_editable_file"
    t.string "countries", array: true
    t.string "duration"
    t.string "caption"
    t.string "other_caption"
    t.text "description"
    t.text "additional_tour"
    t.text "optional_tour"
    t.string "category"
    t.integer "old_department_enum"
    t.integer "status", default: 0
    t.jsonb "including", default: {}, null: false
    t.string "zone"
    t.integer "department_id"
    t.string "cn_file"
    t.int4range "temperature"
    t.string "currency_english", array: true
    t.string "currency_chinese", array: true
    t.string "time_difference_english"
    t.string "time_difference_chinese"
    t.string "voltage"
    t.string "tour_confirmation_remarks"
  end

  create_table "land_tour_activities", force: :cascade do |t|
    t.bigint "land_tour_day_id"
    t.string "taggable_type"
    t.bigint "taggable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["land_tour_day_id"], name: "index_land_tour_activities_on_land_tour_day_id"
    t.index ["taggable_type", "taggable_id"], name: "index_land_tour_activities_on_taggable_type_and_taggable_id"
  end

  create_table "land_tour_booking_groups", force: :cascade do |t|
    t.bigint "land_tour_category_id"
    t.string "code"
    t.string "remark"
    t.decimal "total_price"
    t.decimal "deposit", default: "0.0"
    t.decimal "received_payment", default: "0.0"
    t.datetime "departure_date"
    t.jsonb "price_breakdown", default: {}
    t.string "rooms"
    t.string "language"
    t.jsonb "flight_departure", default: {}
    t.jsonb "flight_return", default: {}
    t.integer "last_edited_user_id"
    t.datetime "last_edited_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "hotel_name"
    t.string "signboard_text"
    t.string "special_request", array: true
    t.index ["land_tour_category_id"], name: "index_land_tour_booking_groups_on_land_tour_category_id"
  end

  create_table "land_tour_bookings", force: :cascade do |t|
    t.bigint "land_tour_booking_group_id"
    t.bigint "land_tour_id"
    t.string "designation"
    t.string "name"
    t.string "mobile"
    t.datetime "date_of_birth"
    t.string "passport_number"
    t.string "email"
    t.string "nationality"
    t.integer "quantity"
    t.decimal "price"
    t.string "category"
    t.string "remark"
    t.jsonb "price_breakdown", default: {}
    t.integer "status", default: 0
    t.integer "agent_user_id"
    t.integer "last_edited_user_id"
    t.datetime "last_edited_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "withdrawn_user_id"
    t.datetime "withdrawn_at"
    t.index ["land_tour_booking_group_id"], name: "index_land_tour_bookings_on_land_tour_booking_group_id"
    t.index ["land_tour_id"], name: "index_land_tour_bookings_on_land_tour_id"
  end

  create_table "land_tour_categories", force: :cascade do |t|
    t.bigint "land_tour_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["land_tour_id"], name: "index_land_tour_categories_on_land_tour_id"
  end

  create_table "land_tour_cities", force: :cascade do |t|
    t.bigint "land_tour_day_id"
    t.bigint "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_land_tour_cities_on_city_id"
    t.index ["land_tour_day_id"], name: "index_land_tour_cities_on_land_tour_day_id"
  end

  create_table "land_tour_days", force: :cascade do |t|
    t.bigint "land_tour_id"
    t.integer "day_no"
    t.string "title"
    t.text "description"
    t.jsonb "inclusion", default: {}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["land_tour_id"], name: "index_land_tour_days_on_land_tour_id"
  end

  create_table "land_tour_prices", force: :cascade do |t|
    t.bigint "land_tour_category_id"
    t.daterange "dates", array: true
    t.jsonb "prices"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["land_tour_category_id"], name: "index_land_tour_prices_on_land_tour_category_id"
  end

  create_table "land_tours", force: :cascade do |t|
    t.string "code"
    t.string "title"
    t.string "cn_title"
    t.tsrange "travel_date"
    t.integer "category"
    t.integer "max_booking"
    t.integer "min_booking"
    t.string "highlight"
    t.text "description"
    t.text "cn_description"
    t.text "terms_and_conditions"
    t.string "guide_languages", array: true
    t.daterange "blockout_dates", array: true
    t.integer "cut_off_day"
    t.jsonb "compulsory_charges"
    t.jsonb "child_prices"
    t.jsonb "dta_prices"
    t.integer "free_of_charge"
    t.integer "deposit"
    t.integer "deposit_type", default: 0
    t.integer "single_supplement_price", default: 0
    t.jsonb "inclusion", default: {}
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "depart_day", array: true
    t.jsonb "supplier_details", default: {}
    t.jsonb "emergency_contact", default: {}
  end

  create_table "payments", force: :cascade do |t|
    t.string "taggable_type"
    t.bigint "taggable_id"
    t.integer "user_id"
    t.decimal "amount"
    t.decimal "net_amount"
    t.string "method"
    t.integer "gateway"
    t.integer "callback"
    t.string "redirect_url"
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "transaction_no"
    t.integer "credit_refund_id"
    t.string "action_remark"
    t.index ["taggable_type", "taggable_id"], name: "index_payments_on_taggable_type_and_taggable_id"
  end

  create_table "pre_commisions", force: :cascade do |t|
    t.integer "allow_user_ids", default: [], array: true
    t.integer "allow_itinerary_ids", default: [], array: true
    t.string "code"
    t.string "title"
    t.string "remark"
    t.tsrange "duration"
    t.integer "commisions"
    t.boolean "active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "commision_category"
  end

  create_table "receipts", force: :cascade do |t|
    t.string "number"
    t.integer "amount"
    t.string "remark"
    t.bigint "booking_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["booking_group_id"], name: "index_receipts_on_booking_group_id"
  end

  create_table "reminders", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.date "reminder_date"
    t.string "email"
    t.bigint "taggable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "taggable_type"
    t.index ["taggable_id"], name: "index_reminders_on_taggable_id"
  end

  create_table "roamingman_booking_groups", force: :cascade do |t|
    t.decimal "price"
    t.integer "deposit_mode", default: 1
    t.integer "deposit_amount", default: 0
    t.string "remark"
    t.integer "last_edited_user_id"
    t.datetime "last_edited_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roamingman_bookings", force: :cascade do |t|
    t.bigint "roamingman_booking_group_id"
    t.string "oms_order_id"
    t.string "name"
    t.string "mobile"
    t.string "email"
    t.string "address"
    t.string "postcode"
    t.string "country_code", default: "MYS"
    t.date "depart_date"
    t.date "return_date"
    t.integer "ship_way"
    t.integer "quantity"
    t.string "deliver_point_id"
    t.string "return_point_id"
    t.string "is_send_sms"
    t.string "is_send_email"
    t.integer "status", default: 0
    t.string "remark", default: ""
    t.integer "agent_user_id"
    t.datetime "last_edited_at"
    t.integer "last_edited_user_id"
    t.integer "export_status", default: 0
    t.string "exported_remark"
    t.integer "exporter_id"
    t.datetime "exported_at"
    t.integer "cancel_user_id"
    t.datetime "cancelled_at"
    t.jsonb "cancelled_details", default: {}
    t.bigint "roamingman_package_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "price"
    t.jsonb "price_breakdown"
    t.index ["roamingman_booking_group_id"], name: "index_roamingman_bookings_on_roamingman_booking_group_id"
    t.index ["roamingman_package_id"], name: "index_roamingman_bookings_on_roamingman_package_id"
  end

  create_table "roamingman_packages", force: :cascade do |t|
    t.string "name"
    t.string "code"
    t.string "package_type"
    t.integer "category"
    t.string "countries", array: true
    t.integer "min_rent_days", default: 2
    t.string "data_rules"
    t.decimal "channel_price"
    t.decimal "retail_price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "highlight"
    t.text "terms_and_conditions"
    t.integer "status", default: 0
  end

  create_table "sessions", force: :cascade do |t|
    t.string "token", default: ""
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["token"], name: "index_sessions_on_token"
    t.index ["user_id"], name: "index_sessions_on_user_id"
  end

  create_table "settings", force: :cascade do |t|
    t.string "var", null: false
    t.text "value"
    t.integer "thing_id"
    t.string "thing_type", limit: 30
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true
  end

  create_table "slider_cards", force: :cascade do |t|
    t.integer "priority"
    t.string "cover"
    t.string "link"
    t.string "title1", default: ""
    t.string "subtitle1", default: ""
    t.string "highlight1", default: ""
    t.string "logo1", default: ""
    t.string "title2", default: ""
    t.string "subtitle2", default: ""
    t.string "highlight2", default: ""
    t.string "logo2", default: ""
    t.integer "slider_tab_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "slider_tabs", force: :cascade do |t|
    t.string "title"
    t.integer "priority"
    t.integer "slider_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sliders", force: :cascade do |t|
    t.integer "category"
    t.string "title"
    t.integer "priority"
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tbo_amendment_requests", force: :cascade do |t|
    t.bigint "tbo_booking_id"
    t.integer "status"
    t.string "remarks"
    t.string "amendment_message"
    t.jsonb "amendment_requested"
    t.jsonb "approval_information"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tbo_booking_id"], name: "index_tbo_amendment_requests_on_tbo_booking_id"
  end

  create_table "tbo_bookings", force: :cascade do |t|
    t.date "check_in"
    t.date "check_out"
    t.string "hotel_code"
    t.integer "booking_id"
    t.string "reference_no"
    t.string "confirmation_no"
    t.string "invoice_number"
    t.string "currency"
    t.decimal "original_total_fare", default: "0.0"
    t.decimal "markup", default: "0.0"
    t.decimal "total_fare", default: "0.0"
    t.integer "trip_id"
    t.integer "no_of_rooms", default: 0
    t.integer "no_of_guests", default: 0
    t.string "error_code"
    t.string "message"
    t.text "cancellation_policies"
    t.date "last_cancellation_deadline"
    t.decimal "refund_amount", default: "0.0"
    t.decimal "cancellation_charge", default: "0.0"
    t.jsonb "rooms", default: {}, null: false
    t.jsonb "address_info", default: {}, null: false
    t.integer "booking_status"
    t.boolean "voucher_status"
    t.integer "cancellation_status"
    t.integer "amendment_status"
    t.boolean "cancellation_requested", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "agent_user_id"
    t.string "remark"
    t.index ["booking_id"], name: "index_tbo_bookings_on_booking_id"
    t.index ["confirmation_no"], name: "index_tbo_bookings_on_confirmation_no"
    t.index ["hotel_code"], name: "index_tbo_bookings_on_hotel_code"
    t.index ["invoice_number"], name: "index_tbo_bookings_on_invoice_number"
    t.index ["reference_no"], name: "index_tbo_bookings_on_reference_no"
  end

  create_table "tbo_cities", primary_key: "code", id: :string, force: :cascade do |t|
    t.string "country_code"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country_code"], name: "index_tbo_cities_on_country_code"
  end

  create_table "tbo_countries", primary_key: "code", id: :string, force: :cascade do |t|
    t.string "name"
    t.boolean "is_active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tbo_guests", force: :cascade do |t|
    t.bigint "tbo_booking_id"
    t.boolean "lead_guest", default: false
    t.integer "guest_type", default: 0
    t.integer "guest_in_room"
    t.integer "title", default: 0
    t.string "first_name"
    t.string "last_name"
    t.integer "age"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tbo_booking_id"], name: "index_tbo_guests_on_tbo_booking_id"
  end

  create_table "tbo_hotels", force: :cascade do |t|
    t.string "hotel_code"
    t.string "city_code"
    t.string "hotel_name"
    t.string "address"
    t.string "hotel_rating"
    t.string "fax_number"
    t.string "phone_number"
    t.string "country_name"
    t.string "description"
    t.string "map"
    t.string "pin_code"
    t.string "hotel_website_url"
    t.string "trip_advisor_rating"
    t.string "trip_advisor_review_url"
    t.string "city_name"
    t.string "hotel_location"
    t.decimal "markup_amount"
    t.integer "markup_type"
    t.text "attractions"
    t.text "hotel_facilities"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_active", default: false
    t.index ["city_code"], name: "index_tbo_hotels_on_city_code"
    t.index ["hotel_code"], name: "index_tbo_hotels_on_hotel_code"
  end

  create_table "tour_accomodations", force: :cascade do |t|
    t.string "hotel_detail"
    t.datetime "check_in_date"
    t.datetime "check_out_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "tour_id"
    t.index ["tour_id"], name: "index_tour_accomodations_on_tour_id"
  end

  create_table "tour_flights", force: :cascade do |t|
    t.datetime "departure"
    t.datetime "arrival"
    t.string "from_to"
    t.string "flight_no"
    t.string "internal_remark"
    t.string "agent_remark"
    t.bigint "tour_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tour_id"], name: "index_tour_flights_on_tour_id"
  end

  create_table "tour_leaders", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.datetime "date_of_birth"
    t.string "mobile"
    t.string "emergency_number"
    t.integer "gender"
    t.string "photo"
    t.datetime "passport_expiry_date"
    t.text "passport_number"
    t.string "passport_copy"
    t.string "passport_country"
    t.boolean "insurance_confirmation"
    t.string "insurance_copy"
    t.datetime "insurance_expiry_date"
    t.string "nationality"
    t.string "languages", default: [], array: true
    t.text "description"
    t.string "address"
    t.string "experience"
    t.string "expertise"
    t.string "visa_no"
    t.string "designation"
    t.boolean "active", default: false
    t.integer "status", default: 0
    t.string "room_type", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "visa_expiry_date"
  end

  create_table "tours", force: :cascade do |t|
    t.string "code"
    t.string "caption"
    t.string "other_caption"
    t.string "contient_country"
    t.jsonb "max_seats", default: {}, null: false
    t.jsonb "vacant_seats", default: {}, null: false
    t.jsonb "prices", default: {}, null: false
    t.datetime "early_bird_date"
    t.datetime "departure_date"
    t.datetime "cut_off_date"
    t.boolean "vacant_seats_available", default: false
    t.boolean "guaranteed_departure_flag", default: true
    t.integer "guaranteed_departure_seats"
    t.integer "payment_seats", default: 0
    t.integer "max_booking_seats", default: 6
    t.integer "deposit"
    t.string "tour_leader_name"
    t.string "remark"
    t.string "internal_reference"
    t.boolean "active", default: false
    t.datetime "first_activated_at"
    t.bigint "itinerary_id"
    t.integer "created_user_id"
    t.integer "last_edited_user_id"
    t.datetime "last_edited_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "category"
    t.boolean "is_cruise", default: false
    t.boolean "guaranteed_departure", default: false
    t.integer "ground_price"
    t.boolean "ground_price_flag", default: false
    t.integer "single_supplement_price", default: 0
    t.text "tour_leader_remark"
    t.integer "misc_fee", default: 0
    t.jsonb "child_fares", default: {}
    t.jsonb "dta_prices", default: {}
    t.string "zone"
    t.string "highlight"
    t.jsonb "subtract_fees"
    t.jsonb "additional_fees"
    t.string "land_staff"
    t.string "land_company"
    t.string "guide_languages"
    t.string "welcome_board"
    t.string "letter_head_image"
    t.string "operator_remarks", array: true
    t.string "tipping"
    t.string "tour_leader_id"
    t.string "pnrs", array: true
    t.string "meet_up_point"
    t.datetime "assembly_time"
    t.string "tour_guide_name"
    t.string "tour_guide_phone"
    t.string "tour_guide_chinese_name"
    t.string "hand_carry_weight"
    t.string "luggage_quantity"
    t.string "luggage_weight"
    t.string "confirmation_emergency_contact"
    t.index ["itinerary_id"], name: "index_tours_on_itinerary_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.integer "role", default: 0
    t.string "code"
    t.string "username"
    t.string "full_name"
    t.string "other_name"
    t.string "alias_name"
    t.string "address"
    t.string "city"
    t.string "state"
    t.string "postal"
    t.string "country"
    t.string "phones", array: true
    t.string "faxes", array: true
    t.string "emails", array: true
    t.string "website"
    t.string "remark"
    t.integer "max_sign_in", default: 0
    t.boolean "is_active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.integer "credits_decimal", default: 0
    t.integer "last_edited_user_id"
    t.datetime "last_edited_at"
    t.integer "last_reset_password_edited_user_id"
    t.datetime "last_reset_password_edited_at"
    t.string "domains", default: [], array: true
    t.integer "agency_id"
    t.jsonb "commission_details"
    t.jsonb "travelb2b_module_access"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
    t.index ["username", "agency_id"], name: "index_users_on_username_and_agency_id", unique: true
  end

  add_foreign_key "cancelled_hotel_booking_groups", "hotel_booking_groups"
  add_foreign_key "hotel_bookings", "hotel_booking_groups"
  add_foreign_key "tbo_amendment_requests", "tbo_bookings"
  add_foreign_key "tbo_guests", "tbo_bookings"
end

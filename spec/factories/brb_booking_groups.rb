FactoryBot.define do
  factory :brb_booking_group do
    price { "MyString" }
    service_number { "MyString" }
    product_code { "MyString" }
    is_international { false }
  end
end

FactoryBot.define do
  factory :brb_booking do
    first_name { "MyString" }
    last_name { "MyString" }
    brb_airline_id { 1 }
    airline_name { "MyString" }
    brb_airline_confirmation_number { "MyString" }
    agent_user_id { 1 }
    status { "MyString" }
  end
end

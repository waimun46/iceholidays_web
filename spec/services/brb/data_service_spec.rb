require 'rails_helper'

describe Brb::DataService do

  describe "#initialize" do
    subject { Brb::DataService.new({}) }

    it { expect(subject.url).to eq("http://validation-api.blueribbonbags.com/api") }
    it { expect(subject.token).to eq("MzdkMTU3YjgtNjExOS00MTk2LWE0ZDAtNDJjODM2MDM2YjUy") }
  end
end

# worker_processes 25 # RAM 8GB
# worker_processes 55 # RAM 16GB
# worker_processes 145 # RAM 32GB
# worker_processes 300 # RAM 64GB

# (total_system_memory(MB) - 1024(MB)) / (250MB)
worker_number = (%x(free).split(" ")[7].to_i/1024 - 1024) / 250
worker_processes worker_number

app_path = "/home/admin/iceb2b"
working_directory("#{app_path}/current")
shared_path = "#{app_path}/shared"

pid "#{app_path}/current/tmp/pids/unicorn.pid"

listen "#{shared_path}/unicorn.sock"

stderr_path "#{shared_path}/log/unicorn.log"

preload_app true
timeout 60

GC.respond_to?(:copy_on_write_friendly=) and
  GC.copy_on_write_friendly = true

before_exec do |server|
  ENV["BUNDLE_GEMFILE"] = File.join("#{app_path}/current", 'Gemfile')
end

rails_env = 'iceb2b'

before_fork do |server, worker|

  old_pid = "#{server.config[:pid]}.oldbin"
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      # someone else did our job for us
    end
  end

  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.connection.disconnect!
  defined?(REDIS) and
    REDIS.client.disconnect
end

after_fork do |server, worker|
  # GC.disable
  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.establish_connection
  defined?(REDIS) and
    REDIS.client.connect
end

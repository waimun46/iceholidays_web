# server is UTC(+0) time zone, Malaysia(+8) time zone
# server time '5:30pm', Malaysia time '1:30am'
# server time '6:30pm', Malaysia time '2:30am'
# server time '7:30pm', Malaysia time '3:30am'
# server time '4:00pm', Malaysia time '12:00am'
# server time '10:15am', Malaysia time '6:15pm'

env :PATH, ENV['PATH']

# every :day, :at => '4:01pm' do
#   rake "batch:testing_reminder"
# end

every :day, :at => '5:30pm' do
  rake "batch:change_pending_payment_to_fail"
  rake "batch:sending_booking_reminder"
  rake "batch:sending_reminder"
  rake "batch:auto_cancellation"

  rake "batch:process_pay_later_reminder"
  rake "batch:process_balance_payment"
  
  rake "batch:sending_land_tour_reminder"
  # rake "batch:land_tour_auto_cancellation"
  rake "batch:land_tour_voucher"
end

every :day, :at => '10:15am' do
  rake "batch:sending_daily_sales_report"
end

every '30 18 28-31 * *' do
  rake "batch:creating_invoice"
end

every :day, :at => '7:30pm' do
  # restore to production
  # pg_restore -F c --clean --no-acl --no-owner -d iceb2b_production  production_backup/databases/PostgreSQL.sql

  # restore to local
  # pg_restore -F c --clean --no-acl --no-owner -d iceholidays_development  production_backup/databases/PostgreSQL.sql
  command "backup perform -t production_backup"
end

every :day, :at => '4:00pm' do
  rake "batch:updating_globaltix"
end

if Rails.env.iceb2b?
  Sidekiq.configure_server do |config|
    config.on(:shutdown) do
      Rails.logger.info("Sidekiq is shutting down")
    end

    config.redis = { :url => 'redis://192.168.142.249:6379/15', password: 'thisNotEvenAp@ssw0rdAtAll' }
  end

  Sidekiq.configure_client do |config|
    config.redis = { :url => 'redis://192.168.142.249:6379/15', password: 'thisNotEvenAp@ssw0rdAtAll' }
  end
elsif Rails.env.testb2b?
  Sidekiq.configure_server do |config|
    config.on(:shutdown) do
      Rails.logger.info("Sidekiq is shutting down")
    end

    config.redis = { :url => 'redis://192.168.142.249/0', password: 'thisNotEvenAp@ssw0rdAtAll' }
  end

  Sidekiq.configure_client do |config|
    config.redis = { :url => 'redis://192.168.142.249/0', password: 'thisNotEvenAp@ssw0rdAtAll' }
  end
elsif Rails.env.development?
  Sidekiq.configure_server do |config|
    config.on(:shutdown) do
      Rails.logger.info("Sidekiq is shutting down")
    end

    config.redis = { :url => 'redis://localhost:6379/0' }
  end

  Sidekiq.configure_client do |config|
    config.redis = { :url => 'redis://localhost:6379/0' }
  end
end
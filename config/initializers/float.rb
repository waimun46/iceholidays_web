class Float
  def round_to_half(x = 0)
    times = (2 * 10**(x-1)).to_f
    (self * times ).round / times
  end


end
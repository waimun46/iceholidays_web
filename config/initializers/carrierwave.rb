if Rails.env.test? || Rails.env.development?
  CarrierWave.configure do |config|
    config.storage = :file
    config.asset_host = ActionController::Base.asset_host
  end
else
  CarrierWave.configure do |config|
    config.fog_provider = 'fog/aws'
    config.fog_credentials = {
      provider: 'AWS',
      aws_access_key_id: Rails.application.credentials.aws[:access_key],
      aws_secret_access_key:  Rails.application.credentials.aws[:secret_key],
      region: 'ap-southeast-1',
      path_style: true
    }
    config.fog_directory  = Rails.application.credentials.aws[:fog_directory]
    config.asset_host  = Rails.application.credentials.aws[:asset_host]
    config.fog_attributes = { 'Cache-Control' => 'max-age=315360000' }
    config.storage = :fog
  end
end
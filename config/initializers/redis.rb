if Rails.env.iceb2b?
  REDIS = Redis.new(host: '192.168.142.249', port: '6379', db: '15', driver: 'hiredis', password: 'thisNotEvenAp@ssw0rdAtAll')
elsif Rails.env.testb2b?
  REDIS = Redis.new(host: '192.168.142.249', port: '6379', db: '14', driver: 'hiredis', password: 'thisNotEvenAp@ssw0rdAtAll')
elsif Rails.env.development?
  REDIS = Redis.new(host: 'localhost', port: '6379', db: '2', driver: 'hiredis')
end
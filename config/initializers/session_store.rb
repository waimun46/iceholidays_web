if Rails.env.iceb2b?
  Rails.application.config.session_store :cookie_store, key: '_iceholidays_session', secure: true
end
class String
  def seats_to_price
    self.sub(/_seats\z/, '_price')
  end

  def price_to_seats
    self.sub(/_price\z/, '_seats')
  end

  def remove_price_seats
    self.sub(/_price\z/, '').sub(/_seats\z/, '')
  end
end
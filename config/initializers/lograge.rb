Rails.application.configure do
  config.lograge.keep_original_rails_log = true

  config.lograge.formatter = Lograge::Formatters::Logstash.new

  config.lograge.custom_payload do |controller|
    {
      host: controller.request.host,
      ip: controller.request.remote_ip,
      user_id: controller.current_user.try(:id)
    }
  end

  config.lograge.custom_options = lambda do |event|
    exceptions = %w(controller action format)
    {
      params: event.payload[:params].except(*exceptions)
    }
  end

  config.lograge.logger = ActiveSupport::Logger.new "#{Rails.root}/log/lograge_#{Rails.env}.log"
end
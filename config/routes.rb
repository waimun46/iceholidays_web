Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_scope :user do
    root :to => 'devise/sessions#new'
  end

  devise_for :users

  namespace :api, defaults: {format: 'json'} do
    resources :tours, only: [:index]
    resources :series, only: [:index, :show] do
      collection do
        get 'country_list'
        get 'itinerary_list'
      end
    end
    
    namespace :v1 do
      resources :flights, only: [:index, :show] do
        member do
          post 'create_booking'
        end
        collection do
          get 'bookings'
        end
      end
      resources :activities, only: [:index, :show] do
        collection do
          get 'country_list'
          get 'bookings'
        end
        member do
          post 'create_booking'
        end
      end
      resources :hotels, only: [:index, :show] do
        collection do
          get 'cities'
          get 'bookings'
        end
        member do
          post 'create_booking'
          get 'room_policy'
        end
      end
      resources :wifis, only: [:index, :show] do
        collection do
          get 'country_list'
          get 'bookings'
        end
        member do
          post 'create_booking'
        end
      end
      resources :series, only: [:index, :show] do
        collection do
          get 'country_list'
          get 'itinerary_list'
          get 'designation_list'
          get 'bookings'
        end
        member do
          post 'create_booking'
          patch 'update_bookings'
          get 'new_payment'
          post 'create_payment'
          post 'create_insurance_export'
        end
      end

      resources :land_tours, only: [:index, :show] do
        collection do
          get 'country_list'
          get 'land_tours_list'
          get 'designation_list'
          get 'bookings'
        end
        member do
          post 'create_booking'
          get 'new_payment'
          post 'create_payment'
        end
      end

      resources :payments, only: [:show] do
        collection do
          get 'payment_method'
        end
      end

      resources :users, only:[:none] do
        collection do
          get 'details'
          get 'credit_usage'
          get 'credit_packages'
        end
        member do
          post 'purchase_credits'
        end
      end
      
    end

    namespace :tbo_holidays do
      get 'cities' => 'cities#index'
    end
  end

  namespace :admin do
    root to: "sites#dashboard"
    get   '/dashboard'        => 'sites#dashboard', as: :dashboard
    match '/setting_cover'       => 'sites#cover', as: :cover, via: [:get, :post, :delete]

    resources :users, except: [:destroy] do
      member do
        get 'reset_password'
        post 'reset_password'
        get 'edit_password'
        patch 'update_password'
        get 'topup_credits'
        post 'topup_credits'
      end
      collection do
        get 'access_log'
        get 'online_list'
      end
    end

    resources :flight_commissions, except: [:destroy] do
    end

    resources :tour_leaders, expect: [:destroy] do
    end

    resources :tours, except: [:destroy] do
      member do
        post 'create_booking'
        post 'duplicate'
        get 'insurance'
        get 'booking_rooms'
        get 'edit_booking_rooms'
        get 'confirmation'
        patch 'update_booking_rooms'
      end
      collection do
        get 'uncertainty'
        get 'calendar'
      end

    end

    resources :holiday_calendars, except: [:destroy] do 
    end

    resources :flight_booking_groups, only: [:index, :show] do
    end

    resources :hotels, except: [:destroy]

    resources :tbo_hotels, except: [:destroy] do
      member do
        post 'activate'
        post 'deactivate'
      end
    end

    resources :hotel_booking_groups, path: 'hotel_booking_groups', only: [:index, :show]

    resources :tbo_bookings, path: 'tbo_bookings', only: [:index, :show] do
      resource :amendment_request, module: :tbo_bookings do
        member do
          post 'widthraw'
        end
      end
      member do
        post 'cancel'
      end
    end

    resources :globaltix_ticket_booking_groups, path: 'ticket_booking_groups', only: [:index, :show] do
    end

    resources :itineraries do
      member do
        post 'tour_summary'
        post 'duplicate'
      end
      collection do
        get 'tour_status'
      end
    end

    resources :settings do
      collection do
        get 'travelb2b'
        get 'edit_travelb2b'
        post 'update_travelb2b'
      end
    end

    resources :sliders do
    end

    resources :departments, except: [:destroy] do
    end

    resources :etravel_invoices, only: [:index] do
    end
    
    resources :fairs, except: [:destroy] do
    end

    resources :holidays, except: [:destroy] do
    end

    resources :credit_refunds, except: [:destroy] do
      member do
        post 'update_status'
      end
    end

    resources :booking_groups, only: [:index, :show, :edit, :update] do
      member do
        get  'new_alter'
        post 'create_alter'
        get  'new_export'
        post 'create_export'
        post 'create_insurance_export'
        post 'create_insurance_endorse'
        post 'proforma_invoice'
        post 'assign_agent'
      end
      collection do
        get 'received_deposit'
        get 'tour_booking_summary'
        get 'booking_statistic'
        get 'sales_statistic'
        get 'insurance_changes'
      end
    end

    resources :booking_alterations, only: [:index, :show] do
      member do
        get 'restore'
      end
    end

    resources :invoices, only: [:index, :show] do
    end

    get  'search/fair-tag'                => 'sites#fair_tag'
    get  'search/itinerary-tag'           => 'sites#itinerary_tag'
    get  'search/tour-tag'                => 'sites#tour_tag'
    get  'search/user-tag'                => 'sites#user_tag'
    get  'search/country-tag'             => 'sites#country_tag'

    resources :gifts do
      collection do
        get 'gift_stock_report'
      end
    end

    resources :insurance_rebates, only: [:index, :show, :edit, :update] do
    end

    resources :pre_commisions, except: [:destroy] do
    end

    resources :roamingman_booking_groups, only: [:index, :show, :edit, :update, :destroy] do
    end

    resources :gift_tracking_groups, except: [:destroy] do
      collection do
        get 'tracking_summary'
      end
      member do
        get 'summary_detail/:id', to: 'gift_tracking_groups#summary_detail', as: 'summary_detail'
        post 'deliver'
      end
    end

    resources :globaltix_tickets do
      collection do
        get 'select', constraints: { country_id: /[0-9]*/, page: /[0-9]*/ }
        get 'activities', constraints: { attraction_id: /[0-9]*/ }
      end
    end

    resources :globaltix_attractions do
      collection do
        get 'select', constraints: { country_id: /[0-9]*/, page: /[0-9]*/ }
        # get 'activities', constraints: { attraction_id: /[0-9]*/ }
      end
      member do
        post 'add_ticket'
      end
    end

    resources :exchange_rates, except: [:destroy] do
    end

    resources :roamingman_packages, except: [:destroy] do
      member do
        get 'new_booking'
        post 'create_booking'
      end
    end

    resources :roamingman_booking_groups, only: [:index, :show, :edit, :update, :destroy] do
    end

    resources :airlines, except: [:destroy] do
    end

    resources :activities, except: [:destroy] do
    end

    resources :flight_inventories, except: [:destroy] do
    end

    resources :activity_booking_groups, except: [:destroy] do
    end

    resources :land_tours, except: [:destroy] do
    end

    resources :cities, except: [:destroy] do
    end

    resources :land_tour_booking_groups, except: [:destroy] do
      member do
        get 'resend_confirmation'
        get  'new_withdraw'
        post 'create_withdraw'
        get 'voucher'
      end
    end
  end

  resources :gift_tracking_groups, except: [:destroy] do
    member do
      post 'submit'
      post 'acknowledge'
      post 'reject'
    end
  end

  resources :hotels, path: 'hotels', only: [:index] do
    collection do
      get   'details'
      get   'booking'
      post  'booking'
    end
  end

  resources :hotel_booking_groups, path: 'hotel_booking_groups', only: [:create] do
    member do
      post  'cancel'
    end
  end

  resources :flights, only: [:index, :show] do
    member do
      post 'create_booking'
    end
    collection do
      get 'thankyou'
    end
  end

  resources :flight_booking_groups, only: [:index, :show] do
    member do
      get 'invoice'
    end
  end

  # resources :activities, only: [:index, :show, :create] do
    # resources :activity_bookings, only: [:new, :create] do
    # end

    # collection do
    #   get 'new_booking/:id', to: 'activities#new_booking', as: 'new_booking'
    #   post 'create_booking'
    # end
  # end

  resources :activity_booking_groups, only: [:show, :edit, :update, :index] do
    collection do
      get 'thank_you'
    end
    member do
      get 'invoice'
    end
  end

  resources :payments, only: [:index] do
    collection do
      get 'error_page'
    end
  end
  
  resources :itineraries do
  end

  resources :globaltix_ticket_booking_groups, path: 'ticket_booking_groups', only: [:index, :show] do
    collection do
      get 'thank_you'
    end
    member do
      get 'invoice'
    end
  end

  resources :roamingman_packages, only: [:index, :show] do
    resources :roamingman_bookings, only: [:new, :create] do
    end
  end

  resources :roamingman_booking_groups, only: [:index, :show, :edit, :update, :destroy] do
    collection do
      get 'thank_you'
    end
    member do
      get 'invoice'
    end
  end

  get   '/tours(/:brand)' => 'tours#index', as: :tours, constraints: { brand: /(fs)|(premium)|(cruise)|(gtrip)|(gd)/ }

  resources :tours, only: [:index, :show] do
    resources :bookings, only: [:new, :create] do
      collection do
        get 'thankyou'
        post 'cruise_options'
      end
    end
  end

  # resources :bookings, only: [:index] do
  # end

  resources :booking_groups, only: [:show, :edit, :update, :index] do
    member do
      post 'create_insurance_export'
      get 'booking_rooms'
      get 'edit_booking_rooms'
      patch 'update_booking_rooms'
      get 'invoice'
      get 'invoice/:invoice_id', to: 'booking_groups#invoice'
      post 'create_gift_tracking'
    end
  end

  resource :user, only: [:edit] do
    collection do
      patch 'update_password'
      get 'ktic_login'
    end
  end

  namespace :tbo_holidays do
    root to: 'hotels#index'
    resources :hotels, except:[:destroy]
    resources :bookings, only:[:index, :show, :new, :create] do
      collection do
        get 'thankyou'
        get 'cancellation_policies'
      end

      member do
        get 'invoice'
      end
    end
  end

  get   '/dashboard'        => 'sites#dashboard', as: :dashboard
  get   '/app'             => 'sites#app', as: :app

  get   '/payment/:payment_id/proceed_to_payment' => 'payments#proceed_to_payment', as: :proceed_to_payment
  
  get   '/payment/new_pbb' => 'payments#new_pbb', as: :new_pbb
  get   '/payment/:payment_id/proceed_to_pbb' => 'payments#proceed_to_pbb', as: :proceed_to_pbb
  post   '/payment/pbb' => 'payments#pbb_direct', as: :pbb_direct
  post   '/payment/pbb_indirect' => 'payments#pbb_indirect', as: :pbb_indirect

  get   '/payment/new_fpx' => 'payments#new_fpx', as: :new_fpx
  get   '/payment/:payment_id/proceed_to_fpx' => 'payments#proceed_to_fpx', as: :proceed_to_fpx
  get   '/payment/:payment_id/fpx_status' => 'payments#fpx_status', as: :fpx_status
  post   '/payment/fpx' => 'payments#fpx_direct', as: :fpx_direct
  # post   '/payment/fpx_indir' => 'payments#fpx_indirect', as: :fpx_indirect
  post   '/payment/fpx_indirect' => 'payments#fpx_indirect', as: :fpx_indirect

  resources :land_tours, only: [:index, :show] do
    resources :land_tour_bookings, only: [:new, :create] do
      collection do
        get 'thankyou'
      end
    end
  end

  resources :land_tour_booking_groups, except: [:destroy] do
    member do
      get 'invoice'
      get 'invoice/:invoice_id', to: 'land_tour_booking_groups#invoice'
      get 'voucher'
    end
  end

end

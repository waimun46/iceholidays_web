Ruby Version: 2.4.2
Rails: 5.1.4

* Ruby version
2.4.2

* Configuration
- Ruby
- Postgresql
- Redis
- Puma-dev (Recommend)

* Services (job queues, cache servers, search engines, etc.)
`bundle exec sidekiq`
`redis-cli`

To run react
`./bin/webpack-dev-server`


source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.3'
gem 'bootsnap', require: false
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem "rails-settings-cached"

gem 'unicorn'
gem 'unicorn-worker-killer'

gem 'devise'
gem 'kaminari'
gem 'carrierwave'
gem 'fog-aws'
gem 'activerecord-typedstore', '>= 1.1.3'
gem 'cocoon'
gem 'rqrcode'

gem 'business_time'
gem "groupdate", '~> 3.2.1'
gem 'calculate-all'
gem "simple_calendar", "~> 2.0"

gem 'turbolinks', '~> 5.1.0'

# data
gem 'airports', '~> 1.5.0'
gem 'country_select', '~> 3.1'
gem 'city-state'

# background job
gem 'sidekiq'

#email css inline
gem 'inky-rb', require: 'inky'
gem 'premailer-rails'

# Caching
gem 'redis', '~>3.3.5'
gem 'hiredis'
gem 'redis-rails'
# log
gem 'awesome_print'

gem 'newrelic_rpm'

gem "lograge"
gem "logstash-event"

gem 'mailgun-ruby', '~>1.1.6'

gem 'faraday'
gem 'httparty'
gem 'gyoku', git: 'https://github.com/savonrb/gyoku.git'
gem 'nori', git: 'https://github.com/savonrb/nori.git'
gem 'active_model_attributes' # Can remove this on rails 5.2 or higher
gem 'down'

gem 'webpacker'

# Crontab
gem 'whenever'

# Authorization
gem "pundit"

gem 'money'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
  # Use Puma as the app server
  gem 'puma', '~> 3.7'
  gem 'pry'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem "letter_opener"

  # gem 'mina'
  # gem 'mina-unicorn', require: false
  # gem 'mina-whenever', require: false
  gem 'capistrano', '~> 3.7', '>= 3.7.1'
  gem 'capistrano-rails', '~> 1.2'
  gem 'capistrano-rbenv', '~> 2.1'
  gem 'capistrano-bundler', '~> 1.3'
  gem 'capistrano3-unicorn'
  gem 'capistrano-rails-console', require: false
  gem 'capistrano-rails-db'
  gem 'capistrano-sidekiq'
  gem 'capistrano-local-precompile', '~> 1.2.0', require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

require 'watir'
require 'nokogiri'
require 'csv'

module Selenium
  module WebDriver
    module Remote
      module Http
        module DefaultExt
          def request(*args)
            tries ||= 3
            super
          rescue Net::ReadTimeout, Net::HTTPRequestTimeOut, Errno::ETIMEDOUT => ex
            puts "#{ex.class} detected, retrying operation"
            (tries -= 1).zero? ? raise : retry            
          end
        end
      end
    end
  end
end
Selenium::WebDriver::Remote::Http::Default.prepend(Selenium::WebDriver::Remote::Http::DefaultExt)


@url_base = "http://iceholidays.sofarso.com/Operations/"

def start
  @b = Watir::Browser.new :chrome, switches: %w(--ignore-certificate-errors --disable-popup-blocking --disable-translate)
  process_login

end

def process_login
  @b.goto "http://iceholidays.sofarso.com/"
  @b.text_field(name: 'txtUser').set 'foong'
  @b.text_field(name: 'txtPswd').set! 'foong24'
  @b.button(id: 'btnLogin').click
  Watir::Wait.until { @b.url == "http://iceholidays.sofarso.com/Operations/Default.aspx" }
end

def goto_page(url)
  begin
    tries ||= 3
    @b.goto url
  rescue Net::ReadTimeout
    puts "Net::ReadTimeout: #{url}"
    if tries == 0
      raise
    end

    @b.close
    @b = Watir::Browser.new :chrome, switches: %w(--ignore-certificate-errors --disable-popup-blocking --disable-translate)
    process_login
    @b.goto url
    tries -= 1
  end

  if @b.url != url
    process_login
    @b.goto url
  end
end

def crawl_users
  users_raw_file = CSV.open("./users_raw.txt", 'a', {:col_sep => "\t"})
  users_seed_file = File.open("./users_seed.txt", 'a')

  goto_page "http://iceholidays.sofarso.com/Operations/Users.aspx"
  validation = @b.input(id: "__EVENTVALIDATION").value

  users_link_arr = []
  current_page = 1
  next_page = ''
  begin
    doc = Nokogiri::HTML(@b.html)
    next_page = ''
    doc.css('table.gvw0 tbody tr td a').each do |el|
      if el['href'].include?('UserEdit.aspx?q=')
        users_link_arr << el['href']
      elsif next_page == ''
        ret = el['href'].match /Page\$(\d+)/
        if ret.length == 2 && current_page < el.text.to_i
          next_page = el
          current_page = el.text.to_i
        end
      end
    end
    if next_page != '' && @b.link(href: next_page['href']).present?
      @b.execute_script('arguments[0].scrollIntoView();', @b.link(href: next_page['href']))
      @b.link(href: next_page['href']).click
      Watir::Wait.until {@b.input(id: "__EVENTVALIDATION").value != validation}
      validation = @b.input(id: "__EVENTVALIDATION").value
    end
  end while next_page != ''

  if users_link_arr != nil
    @raw_data_set = []
    users_link_arr.each do |link|
      goto_page @url_base + link
      doc = Nokogiri::HTML(@b.html)
      data = doc.css('table table tr td span').map(&:text)
      data << doc.css('table table tr td a').text
      users_raw_file << data
      @raw_data_set << data

      if data_process(data[1]) == "IH-ADMIN" || data_process(data[1]) == "IH-GUEST"
        next
      end


      if data_process(data[3]) != "(Reserved)"
        if data.count == 39 && data[0] == "Person"
          if data_process(data[30]) == '(Unspecified)'
            username = data_process(data[5]).gsub(/[-!$%^&*()+|~=`{}\[\]:";'<>?,.\/]/, '_').chomp
          else
            username = data_process(data[30])
          end
          username.gsub(/\s/, '_')

          address = "#{data_process(data[11])} #{data_process(data[13])} #{data_process(data[15])} #{data_process(data[17])}".strip
          phones = [data_process(data[12]), data_process(data[14]), data_process(data[18]), data_process(data[20])].reject { |d| d.empty? }
          emails = [data_process(data[22]), data_process(data[24])].reject { |d| d.empty? }
          state = state_mapping(data[23])
          users_seed_file.puts "User.create(code: \"#{data_process(data[1])}\", full_name: \"#{data_process(data[3])}\", alias_name: \"#{data_process(data[5])}\", other_name: \"#{data_process(data[7])}\", address: \"#{address}\", phones: #{phones}, faxes: [\"#{data_process(data[16])}\"], postal: \"#{data_process(data[19])}\", city: \"#{data_process(data[21])}\", emails: #{emails}, state: \"#{state}\", country: \"#{data_process(data[25])}\", website: \"#{data_process(data[26])}\", remark: \"#{data_process(data[27])}\", role: \"#{data_process(data[28]).downcase}\", username: \"#{username}\", is_active: #{data[32] == 'Yes' ? 'true' : 'false'}, created_at: \"#{data_process(data[36])}\", updated_at: \"#{data_process(data[34])}\", max_sign_in: \"#{data_process(data[38])}\")"
        elsif data.count == 36 && data[0] == "Organisation"
          if data_process(data[27]) == '(Unspecified)'
            username = data_process(data[5]).gsub(/[-!$%^&*()+|~=`{}\[\]:";'<>?,.\/]/, '_').chomp
          else
            username = data_process(data[27])
          end
          username.gsub(/\s/, '_')

          address = "#{data_process(data[8])} #{data_process(data[10])} #{data_process(data[12])} #{data_process(data[14])}".strip
          phones = [data_process(data[9]), data_process(data[11]), data_process(data[15]), data_process(data[17])].reject { |d| d.empty? }
          emails = [data_process(data[19]), data_process(data[21])].reject { |d| d.empty? }
          state = state_mapping(data[20])
          users_seed_file.puts "User.create(code: \"#{data_process(data[1])}\", full_name: \"#{data_process(data[3])}\", alias_name: \"#{data_process(data[5])}\", other_name: \"#{data_process(data[7])}\", address: \"#{address}\", phones: #{phones}, faxes: [\"#{data_process(data[13])}\"], postal: \"#{data_process(data[16])}\", city: \"#{data_process(data[18])}\", emails: #{emails}, state: \"#{state}\", country: \"#{data_process(data[22])}\", website: \"#{data_process(data[23])}\", remark: \"#{data_process(data[24])}\", role: \"#{data_process(data[25]).downcase}\", username: \"#{username}\", is_active: #{data[29] == 'Yes' ? 'true' : 'false'}, created_at: \"#{data_process(data[33])}\", updated_at: \"#{data_process(data[31])}\", max_sign_in: \"#{data_process(data[35])}\")"
        else
          puts "errors"
          puts data
        end
      end

    end
  end
  users_raw_file.close
  users_seed_file.close

end

def crawl_tours
  tours_raw_file = CSV.open("./tours_raw.txt", 'a', {:col_sep => "\t"})
  tours_seed_file = File.open("./tours_seed.txt", 'a')
  tours_id_file = File.open("./tours_id.txt", 'a')
  tours_itinerary_seed_file = File.open("./tours_itinerary_seed.txt", 'a')
  tours_error_file = File.open("./tours_error.txt", 'a')
  test_group = ['http://iceholidays.sofarso.com/Operations/TourEdit.aspx?q=20763', 'http://iceholidays.sofarso.com/Operations/TourEdit.aspx?q=20764', 'http://iceholidays.sofarso.com/Operations/TourEdit.aspx?q=23785', 'http://iceholidays.sofarso.com/Operations/TourEdit.aspx?q=23786', 'http://iceholidays.sofarso.com/Operations/TourEdit.aspx?q=23141', 'http://iceholidays.sofarso.com/Operations/TourEdit.aspx?q=23142']

  # test_group.each do |link|
  # (1..30000).each do |i|
  # (14171..30000).each do |i|
  # (19871..30000).each do |i|
  # (24466..30000).each do |i|
  # (30001..30200).each do |i|
  # (30201..31000).each do |i|
  # (1..32000).each do |i|
  # (1..31400).each do |i|

  # (1..14368).each do |i|
  (14369..31400).each do |i|
    goto_page "http://iceholidays.sofarso.com/Operations/TourEdit.aspx?q=#{i}"

    if @b.title == "(Missing)"
      next
    end

    doc = Nokogiri::HTML(@b.html)
    data = doc.css('table div.layer0 table tr td span').map(&:text)
    data << doc.css('table div.layer0 table tr td a#lnkCode').text
    flights = doc.css('table div#tbpFlight table tr td').map(&:text).map(&:strip)
    others = doc.css('table div#tbpOthers table tr td span').map(&:text)
    promo = doc.css('table div#tbpPromo table tr td span').map(&:text)
    internal = if doc.css('table div#tbpNote span#lblNote span').text == "(Blank)"
      ""
    else
      doc.css('table div#tbpNote span#lblNote').inner_html
    end
    data << internal
    updated = doc.css('table div#panUpdate table tr td span').map(&:text)


    data.delete_at(1) if data[1] == 'NEW'
    data.delete_at(4) if data[3] == 'NO' && data[4] == 'NO'
    data.delete_at(4) if data[4] == "Sync"
    data.delete_at(6) if data[5].to_i != 0 && data[5] == data[6]
    data.delete_at(16) if data[16] == "\u25c0"

    data.each do |c|
      c.gsub!(/"/,"\\\"")
    end
    flights.each do |c|
      c.gsub!(/"/,"\\\"")
    end
    others.each do |c|
      c.gsub!(/"/,"\\\"")
    end
    promo.each do |c|
      c.gsub!(/"/,"\\\"")
    end
    updated.each do |c|
      c.gsub!(/"/,"\\\"")
    end

    tours_raw_file << data
    tours_raw_file << flights
    tours_raw_file << others
    tours_raw_file << promo
    tours_raw_file << updated

    tour_create = "Tour.create(id: #{i}, code: \"#{data_process(data[-2])}\", caption: \"#{data_process(data[4])}\", other_caption: \"#{data_process(data[6])}\", contient_country: \"#{data_process(data[9])}\", total_seats: \"#{data_process(data[12])}\", normal_price: \"#{number_process(data[15])}\", departure_date: \"#{data_process(data[-7])}\", guaranteed_departure_flag: #{data[8] == 'Auto' ? 'true' : 'false'}, guaranteed_departure_seats: \"#{data_process(data[11])}\", max_booking_seats: \"#{data_process(data[14])}\", deposit: \"#{number_process(data[-8])}\", tour_leader: \"#{data_process(others[0])}\", remark: \"#{data_process(data[-5])}\", internal_reference: \"#{data_process(data[-1])}\", active: #{data[3] == 'Yes' ? 'true' : 'false'}"

    if !updated.empty?
      if updated[0] != "{Unknown}"
        tour_create += ", last_edited_at: \"#{data_process(updated[0])}\""
      end
      if !updated[1].empty?
        tour_create += ", last_edited_user_id: User.find_by_alias_name(\"#{data_process(updated[1])}\").try(&:id)"
      end
      if updated[2] != "{Unknown}" && !updated[2] != "{Unknown)"
        tour_create += ", created_at: \"#{data_process(updated[2])}\""
      end
      if !updated[3].empty?
        tour_create += ", created_user_id: User.find_by_alias_name(\"#{data_process(updated[3])}\").try(&:id)"
      end
    end

    if data_process(promo[1]) == 'Limited Promo' || data_process(promo[1]) == 'Promotional Offer' || data_process(promo[1]) == 'Early Bird Promo'
      promo_seat = if promo[-6].to_i > data[12].to_i
        data[12]
      else
        promo[-6]
      end

      if data_process(promo[1]) == 'Promotional Offer'
        tour_create += ", promo_seats: \"#{promo_seat}\", promo_price: \"#{number_process(promo[-3])}\""
      elsif data_process(promo[1]) == 'Limited Promo'
        tour_create += ", superpromo_seats: \"#{promo_seat}\", superpromo_price: \"#{number_process(promo[-3])}\""
      elsif data_process(promo[1]) == 'Early Bird Promo'
        tour_create += ", early_bird_seats: \"#{promo_seat}\", early_bird_price: \"#{number_process(promo[-3])}\""
      end
    else
      tours_error_file.puts "#{i} - #{data_process(data[-1])}, promo error: #{promo[1]}" if data_process(promo[1]) != ''
    end

    if !flights.empty?
      tour_create += ", flights_attributes: ["
      if flights.count % 5 == 0
        for i in (0..(flights.count / 5)-1)
          if i != 0
            tour_create +=", "
          end
          tour_create += "{departure: \"#{flight_date_process(flights[0+i*5])}\", arrival: \"#{flight_date_process(flights[1+i*5])}\", from_to: \"#{data_process(flights[2+i*5])}\", flight_no: \"#{data_process(flights[3+i*5])}\", internal_remark: \"#{data_process(flights[4+i*5])}\"}"
        end
      else
        tours_error_file.puts "#{i} - #{data_process(data[-1])}, flights error"
      end
      tour_create += "]"
    end

    if !data_process(data[-3]).empty? && data_process(data[-3])[0..3] == 'http'
      tours_itinerary_seed_file.puts "t = Tour.find_by_code(\"#{data_process(data[-2])}\")"
      tours_itinerary_seed_file.puts "t.update(remote_itinerary_url: \"#{data_process(data[-3]).gsub(/\s/, '')}\")"
    end

    tour_create += ")"
    tours_seed_file.puts tour_create
    tours_id_file.puts data_process(data[2])

  end


  tours_raw_file.close
  tours_seed_file.close
  tours_itinerary_seed_file.close
  tours_error_file.close
end

def crawl_bookings(divide = nil, index = nil)
  ids = File.readlines("./tours_id.txt").map{|l| l.gsub!("\n", '')}.compact
  
  if divide != nil && index != nil
    ids_ori = ids
    element_number = ids_ori.count / divide
    if ids_ori.count % divide != 0
      element_number = element_number + 1
    end

    ids = ids_ori[(element_number*index)..(element_number*(index+1)-1)]
    # ids = ids_ori[5129..5137]

    bookings_raw_file = CSV.open("./bookings_raw-#{index}.txt", 'a', {:col_sep => "\t"})
    bookings_seed_file = File.open("./bookings_seed-#{index}.txt", 'a')
    bookings_error_file = File.open("./bookings_error-#{index}.txt", 'a')
  else
    bookings_raw_file = CSV.open("./bookings_raw.txt", 'a', {:col_sep => "\t"})
    bookings_seed_file = File.open("./bookings_seed.txt", 'a')
    bookings_error_file = File.open("./bookings_error.txt", 'a')
  end

  # ids_start = "5161"
  # ids[ids.find_index(ids_start)..-1].each do |i|

  ids.each do |i|
    goto_page "http://iceholidays.sofarso.com/Operations/Booking1.aspx?q=#{i}"

    booking_links = []
    data = []
    doc = Nokogiri::HTML(@b.html)
    tour_code = doc.css('span#ctl00_cphMain_fvw1_lblCode').text()

    if doc.css('table.gvw2 tr td[colspan="22"]').text.include?('No booking transactions')
      next
    end

    if tour_code.empty?
      bookings_error_file.puts "#{i} - tour_code missing"
      next
    end

    validation = @b.input(id: "__EVENTVALIDATION").value
    current_page = 1
    begin
      next_page = ''
      booking_links.concat doc.css('table.gvw2 td a[id$="lnkGrouping"]').map{|x| [x['href'], x['title'].gsub("\n", ' ')]}
      doc.css('table.gvw2 tr.gvwPgrA table tr td a').each do |el|
        if next_page == ''
          ret = el['href'].match /Page\$(\d+)/
          if ret.length == 2 && (current_page+1) == el.text.to_i
            next_page = el
            current_page = el.text.to_i
            @b.execute_script('arguments[0].scrollIntoView();', @b.link(href: next_page['href']))
            @b.link(href: next_page['href']).click
            Watir::Wait.until {@b.input(id: "__EVENTVALIDATION").value != validation}
            validation = @b.input(id: "__EVENTVALIDATION").value
            doc = Nokogiri::HTML(@b.html)
          end
        end
      end
      # if next_page != '' && @b.link(href: next_page['href']).present?
      #   @b.execute_script('arguments[0].scrollIntoView();', @b.link(href: next_page['href']))
      #   @b.link(href: next_page['href']).click
      #   doc = Nokogiri::HTML(@b.html)
      # end
    end while next_page != ''

    crawled_links = []
    booking_links.uniq.each do |link|
      if crawled_links.include?(link[0])
        next
      else
        crawled_links << link[0]
      end

      export = link[1]
      goto_page "http://iceholidays.sofarso.com/Operations/#{link[0]}"
      sub_doc = Nokogiri::HTML(@b.html)
      data = sub_doc.css('table.tb1 span').map(&:text)
      validation = @b.input(id: "__EVENTVALIDATION").value

      while !sub_doc.css("input[value='Next']").empty? && sub_doc.css("input[value='Next']").attribute('disabled') == nil do
        @b.execute_script('arguments[0].scrollIntoView();', @b.button(value: 'Next'))
        @b.button(value: 'Next').click
        Watir::Wait.until {@b.input(id: "__EVENTVALIDATION").value != validation}
        validation = @b.input(id: "__EVENTVALIDATION").value
        sub_doc = Nokogiri::HTML(@b.html)
        data.concat sub_doc.css('table.tb1 span').map(&:text)
      end

      data.each do |c|
        c.gsub!(/"/,"\\\"")
      end

      i = 0
      while data.count > ((i+1)*42)
        data.delete_at(31+i*42) if data[31+i*42] == data[32+i*42]
        data.delete_at(33+i*42) if data[33+i*42] == data[34+i*42]
        i = i + 1
      end

      bookings_raw_file << link
      bookings_raw_file << data

      booking_group_create = "bg = BookingGroup.create(code: \"#{data_process(data[0])}\", created_at: \"#{data_process(data[4])}\""

      if !export.empty? && export.include?('Exported on')
        booking_group_create += ", exported_remark: \"#{data_process(export)}\""
      end

      if data_process(data[17]) != ''
        booking_group_create += ", receipts_attributes: [{number: \"#{data_process(data[17])}\", amount: \"#{number_process(data[13])}\" }]"

        if number_process(data[13]) <= 0
          bookings_error_file.puts "#{link[0]} - #{tour_code}, data_process(data[0]), booking receipt amount error"
        end
      end
      booking_group_create += ")"

      bookings_seed_file.puts booking_group_create


      if !data.empty? && data.count % 42 == 0
        for i in (0..(data.count / 42)-1)
          bookings_create = "Booking.create(booking_group: bg, id: \"#{data_process(data[3+i*42])}\", tour_id: Tour.find_by_code(\"#{data_process(tour_code)}\").try(&:id), price: \"#{number_process(data[9+i*42])}\", price_type: \"#{data[10+i*42] == 'Yes' ? 'promo' : 'normal'}\", price_offered: #{data[10+i*42] == 'Yes' ? 'true' : 'false'}, designation: \"#{data_process(data[8+i*42])}\", name: \"#{data_process(data[2+i*42])}\", gender: \"#{gender_process(data[12+i*42])}\", mobile: \"#{data_process(data[35+i*42])}\", passport_number: \"#{data_process(data[25+i*42])}\", photo_received: #{data[30+i*42] == 'Yes' ? 'true' : 'false'}, own_visa: #{data[27+i*42] == 'Yes' ? 'true' : 'false'}, visa_no: \"#{data_process(data[28+i*42])}\", room_type: \"#{room_type_process(data[22+i*42])}\", extra_bed: \"#{data_process(data[24+i*42])}\", remark: \"#{data_process(data[36+i*42])}\", agent_confirmation: true, created_at: \"#{data_process(data[4+i*42])}\""

          if data[37+i*42] != "{Unknown}"
            bookings_create += ", last_edited_at: \"#{data_process(data[37+i*42])}\""
          end
          if !data[38+i*42].empty?
            bookings_create += ", last_edited_user_id: User.find_by_alias_name(\"#{data_process(data[38+i*42])}\").try(&:id)"
          end
          if !data[39+i*42].empty?
            bookings_create += ", agent_user_id: User.find_by_alias_name(\"#{data_process(data[39+i*42])}\").try(&:id), sale_rep: \"#{data_process(data[39+i*42])}\""
          end
          if !data[41+i*42].empty? && data[41+i*42].empty? != "(Unknown)"
            bookings_create += ", initiator_user_id: User.find_by_alias_name(\"#{data_process(data[41+i*42])}\").try(&:id)"
          end

          bookings_create += ", update_later: true, ignore_overbook: true)"

          bookings_seed_file.puts bookings_create
        end
      else
        bookings_error_file.puts "#{link[0]} - #{i} - #{tour_code}, data_process(data[0]), booking data.count error"
      end

    end
    bookings_seed_file.puts "Tour.find_by_code(\"#{data_process(tour_code)}\").try(:calculate_vacant_seats)"

  end

  if divide != nil && index != nil && divide == (index - 1)
    bookings_seed_file.puts "BookingGroup.after_crawling"
  else
    bookings_seed_file.puts "BookingGroup.after_crawling"
  end

  bookings_raw_file.close
  bookings_seed_file.close
  bookings_error_file.close

end

def crawl_cancellation
  cancellations_raw_file = CSV.open("./cancellations_raw.txt", 'a', {:col_sep => "\t"})
  cancellations_seed_file = File.open("./cancellations_seed.txt", 'a')
  cancellations_error_file = File.open("./cancellations_error.txt", 'a')
  cancellations_booking_ids_file = File.open("./cancellations_booking_ids.txt", 'a')

  cancellations_booking_ids = []
  
  range = ["2016-02", "2016-03", "2016-04", "2016-05", "2016-06", "2016-07", "2016-08", "2016-09", "2016-10", "2016-11", "2016-12", "2017-01", "2017-02", "2017-03", "2017-04", "2017-05", "2017-06", "2017-07", "2017-08", "2017-09", "2017-10", "2017-11", "2017-12", "2018-01", "2018-02"]
  # range = ["2018-01"]

  range.each do |yeardate|
    goto_page "http://iceholidays.sofarso.com/Operations/BookingsCancels.aspx"

    validation = @b.input(id: "__EVENTVALIDATION").value
    @b.select_list(:name, 'ctl00$cphMain$ddlType').select('0')

    Watir::Wait.until {@b.input(id: "__EVENTVALIDATION").value != validation}
    validation = @b.input(id: "__EVENTVALIDATION").value

    if @b.select_list(:id, 'ctl00_cphMain_ddlYM').value != yeardate
      @b.select_list(:name, 'ctl00$cphMain$ddlYM').select(yeardate)
      Watir::Wait.until {@b.input(id: "__EVENTVALIDATION").value != validation}
      validation = @b.input(id: "__EVENTVALIDATION").value
    end

    doc = Nokogiri::HTML(@b.html)

    while !doc.css("#ctl00_cphMain_gvw1_ctl02_lblDateCancelled").text.include?("/#{yeardate[-2..-1]}/")
      sleep 1
      doc = Nokogiri::HTML(@b.html)
    end

    doc = Nokogiri::HTML(@b.html)
    links = []
    current_page = 1

    begin
      next_page = ''
      links.concat doc.css('table td a[id$="lnkGrouping"]').map{|x| x['href']}
      doc.css('table tr.gvwPgrA table tr td a').each do |el|
        if next_page == ''
          ret = el['href'].match /Page\$(\d+)/
          # if ret.length == 2 && ((current_page+1) == el.text.to_i || ((current_page % 5) == 0 && el.text == '...'))
          if ret.length == 2 && (current_page+1) == ret[1].to_i
            next_page = el
            current_page += 1
            @b.execute_script('arguments[0].scrollIntoView();', @b.link(href: next_page['href']))
            @b.link(href: next_page['href']).click
            Watir::Wait.until {@b.input(id: "__EVENTVALIDATION").value != validation}
            validation = @b.input(id: "__EVENTVALIDATION").value
            doc = Nokogiri::HTML(@b.html)
          end
        end
      end
    end while next_page != ''

    links.each do |link|
      goto_page "http://iceholidays.sofarso.com/Operations/#{link}"
      validation = @b.input(id: "__EVENTVALIDATION").value
      sub_doc = Nokogiri::HTML(@b.html)
      data = []
      current_page = 1

      begin
        next_page = ''
        data.concat sub_doc.css('#ctl00_cphMain_lvw1_ipc1 table span').map(&:text)
        sub_doc.css("#ctl00_cphMain_lvw1_dpg1 a").each do |el|
          if next_page == ''
            if (current_page+1) == el.text.to_i || ((current_page % 5) == 0 && el.text == '...' && el != sub_doc.css("#ctl00_cphMain_lvw1_dpg1 a").first)
              next_page = el
              current_page += 1
              @b.execute_script('arguments[0].scrollIntoView();', @b.link(href: next_page['href']))
              @b.link(href: next_page['href']).click
              Watir::Wait.until {@b.input(id: "__EVENTVALIDATION").value != validation}
              validation = @b.input(id: "__EVENTVALIDATION").value
              sub_doc = Nokogiri::HTML(@b.html)
            end
          end
        end
      end while next_page != ''

      data.each do |c|
        c.gsub!(/"/,"\\\"")
        c.strip!
      end

      i = 0
      whole_group = ""
      while data.count > ((i+1)*25)
        if data[9+i*25] == 'Name in Chinese:'
          data[8+i*25].gsub!("#{data[9+i*25]}#{data[10+i*25]}", '')
          data.delete_at(9+i*25)
          data.delete_at(10+i*25)
        end

        data.delete_at(11+i*25) if data[11+i*25] == 'OFFERED'

        if data[19+i*25] == '(Whole Group)'
          whole_group = data[19+i*25]
          data.delete_at(19+i*25)
        end
        i = i + 1
      end

      cancellations_raw_file << [link, whole_group]
      cancellations_raw_file << data

      booking_group_create = "bg = BookingGroup.create(code: \"#{data_process(data[7])}\", created_at: \"#{data_process(data[4])}\""

      if !data_process(data[21]).empty? && data_process(data[21]).include?('xported on')
        booking_group_create += ", exported_remark: \"#{data_process(data[21])}\""
      end

      if data_process(data[13]) != ''
        booking_group_create += ", receipts_attributes: [{number: \"#{data_process(data[13])}\", amount: \"#{number_process(data[12])}\" }]"

        if number_process(data[12]) <= 0
          bookings_error_file.puts "#{link} - booking receipt amount error"
        end
      end
      booking_group_create += ")"



      if !data.empty? && data.count % 25 == 0

        amount = 0
        is_id_duplicated = false
        for i in (0..(data.count / 25)-1)
          amount += number_process(data[10+i*25])

          if cancellations_booking_ids.include?(data_process(data[2+i*25]))
            cancellations_error_file.puts "#{link} - duplicated booking id found: #{data[2+i*25]}"
            is_id_duplicated = true
          end
        end

        if !is_id_duplicated
          cancellations_seed_file.puts booking_group_create
          booking_alteration =  "ba = BookingAlteration.create(alter_type: :withdrawn, reason: \"#{data_process(data[19])}\", whole_group: #{whole_group == '(Whole Group)' ? 'true' : 'false'}, original_tour_id: Tour.find_by_code(\"#{data_process(data[0])}\").try(&:id), original_booking_group: bg, amount: \"#{amount}\", created_user_id: User.find_by_alias_name(\"#{data_process(data[14])}\").try(&:id), created_at: \"#{data_process(data[16+i*25])}\""
          if data_process(data[22]) != "(Unknown)" && data_process(data[23]) != "(Unknown)" && data_process(data[24]) != "0"
            booking_alteration += ", exported_user_id: User.find_by_alias_name(\"#{data_process(data[23])}\").try(&:id), exported_at: \"#{data_process(data[22])}\", exported_count: \"#{data_process(data[24])}\""
          end
          booking_alteration += ")"
          cancellations_seed_file.puts booking_alteration

          for i in (0..(data.count / 25)-1)
            bookings_create = "Booking.create(id: \"#{data_process(data[2+i*25])}\", booking_group: bg, booking_alteration: ba, tour_id: Tour.find_by_code(\"#{data_process(data[0+i*25])}\").try(&:id), price: \"#{number_process(data[10+i*25])}\", price_type: \"#{data[11+i*25] == 'Yes' ? 'promo' : 'normal'}\", price_offered: #{data[11+i*25] == 'Yes' ? 'true' : 'false'}, name: \"#{data_process(data[8+i*25])}\", mobile: \"#{data_process(data[9+i*25])}\", agent_confirmation: true, created_at: \"#{data_process(data[4+i*25])}\""

            if !data[15+i*25].empty?
              bookings_create += ", agent_user_id: User.find_by_alias_name(\"#{data_process(data[15+i*25])}\").try(&:id), sale_rep: \"#{data_process(data[15+i*25])}\""
            end

            bookings_create += ", update_later: true, ignore_overbook: true, status: :withdrawn)"

            cancellations_seed_file.puts bookings_create
            cancellations_booking_ids_file.puts data_process(data[2+i*25])

            if cancellations_booking_ids.include?(data_process(data[2+i*25]))
              cancellations_error_file.puts "#{link} - duplicated booking id #{data[2+i*25]}"
            end

            cancellations_booking_ids << data_process(data[2+i*25])
          end
        end

      else
        cancellations_error_file.puts "#{link} - #{i} cancelations count error"
      end
    end
  end

  cancellations_raw_file.close
  cancellations_seed_file.close
  cancellations_error_file.close
  cancellations_booking_ids_file.close

end



def crawl_transferred
  transferreds_raw_file = CSV.open("./transferreds_raw.txt", 'a', {:col_sep => "\t"})
  transferreds_seed_data_file = CSV.open("./transferreds_seed_data.txt", 'a', {:col_sep => "\t"})
  transferreds_error_file = File.open("./transferreds_error.txt", 'a')
  transferreds_booking_ids_file = File.open("./transferreds_booking_ids.txt", 'a')

  transferreds_booking_ids = []
  
  range = ["2016-02", "2016-03", "2016-04", "2016-05", "2016-06", "2016-07", "2016-08", "2016-09", "2016-10", "2016-11", "2016-12", "2017-01", "2017-02", "2017-03", "2017-04", "2017-05", "2017-06", "2017-07", "2017-08", "2017-09", "2017-10", "2017-11", "2017-12", "2018-01", "2018-02"]

  range.reverse.each do |yeardate|
    goto_page "http://iceholidays.sofarso.com/Operations/BookingsCancels.aspx"

    validation = @b.input(id: "__EVENTVALIDATION").value
    @b.select_list(:name, 'ctl00$cphMain$ddlType').select('1')

    Watir::Wait.until {@b.input(id: "__EVENTVALIDATION").value != validation}
    validation = @b.input(id: "__EVENTVALIDATION").value

    if @b.select_list(:id, 'ctl00_cphMain_ddlYM').value != yeardate
      @b.select_list(:name, 'ctl00$cphMain$ddlYM').select(yeardate)
      Watir::Wait.until {@b.input(id: "__EVENTVALIDATION").value != validation}
      validation = @b.input(id: "__EVENTVALIDATION").value
    end

    doc = Nokogiri::HTML(@b.html)

    while !doc.css("#ctl00_cphMain_gvw1_ctl02_lblDateCancelled").text.include?("/#{yeardate[-2..-1]}/")
      sleep 1
      doc = Nokogiri::HTML(@b.html)
    end

    doc = Nokogiri::HTML(@b.html)
    links = []
    current_page = 1

    begin
      next_page = ''
      links.concat doc.css('table td a[id$="lnkGrouping"]').map{|x| x['href']}
      doc.css('table tr.gvwPgrA table tr td a').each do |el|
        if next_page == ''
          ret = el['href'].match /Page\$(\d+)/
          # if ret.length == 2 && ((current_page+1) == el.text.to_i || ((current_page % 5) == 0 && el.text == '...'))
          if ret.length == 2 && (current_page+1) == ret[1].to_i
            next_page = el
            current_page += 1
            @b.execute_script('arguments[0].scrollIntoView();', @b.link(href: next_page['href']))
            @b.link(href: next_page['href']).click
            Watir::Wait.until {@b.input(id: "__EVENTVALIDATION").value != validation}
            validation = @b.input(id: "__EVENTVALIDATION").value
            doc = Nokogiri::HTML(@b.html)
          end
        end
      end
    end while next_page != ''

    links.each do |link|
      goto_page "http://iceholidays.sofarso.com/Operations/#{link}"
      validation = @b.input(id: "__EVENTVALIDATION").value
      sub_doc = Nokogiri::HTML(@b.html)
      data = []
      current_page = 1

      begin
        next_page = ''
        data.concat sub_doc.css('#ctl00_cphMain_lvw1_ipc1 table span').map(&:text)
        sub_doc.css("#ctl00_cphMain_lvw1_dpg1 a").each do |el|
          if next_page == ''
            if (current_page+1) == el.text.to_i || ((current_page % 5) == 0 && el.text == '...' && el != sub_doc.css("#ctl00_cphMain_lvw1_dpg1 a").first)
              next_page = el
              current_page += 1
              @b.execute_script('arguments[0].scrollIntoView();', @b.link(href: next_page['href']))
              @b.link(href: next_page['href']).click
              Watir::Wait.until {@b.input(id: "__EVENTVALIDATION").value != validation}
              validation = @b.input(id: "__EVENTVALIDATION").value
              sub_doc = Nokogiri::HTML(@b.html)
            end
          end
        end
      end while next_page != ''

      data.each do |c|
        c.gsub!(/"/,"\\\"")
        c.strip!
      end

      i = 0
      whole_group = ""
      while data.count > ((i+1)*25)
        if data[9+i*25] == 'Name in Chinese:'
          data[8+i*25].gsub!("#{data[9+i*25]}#{data[10+i*25]}", '')
          data.delete_at(9+i*25)
          data.delete_at(10+i*25)
        end

        data.delete_at(11+i*25) if data[11+i*25] == 'OFFERED'

        if data[19+i*25] == '(Whole Group)'
          whole_group = data[19+i*25]
          data.delete_at(19+i*25)
        end
        i = i + 1
      end

      transferreds_raw_file << [link, whole_group]
      transferreds_raw_file << data

      if !data.empty? && data.count % 25 == 0

        amount = 0
        alteration_booking_ids = []
        for i in (0..(data.count / 25)-1)
          amount += number_process(data[10+i*25])
          alteration_booking_ids << data_process(data[2+i*25])

          if transferreds_booking_ids.include?(data_process(data[2+i*25]))
            transferreds_error_file.puts "#{link} - duplicated booking id #{data[2+i*25]}"
          end
          transferreds_booking_ids << data_process(data[2+i*25])
        end

        exported_by = 0
        exported_at = 0
        exported_count = 0
        if data_process(data[22]) != "(Unknown)" && data_process(data[23]) != "(Unknown)" && data_process(data[24]) != "0"
          exported_by = data_process(data[23])
          exported_at = data_process(data[22])
          exported_count = data_process(data[24])
        end

        seed_data = [ data_process(data[0]), amount, data_process(data[19]), data_process(data[14]), data_process(data[16]), exported_by, exported_at, exported_count, alteration_booking_ids].flatten

        transferreds_seed_data_file << seed_data
        transferreds_booking_ids_file.puts alteration_booking_ids
      else
        transferreds_error_file.puts "#{link} - #{i} cancelations count error"
      end
    end
  end

  transferreds_raw_file.close
  transferreds_seed_data_file.close
  transferreds_error_file.close
  transferreds_booking_ids_file.close

end



def data_process(data)
  if data == '-' || data == nil
    ''
  else
    data
  end
end

def number_process(data)
  if data == '-' || data == '' || data == nil
    nil
  else
    data.gsub(/[^0-9\.]/,'').to_i
  end
end

def flight_date_process(data)
  if data == '-' || data == '' || data == nil
    nil
  else
    DateTime.strptime(data.gsub(/ /, ' ').concat(' +8'), '%d/%m/%y %H:%M %a %z')
  end
end

def gender_process(data)
  if data == 'Male'
    :male
  elsif data == 'Female'
    :female
  else
    nil
  end
end

ROOMTYPE_MAPPING = {"DBL" => "Double Bed", "TWN" => "Twin Sharing", "TRP" => "Triple Sharing", "CNB" => "Child No Bed" }

def room_type_process(data)
  if ROOMTYPE_MAPPING.has_key?(data)
    ROOMTYPE_MAPPING[data]
  else
    ""
  end
end

STATE_MAPPING = {"SELANGOR" => "Selangor", "JOHOR" => "Johor", "PAHANG" => "Pahang", "NEGERI SEMBILAN" => "Negeri Sembilan", "MELAKA" => "Melaka", "SABAH" => "Sabah", "SARAWAK" => "Sarawak", "NSembilan" => "Negeri Sembilan", "PENANG" => "Pulau Pinang", "Selangor" => "Selangor", "W.P. Kuala Lumpur" => "W.P. Kuala Lumpur", "PERAK" => "Perak", "Johor" => "Johor", "KUALA LUMPUR" => "W.P. Kuala Lumpur", "PULAU PINANG" => "Pulau Pinang", "Wilayah Persekutuan" => "W.P. Kuala Lumpur", "KELANTAN" => "Kelantan", "KEDAH" => "Kedah", "TERENGGANU" => "Terengganu", "Melaka" => "Melaka", "Kluang JOHOR" => "Johor", "Penang" => "Pulau Pinang", "Sabah" => "Sabah", "Terengganu" => "Terengganu", "Sarawak" => "Sarak", "Kuala Lumpur" => "W.P. Kuala Lumpur", "Pahang" => "Pahang", "JB Johor" => "Johor"}
def state_mapping(data)
  if data == '-'
    ''
  elsif STATE_MAPPING.has_key?(data)
    STATE_MAPPING[data]
  else
    data
  end
end


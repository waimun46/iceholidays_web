module FpxPayment
  require 'uri'

  if Rails.env.iceb2b?
    FPX_BANKLIST_URL = "#{Rails.application.credentials.fpx[:url]}/RetrieveBankList"
    FPX_STATUS_URL = "#{Rails.application.credentials.fpx[:url]}/sellerNVPTxnStatus.jsp"
  else
    FPX_BANKLIST_URL = "https://uat.mepsfpx.com.my/FPXMain/RetrieveBankList"
    FPX_STATUS_URL = "https://uat.mepsfpx.com.my/FPXMain/sellerNVPTxnStatus.jsp"
  end

  def get_fpx_banklist(payment)
    begin
      response = HTTParty.post(FPX_BANKLIST_URL, body: construct_message(payment), headers: {'Content-type' => 'application/x-www-form-urlencoded'})
    rescue Net::ReadTimeout => exception
      Rails.logger.info "Exported failed: Net::ReadTimeout"
      # return false
      return []
    rescue Net::OpenTimeout => exception
      Rails.logger.info "Exported failed: Net::OpenTimeout"
      # return false
      return []
    rescue Errno::ECONNRESET => exception
      Rails.logger.info "Exported failed: Errno::ECONNRESET"
      # return false
      return []
    end
    Rails.logger.info "FPX banklist response: #{response}"

    response_hash = URI::decode_www_form(response).to_h
    banklist_value = response_hash['fpx_bankList']
    banklist = []

    if banklist_value.present?
      list = banklist_value.split(',')
      list.each do |l|
        banklist << l.split('~')
      end
    end

    data = Rails.cache.fetch("fpx_banklist", expires_in: 1.hour) do
      payment.generate_banklist(banklist)
    end
    
    data
  end

  def construct_message(payment)
    seller_ex_id = Payment::FPX_SELLER_EX_ID
    msg_token = Payment::FPX_MSG_TOKEN
    msg_type = "BE"
    version = Payment::FPX_VERSION

    data_hash = {}
    add_message(data_hash, "fpx_msgType", msg_type)
    add_message(data_hash, "fpx_msgToken", msg_token)
    add_message(data_hash, "fpx_sellerExId", seller_ex_id)
    add_message(data_hash, "fpx_version", version)
    add_message(data_hash, "fpx_checkSum", Payment.fpx_banklist_checksum(msg_token, msg_type, seller_ex_id, version))

    Rails.logger.info "FPX banklist request: #{URI.encode_www_form(data_hash)}"
    URI.encode_www_form(data_hash)
  end

  def get_fpx_status(payment)
    begin
      response = HTTParty.post(FPX_STATUS_URL, body: construct_ae_message(payment), headers: {'Content-type' => 'application/x-www-form-urlencoded'})
    rescue Net::ReadTimeout => exception
      Rails.logger.info "Exported failed: Net::ReadTimeout"
      return false
    rescue Net::OpenTimeout => exception
      Rails.logger.info "Exported failed: Net::OpenTimeout"
      return false
    rescue Errno::ECONNRESET => exception
      Rails.logger.info "Exported failed: Errno::ECONNRESET"
      return false
    end
    Rails.logger.info "FPX AE response: #{response.squish}"

    response_hash = URI::decode_www_form(response.squish).to_h
    if payment.pending? && payment.fpx_update_status_and_action!(response_hash['fpx_debitAuthCode'], response_hash['fpx_fpxTxnId'])
      return response_hash['fpx_sellerOrderNo']
    else
      false
    end
    false
  end

  def construct_ae_message(payment)
    buyer_bank_id = payment.method
    buyer_email = payment.taggable.buyer_email
    seller_ex_id = Payment::FPX_SELLER_EX_ID
    msg_token = Payment::FPX_MSG_TOKEN
    msg_type = "AE"
    version = Payment::FPX_VERSION
    transaction_time = Time.now.in_time_zone.strftime("%Y%m%d%H%M%S")
    invoice_no = payment.generate_invoice_no
    amount = "#{payment.amount}.00"
    product_desc = payment.taggable.product_description
    seller_bankcode = "01"
    seller_id = Payment::FPX_SELLER_ID
    
    data_hash = {}

    add_message(data_hash, "fpx_buyerAccNo", "")
    add_message(data_hash, "fpx_buyerBankBranch", "")
    add_message(data_hash, "fpx_buyerBankId", buyer_bank_id)
    add_message(data_hash, "fpx_buyerEmail", buyer_email)
    add_message(data_hash, "fpx_buyerIban", "")
    add_message(data_hash, "fpx_buyerId", "")
    add_message(data_hash, "fpx_buyerName", "")
    add_message(data_hash, "fpx_makerName", "")
    add_message(data_hash, "fpx_msgToken", msg_token)
    add_message(data_hash, "fpx_msgType", msg_type)
    add_message(data_hash, "fpx_productDesc", product_desc)
    add_message(data_hash, "fpx_sellerBankCode", seller_bankcode)
    add_message(data_hash, "fpx_sellerExId", seller_ex_id)
    add_message(data_hash, "fpx_sellerExOrderNo", invoice_no)
    add_message(data_hash, "fpx_sellerId", seller_id)
    add_message(data_hash, "fpx_sellerOrderNo", invoice_no)
    add_message(data_hash, "fpx_sellerTxnTime", transaction_time)
    add_message(data_hash, "fpx_txnAmount", amount)
    add_message(data_hash, "fpx_txnCurrency", 'MYR')
    add_message(data_hash, "fpx_version", version)
    add_message(data_hash, "fpx_checkSum", Payment.fpx_generate_checksum(data_hash))

    Rails.logger.info "FPX AE request: #{URI.encode_www_form(data_hash)}"
    URI.encode_www_form(data_hash)
  end

  def add_message(mhash, key, value)
    if value.present?
      mhash[key] = value
    else
      mhash["#{key}/"] = nil
    end
  end
end
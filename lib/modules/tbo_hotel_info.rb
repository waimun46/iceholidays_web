module TBOHotelInfo
  def build_hotels
    hotel_seed = "#{Rails.root}/lib/records/tbo_hotels_seed.txt"
    lines = File.readlines(hotel_seed)
    lines.each do |line|
      row = line.sub("\n", '').split('^')
      # tbo_hotel_id^hotel_name^city_name^country_code^country_name^address_line_1^address_line_2^telephone^latitude^longitude
      city = TBOCity.search_by_name(row[2]).first

      if city.present?
        TBOHotel.create(hotel_code: row[0], hotel_name: row[1], city_name: row[2], city_code: city.code, country_name: row[4], address: "#{row[5]}, #{row[6]}", phone_number: row[7], map: "#{row[8]}|#{row[9]}")
      else
        next
      end
    end
  end
end
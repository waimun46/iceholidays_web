module ChangeCarrierwaveStoreDir
  def change_store_dir
    connection = Fog::Storage.new({
      provider: 'AWS',
      aws_access_key_id: Rails.application.credentials.aws[:access_key],
      aws_secret_access_key:  Rails.application.credentials.aws[:secret_key],
      region: 'ap-southeast-1',
      path_style: true
    })
    bucket = Rails.application.credentials.aws[:fog_directory]

    # Itinerary.all.each do |i|
    #   if i.file.path.to_s != ""
    #     new_full_path = i.file.path.to_s
    #     filename = i.file.identifier
    #     original_file_path = "#{i.file.old_store_dir}/#{filename}"
    #     begin
    #       connection.copy_object(bucket, original_file_path, bucket, new_full_path, 'x-amz-acl' => 'public-read')
    #       puts "Copied: #{original_file_path}"
    #     rescue
    #       puts "Failed copied: #{original_file_path}"
    #     end
    #   end
    # end
  end

  def delete_old_store_dir_file
    connection = Fog::Storage.new({
      provider: 'AWS',
      aws_access_key_id: Rails.application.credentials.aws[:access_key],
      aws_secret_access_key:  Rails.application.credentials.aws[:secret_key],
      region: 'ap-southeast-1',
      path_style: true
    })
    bucket = Rails.application.credentials.aws[:fog_directory]

    # Itinerary.all.each do |i|
    #   if i.file.path.to_s != ""
    #     filename = i.file.identifier
    #     original_file_path = "#{i.file.old_store_dir}/#{filename}"
    #     begin
    #       connection.delete_object(bucket, original_file_path)
    #       puts "Deleted: #{original_file_path}"
    #     rescue
    #       puts "Failed deleted: #{original_file_path}"
    #     end
    #   end
    # end

  end

end
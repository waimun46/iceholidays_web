module TravelPortApi

  HEADER = {'Content-type' => 'text/xml; charset=utf-8', 'Accept-Encoding': 'gzip,deflate', 'SOAPAction'=> ""}
  if Rails.env.iceb2b?
    URL_PATH = Rails.application.credentials.travelport[:url]
    AUTH = {:username => Rails.application.credentials.travelport[:username], :password => Rails.application.credentials.travelport[:password]}
    TARGET_BRANCH = Rails.application.credentials.travelport[:target_branch]
  else
    URL_PATH = "https://apac.universal-api.pp.travelport.com/B2BGateway/connect/uAPI/"
    AUTH = {:username => "Universal API/uAPI5095459404-60379594", :password => "aN&7L5x*%b"}
    TARGET_BRANCH = "P7082441"
  end

  GENDER_MAPPING = {
    "Miss" => "F",
    "Mr" => "M",
    "Mrs" => "F",
    "Ms" => "F",
    "Mstr" => "M",
    "Mdm" => "F"
  }

  def ping_travel_port
    content = Gyoku.xml({
      PingReq: { 
        '@TraceId': 'Test', 
        '@xmlns': 'http://www.travelport.com/schema/system_v32_0', 
        'BillingPointOfSaleInfo/': {
          '@xmlns': 'http://www.travelport.com/schema/common_v32_0', 
          '@OriginApplication':'uAPI', 
          content!: ''},
        Payload: 'User and password are correct.'
      }
    }, {key_converter: :camelcase})

    response = HTTParty.post(URL_PATH+'SystemService', body: construct_xml(content), headers: HEADER, basic_auth: AUTH)
    # Rails.logger.info response
    response_hash = Hash.from_xml(response.body)

    if response_hash.present? && response_hash.dig("Envelope", "Body", "PingRsp", "Payload") == 'User and password are correct.'
      true
    else
      false
    end

  end

  def search_trips(trips, adults = 1, children = 0, infants = 0, cabin_class = 'Economy')
    cache_data = REDIS.get("#{trips}-#{adults}-#{children}-#{infants}-#{cabin_class}")
    if cache_data != nil
      solutions = Marshal.load(cache_data)
    else
      ret_hash = low_fare_search(trips, adults, children, infants, cabin_class)
      solutions = process_low_fare_ret(ret_hash, cabin_class)
      if solutions.present?
        REDIS.setex("#{trips}-#{adults}-#{children}-#{infants}-#{cabin_class}", 900, Marshal.dump(solutions))
      end
    end
    solutions
  end

  def check_price_detail(trips, key, adults = 1, children = 0, infants = 0, cabin_class = 'Economy')
    cache_data = REDIS.get("#{trips}-#{key}-#{adults}-#{children}-#{infants}-#{cabin_class}")
    # Rails.logger.info "#{trips}-#{key}-#{adults}-#{children}-#{infants}-#{cabin_class}"
    if cache_data != nil
      solution = Marshal.load(cache_data)
    else
      ret_hash = air_price(trips, key, adults, children, infants, cabin_class)
      solution = process_air_price(ret_hash, cabin_class)
      if solution.present?
        REDIS.setex("#{trips}-#{key}-#{adults}-#{children}-#{infants}-#{cabin_class}", 300, Marshal.dump(solution))
      end
    end
    solution
  end

  def reservation(flight_booking_params, payment_params, trips, key, user, agent_rep = nil, adults = 1, children = 0, infants = 0, cabin_class = 'Economy')
    ret_hash, solution = air_reservation(flight_booking_params, trips, key, adults, children, infants, cabin_class)

    if ret_hash.present? && solution.present?
      flight_booking_group = process_air_reservation(ret_hash)
      if flight_booking_group.present?
        flight_booking_group.price = solution.markupTotalPrice
        if agent_rep == nil
          agent_user = user
        else
          agent_user = User.find_by_username(agent_rep)
        end

        flight_booking_group.save

        if payment_params[0][:gateway] == "public_bank"
          net_amount = solution.markupTotalPrice - (solution.markupTotalPrice * (Payment::PBB_CC_PERCENTAGE) / 100)
          redirect_url = Rails.application.routes.url_helpers.pbb_direct_path
        elsif payment_params[0][:gateway].include?("fpx")
          net_amount = solution.markupTotalPrice
          redirect_url = Rails.application.routes.url_helpers.fpx_direct_path
        else
          net_amount = solution.markupTotalPrice
          redirect_url = nil
        end
        flight_booking_group.payment = Payment.new(payment_params[0].merge(amount: solution.markupTotalPrice, taggable: flight_booking_group, callback: 0, user_id: user, net_amount: net_amount, redirect_url: redirect_url))

        flight_booking_group.flight_bookings.each_with_index do |flight_booking, index|
          if flight_booking_params[index].present? &&
            flight_booking.first_name == flight_booking_params[index][:first_name] && 
            flight_booking.last_name == flight_booking_params[index][:last_name] &&
            flight_booking.category == FlightBooking.category_mapping(flight_booking_params[index][:category])

            flight_booking.update_attributes(
              country: flight_booking_params[index][:country], 
              passport_number: flight_booking_params[index][:passport_number], 
              passport_issue_date: flight_booking_params[index][:passport_issue_date], 
              passport_expiry_date: flight_booking_params[index][:passport_expiry_date], 
              date_of_birth: flight_booking_params[index][:date_of_birth],
              remark: flight_booking_params[index][:remark],
              agent_user: agent_user,
              sale_rep: agent_rep,
              initiator_user: user
            )
          else
            flight_booking.update_attributes(
              agent_user: agent_user,
              sale_rep: agent_rep,
              initiator_user: user
            )
          end
        end
        flight_booking_group
      else
        nil
      end
    else
      nil
    end
  end

  def low_fare_search(trips, adults = 1, children = 0, infants = 0, cabin_class = 'Economy')
    Rails.logger.info "low_fare_search"

    search_air_leg = []
    trips.each do |trip|
      search_air_leg << {
        SearchOrigin: {
          'CityOrAirport/': {
          # 'Airport/': {
            '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
            '@Code': trip[:from].split(',')[0],
            '@PreferCity': 'true',
            content!: ''
          }
        },
        SearchDestination: {
          'CityOrAirport/': {
          # 'Airport/': {
            '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
            '@Code': trip[:to].split(',')[0],
            '@PreferCity': 'true',
            content!: ''
          }
        },
        'SearchDepTime/': {
          '@PreferredTime': trip[:depart],
          content!: ''
        },
        AirLegModifiers: {
          PreferredCabins: {
            'CabinClass/': {
              '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
              '@Type': cabin_class,
              content!: ''
            }
          }
          # 'FlightType/': {
          #   '@NonStopDirects': 'true',
          #   content!: ''
          # }
        }

      }
    end

    passengers = []
    adults.times do
      passengers << {
        '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
        '@Code': 'ADT',
        content!: ''
      }
    end
    children.times do
      passengers << {
        '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
        '@Code': 'CNN',
        '@Age': '8',
        content!: ''
      }
    end
    infants.times do
      passengers << {
        '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
        '@Code': 'INF',
        '@Age': '1',
        content!: ''
      }
    end

    content = Gyoku.xml({
      LowFareSearchReq: { 
        '@TraceId': 'Test', 
        '@xmlns': 'http://www.travelport.com/schema/air_v46_0',
        '@TargetBranch': TARGET_BRANCH,
        '@SolutionResult': 'true',
        'BillingPointOfSaleInfo/': {
          '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
          '@OriginApplication':'uAPI',
          content!: ''},
        SearchAirLeg: search_air_leg,
        AirSearchModifiers: {
          PreferredProviders: {
            'Provider/': {
              '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
              '@Code': '1G',
              content!: ''
            }
          }
        },
        'SearchPassenger/': passengers,
        AirPricingModifiers: {
          AccountCodes: {
            'AccountCode/': {
              '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
              '@Code': '-',
              content!: ''
            }
          }
        }
      }
    }, {key_converter: :camelcase})

    response = HTTParty.post(URL_PATH+'AirService', body: construct_xml(content), headers: HEADER, basic_auth: AUTH)
    # Rails.logger.info construct_xml(content)
    # Rails.logger.info response


    response_hash = Hash.from_xml(response.body)

    if response_hash.present?
      response_hash
    else
      nil
    end
  end

  def air_price(trips, key, adults = 1, children = 0, infants = 0, cabin_class = 'Economy')
    Rails.logger.info "air_price"

    solutions = search_trips(trips, adults, children, infants, cabin_class)
    if solutions.present?
      solution = solutions.solutions.find {|s| s.Key == key }

      if solution.present?
        passengers = []
        adults.times do |index|
          passengers << {
            '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
            '@Code': 'ADT',
            '@BookingTravelerRef': "ADT#{index+1}",
            content!: ''
          }
        end
        children.times do |index|
          passengers << {
            '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
            '@Code': 'CNN',
            '@BookingTravelerRef': "CNN#{index+1}",
            '@Age': '8',
            content!: ''
          }
        end
        infants.times do |index|
          passengers << {
            '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
            '@Code': 'INF',
            '@BookingTravelerRef': "INF#{index+1}",
            '@Age': '1',
            content!: ''
          }
        end

        air_itinerary = []
        air_pricing_command = []
        solution.Journeys.each do |journey|
          journey.AirSegments.each_with_index do |airsegment, index|

            data = airsegment.to_h.merge({ClassOfService: airsegment.extra.BookingCode}).except(:extra, :CodeshareInfo, :airline).transform_keys{|k| "@#{k}"}
            if index == 0
              data[:content!] = ''
            else
              data[:content!] = { 'Connection/': {content!: ''} }
            end

            air_itinerary << {
              'AirSegment/': data
            }

            air_pricing_command << {
              AirSegmentPricingModifiers: {
                '@AirSegmentRef': airsegment.Key,
                # '@FareBasisCode': airsegment.extra.FareBasisCode,
                PermittedBookingCodes: {
                  'BookingCode/': {
                    '@Code': airsegment.extra.BookingCode,
                    content!: ''
                  }
                }
              }
            }

          end
        end

        content = Gyoku.xml({
          AirPriceReq: { 
            '@TraceId': 'Test', 
            '@xmlns': 'http://www.travelport.com/schema/air_v46_0',
            '@TargetBranch': TARGET_BRANCH,
            'BillingPointOfSaleInfo/': {
              '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
              '@OriginApplication':'uAPI',
              content!: ''},
            AirItinerary: air_itinerary,
            AirPricingModifiers: {
              '@InventoryRequestType': 'DirectAccess'
            },
            'SearchPassenger/': passengers,
            AirPricingCommand: air_pricing_command
          }
        }, {key_converter: :camelcase, unwrap: [:AirPricingCommand, :AirItinerary]})

        # Rails.logger.info construct_xml(content)
        response = HTTParty.post(URL_PATH+'AirService', body: construct_xml(content), headers: HEADER, basic_auth: AUTH)
        # Rails.logger.info response

        response_hash = Hash.from_xml(response.body)

        if response_hash.present?
          doc = Nokogiri::XML(response.body)
          doc.remove_namespaces!
          host_tokens = doc.xpath("//HostToken")
          response_hash['HostToken'] = []
          doc.xpath("//HostToken").each do |host_token|
            response_hash['HostToken'] << { Key: host_token["Key"], Content: host_token.children.text }
          end
          response_hash
        else
          nil
        end
      else
        nil
      end
    else
      nil
    end
  end

  def air_reservation(flight_booking_params, trips, key, adults = 1, children = 0, infants = 0, cabin_class = 'Economy')
    Rails.logger.info "air_reservation"

    solution = check_price_detail(trips, key, adults, children, infants, cabin_class)

    if solution.present?
      travelers = []
      traveler_keys = []
      now = Time.now
      flight_booking_params.each_with_index do |flight_booking, index|
        traveler_keys << "#{flight_booking["category"]}#{index+1}"
        age = ((now - Date.parse(flight_booking["date_of_birth"]).to_time)/ 1.year.seconds).floor

        traveler = {
          '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
          '@TravelerType': flight_booking["category"],
          '@Key': "#{flight_booking["category"]}#{index+1}",
          '@Age': age,
          '@DOB': Date.parse(flight_booking["date_of_birth"]).strftime('%F'),
          content!: '',
          BookingTravelerName: {
            '@First': flight_booking["first_name"],
            '@Last': flight_booking["last_name"],
            '@Prefix': flight_booking["designation"],
            content!: ''
          },
          PhoneNumber: {
            '@Number': flight_booking[:mobile],
            content!: ''
          }
        }

        if flight_booking[:passport_number].present? && flight_booking[:passport_expiry_date].present?
          traveler[:SSR] = {
            '@Type': 'DOCS',
            '@Status': 'PN',
            # '@FreeText': "P/#{flight_booking[:country]}/#{flight_booking[:passport_number]}/#{flight_booking[:country]}/#{Date.parse(flight_booking[:date_of_birth]).strftime("%d%^b%y")}/#{GENDER_MAPPING[flight_booking[:designation]]}/#{Date.parse(flight_booking[:passport_expiry_date]).strftime("%d%^b%y")}/#{flight_booking[:first_name]}/#{flight_booking[:last_name]}",
            '@FreeText': "P////#{flight_booking[:passport_number]}/#{flight_booking[:country]}/#{Date.parse(flight_booking[:date_of_birth]).strftime("%d%^b%y")}/#{GENDER_MAPPING[flight_booking[:designation]]}/#{Date.parse(flight_booking[:passport_expiry_date]).strftime("%d%^b%y")}/#{flight_booking[:first_name]}/#{flight_booking[:last_name]}",
            content!: ''
          }
        end

        travelers << traveler

        if flight_booking["category"] == 'CNN'
          travelers.last[:NameRemark] = {
            '@Category': 'AIR',
            RemarkData: {
              content!: "P-C#{age}"
            }
          }
        end

      end

      air_pricing_segments = []
      solution.Journeys.map(&:AirSegments).flatten.each do |airsegment|
        airsegment.delete_field("extra")
        airsegment.delete_field("CodeshareInfo") if airsegment.CodeshareInfo.present?
        # airsegment.delete_field("FlightTime") if airsegment.FlightTime.present?
        # airsegment.FlightDetails.delete("FlightTime") if airsegment.FlightDetails["FlightTime"].present?
        data = airsegment.to_h.deep_transform_keys do |k|
          if k == :FlightDetails || k == :Connection
            "#{k}"
          else
            "@#{k}"
          end
        end
        if data.has_key?("Connection")
          data['Connection/'] = {content!: ''}
          data.delete('Connection')
        end

        air_pricing_segments << {
          'AirSegment/': data
        }
      end

      solution.AirPricingsData.each_with_index do |airpricingdata, index|
        airpricingdata.except!('FareCalc')
        if airpricingdata['FareInfo'].class.to_s == 'Array'
          airpricingdata['FareInfo'].map{|a| a.except!('FareRuleKey')}
          airpricingdata['FareInfo'].map{|a| a.except!('Endorsement')}
          airpricingdata['FareInfo'].map{|a| a.except!('Brand')}
          airpricingdata['FareInfo'].map{|a| a.except!('FareTicketDesignator')}
        else
          airpricingdata['FareInfo'].except!('FareRuleKey')
          airpricingdata['FareInfo'].except!('Endorsement')
          airpricingdata['FareInfo'].except!('Brand')
          airpricingdata['FareInfo'].except!('FareTicketDesignator')
        end
        airpricingdata.except!('ChangePenalty')
        airpricingdata.except!('CancelPenalty')

        if airpricingdata['BaggageAllowances']['BaggageAllowanceInfo'].class.to_s == 'Array'
          airpricingdata['BaggageAllowances']['BaggageAllowanceInfo'].map{|a| a.except!('URLInfo')}
        else
          airpricingdata['BaggageAllowances']['BaggageAllowanceInfo'].except!('URLInfo')
        end
        if airpricingdata['BaggageAllowances']['CarryOnAllowanceInfo'].class.to_s == 'Array'
          airpricingdata['BaggageAllowances']['CarryOnAllowanceInfo'].map{|a| a.except!('CarryOnDetails')}
        else
          airpricingdata['BaggageAllowances']['CarryOnAllowanceInfo'].except!('CarryOnDetails')
        end

        # if airpricingdata['BookingInfo'].class.to_s == 'Array'
        #   airpricingdata['BookingInfo'].map{|a| a.except!('HostTokenRef')}
        # else
        #   airpricingdata['BookingInfo'].except!('HostTokenRef')
        # end


        always_array(airpricingdata['PassengerType']).each do |passenger|
          match_key = traveler_keys.index{|k| k.include?(passenger['Code'])}
          passenger['BookingTravelerRef'] = traveler_keys.delete_at(match_key)
        end
        # airpricingdata['PassengerType']['BookingTravelerRef'] = "#{airpricingdata['PassengerType']['']}{index}"
        # airpricingdata['BaggageAllowances']['BaggageRestriction'].except!('CarryOnDetails')

        data = airpricingdata.deep_transform_keys do |k|
          if k == 'FareInfo' || k == 'FareSurcharge' || k == 'Brand' || k == 'BookingInfo' || k == 'TaxInfo' || k == 'PassengerType' || k == 'ChangePenalty' || k == 'CancelPenalty' || k == 'BaggageAllowances' || k == 'BaggageAllowanceInfo' || k == 'URLInfo' || k == 'TextInfo' || k == 'Text' || k == 'BagDetails' || k == 'BaggageRestriction' || k == 'URLInfo' || k == 'CarryOnAllowanceInfo'
            "#{k}"
          else
            "@#{k}"
          end
        end

        air_pricing_segments << {
          'AirPricingInfo/': data
        }
      end

      # solution.HostToken.each do |host_token|
      #   air_pricing_segments << {
      #     'HostToken': {
      #       '@Key': host_token[:Key],
      #       content!: host_token[:Content]
      #     }
      #   }
      # end

      Rails.logger.info(air_pricing_segments)

      content = Gyoku.xml({
        AirCreateReservationReq: { 
          '@TraceId': 'Test', 
          '@xmlns': 'http://www.travelport.com/schema/universal_v46_0',
          '@TargetBranch': TARGET_BRANCH,
          # '@RetainReservation': 'Schedule',
          '@RetainReservation': 'None',
          '@RestrictWaitlist': 'true',
          'BillingPointOfSaleInfo/': {
            '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
            '@OriginApplication':'uAPI',
            content!: ''},
          BookingTraveler: travelers,
          AirPricingSolution: {
            '@Key': solution.Key,
            '@TotalPrice': solution.TotalPrice,
            '@BasePrice': solution.BasePrice,
            '@ApproximateTotalPrice': solution.ApproximateTotalPrice,
            '@ApproximateBasePrice': solution.ApproximateBasePrice,
            '@Taxes': solution.Taxes,
            '@ApproximateTaxes': solution.ApproximateTaxes,
            '@QuoteDate': solution.QuoteDate,
            '@xmlns': 'http://www.travelport.com/schema/air_v46_0',
            content!: air_pricing_segments
          },
          ActionStatus: {
            '@xmlns': 'http://www.travelport.com/schema/common_v46_0',
            '@ProviderCode': '1G',
            '@Type': 'ACTIVE'
          }
        }
      }, {key_converter: :camelcase, unwrap: [:AirPricingSolution]})


      # Rails.logger.info construct_xml(content)
      response = HTTParty.post(URL_PATH+'AirService', body: construct_xml(content), headers: HEADER, basic_auth: AUTH)
      # Rails.logger.info response
      response_hash = Hash.from_xml(response.body)
      if response_hash.present?
        [response_hash, solution]
      else
        nil
      end
    else
      nil
    end

  end

  def process_low_fare_ret(content, cabin_class)
    if content.dig("Envelope","Body","LowFareSearchRsp","AirPricingSolution").present?
      temp = Hash.new
      temp["AirSegment"] = Hash.new
      content["Envelope"]["Body"]["LowFareSearchRsp"]["AirSegmentList"]["AirSegment"].each do |data|
        temp["AirSegment"][data["Key"]] = data
      end
      temp["FareInfo"] = Hash.new
      content["Envelope"]["Body"]["LowFareSearchRsp"]["FareInfoList"]["FareInfo"].each do |data|
        temp["FareInfo"][data["Key"]] = data
      end

      solutions = []
      airlines = []
      unknown_airline = Airline.find_by_name('Unknown')
      airline = nil

      content["Envelope"]["Body"]["LowFareSearchRsp"]["AirPricingSolution"].each do |solution_data|
        block = false
        solution = OpenStruct.new(Key: solution_data["Key"], TotalPrice: solution_data["TotalPrice"], Journeys: [], AirPricings: [], TotalTravelTime: 0)

        always_array(solution_data["Journey"]).each do |journey_data|
          travel_time_ret = journey_data["TravelTime"].match(/P(\d+)DT(\d+)H(\d+)M(\d+)S/)
          trave_time = travel_time_ret[1].to_i * 3600 * 24 + travel_time_ret[2].to_i * 3600 + travel_time_ret[3].to_i*60 + travel_time_ret[4].to_i
          solution.TotalTravelTime += trave_time
          journey = OpenStruct.new(TravelTime: trave_time, AirSegments: [])
          always_array(journey_data["AirSegmentRef"]).map(&:values).flatten.each do |airsegment_key|
            airsegment = OpenStruct.new(temp["AirSegment"][airsegment_key])
            airsegment.ProviderCode = airsegment.AirAvailInfo["ProviderCode"]
            airsegment.delete_field('FlightDetailsRef')
            airsegment.delete_field("AirAvailInfo")
            airsegment.delete_field("CodeshareInfo") if airsegment.CodeshareInfo.present?
            
            airline = Airline.find_by_iata_code(airsegment.Carrier)
            if airline.present?
              airlines << [airline.name, airline.iata_code]
              airsegment.airline = airline
              if airline.block?
                block = true
              end
            end

            bookinginfo = always_array(always_array(solution_data['AirPricingInfo']).first["BookingInfo"]).find{|bi| bi["SegmentRef"] == airsegment_key}
            max_weight = temp["FareInfo"][bookinginfo["FareInfoRef"]]["BaggageAllowance"]["MaxWeight"]
            fare_code =  temp["FareInfo"][bookinginfo["FareInfoRef"]]["FareBasis"]


            airsegment.extra = OpenStruct.new(
              MaxWeight: max_weight.present? ? max_weight.values.join(" ") : '',
              CabinClass: bookinginfo["CabinClass"],
              BookingCode: bookinginfo["BookingCode"],
              FareBasisCode: fare_code
            )

            journey.AirSegments << airsegment

          end

          solution.Journeys << journey

        end

        always_array(solution_data['AirPricingInfo']).each do |pricing|
          if airline.present?
            markup_price = airline.final_price(pricing["ApproximateBasePrice"].gsub(/[^\d\.]/, '').to_f, cabin_class.downcase)
          else
            markup_price = unknown_airline.final_price(pricing["ApproximateBasePrice"].gsub(/[^\d\.]/, '').to_f, cabin_class.downcase)
          end
          count = pricing["PassengerType"].class.to_s == 'Hash' ? 1 : pricing["PassengerType"].count

          taxes = (pricing["ApproximateTaxes"] || pricing["Taxes"]).gsub(/[^\d\.]/, '').to_f.ceil
          solution.AirPricings << OpenStruct.new(
            PassengerType: pricing["PassengerType"].class.to_s == 'Hash' ? pricing["PassengerType"]["Code"] :  pricing["PassengerType"].first["Code"],
            Count: count,
            TotalPrice: pricing["TotalPrice"].gsub(/[^\d\.]/, '').to_f,
            BasePrice: pricing["BasePrice"].gsub(/[^\d\.]/, '').to_f,
            Taxes: pricing["Taxes"].gsub(/[^\d\.]/, '').to_f,
            ApproximateBasePrice: pricing["ApproximateBasePrice"].gsub(/[^\d\.]/, '').to_f,
            ApproximateTaxes: taxes,
            markupBasePrice: markup_price,
            markupTotalPrice: markup_price + taxes,
            markupTotalAllPrice: (markup_price + taxes) * count
          )
        end
        solution.markupTotalPrice = solution.AirPricings.map(&:markupTotalAllPrice).inject(:+)

        unless block
          solutions << solution
        end

      end
      OpenStruct.new(solutions: solutions, airlines: airlines.uniq, prices_range: solutions.map(&:TotalPrice).map{|x| x.gsub(/[^\d\.]/, '').to_f}.minmax, times_range: solutions.map(&:TotalTravelTime).minmax)
    else
      nil
    end
  end

  def process_air_price(content, cabin_class)
    if content.dig("Envelope", "Body", 'AirPriceRsp', 'AirPriceResult', 'AirPricingSolution').present? && 
        content.dig("Envelope", "Body", 'AirPriceRsp', 'AirItinerary', 'AirSegment').present?
      solution_data = always_array(content["Envelope"]["Body"]["AirPriceRsp"]['AirPriceResult']["AirPricingSolution"]).first

      unknown_airline = Airline.find_by_name('Unknown')
      airline = nil
      block = false

      solution = OpenStruct.new(
        Key: solution_data["Key"],
        TotalPrice: solution_data["TotalPrice"],
        BasePrice: solution_data["BasePrice"],
        ApproximateTotalPrice: solution_data["ApproximateTotalPrice"],
        ApproximateBasePrice: solution_data["ApproximateBasePrice"],
        Taxes: solution_data["Taxes"],
        ApproximateTaxes: solution_data["ApproximateTaxes"],
        QuoteDate: solution_data["QuoteDate"],
        Journeys: [],
        AirPricings: [],
        AirPricingsData: [],
        HostToken: content["HostToken"],
      )

      always_array(content["Envelope"]["Body"]['AirPriceRsp']['AirItinerary']['AirSegment']).each do |airsegment_data|
        # first airsegment
        if !airsegment_data.has_key?('Connection')
          solution.Journeys << OpenStruct.new(AirSegments: [])
        end
        airsegment = OpenStruct.new(airsegment_data)
        bookinginfo = always_array(always_array(solution_data["AirPricingInfo"]).first['BookingInfo']).find{|bi| bi["SegmentRef"] == airsegment.Key}
        airsegment.extra = OpenStruct.new(CabinClass: bookinginfo["CabinClass"])

        airline = Airline.find_by_iata_code(airsegment.Carrier)
        if airline.present?
          airsegment.airline = airline
          if airline.block?
            block = true
          end
        end

        solution.Journeys.last.AirSegments << airsegment
      end

      always_array(solution_data['AirPricingInfo']).each do |pricing|
        if airline.present?
            markup_price = airline.final_price(pricing["ApproximateBasePrice"].gsub(/[^\d\.]/, '').to_f, cabin_class.downcase)
          else
            markup_price = unknown_airline.final_price(pricing["ApproximateBasePrice"].gsub(/[^\d\.]/, '').to_f, cabin_class.downcase)
          end
          count = pricing["PassengerType"].class.to_s == 'Hash' ? 1 : pricing["PassengerType"].count

        taxes = (pricing["ApproximateTaxes"] || pricing["Taxes"]).gsub(/[^\d\.]/, '').to_f.ceil


        solution.AirPricings << OpenStruct.new(
          PassengerType: pricing["PassengerType"].class.to_s == 'Hash' ? pricing["PassengerType"]["Code"] :  pricing["PassengerType"].first["Code"],
          Count: count,
          TotalPrice: pricing["TotalPrice"].gsub(/[^\d\.]/, '').to_f,
          BasePrice: pricing["BasePrice"].gsub(/[^\d\.]/, '').to_f,
          Taxes: pricing["Taxes"].gsub(/[^\d\.]/, '').to_f,
          ApproximateBasePrice: pricing["ApproximateBasePrice"].gsub(/[^\d\.]/, '').to_f,
          ApproximateTaxes: taxes,
          markupBasePrice: markup_price,
          markupTotalPrice: markup_price + taxes,
          markupTotalAllPrice: (markup_price + taxes) * count
        )
        solution.AirPricingsData << pricing
        # Rails.logger.info pricing
      end
      solution.markupTotalPrice = solution.AirPricings.map(&:markupTotalAllPrice).inject(:+)
      # Rails.logger.info solution.AirPricingsData

      if block
        nil
      else
        solution
      end
    else
      Rails.logger.info content["Envelope"]["Body"]
      nil
    end
  end

  def process_air_reservation(content)
    if content.dig("Envelope", "Body", 'AirCreateReservationRsp', 'UniversalRecord', 'AgencyInfo', 'AgentAction', 'ActionType').present? &&
        content["Envelope"]["Body"]['AirCreateReservationRsp']['UniversalRecord']['AgencyInfo']['AgentAction']['ActionType'] == "Created"
      flight_booking_group = FlightBookingGroup.new
      flight_booking_group.universal_code = content["Envelope"]["Body"]['AirCreateReservationRsp']['UniversalRecord']['LocatorCode']
      flight_booking_group.host_code = content["Envelope"]["Body"]['AirCreateReservationRsp']['UniversalRecord']['ProviderReservationInfo']['LocatorCode']
      flight_booking_group.uAPI_code = content["Envelope"]["Body"]['AirCreateReservationRsp']['UniversalRecord']['AirReservation']['LocatorCode']

      always_array(content["Envelope"]["Body"]['AirCreateReservationRsp']['UniversalRecord']['AirReservation']['AirSegment']).each do |air|
        flight_booking_detail = FlightBookingDetail.new(
          group: air["Group"],
          carrier: air["Carrier"],
          cabin_class: air["CabinClass"],
          flight_number: air["FlightNumber"],
          provider_code: air["ProviderCode"],
          origin: air["Origin"],
          origin_terminal: air["FlightDetails"].present? ? air["FlightDetails"]["OriginTerminal"] : '',
          destination: air["Destination"],
          destination_terminal: air["FlightDetails"].present? ? air["FlightDetails"]["DestinationTerminal"] : '',
          departure_time: DateTime.parse(air["DepartureTime"]),
          arrival_time: DateTime.parse(air["ArrivalTime"]),
          travel_time: air["TravelTime"],
          class_of_service: air["ClassOfService"],
          e_ticketability: air["ETicketability"],
          equipment: air["Equipment"],
          status: air["Status"],
          change_of_plane: air["ChangeOfPlane"],
          guaranteed_payment_carrier: air["GuaranteedPaymentCarrier"],
          provider_reservation_info_ref: air["ProviderReservationInfoRef"],
          travel_order: air["TravelOrder"],
          provider_segment_order: air["ProviderSegmentOrder"],
          optional_services_indicator: air["OptionalServicesIndicator"],
          participant_level: air["ParticipantLevel"],
          link_availability: air["LinkAvailability"],
          el_stat: air["ElStat"],
          automated_checkin: air["FlightDetails"].present? ? air["FlightDetails"]["AutomatedCheckin"] : '',
          connection: air["Connection"].present? ? true : false,
          sell_message: always_array(air["SellMessage"]).join("\n")
        )
        flight_booking_group.flight_booking_details << flight_booking_detail
      end

      travelers = always_array(content["Envelope"]["Body"]['AirCreateReservationRsp']['UniversalRecord']['BookingTraveler'])

      travelers.each do |traveler|
        flight_booking = FlightBooking.new(designation: traveler['BookingTravelerName']['Prefix'], first_name: traveler['BookingTravelerName']['First'], last_name: traveler['BookingTravelerName']['Last'], mobile: traveler['PhoneNumber']['Number'], category: FlightBooking.category_mapping(traveler['TravelerType']))
        flight_booking_group.flight_bookings << flight_booking
      end

      flight_booking_group
    else
      Rails.logger.info content["Envelope"]["Body"]
      nil
    end
  end

  def always_array(val)
    if val.class.to_s == 'Array'
      val
    else
      [val]
    end
  end

  def highest_cabin_class(cabin1, cabin2)

  end


  def construct_xml(content)
    "<Envelope xmlns='http://schemas.xmlsoap.org/soap/envelope/' ><Body>#{content}</Body></Envelope>"
  end

end
module ExportEcard
  EXPORTED_VALIDKEY = 'sofarso'
  URL = "http://13.229.12.58/ETravel_ICE/Webservice/ETravelWebService.asmx?op=CreateNewTour"

  def export_to_ecard(booking_group)
    if Rails.env.iceb2b?
      begin
        response = HTTParty.post(URL, body: construct_message(booking_group), headers: {'Content-type' => 'text/xml; charset=utf-8', 'SOAPAction': 'http://tempuri.org/CreateNewTour'})
      rescue Net::ReadTimeout => exception
        Rails.logger.info "Exported failed: Net::ReadTimeout"
        return false
      rescue Net::OpenTimeout => exception
        Rails.logger.info "Exported failed: Net::OpenTimeout"
        return false
      rescue Errno::ECONNRESET => exception
        Rails.logger.info "Exported failed: Errno::ECONNRESET"
        return false
      end
      Rails.logger.info "Exported response: #{response}"
      if response.body.include?("Tour save successfully.") && response.body.include?("Flight save successfully.") && response.body.include?("Pax save successfully.")
        true
      else
        Rails.logger.info "Exported failed: Response isn't successfully"
        false
      end
    else
      construct_message(booking_group)
      true
    end
  end

  def construct_message(booking_group)
    if booking_group.bookings.present?
      tour = booking_group.bookings.first.tour

      tour_header = Gyoku.xml({
        TourHeader:{
          TourRefNo: tour.id,
          TourCode: tour.code,
          Caption: tour.caption,
          CaptionChinese: tour.other_caption,
          DepartureDate: tour.departure_date.strftime("%FT%T")
        }
      }, {key_converter: :none})
      
      tour_flight = {Flight:{}}
      flight_array = []
      tour.tour_flights.order(:departure).each do |flight|
        flight_hash = {}
        add_message(flight_hash, "FlightRefNo", flight.flight_no)
        add_message(flight_hash, "TourCode", tour.code)
        add_message(flight_hash, "DepartureDate", flight.departure.strftime("%FT%R"))
        add_message(flight_hash, "DepartureTime", flight.departure.strftime("%R"))
        add_message(flight_hash, "ArrivalDate", flight.arrival.strftime("%FT%R"))
        add_message(flight_hash, "ArrivalTime", flight.arrival.strftime("%R"))
        add_message(flight_hash, "FlightFrom", flight.from)
        add_message(flight_hash, "FlightTo", flight.to)
        add_message(flight_hash, "FlightNo", flight.flight_no)

        if flight_hash.present?
          flight_array << flight_hash
        end
      end

      if flight_array.present?
        add_message(tour_flight[:Flight], "TourFlight", flight_array)
      else
        flight_hash = {}
        add_message(flight_hash, "FlightRefNo", nil)
        add_message(flight_hash, "TourCode", nil)
        add_message(flight_hash, "DepartureDate", nil)
        add_message(flight_hash, "DepartureTime", nil)
        add_message(flight_hash, "ArrivalDate", nil)
        add_message(flight_hash, "ArrivalTime", nil)
        add_message(flight_hash, "FlightFrom", nil)
        add_message(flight_hash, "FlightTo", nil)
        add_message(flight_hash, "FlightNo", nil)
        flight_array << flight_hash
        add_message(tour_flight[:Flight], "TourFlight", flight_array)
      end

      tour_flight = Gyoku.xml(tour_flight, {key_converter: :none})

      tour_pax = {Pax:{}}
      booking_array = []
      booking_group.bookings.visible.order(:id).each do |booking|
        booking_hash = {}
        add_message(booking_hash, "PaxRefNo", booking.id)
        add_message(booking_hash, "TourCode", tour.code)
        # add_message(booking_hash, "GroupingNo", "Testing - #{booking_group.code}")
        add_message(booking_hash, "GroupingNo", booking_group.code)
        add_message(booking_hash, "BookDate", booking.created_at.strftime("%FT%T"))
        add_message(booking_hash, "FullName", booking.name)
        add_message(booking_hash, "Price", booking.export_price)
        if booking.agent_user.present?
          add_message(booking_hash, "AgentCode", booking.agent_user.try(:code))
          add_message(booking_hash, "AgentName", booking.agent_user.try(:alias_name))
        else
          add_message(booking_hash, "AgentCode", nil)
          add_message(booking_hash, "AgentName", booking.sale_rep)
        end
        add_message(booking_hash, "TitleOfName", booking.designation)
        add_message(booking_hash, "Gender", booking.gender)
        add_message(booking_hash, "ContactNo", booking.mobile)

        if booking_hash.present?
          booking_array << booking_hash
        end
      end
      if booking_array.present?
        add_message(tour_pax[:Pax], "TourPax", booking_array)
      else
        add_message(tour_pax[:Pax], "TourPax", nil)
      end
      tour_pax = Gyoku.xml(tour_pax, {key_converter: :none})


      message = Gyoku.xml({
        validKey: EXPORTED_VALIDKEY,
        stourHeader: tour_header,
        stourFlight: tour_flight,
        stourPax: tour_pax
      })

      Rails.logger.info "Export message: #{message}"

      "<?xml version='1.0' encoding='utf-8'?><soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body><CreateNewTour xmlns='http://tempuri.org/'>#{message}<resultTable>xmlxml</resultTable></CreateNewTour></soap:Body></soap:Envelope>"
    else
      nil
    end
  end

  def add_message(mhash, key, value)
    if value.present?
      mhash[key] = value
    else
      mhash["#{key}/"] = nil
    end
  end


end
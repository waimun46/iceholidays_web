module ExportChubb
  if Rails.env.iceb2b?
    URL = "https://receive.travel.acegroup.com/CRS_ACORD_WS/ACORDService.asmx"
    USERNAME = "iceholidays.svc"
    PASSWORD = "cbI-pR$fX"
  else
    URL = "https://btsws.atuat.acegroup.com/CRS_ACORD_WS/ACORDService.asmx"
    USERNAME = "iceholidays.svc"
    PASSWORD = "$craMBLeD@123!"
  end
  # header = {"Content-Type"=>"text/xml"}

  def export_to_chubb(booking, status)
    if Rails.env.iceb2b? || Rails.env.testb2b?
      begin
        response = HTTParty.post(URL, body: construct_create_message(booking, status), headers: {"Content-Type"=>"text/xml"})
      rescue Net::ReadTimeout => exception
        Rails.logger.info "Exported failed: Net::ReadTimeout"
        return false
      rescue Net::OpenTimeout => exception
        Rails.logger.info "Exported failed: Net::OpenTimeout"
        return false
      rescue Errno::ECONNRESET => exception
        Rails.logger.info "Exported failed: Errno::ECONNRESET"
        return false
      end
      Rails.logger.info "Exported response: #{response}"

      response_hash = Hash.from_xml(response.body)
      policy_details = {}

      if status == "new"
        if response_hash.present? && response_hash.dig("Envelope", "Body", "GetTravelPolicyResponse", "ACORD", "InsuranceSvcRs", "PersPkgPolicyAddRs", "MsgStatus", "MsgStatusCd") == 'Success'
          response_hash.dig("Envelope", "Body", "GetTravelPolicyResponse", "ACORD", "InsuranceSvcRs", "PersPkgPolicyAddRs", "com.acegroup_DataExtensions", "DataItem").select { |data|
            if data['key'] == "PolicySchedule" && data['value'].present?
              add_message(policy_details, "insurance_cert", data['value'])
            end
          }

          policy_no = response_hash.dig("Envelope", "Body", "GetTravelPolicyResponse", "ACORD", "InsuranceSvcRs", "PersPkgPolicyAddRs", "PersPolicy", "PolicyNumber")
          add_message(policy_details, "policy_no", policy_no)

          return policy_details
        else
          false
        end
      else
        if response_hash.present? && response_hash.dig("Envelope", "Body", "GetTravelEndorsementResponse", "ACORD", "InsuranceSvcRs", "PersPkgPolicyModRs", "MsgStatus", "MsgStatusDesc") == 'Policy Endorsed'
          response_hash.dig("Envelope", "Body", "GetTravelEndorsementResponse", "ACORD", "InsuranceSvcRs", "PersPkgPolicyModRs", "com.acegroup_DataExtensions", "DataItem").select { |data|
            if data['key'] == "PolicySchedule" && data['value'].present?
              add_message(policy_details, "insurance_cert", data['value'])
            end
          }

          policy_no = response_hash.dig("Envelope", "Body", "GetTravelEndorsementResponse", "ACORD", "InsuranceSvcRs", "PersPkgPolicyModRs", "PersPolicy", "PolicyNumber")
          add_message(policy_details, "policy_no", policy_no)

          return policy_details
        else
          false
        end
      end
      false
    else
      construct_create_message(booking, status)
      true
    end
  rescue REXML::ParseException => exception
    false
  end

  def construct_create_message(booking, status)
    if booking.present?
      current_time = Time.now.in_time_zone.xmlschema

      gender_mapping = {
        "Miss" => "F",
        "Mr" => "M",
        "Mrs" => "F",
        "Ms" => "F",
        "Mstr" => "M",
        "Mdm" => "F"
      }

      if booking.has_attribute?(:guardian_id)
        tour = booking.booking_group.tour
        if booking.agent_user.present?
          agent = booking.agent_user
        else
          agent = booking.initiator_user
        end
        insurance_receiver = booking.booking_group.insurance_receiver
        gender = gender_mapping[booking.designation]
        title = booking.designation

        if booking.over_18?
          relationship = "Self"
        else
          relationship = "Child"
        end
      else
        tour = booking.booking.booking_group.tour
        if booking.agent_user.present?
          agent = booking.booking.agent_user
        else
          agent = booking.booking.initiator_user
        end
        insurance_receiver = booking.booking.booking_group.insurance_receiver
        relationship = "Child"
        gender = booking.gender[0].capitalize
        title = ""
      end

      if !booking.insurance_nomination_flag? && booking.over_18?
        nomination = booking.insurance_nomination.map{|v| !v.any?{|k,v| v.blank? } ? v : '' }.reject(&:empty?)
        nominee_surname = nomination.map{|v| v["nominee_surname"] }.join(";")
        nominee_given_name = nomination.map{|v| v["nominee_given_name"] }.join(";")
        nominee_id = nomination.map{|v| v["nominee_passport_number"] }.join(";")
        nominee_dob = nomination.map{|v| v["nominee_date_of_birth"] }.join(";")
        nominee_relationship = nomination.map{|v| v["nominee_relationship"] }.join(";")
        nominee_share = nomination.map{|v| v["nominee_share"] }.join(";")
      end

      general_info_array = []
      phone_hash = {}
      email_hash = {}
      policy_holder_hash = {}
      policy_holder_name = {}
      policy_holder = {}

      policy_holder_det = booking.policy_holder_detail

      if policy_holder_det.present?
        name_split = policy_holder_det[:name].split(' ')
        det_surname = name_split[0]

        if name_split.length == 1
          det_given = '.'
        else
          det_given = name_split[1..-1].join(' ')
        end

        add_message(policy_holder_name, "Surname", det_surname)
        add_message(policy_holder_name, "GivenName", det_given)
        add_message(policy_holder_name, "TitlePrefix", policy_holder_det[:designation])

        add_message(policy_holder_hash, "@id", "1")
        add_message(policy_holder_hash, "PersonName", policy_holder_name)
        add_message(policy_holder_hash, "BirthDt", policy_holder_det[:dob].strftime("%Y-%m-%d"))
        add_message(policy_holder_hash, "Gender", gender_mapping[policy_holder_det[:designation]])
        add_message(policy_holder_hash, "IdNumberTypeCd", "Passport")
        add_message(policy_holder_hash, "IdNumber", policy_holder_det[:passport_number])
        add_message(policy_holder, "NameInfo", policy_holder_hash)

        if policy_holder_hash.present?
          general_info_array << policy_holder
        end
      end

      name_info = {}
      name_info_hash = {}
      name_hash = {}
      
      name_split = booking.name.split(' ')
      surname = name_split[0]

      if name_split.length == 1
        given = '.'
      else
        given = name_split[1..-1].join(' ')
      end

      add_message(name_hash, "Surname", surname)
      add_message(name_hash, "GivenName", given)
      add_message(name_hash, "TitlePrefix", title)

      add_message(name_info_hash, "@id", "2")
      add_message(name_info_hash, "PersonName", name_hash)
      add_message(name_info_hash, "TitleRelationshipDesc", relationship)
      add_message(name_info_hash, "BirthDt", booking.date_of_birth.strftime("%Y-%m-%d"))
      add_message(name_info_hash, "Gender", gender)
      add_message(name_info_hash, "IdNumberTypeCd", "Passport")
      add_message(name_info_hash, "IdNumber", booking.passport_number)
      add_message(name_info, "NameInfo", name_info_hash)

      if name_info_hash.present?
        general_info_array << name_info
      end

      if agent.present?
        address_hash = {}
        communication_hash = {}
        addr_comm_hash = {}
       
        # Address
        add_message(address_hash, "@id", "2001")
        add_message(address_hash, "@NameInfoRef", "1")
        add_message(address_hash, "Addr1", "N/A")
        add_message(address_hash, "City", "N/A")
        add_message(address_hash, "StateProv", "N/A")
        add_message(address_hash, "PostalCode", "N/A")
        add_message(address_hash, "Country", "N/A")
        add_message(addr_comm_hash, "Addr", address_hash)

        #Communications
        add_message(phone_hash, "PhoneTypeCd", "Telephone")
        add_message(phone_hash, "PhoneNumber", agent.phone)
        add_message(communication_hash, "PhoneInfo", phone_hash)
        add_message(email_hash, "EmailAddr", insurance_receiver)
        add_message(communication_hash, "EmailInfo", email_hash)
        add_message(addr_comm_hash, "Communications", communication_hash)

        general_info_array << addr_comm_hash
      end

      # get destination
      destination_mapping = {
        "Zone 1" => "ECBD2B10-0072-492D-B322-A60A006AB017",
        "Zone 2" => "2111EFAA-07A5-4B25-8D4D-E4DC90C9C899",
        "Zone 3" => "17FA165E-0F71-435D-A7A2-78996B3273D6",
        "Domestic" => "E8FD1ED6-C011-4CE8-8510-CA054DB899E0"
      }

      if status == "new"
        if !booking.insurance_nomination_flag? && booking.over_18?
          message = Gyoku.xml({
          "InsuranceSvcRq": {
            "RqUID": "94c5b6ae-01a6-4263-9cf0-c1fd55ddf70d",
            "PersPkgPolicyAddRq": {
              "TransactionRequestDt": "#{current_time}",
              "InsuredOrPrincipal": {
                "GeneralPartyInfo": general_info_array
              },
              "PersPolicy": {
                "CompanyProductCd": "3B7DC5CB-53F7-4F96-90D0-A60A006A1942",
                "ContractTerm": { 
                  "EffectiveDt": tour.departure.strftime("%Y-%m-%d"),
                  "ExpirationDt": tour.arrival.strftime("%Y-%m-%d")
                },
                "com.acegroup_Destination": {
                  "RqUID": destination_mapping[tour.zone],
                  "DestinationDesc": tour.zone
                },
                "com.acegroup_InsuredPackage": {
                  "RqUID": "851CFFF2-ED6E-4835-A7FD-9D0D0019C474",
                  "InsuredPackageDesc": "Individual"
                },
                "com.acegroup_Plan": {
                  "RqUID": "C96EE067-490D-4CF0-A588-A60A006A60D9",
                  "PlanDesc": "Chubb Executive"
                }
              },
              "com.acegroup_DataExtensions": [
                {
                  "DataItem": {
                    "@key": "BookingReference",
                    "@type": "System.String",
                    content!: {:value => tour.code }
                  }
                },
                {
                  "DataItem": {
                    "@key": "Source_System",
                    "@type": "System.String",
                    content!: {:value => "ICEHOLIDAYS" }
                  }
                },
                {
                  "DataItem": {
                    "@key": "PaymentMethod",
                    "@type": "System.String",
                    content!: {:value => "Credit" }
                  }
                },
                {
                  "DataItem": {
                    "@key": "HasDefaultBeneficiary",
                    "@type": "System.Boolean",
                    content!: {:value => booking.insurance_nomination_flag }
                  }
                },
                {
                  "DataItem": {
                    "@key": "BENEFICIARYSURNAME",
                    "@type": "System.String",
                    content!: {:value => nominee_surname }
                  }
                },
                {
                  "DataItem": {
                    "@key": "BENEFICIARYGIVENNAME",
                    "@type": "System.String",
                    content!: {:value => nominee_given_name }
                  }
                },
                {
                  "DataItem": {
                    "@key": "BENEFICIARYIDNUMBER",
                    "@type": "System.String",
                    content!: {:value => nominee_id }
                  }
                },
                {
                  "DataItem": {
                    "@key": "BENEFICIARYDOB",
                    "@type": "System.String",
                    content!: {:value => nominee_dob }
                  }
                },
                {
                  "DataItem": {
                    "@key": "BENEFICIARYRELATIONSHIP",
                    "@type": "System.String",
                    content!: {:value => nominee_relationship }
                  }
                },
                {
                  "DataItem": {
                    "@key": "BENEFICIARYSHARE",
                    "@type": "System.String",
                    content!: {:value => nominee_share }
                  }
                },
                {
                  "DataItem": {
                    "@key": "BENEFICIARYWITNESSSURNAME",
                    "@type": "System.String",
                    content!: {:value => booking.witness_details['witness_surname'] }
                  }
                },
                {
                  "DataItem": {
                    "@key": "BENEFICIARYWITNESSGIVENNAME",
                    "@type": "System.String",
                    content!: {:value => booking.witness_details['witness_given_name'] }
                  }
                },
                {
                  "DataItem": {
                    "@key": "BENEFICIARYWITNESSIDNUMBER",
                    "@type": "System.String",
                    content!: {:value => booking.witness_details['witness_passport_number'] }
                  }
                }                      
              ]
            }
          }
        }, {key_converter: :none, unwrap: true})
        else
          message = Gyoku.xml({
            "InsuranceSvcRq": {
              "RqUID": "94c5b6ae-01a6-4263-9cf0-c1fd55ddf70d",
              "PersPkgPolicyAddRq": {
                "TransactionRequestDt": "#{current_time}",
                "InsuredOrPrincipal": {
                  "GeneralPartyInfo": general_info_array
                },
                "PersPolicy": {
                  "CompanyProductCd": "3B7DC5CB-53F7-4F96-90D0-A60A006A1942",
                  "ContractTerm": { 
                    "EffectiveDt": tour.departure.strftime("%Y-%m-%d"),
                    "ExpirationDt": tour.arrival.strftime("%Y-%m-%d")
                  },
                  "com.acegroup_Destination": {
                    "RqUID": destination_mapping[tour.zone],
                    "DestinationDesc": tour.zone
                  },
                  "com.acegroup_InsuredPackage": {
                    "RqUID": "851CFFF2-ED6E-4835-A7FD-9D0D0019C474",
                    "InsuredPackageDesc": "Individual"
                  },
                  "com.acegroup_Plan": {
                    "RqUID": "C96EE067-490D-4CF0-A588-A60A006A60D9",
                    "PlanDesc": "Chubb Executive"
                  }
                },
                "com.acegroup_DataExtensions": [
                  {
                    "DataItem": {
                      "@key": "BookingReference",
                      "@type": "System.String",
                      content!: {:value => tour.code }
                    }
                  },
                  {
                    "DataItem": {
                      "@key": "Source_System",
                      "@type": "System.String",
                      content!: {:value => "ICEHOLIDAYS" }
                    }
                  },
                  {
                    "DataItem": {
                      "@key": "PaymentMethod",
                      "@type": "System.String",
                      content!: {:value => "Credit" }
                    }
                  },
                  {
                    "DataItem": {
                      "@key": "HasDefaultBeneficiary",
                      "@type": "System.Boolean",
                      content!: {:value => booking.over_18? ? booking.insurance_nomination_flag : true  }
                    }
                  }                      
                ]
              }
            }
          }, {key_converter: :none, unwrap: true})
        end

        Rails.logger.info "Export message: #{message}"

        "<?xml version='1.0' encoding='utf-8'?><soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body><GetTravelPolicy xmlns='http://ACE.Global.Travel.CRS.Schemas.ACORD.WS/'><ACORD xmlns='http://ACE.Global.Travel.CRS.Schemas.ACORD_PolicyReq'><SignonRq><SignonPswd><CustId><SPName>ICEHOLIDAYS</SPName><CustLoginId>#{USERNAME}</CustLoginId></CustId><CustPswd><EncryptionTypeCd>None</EncryptionTypeCd><Pswd>#{PASSWORD}</Pswd></CustPswd></SignonPswd><ClientDt>#{current_time}</ClientDt><CustLangPref>en</CustLangPref><ClientApp><Org>ICE HOLIDAYS SDN BHD (INTEGRATED)</Org><Name>ICE HOLIDAYS SDN BHD (INTEGRATED)</Name><Version>1.0</Version></ClientApp></SignonRq>#{message}</ACORD></GetTravelPolicy></soap:Body></soap:Envelope>"
      else # status == "endorsement"
        if !booking.insurance_nomination_flag? && booking.over_18?
          message = Gyoku.xml({
            "InsuranceSvcRq": {
              "RqUID": "94c5b6ae-01a6-4263-9cf0-c1fd55ddf70d",
              "PersPkgPolicyModRq": {
                "TransactionRequestDt": "#{current_time}",
                "InsuredOrPrincipal": {
                  "GeneralPartyInfo": general_info_array
                },
                "PersPolicy": {
                  "CompanyProductCd": "3B7DC5CB-53F7-4F96-90D0-A60A006A1942",
                  "ContractTerm": { 
                    "EffectiveDt": tour.departure.strftime("%Y-%m-%d"),
                    "ExpirationDt": tour.arrival.strftime("%Y-%m-%d")
                  },
                  "com.acegroup_Destination": {
                    "RqUID": destination_mapping[tour.zone],
                    "DestinationDesc": tour.zone
                  },
                  "com.acegroup_InsuredPackage": {
                    "RqUID": "851CFFF2-ED6E-4835-A7FD-9D0D0019C474",
                    "InsuredPackageDesc": "Individual"
                  },
                  "com.acegroup_Plan": {
                    "RqUID": "C96EE067-490D-4CF0-A588-A60A006A60D9",
                    "PlanDesc": "Chubb Executive"
                  }
                },
                "com.acegroup_DataExtensions": [
                  {
                    "DataItem": {
                      "@key": "PolicyCertificateNumber",
                      "@type": "System.String",
                      content!: {:value => booking.policy_number }
                    }
                  },
                  {
                    "DataItem": {
                      "@key": "HasDefaultBeneficiary",
                      "@type": "System.Boolean",
                      content!: {:value => booking.insurance_nomination_flag }
                    }
                  },
                  {
                    "DataItem": {
                      "@key": "BENEFICIARYSURNAME",
                      "@type": "System.String",
                      content!: {:value => nominee_surname }
                    }
                  },
                  {
                    "DataItem": {
                      "@key": "BENEFICIARYGIVENNAME",
                      "@type": "System.String",
                      content!: {:value => nominee_given_name }
                    }
                  },
                  {
                    "DataItem": {
                      "@key": "BENEFICIARYIDNUMBER",
                      "@type": "System.String",
                      content!: {:value => nominee_id }
                    }
                  },
                  {
                    "DataItem": {
                      "@key": "BENEFICIARYDOB",
                      "@type": "System.String",
                      content!: {:value => nominee_dob }
                    }
                  },
                  {
                    "DataItem": {
                      "@key": "BENEFICIARYRELATIONSHIP",
                      "@type": "System.String",
                      content!: {:value => nominee_relationship }
                    }
                  },
                  {
                    "DataItem": {
                      "@key": "BENEFICIARYSHARE",
                      "@type": "System.String",
                      content!: {:value => nominee_share }
                    }
                  },
                  {
                    "DataItem": {
                      "@key": "BENEFICIARYWITNESSSURNAME",
                      "@type": "System.String",
                      content!: {:value => booking.witness_details['witness_surname'] }
                    }
                  },
                  {
                    "DataItem": {
                      "@key": "BENEFICIARYWITNESSGIVENNAME",
                      "@type": "System.String",
                      content!: {:value => booking.witness_details['witness_given_name'] }
                    }
                  },
                  {
                    "DataItem": {
                      "@key": "BENEFICIARYWITNESSIDNUMBER",
                      "@type": "System.String",
                      content!: {:value => booking.witness_details['witness_passport_number'] }
                    }
                  }
                ]
              }
            }
          }, {key_converter: :none, unwrap: true})
        else
          message = Gyoku.xml({
            "InsuranceSvcRq": {
              "RqUID": "94c5b6ae-01a6-4263-9cf0-c1fd55ddf70d",
              "PersPkgPolicyModRq": {
                "TransactionRequestDt": "#{current_time}",
                "InsuredOrPrincipal": {
                  "GeneralPartyInfo": general_info_array
                },
                "PersPolicy": {
                  "CompanyProductCd": "3B7DC5CB-53F7-4F96-90D0-A60A006A1942",
                  "ContractTerm": { 
                    "EffectiveDt": tour.departure.strftime("%Y-%m-%d"),
                    "ExpirationDt": tour.arrival.strftime("%Y-%m-%d")
                  },
                  "com.acegroup_Destination": {
                    "RqUID": destination_mapping[tour.zone],
                    "DestinationDesc": tour.zone
                  },
                  "com.acegroup_InsuredPackage": {
                    "RqUID": "851CFFF2-ED6E-4835-A7FD-9D0D0019C474",
                    "InsuredPackageDesc": "Individual"
                  },
                  "com.acegroup_Plan": {
                    "RqUID": "C96EE067-490D-4CF0-A588-A60A006A60D9",
                    "PlanDesc": "Chubb Executive"
                  }
                },
                "com.acegroup_DataExtensions": [
                  {
                    "DataItem": {
                      "@key": "PolicyCertificateNumber",
                      "@type": "System.String",
                      content!: {:value => booking.policy_number }
                    }
                  },
                  {
                    "DataItem": {
                      "@key": "HasDefaultBeneficiary",
                      "@type": "System.Boolean",
                      content!: {:value => booking.over_18? ? booking.insurance_nomination_flag : true }
                    }
                  }
                ]
              }
            }
          }, {key_converter: :none, unwrap: true})
       end 

        Rails.logger.info "Endorse message: #{message}"

        "<?xml version='1.0' encoding='utf-8'?><soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body><GetTravelEndorsement xmlns='http://ACE.Global.Travel.CRS.Schemas.ACORD.WS/'><ACORD xmlns='http://ACE.Global.Travel.CRS.Schemas.ACORD_EndorsementReq'><SignonRq><SignonPswd><CustId><SPName>ICEHOLIDAYS</SPName><CustLoginId>#{USERNAME}</CustLoginId></CustId><CustPswd><EncryptionTypeCd>None</EncryptionTypeCd><Pswd>#{PASSWORD}</Pswd></CustPswd></SignonPswd><ClientDt>#{current_time}</ClientDt><CustLangPref>en</CustLangPref><ClientApp><Org>ICE HOLIDAYS SDN BHD (INTEGRATED)</Org><Name>ICE HOLIDAYS SDN BHD (INTEGRATED)</Name><Version>1.0</Version></ClientApp></SignonRq>#{message}</ACORD></GetTravelEndorsement></soap:Body></soap:Envelope>"
      end
    else
      nil
    end
  end

  def add_message(mhash, key, value)
    if value.present?
      mhash[key] = value
    else
      mhash["#{key}/"] = nil
    end
  end
end
module BuildFlightInfo
  def build_aircrafts
    aircraft_seed = "#{Rails.root}/lib/records/opentravel_aircraft.txt"
    lines = File.readlines(aircraft_seed)
    lines.each do |line|
      row = line.sub("\n", '').split('^')
       # iata_code^manufacturer^model^iata_group^iata_category^icao_code^nb_engines^aircraft_type
      Aircraft.create(iata_code: row[0] ,manufacturer: row[1] ,model: row[2] ,iata_group: row[3] ,iata_category: row[4] ,icao_code: row[5] ,nb_engines: row[6] ,aircraft_type: row[7] )
    end
  end

  def build_airlines
    airline_seed = "#{Rails.root}/lib/records/openflights_airline.txt"
    lines = File.readlines(airline_seed)
    lines.each do |line|
      row = line.sub("\n", '').split(',')
      # id, name, alias, iata, icao, call sign, country, active
      if row[7].include?('Y') && row[1].gsub("\"", '').present? && row[3].gsub("\"", '').present?
        Airline.create(name: row[1].gsub("\"", ''), iata_code: row[3].gsub("\"", ''), country: row[6].gsub("\"", ''), remote_logo_url: "http://pics.avs.io/200/200/#{row[3].gsub("\"", '')}@2x.png")
      end
    end
  end

  def build_airport_json
    airport_seed = "#{Rails.root}/lib/records/openflights_airport.txt"
    lines = File.readlines(airport_seed)
    airports_array = []
    lines.each do |line|
      row = line.sub("\n", '').split(',')
      # id, name, city, country, iata, icao, lat, lng, altitude, timezone, dst, tz, type, source
      if row[1].gsub("\"", '').present? && !row[1].include?("\\N") && row[2].gsub("\"", '').present? && !row[2].include?("\\N") && row[3].gsub("\"", '').present? && !row[3].include?("\\N") && row[4].gsub("\"", '').present? && !row[4].include?("\\N")
        airports_array << {name: row[1].gsub("\"", ''), city: row[2].gsub("\"", ''), country: row[3].gsub("\"", ''), iata: row[4].gsub("\"", '')}
      end
    end

    if airports_array.present?
      airports_json_file = File.open("#{Rails.root}/vendor/assets/javascripts/airports.json", 'w')
      airports_json_file.puts airports_array.to_json      
      airports_json_file.close
    end
  end

end
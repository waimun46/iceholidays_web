module ExportRoamingman

  if Rails.env.iceb2b?
    ACCOUNT_ID = Rails.application.credentials.roamingman[:account_id]
    SECRET_KEY = Rails.application.credentials.roamingman[:secret_key]
    CREATE_URL = "#{Rails.application.credentials.roamingman[:url]}/api/create"
    CANCEL_URL = "#{Rails.application.credentials.roamingman[:url]}/api/cancel"
  else
    ACCOUNT_ID = "95cbc662909e48c4b492fd1b40530a7f"
    SECRET_KEY = "df37c971fbd7414a84c8ae63b21fbe56"
    CREATE_URL = "http://58.251.37.197:10186/api/create"
    CANCEL_URL = "http://58.251.37.197:10186/api/cancel"
  end

  def export_to_roamingman(booking)
    if Rails.env.iceb2b? || Rails.env.testb2b?
      begin
        response = HTTParty.post(CREATE_URL, body: construct_message_create(booking), headers: {'Content-type' => 'text; charset=utf-8'})
      rescue Net::ReadTimeout => exception
        Rails.logger.info "Exported failed: Net::ReadTimeout"
        return false
      rescue Net::OpenTimeout => exception
        Rails.logger.info "Exported failed: Net::OpenTimeout"
        return false
      rescue Errno::ECONNRESET => exception
        Rails.logger.info "Exported failed: Errno::ECONNRESET"
        return false
      end
      Rails.logger.info "Exported response: #{response}"

      order_details = JSON.parse(booking.decode_response(response)).deep_symbolize_keys

      if response.present? && order_details[:resultCode] == "0000" && order_details[:data][:vendorOrderId].present?
        return order_details
      end
      return false
    else
      construct_message_create(booking)
      return true
    end
  end

  def construct_message_create(booking)
    if booking.present?
      current_time = Time.now.in_time_zone.strftime("%Y-%m-%d %H:%M:%S")
      current_date = Time.now.in_time_zone.strftime("%Y-%m-%d")

      ship_way_mapping = {
        "delivery" => 1,
        "self_pickup" => 2
      }

      notification_mapping = {
        "yes" => 1,
        "no" => 0
      }

      deposit_mode_mapping = {
        "prepaid" => 1,
        "payment_on_the_spot" => 2
      }

      booking_hash = {}
      request_hash = {}

      if booking.mobile[0, 1] == "0"
        mobile = "6#{booking.mobile}"
      elsif booking.mobile[0, 1] == "6"
        mobile = booking.mobile
      else
        mobile = "0123456789"
      end
     
      add_message(booking_hash, "partnerOrderId", "#{current_date}#{booking.id}")
      add_message(booking_hash, "productId", booking.roamingman_package.code)
      add_message(booking_hash, "startTime", booking.depart_date.strftime("%Y-%m-%d"))
      add_message(booking_hash, "endTime", booking.return_date.strftime("%Y-%m-%d"))
      add_message(booking_hash, "quantity", booking.quantity)
      add_message(booking_hash, "contactName", booking.name)
      add_message(booking_hash, "contactMobile", mobile)
      add_message(booking_hash, "contactEmail", booking.email)
      add_message(booking_hash, "shipWay", ship_way_mapping[booking.ship_way])

      if booking.ship_way == "self_pickup"
        add_message(booking_hash, "deliverPointId", booking.deliver_point_id)
      else
        add_message(booking_hash, "address", booking.address)
        add_message(booking_hash, "postCode", booking.postcode)
        add_message(booking_hash, "countryCode", booking.country_code)
      end

      add_message(booking_hash, "isSendEmail", notification_mapping[booking.is_send_email])
      add_message(booking_hash, "isSendSms", notification_mapping[booking.is_send_sms])
      add_message(booking_hash, "depositMode", deposit_mode_mapping[booking.roamingman_booking_group.deposit_mode])
      add_message(booking_hash, "depositAmount", booking.roamingman_booking_group.deposit_amount)
      # add_message(booking_hash, "note", booking.remark.present? ? booking.remark : "")

      encode_booking = booking.encode_booking_details(booking_hash.to_json)
      Rails.logger.info "Export booking_hash: #{booking_hash.to_json}"
     
      sign_string = "#{ACCOUNT_ID}#{current_time}#{encode_booking}#{SECRET_KEY}"
      signature = booking.calculate_sign(sign_string)
      Rails.logger.info "Export sign_string: #{sign_string}"
      Rails.logger.info "Export signature: #{signature}"
      
      add_message(request_hash, "accountId", ACCOUNT_ID)
      add_message(request_hash, "requestTime", current_time)
      add_message(request_hash, "sign", signature)
      add_message(request_hash, "data", encode_booking)

      message = booking.encode_request_data(request_hash.to_json)

      Rails.logger.info "Export message: #{message}"

      message
    else
      nil
    end
  end

  def cancel_to_roamingman(booking)
    if Rails.env.iceb2b? || Rails.env.testb2b?
      begin
        response = HTTParty.post(CANCEL_URL, body: construct_message_cancel(booking), headers: {'Content-type' => 'text; charset=utf-8'})
      rescue Net::ReadTimeout => exception
        Rails.logger.info "Exported failed: Net::ReadTimeout"
        return false
      rescue Net::OpenTimeout => exception
        Rails.logger.info "Exported failed: Net::OpenTimeout"
        return false
      rescue Errno::ECONNRESET => exception
        Rails.logger.info "Exported failed: Errno::ECONNRESET"
        return false
      end
      Rails.logger.info "Exported response: #{response}"

      cancel_details = JSON.parse(booking.decode_response(response)).deep_symbolize_keys

      if response.present? && cancel_details[:resultCode] == "0000"
        return cancel_details
      end
      false
    else
      construct_message_cancel(booking)
      true
    end
  end

  def construct_message_cancel(booking)
    if booking.present?
      current_time = Time.now.in_time_zone.strftime("%Y-%m-%d %H:%M:%S")

      booking_hash = {}
      request_hash = {}
     
      add_message(booking_hash, "vendorOrderId", "#{booking.oms_order_id}")
      add_message(booking_hash, "cancelCount", booking.quantity)

      encode_booking = booking.encode_booking_details(booking_hash.to_json)
     
      sign_string = "#{ACCOUNT_ID}#{current_time}#{encode_booking}#{SECRET_KEY}"
      signature = booking.calculate_sign(sign_string)
      
      add_message(request_hash, "accountId", ACCOUNT_ID)
      add_message(request_hash, "requestTime", current_time)
      add_message(request_hash, "sign", signature)
      add_message(request_hash, "data", encode_booking)

      message = booking.encode_request_data(request_hash.to_json)

      Rails.logger.info "Export message: #{message}"

      message
    else
      nil
    end
  end

  def add_message(mhash, key, value)
    if value.present?
      mhash[key] = value
    else
      mhash["#{key}/"] = nil
    end
  end
end
module BuildHotelInfo
  def build_hotels
    travelprologue_hotels_seed = "#{Rails.root}/lib/records/travelprologue_hotels_seed.txt"
    lines = File.readlines(travelprologue_hotels_seed)
    lines.each do |line|
      eval line
    end
  end
end
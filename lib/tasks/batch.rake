# encoding: UTF-8
namespace :batch do

  desc "sending a daily sales report to the admin on 6:00p.m"
  task(:sending_daily_sales_report => :environment) do
    payments = Payment.paid.where(created_at: DateTime.parse('6pm +8') - 24.hours..DateTime.parse('5:59pm +8'))
    recipients = ['ecomm@ice-holidays.com', 'vin@ice-holidays.com', 'kenix@ice-holidays.com', 'ga@ice-holidays.com']
    if payments.present?
      ReportMailer.daily_sales_report(recipients, payments).deliver_now
    end
  end

  desc "sending a reminder to Tour booking based on Reminder Date entered"
  task(:sending_booking_reminder => :environment) do
    reminders = Reminder.where("reminder_date = ?", Date.today)
    reminders.each do |reminder|
      ReminderMailer.reminder(reminder).deliver_now
    end
  end
 
  desc "sending a reminder to KIV booking more than 2 bussiness days"
  task(:sending_reminder => :environment) do
    puts "Running rake batch:sending_reminder at #{Time.now}" if !Rails.env.test?

    holidays = Holiday.recent_holidays
    if holidays.present?
      holidays.each do |holiday|
        BusinessTime::Config.holidays << holiday
      end
    end

    if Date.current.workday?
      bgs = BookingGroup.not_travelb2b_booking.in_between(2.business_day.ago...1.business_day.ago).kiv.where("booking_groups.deposit > 0")
      bgs.each do |bg|
        # if /[a-zA-Z]/ === bg.code[0]
        #   next
        # end

        if bg.fair_booked?
          next
        end

        if bg.agent_user.present? && bg.agent_user.email.present? && bg.deposit > 0
          bg.agent_user.emails.each do |email|
            BookingMailer.reminder(bg.agent_user, bg, email).deliver_now
          end
        end
      end
    end
  end

  desc "sending a cancellation to KIV booking more than 3 bussiness days and after cut-off date"
  task(:auto_cancellation => :environment) do
    puts "Running rake batch:auto_cancellation at #{Time.now}" if !Rails.env.test?

    holidays = Holiday.recent_holidays
    if holidays.present?
      holidays.each do |holiday|
        BusinessTime::Config.holidays << holiday
      end
    end
    
    if Date.current.workday?
      # Starting from 1 January 2019, the due date changes to 5 business days
      # bgs = BookingGroup.not_travelb2b_booking.in_between(5.business_day.ago...4.business_day.ago).where(created_at: "1 January 2019".in_time_zone..."1 November 2019".in_time_zone).kiv.where("booking_groups.deposit > 0")
      # Starting from 1 Nov 2019, the due date changes to 3 business days
      bgs = BookingGroup.not_travelb2b_booking.in_between(3.business_day.ago...2.business_day.ago).where("created_at >= ?", "1 November 2019".in_time_zone).kiv.where("booking_groups.deposit > 0")
      bgs.each do |bg|

        # if bg.created_at.to_date === Date.new(2019, 1, 12)
        #   next
        # end

        if bg.fair_booked? || (bg.received_payment.present? && bg.received_payment > 0)
          next
        end

        if bg.agent_user.present? && bg.agent_user.email.present? && bg.deposit > 0
          bg.agent_user.emails.each do |email|
            BookingMailer.auto_cancellation(bg.agent_user, bg, email).deliver_now
          end
        end
        if bg.deposit > 0
          BookingAlteration.bulk_withdraw(bg.bookings, {alter_type: :withdrawn, reason: "Auto-cancellation: over 3 business days no deposit"}, nil)
        end
      end
    end

    tour_ids = Tour.in_between_cut_off((Date.current - 1.days)...(Date.current)).with_bookings.ids.uniq
    bgs = BookingGroup.not_travelb2b_booking.includes(:bookings).where(bookings: {tour_id: tour_ids}).kiv.where("booking_groups.deposit > 0")
    bgs.each do |bg|

      # if bg.created_at.to_date === Date.new(2019, 1, 12)
      #   next
      # end

      if bg.fair_booked? || (bg.received_payment.present? && bg.received_payment > 0)
        next
      end
        
      if bg.agent_user.present? && bg.agent_user.email.present? && bg.deposit > 0
        bg.agent_user.emails.each do |email|
          BookingMailer.auto_cancellation(bg.agent_user, bg, email).deliver_now
        end
      end
      if bg.deposit > 0
        BookingAlteration.bulk_withdraw(bg.bookings, {alter_type: :withdrawn, reason: "Auto-cancellation: after cut-off date no deposit"}, nil)
      end
    end
  end

  desc "process travelb2b pay_later bookings"
  task(:process_pay_later_reminder => :environment) do
    puts "Running rake batch:process_pay_later_reminder at #{Time.now}" if !Rails.env.test?

    today = Time.now

    bgs = BookingGroup.travelb2b_booking.where("created_at < ?", (today - 3.days)).where(received_payment: 0).where("booking_groups.deposit > 0")
    bgs.each do |bg|
      if bg.agent_user.present? && bg.agent_user.email.present? && bg.deposit > 0 && !bg.has_payment?
        BookingAlteration.bulk_withdraw(bg.bookings, {alter_type: :withdrawn, reason: "Auto-cancellation: over 3 days no deposit"}, nil)
        
        bg.agent_user.emails.each do |email|
          TourMailer.auto_cancellation(bg.agent_user, bg, email).deliver_now # Sending auto-cancellation email
        end
      end
    end
    
    bgs = BookingGroup.travelb2b_booking.in_between((today - 3.days)...today).where(received_payment: 0).where("booking_groups.deposit > 0")
    bgs.each do |bg|
      if bg.agent_user.present? && bg.agent_user.email.present? && bg.deposit > 0 && !bg.has_payment?
        bg.agent_user.emails.each do |email|
          TourMailer.payment_reminder(bg.agent_user, bg, email, 'deposit').deliver_now # Sending reminder for deposit payment
        end
      end
    end
  end

  desc "process travelb2b balance payment bookings"
  task(:process_balance_payment => :environment) do
    puts "Running rake batch:process_balance_payment at #{Time.now}" if !Rails.env.test?

    today = Date.current

    # Auto-cancel the guarantee departure tour's booking without balance payment on cut off date
    guaranteed_tour_ids = Tour.in_between_cut_off((today - 1.days)...today).certainty.with_bookings.ids.uniq

    # Auto-cancel the not materialise tour's booking on cut off date and cut off date - 3.days 
    not_materialise_tour_ids = Tour.in_between_cut_off((today - 1.days)...today).uncertainty.with_bookings.ids.uniq
    not_materialise_tour_ids += Tour.in_between_cut_off((today + 2.days)...(today + 3.days)).uncertainty.with_bookings.ids.uniq

    # Auto-cancel the guarantee departure tour's booking without balance payment on cut off date
    bgs = BookingGroup.travelb2b_booking.includes(:bookings).where(bookings: {tour_id: guaranteed_tour_ids}).where("received_payment < price").where("booking_groups.deposit > 0")
    bgs.each do |bg|
      if bg.agent_user.present? && bg.agent_user.email.present? && bg.deposit > 0 && !bg.full_payment? && !bg.fair_booked?
        BookingAlteration.bulk_withdraw(bg.bookings, {alter_type: :withdrawn, reason: "Auto-cancellation: after cut-off date no full payment"}, nil)

        bg.payments.paid.each do |payment|
          if payment.paid? && payment.amount > 0
            credit_refund = CreditRefund.create(amount: payment.amount, credits: payment.amount, user_id: bg.agent_user.id, payment_id: payment.id, remark: 'Credits refunded due to no full payment after cut-off dates') # Refund the credits
            if credit_refund.id.present?
              payment.update_column(credit_refund_id: credit_refund.id)
              # payment.refunded_and_action!
            end
          end
        end

        bg.agent_user.emails.each do |email|
          TourMailer.auto_cancellation(bg.agent_user, bg, email).deliver_now # Sending auto-cancellation email
        end
      end
    end

    # Auto-cancel the not materialise tour's booking on cut off date and cut off date - 3.days 
    bgs = BookingGroup.travelb2b_booking.includes(:bookings).where(bookings: {tour_id: not_materialise_tour_ids}).where("booking_groups.deposit > 0")
    bgs.each do |bg|
      if bg.agent_user.present? && bg.agent_user.email.present? && bg.deposit > 0
        BookingAlteration.bulk_withdraw(bg.bookings, {alter_type: :withdrawn, reason: "Auto-cancellation: after cut-off date the tour not materialise"}, nil)

        bg.payments.paid.each do |payment|
          if payment.paid? && payment.amount > 0
            credit_refund = CreditRefund.create(amount: payment.amount, credits: payment.amount, user_id: bg.agent_user.id, payment_id: payment.id, remark: 'Credits refunded due to tour not materialise') # Refund the credits
            if credit_refund.id.present?
              payment.update_column(credit_refund_id: credit_refund.id)
              # payment.refunded_and_action!
            end
          end
        end

        bg.agent_user.emails.each do |email|
          TourMailer.auto_cancellation(bg.agent_user, bg, email).deliver_now # Sending auto-cancellation email
        end
      end
    end

    tour_ids = Tour.in_between_cut_off((today)...(today + 3.days)).certainty.with_bookings.ids.uniq
    tour_ids += Tour.in_between_cut_off((today + 5.days)...(today + 6.days)).certainty.with_bookings.ids.uniq
    tour_ids += Tour.in_between_cut_off((today + 8.days)...(today + 9.days)).certainty.with_bookings.ids.uniq

    bgs = BookingGroup.travelb2b_booking.includes(:bookings).where(bookings: {tour_id: tour_ids}).where("received_payment < price").where("booking_groups.deposit > 0")
    bgs.each do |bg|
      if bg.agent_user.present? && bg.agent_user.email.present? && bg.deposit > 0 && bg.full_payment?
        bg.agent_user.emails.each do |email|
          TourMailer.payment_reminder(bg.agent_user, bg, email, 'balance').deliver_now # Sending reminder for balance payment
        end
      end
    end
  end


  desc "creating invoice"
  task(:creating_invoice => :environment) do
    puts "Running rake batch:creating_invoice at #{Time.now}" if !Rails.env.test?
    if Time.zone.now.day == 1
      range = Time.zone.now.last_month.beginning_of_month..Time.zone.now.last_month.end_of_month
      Invoice.auto_generate(range)
    end
  end

  desc "update globaltix ticket status to N/A when ticket is not present"
  task(:updating_globaltix => :environment) do
    puts "Running rake batch:updating_globaltix at #{Time.now}" if !Rails.env.test?
    
    Rails.cache.delete_matched("Globaltix/*")
    gtgs = GlobaltixTicket.active
    gtgs.each do |gtg|
      if !gtg.ticket.present? || gtg.ticket.id.blank? || gtg.ticket.currency.blank? || gtg.ticket.settlement_rate.blank?
        gtg.update_column :status, "n_a"
        GlobaltixTicketMailer.notify_ticket_na(gtg).deliver_now
      else
        # gtg.name = gtg.ticket.name
        # gtg.variation = gtg.ticket.variation
        gtg.currency = gtg.ticket.currency
        gtg.price = gtg.ticket.settlement_rate
        gtg.description = gtg.ticket.description
        gtg.terms_and_conditions = gtg.ticket.terms_and_conditions
        gtg.save
      end
    end
  end

  desc "set the pending payment to fail"
  task(:change_pending_payment_to_fail => :environment) do
    puts "Running rake batch:change_pending_payment_to_fail at #{Time.now}" if !Rails.env.test?
    
    Payment.pending.where("created_at <= ?", Time.now - 2.hour).each do |payment|
      payment.failed_and_action!
    end
  end

  desc "sending a balance reminder to land tour booking(deposit) in 30 days before cut_off_date"
  task(:sending_land_tour_reminder => :environment) do
    puts "Running rake batch:sending_land_tour_reminder at #{Time.now}" if !Rails.env.test?

    bgs = LandTourBookingGroup.with_booking.where("deposit > 0")
    bgs.each do |bg|
      if !bg.full_payment? && Time.now >= bg.cut_off_date - 30.days
        if bg.agent_user.present? && bg.agent_user.email.present? && bg.deposit > 0
          bg.agent_user.emails.each do |email|
            LandTourMailer.payment_reminder(bg.agent_user, bg, email).deliver_now
          end
        end
      end
    end
  end

  desc "sending a cancellation to land tour booking without full payment, after more than 3 days"
  task(:land_tour_auto_cancellation => :environment) do
    puts "Running rake batch:land_tour_auto_cancellation at #{Time.now}" if !Rails.env.test?
    bgs = LandTourBookingGroup.with_booking.in_between(3.business_day.ago...2.business_day.ago).where("land_tour_booking_groups.received_payment != land_tour_booking_groups.total_price")
    bgs.each do |bg|
      bg.payments.paid.each do |payment|
        if payment.paid? && payment.amount > 0
          credit_refund = CreditRefund.create(amount: payment.amount, credits: payment.amount, user_id: bg.agent_user.id, payment_id: payment.id, remark: 'Credits refunded due to ground tour not materialise') # Refund the credits
          if credit_refund.id.present?
            payment.update_column(credit_refund_id: credit_refund.id)
            # payment.refunded_and_action!
          end
        end
      end

      if bg.agent_user.present? && bg.agent_user.email.present?
        bg.agent_user.emails.each do |email|
          LandTourMailer.auto_cancellation(bg.agent_user, bg, email).deliver_now
        end
      end
      bg.land_tour_bookings.map(&:wihdrawn!)
    end
  end

  desc "sending land tour voucher to user, 1 week before departure"
  task(:land_tour_voucher => :environment) do
    puts "Running rake batch:land_tour_voucher at #{Time.now}" if !Rails.env.test?
    bgs = LandTourBookingGroup.with_booking.where("departure_date <= ?", Date.today + 7.days)
    bgs.each do |bg|
      if bg.agent_user.present? && bg.agent_user.email.present?
        bg.agent_user.emails.each do |email|
          LandTourMailer.voucher(bg.agent_user, bg, email).deliver_now
        end
      end
    end
  end
end
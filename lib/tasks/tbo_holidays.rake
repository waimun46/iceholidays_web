# encoding: UTF-8
namespace :tbo_holidays do
  desc "Create or update tbo holidays countries in db"
  task(:load_countries => :environment) do
    response = HotelBooker[:tbo_holidays].get_countries
    response.countries.each do |country|      
      tbo_country = TBOCountry.where(code: country.code).first_or_initialize
      tbo_country.name = country.name
      tbo_country.is_active = country.code == 'MY' # Activate Malaysia by default
      tbo_country.save
    end if response.success? 
    TBOCity.clear_cache
  end

  desc "Create or update tbo cities in db"
  task(:load_cities => :environment) do    
    TBOCountry.active.pluck(:code).each do |country_code|
      response = HotelBooker[:tbo_holidays].get_cities(country_code: country_code)      
      response.cities.each do |city|      
        tbo_city = TBOCity.where(country_code: country_code, code: city.code).first_or_initialize
        tbo_city.name = city.name
        tbo_city.save        
      end if response.success? 
      puts "#{country_code} : #{response.message}"
    end
    TBOCity.clear_cache
  end

  desc "Create or update tbo city hotels in db"
  task(:load_hotels => :environment) do
    TBOCity.includes(:country).each do |city|
      response = HotelBooker[:tbo_holidays].get_hotels(city_code: city.code)      
      response.hotels.each do |hotel|
        tbo_hotel = TBOHotel.where(city_code: city.code, hotel_code: hotel.hotel_code).first_or_initialize
        tbo_hotel.hotel_name = hotel.hotel_name
        tbo_hotel.hotel_rating = hotel.hotel_rating
        tbo_hotel.address = hotel.address
        tbo_hotel.fax_number = hotel.fax_number
        tbo_hotel.phone_number = hotel.phone_number
        tbo_hotel.country_name = hotel.country_name
        tbo_hotel.description = hotel.description
        tbo_hotel.map = hotel.map
        tbo_hotel.pin_code = hotel.pin_code
        tbo_hotel.hotel_website_url = hotel.hotel_website_url
        tbo_hotel.trip_advisor_rating = hotel.trip_advisor_rating
        tbo_hotel.trip_advisor_review_url = hotel.trip_advisor_review_url
        tbo_hotel.city_name = hotel.city_name
        tbo_hotel.hotel_location = hotel.hotel_location
        tbo_hotel.attractions = hotel.attractions
        tbo_hotel.hotel_facilities = hotel.hotel_facilities
        tbo_hotel.save
      end if response.success?
      puts "#{city.name} : #{response.message}"
    end  
  end

end
